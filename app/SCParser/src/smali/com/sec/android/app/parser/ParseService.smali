.class public Lcom/sec/android/app/parser/ParseService;
.super Landroid/app/Service;
.source "ParseService.java"


# static fields
.field public static final IS_DEBUG:Z

.field private static isFactoryMode:Z

.field private static isKeystringBlocked:Z

.field private static isPRDKeystringBlocked:Z

.field private static isReceivedFactoryModeIntent:Z

.field private static final mProductShip:Ljava/lang/String;

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field private isJig:Z

.field private final mBindHandler:Landroid/os/Handler;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mIsUserData:Z

.field private mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

.field private final mMessenger:Landroid/os/Messenger;

.field private model:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 32
    const-string v0, "ro.product_ship"

    const-string v3, "FALSE"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/ParseService;->mProductShip:Ljava/lang/String;

    .line 34
    const-string v0, "ro.product_ship"

    const-string v3, "FALSE"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v3, "TRUE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    .line 44
    const-string v0, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    .line 49
    sput-boolean v2, Lcom/sec/android/app/parser/ParseService;->isKeystringBlocked:Z

    .line 50
    sput-boolean v2, Lcom/sec/android/app/parser/ParseService;->isPRDKeystringBlocked:Z

    .line 51
    sput-boolean v1, Lcom/sec/android/app/parser/ParseService;->isFactoryMode:Z

    .line 52
    sput-boolean v1, Lcom/sec/android/app/parser/ParseService;->isReceivedFactoryModeIntent:Z

    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/parser/ParseService;->isJig:Z

    .line 58
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->model:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/sec/android/app/parser/ParseService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/ParseService$1;-><init>(Lcom/sec/android/app/parser/ParseService;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 76
    new-instance v0, Lcom/sec/android/app/parser/ParseService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/ParseService$2;-><init>(Lcom/sec/android/app/parser/ParseService;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mBindHandler:Landroid/os/Handler;

    .line 105
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/parser/ParseService;->mBindHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mMessenger:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 31
    sput-boolean p0, Lcom/sec/android/app/parser/ParseService;->isFactoryMode:Z

    return p0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 31
    sput-boolean p0, Lcom/sec/android/app/parser/ParseService;->isReceivedFactoryModeIntent:Z

    return p0
.end method

.method static synthetic access$202(Lcom/sec/android/app/parser/ParseService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/ParseService;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/parser/ParseService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/ParseService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/parser/ParseService;->process(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private initialize()Z
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isKeyStringBlocked()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/ParseService;->isKeystringBlocked:Z

    .line 190
    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isPRDKeyStringBlocked()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/ParseService;->isPRDKeystringBlocked:Z

    .line 192
    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isJigOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/parser/ParseService;->isJig:Z

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v0}, Lcom/sec/android/app/parser/KeystringCommon;->updateHiddnemenu()V

    .line 195
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->isReceivedFactoryModeIntent:Z

    if-nez v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/parser/ParseService;->isFactorySim()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/ParseService;->isFactoryMode:Z

    .line 199
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private static isJigOn()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 408
    new-instance v2, Ljava/io/File;

    const-string v4, "/sys/class/sec/switch/adc"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 410
    .local v2, "mJigOn":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 411
    const-string v4, "/sys/class/sec/switch/adc"

    invoke-static {v4}, Lcom/sec/android/app/parser/ParseService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 413
    .local v1, "jigvalue":Ljava/lang/String;
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_0

    .line 414
    const-string v4, "ParseService"

    const-string v5, "JIG: 28"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_0
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_1

    .line 418
    const-string v4, "ParseService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "JIG value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :cond_1
    const/16 v4, 0x10

    :try_start_0
    invoke-static {v1, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    const/16 v5, 0x1c

    if-ne v4, v5, :cond_4

    .line 423
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_2

    .line 424
    const-string v4, "ParseService"

    const-string v5, "JIG ON"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_2
    const/4 v3, 0x1

    .line 447
    .end local v1    # "jigvalue":Ljava/lang/String;
    :cond_3
    :goto_0
    return v3

    .line 429
    .restart local v1    # "jigvalue":Ljava/lang/String;
    :cond_4
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_3

    .line 430
    const-string v4, "ParseService"

    const-string v5, "Wrong value"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_3

    .line 437
    const-string v4, "ParseService"

    const-string v5, "value has unknown"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 443
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "jigvalue":Ljava/lang/String;
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v4, :cond_3

    .line 444
    const-string v4, "ParseService"

    const-string v5, "File Does not Exist!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isKeyStringBlocked()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 344
    const/4 v0, 0x0

    .line 346
    .local v0, "imeiBlocked":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isJigOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 347
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_0

    .line 348
    const-string v2, "ParseService"

    const-string v3, "Keystring access unblocked"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    const-string v2, "/efs/FactoryApp/keystr"

    invoke-static {v2}, Lcom/sec/android/app/parser/ParseService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_2

    .line 357
    const-string v2, "ParseService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isKeyStringBlocked : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    :cond_2
    const-string v2, "ON"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 361
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_3

    .line 362
    const-string v1, "ParseService"

    const-string v2, "isKeyStringBlocked : return true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 367
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_0

    .line 368
    const-string v2, "ParseService"

    const-string v3, "isKeyStringBlocked : return false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isPRDKeyStringBlocked()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 376
    const/4 v0, 0x0

    .line 378
    .local v0, "imeiBlocked":Ljava/lang/String;
    const-string v2, "/efs/FactoryApp/keystr"

    invoke-static {v2}, Lcom/sec/android/app/parser/ParseService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_0

    .line 381
    const-string v2, "ParseService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPRDKeyStringBlocked : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_0
    const-string v2, "ON"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 385
    const-string v2, "TRUE"

    sget-object v3, Lcom/sec/android/app/parser/ParseService;->mProductShip:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 386
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_1

    .line 387
    const-string v1, "ParseService"

    const-string v2, "isPRDKeyStringBlocked : return true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_1
    const/4 v1, 0x1

    .line 403
    :cond_2
    :goto_0
    return v1

    .line 392
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_2

    .line 393
    const-string v2, "ParseService"

    const-string v3, "isPRDKeyStringBlocked : return false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 399
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v2, :cond_2

    .line 400
    const-string v2, "ParseService"

    const-string v3, "isPRDKeyStringBlocked : return false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isfactorymode()Z
    .locals 4

    .prologue
    .line 452
    const/4 v0, 0x0

    .line 453
    .local v0, "imeiBlocked":Ljava/lang/String;
    const-string v1, "/efs/FactoryApp/factorymode"

    invoke-static {v1}, Lcom/sec/android/app/parser/ParseService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 455
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 456
    const-string v1, "ParseService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isfactorymode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    :cond_0
    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 460
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_1

    .line 461
    const-string v1, "ParseService"

    const-string v2, "isfactorymode : return true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_1
    const/4 v1, 0x1

    .line 470
    :goto_0
    return v1

    .line 466
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_3

    .line 467
    const-string v1, "ParseService"

    const-string v2, "isfactorymode: return false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized process(Ljava/lang/String;)Z
    .locals 14
    .param p1, "keystring"    # Ljava/lang/String;

    .prologue
    const/4 v13, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 203
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "keystring_noSeparators":Ljava/lang/String;
    const-string v10, "eng"

    sget-object v11, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 206
    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10}, Lcom/sec/android/app/parser/KeystringCommon;->isHiddnemenuOn()Z

    move-result v10

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lcom/sec/android/app/parser/ParseService;->isJig:Z

    if-nez v10, :cond_1

    const-string v10, "##366633#"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10}, Lcom/sec/android/app/parser/KeystringCommon;->isVzwFamily()Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "SCH-I545L"

    iget-object v11, p0, Lcom/sec/android/app/parser/ParseService;->model:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 209
    sget-boolean v9, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v9, :cond_0

    .line 210
    const-string v9, "ParseService"

    const-string v10, "Keystring blocked by hiddnemenu is disabled"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    :cond_0
    :goto_0
    monitor-exit p0

    return v8

    .line 217
    :cond_1
    :try_start_1
    const-string v10, "**04"

    invoke-virtual {p1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "**05"

    invoke-virtual {p1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    :cond_2
    const-string v10, "#"

    invoke-virtual {p1, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 218
    const/4 v6, 0x0

    .line 219
    .local v6, "subscription":I
    iget-boolean v10, p0, Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z

    if-nez v10, :cond_4

    .line 220
    sget-boolean v8, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v8, :cond_3

    .line 221
    const-string v8, "ParseService"

    const-string v10, "Input from ext source"

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    move v8, v9

    .line 224
    goto :goto_0

    .line 228
    :cond_4
    :try_start_2
    const-string v10, "ParseService"

    const-string v11, "Entering Here!!!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/parser/ParseService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "CURRENT_NETWORK"

    const/4 v12, -0x1

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 232
    .local v0, "currentNetwork":I
    if-eq v0, v13, :cond_5

    const-string v10, "CHN"

    sget-object v11, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    :cond_5
    const-string v10, "CHU"

    sget-object v11, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 233
    :cond_6
    if-nez v0, :cond_7

    .line 234
    const-string v9, "phone"

    invoke-static {v9}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v9

    invoke-interface {v9, p1}, Lcom/android/internal/telephony/ITelephony;->handlePinMmi(Ljava/lang/String;)Z

    move-result v8

    goto :goto_0

    .line 236
    :cond_7
    if-ne v0, v9, :cond_8

    .line 237
    const-string v9, "phone2"

    invoke-static {v9}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v9

    invoke-interface {v9, p1}, Lcom/android/internal/telephony/ITelephony;->handlePinMmi(Ljava/lang/String;)Z

    move-result v8

    goto :goto_0

    .line 243
    :cond_8
    const-string v10, "persist.radio.calldefault.simid"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 245
    const-string v10, "ParseService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Sending MMI on subscription :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const-string v10, "phone2"

    invoke-static {v10}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v5

    .line 248
    .local v5, "phone2":Lcom/android/internal/telephony/ITelephony;
    if-ne v6, v9, :cond_9

    if-eqz v5, :cond_9

    .line 249
    invoke-interface {v5, p1}, Lcom/android/internal/telephony/ITelephony;->handlePinMmi(Ljava/lang/String;)Z

    move-result v8

    goto/16 :goto_0

    .line 251
    :cond_9
    const-string v9, "phone"

    invoke-static {v9}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v9

    invoke-interface {v9, p1}, Lcom/android/internal/telephony/ITelephony;->handlePinMmi(Ljava/lang/String;)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    goto/16 :goto_0

    .line 254
    .end local v0    # "currentNetwork":I
    .end local v5    # "phone2":Lcom/android/internal/telephony/ITelephony;
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v9, "ParseService"

    const-string v10, "Failed to handlePinMmi due to remote exception"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 203
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v4    # "keystring_noSeparators":Ljava/lang/String;
    .end local v6    # "subscription":I
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 260
    .restart local v4    # "keystring_noSeparators":Ljava/lang/String;
    :cond_a
    :try_start_4
    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->hasKeystring(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_e

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x9

    if-ne v10, v11, :cond_e

    const-string v10, "##"

    invoke-virtual {p1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    const-string v10, "#"

    invoke-virtual {p1, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    const-string v10, "**"

    const/4 v11, 0x4

    const/4 v12, 0x6

    invoke-virtual {p1, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "XAS"

    sget-object v11, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    sget-object v10, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    const-string v11, "XAR"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    sget-object v10, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    const-string v11, "BST"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    sget-object v10, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    const-string v11, "SPR"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    sget-object v10, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    const-string v11, "VMU"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 266
    :cond_b
    iget-boolean v8, p0, Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z

    if-nez v8, :cond_d

    .line 267
    sget-boolean v8, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v8, :cond_c

    .line 268
    const-string v8, "ParseService"

    const-string v10, "Input from ext source"

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    move v8, v9

    .line 271
    goto/16 :goto_0

    .line 274
    :cond_d
    const-string v8, "ParseService"

    const-string v10, "Entering ##_#!!!"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    new-instance v2, Landroid/content/Intent;

    const-string v8, "android.provider.Telephony.SECRET_CODE"

    const-string v10, "android_secret_code://MSL_OTKSL"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v2, v8, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 277
    .local v2, "intent":Landroid/content/Intent;
    const-string v8, "String"

    const/4 v10, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v8, "com.sec.android.app.hiddenmenu.permission.KEYSTRING"

    invoke-virtual {p0, v2, v8}, Lcom/sec/android/app/parser/ParseService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    move v8, v9

    .line 279
    goto/16 :goto_0

    .line 282
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_e
    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->hasKeystring(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_f

    .line 283
    sget-boolean v9, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v9, :cond_0

    .line 284
    const-string v9, "ParseService"

    const-string v10, "Keystring not in the list"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 288
    :cond_f
    sget-boolean v10, Lcom/sec/android/app/parser/ParseService;->isFactoryMode:Z

    if-nez v10, :cond_10

    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->getFactorymodeFlag(Ljava/lang/String;)Z

    move-result v10

    if-ne v10, v9, :cond_10

    .line 290
    sget-boolean v9, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v9, :cond_0

    .line 291
    const-string v9, "ParseService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isFactoryMode:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/parser/ParseService;->isFactoryMode:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 295
    :cond_10
    sget-boolean v10, Lcom/sec/android/app/parser/ParseService;->isKeystringBlocked:Z

    if-ne v10, v9, :cond_11

    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->getKeystringBlockFlag(Ljava/lang/String;)Z

    move-result v10

    if-ne v10, v9, :cond_11

    .line 297
    sget-boolean v9, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v9, :cond_0

    .line 298
    const-string v9, "ParseService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isKeystringBlocked:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/parser/ParseService;->isKeystringBlocked:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 302
    :cond_11
    const-string v10, "*#27663368378#"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    const-string v10, "CTC"

    sget-object v11, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    sget-boolean v10, Lcom/sec/android/app/parser/ParseService;->isPRDKeystringBlocked:Z

    if-ne v10, v9, :cond_12

    .line 305
    sget-boolean v9, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v9, :cond_0

    .line 306
    const-string v9, "ParseService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isPRDKeystringBlocked:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/parser/ParseService;->isPRDKeystringBlocked:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 311
    :cond_12
    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 312
    .local v7, "uri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/parser/KeystringCommon;->isGoogleKeystring(Ljava/lang/String;)Z

    move-result v3

    .line 314
    .local v3, "isGoogleKeystring":Z
    if-eqz v7, :cond_0

    .line 315
    iget-boolean v8, p0, Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z

    if-nez v8, :cond_14

    .line 316
    sget-boolean v8, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v8, :cond_13

    .line 317
    const-string v8, "ParseService"

    const-string v10, "Input from ext source"

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    move v8, v9

    .line 320
    goto/16 :goto_0

    .line 323
    :cond_14
    sget-boolean v8, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v8, :cond_15

    .line 324
    const-string v8, "ParseService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "uri : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_15
    new-instance v2, Landroid/content/Intent;

    const-string v8, "android.provider.Telephony.SECRET_CODE"

    invoke-direct {v2, v8, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 329
    .restart local v2    # "intent":Landroid/content/Intent;
    const/16 v8, 0x20

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0, v2}, Lcom/sec/android/app/parser/ParseService;->sendBroadcast(Landroid/content/Intent;)V

    .line 332
    sget-boolean v8, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v8, :cond_16

    .line 333
    const-string v8, "ParseService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Go Keystring : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_16
    move v8, v9

    .line 336
    goto/16 :goto_0
.end method

.method private static readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 496
    const-string v7, ""

    .line 497
    .local v7, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 498
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 499
    .local v5, "fr":Ljava/io/FileReader;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 501
    const-string v8, ""

    .line 534
    :goto_0
    return-object v8

    .line 504
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v8, 0x1fa0

    invoke-direct {v1, v6, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 507
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_1

    .line 508
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    .line 518
    :cond_1
    if-eqz v6, :cond_2

    .line 519
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 522
    :cond_2
    if-eqz v1, :cond_3

    .line 523
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 531
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_4
    :goto_1
    if-nez v7, :cond_9

    .line 532
    const-string v8, ""

    goto :goto_0

    .line 525
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 526
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "ParseService"

    const-string v9, "IOException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 529
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .line 510
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 511
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v8, "ParseService"

    const-string v9, "FileNotFoundException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 518
    if-eqz v5, :cond_5

    .line 519
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 522
    :cond_5
    if-eqz v0, :cond_4

    .line 523
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 525
    :catch_2
    move-exception v2

    .line 526
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "ParseService"

    const-string v9, "IOException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 513
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 514
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v8, "ParseService"

    const-string v9, "IOException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 518
    if-eqz v5, :cond_6

    .line 519
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 522
    :cond_6
    if-eqz v0, :cond_4

    .line 523
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 525
    :catch_4
    move-exception v2

    .line 526
    const-string v8, "ParseService"

    const-string v9, "IOException"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 517
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 518
    :goto_4
    if-eqz v5, :cond_7

    .line 519
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 522
    :cond_7
    if-eqz v0, :cond_8

    .line 523
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 528
    :cond_8
    :goto_5
    throw v8

    .line 525
    :catch_5
    move-exception v2

    .line 526
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v9, "ParseService"

    const-string v10, "IOException"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 534
    .end local v2    # "e":Ljava/io/IOException;
    :cond_9
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 517
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v8

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 513
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 510
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method


# virtual methods
.method public isFactorySim()Z
    .locals 5

    .prologue
    .line 476
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/parser/ParseService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 477
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    const-string v0, "999999999999999"

    .line 478
    .local v0, "IMSI":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 480
    .local v1, "imsi":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "999999999999999"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 481
    sget-boolean v3, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v3, :cond_0

    .line 482
    const-string v3, "ParseService"

    const-string v4, "isFactorySim : true"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_0
    const/4 v3, 0x1

    .line 492
    :goto_0
    return v3

    .line 487
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v3, :cond_2

    .line 488
    const-string v3, "ParseService"

    const-string v4, "isFactorySim : false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :cond_2
    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isfactorymode()Z

    move-result v3

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 125
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "ParseService"

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 109
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 110
    const-string v1, "ParseService"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    if-nez v1, :cond_1

    .line 114
    new-instance v1, Lcom/sec/android/app/parser/KeystringCommon;

    invoke-direct {v1, p0}, Lcom/sec/android/app/parser/KeystringCommon;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    .line 117
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SET_FACTORY_SIM_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/parser/ParseService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/parser/ParseService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/parser/ParseService;->initialize()Z

    .line 121
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 141
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "ParseService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/parser/ParseService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    if-eqz v0, :cond_2

    .line 151
    const-string v0, "VZW"

    sget-object v1, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v0}, Lcom/sec/android/app/parser/KeystringCommon;->unregistReceiverForSpecialKeystring_VZW()V

    .line 158
    :cond_2
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 159
    return-void

    .line 153
    :cond_3
    const-string v0, "TFN"

    sget-object v1, Lcom/sec/android/app/parser/ParseService;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/parser/ParseService;->mKeystringTable:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v0}, Lcom/sec/android/app/parser/KeystringCommon;->unregistReceiverForSpecialKeystring_TFN()V

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 134
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "ParseService"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 163
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 164
    const-string v1, "ParseService"

    const-string v2, "onStartCommand()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    const-string v0, ""

    .line 169
    .local v0, "keystring":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 170
    const-string v1, "KEYSTRING"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_2

    .line 174
    const-string v1, "ParseService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Get Keystring via start service : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/parser/ParseService;->initialize()Z

    .line 179
    if-eqz v0, :cond_3

    .line 180
    invoke-direct {p0, v0}, Lcom/sec/android/app/parser/ParseService;->process(Ljava/lang/String;)Z

    .line 184
    :cond_3
    const-string v1, "ParseService"

    const-string v2, "onStartCommand() - return START_STICKY"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v1, 0x1

    return v1
.end method
