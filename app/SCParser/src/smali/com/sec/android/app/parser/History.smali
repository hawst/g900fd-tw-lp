.class Lcom/sec/android/app/parser/History;
.super Ljava/lang/Object;
.source "History.java"


# instance fields
.field mEntries:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/parser/HistoryEntry;",
            ">;"
        }
    .end annotation
.end field

.field mObserver:Landroid/widget/BaseAdapter;

.field mPos:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/parser/History;->clear()V

    .line 37
    return-void
.end method

.method constructor <init>(ILjava/io/DataInput;)V
    .locals 5
    .param p1, "version"    # I
    .param p2, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    .line 40
    const/4 v2, 0x1

    if-lt p1, v2, :cond_1

    .line 41
    invoke-interface {p2}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 43
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    new-instance v3, Lcom/sec/android/app/parser/HistoryEntry;

    invoke-direct {v3, p1, p2}, Lcom/sec/android/app/parser/HistoryEntry;-><init>(ILjava/io/DataInput;)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    invoke-interface {p2}, Ljava/io/DataInput;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/parser/History;->mPos:I

    .line 51
    return-void

    .line 49
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private notifyChanged()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/parser/History;->mObserver:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/parser/History;->mObserver:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method clear()V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    new-instance v1, Lcom/sec/android/app/parser/HistoryEntry;

    const-string v2, ""

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/parser/HistoryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/parser/History;->mPos:I

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/parser/History;->notifyChanged()V

    .line 69
    return-void
.end method

.method write(Ljava/io/DataOutput;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v2, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/parser/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/parser/HistoryEntry;

    .line 86
    .local v0, "entry":Lcom/sec/android/app/parser/HistoryEntry;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/parser/HistoryEntry;->write(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 89
    .end local v0    # "entry":Lcom/sec/android/app/parser/HistoryEntry;
    :cond_0
    iget v2, p0, Lcom/sec/android/app/parser/History;->mPos:I

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 90
    return-void
.end method
