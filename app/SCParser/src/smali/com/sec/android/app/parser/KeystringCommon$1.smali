.class Lcom/sec/android/app/parser/KeystringCommon$1;
.super Landroid/content/BroadcastReceiver;
.source "KeystringCommon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/parser/KeystringCommon;->registReceiverForSpecialKeystring_VZW()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/parser/KeystringCommon;


# direct methods
.method constructor <init>(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0

    .prologue
    .line 1177
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1179
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1181
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/KeystringCommon;->updateHiddnemenu()V

    .line 1183
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 1184
    const-string v1, "KeystringCommon"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVZWReceiver.onReceive, action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1187
    :cond_0
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_ENABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1188
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringVZW()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$500(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1189
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->registerFactoryModeKeystrings()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$600(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1191
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$700(Lcom/sec/android/app/parser/KeystringCommon;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1192
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringACG()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$800(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1194
    :cond_1
    const-string v1, "CRI"

    # getter for: Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/parser/KeystringCommon;->access$900()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1195
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringCRI()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1000(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1208
    :cond_2
    :goto_0
    return-void

    .line 1198
    :cond_3
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_DISABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1200
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringVZW()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1100(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1201
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$700(Lcom/sec/android/app/parser/KeystringCommon;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1202
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringACG()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1200(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1204
    :cond_4
    const-string v1, "CRI"

    # getter for: Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/parser/KeystringCommon;->access$900()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1205
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$1;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringCRI()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1300(Lcom/sec/android/app/parser/KeystringCommon;)V

    goto :goto_0
.end method
