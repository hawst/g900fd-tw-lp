.class public Lcom/sec/android/app/parser/KeystrBlockCheck;
.super Ljava/lang/Object;
.source "KeystrBlockCheck.java"


# static fields
.field private static mAlwaysOpenKeystringList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final mCountryCode:Ljava/lang/String;

.field private static final mGlobalOpenKeystringList:[Ljava/lang/String;

.field private static final mModelName:Ljava/lang/String;

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field private IMEI_NUMBER:Ljava/lang/String;

.field private device:Ljava/lang/String;

.field private mIsModelGSM:Z

.field private mName:Ljava/lang/String;

.field private mNetworkCountry:Ljava/lang/String;

.field private mPhoneType:I

.field private mResultKeystring:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    .line 18
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mModelName:Ljava/lang/String;

    .line 20
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mCountryCode:Ljava/lang/String;

    .line 34
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "*#12580*369#"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "*#0011#"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "*#22558463#"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*#0*#"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "*#0228#"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "*#0283#"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "*#06#"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "*#7353#"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "*#66336#"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "*#22228378#"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "*#1106#"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mGlobalOpenKeystringList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "ro.product.device"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->device:Ljava/lang/String;

    .line 24
    const-string v0, "ro.product.name"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mName:Ljava/lang/String;

    .line 28
    iput v2, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mPhoneType:I

    .line 29
    const-string v0, "open"

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mNetworkCountry:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mResultKeystring:Ljava/lang/String;

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mIsModelGSM:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->IMEI_NUMBER:Ljava/lang/String;

    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/app/parser/KeystrBlockCheck;->initSettings(Landroid/content/Context;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystrBlockCheck;->setFlagsForAlwaysOpenKeystrings()V

    .line 89
    return-void
.end method

.method private initSettings(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    const-string v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 93
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mPhoneType:I

    .line 94
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->IMEI_NUMBER:Ljava/lang/String;

    .line 96
    const-string v1, "us"

    iput-object v1, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mNetworkCountry:Ljava/lang/String;

    .line 97
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    .line 98
    invoke-static {}, Lcom/sec/android/app/parser/KeystrBlockCheck;->isModelCDMA()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v2, "TFN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mIsModelGSM:Z

    .line 99
    return-void

    .line 98
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static final isModelCDMA()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    const/4 v2, 0x0

    .line 103
    .local v2, "result":Z
    const/16 v5, 0x14

    new-array v0, v5, [Ljava/lang/String;

    const-string v5, "VZW"

    aput-object v5, v0, v3

    const-string v5, "SPR"

    aput-object v5, v0, v4

    const/4 v5, 0x2

    const-string v6, "XAS"

    aput-object v6, v0, v5

    const/4 v5, 0x3

    const-string v6, "VMU"

    aput-object v6, v0, v5

    const/4 v5, 0x4

    const-string v6, "BST"

    aput-object v6, v0, v5

    const/4 v5, 0x5

    const-string v6, "USC"

    aput-object v6, v0, v5

    const/4 v5, 0x6

    const-string v6, "MTR"

    aput-object v6, v0, v5

    const/4 v5, 0x7

    const-string v6, "XAR"

    aput-object v6, v0, v5

    const/16 v5, 0x8

    const-string v6, "SKT"

    aput-object v6, v0, v5

    const/16 v5, 0x9

    const-string v6, "KTT"

    aput-object v6, v0, v5

    const/16 v5, 0xa

    const-string v6, "LGT"

    aput-object v6, v0, v5

    const/16 v5, 0xb

    const-string v6, "SKC"

    aput-object v6, v0, v5

    const/16 v5, 0xc

    const-string v6, "KTC"

    aput-object v6, v0, v5

    const/16 v5, 0xd

    const-string v6, "LUC"

    aput-object v6, v0, v5

    const/16 v5, 0xe

    const-string v6, "SKO"

    aput-object v6, v0, v5

    const/16 v5, 0xf

    const-string v6, "KTO"

    aput-object v6, v0, v5

    const/16 v5, 0x10

    const-string v6, "LUO"

    aput-object v6, v0, v5

    const/16 v5, 0x11

    const-string v6, "ANY"

    aput-object v6, v0, v5

    const/16 v5, 0x12

    const-string v6, "KOO"

    aput-object v6, v0, v5

    const/16 v5, 0x13

    const-string v6, "ACG"

    aput-object v6, v0, v5

    .line 126
    .local v0, "cdmalist":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_2

    .line 127
    if-nez v2, :cond_0

    aget-object v5, v0, v1

    sget-object v6, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v2, v4

    .line 126
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 127
    goto :goto_1

    .line 130
    :cond_2
    return v2
.end method


# virtual methods
.method public isKeystringAlwaysOpen(Ljava/lang/String;)Z
    .locals 2
    .param p1, "keystring"    # Ljava/lang/String;

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 427
    .local v0, "result":Z
    sget-object v1, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    sget-object v1, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 431
    :cond_0
    return v0
.end method

.method public setFlagsForAlwaysOpenKeystrings()V
    .locals 54

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mPhoneType:I

    move/from16 v50, v0

    if-nez v50, :cond_a

    const/16 v49, 0x1

    .line 138
    .local v49, "isModelWifi":Z
    :goto_0
    if-nez v49, :cond_b

    invoke-static {}, Lcom/sec/android/app/parser/KeystrBlockCheck;->isModelCDMA()Z

    move-result v50

    if-nez v50, :cond_b

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "TFN"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_b

    const/16 v19, 0x1

    .line 139
    .local v19, "isModelGSM":Z
    :goto_1
    if-nez v49, :cond_c

    invoke-static {}, Lcom/sec/android/app/parser/KeystrBlockCheck;->isModelCDMA()Z

    move-result v50

    if-eqz v50, :cond_c

    const/4 v11, 0x1

    .line 142
    .local v11, "isModelCDMA":Z
    :goto_2
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "TMB"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    .line 144
    .local v36, "isModelTMO":Z
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "VZW"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    .line 145
    .local v47, "isModelVZW":Z
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "SPR"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_0

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "XAS"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_0

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "BST"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_d

    :cond_0
    const/16 v33, 0x1

    .line 147
    .local v33, "isModelSPR":Z
    :goto_3
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "CRI"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 148
    .local v12, "isModelCRI":Z
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "ATT"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_1

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "AIO"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_e

    :cond_1
    const/4 v8, 0x1

    .line 149
    .local v8, "isModelATT":Z
    :goto_4
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "XEF"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 150
    .local v17, "isModelFRA":Z
    if-eqz v19, :cond_f

    if-nez v8, :cond_2

    if-eqz v36, :cond_f

    :cond_2
    const/16 v42, 0x1

    .line 152
    .local v42, "isModelUSGSM":Z
    :goto_5
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mCountryCode:Ljava/lang/String;

    const-string v51, "USA"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    .line 153
    .local v41, "isModelUS":Z
    const-string v50, "RWC/TLS/BMC/BWA"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    .line 154
    .local v20, "isModelI747M":Z
    const-string v50, "VTR/MCT/GLW"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v34

    .line 155
    .local v34, "isModelT999V":Z
    if-nez v20, :cond_3

    if-nez v34, :cond_3

    const-string v50, "RCS/RWC/XAC/FMC/CAN/BMC/BWA/MTS/VMC/FNC/CHR/TLS/CLN/KDO/SOL/PCM/PMB/VTR/GLW/MTA/ESK/SPC"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_10

    :cond_3
    const/4 v10, 0x1

    .line 160
    .local v10, "isModelCAN":Z
    :goto_6
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mCountryCode:Ljava/lang/String;

    const-string v51, "KOREA"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    .line 161
    .local v25, "isModelKOR":Z
    if-eqz v25, :cond_11

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "SKT"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_4

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "SKC"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_4

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "SKO"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_11

    :cond_4
    const/16 v32, 0x1

    .line 162
    .local v32, "isModelSKT":Z
    :goto_7
    if-eqz v25, :cond_12

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "KTT"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_5

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "KTC"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_5

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "KTO"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_12

    :cond_5
    const/16 v26, 0x1

    .line 163
    .local v26, "isModelKTT":Z
    :goto_8
    if-eqz v25, :cond_13

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "LGT"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_6

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "LUC"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_6

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "LUO"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_13

    :cond_6
    const/16 v27, 0x1

    .line 164
    .local v27, "isModelLGT":Z
    :goto_9
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "CTC"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 165
    .local v13, "isModelCTC":Z
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mModelName:Ljava/lang/String;

    const-string v51, "P5100"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-nez v50, :cond_7

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mModelName:Ljava/lang/String;

    const-string v51, "P5110"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_14

    :cond_7
    const/4 v7, 0x1

    .line 166
    .local v7, "isModel5100":Z
    :goto_a
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "TFN"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    .line 167
    .local v37, "isModelTRF":Z
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "VMU"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    .line 168
    .local v46, "isModelVMU":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->mName:Ljava/lang/String;

    move-object/from16 v50, v0

    const-string v51, "ue"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v48

    .line 170
    .local v48, "isModelVZWUE":Z
    const-string v50, "ILO/PTR/PCL/CEL/MIR"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    .line 171
    .local v22, "isModelISRAEL":Z
    const-string v50, "FTM/SFR/BOG/FRE/DIG/VGF/NRJ/ORC/OUT/LPM/LBM/FTB/ORN"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_15

    const/16 v18, 0x1

    .line 174
    .local v18, "isModelFRANCE":Z
    :goto_b
    const-string v50, "INS/INU/NPL/ETR/TML/SLK"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    .line 176
    .local v21, "isModelINDIA":Z
    const-string v50, "XTC/SMA/GLB/XTE"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_16

    const/16 v29, 0x1

    .line 177
    .local v29, "isModelPHILI":Z
    :goto_c
    const-string v50, "SIN/MM1/STH/XSP"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_17

    const/16 v31, 0x1

    .line 183
    .local v31, "isModelSING":Z
    :goto_d
    const-string v50, "EVR/ORA/TMU/TMP/FTM/IDE/AMN/ORO/ORS/ORG/OPT/MST"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_18

    const/16 v28, 0x1

    .line 187
    .local v28, "isModelOrange":Z
    :goto_e
    const-string v50, "CYV/OMN/TCL/VD2/VDI/VOD"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_19

    const/16 v43, 0x1

    .line 188
    .local v43, "isModelVFG":Z
    :goto_f
    const-string v50, "TEL"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1a

    const/16 v39, 0x1

    .line 190
    .local v39, "isModelTelstra":Z
    :goto_10
    const-string v50, "ATL/CNX/TCL/VDH/VDI/VDP/VDR/VNL/VOP"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1b

    const/16 v44, 0x1

    .line 192
    .local v44, "isModelVFN":Z
    :goto_11
    const-string v50, "OMN/VD2/VDC/VDF/VGR/VNN/VOD/XFV"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1c

    const/16 v45, 0x1

    .line 194
    .local v45, "isModelVFR":Z
    :goto_12
    const-string v50, "SWR/PRO/VOM/COA/XEF/BOG/NRJ/TIM/KPN/PLS/VNN/DTM/COS/CRO/MAX/MBM/TMH/TMS/TMZ/TNL/TPL/TRG/DTR/TTR/H3G/3IE/DRE/HUI/HTS/HTD/XEC/O2U/O2I/O2C/VIA/VIT/VID/VD2/VDR/OMN/ATL/VDF/VDP/TCL/VDI/VGR/VDH/VDC/CNX/AVF/CYV/SWC/MOB/VIP/SIM/MTL/VOD/VOP/TOP/XFV/SFR/EVR/ORA/TMU/TMP/FTM/IDE/AMN/ONE/ORO/ORS/ORG/OPT/MST"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1d

    const/4 v15, 0x1

    .line 196
    .local v15, "isModelEUR":Z
    :goto_13
    const-string v50, "SIN/MM1/STH/XSP/MXI/CLM/XME/XTC/SMA/GLB/XTE/XSE/XXV/XEV/THL/THW/MYA/CAM/LAO"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1e

    const/16 v30, 0x1

    .line 198
    .local v30, "isModelSEA":Z
    :goto_14
    const-string v50, "DTM/MAX/TNL/TMZ/COA/TPL/TMS/COS/TTR/TRG/CRO/MBM/TMH/TBM/DTR/TNP"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1f

    const/16 v16, 0x1

    .line 200
    .local v16, "isModelEurDtGPG":Z
    :goto_15
    const-string v50, "O2U/O2I/O2C/VIA/VIT/VID/XEC"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_20

    const/16 v38, 0x1

    .line 202
    .local v38, "isModelTeleO2":Z
    :goto_16
    if-eqz v49, :cond_21

    if-eqz v8, :cond_21

    const/4 v9, 0x1

    .line 203
    .local v9, "isModelAttWifi":Z
    :goto_17
    if-eqz v49, :cond_22

    if-eqz v36, :cond_22

    const/16 v40, 0x1

    .line 206
    .local v40, "isModelTmoWifi":Z
    :goto_18
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mCountryCode:Ljava/lang/String;

    const-string v51, "JP"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    .line 207
    .local v23, "isModelJPN":Z
    if-eqz v23, :cond_23

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "DCM"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_23

    const/4 v14, 0x1

    .line 208
    .local v14, "isModelDCM":Z
    :goto_19
    if-eqz v23, :cond_24

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "KDI"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_24

    const/16 v24, 0x1

    .line 210
    .local v24, "isModelKDI":Z
    :goto_1a
    const-string v50, "CHN"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_8

    const-string v50, "CHU"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_8

    const-string v50, "CTC"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_8

    const-string v50, "CHM"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_8

    const-string v50, "CHC"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_25

    :cond_8
    const/4 v5, 0x1

    .line 211
    .local v5, "isCHN":Z
    :goto_1b
    const-string v50, "TGY"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_9

    const-string v50, "BRI"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_9

    const-string v50, "CWT"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_9

    const-string v50, "TWN"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_9

    const-string v50, "FET"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_9

    const-string v50, "ZZH"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_26

    :cond_9
    const/4 v6, 0x1

    .line 213
    .local v6, "isHKTW":Z
    :goto_1c
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mCountryCode:Ljava/lang/String;

    const-string v51, "MEXICO"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_27

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v51, "TCE"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_27

    const/16 v35, 0x1

    .line 215
    .local v35, "isModelTCE":Z
    :goto_1d
    const-string v50, "ACG"

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_28

    .line 216
    const/4 v4, 0x1

    .line 223
    .local v4, "isACG":Z
    :goto_1e
    const/4 v3, 0x0

    .local v3, "count":I
    :goto_1f
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mGlobalOpenKeystringList:[Ljava/lang/String;

    move-object/from16 v0, v50

    array-length v0, v0

    move/from16 v50, v0

    move/from16 v0, v50

    if-ge v3, v0, :cond_2a

    .line 224
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mGlobalOpenKeystringList:[Ljava/lang/String;

    aget-object v51, v51, v3

    const/16 v52, 0x1

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    .line 137
    .end local v3    # "count":I
    .end local v4    # "isACG":Z
    .end local v5    # "isCHN":Z
    .end local v6    # "isHKTW":Z
    .end local v7    # "isModel5100":Z
    .end local v8    # "isModelATT":Z
    .end local v9    # "isModelAttWifi":Z
    .end local v10    # "isModelCAN":Z
    .end local v11    # "isModelCDMA":Z
    .end local v12    # "isModelCRI":Z
    .end local v13    # "isModelCTC":Z
    .end local v14    # "isModelDCM":Z
    .end local v15    # "isModelEUR":Z
    .end local v16    # "isModelEurDtGPG":Z
    .end local v17    # "isModelFRA":Z
    .end local v18    # "isModelFRANCE":Z
    .end local v19    # "isModelGSM":Z
    .end local v20    # "isModelI747M":Z
    .end local v21    # "isModelINDIA":Z
    .end local v22    # "isModelISRAEL":Z
    .end local v23    # "isModelJPN":Z
    .end local v24    # "isModelKDI":Z
    .end local v25    # "isModelKOR":Z
    .end local v26    # "isModelKTT":Z
    .end local v27    # "isModelLGT":Z
    .end local v28    # "isModelOrange":Z
    .end local v29    # "isModelPHILI":Z
    .end local v30    # "isModelSEA":Z
    .end local v31    # "isModelSING":Z
    .end local v32    # "isModelSKT":Z
    .end local v33    # "isModelSPR":Z
    .end local v34    # "isModelT999V":Z
    .end local v35    # "isModelTCE":Z
    .end local v36    # "isModelTMO":Z
    .end local v37    # "isModelTRF":Z
    .end local v38    # "isModelTeleO2":Z
    .end local v39    # "isModelTelstra":Z
    .end local v40    # "isModelTmoWifi":Z
    .end local v41    # "isModelUS":Z
    .end local v42    # "isModelUSGSM":Z
    .end local v43    # "isModelVFG":Z
    .end local v44    # "isModelVFN":Z
    .end local v45    # "isModelVFR":Z
    .end local v46    # "isModelVMU":Z
    .end local v47    # "isModelVZW":Z
    .end local v48    # "isModelVZWUE":Z
    .end local v49    # "isModelWifi":Z
    :cond_a
    const/16 v49, 0x0

    goto/16 :goto_0

    .line 138
    .restart local v49    # "isModelWifi":Z
    :cond_b
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 139
    .restart local v19    # "isModelGSM":Z
    :cond_c
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 145
    .restart local v11    # "isModelCDMA":Z
    .restart local v36    # "isModelTMO":Z
    .restart local v47    # "isModelVZW":Z
    :cond_d
    const/16 v33, 0x0

    goto/16 :goto_3

    .line 148
    .restart local v12    # "isModelCRI":Z
    .restart local v33    # "isModelSPR":Z
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 150
    .restart local v8    # "isModelATT":Z
    .restart local v17    # "isModelFRA":Z
    :cond_f
    const/16 v42, 0x0

    goto/16 :goto_5

    .line 155
    .restart local v20    # "isModelI747M":Z
    .restart local v34    # "isModelT999V":Z
    .restart local v41    # "isModelUS":Z
    .restart local v42    # "isModelUSGSM":Z
    :cond_10
    const/4 v10, 0x0

    goto/16 :goto_6

    .line 161
    .restart local v10    # "isModelCAN":Z
    .restart local v25    # "isModelKOR":Z
    :cond_11
    const/16 v32, 0x0

    goto/16 :goto_7

    .line 162
    .restart local v32    # "isModelSKT":Z
    :cond_12
    const/16 v26, 0x0

    goto/16 :goto_8

    .line 163
    .restart local v26    # "isModelKTT":Z
    :cond_13
    const/16 v27, 0x0

    goto/16 :goto_9

    .line 165
    .restart local v13    # "isModelCTC":Z
    .restart local v27    # "isModelLGT":Z
    :cond_14
    const/4 v7, 0x0

    goto/16 :goto_a

    .line 171
    .restart local v7    # "isModel5100":Z
    .restart local v22    # "isModelISRAEL":Z
    .restart local v37    # "isModelTRF":Z
    .restart local v46    # "isModelVMU":Z
    .restart local v48    # "isModelVZWUE":Z
    :cond_15
    const/16 v18, 0x0

    goto/16 :goto_b

    .line 176
    .restart local v18    # "isModelFRANCE":Z
    .restart local v21    # "isModelINDIA":Z
    :cond_16
    const/16 v29, 0x0

    goto/16 :goto_c

    .line 177
    .restart local v29    # "isModelPHILI":Z
    :cond_17
    const/16 v31, 0x0

    goto/16 :goto_d

    .line 183
    .restart local v31    # "isModelSING":Z
    :cond_18
    const/16 v28, 0x0

    goto/16 :goto_e

    .line 187
    .restart local v28    # "isModelOrange":Z
    :cond_19
    const/16 v43, 0x0

    goto/16 :goto_f

    .line 188
    .restart local v43    # "isModelVFG":Z
    :cond_1a
    const/16 v39, 0x0

    goto/16 :goto_10

    .line 190
    .restart local v39    # "isModelTelstra":Z
    :cond_1b
    const/16 v44, 0x0

    goto/16 :goto_11

    .line 192
    .restart local v44    # "isModelVFN":Z
    :cond_1c
    const/16 v45, 0x0

    goto/16 :goto_12

    .line 194
    .restart local v45    # "isModelVFR":Z
    :cond_1d
    const/4 v15, 0x0

    goto/16 :goto_13

    .line 196
    .restart local v15    # "isModelEUR":Z
    :cond_1e
    const/16 v30, 0x0

    goto/16 :goto_14

    .line 198
    .restart local v30    # "isModelSEA":Z
    :cond_1f
    const/16 v16, 0x0

    goto/16 :goto_15

    .line 200
    .restart local v16    # "isModelEurDtGPG":Z
    :cond_20
    const/16 v38, 0x0

    goto/16 :goto_16

    .line 202
    .restart local v38    # "isModelTeleO2":Z
    :cond_21
    const/4 v9, 0x0

    goto/16 :goto_17

    .line 203
    .restart local v9    # "isModelAttWifi":Z
    :cond_22
    const/16 v40, 0x0

    goto/16 :goto_18

    .line 207
    .restart local v23    # "isModelJPN":Z
    .restart local v40    # "isModelTmoWifi":Z
    :cond_23
    const/4 v14, 0x0

    goto/16 :goto_19

    .line 208
    .restart local v14    # "isModelDCM":Z
    :cond_24
    const/16 v24, 0x0

    goto/16 :goto_1a

    .line 210
    .restart local v24    # "isModelKDI":Z
    :cond_25
    const/4 v5, 0x0

    goto/16 :goto_1b

    .line 211
    .restart local v5    # "isCHN":Z
    :cond_26
    const/4 v6, 0x0

    goto/16 :goto_1c

    .line 213
    .restart local v6    # "isHKTW":Z
    :cond_27
    const/16 v35, 0x0

    goto/16 :goto_1d

    .line 217
    .restart local v35    # "isModelTCE":Z
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->device:Ljava/lang/String;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->length()I

    move-result v50

    const/16 v51, 0x2

    move/from16 v0, v50

    move/from16 v1, v51

    if-lt v0, v1, :cond_29

    .line 218
    const-string v50, "WW"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->device:Ljava/lang/String;

    move-object/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->device:Ljava/lang/String;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Ljava/lang/String;->length()I

    move-result v52

    add-int/lit8 v52, v52, -0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->device:Ljava/lang/String;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Ljava/lang/String;->length()I

    move-result v53

    invoke-virtual/range {v51 .. v53}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .restart local v4    # "isACG":Z
    goto/16 :goto_1e

    .line 220
    .end local v4    # "isACG":Z
    :cond_29
    const/4 v4, 0x0

    .restart local v4    # "isACG":Z
    goto/16 :goto_1e

    .line 227
    .restart local v3    # "count":I
    :cond_2a
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "#7465625*638*#"

    if-nez v19, :cond_2b

    if-nez v46, :cond_2b

    if-nez v47, :cond_2b

    if-eqz v35, :cond_49

    :cond_2b
    const/16 v50, 0x1

    :goto_20
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "#7465625*77*#"

    if-nez v17, :cond_2c

    if-nez v46, :cond_2c

    if-nez v18, :cond_2c

    if-eqz v47, :cond_4a

    :cond_2c
    const/16 v50, 0x1

    :goto_21
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#34971539#"

    if-nez v19, :cond_2d

    if-nez v49, :cond_2d

    if-nez v33, :cond_2d

    if-nez v46, :cond_2d

    if-eqz v25, :cond_4b

    :cond_2d
    const/16 v50, 0x1

    :goto_22
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*147359*682*"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "#758353266#646#"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "#5487587#682#"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##10306#"

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*66723646#"

    if-nez v32, :cond_2e

    if-nez v26, :cond_2e

    if-eqz v27, :cond_4c

    :cond_2e
    const/16 v50, 0x1

    :goto_23
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#638732#"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##3282#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "SPR|BST|VMU|XAS"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##33284#"

    if-nez v33, :cond_2f

    if-nez v37, :cond_2f

    if-nez v13, :cond_2f

    if-nez v46, :cond_2f

    if-eqz v12, :cond_4d

    :cond_2f
    const/16 v50, 0x1

    :goto_24
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##786#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VMU|SPR|BST|XAS|TRF"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##4772579#"

    if-nez v33, :cond_30

    if-eqz v46, :cond_4e

    :cond_30
    const/16 v50, 0x1

    :goto_25
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##72786#"

    if-nez v33, :cond_31

    if-nez v37, :cond_31

    if-eqz v46, :cond_4f

    :cond_31
    const/16 v50, 0x1

    :goto_26
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##889#"

    if-nez v33, :cond_32

    if-nez v37, :cond_32

    if-eqz v46, :cond_50

    :cond_32
    const/16 v50, 0x1

    :goto_27
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#22745927"

    invoke-static/range {v37 .. v37}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#83786633"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "**6828378"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##7764726"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#9999#"

    if-nez v36, :cond_33

    if-eqz v47, :cond_51

    :cond_33
    const/16 v50, 0x1

    :goto_28
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#8888#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "TMB|MTR"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#2222#"

    if-nez v19, :cond_34

    if-eqz v49, :cond_52

    :cond_34
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VZW|USC|MTR|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|TMB"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v50

    if-nez v50, :cond_52

    const/16 v50, 0x1

    :goto_29
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#1*#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "ATT|TMB|AIO|TRF|MTR"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#7284#"

    invoke-static/range {v46 .. v46}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#2263#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "LAO|CLM|MXI|XME|MYA|XEV|XXV|MM1|SIN|STH|XSP|XSE|CAM|THL|THW|GLB|SMA|XTC|XTE|ATT|TMB|AIO|TRF|MTR|BMC|RWC|TLS|VTR|BWA|FMC|KDO|GLW|PCM|MTA|SPC|MCT|SOL|ESK|VMC|DTM|TMZ|DTR"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#8736364#"

    if-nez v42, :cond_35

    if-nez v9, :cond_35

    if-eqz v40, :cond_53

    :cond_35
    const/16 v50, 0x1

    :goto_2a
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#9090#"

    if-nez v19, :cond_36

    if-eqz v49, :cond_54

    :cond_36
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "KDI|DCM|SBM"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v50

    if-nez v50, :cond_54

    const/16 v50, 0x1

    :goto_2b
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#222875#"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#87222#*#*"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#638#"

    invoke-static/range {v41 .. v41}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#2627#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "ATT|AIO|TMB|MTR"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#0514#"

    invoke-static/range {v36 .. v36}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#33248#"

    invoke-static/range {v37 .. v37}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#232337#"

    if-nez v46, :cond_37

    if-eqz v47, :cond_55

    :cond_37
    const/16 v50, 0x1

    :goto_2c
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#0000#"

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#147235981#"

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#1472359810#"

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "3215987123580"

    if-nez v33, :cond_38

    if-nez v37, :cond_38

    if-eqz v46, :cond_56

    :cond_38
    const/16 v50, 0x1

    :goto_2d
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#86824#"

    if-nez v46, :cond_39

    if-eqz v13, :cond_57

    :cond_39
    const/16 v50, 0x1

    :goto_2e
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#1111#"

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VZW|USC|MTR|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|TMB"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v50

    if-nez v50, :cond_58

    const/16 v50, 0x1

    :goto_2f
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#2663#"

    if-nez v19, :cond_3a

    if-nez v49, :cond_3a

    if-nez v46, :cond_3a

    if-nez v8, :cond_3a

    if-nez v36, :cond_3a

    if-nez v33, :cond_3a

    if-nez v13, :cond_3a

    if-nez v47, :cond_3a

    if-eqz v25, :cond_59

    :cond_3a
    const/16 v50, 0x1

    :goto_30
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#22745927#"

    if-nez v47, :cond_3b

    if-eqz v37, :cond_5a

    :cond_3b
    const/16 v50, 0x1

    :goto_31
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#7412365*"

    if-nez v19, :cond_3c

    if-eqz v49, :cond_5b

    :cond_3c
    const/16 v50, 0x1

    :goto_32
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#7412365#"

    if-nez v33, :cond_3d

    if-nez v46, :cond_3d

    if-nez v47, :cond_3d

    if-eqz v25, :cond_5c

    :cond_3d
    const/16 v50, 0x1

    :goto_33
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*123456#"

    if-nez v32, :cond_3e

    if-nez v26, :cond_3e

    if-eqz v27, :cond_5d

    :cond_3e
    const/16 v50, 0x1

    :goto_34
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*1232580#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "SKT|SKC|SKO|LGT|LUC|LUO"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*147359#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "KTT|KTC|KTO|LGT|LUC|LUO"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "319712358"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#248659#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|KOR|ANY|KOO|TGY"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*##1235814863#"

    invoke-static/range {v26 .. v26}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#57739#"

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "1478912358"

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##700629#"

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#7415369#*#*"

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#197328640#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "ATT|TMB|AIO|TRF|MTR|BMC|RWC|TLS|VTR|BWA|FMC|KDO|GLW|PCM|MTA|SPC|MCT|SOL|ESK|VMC"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "47*68#13580"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#9900#"

    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VZW|USC|XAR|ANY|KOO"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v50

    if-nez v50, :cond_5e

    const/16 v50, 0x1

    :goto_35
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##3424#"

    if-nez v33, :cond_3f

    if-eqz v46, :cond_5f

    :cond_3f
    const/16 v50, 0x1

    :goto_36
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##564#"

    if-nez v33, :cond_40

    if-eqz v46, :cond_60

    :cond_40
    const/16 v50, 0x1

    :goto_37
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##737425#"

    if-nez v33, :cond_41

    if-eqz v46, :cond_61

    :cond_41
    const/16 v50, 0x1

    :goto_38
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##66236#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "SPR|BST|VMU|XAS"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##66264#"

    if-nez v33, :cond_42

    if-eqz v46, :cond_62

    :cond_42
    const/16 v50, 0x1

    :goto_39
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##873283#"

    if-nez v33, :cond_43

    if-eqz v46, :cond_63

    :cond_43
    const/16 v50, 0x1

    :goto_3a
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##6343#"

    if-nez v33, :cond_44

    if-eqz v46, :cond_64

    :cond_44
    const/16 v50, 0x1

    :goto_3b
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#24320#"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#243203855#"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "*#272*"

    move-object/from16 v0, v50

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->IMEI_NUMBER:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, v50

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v52, "#"

    move-object/from16 v0, v50

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    if-nez v10, :cond_45

    if-nez v29, :cond_45

    if-nez v31, :cond_45

    if-eqz v21, :cond_65

    :cond_45
    const/16 v50, 0x1

    :goto_3c
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "*#170582*"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/parser/KeystrBlockCheck;->IMEI_NUMBER:Ljava/lang/String;

    move-object/from16 v52, v0

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    const-string v52, "#"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#07#"

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#272837883#"

    if-nez v44, :cond_46

    if-eqz v45, :cond_66

    :cond_46
    const/16 v50, 0x1

    :goto_3d
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#4636#*#*"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "ATT|AIO"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##675#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "TRF"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#33248#"

    invoke-static/range {v37 .. v37}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##366633#"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "**7838"

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#8663366#*#*"

    invoke-static/range {v36 .. v36}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "##8378#"

    if-nez v12, :cond_47

    if-nez v33, :cond_47

    if-eqz v46, :cond_67

    :cond_47
    const/16 v50, 0x1

    :goto_3e
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##4357*"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##73738#"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##7764*"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##626*"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#0808#"

    if-nez v19, :cond_48

    if-eqz v49, :cond_68

    :cond_48
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VZW|USC|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|BST|SPR|VMU|XAS|KDI"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v50

    if-nez v50, :cond_68

    const/16 v50, 0x1

    :goto_3f
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#87976633#"

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#272*62826#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "DTM|TNL|MAX|TMZ|CRO|COS|TPL|TMS|TMH|MBM|TTR|DTR|TNP|COA|TRG|AMN|AMM|AMO|DDE|DHR|DNL|DPL|TMO|DBB|DBE|DCO|DHE"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#467#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "AIO|BBY|CRI|CSP|MTR|SPR|STA|TRF|USC|VMU|XAR|XAS|VIT|VIA|VDR|VDP"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#77467#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "ATT|TMB|FTM"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#2432546#*#*"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "VMU|ATT|CRI|SPR|TRF|AIO|BBY|BST|CSP|MTR|STA|XAR|XAS|USC"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#6278355872#"

    if-eqz v19, :cond_69

    if-nez v5, :cond_69

    if-nez v6, :cond_69

    const/16 v50, 0x1

    :goto_40
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#62783553424#"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#*#8255#*#*"

    invoke-static/range {v41 .. v41}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "##4382#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "SPR|BST|VMU|XAS"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#2255369273#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "CHM"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#83583872#"

    sget-object v52, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    const-string v53, "CHM"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    sget-object v50, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v51, "*#487634751964#"

    invoke-static/range {v38 .. v38}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    invoke-virtual/range {v50 .. v52}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    sget-object v51, Lcom/sec/android/app/parser/KeystrBlockCheck;->mAlwaysOpenKeystringList:Ljava/util/HashMap;

    const-string v52, "*#1234#"

    const-string v50, "VZW|TMB"

    sget-object v53, Lcom/sec/android/app/parser/KeystrBlockCheck;->mSalesCode:Ljava/lang/String;

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-nez v50, :cond_6a

    const/16 v50, 0x1

    :goto_41
    invoke-static/range {v50 .. v50}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v50

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    return-void

    .line 227
    :cond_49
    const/16 v50, 0x0

    goto/16 :goto_20

    .line 228
    :cond_4a
    const/16 v50, 0x0

    goto/16 :goto_21

    .line 229
    :cond_4b
    const/16 v50, 0x0

    goto/16 :goto_22

    .line 247
    :cond_4c
    const/16 v50, 0x0

    goto/16 :goto_23

    .line 252
    :cond_4d
    const/16 v50, 0x0

    goto/16 :goto_24

    .line 254
    :cond_4e
    const/16 v50, 0x0

    goto/16 :goto_25

    .line 255
    :cond_4f
    const/16 v50, 0x0

    goto/16 :goto_26

    .line 256
    :cond_50
    const/16 v50, 0x0

    goto/16 :goto_27

    .line 268
    :cond_51
    const/16 v50, 0x0

    goto/16 :goto_28

    .line 271
    :cond_52
    const/16 v50, 0x0

    goto/16 :goto_29

    .line 277
    :cond_53
    const/16 v50, 0x0

    goto/16 :goto_2a

    .line 279
    :cond_54
    const/16 v50, 0x0

    goto/16 :goto_2b

    .line 286
    :cond_55
    const/16 v50, 0x0

    goto/16 :goto_2c

    .line 290
    :cond_56
    const/16 v50, 0x0

    goto/16 :goto_2d

    .line 291
    :cond_57
    const/16 v50, 0x0

    goto/16 :goto_2e

    .line 296
    :cond_58
    const/16 v50, 0x0

    goto/16 :goto_2f

    .line 298
    :cond_59
    const/16 v50, 0x0

    goto/16 :goto_30

    .line 301
    :cond_5a
    const/16 v50, 0x0

    goto/16 :goto_31

    .line 307
    :cond_5b
    const/16 v50, 0x0

    goto/16 :goto_32

    .line 308
    :cond_5c
    const/16 v50, 0x0

    goto/16 :goto_33

    .line 309
    :cond_5d
    const/16 v50, 0x0

    goto/16 :goto_34

    .line 338
    :cond_5e
    const/16 v50, 0x0

    goto/16 :goto_35

    .line 340
    :cond_5f
    const/16 v50, 0x0

    goto/16 :goto_36

    .line 341
    :cond_60
    const/16 v50, 0x0

    goto/16 :goto_37

    .line 342
    :cond_61
    const/16 v50, 0x0

    goto/16 :goto_38

    .line 345
    :cond_62
    const/16 v50, 0x0

    goto/16 :goto_39

    .line 346
    :cond_63
    const/16 v50, 0x0

    goto/16 :goto_3a

    .line 348
    :cond_64
    const/16 v50, 0x0

    goto/16 :goto_3b

    .line 352
    :cond_65
    const/16 v50, 0x0

    goto/16 :goto_3c

    .line 358
    :cond_66
    const/16 v50, 0x0

    goto/16 :goto_3d

    .line 372
    :cond_67
    const/16 v50, 0x0

    goto/16 :goto_3e

    .line 378
    :cond_68
    const/16 v50, 0x0

    goto/16 :goto_3f

    .line 385
    :cond_69
    const/16 v50, 0x0

    goto/16 :goto_40

    .line 400
    :cond_6a
    const/16 v50, 0x0

    goto/16 :goto_41
.end method
