.class public Lcom/sec/android/app/parser/SecretCodeIME;
.super Landroid/app/Activity;
.source "SecretCodeIME.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;
    }
.end annotation


# static fields
.field private static local_clip:Ljava/lang/String;

.field private static mAutoRotateSetting:I

.field private static mCurrentTextSize:I


# instance fields
.field private bt_id:[I

.field private bundle:Landroid/os/Bundle;

.field private customObserver:Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

.field mDTMFToneEnabled:Z

.field private mDisplay:Landroid/widget/EditText;

.field private mListener:Lcom/sec/android/app/parser/EventListener;

.field private mLogic:Lcom/sec/android/app/parser/EventHandler;

.field private mPersist:Lcom/sec/android/app/parser/Persist;

.field private review:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/parser/SecretCodeIME;->local_clip:Ljava/lang/String;

    .line 80
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/parser/SecretCodeIME;->mCurrentTextSize:I

    .line 133
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/parser/SecretCodeIME;->mAutoRotateSetting:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 90
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bt_id:[I

    .line 251
    return-void

    .line 90
    nop

    :array_0
    .array-data 4
        0x7f060011
        0x7f060007
        0x7f060008
        0x7f060009
        0x7f06000a
        0x7f06000b
        0x7f06000c
        0x7f06000d
        0x7f06000e
        0x7f06000f
        0x7f060010
        0x7f060012
        0x7f060005
        0x7f060006
    .end array-data
.end method

.method private initControls()V
    .locals 5

    .prologue
    .line 318
    const v2, 0x7f060004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/parser/SecretCodeIME;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    .line 319
    new-instance v2, Lcom/sec/android/app/parser/EventListener;

    invoke-direct {v2}, Lcom/sec/android/app/parser/EventListener;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mListener:Lcom/sec/android/app/parser/EventListener;

    .line 320
    new-instance v2, Lcom/sec/android/app/parser/EventHandler;

    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-direct {v2, p0, v3, p0}, Lcom/sec/android/app/parser/EventHandler;-><init>(Landroid/content/Context;Landroid/widget/EditText;Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;)V

    iput-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->connectToParsingService()V

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mListener:Lcom/sec/android/app/parser/EventListener;

    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    iget-object v4, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/parser/EventListener;->setHandler(Lcom/sec/android/app/parser/EventHandler;Landroid/widget/EditText;)V

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 327
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bt_id:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bt_id:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/parser/SecretCodeIME;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 329
    .local v1, "vw":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 330
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 333
    const v2, 0x7f060006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/parser/SecretCodeIME;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mListener:Lcom/sec/android/app/parser/EventListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 327
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 337
    .end local v1    # "vw":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private removeShortcut()V
    .locals 4

    .prologue
    .line 235
    const-string v2, "SecretCodeIME"

    const-string v3, "removeShortcut()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v1, "shortcutIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string v2, "com.sec.android.app.parser.LauncherShortcuts"

    const-string v3, "ApiDemos Provided This Shortcut"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    const/high16 v2, 0x10200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 243
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 244
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    const-string v2, "android.intent.extra.shortcut.NAME"

    const/high16 v3, 0x7f050000

    invoke-virtual {p0, v3}, Lcom/sec/android/app/parser/SecretCodeIME;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v2, "duplicate"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 247
    const-string v2, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    invoke-virtual {p0, v0}, Lcom/sec/android/app/parser/SecretCodeIME;->sendBroadcast(Landroid/content/Intent;)V

    .line 249
    return-void
.end method

.method private setupShortcut()V
    .locals 5

    .prologue
    .line 209
    const-string v3, "SecretCodeIME"

    const-string v4, "setupShortcut()"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    .local v2, "shortcutIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v3, "com.sec.android.app.parser.LauncherShortcuts"

    const-string v4, "ApiDemos Provided This Shortcut"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const/high16 v3, 0x10200000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 217
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 218
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 219
    const-string v3, "android.intent.extra.shortcut.NAME"

    const/high16 v4, 0x7f050000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/parser/SecretCodeIME;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const v3, 0x7f02002a

    invoke-static {p0, v3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    .line 221
    .local v0, "iconResource":Landroid/os/Parcelable;
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 222
    const-string v3, "duplicate"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 223
    const-string v3, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v1}, Lcom/sec/android/app/parser/SecretCodeIME;->sendBroadcast(Landroid/content/Intent;)V

    .line 225
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/sec/android/app/parser/SecretCodeIME;->review:Landroid/view/View;

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mListener:Lcom/sec/android/app/parser/EventListener;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/parser/EventListener;->onClick(Landroid/view/View;)V

    .line 503
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 139
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    const-string v7, "SecretCodeIME"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const-string v7, "com.sec.android.app.parser.shortcuts"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "SUBACTION"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 145
    const-string v7, "SecretCodeIME"

    const-string v8, "onCreate() : SUBACTION"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const-string v7, "SUBACTION"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "REMOVE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 147
    const-string v7, "SecretCodeIME"

    const-string v8, "onCreate() : REMOVE"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->removeShortcut()V

    .line 153
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->finish()V

    .line 199
    :cond_1
    :goto_1
    return-void

    .line 149
    :cond_2
    const-string v7, "SUBACTION"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "CREATE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 150
    const-string v7, "SecretCodeIME"

    const-string v8, "onCreate() : CREATE"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->setupShortcut()V

    goto :goto_0

    .line 157
    :cond_3
    invoke-virtual {p0, v10}, Lcom/sec/android/app/parser/SecretCodeIME;->setRequestedOrientation(I)V

    .line 158
    sget-object v6, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    .line 159
    .local v6, "uri":Landroid/net/Uri;
    new-instance v7, Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

    invoke-direct {v7}, Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->customObserver:Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

    .line 160
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->customObserver:Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;->onChange(Z)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/parser/SecretCodeIME;->customObserver:Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

    invoke-virtual {v7, v6, v10, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 163
    new-instance v7, Lcom/sec/android/app/parser/Persist;

    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/Persist;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mPersist:Lcom/sec/android/app/parser/Persist;

    .line 164
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    .line 165
    const/high16 v7, 0x7f030000

    invoke-virtual {p0, v7}, Lcom/sec/android/app/parser/SecretCodeIME;->setContentView(I)V

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->initControls()V

    .line 168
    if-eqz p1, :cond_1

    .line 169
    const-string v7, "mStart"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 170
    .local v4, "start":I
    const-string v7, "mEnd"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 171
    .local v5, "stop":I
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    const-string v8, "mEnterEnd"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, v7, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    .line 173
    .local v3, "mEdit":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    const-string v8, "mCursorState"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/parser/EventHandler;->setmCursorState(Z)V

    .line 174
    const-string v7, "mCursorState"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 176
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    const-string v8, "String"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/parser/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 177
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 178
    .local v2, "len":I
    if-le v4, v2, :cond_4

    move v4, v2

    .line 179
    :cond_4
    if-le v5, v2, :cond_5

    move v5, v2

    .line 183
    :cond_5
    if-ne v4, v5, :cond_6

    .line 184
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v7}, Lcom/sec/android/app/parser/EventHandler;->goneCursor()V

    goto/16 :goto_1

    .line 186
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v8, "mStart"

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v8, "mEnd"

    invoke-virtual {v7, v8, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 188
    iget-object v7, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v8, "isSelecting"

    const-string v9, "isSelecting"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 473
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 477
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.app.parser.shortcuts"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 478
    const-string v2, "SecretCodeIME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDestroy() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->customObserver:Lcom/sec/android/app/parser/SecretCodeIME$CustomContentObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 482
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    if-eqz v2, :cond_1

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->disconnectParsingService()V

    .line 486
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 487
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 341
    const/16 v0, 0x52

    if-ne v0, p1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->closeOptionsMenu()V

    .line 345
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    .line 492
    .local v0, "mV":Landroid/widget/EditText;
    iget-object v1, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/EventHandler;->getmCursorState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 493
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 496
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/parser/EventHandler;->setLongClick(Z)V

    .line 497
    const/4 v1, 0x0

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mPersist:Lcom/sec/android/app/parser/Persist;

    invoke-virtual {v0}, Lcom/sec/android/app/parser/Persist;->save()V

    .line 425
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 426
    return-void
.end method

.method public onResume()V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 429
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "dtmf_tone"

    invoke-static {v6, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v7, :cond_3

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDTMFToneEnabled:Z

    .line 432
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v9, "mReview"

    invoke-virtual {v6, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 433
    .local v2, "mreview":I
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v9, "mStart"

    invoke-virtual {v6, v9, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 434
    .local v4, "start":I
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v9, "mEnd"

    invoke-virtual {v6, v9, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 435
    .local v5, "stop":I
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v9, "isSelecting"

    invoke-virtual {v6, v9, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 437
    .local v1, "isSelecting":Z
    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v9, "mOrientation"

    invoke-virtual {v6, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v9, :cond_0

    .line 439
    invoke-virtual {p0, v2}, Lcom/sec/android/app/parser/SecretCodeIME;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->review:Landroid/view/View;

    .line 441
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    invoke-virtual {v6}, Landroid/os/Bundle;->clear()V

    .line 444
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 445
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v9, "="

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 447
    .local v3, "recentStringArray":[Ljava/lang/String;
    array-length v6, v3

    if-le v6, v7, :cond_1

    .line 448
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    iget-object v9, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v11, v3, v8

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/parser/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 453
    .end local v3    # "recentStringArray":[Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    if-eqz v6, :cond_2

    .line 454
    if-eqz v1, :cond_4

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    .line 456
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6, v4, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 457
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v6, v0, v7}, Lcom/sec/android/app/parser/EventHandler;->onShift(Landroid/view/View;Z)V

    .line 465
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_2
    :goto_1
    return-void

    .end local v1    # "isSelecting":Z
    .end local v2    # "mreview":I
    .end local v4    # "start":I
    .end local v5    # "stop":I
    :cond_3
    move v6, v8

    .line 430
    goto/16 :goto_0

    .line 459
    .restart local v1    # "isSelecting":Z
    .restart local v2    # "mreview":I
    .restart local v4    # "start":I
    .restart local v5    # "stop":I
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    .line 460
    .restart local v0    # "et":Landroid/widget/EditText;
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v6, v0, v8}, Lcom/sec/android/app/parser/EventHandler;->onShift(Landroid/view/View;Z)V

    .line 461
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 462
    iget-object v6, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v6}, Lcom/sec/android/app/parser/EventHandler;->goneCursor()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 302
    const-string v0, "mEnterEnd"

    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->isCheckResult()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    const-string v0, "mCursorState"

    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->getmCursorState()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 306
    const-string v0, "isSelecting"

    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->isSelecting()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v3, "mReview"

    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->review:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->review:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->bundle:Landroid/os/Bundle;

    const-string v2, "mOrientation"

    invoke-virtual {p0}, Lcom/sec/android/app/parser/SecretCodeIME;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 310
    const-string v0, "mStart"

    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 311
    const-string v0, "mEnd"

    iget-object v2, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    iget-object v3, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 312
    const-string v0, "String"

    iget-object v1, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 314
    return-void

    :cond_0
    move v0, v1

    .line 308
    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 468
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 469
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 374
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 389
    :goto_0
    return v2

    .line 377
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/parser/EventHandler;->delst:Z

    .line 378
    iput-object p1, p0, Lcom/sec/android/app/parser/SecretCodeIME;->review:Landroid/view/View;

    goto :goto_0

    .line 381
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mListener:Lcom/sec/android/app/parser/EventListener;

    invoke-virtual {v0}, Lcom/sec/android/app/parser/EventListener;->removeHandlerEvent()V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/parser/SecretCodeIME;->mLogic:Lcom/sec/android/app/parser/EventHandler;

    iput-boolean v2, v0, Lcom/sec/android/app/parser/EventHandler;->delst:Z

    goto :goto_0

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
