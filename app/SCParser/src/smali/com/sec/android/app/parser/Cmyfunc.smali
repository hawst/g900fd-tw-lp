.class Lcom/sec/android/app/parser/Cmyfunc;
.super Ljava/lang/Object;
.source "Logic.java"


# static fields
.field private static mOrigin:Ljava/lang/String;

.field static final mStrTokens:[Ljava/lang/String;

.field private static mTrans:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "("

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ")"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "abs"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "^"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sin"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cos"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "tan"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ln"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\u221a"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "%"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "!"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "log"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\u00d7"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\u00f7"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "-"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/parser/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    .line 161
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/parser/Cmyfunc;->mTrans:Ljava/lang/String;

    .line 175
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/parser/Cmyfunc;->mOrigin:Ljava/lang/String;

    return-void
.end method

.method public static clearmOrigin()V
    .locals 1

    .prologue
    .line 186
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/parser/Cmyfunc;->mOrigin:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public static getmOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/sec/android/app/parser/Cmyfunc;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method static isDigit(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 138
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x65

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3c0

    if-eq p0, v0, :cond_1

    const/16 v0, 0x45

    if-eq p0, v0, :cond_1

    const/16 v0, 0x2c

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOnlyDigit(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 143
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x2c

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOpByTwo(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 116
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd7

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x25

    if-eq p0, v0, :cond_0

    const/16 v0, 0xad

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static whereLastToken(Ljava/lang/String;Z)I
    .locals 4
    .param p0, "strExp"    # Ljava/lang/String;
    .param p1, "bContainToken"    # Z

    .prologue
    .line 63
    const/4 v1, -0x1

    .line 66
    .local v1, "nFind":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/parser/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 67
    sget-object v3, Lcom/sec/android/app/parser/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 69
    .local v2, "nTmp":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 70
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 71
    sget-object v3, Lcom/sec/android/app/parser/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v2, v3

    .line 74
    :cond_0
    if-ge v1, v2, :cond_1

    .line 75
    move v1, v2

    .line 66
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    .end local v2    # "nTmp":I
    :cond_2
    return v1
.end method
