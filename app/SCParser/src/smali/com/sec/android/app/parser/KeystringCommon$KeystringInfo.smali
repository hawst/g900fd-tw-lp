.class Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;
.super Ljava/lang/Object;
.source "KeystringCommon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/parser/KeystringCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeystringInfo"
.end annotation


# instance fields
.field private mCategory:I

.field private mInput:Ljava/lang/String;

.field private mIsBlocking:Z

.field private mIsFactorymode:Z

.field private mIsGoogleKeystring:Z

.field private mOutput:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1352
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mPackageName:Ljava/lang/String;

    .line 1356
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z

    .line 1358
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    .line 1438
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;

    .line 1439
    const/4 v0, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    .line 1440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android_secret_code://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;

    .line 1441
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "output"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1352
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mPackageName:Ljava/lang/String;

    .line 1356
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z

    .line 1358
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    .line 1426
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;

    .line 1428
    if-eqz p2, :cond_0

    .line 1429
    iput-object p2, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    .line 1434
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android_secret_code://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;

    .line 1435
    return-void

    .line 1431
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "output"    # Ljava/lang/String;
    .param p3, "category"    # I

    .prologue
    const/4 v1, 0x0

    .line 1412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1352
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mPackageName:Ljava/lang/String;

    .line 1356
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z

    .line 1358
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    .line 1413
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;

    .line 1415
    if-eqz p2, :cond_0

    .line 1416
    iput-object p2, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    .line 1421
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android_secret_code://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;

    .line 1422
    iput p3, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mCategory:I

    .line 1423
    return-void

    .line 1418
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "output"    # Ljava/lang/String;
    .param p3, "isGoogleKeystring"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1352
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mPackageName:Ljava/lang/String;

    .line 1356
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z

    .line 1358
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    .line 1445
    iput-boolean p3, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    .line 1446
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;

    .line 1448
    if-eqz p2, :cond_0

    .line 1449
    iput-object p2, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    .line 1454
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android_secret_code://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;

    .line 1455
    return-void

    .line 1451
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mOutput:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .prologue
    .line 1350
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .prologue
    .line 1350
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .prologue
    .line 1350
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;

    return-object v0
.end method
