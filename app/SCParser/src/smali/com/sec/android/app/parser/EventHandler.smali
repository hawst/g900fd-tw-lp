.class public Lcom/sec/android/app/parser/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;
    }
.end annotation


# static fields
.field public static final IS_DEBUG:Z

.field private static LENGTH_FACTORY_CHECK:I


# instance fields
.field private checkChar:[Ljava/lang/String;

.field public delst:Z

.field public errorCursor:I

.field private exception:Lcom/sec/android/app/parser/SyntaxException;

.field private frontText:Ljava/lang/String;

.field public handler:Landroid/os/Handler;

.field private isLongClick:Z

.field private isSelecting:Z

.field private mContext:Landroid/content/Context;

.field private mCursorState:Z

.field private mDisplay:Landroid/widget/EditText;

.field public mEnterEnd:Z

.field public mHandler:Landroid/os/Handler;

.field private mIsError:Z

.field private mIsOpenedSignParenthesis:Z

.field private mMessenger:Landroid/os/Messenger;

.field private mOnSecretActionListener:Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;

.field private mParserServiceConnection:Landroid/content/ServiceConnection;

.field public mPaste:Z

.field private mReceiveMessenger:Landroid/os/Messenger;

.field private mType:Landroid/graphics/Typeface;

.field private sb:Ljava/lang/StringBuilder;

.field watcher1:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    const-string v0, "ro.product_ship"

    const-string v1, "FALSE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TRUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/parser/EventHandler;->IS_DEBUG:Z

    .line 87
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/parser/EventHandler;->LENGTH_FACTORY_CHECK:I

    return-void

    .line 65
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Landroid/widget/EditText;Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "display"    # Landroid/widget/EditText;
    .param p3, "listener"    # Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/sec/android/app/parser/SyntaxException;

    invoke-direct {v0}, Lcom/sec/android/app/parser/SyntaxException;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->exception:Lcom/sec/android/app/parser/SyntaxException;

    .line 71
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->mIsError:Z

    .line 72
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->mCursorState:Z

    .line 73
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->isSelecting:Z

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->isLongClick:Z

    .line 75
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    .line 76
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->mPaste:Z

    .line 77
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->delst:Z

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->frontText:Ljava/lang/String;

    .line 79
    iput v3, p0, Lcom/sec/android/app/parser/EventHandler;->errorCursor:I

    .line 81
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sin("

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "cos("

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "tan("

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ln("

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "abs("

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "log("

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "^("

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\u221a("

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->checkChar:[Ljava/lang/String;

    .line 85
    iput-boolean v3, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;

    .line 127
    new-instance v0, Lcom/sec/android/app/parser/EventHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/EventHandler$1;-><init>(Lcom/sec/android/app/parser/EventHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mParserServiceConnection:Landroid/content/ServiceConnection;

    .line 161
    new-instance v0, Lcom/sec/android/app/parser/EventHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/EventHandler$2;-><init>(Lcom/sec/android/app/parser/EventHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mHandler:Landroid/os/Handler;

    .line 173
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mReceiveMessenger:Landroid/os/Messenger;

    .line 175
    new-instance v0, Lcom/sec/android/app/parser/EventHandler$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/EventHandler$3;-><init>(Lcom/sec/android/app/parser/EventHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->watcher1:Landroid/text/TextWatcher;

    .line 187
    new-instance v0, Lcom/sec/android/app/parser/EventHandler$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/parser/EventHandler$4;-><init>(Lcom/sec/android/app/parser/EventHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->handler:Landroid/os/Handler;

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->sb:Ljava/lang/StringBuilder;

    .line 92
    iput-object p1, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    .line 94
    iput-object p3, p0, Lcom/sec/android/app/parser/EventHandler;->mOnSecretActionListener:Lcom/sec/android/app/parser/EventHandler$OnSecretActionListener;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->watcher1:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/parser/EventHandler;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/EventHandler;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method private clearSB()V
    .locals 3

    .prologue
    .line 250
    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 252
    .local v0, "len":I
    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->sb:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private handleTestmodeSecretCode(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "input"    # Ljava/lang/String;

    .prologue
    .line 567
    sget-boolean v0, Lcom/sec/android/app/parser/EventHandler;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 568
    const-string v0, "FactoryModeEventHandler"

    const-string v1, "handleTestmodeSecretCode()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mParserServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_3

    .line 572
    sget-boolean v0, Lcom/sec/android/app/parser/EventHandler;->IS_DEBUG:Z

    if-eqz v0, :cond_1

    .line 573
    const-string v0, "FactoryModeEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* handleTestmodeSecretCode * : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/android/app/parser/EventHandler;->sendMessage(Ljava/lang/String;)V

    .line 582
    :cond_2
    :goto_0
    return-void

    .line 578
    :cond_3
    sget-boolean v0, Lcom/sec/android/app/parser/EventHandler;->IS_DEBUG:Z

    if-eqz v0, :cond_2

    .line 579
    const-string v0, "FactoryModeEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* can not send message * : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertComma(Ljava/lang/String;II)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 428
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 429
    .local v0, "s":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0, p2, p3}, Lcom/sec/android/app/parser/EventHandler;->insertComma(Ljava/lang/StringBuilder;II)V

    .line 430
    return-void
.end method

.method private insertComma(Ljava/lang/StringBuilder;II)V
    .locals 4
    .param p1, "text"    # Ljava/lang/StringBuilder;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextBuilder()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 414
    .local v2, "s":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    .local v1, "output":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v0

    .line 418
    .local v0, "input":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 424
    :cond_0
    return-void
.end method

.method private sendMessage(Ljava/lang/String;)V
    .locals 6
    .param p1, "keystring"    # Ljava/lang/String;

    .prologue
    .line 141
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 142
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "KEYSTRING"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 144
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mReceiveMessenger:Landroid/os/Messenger;

    iput-object v3, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 148
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_1

    .line 149
    sget-boolean v3, Lcom/sec/android/app/parser/EventHandler;->IS_DEBUG:Z

    if-eqz v3, :cond_0

    .line 150
    const-string v3, "FactoryModeEventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendMessage() KEY_KEYSTRING = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public CheckInput()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 586
    const/4 v0, 0x0

    .line 587
    .local v0, "error":I
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    .line 588
    .local v2, "input":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextBuilder()Ljava/lang/StringBuilder;

    move-result-object v4

    .line 589
    .local v4, "s":Ljava/lang/StringBuilder;
    const-string v3, ""

    .line 590
    .local v3, "mResult":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/parser/CExpression;

    invoke-direct {v1}, Lcom/sec/android/app/parser/CExpression;-><init>()V

    .line 591
    .local v1, "exp":Lcom/sec/android/app/parser/CExpression;
    iput-boolean v6, p0, Lcom/sec/android/app/parser/EventHandler;->mPaste:Z

    .line 593
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_0

    .line 600
    :goto_0
    return-void

    .line 597
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5, v2}, Lcom/sec/android/app/parser/EventHandler;->handleTestmodeSecretCode(Landroid/content/Context;Ljava/lang/String;)V

    .line 598
    iput-boolean v6, p0, Lcom/sec/android/app/parser/EventHandler;->mIsError:Z

    .line 599
    iput-boolean v6, p0, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    goto :goto_0
.end method

.method public changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 369
    .local v0, "txt":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/parser/EventHandler;->changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    return-object v1
.end method

.method public changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 11
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    const/high16 v10, -0x1000000

    const/16 v9, 0x21

    .line 316
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 317
    .local v5, "output":Landroid/text/SpannableStringBuilder;
    const v1, 0x3b9ac9ff

    .line 318
    .local v1, "count":I
    const/4 v4, 0x0

    .line 320
    .local v4, "i":I
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 321
    const-string v7, ""

    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 363
    .end local v5    # "output":Landroid/text/SpannableStringBuilder;
    :cond_0
    return-object v5

    .line 334
    .local v0, "ch":C
    .restart local v5    # "output":Landroid/text/SpannableStringBuilder;
    :sswitch_0
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v7, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 336
    move v1, v4

    .line 338
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 339
    .local v3, "enter":I
    const/4 v7, -0x1

    if-eq v3, v7, :cond_5

    move v2, v3

    .line 340
    .local v2, "en":I
    :goto_0
    const/16 v6, 0x30

    .line 341
    .local v6, "size":I
    new-instance v7, Landroid/text/style/AbsoluteSizeSpan;

    const/4 v8, 0x0

    invoke-direct {v7, v6, v8}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    invoke-virtual {v5, v7, v4, v2, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 360
    .end local v2    # "en":I
    .end local v3    # "enter":I
    .end local v6    # "size":I
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 324
    .end local v0    # "ch":C
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 325
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 328
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/sec/android/app/parser/Cmyfunc;->isOnlyDigit(C)Z

    move-result v7

    if-eqz v7, :cond_2

    const/16 v7, 0x2c

    if-ne v0, v7, :cond_3

    .line 329
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mType:Landroid/graphics/Typeface;

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v7, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 332
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 351
    if-le v4, v1, :cond_4

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 352
    new-instance v7, Landroid/text/style/ScaleXSpan;

    const v8, 0x3f59999a    # 0.85f

    invoke-direct {v7, v8}, Landroid/text/style/ScaleXSpan;-><init>(F)V

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v7, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 355
    :cond_4
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v7, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 339
    .restart local v3    # "enter":I
    :cond_5
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    goto :goto_0

    .line 345
    .end local v3    # "enter":I
    :sswitch_1
    const v1, 0x3b9ac9ff

    .line 346
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v7, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 332
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x3d -> :sswitch_0
    .end sparse-switch
.end method

.method public closeSignParenthesis()V
    .locals 2

    .prologue
    .line 1096
    iget-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/parser/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 1100
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1101
    return-void
.end method

.method public connectToParsingService()V
    .locals 5

    .prologue
    .line 106
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/parser/ParseService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 109
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 110
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mParserServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v2, "FactoryModeEventHandler"

    const-string v3, "SecurityException()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method public disconnectParsingService()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mParserServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->mParserServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 123
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 101
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 102
    return-void
.end method

.method public getCursor(Landroid/widget/EditText;Z)I
    .locals 3
    .param p1, "v"    # Landroid/widget/EditText;
    .param p2, "distribution"    # Z

    .prologue
    .line 380
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 381
    .local v1, "x":I
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 382
    .local v2, "y":I
    if-eqz p2, :cond_0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 383
    .local v0, "cursor":I
    :goto_0
    return v0

    .line 382
    .end local v0    # "cursor":I
    :cond_0
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextBuilder()Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/sec/android/app/parser/EventHandler;->clearSB()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->sb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public getTextLength()I
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    return v0
.end method

.method public getmCursorState()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->mCursorState:Z

    return v0
.end method

.method public goneCursor()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 215
    invoke-virtual {p0, v1}, Lcom/sec/android/app/parser/EventHandler;->setmCursorState(Z)V

    .line 216
    return-void
.end method

.method public insert(Ljava/lang/StringBuilder;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v9, 0x0

    .line 388
    const/4 v4, 0x0

    .line 389
    .local v4, "frontchar":C
    const/4 v6, 0x0

    .line 390
    .local v6, "substart":I
    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 391
    .local v0, "checkchar":C
    iput-boolean v9, p0, Lcom/sec/android/app/parser/EventHandler;->mIsError:Z

    .line 392
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v5

    .line 393
    .local v5, "startCursor":I
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v3

    .line 396
    .local v3, "endCursor":I
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->isSelecting()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextLength()I

    move-result v7

    if-ne v3, v7, :cond_0

    if-eq v5, v3, :cond_0

    .line 397
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7, v5, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 403
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 404
    .local v1, "currentText":Landroid/text/Editable;
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v2

    .line 405
    .local v2, "displaycursor":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/parser/EventHandler;->changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-interface {v1, v2, v7}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 407
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/app/parser/EventHandler;->getCursor(Landroid/widget/EditText;Z)I

    move-result v2

    .line 408
    invoke-direct {p0, p1, v2, v2}, Lcom/sec/android/app/parser/EventHandler;->insertComma(Ljava/lang/StringBuilder;II)V

    .line 409
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 410
    return-void

    .line 399
    .end local v1    # "currentText":Landroid/text/Editable;
    .end local v2    # "displaycursor":I
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method public isCheckResult()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 603
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextBuilder()Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 605
    .local v1, "equal":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v3, v1, :cond_1

    .line 610
    :cond_0
    :goto_0
    return v2

    .line 609
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v3, v4}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    .line 610
    .local v0, "ch":C
    invoke-static {v0}, Lcom/sec/android/app/parser/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isOpenedSignParenthesis()Z
    .locals 1

    .prologue
    .line 1104
    iget-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    return v0
.end method

.method public isSelecting()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->isSelecting:Z

    return v0
.end method

.method public onBackspace()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/16 v11, 0xad

    const/16 v10, 0x28

    const/4 v8, -0x1

    const/4 v9, 0x0

    .line 1014
    iget-boolean v6, p0, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    if-eqz v6, :cond_0

    .line 1015
    iput-boolean v9, p0, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    .line 1019
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->onClearOrigin()V

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v1

    .line 1021
    .local v1, "displayText":Ljava/lang/String;
    const-string v4, ""

    .line 1022
    .local v4, "match":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextLength()I

    move-result v3

    .line 1023
    .local v3, "len":I
    const/4 v2, -0x1

    .line 1035
    .local v2, "index":I
    invoke-static {v1, v9}, Lcom/sec/android/app/parser/Cmyfunc;->whereLastToken(Ljava/lang/String;Z)I

    move-result v2

    .line 1036
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 1042
    .local v0, "displayCursor":I
    if-eq v2, v8, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v11, :cond_1

    .line 1043
    if-lez v2, :cond_1

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_1

    .line 1044
    add-int/lit8 v2, v2, -0x1

    .line 1045
    iput-boolean v9, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1049
    :cond_1
    if-eq v2, v8, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x29

    if-ne v6, v7, :cond_3

    .line 1050
    invoke-virtual {v1, v10, v2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v5

    .line 1052
    .local v5, "nOpenPos":I
    if-eq v5, v8, :cond_2

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v11, :cond_2

    .line 1057
    iput-boolean v12, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1060
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6, v2, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1061
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 1062
    const-string v6, ""

    invoke-direct {p0, v6, v0, v0}, Lcom/sec/android/app/parser/EventHandler;->insertComma(Ljava/lang/String;II)V

    .line 1081
    .end local v5    # "nOpenPos":I
    :goto_0
    return-void

    .line 1066
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    add-int/lit8 v6, v3, -0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/parser/Cmyfunc;->isDigit(C)Z

    move-result v6

    if-ne v6, v12, :cond_4

    .line 1067
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    new-instance v7, Landroid/view/KeyEvent;

    const/16 v8, 0x43

    invoke-direct {v7, v9, v8}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 1079
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 1080
    const-string v6, ""

    invoke-direct {p0, v6, v0, v0}, Lcom/sec/android/app/parser/EventHandler;->insertComma(Ljava/lang/String;II)V

    goto :goto_0

    .line 1068
    :cond_4
    if-eq v2, v8, :cond_6

    .line 1069
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_5

    add-int/lit8 v6, v2, 0x1

    if-ge v6, v3, :cond_5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v11, :cond_5

    .line 1071
    iput-boolean v9, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1074
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6, v2, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_1

    .line 1076
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    new-instance v7, Landroid/view/KeyEvent;

    const/16 v8, 0x43

    invoke-direct {v7, v9, v8}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_1
.end method

.method public onClear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler;->mDisplay:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 616
    iput-boolean v2, p0, Lcom/sec/android/app/parser/EventHandler;->mIsError:Z

    .line 617
    iput-boolean v2, p0, Lcom/sec/android/app/parser/EventHandler;->mEnterEnd:Z

    .line 618
    invoke-static {}, Lcom/sec/android/app/parser/Cmyfunc;->clearmOrigin()V

    .line 619
    iput-boolean v2, p0, Lcom/sec/android/app/parser/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 620
    return-void
.end method

.method public onClearOrigin()V
    .locals 5

    .prologue
    .line 989
    invoke-static {}, Lcom/sec/android/app/parser/Cmyfunc;->getmOrigin()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 991
    .local v2, "len":I
    if-nez v2, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return-void

    .line 995
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/parser/Cmyfunc;->getmOrigin()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 997
    .local v1, "index":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 1001
    add-int v3, v2, v1

    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getTextLength()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 1002
    invoke-virtual {p0}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    add-int v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1004
    .local v0, "ch":C
    invoke-static {v0}, Lcom/sec/android/app/parser/Cmyfunc;->isOpByTwo(C)Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x21

    if-eq v0, v3, :cond_0

    .line 1009
    .end local v0    # "ch":C
    :cond_2
    invoke-static {}, Lcom/sec/android/app/parser/Cmyfunc;->clearmOrigin()V

    goto :goto_0
.end method

.method public onShift(Landroid/view/View;Z)V
    .locals 4
    .param p1, "mV"    # Landroid/view/View;
    .param p2, "onOff"    # Z

    .prologue
    const/16 v3, 0x3b

    const/4 v2, 0x0

    .line 279
    iget-boolean v0, p0, Lcom/sec/android/app/parser/EventHandler;->isSelecting:Z

    if-ne p2, v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 283
    :cond_0
    if-eqz p2, :cond_1

    .line 284
    new-instance v0, Landroid/view/KeyEvent;

    invoke-direct {v0, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 293
    :goto_1
    invoke-virtual {p0, p2}, Lcom/sec/android/app/parser/EventHandler;->setSelecting(Z)V

    goto :goto_0

    .line 286
    :cond_1
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 290
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1
.end method

.method public setFrontText(Ljava/lang/String;)V
    .locals 0
    .param p1, "frontText"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/app/parser/EventHandler;->frontText:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setLongClick(Z)V
    .locals 0
    .param p1, "isLongClick"    # Z

    .prologue
    .line 231
    iput-boolean p1, p0, Lcom/sec/android/app/parser/EventHandler;->isLongClick:Z

    .line 232
    return-void
.end method

.method public setSelecting(Z)V
    .locals 0
    .param p1, "isSelecting"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/sec/android/app/parser/EventHandler;->isSelecting:Z

    .line 220
    return-void
.end method

.method public setmCursorState(Z)V
    .locals 0
    .param p1, "mCursorState"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/sec/android/app/parser/EventHandler;->mCursorState:Z

    .line 206
    return-void
.end method
