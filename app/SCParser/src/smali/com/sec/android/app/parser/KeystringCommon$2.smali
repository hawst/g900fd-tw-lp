.class Lcom/sec/android/app/parser/KeystringCommon$2;
.super Landroid/content/BroadcastReceiver;
.source "KeystringCommon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/parser/KeystringCommon;->registReceiverForSpecialKeystring_TFN()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/parser/KeystringCommon;


# direct methods
.method constructor <init>(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0

    .prologue
    .line 1280
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon$2;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1282
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1284
    .local v0, "action":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 1285
    const-string v1, "KeystringCommon"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTFNReceiver.onReceive, action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    :cond_0
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_ENABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1289
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$2;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringTFN()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1400(Lcom/sec/android/app/parser/KeystringCommon;)V

    .line 1293
    :cond_1
    :goto_0
    return-void

    .line 1290
    :cond_2
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_DISABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1291
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon$2;->this$0:Lcom/sec/android/app/parser/KeystringCommon;

    # invokes: Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringTFN()V
    invoke-static {v1}, Lcom/sec/android/app/parser/KeystringCommon;->access$1500(Lcom/sec/android/app/parser/KeystringCommon;)V

    goto :goto_0
.end method
