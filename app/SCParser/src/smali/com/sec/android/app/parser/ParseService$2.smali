.class Lcom/sec/android/app/parser/ParseService$2;
.super Landroid/os/Handler;
.source "ParseService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/parser/ParseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/parser/ParseService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/parser/ParseService;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/parser/ParseService$2;->this$0:Lcom/sec/android/app/parser/ParseService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 78
    sget-boolean v5, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v5, :cond_0

    .line 79
    const-string v5, "ParseService"

    const-string v6, "Get Keystring via bind service"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 83
    .local v1, "data":Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 84
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "KEYSTRING"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "keystring":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/parser/ParseService$2;->this$0:Lcom/sec/android/app/parser/ParseService;

    const-string v6, "USERDATA"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    # setter for: Lcom/sec/android/app/parser/ParseService;->mIsUserData:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/parser/ParseService;->access$202(Lcom/sec/android/app/parser/ParseService;Z)Z

    .line 89
    if-eqz v4, :cond_1

    .line 90
    iget-object v5, p0, Lcom/sec/android/app/parser/ParseService$2;->this$0:Lcom/sec/android/app/parser/ParseService;

    # invokes: Lcom/sec/android/app/parser/ParseService;->process(Ljava/lang/String;)Z
    invoke-static {v5, v4}, Lcom/sec/android/app/parser/ParseService;->access$300(Lcom/sec/android/app/parser/ParseService;Ljava/lang/String;)Z

    move-result v3

    .line 92
    .local v3, "isKeyString":Z
    if-eqz v3, :cond_1

    .line 94
    :try_start_0
    iget-object v5, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .end local v3    # "isKeyString":Z
    :cond_1
    :goto_0
    return-void

    .line 95
    .restart local v3    # "isKeyString":Z
    :catch_0
    move-exception v2

    .line 96
    .local v2, "e":Landroid/os/RemoteException;
    sget-boolean v5, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v5, :cond_1

    .line 97
    const-string v5, "ParseService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RemoteException"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
