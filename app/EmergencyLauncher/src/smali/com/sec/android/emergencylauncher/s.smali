.class Lcom/sec/android/emergencylauncher/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    const/high16 v4, 0x10000000

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "favAppClickListener - onItemClick() : position = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iput p3, v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->j(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.mms.ui.ConversationComposer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->k(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0

    :cond_1
    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.emergencylauncher.LED light"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->h(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0

    :cond_4
    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.samsung.contacts.activities.EmergencyPeopleActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "favAppClickListener - onItemClick() : launching "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "favAppClickListener - onItemClick() : launching "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/s;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0
.end method
