.class public Lcom/sec/android/emergencylauncher/view/EMGridView;
.super Landroid/widget/GridView;


# static fields
.field private static final b:Z


# instance fields
.field a:Lcom/sec/android/emergencylauncher/view/f;

.field private c:Lcom/sec/android/emergencylauncher/AppController;

.field private d:Landroid/content/Context;

.field private e:Landroid/os/Handler;

.field private f:Lcom/sec/android/emergencylauncher/b/a;

.field private g:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/view/EMGridView;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->e:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/emergencylauncher/view/d;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->e:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/emergencylauncher/view/d;-><init>(Lcom/sec/android/emergencylauncher/view/EMGridView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->g:Landroid/database/ContentObserver;

    const-string v0, "EmergencyLauncher.EMGridView"

    const-string v1, "EMGridView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->d:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    new-instance v0, Lcom/sec/android/emergencylauncher/view/f;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/view/f;-><init>(Lcom/sec/android/emergencylauncher/view/EMGridView;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->a:Lcom/sec/android/emergencylauncher/view/f;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->a:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/b/a;->a(Landroid/content/Context;)Lcom/sec/android/emergencylauncher/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->f:Lcom/sec/android/emergencylauncher/b/a;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/emergencylauncher/b/a;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->g:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    new-instance v1, Lcom/sec/android/emergencylauncher/view/a;

    invoke-direct {v1, p0}, Lcom/sec/android/emergencylauncher/view/a;-><init>(Lcom/sec/android/emergencylauncher/view/EMGridView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/AppController;->a(Lcom/sec/android/emergencylauncher/e;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/view/EMGridView;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/b/a;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->f:Lcom/sec/android/emergencylauncher/b/a;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->d:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method a(Z)V
    .locals 5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->q:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->size()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->n:I

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    if-le v1, v3, :cond_4

    add-int/lit8 v0, v4, -0x1

    move v2, v1

    move v1, v0

    :goto_2
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->n:I

    if-le v1, v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    :cond_1
    if-ne v2, v3, :cond_6

    move v1, v2

    :goto_3
    if-gtz v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->m:I

    :cond_2
    if-eqz p0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->a:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    :cond_3
    if-eqz p0, :cond_4

    new-instance v0, Lcom/sec/android/emergencylauncher/view/c;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/view/c;-><init>(Lcom/sec/android/emergencylauncher/view/EMGridView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->post(Ljava/lang/Runnable;)Z

    :cond_4
    const-string v0, "EmergencyLauncher.EMGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkOtherAppsList() : otherAppsListSize : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mDefaultApps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/EMGridView;->c:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " emptyCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void

    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
