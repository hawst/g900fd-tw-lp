.class Lcom/sec/android/emergencylauncher/view/i;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:I

.field e:I

.field f:Ljava/lang/String;

.field g:I

.field h:I

.field final synthetic i:Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/view/i;->i:Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/i;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;Lcom/sec/android/emergencylauncher/view/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/view/i;-><init>(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/high16 v0, 0x42c80000    # 100.0f

    const-string v3, "level"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    const-string v3, "scale"

    const/16 v4, 0x64

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, p0, Lcom/sec/android/emergencylauncher/view/i;->a:I

    if-eq v3, v0, :cond_6

    move v5, v1

    :goto_0
    if-eqz v5, :cond_0

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/i;->a:I

    :cond_0
    iget v0, p0, Lcom/sec/android/emergencylauncher/view/i;->b:I

    const-string v3, "plugged"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-eq v0, v3, :cond_7

    move v4, v1

    :goto_1
    if-eqz v4, :cond_1

    const-string v0, "plugged"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/i;->b:I

    :cond_1
    iget v0, p0, Lcom/sec/android/emergencylauncher/view/i;->b:I

    if-eqz v0, :cond_8

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/android/emergencylauncher/view/i;->c:Z

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/i;->d:I

    const-string v3, "health"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-eq v0, v3, :cond_9

    move v3, v1

    :goto_3
    if-eqz v3, :cond_2

    const-string v0, "health"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/i;->d:I

    :cond_2
    iget v0, p0, Lcom/sec/android/emergencylauncher/view/i;->e:I

    const-string v6, "status"

    invoke-virtual {p2, v6, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    if-eq v0, v6, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_3

    const-string v6, "status"

    invoke-virtual {p2, v6, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencylauncher/view/i;->e:I

    :cond_3
    const-string v6, "technology"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/emergencylauncher/view/i;->f:Ljava/lang/String;

    const-string v6, "voltage"

    invoke-virtual {p2, v6, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencylauncher/view/i;->g:I

    const-string v6, "temperature"

    invoke-virtual {p2, v6, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencylauncher/view/i;->h:I

    const-string v2, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_BATTERY_CHANGED : level:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/emergencylauncher/view/i;->a:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " status:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/emergencylauncher/view/i;->e:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " health:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/emergencylauncher/view/i;->d:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_4

    if-nez v4, :cond_4

    if-nez v3, :cond_4

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/i;->i:Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;

    const v2, 0x7f0a0036

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string v0, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    const-string v2, "setContentDescription"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/i;->i:Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/i;->i:Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    return-void

    :cond_6
    move v5, v2

    goto/16 :goto_0

    :cond_7
    move v4, v2

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_2

    :cond_9
    move v3, v2

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_4
.end method
