.class public Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;
.super Landroid/widget/ImageView;


# instance fields
.field a:[I

.field b:Landroid/graphics/Paint;

.field c:Landroid/graphics/Paint;

.field d:Landroid/graphics/Paint;

.field e:Landroid/graphics/Paint;

.field f:Landroid/graphics/Paint;

.field g:Landroid/graphics/Paint;

.field h:I

.field i:Lcom/sec/android/emergencylauncher/view/i;

.field private j:F

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private final o:[F

.field private final p:Landroid/graphics/Path;

.field private final q:Landroid/graphics/RectF;

.field private final r:Landroid/graphics/RectF;

.field private final s:Landroid/graphics/RectF;

.field private final t:Landroid/graphics/RectF;

.field private u:Z

.field private v:F

.field private w:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12

    const/4 v1, 0x0

    const/4 v11, -0x1

    const/4 v10, 0x0

    const/4 v5, 0x2

    const/4 v9, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->s:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    new-instance v0, Lcom/sec/android/emergencylauncher/view/i;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/emergencylauncher/view/i;-><init>(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;Lcom/sec/android/emergencylauncher/view/h;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->i:Lcom/sec/android/emergencylauncher/view/i;

    new-instance v0, Lcom/sec/android/emergencylauncher/view/h;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/view/h;-><init>(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->w:Landroid/os/Handler;

    const-string v0, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    const-string v2, "EmergencyLauncherBatteryMeterView"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [I

    fill-array-data v3, :array_0

    new-array v4, v5, [I

    fill-array-data v4, :array_1

    new-array v0, v5, [I

    fill-array-data v0, :array_2

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    array-length v5, v3

    mul-int/lit8 v0, v5, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    mul-int/lit8 v7, v0, 0x2

    aget v8, v3, v0

    aput v8, v6, v7

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    mul-int/lit8 v7, v0, 0x2

    add-int/lit8 v7, v7, 0x1

    aget v8, v4, v0

    aput v8, v6, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "!"

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->m:Ljava/lang/String;

    const-string v0, "X"

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->n:Ljava/lang/String;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->f:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColor(I)V

    const-string v0, "sans-serif-condensed"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColor(I)V

    const-string v0, "sans-serif"

    invoke-static {v0, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    const v1, -0x17cbf7

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const-string v0, "sans-serif"

    invoke-static {v0, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->g:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->g:Landroid/graphics/Paint;

    const v1, 0x7f070001

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-static {v2}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a(Landroid/content/res/Resources;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    const/4 v0, 0x0

    invoke-virtual {p0, v9, v0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x64
    .end array-data

    :array_1
    .array-data 4
        -0x1
        -0x1
    .end array-data

    :array_2
    .array-data 4
        -0x1
        -0x7433eb
    .end array-data

    :array_3
    .array-data 4
        0x4
        0xa
        0x14
        0x64
    .end array-data

    :array_4
    .array-data 4
        -0xcf00
        -0xcf00
        -0x2900
        -0x1
    .end array-data
.end method

.method private a(I)I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    aget v2, v1, v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v1, v1, v3

    if-gt p1, v2, :cond_1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->w:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(Landroid/content/res/Resources;)[F
    .locals 8

    const/4 v1, 0x0

    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget v5, v4, v0

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v5, v0, 0x1

    aget v5, v4, v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    array-length v0, v4

    new-array v0, v0, [F

    :goto_1
    array-length v5, v4

    if-ge v1, v5, :cond_1

    aget v5, v4, v1

    int-to-float v5, v5

    int-to-float v6, v3

    div-float/2addr v5, v6

    aput v5, v0, v1

    add-int/lit8 v5, v1, 0x1

    add-int/lit8 v6, v1, 0x1

    aget v6, v4, v6

    int-to-float v6, v6

    int-to-float v7, v2

    div-float/2addr v6, v7

    aput v6, v0, v5

    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v3, 0x2

    const/4 v13, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->i:Lcom/sec/android/emergencylauncher/view/i;

    iget v2, v4, Lcom/sec/android/emergencylauncher/view/i;->e:I

    const/4 v5, 0x4

    if-ne v2, v5, :cond_3

    iget v2, v4, Lcom/sec/android/emergencylauncher/view/i;->d:I

    const/4 v5, 0x3

    if-eq v2, v5, :cond_0

    iget v2, v4, Lcom/sec/android/emergencylauncher/view/i;->d:I

    const/4 v5, 0x7

    if-eq v2, v5, :cond_0

    iget v2, v4, Lcom/sec/android/emergencylauncher/view/i;->d:I

    const/4 v5, 0x6

    if-ne v2, v5, :cond_3

    :cond_0
    iput-boolean v12, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->u:Z

    const-string v2, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    const-string v5, "battery icon blink..."

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->w:Landroid/os/Handler;

    invoke-virtual {v2, v12}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->w:Landroid/os/Handler;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v2, v12, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_0
    iget v5, v4, Lcom/sec/android/emergencylauncher/view/i;->a:I

    const/4 v2, -0x1

    if-ne v5, v2, :cond_4

    :cond_2
    :goto_1
    return-void

    :cond_3
    iput-boolean v13, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->u:Z

    goto :goto_0

    :cond_4
    int-to-float v2, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v2, v6

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getPaddingLeft()I

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getPaddingRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getPaddingBottom()I

    move-result v9

    iget v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->k:I

    sub-int/2addr v10, v6

    sub-int v9, v10, v9

    iget v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->l:I

    sub-int/2addr v10, v7

    sub-int v8, v10, v8

    int-to-float v10, v9

    const v11, 0x3df5c28f    # 0.12f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->h:I

    iget-object v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    int-to-float v11, v8

    int-to-float v9, v9

    invoke-virtual {v10, v0, v0, v11, v9}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v9, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    int-to-float v7, v7

    int-to-float v6, v6

    invoke-virtual {v9, v7, v6}, Landroid/graphics/RectF;->offset(FF)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f090000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-float v6, v6

    iput v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->v:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    int-to-float v9, v8

    const/high16 v10, 0x3e800000    # 0.25f

    mul-float/2addr v9, v10

    add-float/2addr v7, v9

    iget v9, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->v:F

    add-float/2addr v7, v9

    iget-object v9, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    int-to-float v8, v8

    const/high16 v11, 0x3e800000    # 0.25f

    mul-float/2addr v8, v11

    sub-float v8, v10, v8

    iget v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->v:F

    sub-float/2addr v8, v10

    iget-object v10, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->h:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40a00000    # 5.0f

    add-float/2addr v10, v11

    invoke-virtual {v6, v7, v9, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->h:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-direct {p0, v5}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->a(I)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    const-string v7, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onDraw batteryColor : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v6, 0x60

    if-lt v5, v6, :cond_6

    move v0, v1

    :cond_5
    :goto_2
    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->r:Landroid/graphics/RectF;

    cmpl-float v2, v0, v1

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    :goto_3
    invoke-virtual {p1, v6, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->s:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v2, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->s:Landroid/graphics/RectF;

    iget v6, v2, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float v0, v1, v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v6

    iput v0, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->save(I)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->s:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-boolean v0, v4, Lcom/sec/android/emergencylauncher/view/i;->c:Z

    if-eqz v0, :cond_b

    iget v0, v4, Lcom/sec/android/emergencylauncher/view/i;->e:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40900000    # 4.5f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    const/high16 v6, 0x40e00000    # 7.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->q:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    const/high16 v7, 0x41200000    # 10.0f

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    invoke-static {v6, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    invoke-static {v7, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v7

    or-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    invoke-static {v7, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v7

    or-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    invoke-static {v7, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v7

    or-int/2addr v6, v7

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v6, v0, v1, v2, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    aget v2, v2, v13

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    aget v5, v5, v12

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    move v0, v3

    :goto_4
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    array-length v1, v1

    if-ge v0, v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    aget v5, v5, v0

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    add-int/lit8 v7, v0, 0x1

    aget v6, v6, v7

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_4

    :cond_6
    const/4 v6, 0x4

    if-le v5, v6, :cond_5

    move v0, v2

    goto/16 :goto_2

    :cond_7
    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->b:Landroid/graphics/Paint;

    goto/16 :goto_3

    :cond_8
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    aget v2, v2, v13

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->o:[F

    aget v5, v5, v12

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_9
    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->u:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->p:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_a
    iget v0, v4, Lcom/sec/android/emergencylauncher/view/i;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget v0, v4, Lcom/sec/android/emergencylauncher/view/i;->b:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->l:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->k:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->j:F

    add-float/2addr v1, v2

    const v2, 0x3ef5c28f    # 0.48f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x4

    if-gt v5, v0, :cond_2

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->l:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->k:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->j:F

    add-float/2addr v1, v2

    const v2, 0x3ef5c28f    # 0.48f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    const-string v0, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    const-string v1, "onAttachedToWindow2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->i:Lcom/sec/android/emergencylauncher/view/i;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->i:Lcom/sec/android/emergencylauncher/view/i;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/emergencylauncher/view/i;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    const-string v0, "EmergencyLauncher.EmergencyLauncherBatteryMeterView"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->i:Lcom/sec/android/emergencylauncher/view/i;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/high16 v2, 0x3f400000    # 0.75f

    iput p2, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->k:I

    iput p1, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->l:I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    int-to-float v1, p2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->e:Landroid/graphics/Paint;

    int-to-float v1, p2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/EmergencyLauncherBatteryMeterView;->j:F

    return-void
.end method
