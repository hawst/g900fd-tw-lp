.class Lcom/sec/android/emergencylauncher/view/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/view/d;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/view/d;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/view/e;->a:Lcom/sec/android/emergencylauncher/view/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.EMGridView"

    const-string v1, "MainFragment.getInstance().mDefaultsGrid.post(new Runnable()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/e;->a:Lcom/sec/android/emergencylauncher/view/d;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/view/d;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/view/EMGridView;->a:Lcom/sec/android/emergencylauncher/view/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/e;->a:Lcom/sec/android/emergencylauncher/view/d;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/view/d;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/view/EMGridView;->a:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/e;->a:Lcom/sec/android/emergencylauncher/view/d;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/view/d;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidate()V

    :cond_1
    return-void
.end method
