.class Lcom/sec/android/emergencylauncher/t;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick() : Y="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->g()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->l(Lcom/sec/android/emergencylauncher/LauncherActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onClick() : already running emergency call"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->g()F

    move-result v0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->y:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onClick() : launching primary contacts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.contacts.action.SHOW_EMERGENCY_CONTACT_LIST_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->r:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->m(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Lcom/sec/android/emergencylauncher/LauncherActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->n(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_5
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onClick() : perform Emergency call"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/t;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0
.end method
