.class public Lcom/sec/android/emergencylauncher/b/b;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Z

.field private static final b:Landroid/net/Uri;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/ContentResolver;

.field private final e:Landroid/content/ContentValues;

.field private final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/b/b;->a:Z

    const-string v0, "content://com.sec.badge/apps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/emergencylauncher/b/b;->b:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/b/b;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->d:Landroid/content/ContentResolver;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/android/emergencylauncher/b/b;
    .locals 3

    new-instance v0, Lcom/sec/android/emergencylauncher/b/b;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/b/b;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.BADGE_COUNT_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->c:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BADGE_COUNT_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "badge_count"

    invoke-virtual {p2, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v0, "badge_count_package_name"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "badge_count_class_name"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "EmergencyLauncher.BadgeCountReceiver"

    const-string v1, "packageName or className not specified"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-boolean v3, Lcom/sec/android/emergencylauncher/b/b;->a:Z

    if-eqz v3, :cond_3

    const-string v3, "EmergencyLauncher.BadgeCountReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "packageName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", className: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v3, "KDI"

    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "com.kddi.android.cmail"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v0, "com.kddi.android.cmail.ui.list.ThreadListActivity"

    :cond_4
    iget-object v3, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    const-string v4, "badgecount"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    aput-object v2, v1, v7

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    aput-object v0, v1, v8

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->d:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/android/emergencylauncher/b/b;->b:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    const-string v5, "package=? AND class=?"

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    const-string v3, "package"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    const-string v2, "class"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->d:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/emergencylauncher/b/b;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :cond_5
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->e:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/b/b;->f:[Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v2, v1, v8

    aput-object v2, v0, v7

    goto/16 :goto_0
.end method
