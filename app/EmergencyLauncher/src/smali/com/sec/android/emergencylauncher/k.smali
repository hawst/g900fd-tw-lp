.class Lcom/sec/android/emergencylauncher/k;
.super Landroid/telephony/PhoneStateListener;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/k;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call State Change : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-ne p1, v3, :cond_2

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Z)Z

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v0, v3, :cond_1

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->b(Z)Z

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/k;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/k;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->o(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_0

    :cond_2
    if-nez p1, :cond_4

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->h()Z

    move-result v0

    if-ne v0, v3, :cond_3

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->b(Z)Z

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/k;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    :cond_3
    invoke-static {v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Z)Z

    invoke-static {v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c(Z)Z

    goto :goto_1

    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c(Z)Z

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v0, v3, :cond_1

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->b(Z)Z

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/k;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    goto :goto_1
.end method
