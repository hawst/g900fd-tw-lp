.class Lcom/sec/android/emergencylauncher/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;Z)Z

    invoke-static {}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "onItemClick()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EditPosition"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "EmergencyLauncher.AllowedAppsListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick() : editposition="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v0, v4, :cond_1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    invoke-virtual {v1, p3}, Lcom/sec/android/emergencylauncher/b;->a(I)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;Lcom/sec/android/emergencylauncher/a/a;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/AppController;

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/AppController;->b(Lcom/sec/android/emergencylauncher/a/a;)V

    const-string v2, "EmergencyLauncher.AllowedAppsListActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "defaultApps : pos ="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " pkg : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/a;->a:Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->finish()V

    goto/16 :goto_0
.end method
