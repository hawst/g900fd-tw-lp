.class public Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;
.super Landroid/widget/ImageView;


# instance fields
.field public a:I

.field b:Lcom/sec/android/emergencylauncher/view/k;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a:I

    new-instance v0, Lcom/sec/android/emergencylauncher/view/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/emergencylauncher/view/k;-><init>(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;Lcom/sec/android/emergencylauncher/view/j;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->b:Lcom/sec/android/emergencylauncher/view/k;

    const-string v0, "EmergencyLauncher.UpsmUsageTimeView"

    const-string v1, "UpsmUsageTimeView"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    iput v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a:I

    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->c:I

    return v0
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->c:I

    return p1
.end method

.method private setUsageTimeImageUPSM(I)V
    .locals 8

    const/16 v7, 0x12

    const/16 v6, 0xf

    const/16 v5, 0xc

    const/16 v4, 0x9

    const/4 v3, 0x7

    const-string v0, "EmergencyLauncher.UpsmUsageTimeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUsageTimeImageUPSM Level is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x60

    if-gt v0, p1, :cond_1

    const/16 v0, 0x64

    if-gt p1, v0, :cond_1

    const v0, 0x7f02004d

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x5e

    if-gt v0, p1, :cond_2

    const/16 v0, 0x60

    if-ge p1, v0, :cond_2

    const v0, 0x7f02004c

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x5c

    if-gt v0, p1, :cond_3

    const/16 v0, 0x5e

    if-ge p1, v0, :cond_3

    const v0, 0x7f02004b

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x5a

    if-gt v0, p1, :cond_4

    const/16 v0, 0x5c

    if-ge p1, v0, :cond_4

    const v0, 0x7f02004a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x57

    if-gt v0, p1, :cond_5

    const/16 v0, 0x5a

    if-ge p1, v0, :cond_5

    const v0, 0x7f020049

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_5
    const/16 v0, 0x54

    if-gt v0, p1, :cond_6

    const/16 v0, 0x57

    if-ge p1, v0, :cond_6

    const v0, 0x7f020048

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_6
    const/16 v0, 0x51

    if-gt v0, p1, :cond_7

    const/16 v0, 0x54

    if-ge p1, v0, :cond_7

    const v0, 0x7f020047

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_7
    const/16 v0, 0x4e

    if-gt v0, p1, :cond_8

    const/16 v0, 0x51

    if-ge p1, v0, :cond_8

    const v0, 0x7f020046

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_8
    const/16 v0, 0x4b

    if-gt v0, p1, :cond_9

    const/16 v0, 0x4e

    if-ge p1, v0, :cond_9

    const v0, 0x7f020045

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto :goto_0

    :cond_9
    const/16 v0, 0x48

    if-gt v0, p1, :cond_a

    const/16 v0, 0x4b

    if-ge p1, v0, :cond_a

    const v0, 0x7f020044

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_a
    const/16 v0, 0x45

    if-gt v0, p1, :cond_b

    const/16 v0, 0x48

    if-ge p1, v0, :cond_b

    const v0, 0x7f020043

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_b
    const/16 v0, 0x42

    if-gt v0, p1, :cond_c

    const/16 v0, 0x45

    if-ge p1, v0, :cond_c

    const v0, 0x7f020042

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_c
    const/16 v0, 0x3f

    if-gt v0, p1, :cond_d

    const/16 v0, 0x42

    if-ge p1, v0, :cond_d

    const v0, 0x7f020041

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_d
    const/16 v0, 0x3c

    if-gt v0, p1, :cond_e

    const/16 v0, 0x3f

    if-ge p1, v0, :cond_e

    const v0, 0x7f020040

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_e
    const/16 v0, 0x39

    if-gt v0, p1, :cond_f

    const/16 v0, 0x3c

    if-ge p1, v0, :cond_f

    const v0, 0x7f02003f

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_f
    const/16 v0, 0x36

    if-gt v0, p1, :cond_10

    const/16 v0, 0x39

    if-ge p1, v0, :cond_10

    const v0, 0x7f02003e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0x35

    if-gt v0, p1, :cond_11

    const/16 v0, 0x36

    if-ge p1, v0, :cond_11

    const v0, 0x7f02003d

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_11
    const/16 v0, 0x33

    if-gt v0, p1, :cond_12

    const/16 v0, 0x35

    if-ge p1, v0, :cond_12

    const v0, 0x7f02003c

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_12
    const/16 v0, 0x31

    if-gt v0, p1, :cond_13

    const/16 v0, 0x33

    if-ge p1, v0, :cond_13

    const v0, 0x7f02003b

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_13
    const/16 v0, 0x30

    if-gt v0, p1, :cond_14

    const/16 v0, 0x31

    if-ge p1, v0, :cond_14

    const v0, 0x7f02003a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_14
    const/16 v0, 0x2d

    if-gt v0, p1, :cond_15

    const/16 v0, 0x30

    if-ge p1, v0, :cond_15

    const v0, 0x7f020039

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_15
    const/16 v0, 0x2a

    if-gt v0, p1, :cond_16

    const/16 v0, 0x2d

    if-ge p1, v0, :cond_16

    const v0, 0x7f020038

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_16
    const/16 v0, 0x27

    if-gt v0, p1, :cond_17

    const/16 v0, 0x2a

    if-ge p1, v0, :cond_17

    const v0, 0x7f020037

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_17
    const/16 v0, 0x24

    if-gt v0, p1, :cond_18

    const/16 v0, 0x27

    if-ge p1, v0, :cond_18

    const v0, 0x7f020036

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_18
    const/16 v0, 0x21

    if-gt v0, p1, :cond_19

    const/16 v0, 0x24

    if-ge p1, v0, :cond_19

    const v0, 0x7f020035

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_19
    const/16 v0, 0x1e

    if-gt v0, p1, :cond_1a

    const/16 v0, 0x21

    if-ge p1, v0, :cond_1a

    const v0, 0x7f020034

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1a
    const/16 v0, 0x1d

    if-gt v0, p1, :cond_1b

    const/16 v0, 0x1e

    if-ge p1, v0, :cond_1b

    const v0, 0x7f020033

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1b
    const/16 v0, 0x1b

    if-gt v0, p1, :cond_1c

    const/16 v0, 0x1d

    if-ge p1, v0, :cond_1c

    const v0, 0x7f020032

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1c
    const/16 v0, 0x18

    if-gt v0, p1, :cond_1d

    const/16 v0, 0x1b

    if-ge p1, v0, :cond_1d

    const v0, 0x7f020031

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1d
    const/16 v0, 0x15

    if-gt v0, p1, :cond_1e

    const/16 v0, 0x18

    if-ge p1, v0, :cond_1e

    const v0, 0x7f020030

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1e
    if-gt v7, p1, :cond_1f

    const/16 v0, 0x15

    if-ge p1, v0, :cond_1f

    const v0, 0x7f02002f

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1f
    if-gt v6, p1, :cond_20

    if-ge p1, v7, :cond_20

    const v0, 0x7f02002e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_20
    if-gt v5, p1, :cond_21

    if-ge p1, v6, :cond_21

    const v0, 0x7f02002d

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_21
    if-gt v4, p1, :cond_22

    if-ge p1, v5, :cond_22

    const v0, 0x7f02002c

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_22
    if-gt v3, p1, :cond_23

    if-ge p1, v4, :cond_23

    const v0, 0x7f02002b

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_23
    const/4 v0, 0x6

    if-gt v0, p1, :cond_24

    if-ge p1, v3, :cond_24

    const v0, 0x7f02002a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_24
    const/4 v0, 0x5

    if-gt p1, v0, :cond_0

    const v0, 0x7f020029

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setImageResource(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    const-string v0, "EmergencyLauncher.UpsmUsageTimeView"

    const-string v1, "onAttachedToWindow2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->b:Lcom/sec/android/emergencylauncher/view/k;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->b:Lcom/sec/android/emergencylauncher/view/k;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/emergencylauncher/view/k;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    const-string v0, "EmergencyLauncher.UpsmUsageTimeView"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->b:Lcom/sec/android/emergencylauncher/view/k;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    const-string v0, "EmergencyLauncher.UpsmUsageTimeView"

    const-string v1, "onDraw:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a:I

    if-nez v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->c:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->c:I

    invoke-direct {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setUsageTimeImageUPSM(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->setVisibility(I)V

    goto :goto_0
.end method
