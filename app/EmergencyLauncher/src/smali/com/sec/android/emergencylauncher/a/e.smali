.class public Lcom/sec/android/emergencylauncher/a/e;
.super Ljava/lang/Object;


# static fields
.field public static final a:Z

.field public static b:Z

.field public static c:I

.field public static d:Z

.field public static e:Z

.field public static f:I

.field public static g:I

.field public static h:Z

.field public static i:Z

.field public static j:I

.field public static k:I

.field public static l:I

.field public static m:I

.field public static n:I

.field public static o:I

.field public static p:Z

.field public static q:Z

.field public static r:Z

.field public static s:Z

.field public static t:Z

.field public static u:Ljava/lang/String;

.field public static v:Ljava/lang/String;

.field public static w:Ljava/lang/String;

.field public static x:I

.field public static y:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    sput v1, Lcom/sec/android/emergencylauncher/a/e;->c:I

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    sput v1, Lcom/sec/android/emergencylauncher/a/e;->f:I

    sput v1, Lcom/sec/android/emergencylauncher/a/e;->g:I

    sput-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->h:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->i:Z

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->j:I

    sput v5, Lcom/sec/android/emergencylauncher/a/e;->k:I

    sput v5, Lcom/sec/android/emergencylauncher/a/e;->l:I

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->m:I

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->n:I

    sput v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->q:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->r:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->s:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->t:Z

    sput-object v3, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    sput-object v3, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    sput-object v3, Lcom/sec/android/emergencylauncher/a/e;->w:Ljava/lang/String;

    const/16 v0, 0x8c

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->x:I

    const/high16 v0, 0x430c0000    # 140.0f

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->y:F

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
