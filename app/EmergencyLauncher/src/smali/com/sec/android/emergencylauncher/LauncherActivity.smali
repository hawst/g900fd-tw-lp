.class public Lcom/sec/android/emergencylauncher/LauncherActivity;
.super Landroid/app/Activity;


# static fields
.field private static F:F

.field private static K:Landroid/content/BroadcastReceiver;

.field private static O:Z

.field private static P:Z

.field private static W:Landroid/media/SoundPool;

.field private static X:Landroid/media/SoundPool$OnLoadCompleteListener;

.field private static Y:Z

.field private static Z:Z

.field private static aa:Z

.field private static ab:I

.field private static ac:I

.field private static ad:I

.field private static ae:I

.field private static af:I

.field private static ag:I

.field private static ah:I

.field private static final ai:Ljava/lang/String;

.field public static e:Z

.field public static f:Z

.field private static final l:Z

.field private static y:I


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/telephony/TelephonyManager;

.field private D:Landroid/telephony/TelephonyManager;

.field private E:Landroid/telephony/TelephonyManager;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/app/AlertDialog;

.field private J:Landroid/app/AlertDialog$Builder;

.field private L:Lcom/sec/android/emergencylauncher/b/b;

.field private M:Landroid/telephony/PhoneStateListener;

.field private N:Landroid/app/ProgressDialog;

.field private Q:Z

.field private R:Landroid/view/View$OnClickListener;

.field private S:Landroid/view/View$OnTouchListener;

.field private T:Landroid/view/View$OnClickListener;

.field private U:Landroid/view/View$OnClickListener;

.field private final V:Landroid/telephony/PhoneStateListener;

.field a:I

.field private aj:Landroid/os/Handler;

.field b:Lcom/sec/android/emergencylauncher/view/EMGridView;

.field c:Lcom/sec/android/emergencylauncher/view/f;

.field d:Lcom/sec/android/emergencylauncher/AppController;

.field g:Ljava/text/NumberFormat;

.field h:Landroid/app/KeyguardManager;

.field i:Landroid/widget/AdapterView$OnItemClickListener;

.field j:Landroid/content/BroadcastReceiver;

.field k:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Landroid/content/Context;

.field private o:Landroid/content/pm/PackageManager;

.field private p:Lcom/sec/android/emergencylauncher/a/c;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/widget/LinearLayout;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/content/BroadcastReceiver;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->y:I

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->e:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->f:Z

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->F:F

    sput-object v3, Lcom/sec/android/emergencylauncher/LauncherActivity;->K:Landroid/content/BroadcastReceiver;

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->O:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->P:Z

    sput-object v3, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    sput-object v3, Lcom/sec/android/emergencylauncher/LauncherActivity;->X:Landroid/media/SoundPool$OnLoadCompleteListener;

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->Y:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->Z:Z

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ab:I

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ac:I

    sput v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ad:I

    sput v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ae:I

    sput v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->af:I

    sput v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ag:I

    sput v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ah:I

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->ai:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->m:Z

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->D:Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->E:Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->I:Landroid/app/AlertDialog;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->M:Landroid/telephony/PhoneStateListener;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->N:Landroid/app/ProgressDialog;

    iput-boolean v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Q:Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->g:Ljava/text/NumberFormat;

    new-instance v0, Lcom/sec/android/emergencylauncher/s;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/s;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->i:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/t;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/t;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->R:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/u;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/u;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->S:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/v;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/v;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->T:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/j;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/j;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->U:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/k;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/k;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->V:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/l;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/l;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->j:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/sec/android/emergencylauncher/m;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/m;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->aj:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/emergencylauncher/n;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/n;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->k:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(F)F
    .locals 0

    sput p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->F:F

    return p0
.end method

.method static synthetic a(I)I
    .locals 0

    sput p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->ac:I

    return p0
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/LauncherActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->I:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-direct {v0, p1}, Lcom/sec/android/emergencylauncher/a/a;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.android.emergencylauncher"

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->b(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.emergencylauncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.sec.android.emergencylauncher."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.sec.android.emergencylauncher."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v5}, Lcom/sec/android/emergencylauncher/a/a;->b(I)V

    const-string v2, "LED light"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-nez v2, :cond_1

    const v2, 0x7f02005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-ne v2, v5, :cond_0

    const v2, 0x7f02005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    const-string v2, "Siren"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v5}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-nez v2, :cond_3

    const v2, 0x7f02005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v2, v5, :cond_0

    const v2, 0x7f02005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_7

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const-string v6, "EmergencyLauncher.LauncherActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "preArrangeDefaultApps : check PackageEnabled = "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/a/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    sput v3, Lcom/sec/android/emergencylauncher/a/e;->k:I

    sub-int v6, v3, v4

    move v3, v2

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/a/d;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v1

    if-lez v1, :cond_4

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    :goto_2
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    :cond_3
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "preArrangeDefaultApps : orig pos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", offset = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v1

    sub-int/2addr v1, v6

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    move v1, v2

    goto :goto_2

    :cond_5
    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "preArrangeDefaultApps : disabled pkg = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    goto :goto_3

    :cond_6
    sput v4, Lcom/sec/android/emergencylauncher/a/e;->l:I

    move-object v0, v5

    :goto_4
    return-object v0

    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/a/d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_9
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    goto :goto_6

    :cond_a
    move-object v0, v1

    goto :goto_4
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->n()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/LauncherActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->P:Z

    return p0
.end method

.method static synthetic b(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->G:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(I)V
    .locals 5

    const v4, 0x7f0a0035

    const v2, 0x7f0a0015

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/sec/android/emergencylauncher/a/d;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    if-le p1, v3, :cond_1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->H:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->H:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-le p1, v3, :cond_3

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Z:Z

    return p0
.end method

.method static synthetic c(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->O:Z

    return p0
.end method

.method static synthetic d(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->x:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private d(Z)Ljava/lang/String;
    .locals 10

    const/4 v2, 0x0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEmergencyContact() theme is : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_4

    const-string v0, "content://com.android.contacts/groups/title/ICE/contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "TAG"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ICE getPhoneNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/sec/android/emergencylauncher/a/d;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/sec/android/emergencylauncher/a/d;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ","

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_3

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getEmergencyContact() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ICE"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "emergency"

    const-string v3, "1"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "defaultId"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "data1"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "display_name"

    aput-object v1, v5, v0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v0, -0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_5
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "data1"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, ","

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_6
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0
.end method

.method static synthetic d()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    return v0
.end method

.method static synthetic e()Landroid/content/BroadcastReceiver;
    .locals 1

    sget-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->K:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/emergencylauncher/LauncherActivity;)Lcom/sec/android/emergencylauncher/b/b;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->L:Lcom/sec/android/emergencylauncher/b/b;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/telephony/PhoneStateListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->V:Landroid/telephony/PhoneStateListener;

    return-object v0
.end method

.method static synthetic f()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    return v0
.end method

.method static synthetic g()F
    .locals 1

    sget v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->F:F

    return v0
.end method

.method static synthetic g(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/telephony/TelephonyManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->C:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->o()V

    return-void
.end method

.method static synthetic h()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->P:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->q()V

    return-void
.end method

.method static synthetic i()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->O:Z

    return v0
.end method

.method static synthetic j(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->v()V

    return-void
.end method

.method static synthetic j()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Z:Z

    return v0
.end method

.method private k()V
    .locals 3

    const/high16 v2, 0x7f020000

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f030008

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const v0, 0x7f0d0021

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0a003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->t()V

    return-void
.end method

.method private l()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "enabled"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "flag"

    const/16 v3, 0x200

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v2, 0x7f0a0039

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f020058

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, -0x2

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-static {v3, v6, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method static synthetic l(Lcom/sec/android/emergencylauncher/LauncherActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->m:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/app/AlertDialog$Builder;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    return-object v0
.end method

.method private m()V
    .locals 10

    const/4 v9, 0x2

    const v8, 0x7f0d002a

    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v2, 0x0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v3, "init()"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->ensureCapacity(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/view/EMGridView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->i:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencylauncher/view/EMGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/view/f;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    const v0, 0x7f0d0022

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->q:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v4, "keyguard"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->h:Landroid/app/KeyguardManager;

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->q:Landroid/widget/TextView;

    const v4, 0x7f0a001b

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_0

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-nez v0, :cond_2

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_e

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->l()V

    const v0, 0x7f0d001a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->G:Landroid/widget/TextView;

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->H:Landroid/widget/TextView;

    :cond_1
    :goto_1
    new-instance v0, Lcom/sec/android/emergencylauncher/p;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/p;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->x:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->x:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    new-instance v0, Lcom/sec/android/emergencylauncher/q;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/q;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    sput-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->K:Landroid/content/BroadcastReceiver;

    sget-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->K:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/sec/android/emergencylauncher/b/b;->a(Landroid/content/Context;)Lcom/sec/android/emergencylauncher/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->L:Lcom/sec/android/emergencylauncher/b/b;

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_5

    const v0, 0x7f0d0024

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->z:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->z:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->z:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->S:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v1, :cond_3

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-ne v0, v9, :cond_11

    :cond_3
    invoke-virtual {p0, v8}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    const v4, 0x7f0a002d

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_2
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->z:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020055

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0d0029

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->A:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->A:Landroid/widget/ImageView;

    const v4, 0x7f020013

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->C:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->C:Landroid/telephony/TelephonyManager;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->V:Landroid/telephony/PhoneStateListener;

    const/16 v5, 0x20

    invoke-virtual {v0, v4, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    const-string v0, "all_sound_off"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_6

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init() : turnOffAllSoundsInNormalMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v0, Lcom/sec/android/emergencylauncher/r;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/r;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    sput-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->X:Landroid/media/SoundPool$OnLoadCompleteListener;

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v1, :cond_7

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-ne v0, v9, :cond_8

    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c()V

    :cond_8
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_9

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->b()V

    :cond_9
    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "assistive light : ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "torch_light"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "torch_light"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_b

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_a

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assistive light is on("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "torch_light"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). so turns it off"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_b
    return-void

    :cond_c
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->q:Landroid/widget/TextView;

    const v4, 0x7f0a001d

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->q:Landroid/widget/TextView;

    const v4, 0x7f0a001c

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_e
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-nez v0, :cond_f

    const v0, 0x7f0d000e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->r:Landroid/widget/LinearLayout;

    const v0, 0x7f0d000f

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->s:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->r:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->r:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0a0027

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0a0032

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    const v0, 0x7f0d0011

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    const v0, 0x7f0d0012

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->u:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->U:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0a0030

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0a0032

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_f
    const v0, 0x7f0d0026

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->G:Landroid/widget/TextView;

    const v0, 0x7f0d0028

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->v:Landroid/widget/TextView;

    const v0, 0x7f0d0027

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->w:Landroid/widget/TextView;

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->G:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_10
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->r:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v4, Landroid/view/ContextThemeWrapper;

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const v6, 0x7f0b0002

    invoke-direct {v4, v5, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a0008

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a0005

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a0043

    new-instance v5, Lcom/sec/android/emergencylauncher/i;

    invoke-direct {v5, p0}, Lcom/sec/android/emergencylauncher/i;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->J:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a002c

    new-instance v5, Lcom/sec/android/emergencylauncher/o;

    invoke-direct {v5, p0}, Lcom/sec/android/emergencylauncher/o;-><init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    :cond_11
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->r:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencylauncher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->B:Landroid/widget/TextView;

    const v4, 0x7f0a0008

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_12
    move v0, v2

    goto/16 :goto_3
.end method

.method static synthetic n(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->I:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private n()V
    .locals 2

    const-string v0, "CAN"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CANADA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Australia"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "emcall"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/EmergencySettings;->getEmergencyNumber(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "getEmergencyNumber() is failed, so use default value"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "911"

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->m:Z

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencymode/EmergencyManager;->requestCallPrivileged(Ljava/lang/String;)I

    goto :goto_0
.end method

.method private o()V
    .locals 8

    const v7, 0x7f02005c

    const/4 v6, 0x2

    const v5, 0x7f0a0027

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleLedTorch() : LauncherCommon.TORCH_STATE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-nez v0, :cond_6

    sput v3, Lcom/sec/android/emergencylauncher/a/e;->f:I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "torch_light"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v3, :cond_0

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-ne v0, v6, :cond_5

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "LED light"

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "en_US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "Torch"

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_1
    :goto_1
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "turn on LED torch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {v3}, Lcom/sec/android/hardware/SecHardwareInterface;->setTorchLight(I)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->s:Landroid/widget/ImageView;

    const v1, 0x7f02005d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->r:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0a0033

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-ne v0, v3, :cond_3

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->f:I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "torch_light"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v3, :cond_7

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-ne v0, v6, :cond_b

    :cond_7
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "LED light"

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "en_US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "Torch"

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    :goto_3
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_8
    :goto_4
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_9

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "turn off LED torch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-static {v4}, Lcom/sec/android/hardware/SecHardwareInterface;->setTorchLight(I)V

    goto/16 :goto_2

    :cond_a
    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->r:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0a0032

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method static synthetic o(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->p()V

    return-void
.end method

.method static synthetic p(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->N:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private p()V
    .locals 3

    const v2, 0x7f0a0030

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->u:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->u:Landroid/widget/ImageView;

    const v1, 0x7f02005f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0a0033

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->u:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->u:Landroid/widget/ImageView;

    const v1, 0x7f02005e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->t:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0a0032

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private q()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x7

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleSiren() : LauncherCommon.SIREN_STATE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-nez v1, :cond_6

    sget-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "mute"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ad:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ae:I

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->af:I

    invoke-virtual {v0, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ag:I

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ah:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v6, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    invoke-virtual {v0, v4, v4}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    invoke-virtual {v0, v7, v4}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v6, v3}, Landroid/media/SoundPool;-><init>(III)V

    sput-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    sget-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    sget-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->X:Landroid/media/SoundPool$OnLoadCompleteListener;

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    sget-object v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, p0, v1, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    sput v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->ab:I

    :cond_1
    sput v4, Lcom/sec/android/emergencylauncher/a/e;->g:I

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v4, :cond_2

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "Siren"

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0a0030

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->p()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    goto :goto_1

    :cond_6
    sget v1, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v1, v4, :cond_3

    sget-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    if-eqz v1, :cond_7

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "mute"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_7
    invoke-virtual {v0, v6, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    invoke-virtual {v0, v4, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    invoke-virtual {v0, v7, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    sget-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    if-eqz v1, :cond_8

    sget-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    sget v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ac:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->stop(I)V

    sget-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    sget v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ab:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->unload(I)Z

    sget-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    invoke-virtual {v1}, Landroid/media/SoundPool;->release()V

    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->W:Landroid/media/SoundPool;

    :cond_8
    invoke-virtual {v0, v4, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    invoke-virtual {v0, v7, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ad:I

    if-eq v1, v5, :cond_9

    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ad:I

    invoke-virtual {v0, v6, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_9
    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ae:I

    if-eq v1, v5, :cond_a

    const/4 v1, 0x3

    sget v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->ae:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    sput v5, Lcom/sec/android/emergencylauncher/LauncherActivity;->ae:I

    :cond_a
    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->af:I

    if-eq v1, v5, :cond_b

    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->af:I

    invoke-virtual {v0, v4, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    sput v5, Lcom/sec/android/emergencylauncher/LauncherActivity;->af:I

    :cond_b
    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ag:I

    if-eq v1, v5, :cond_c

    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ag:I

    invoke-virtual {v0, v7, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    sput v5, Lcom/sec/android/emergencylauncher/LauncherActivity;->ag:I

    :cond_c
    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ah:I

    if-eq v1, v5, :cond_d

    sget v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->ah:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    sput v5, Lcom/sec/android/emergencylauncher/LauncherActivity;->ah:I

    :cond_d
    sput v3, Lcom/sec/android/emergencylauncher/a/e;->g:I

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v0, v4, :cond_e

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    :cond_e
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "Siren"

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0a0030

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    goto/16 :goto_0

    :cond_f
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    goto/16 :goto_1
.end method

.method static synthetic q(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->r()V

    return-void
.end method

.method private r()V
    .locals 3

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "launchInternet()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->f:Z

    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Intent;Landroid/content/Context;)V

    return-void
.end method

.method private s()V
    .locals 11

    const v10, 0x7f0a0030

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/emergencylauncher/a/c;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filldefaultApps() : defaultAppsFromDb.size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->m:I

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_f

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    sget-boolean v3, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v3, :cond_1

    const-string v3, "EmergencyLauncher.LauncherActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "filldefaultApps() : i="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", defaultApp.getPosition()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filldefaultApps() : empty app is added, i="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v3, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x200

    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    const/16 v7, 0x220

    invoke-virtual {v6, v5, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/Intent;)V

    sget-boolean v7, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-nez v7, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.android.contacts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    const-string v7, "com.android.phone"

    const/16 v8, 0x200

    invoke-virtual {v3, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v7, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v7, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v3}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v3, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->b(I)V

    :cond_4
    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v6

    if-le v3, v6, :cond_5

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v6

    invoke-virtual {v3, v6, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_5
    sget-boolean v3, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v3, :cond_2

    const-string v3, "EmergencyLauncher.LauncherActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Added to favorites app list: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v3

    sget v6, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v6, v9, :cond_6

    sget v6, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    :cond_6
    if-nez v2, :cond_9

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.sec.android.emergencylauncher.LED light"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v3, "LED light"

    invoke-static {v0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "en_US"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "Torch"

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    :goto_4
    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_7
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v7, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    :cond_8
    const v3, 0x7f0a0027

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    if-ne v2, v9, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v3, "Siren"

    invoke-static {v0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    invoke-virtual {p0, v10}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v9, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_a
    sget-boolean v6, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v6, :cond_d

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v6, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v5}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-nez v5, :cond_c

    const v5, 0x7f02005e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_b
    :goto_5
    invoke-virtual {p0, v10}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v5

    invoke-virtual {v3, v5, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_c
    sget v5, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v5, v9, :cond_b

    const v5, 0x7f02005f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    :cond_d
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_e

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Package Name Not Found for component : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "EmergencyLauncher.LauncherActivity"

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_f
    return-void
.end method

.method private t()V
    .locals 5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Show Composer()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.mms"

    const-string v2, "com.android.mms.ui.ConversationComposer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Activity Not Found for message"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/a/d;->b(Landroid/content/Context;)I

    move-result v0

    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "zerotheme : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Q:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", recipientLimit : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Q:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/a/d;->a(Landroid/content/Context;)I

    move-result v1

    if-le v1, v0, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "intent.action.INTERACTION_ICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "maxRecipientCount"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string v2, "smsto"

    iget-boolean v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Q:Z

    invoke-direct {p0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d(Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private u()V
    .locals 6

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "getLastLocation()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_2

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "getLastLocation() : loc is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    const-string v1, "GPS_ACCURACY"

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "GPS_LATITUDE"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "GPS_LONGITUDE"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private v()V
    .locals 3

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Show apps list "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-class v2, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "EditPosition"

    iget v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method a()Lcom/sec/android/emergencylauncher/a/a;
    .locals 4

    const v3, 0x7f020015

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/emergencylauncher/a/a;

    const-string v1, "Add"

    invoke-direct {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_0

    const v2, 0x7f020016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_2

    const v2, 0x7f020014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 11

    const/4 v10, 0x1

    const v7, 0x7f0a002f

    const/4 v6, 0x0

    const-string v0, "com.android.mms"

    const-string v1, "com.android.mms.ui.ConversationComposer"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x34000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v0, "force_new_composer"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->u()V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GPS_LATITUDE"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/EmergencySettings;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "GPS_LONGITUDE"

    const-string v3, "0"

    invoke-static {v0, v2, v3}, Lcom/sec/android/emergencymode/EmergencySettings;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Show Composer() : couldn\'t find location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0a0028

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "has_location"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_1
    const-string v1, "sms_body"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0a0034

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->q:Z

    if-eqz v3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\nhttp://mo.amap.com/?q="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&name="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "Emergency_Alert"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&dev=1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "EmergencyLauncher.LauncherActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AutoNavi"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const-string v1, "has_location"

    invoke-virtual {p1, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_3
    :try_start_1
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "%s\nhttp://maps.google.com/maps?f=q&q=(%.7f,%.7f)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const v7, 0x7f0a002f

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_3

    :catch_0
    move-exception v1

    const-string v2, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Show Composer() : exception occurred - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Activity Not Found for send my location"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method a(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v2, "setInternetAppToDB()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->m:I

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInternetAppToDB() : defaultApp.getPosition()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->e:Z

    if-eqz v0, :cond_5

    :cond_4
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->e:Z

    if-eqz v0, :cond_6

    sput-boolean v1, Lcom/sec/android/emergencylauncher/LauncherActivity;->e:Z

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInternetAppToDB() : Tips exist. add Internet app and replace Tips. isTipsAppAdded="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    const-string v1, "Internet"

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->b(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->c(Ljava/lang/String;)V

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->n:I

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    return-void

    :cond_6
    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInternetAppToDB() : pos("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is empty. adding Internet app."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method b()V
    .locals 7

    const/4 v0, 0x0

    const/4 v4, -0x1

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    if-eqz v1, :cond_0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "addInitlalApp() : return for VZW"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "EmergencyLauncher.LauncherActivity"

    const-string v2, "addInitlalApp()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/ComponentName;

    sget-object v2, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    const/16 v3, 0x20

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/emergencylauncher/a/c;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v5

    move v1, v0

    move v2, v0

    move v3, v4

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v0

    sget v6, Lcom/sec/android/emergencylauncher/a/e;->n:I

    if-ne v0, v6, :cond_1

    move v3, v1

    :cond_1
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "Internet app does not exist, so return"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    if-nez v2, :cond_5

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "addInitlalApp() : perform adding browser app"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v3, v4, :cond_4

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Lcom/sec/android/emergencylauncher/a/a;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Lcom/sec/android/emergencylauncher/a/a;)V

    goto :goto_0

    :cond_5
    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addInitlalApp() : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method c()V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v2, "InitAppsForJapan()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    move v0, v1

    :goto_1
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->m:I

    if-ge v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/emergencylauncher/a/c;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.sec.android.emergencylauncher.LED light"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "InitAppsForJapan() : already add, so return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v2, "InitAppsForJapan() : adjust db values"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->b(I)V

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v2, v0, v4}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->b(I)V

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v2, v0, v4}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    const/4 v0, 0x3

    move v2, v0

    :goto_2
    if-ltz v2, :cond_3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    add-int/lit8 v4, v2, 0x2

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_3
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v2, "InitAppsForJapan() : adding LED light, Siren"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v2, "LED light"

    invoke-static {v0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "en_US"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Torch"

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    :goto_3
    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v2, v2, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const-string v1, "EmergencyLauncher.LauncherActivity"

    const-string v2, "InitAppsForJapan() : perform db update"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-string v1, "Siren"

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    const v1, 0x7f0a0030

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v6, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const-string v1, "EmergencyLauncher.LauncherActivity"

    const-string v2, "InitAppsForJapan() : perform db update"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    const v2, 0x7f0a0027

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    const/4 v6, -0x1

    const/4 v5, 0x1

    if-ne p1, v5, :cond_2

    if-ne p2, v6, :cond_2

    if-eqz p3, :cond_2

    const-string v0, "result"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz v0, :cond_1

    const-string v2, "ICE Length : "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    aget-object v0, v0, v5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    const-string v3, "smsto"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Intent;)V

    :cond_2
    const/16 v0, 0x64

    if-ne p1, v0, :cond_5

    if-ne p2, v6, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->s()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_4

    if-ne p2, v6, :cond_4

    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_6
    const-string v0, "Error"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() : density w="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", h="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_3

    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->setContentView(I)V

    :goto_0
    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() : isUPSM = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->o:Landroid/content/pm/PackageManager;

    invoke-static {}, Lcom/sec/android/emergencylauncher/a/c;->a()Lcom/sec/android/emergencylauncher/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->p:Lcom/sec/android/emergencylauncher/a/c;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Q:Z

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->k()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    if-nez v0, :cond_1

    iput-object p0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->m()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->s()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/AppController;->c()V

    :cond_2
    return-void

    :cond_3
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v0, :cond_4

    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->setContentView(I)V

    goto :goto_0

    :cond_4
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->setContentView(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->o()V

    :cond_0
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->q()V

    :cond_1
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->aa:Z

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "mute"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x1

    const-string v0, "EmergencyLauncher.LauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOptionsItemSelected(): item:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getOrder()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v5

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "enabled"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "flag"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "enabled"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "flag"

    const/16 v2, 0x200

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->n:Landroid/content/Context;

    const-class v2, Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v0, "ro.build.scafe"

    const-string v2, "unknown"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "EmergencyLauncher.LauncherActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scafe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "capuccino"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.android.settings.Settings"

    :goto_1
    const-string v2, "com.android.settings"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const-string v0, "com.android.settings.GridSettings"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0d002f
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->l:Z

    if-eqz v0, :cond_0

    const-string v4, "EmergencyLauncher.LauncherActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPrepareOptionsMenu() : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->d:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    const v1, 0x7f0d0032

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_2
    return v3

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_3
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->m:Z

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_1
    sget-boolean v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->Y:Z

    if-eqz v0, :cond_2

    sput-boolean v2, Lcom/sec/android/emergencylauncher/LauncherActivity;->Y:Z

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->o()V

    :cond_2
    return-void
.end method
