.class public Lcom/sec/android/emergencylauncher/EditModeActivity;
.super Landroid/app/Activity;


# static fields
.field private static final f:Z


# instance fields
.field a:Lcom/sec/android/emergencylauncher/view/EMGridView;

.field b:Lcom/sec/android/emergencylauncher/view/f;

.field c:Z

.field d:Ljava/text/NumberFormat;

.field e:Landroid/widget/AdapterView$OnItemClickListener;

.field private g:Lcom/sec/android/emergencylauncher/AppController;

.field private h:Landroid/content/Context;

.field private i:Lcom/sec/android/emergencylauncher/a/c;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:I

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/view/MenuItem;

.field private q:Landroid/content/BroadcastReceiver;

.field private r:Ljava/util/ArrayList;

.field private s:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/EditModeActivity;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->i:Lcom/sec/android/emergencylauncher/a/c;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->q:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->d:Ljava/text/NumberFormat;

    new-instance v0, Lcom/sec/android/emergencylauncher/g;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/g;-><init>(Lcom/sec/android/emergencylauncher/EditModeActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->s:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/emergencylauncher/h;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/h;-><init>(Lcom/sec/android/emergencylauncher/EditModeActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->e:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/EditModeActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    invoke-static {p1}, Lcom/sec/android/emergencylauncher/a/d;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->o:Landroid/widget/TextView;

    const v1, 0x7f0a0035

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->b(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/EditModeActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/EditModeActivity;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/emergencylauncher/EditModeActivity;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 2

    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "populateData()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->i:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/EditModeActivity;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/emergencylauncher/EditModeActivity;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->m:I

    return p1
.end method

.method private b()V
    .locals 3

    const/high16 v2, 0x7f020000

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f030008

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const v0, 0x7f0d0021

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0a003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    sget-boolean v0, Lcom/sec/android/emergencylauncher/EditModeActivity;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.EditModeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveApplicationInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->k:I

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->l:I

    sub-int/2addr v1, v2

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->i:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/emergencylauncher/EditModeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->d()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/emergencylauncher/EditModeActivity;)Lcom/sec/android/emergencylauncher/AppController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    return-object v0
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    invoke-virtual {v2, v0}, Lcom/sec/android/emergencylauncher/AppController;->b(Lcom/sec/android/emergencylauncher/a/a;)V

    invoke-direct {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->a(Lcom/sec/android/emergencylauncher/a/a;)V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    const-class v2, Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->finish()V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/emergencylauncher/EditModeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->f()V

    return-void
.end method

.method static synthetic e(Lcom/sec/android/emergencylauncher/EditModeActivity;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->m:I

    return v0
.end method

.method private e()V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->i:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/emergencylauncher/a/c;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v4

    move v0, v1

    :goto_0
    sget v2, Lcom/sec/android/emergencylauncher/a/e;->m:I

    if-ge v0, v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/pm/ActivityInfo;

    invoke-direct {v0}, Landroid/content/pm/ActivityInfo;-><init>()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    move v2, v1

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    new-instance v6, Landroid/content/ComponentName;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v3, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    const/16 v7, 0x200

    invoke-virtual {v5, v3, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    const/16 v7, 0x220

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/Intent;)V

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v7

    invoke-virtual {v3, v7, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    sget v7, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-eq v7, v9, :cond_3

    sget v7, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    :cond_3
    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v6, "com.sec.android.emergencylauncher.LED light"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    const-string v3, "LED light"

    invoke-static {v0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    const v3, 0x7f0a0027

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    if-ne v2, v9, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.sec.android.emergencylauncher.Siren"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    const-string v3, "Siren"

    invoke-static {v0, v3}, Lcom/sec/android/emergencylauncher/LauncherActivity;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    const v3, 0x7f0a0030

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v9, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_5
    sget-boolean v0, Lcom/sec/android/emergencylauncher/EditModeActivity;->f:Z

    if-eqz v0, :cond_6

    const-string v0, "EmergencyLauncher.EditModeActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Package Name Not Found for component : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "EmergencyLauncher.EditModeActivity"

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v3, v3, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_7
    return-void
.end method

.method static synthetic f(Lcom/sec/android/emergencylauncher/EditModeActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    const-class v2, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "EditPosition"

    iget v2, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->m:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic g(Lcom/sec/android/emergencylauncher/EditModeActivity;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->p:Landroid/view/MenuItem;

    return-object v0
.end method

.method private g()Lcom/sec/android/emergencylauncher/a/a;
    .locals 4

    const v3, 0x7f020015

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/emergencylauncher/a/a;

    const-string v1, "Add"

    invoke-direct {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_0

    const v2, 0x7f020016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_2

    const v2, 0x7f020014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->e()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->b:Lcom/sec/android/emergencylauncher/view/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->b:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidateViews()V

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->c()V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    const v9, 0x7f0a0033

    const v8, 0x7f0a0032

    const v7, 0x7f0a0030

    const v6, 0x7f0a0027

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_6

    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->setContentView(I)V

    const v0, 0x7f0d0018

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    :goto_1
    iput-object p0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->g:Lcom/sec/android/emergencylauncher/AppController;

    invoke-static {}, Lcom/sec/android/emergencylauncher/a/c;->a()Lcom/sec/android/emergencylauncher/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->i:Lcom/sec/android/emergencylauncher/a/c;

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->b()V

    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->l:Landroid/widget/TextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->l:Landroid/widget/TextView;

    const v1, 0x7f0a002e

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/view/EMGridView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->e:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/view/f;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->b:Lcom/sec/android/emergencylauncher/view/f;

    const v0, 0x7f0d0015

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->j:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->j:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->j:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0003

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0d0016

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->k:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_3

    const v0, 0x7f0d000e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0d000f

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f0d0011

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const v3, 0x7f0d0012

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    sget v5, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-ne v5, v4, :cond_8

    const v5, 0x7f02005d

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v0, v4, :cond_9

    const v0, 0x7f02005f

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    const v0, 0x7f0d001a

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->o:Landroid/widget/TextView;

    new-instance v0, Lcom/sec/android/emergencylauncher/f;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/f;-><init>(Lcom/sec/android/emergencylauncher/EditModeActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->q:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->q:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_4
    return-void

    :cond_5
    move v0, v4

    goto/16 :goto_0

    :cond_6
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v0, :cond_7

    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->setContentView(I)V

    goto/16 :goto_1

    :cond_7
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->setContentView(I)V

    goto/16 :goto_1

    :cond_8
    sget v5, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-nez v5, :cond_2

    const v5, 0x7f02005c

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-nez v0, :cond_3

    const v0, 0x7f02005e

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->q:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "onDestroy() : mBatteryBroadcastReceiver is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    :sswitch_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/EditModeActivity;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "onOptionsItemSelected() : id.home"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "flag"

    const/16 v2, 0x200

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->c()V

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.GridSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->c()V

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->d()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->d()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0d002d -> :sswitch_3
        0x7f0d002e -> :sswitch_4
        0x7f0d002f -> :sswitch_1
        0x7f0d0030 -> :sswitch_2
    .end sparse-switch
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0d002e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->p:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/EditModeActivity;->p:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
