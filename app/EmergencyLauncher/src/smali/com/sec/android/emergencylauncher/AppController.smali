.class public Lcom/sec/android/emergencylauncher/AppController;
.super Landroid/app/Application;


# static fields
.field public static c:Ljava/lang/String;

.field static d:Z

.field public static e:I

.field public static f:Ljava/lang/String;

.field private static final h:Z

.field private static i:Lcom/sec/android/emergencylauncher/AppController;

.field private static k:Ljava/text/Collator;


# instance fields
.field public a:Ljava/util/concurrent/ConcurrentSkipListMap;

.field public b:Ljava/util/ArrayList;

.field g:Lcom/sec/android/emergencylauncher/e;

.field private j:Lcom/sec/android/emergencylauncher/a/c;

.field private l:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    const-string v0, "UPdateListData"

    sput-object v0, Lcom/sec/android/emergencylauncher/AppController;->c:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/emergencylauncher/AppController;->d:Z

    const-string v0, ""

    sput-object v0, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/sec/android/emergencylauncher/AppController;->k:Ljava/text/Collator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->j:Lcom/sec/android/emergencylauncher/a/c;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->g:Lcom/sec/android/emergencylauncher/e;

    sput-object p0, Lcom/sec/android/emergencylauncher/AppController;->i:Lcom/sec/android/emergencylauncher/AppController;

    return-void
.end method

.method public static a()Lcom/sec/android/emergencylauncher/AppController;
    .locals 1

    sget-object v0, Lcom/sec/android/emergencylauncher/AppController;->i:Lcom/sec/android/emergencylauncher/AppController;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method static synthetic f()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/sec/android/emergencylauncher/AppController;->k:Ljava/text/Collator;

    return-object v0
.end method

.method private g()V
    .locals 6

    const/16 v5, 0x230

    const/high16 v4, 0x430c0000    # 140.0f

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initBoundaty() : densityDpi="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v0, :sswitch_data_0

    sput v5, Lcom/sec/android/emergencylauncher/a/e;->x:I

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->y:F

    :goto_0
    return-void

    :sswitch_0
    sput v5, Lcom/sec/android/emergencylauncher/a/e;->x:I

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->y:F

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x15e

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->x:I

    const/high16 v0, 0x42dc0000    # 110.0f

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->y:F

    goto :goto_0

    :sswitch_2
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->x:I

    const/high16 v0, 0x42a00000    # 80.0f

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->y:F

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xf0 -> :sswitch_2
        0x140 -> :sswitch_1
        0x1e0 -> :sswitch_0
    .end sparse-switch
.end method

.method private h()V
    .locals 5

    const/4 v4, 0x6

    const/4 v3, 0x3

    const/4 v2, 0x1

    const-string v0, "JP"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "DCM"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "initRegion() : jpn-dcm"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput v2, Lcom/sec/android/emergencylauncher/a/e;->o:I

    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_4

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->m:I

    :cond_1
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fi_FI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->s:Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "si_IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->t:Z

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initRegion() : isUPSM = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    const-string v0, "KDI"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "initRegion() : jpn-kdi"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    sput-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->i:Z

    goto :goto_0

    :cond_3
    const-string v0, "XJP"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "initRegion() : jpn-xjp"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    goto :goto_0

    :cond_4
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->m:I

    const/high16 v0, 0x43480000    # 200.0f

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->y:F

    goto :goto_1

    :cond_5
    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "initRegion() : China"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->q:Z

    :cond_6
    :goto_2
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/emergencylauncher/a/e;->m:I

    goto/16 :goto_1

    :cond_7
    const-string v0, "USA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sput-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->r:Z

    const-string v0, "VZW"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "initRegion() : USA-VZW"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    goto :goto_2

    :cond_8
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v0, :cond_1

    sput v4, Lcom/sec/android/emergencylauncher/a/e;->m:I

    sput v3, Lcom/sec/android/emergencylauncher/a/e;->n:I

    goto/16 :goto_1
.end method


# virtual methods
.method a(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addAppToMap() : getClassName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/emergencymode/EmergencyManager;->addAppToLauncher(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "EmergencyLauncher.AppController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addAppToMap() : remove app result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public a(Lcom/sec/android/emergencylauncher/e;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/AppController;->g:Lcom/sec/android/emergencylauncher/e;

    return-void
.end method

.method b()V
    .locals 5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    if-eqz v0, :cond_1

    const-string v0, "com.android.chrome"

    sput-object v0, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    const-string v0, "com.google.android.apps.chrome.Main"

    sput-object v0, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    :goto_0
    new-instance v1, Landroid/content/ComponentName;

    sget-object v0, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/emergencylauncher/a/e;->w:Ljava/lang/String;

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init() : default browser name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/emergencylauncher/a/e;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "com.sec.android.app.sbrowser"

    sput-object v0, Lcom/sec/android/emergencylauncher/a/e;->u:Ljava/lang/String;

    const-string v0, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    sput-object v0, Lcom/sec/android/emergencylauncher/a/e;->v:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v2, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    if-eqz v2, :cond_0

    const-string v2, "EmergencyLauncher.AppController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package Name Not Found for component : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EmergencyLauncher.AppController"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method b(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removefromMap() : getClassName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/emergencymode/EmergencyManager;->addAppToLauncher(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "EmergencyLauncher.AppController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removefromMap() : add app result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method c()V
    .locals 2

    const-string v0, "EmergencyLauncher.AppController"

    const-string v1, "fillAllowedAppsMap()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/emergencylauncher/c;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/c;-><init>(Lcom/sec/android/emergencylauncher/AppController;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method d()V
    .locals 7

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->j:Lcom/sec/android/emergencylauncher/a/c;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/c;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/emergencylauncher/AppController;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fillOtherAppsList(), size = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->clear()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x200

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    const/16 v6, 0x220

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v4}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    const-string v3, "EmergencyLauncher.AppController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fillOtherAppsList() : package name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/a/d;->a(Lcom/sec/android/emergencylauncher/a/a;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "EmergencyLauncher.AppController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fillOtherAppsList() : put to mOtherAppsList. key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->g:Lcom/sec/android/emergencylauncher/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->g:Lcom/sec/android/emergencylauncher/e;

    invoke-interface {v0}, Lcom/sec/android/emergencylauncher/e;->a()V

    :cond_2
    return-void
.end method

.method public e()I
    .locals 7

    const/16 v1, 0xa0

    const/16 v0, 0x78

    const/16 v2, 0xf0

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const v5, 0x7f08000c

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const/high16 v6, 0x1050000

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    if-ne v5, v4, :cond_2

    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    :cond_1
    :goto_1
    move v2, v0

    goto :goto_0

    :cond_2
    int-to-float v5, v5

    int-to-float v4, v4

    div-float v4, v5, v4

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    if-le v3, v0, :cond_1

    if-gt v3, v1, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    if-gt v3, v2, :cond_4

    move v0, v2

    goto :goto_1

    :cond_4
    const/16 v0, 0x140

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    sget-boolean v1, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCurrentLanguage"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v1

    sput-object v1, Lcom/sec/android/emergencylauncher/AppController;->k:Ljava/text/Collator;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public onCreate()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    const-string v2, "EmergencyLauncher.AppController"

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    invoke-static {}, Lcom/sec/android/emergencymode/EmergencyManager;->supportGrayScreen()Z

    move-result v2

    if-eqz v2, :cond_0

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    :cond_0
    const-string v2, "EmergencyLauncher.AppController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() : isSupportmDNIe="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "ultra_powersaving_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    if-eqz v0, :cond_3

    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    sput v1, Lcom/sec/android/emergencylauncher/a/e;->c:I

    const-string v1, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() : isUPSM="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", modetype="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-static {}, Lcom/sec/android/emergencylauncher/a/c;->a()Lcom/sec/android/emergencylauncher/a/c;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->j:Lcom/sec/android/emergencylauncher/a/c;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->e()I

    move-result v1

    sput v1, Lcom/sec/android/emergencylauncher/AppController;->e:I

    new-instance v1, Ljava/util/concurrent/ConcurrentSkipListMap;

    new-instance v2, Lcom/sec/android/emergencylauncher/d;

    invoke-direct {v2}, Lcom/sec/android/emergencylauncher/d;-><init>()V

    invoke-direct {v1, v2}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/emergencylauncher/AppController;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AppController;->l:Landroid/content/pm/PackageManager;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/AppController;->g()V

    :cond_2
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/AppController;->h()V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AppController;->b()V

    return-void

    :cond_3
    const-string v2, "ro.product.device"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "m2alte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "ro.product.device"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "m2a3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    sput-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    const-string v1, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() : IS_TORCH_NOT_SUPPORT = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_1
    const-string v1, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() : isUPSM="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", modetype="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    sget-boolean v1, Lcom/sec/android/emergencylauncher/AppController;->h:Z

    if-eqz v1, :cond_5

    const-string v1, "EmergencyLauncher.AppController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() : device = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ro.product.device"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onTerminate()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;->clear()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AppController;->a:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method
