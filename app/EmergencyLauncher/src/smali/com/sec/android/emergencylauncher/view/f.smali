.class public Lcom/sec/android/emergencylauncher/view/f;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/view/EMGridView;


# direct methods
.method public constructor <init>(Lcom/sec/android/emergencylauncher/view/EMGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private a()Lcom/sec/android/emergencylauncher/a/a;
    .locals 7

    const v6, 0x7f020015

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/emergencylauncher/a/a;

    const-string v1, "Add"

    invoke-direct {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/content/ComponentName;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/emergencylauncher/a/a;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "EmergencyLauncher.EMGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEmptyApp() : is theme_zero = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v4}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f060000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_0

    const v2, 0x7f020016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    sget-boolean v2, Lcom/sec/android/emergencylauncher/a/e;->d:Z

    if-eqz v2, :cond_2

    const v2, 0x7f020014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 7

    const/16 v6, 0x11

    const/16 v1, 0x8

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const-string v0, " "

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setGravity(I)V

    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->b(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/b/a;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/b/a;->a(Landroid/content/ComponentName;)I

    move-result v0

    move v1, v0

    :goto_0
    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EmergencyLauncher.EMGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initBadge() : App Name is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Badge Count -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-lez v1, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    int-to-long v0, v1

    invoke-virtual {v2, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setGravity(I)V

    :cond_2
    return-void

    :cond_3
    const-string v0, "com.android.contacts"

    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.android.dialer.DialtactsActivity"

    invoke-direct {v0, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->b(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/b/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/b/a;->a(Landroid/content/ComponentName;)I

    move-result v0

    move v1, v0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 3

    const-string v0, "EmergencyLauncher.EMGridView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EMGridView size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v2}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const-string v0, "EmergencyLauncher.EMGridView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getView() : position"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v3, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v1, :cond_3

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "ColorBadgeBackground"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v1

    const-string v4, "ColorBadgeBackground"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    move-object p2, v2

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_29

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    move-object v4, v0

    :goto_1
    if-nez v4, :cond_5

    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EmergencyLauncher.EMGridView"

    const-string v1, "getView() : Info is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/view/f;->a()Lcom/sec/android/emergencylauncher/a/a;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/sec/android/emergencylauncher/view/f;->a(Landroid/widget/TextView;Lcom/sec/android/emergencylauncher/a/a;)V

    :goto_2
    return-object p2

    :cond_3
    sget-boolean v1, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v1, :cond_4

    const v1, 0x7f030004

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_4
    const v1, 0x7f030002

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f0d0007

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0d0008

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0d000b

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/emergencylauncher/b/c;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_3
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_23

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v3, :cond_10

    if-nez p1, :cond_e

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v6, "com.android.dialer.DialtactsActivity"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    const v3, 0x7f0a000d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    :goto_4
    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "EmergencyLauncher.EMGridView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView() : Info is not null, position : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", title.getText() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", info.getAppName() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", info.getFixed() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "EmergencyLauncher.EMGridView"

    const-string v3, "getView() : title.getText() is null. perform setContentDescription() as add"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const v1, 0x7f0a000b

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_9
    :goto_5
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->d(Lcom/sec/android/emergencylauncher/view/EMGridView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.emergencylauncher.EditModeActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v0

    if-nez v0, :cond_a

    const v0, 0x7f0d0009

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/f;->a:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/view/EMGridView;->c(Lcom/sec/android/emergencylauncher/view/EMGridView;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_26

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_a
    :goto_6
    new-instance v0, Lcom/sec/android/emergencylauncher/view/g;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/view/g;-><init>(Lcom/sec/android/emergencylauncher/view/f;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_7
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_b
    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v3, :cond_d

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->m:I

    sget v6, Lcom/sec/android/emergencylauncher/a/e;->k:I

    sget v7, Lcom/sec/android/emergencylauncher/a/e;->l:I

    sub-int/2addr v6, v7

    sub-int/2addr v3, v6

    if-lt p1, v3, :cond_c

    const-string v3, "EmergencyLauncher.EMGridView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView() : skip setImage, pos = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x8

    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_e
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v6, 0x2

    if-eq v3, v6, :cond_f

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->d()Ljava/lang/String;

    move-result-object v3

    const-string v6, "com.android.mms.ui.ConversationComposer"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const v3, 0x7f0a003c

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    :cond_f
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_10
    sget-boolean v3, Lcom/sec/android/emergencylauncher/a/e;->p:Z

    if-eqz v3, :cond_13

    const v3, 0x7f0a000f

    :goto_8
    sget v6, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-nez v6, :cond_19

    sget-boolean v6, Lcom/sec/android/emergencylauncher/a/e;->e:Z

    if-eqz v6, :cond_16

    const/4 v6, 0x1

    if-ne p1, v6, :cond_14

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_11
    :goto_9
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v6, 0x1

    if-eq v3, v6, :cond_12

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_6

    :cond_12
    if-nez p1, :cond_21

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->f:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_1f

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "en_US"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Torch "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_13
    const v3, 0x7f0a000e

    goto :goto_8

    :cond_14
    const/4 v3, 0x2

    if-ne p1, v3, :cond_15

    const v3, 0x7f0a000d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :cond_15
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    :cond_16
    if-nez p1, :cond_17

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :cond_17
    const/4 v3, 0x1

    if-ne p1, v3, :cond_18

    const v3, 0x7f0a000d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :cond_18
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    :cond_19
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_1c

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1a

    const v3, 0x7f0a000d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :cond_1a
    const/4 v3, 0x3

    if-ne p1, v3, :cond_1b

    const v3, 0x7f0a000e

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_9

    :cond_1b
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_1c
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->o:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_11

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1d

    const v3, 0x7f0a000d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_9

    :cond_1d
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_1e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0a0027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_1f
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "en_US"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Torch "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0032

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_20
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0a0027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0032

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_21
    const/4 v3, 0x1

    if-ne p1, v3, :cond_6

    sget v3, Lcom/sec/android/emergencylauncher/a/e;->g:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0a0030

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_22
    sget v3, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-nez v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0a0030

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v6, 0x7f0a0032

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_23
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_24
    const v1, 0x7f0a000b

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/sec/android/emergencylauncher/view/EMGridView;->a()Z

    move-result v1

    if-eqz v1, :cond_25

    const-string v1, "EmergencyLauncher.EMGridView"

    const-string v3, "getView() : new app is added to Add btn"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_25
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    goto/16 :goto_5

    :cond_26
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    goto/16 :goto_6

    :cond_27
    invoke-virtual {v4}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v0

    if-nez v0, :cond_28

    const v0, 0x7f0d0009

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0, v2, v4}, Lcom/sec/android/emergencylauncher/view/f;->a(Landroid/widget/TextView;Lcom/sec/android/emergencylauncher/a/a;)V

    goto/16 :goto_7

    :cond_28
    invoke-direct {p0, v2, v4}, Lcom/sec/android/emergencylauncher/view/f;->a(Landroid/widget/TextView;Lcom/sec/android/emergencylauncher/a/a;)V

    goto/16 :goto_7

    :cond_29
    move-object v4, v0

    goto/16 :goto_1
.end method
