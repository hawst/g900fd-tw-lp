.class public Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;
.super Landroid/app/Activity;


# static fields
.field private static final e:Z


# instance fields
.field a:Lcom/sec/android/emergencylauncher/b;

.field b:Landroid/widget/AdapterView$OnItemClickListener;

.field private c:Landroid/content/Context;

.field private d:Landroid/widget/ListView;

.field private f:Landroid/app/ActionBar;

.field private g:Z

.field private h:Lcom/sec/android/emergencylauncher/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->a:Z

    sput-boolean v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->g:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->h:Lcom/sec/android/emergencylauncher/a/c;

    new-instance v0, Lcom/sec/android/emergencylauncher/a;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencylauncher/a;-><init>(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->b:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a(Lcom/sec/android/emergencylauncher/a/a;)V

    return-void
.end method

.method private a(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 3

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "addNewAppToAppScreen()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->c:Landroid/content/Context;

    :cond_0
    invoke-static {p1}, Lcom/sec/android/emergencylauncher/a/d;->a(Lcom/sec/android/emergencylauncher/a/a;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->b(Lcom/sec/android/emergencylauncher/a/a;)V

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v0

    sget v1, Lcom/sec/android/emergencylauncher/a/e;->k:I

    sget v2, Lcom/sec/android/emergencylauncher/a/e;->l:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/sec/android/emergencylauncher/a/a;->a(I)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->h:Lcom/sec/android/emergencylauncher/a/c;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->c:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/emergencylauncher/a/c;->a(Lcom/sec/android/emergencylauncher/a/a;Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->e:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->g:Z

    return p1
.end method

.method private b()V
    .locals 2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "populateData()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    if-nez v0, :cond_1

    const v0, 0x7f0d0001

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->b:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private b(Lcom/sec/android/emergencylauncher/a/a;)V
    .locals 3

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "updateDefaultApps()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x200

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/emergencylauncher/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Lcom/sec/android/emergencylauncher/AppController;->a()Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/emergencylauncher/a/a;->a()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->c:Landroid/content/Context;

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->setContentView(I)V

    const v0, 0x7f0d0001

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    new-instance v0, Lcom/sec/android/emergencylauncher/b;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencylauncher/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    const v0, 0x7f0d0002

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->b()V

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->f:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->f:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f020000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    invoke-static {}, Lcom/sec/android/emergencylauncher/a/c;->a()Lcom/sec/android/emergencylauncher/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->h:Lcom/sec/android/emergencylauncher/a/c;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/b;->a()V

    :cond_0
    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->d:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->a:Lcom/sec/android/emergencylauncher/b;

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    sget-boolean v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "onOptionsItemSelected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->e:Z

    if-eqz v0, :cond_1

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "onOptionsItemSelected() : id.home"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencylauncher/AllowedAppsListActivity;->onBackPressed()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "EmergencyLauncher.AllowedAppsListActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
