.class Lcom/sec/android/emergencylauncher/q;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "reason"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "EmergencyLauncher.LauncherActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive() : reason = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onReceive() : notification cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencylauncher/a/e;->b:Z

    if-nez v0, :cond_1

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->o:I

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_6

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->e()Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_7

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->e()Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->e(Lcom/sec/android/emergencylauncher/LauncherActivity;)Lcom/sec/android/emergencylauncher/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/b/b;->a()V

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->g(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->f(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/telephony/PhoneStateListener;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    sget v0, Lcom/sec/android/emergencylauncher/a/e;->f:I

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->h(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    :cond_3
    sget v0, Lcom/sec/android/emergencylauncher/a/e;->g:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/LauncherActivity;->i(Lcom/sec/android/emergencylauncher/LauncherActivity;)V

    :cond_4
    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "mute"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/q;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/LauncherActivity;->c(Lcom/sec/android/emergencylauncher/LauncherActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_5
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EmergencyLauncher.LauncherActivity"

    const-string v2, "onReceive() :  unregisterReceiver(mBatteryBroadcastReceiver) failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_6
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onReceive() : mBatteryBroadcastReceiver is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "EmergencyLauncher.LauncherActivity"

    const-string v2, "onReceive() :  unregisterReceiver(mEmergencyModeReceiver) failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_7
    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "onReceive() : mEmergencyModeReceiver is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
