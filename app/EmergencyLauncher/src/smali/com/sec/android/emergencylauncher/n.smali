.class Lcom/sec/android/emergencylauncher/n;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/LauncherActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/n;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-static {}, Lcom/sec/android/emergencylauncher/LauncherActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.LauncherActivity"

    const-string v1, "mAppsloaderHandler message recieved now invalidating the view"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/n;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/n;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->c:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/n;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/n;->a:Lcom/sec/android/emergencylauncher/LauncherActivity;

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/LauncherActivity;->b:Lcom/sec/android/emergencylauncher/view/EMGridView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/EMGridView;->invalidate()V

    :cond_2
    return-void
.end method
