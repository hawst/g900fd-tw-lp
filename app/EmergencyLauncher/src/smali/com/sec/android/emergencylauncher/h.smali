.class Lcom/sec/android/emergencylauncher/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/EditModeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencylauncher/EditModeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->f()Landroid/content/ComponentName;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v0, p3}, Lcom/sec/android/emergencylauncher/EditModeActivity;->b(Lcom/sec/android/emergencylauncher/EditModeActivity;I)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->c(Lcom/sec/android/emergencylauncher/EditModeActivity;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/emergencylauncher/AppController;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencylauncher/a/a;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/a/a;->g()I

    move-result v2

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->d(Lcom/sec/android/emergencylauncher/EditModeActivity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v2, v3, :cond_2

    invoke-static {}, Lcom/sec/android/emergencylauncher/EditModeActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EmergencyLauncher.EditModeActivity"

    const-string v1, "onItemClick() : fixed, so return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->c(Lcom/sec/android/emergencylauncher/EditModeActivity;)Lcom/sec/android/emergencylauncher/AppController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/emergencylauncher/AppController;->a(Lcom/sec/android/emergencylauncher/a/a;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    iget-object v2, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v2}, Lcom/sec/android/emergencylauncher/EditModeActivity;->e(Lcom/sec/android/emergencylauncher/EditModeActivity;)I

    move-result v2

    invoke-static {v1, v2, v4, v4}, Lcom/sec/android/emergencylauncher/EditModeActivity;->a(Lcom/sec/android/emergencylauncher/EditModeActivity;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    iget-object v1, v1, Lcom/sec/android/emergencylauncher/EditModeActivity;->b:Lcom/sec/android/emergencylauncher/view/f;

    invoke-virtual {v1}, Lcom/sec/android/emergencylauncher/view/f;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    iget-boolean v1, v1, Lcom/sec/android/emergencylauncher/EditModeActivity;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->f(Lcom/sec/android/emergencylauncher/EditModeActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v1}, Lcom/sec/android/emergencylauncher/EditModeActivity;->f(Lcom/sec/android/emergencylauncher/EditModeActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g(Lcom/sec/android/emergencylauncher/EditModeActivity;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/h;->a:Lcom/sec/android/emergencylauncher/EditModeActivity;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/EditModeActivity;->g(Lcom/sec/android/emergencylauncher/EditModeActivity;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
