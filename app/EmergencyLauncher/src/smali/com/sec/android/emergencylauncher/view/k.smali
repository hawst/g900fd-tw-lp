.class Lcom/sec/android/emergencylauncher/view/k;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/emergencylauncher/view/k;->a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;Lcom/sec/android/emergencylauncher/view/j;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/emergencylauncher/view/k;-><init>(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x42c80000    # 100.0f

    const-string v1, "level"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const-string v1, "scale"

    const/16 v2, 0x64

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/sec/android/emergencylauncher/view/k;->a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;

    invoke-static {v1, v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;I)I

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/k;->a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;

    invoke-static {v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/k;->a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->a(Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;I)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencylauncher/view/k;->a:Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;

    invoke-virtual {v0}, Lcom/sec/android/emergencylauncher/view/UpsmUsageTimeView;->invalidate()V

    :cond_1
    return-void
.end method
