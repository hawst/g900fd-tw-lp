.class Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;
.super Ljava/lang/Object;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LastContentUriCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 4317
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4318
    return-void
.end method


# virtual methods
.method public getAllText()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 4322
    const-string v5, ""

    .line 4323
    .local v5, "result":Ljava/lang/String;
    const/4 v2, 0x0

    .line 4324
    .local v2, "index":I
    const/4 v4, 0x0

    .line 4325
    .local v4, "line":I
    const/4 v0, 0x0

    .line 4326
    .local v0, "block":I
    sget v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-lez v6, :cond_4

    .line 4327
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v4, v6, v7

    .line 4328
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    aget v0, v6, v7

    .line 4330
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-lt v1, v6, :cond_1

    .line 4355
    .end local v1    # "i":I
    :cond_0
    return-object v5

    .line 4331
    .restart local v1    # "i":I
    :cond_1
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    aget v6, v6, v1

    if-eq v6, v0, :cond_3

    .line 4332
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4333
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    aget v0, v6, v1

    .line 4339
    :cond_2
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4330
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4334
    :cond_3
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v6, v6, v1

    if-eq v6, v4, :cond_2

    .line 4335
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4336
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v4, v6, v1

    goto :goto_1

    .line 4341
    .end local v1    # "i":I
    :cond_4
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    if-eqz v6, :cond_0

    .line 4342
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    array-length v6, v6

    if-ge v1, v6, :cond_0

    .line 4343
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    aget v6, v6, v1

    if-lt v3, v6, :cond_6

    .line 4349
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    aget v6, v6, v1

    add-int/2addr v2, v6

    .line 4350
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 4351
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4342
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4344
    :cond_6
    if-eqz v3, :cond_7

    .line 4345
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4347
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    add-int v8, v3, v2

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 4343
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public onCompleted()V
    .locals 2

    .prologue
    .line 4359
    const-string v0, "OCR"

    const-string v1, "onCompleted()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4361
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_1

    .line 4362
    :cond_0
    const-string v0, "OCR"

    const-string v1, "[OCR] onCompleted: mCameraEnine null, mOCR null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4380
    :cond_1
    return-void
.end method
