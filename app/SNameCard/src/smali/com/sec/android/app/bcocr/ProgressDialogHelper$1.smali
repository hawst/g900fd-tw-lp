.class Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;
.super Ljava/lang/Object;
.source "ProgressDialogHelper.java"

# interfaces
.implements Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/ProgressDialogHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    const/4 v3, 0x2

    .line 42
    const-string v0, "ProgressDialogHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "progress : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$0(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-static {v0, p1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$1(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$2(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$2(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$2(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getMax()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$3(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V

    .line 54
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$2(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-static {v0, p1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$1(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V

    goto :goto_0

    .line 51
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    # getter for: Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I
    invoke-static {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$4(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;->this$0:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-static {v0, v3}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->access$3(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V

    goto :goto_1
.end method
