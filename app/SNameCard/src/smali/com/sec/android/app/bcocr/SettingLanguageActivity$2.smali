.class Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;
.super Ljava/lang/Object;
.source "SettingLanguageActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/SettingLanguageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x1

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    .line 112
    .local v0, "ischecked":Z
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    .line 114
    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I
    invoke-static {}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$4()I

    move-result v2

    if-gt v2, v1, :cond_1

    if-nez v0, :cond_1

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v2

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v2, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 126
    :goto_0
    return-void

    .line 117
    :cond_1
    if-eqz v0, :cond_2

    .line 118
    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I
    invoke-static {}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$4()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$5(I)V

    .line 123
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$6(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)[Z

    move-result-object v1

    aput-boolean v0, v1, p3

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # invokes: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->setOCREngineLanguage()V
    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$7(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V

    goto :goto_0

    .line 120
    :cond_2
    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I
    invoke-static {}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$4()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$5(I)V

    goto :goto_1
.end method
