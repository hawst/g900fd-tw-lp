.class public Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;
.super Lcom/sec/android/app/bcocr/command/MenuCommand;
.source "LaunchZoomMenuCommand.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LaunchZoomMenuCommand"


# instance fields
.field private mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

.field private mId:I

.field private mMenuDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

.field private mParentView:Landroid/view/ViewGroup;

.field private mSlideDirection:I


# direct methods
.method public constructor <init>(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p3, "parentView"    # Landroid/view/ViewGroup;
    .param p4, "menuDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/command/MenuCommand;-><init>()V

    .line 33
    const/16 v0, 0x63

    iput v0, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mId:I

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mSlideDirection:I

    .line 40
    iput p1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mId:I

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mParentView:Landroid/view/ViewGroup;

    .line 43
    iput-object p4, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mMenuDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 44
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 7

    .prologue
    .line 52
    iget-object v1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isCapturing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    const-string v1, "LaunchZoomMenuCommand"

    const-string v2, "return isCapturing.."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mMenuDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    .line 59
    .local v0, "menu":Lcom/sec/android/app/bcocr/widget/TwZoomMenu;
    if-nez v0, :cond_2

    .line 60
    new-instance v0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    .end local v0    # "menu":Lcom/sec/android/app/bcocr/widget/TwZoomMenu;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const v2, 0x7f03000d

    iget-object v3, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mParentView:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mMenuDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    iget v5, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mZOrder:I

    iget v6, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mSlideDirection:I

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;II)V

    .line 61
    .restart local v0    # "menu":Lcom/sec/android/app/bcocr/widget/TwZoomMenu;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mMenuDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_2
    iget v1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mZOrder:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setZorder(I)V

    .line 66
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->isActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->showMenu()V

    goto :goto_0
.end method

.method public setSliderDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;->mSlideDirection:I

    .line 48
    return-void
.end method
