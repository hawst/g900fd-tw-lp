.class Lcom/sec/android/app/bcocr/PostViewActivity$1;
.super Ljava/lang/Object;
.source "PostViewActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/PostViewActivity;->getOCRWholeDataFromImageAsync(Ljava/lang/String;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/PostViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/PostViewActivity$1;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    .line 1245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity$1;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity$1;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mStrRecogThreadParam:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$12()Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIntRecogThreadParam:Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$13()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->getOCRWholeDataFromImage(Ljava/lang/String;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$14(Lcom/sec/android/app/bcocr/PostViewActivity;I)V

    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity$1;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$15(Lcom/sec/android/app/bcocr/PostViewActivity;Z)V

    .line 1250
    const-string v0, "PostViewActivity"

    const-string v1, "recognitionThread [---]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity$1;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->hideCaptureProgressTimer()V
    invoke-static {v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$16(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    .line 1253
    invoke-static {v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$17(Ljava/lang/String;)V

    .line 1254
    invoke-static {v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$18(Landroid/content/Intent;)V

    .line 1255
    return-void
.end method
