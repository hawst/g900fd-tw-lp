.class public Lcom/sec/android/app/bcocr/OCRExifAppender;
.super Ljava/lang/Object;
.source "OCRExifAppender.java"


# static fields
.field private static final OCR_STRINGDATA_KEYCODE:Ljava/lang/String; = "OCRDATA"

.field private static final OCR_STRINGDATA_TOKEN_SEPEARTOR:Ljava/lang/String; = "^#%"

.field private static final TAG:Ljava/lang/String; = "EditExifForJpeg"

.field private static mContentName:Ljava/lang/String;

.field private static mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

.field private static mOCRData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    sput-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    .line 16
    sput-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    .line 17
    sput-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sput-object p1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public append(I[Ljava/lang/String;)V
    .locals 3
    .param p1, "count"    # I
    .param p2, "data"    # [Ljava/lang/String;

    .prologue
    .line 78
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    if-nez v1, :cond_1

    .line 79
    const-string v1, "EditExifForJpeg"

    const-string v2, "ExifParser is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 83
    const-string v1, "EditExifForJpeg"

    const-string v2, "ContentName is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_4

    .line 87
    :cond_3
    const-string v1, "EditExifForJpeg"

    const-string v2, "The parameter is wrong"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "^#%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 91
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_1
    if-ge v0, p1, :cond_0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "^#%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    if-eqz v0, :cond_0

    .line 32
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    invoke-virtual {v0}, Lcom/sec/android/lib/exifparser/ExifParser;->close()V

    .line 34
    :cond_0
    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    .line 35
    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    .line 36
    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/lib/exifparser/ExifParser;->setApp5Segment(Ljava/lang/String;Z)Z

    .line 72
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    invoke-virtual {v0}, Lcom/sec/android/lib/exifparser/ExifParser;->close()V

    .line 73
    sput-object v3, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    .line 74
    sput-object v3, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public ready()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 40
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41
    const-string v1, "EditExifForJpeg"

    const-string v2, "ContentName is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :goto_0
    return v0

    .line 45
    :cond_0
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    if-eqz v1, :cond_1

    .line 46
    const-string v1, "EditExifForJpeg"

    const-string v2, "ExifParser is not null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    invoke-virtual {v1}, Lcom/sec/android/lib/exifparser/ExifParser;->close()V

    .line 48
    sput-object v3, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    goto :goto_0

    .line 51
    :cond_1
    new-instance v1, Lcom/sec/android/lib/exifparser/ExifParser;

    invoke-direct {v1}, Lcom/sec/android/lib/exifparser/ExifParser;-><init>()V

    sput-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    .line 53
    sget-object v1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mExifParser:Lcom/sec/android/lib/exifparser/ExifParser;

    sget-object v2, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/lib/exifparser/ExifParser;->open(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 54
    const-string v0, "EditExifForJpeg"

    const-string v1, "be ready"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 61
    sput-object v3, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 63
    :cond_2
    sget-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "OCRDATA"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/OCRExifAppender;->mOCRData:Ljava/lang/String;

    .line 64
    const/4 v0, 0x1

    goto :goto_0

    .line 56
    :cond_3
    const-string v1, "EditExifForJpeg"

    const-string v2, "Open is fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setContentName(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentName"    # Ljava/lang/String;

    .prologue
    .line 24
    if-nez p1, :cond_0

    .line 25
    const-string v0, "EditExifForJpeg"

    const-string v1, "contentName is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    :cond_0
    sput-object p1, Lcom/sec/android/app/bcocr/OCRExifAppender;->mContentName:Ljava/lang/String;

    .line 28
    return-void
.end method
