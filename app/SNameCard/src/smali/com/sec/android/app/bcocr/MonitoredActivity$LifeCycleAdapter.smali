.class public Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleAdapter;
.super Ljava/lang/Object;
.source "MonitoredActivity.java"

# interfaces
.implements Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/MonitoredActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LifeCycleAdapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 39
    return-void
.end method

.method public onActivityDestroyed(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 42
    return-void
.end method

.method public onActivityStarted(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 45
    return-void
.end method

.method public onActivityStopped(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 48
    return-void
.end method
