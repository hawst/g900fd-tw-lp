.class public Lcom/sec/android/app/bcocr/PostViewActivity;
.super Landroid/app/Activity;
.source "PostViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;
    }
.end annotation


# static fields
.field private static final FINISH_NAMECARD_CAPTURE_MODE:I = 0x3

.field public static final GET_RESULT_ERROR:I = 0x2

.field public static final GET_RESULT_ERROR_FORMAT:I = 0x4

.field public static final GET_RESULT_ERROR_SIZE:I = 0x3

.field public static final GET_RESULT_NO_TEXT:I = 0x1

.field public static final GET_RESULT_OK:I = 0x0

.field public static final IMAGE_DECODE_SCALEDOWN_NUMBER:I = 0x1

.field public static final KEY_POSTVIEW_RESULT:Ljava/lang/String; = "postview_result"

.field public static final KEY_SETTING_AUTO_CAPTURE_SWITCH:Ljava/lang/String; = "setting_image_auto_capture"

.field public static final KEY_SETTING_AUTO_SAVE_CB_DIRECTLINK:Ljava/lang/String; = "setting_auto_save_cb_directlink"

.field public static final KEY_SETTING_AUTO_SAVE_CB_TEXTSEARCH:Ljava/lang/String; = "setting_auto_save_cb_textsearch"

.field public static final KEY_SETTING_AUTO_SAVE_CB_TRANSLATOR:Ljava/lang/String; = "setting_auto_save_cb_translator"

.field public static final KEY_SETTING_AUTO_SAVE_SWITCH:Ljava/lang/String; = "setting_image_auto_save"

.field public static final KEY_SETTING_NOISE_EDUCTION:Ljava/lang/String; = "setting_noise_reduction"

.field private static final START_CAPTURE_PROGRESS_TIMER_EXPIRED:I = 0x1

.field private static final STOP_CAPTURE_PROGRESS_TIMER_EXPIRED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PostViewActivity"

.field private static mContentResolver:Landroid/content/ContentResolver;

.field private static mIntRecogThreadParam:Landroid/content/Intent;

.field private static mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

.field private static mOCREngineLanguageSelectedSet:[I

.field protected static mPostViewContextNum:I

.field private static mRecognitionThread:Ljava/lang/Thread;

.field private static mStrRecogThreadParam:Ljava/lang/String;

.field public static nLoadImageOrginalResizedRatio:F


# instance fields
.field public final CROP_MODE_GET_TEXT:I

.field public final CROP_MODE_NONE:I

.field public final CROP_MODE_SENTENCE_SELECT:I

.field public final CROP_MODE_TRANSLATE:I

.field public final FALSE:I

.field public final MODE_FUNC_ALL:I

.field public final MODE_FUNC_DETECTTEXT:I

.field public final MODE_FUNC_DIRECTLINK:I

.field public final MODE_FUNC_NONE:I

.field public final MODE_FUNC_SIPOCR:I

.field public final MODE_FUNC_TEXTSEARCH:I

.field public final MODE_FUNC_TRANSLATOR:I

.field public final OCR_SIP_TYPE_EMAIL:I

.field public final OCR_SIP_TYPE_LINK:I

.field public final OCR_SIP_TYPE_MULTITEXT:I

.field public final OCR_SIP_TYPE_PHONENUM:I

.field public final OCR_SIP_TYPE_URL:I

.field public final OPTION_CANCEL:I

.field public final OPTION_DIC_MANAGEMENT:I

.field public final OPTION_DONE:I

.field public final OPTION_GET_TEXT:I

.field public final OPTION_HELP:I

.field public final OPTION_HISTORY:I

.field public final OPTION_LINKING_SETTING:I

.field public final OPTION_LOADIMAGE:I

.field public final OPTION_RECOG_LANGUAGE:I

.field public final OPTION_ROTATE_LEFT:I

.field public final OPTION_ROTATE_RIGHT:I

.field public final OPTION_SAVE:I

.field public final OPTION_SAVE_TO_CONTACT:I

.field public final OPTION_SHARE:I

.field public final OPTION_TRANSLATE_SELECT_TEXT:I

.field public final OPTION_TRANSLATE_TO_SENTENCE:I

.field public final OPTION_TRANSLATE_TO_WORD:I

.field public final TRANSLATOR_SENTENCE_MODE:I

.field public final TRANSLATOR_WORD_MODE:I

.field public final TRUE:I

.field public final WORD_TYPE_ADDRESS:I

.field public final WORD_TYPE_CITY:I

.field public final WORD_TYPE_COMPANY:I

.field public final WORD_TYPE_COUNTRY:I

.field public final WORD_TYPE_DEPARTMENT:I

.field public final WORD_TYPE_EMAIL:I

.field public final WORD_TYPE_FAX:I

.field public final WORD_TYPE_FIRSTNAME:I

.field public final WORD_TYPE_LASTNAME:I

.field public final WORD_TYPE_MOBILE:I

.field public final WORD_TYPE_NAME:I

.field public final WORD_TYPE_NORMAL:I

.field public final WORD_TYPE_NUMBER:I

.field public final WORD_TYPE_POSTCODE:I

.field public final WORD_TYPE_STREET:I

.field public final WORD_TYPE_TITLE:I

.field public final WORD_TYPE_URL:I

.field private mActionBar:Landroid/app/ActionBar;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCallFromUnknownSource:Z

.field public mCallFromWhere:I

.field public mCropBottom:I

.field public mCropLeft:I

.field public mCropRight:I

.field public mCropTop:I

.field mDirectLinkView:Landroid/view/View;

.field private mDropBoxDirectory:Ljava/lang/String;

.field private mImageOrientation:I

.field private mImageUri:Landroid/net/Uri;

.field private mImageView:Lcom/sec/android/app/bcocr/PostImageView;

.field public mIsImageRotatedByOption:Z

.field private mIsLoadImageMode:Ljava/lang/Boolean;

.field public mIsLocalFilePath:Z

.field private mIsNameCardCaptureMode:Ljava/lang/Boolean;

.field private mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;

.field private mIsNameCardGetDataMode:Ljava/lang/Boolean;

.field private mIsScreenCropMode:I

.field private mLastOrientation:I

.field private mOCR:Lcom/dmc/ocr/SecMOCR;

.field private mOCREnginelanguageSelectedNum:I

.field public mOCRSIPType:I

.field private mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

.field private mOldBitmapRect:Landroid/graphics/RectF;

.field private mOldScale:F

.field private mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

.field private mRcSelectedArea:Landroid/graphics/Rect;

.field private mRecogProgressingState:Z

.field private mRecognitionThreadIsWaiting:Z

.field private mResultOfGetDataFromImage:I

.field private mSavetoContactIntent:Landroid/content/Intent;

.field public nBCardRect:[I

.field public nBitmpaMaxSize:I

.field public nCapturedFilePath:Ljava/lang/String;

.field public nCapturedFilePath_for_contact:Ljava/lang/String;

.field public nCapturedImageHeight:I

.field public nCapturedImageWidth:I

.field public nDetectedQRCode:Z

.field public nDispImgHeightLand:I

.field public nDispImgHeightPort:I

.field public nDispImgWidthLand:I

.field public nDispImgWidthPort:I

.field public nFoundWordText:[Ljava/lang/String;

.field public nFoundWordType:[I

.field public nGetText_DirectLinkArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public nGetText_FoundWordText:[Ljava/lang/String;

.field public nGetText_FoundWordType:[I

.field public nGetText_NumberOfFoundLink:I

.field public nGetText_SelectedLangSet:[I

.field public nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

.field public nIDByTime:J

.field public nLanguageSelectorRect:Landroid/graphics/Rect;

.field public nLookUpWordRect:[Landroid/graphics/Rect;

.field public nLookUpWords:[Ljava/lang/String;

.field public nNumberOfFoundLink:I

.field public nNumberOfLookUpWord:I

.field public nNumberOfWholeOrinWord:I

.field public nNumberOfWholeWord:I

.field public nNumberofFoundLink_SIPType:I

.field public nOriginalSentence:Ljava/lang/String;

.field public nPostViewMode:I

.field public nQRCodeRect:Landroid/graphics/Rect;

.field public nQRCodeText:Ljava/lang/String;

.field public nQRCodeType:I

.field public nSelectedAreaHeight:I

.field public nSelectedAreaRect:Landroid/graphics/Rect;

.field public nSelectedAreaWidth:I

.field public nSelectedRecogWordRect:[Landroid/graphics/Rect;

.field public nTranslatedSentence:Ljava/lang/String;

.field public nWholeDisplayHeight:I

.field public nWholeDisplayWidth:I

.field public nWholeOrinWordBlockIndex:[I

.field public nWholeOrinWordLineIndex:[I

.field public nWholeOrinWordRect:[Landroid/graphics/Rect;

.field public nWholeWordInSpcial:[Z

.field public nWholeWordPerLine:[I

.field public nWholeWordRect:[Landroid/graphics/Rect;

.field public nWholeWordType:[I

.field public nWoleOrinWordText:[Ljava/lang/String;

.field public nWoleWordText:[Ljava/lang/String;

.field public nXLeftExtra:I

.field public nXRightExtra:I

.field public nYBottomExtra:I

.field public nYTopExtra:I

.field private selectedAreaArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    .line 203
    const/16 v0, 0x1b

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    .line 231
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nLoadImageOrginalResizedRatio:F

    .line 232
    sput-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    .line 236
    sput-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    .line 237
    sput-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mStrRecogThreadParam:Ljava/lang/String;

    .line 238
    sput-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mIntRecogThreadParam:Landroid/content/Intent;

    .line 326
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 60
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_DIRECTLINK:I

    .line 61
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_TRANSLATOR:I

    .line 62
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_DETECTTEXT:I

    .line 63
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_TEXTSEARCH:I

    .line 64
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_SIPOCR:I

    .line 65
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_ALL:I

    .line 66
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->MODE_FUNC_NONE:I

    .line 68
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OCR_SIP_TYPE_LINK:I

    .line 69
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OCR_SIP_TYPE_EMAIL:I

    .line 70
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OCR_SIP_TYPE_PHONENUM:I

    .line 71
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OCR_SIP_TYPE_URL:I

    .line 72
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OCR_SIP_TYPE_MULTITEXT:I

    .line 74
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_NAME:I

    .line 75
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_FIRSTNAME:I

    .line 76
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_LASTNAME:I

    .line 77
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_TITLE:I

    .line 78
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_DEPARTMENT:I

    .line 79
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_COMPANY:I

    .line 80
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_ADDRESS:I

    .line 81
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_POSTCODE:I

    .line 82
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_COUNTRY:I

    .line 83
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_CITY:I

    .line 84
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_STREET:I

    .line 85
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_NUMBER:I

    .line 86
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_FAX:I

    .line 87
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_MOBILE:I

    .line 88
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_URL:I

    .line 89
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_EMAIL:I

    .line 90
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->WORD_TYPE_NORMAL:I

    .line 93
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_CANCEL:I

    .line 94
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_SAVE_TO_CONTACT:I

    .line 95
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_TRANSLATE_TO_WORD:I

    .line 96
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_TRANSLATE_TO_SENTENCE:I

    .line 97
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_GET_TEXT:I

    .line 98
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_DONE:I

    .line 99
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_SHARE:I

    .line 100
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_TRANSLATE_SELECT_TEXT:I

    .line 103
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_ROTATE_LEFT:I

    .line 104
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_ROTATE_RIGHT:I

    .line 105
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_LOADIMAGE:I

    .line 106
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_HISTORY:I

    .line 107
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_RECOG_LANGUAGE:I

    .line 108
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_DIC_MANAGEMENT:I

    .line 109
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_LINKING_SETTING:I

    .line 110
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_SAVE:I

    .line 111
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->OPTION_HELP:I

    .line 113
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->CROP_MODE_NONE:I

    .line 114
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->CROP_MODE_GET_TEXT:I

    .line 115
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->CROP_MODE_TRANSLATE:I

    .line 116
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->CROP_MODE_SENTENCE_SELECT:I

    .line 118
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->TRANSLATOR_WORD_MODE:I

    .line 119
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->TRANSLATOR_SENTENCE_MODE:I

    .line 121
    iput v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->TRUE:I

    .line 122
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->FALSE:I

    .line 143
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    .line 144
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    .line 145
    const/16 v0, 0x3c0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    .line 146
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    .line 147
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayWidth:I

    .line 148
    const/16 v0, 0x780

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayHeight:I

    .line 149
    const/16 v0, 0x1000

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nBitmpaMaxSize:I

    .line 150
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 151
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 152
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 153
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    .line 156
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSIPType:I

    .line 158
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    .line 159
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    .line 160
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    .line 161
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfLookUpWord:I

    .line 178
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    .line 185
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeType:I

    .line 186
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeText:Ljava/lang/String;

    .line 187
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeRect:Landroid/graphics/Rect;

    .line 188
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDetectedQRCode:Z

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    .line 191
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsImageRotatedByOption:Z

    .line 194
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nIDByTime:J

    .line 195
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberofFoundLink_SIPType:I

    .line 198
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromWhere:I

    .line 201
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 205
    const/16 v0, 0x3c0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    .line 206
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    .line 207
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    .line 208
    const/16 v0, 0x3c0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    .line 211
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    .line 213
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0xd2

    const/16 v2, 0x42e

    const/16 v3, 0x280

    invoke-direct {v0, v1, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    .line 216
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsScreenCropMode:I

    .line 217
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 219
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCropLeft:I

    .line 220
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCropTop:I

    .line 221
    const/16 v0, 0x3bf

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCropRight:I

    .line 222
    const/16 v0, 0x2cf

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCropBottom:I

    .line 225
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldScale:F

    .line 226
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x4433c000    # 719.0f

    const v4, 0x446fc000    # 959.0f

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldBitmapRect:Landroid/graphics/RectF;

    .line 227
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    .line 228
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    .line 229
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    .line 230
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;

    .line 235
    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I

    .line 239
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecogProgressingState:Z

    .line 244
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 250
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    .line 252
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mDropBoxDirectory:Ljava/lang/String;

    .line 254
    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mLastOrientation:I

    .line 258
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThreadIsWaiting:Z

    .line 259
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    .line 50
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$10()Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/bcocr/PostViewActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$12()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mStrRecogThreadParam:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIntRecogThreadParam:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/bcocr/PostViewActivity;I)V
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/bcocr/PostViewActivity;Z)V
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecogProgressingState:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 1742
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->hideCaptureProgressTimer()V

    return-void
.end method

.method static synthetic access$17(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    sput-object p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mStrRecogThreadParam:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$18(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 238
    sput-object p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIntRecogThreadParam:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/PostViewActivity;)I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 1750
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->onCreateErrorMsgDialog()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 1767
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->onCreateErrorMsgDialogUnsupportedImageFormat()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setImageAndDisplayWH()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 753
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->initPostView()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/app/bcocr/PostViewActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 849
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/PostViewActivity;->setImageViewOnOrientation(Ljava/lang/String;)V

    return-void
.end method

.method private deleteFile(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 2505
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2506
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2507
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCAN"

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2508
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR]file delete Success "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2513
    :cond_0
    :goto_0
    return-void

    .line 2510
    :cond_1
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR]file delete Fail "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private destroyRecogData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1809
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    .line 1810
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nLookUpWordRect:[Landroid/graphics/Rect;

    .line 1811
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    .line 1812
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordRect:[Landroid/graphics/Rect;

    .line 1814
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordPerLine:[I

    .line 1815
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordType:[I

    .line 1816
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    .line 1818
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    .line 1820
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordLineIndex:[I

    .line 1821
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordBlockIndex:[I

    .line 1823
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    .line 1824
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleOrinWordText:[Ljava/lang/String;

    .line 1825
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    .line 1826
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nLookUpWords:[Ljava/lang/String;

    .line 1828
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeText:Ljava/lang/String;

    .line 1829
    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeRect:Landroid/graphics/Rect;

    .line 1830
    return-void
.end method

.method private getAddToContactTempFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 2516
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2517
    const-string v2, ".NameCardTemp.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2516
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getBCRTempFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 2521
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/.OCRTemp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2522
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "OCRTemp.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2521
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getCroppedImage(Lcom/sec/android/app/bcocr/DecodeImageUtils;II)V
    .locals 12
    .param p1, "decodeImage"    # Lcom/sec/android/app/bcocr/DecodeImageUtils;
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I

    .prologue
    .line 1262
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/dmc/ocr/SecMOCR;->MOCR_EnhanceCardImage([III)I

    .line 1263
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetBCardRect([III)[I

    .line 1265
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v1

    .line 1266
    const/4 v2, 0x0

    .line 1265
    invoke-static {v1, p2, p3, v2}, Lcom/dmc/ocr/SecMOCR;->MOCR_TrimCardImage([III[I)[I

    move-result-object v0

    .line 1268
    .local v0, "tmp":[I
    if-eqz v0, :cond_0

    .line 1269
    :try_start_0
    const-string v9, ""

    .line 1270
    .local v9, "thumbnailDirectory":Ljava/lang/String;
    const-string v10, ""

    .line 1271
    .local v10, "thumbnailFileName":Ljava/lang/String;
    const-string v11, ""

    .line 1272
    .local v11, "thumbnailPath":Ljava/lang/String;
    const/4 v1, 0x2

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x0

    aget v3, v0, v3

    const/4 v4, 0x1

    aget v4, v0, v4

    .line 1273
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1272
    invoke-static/range {v0 .. v5}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1274
    .local v6, "bmp":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v1

    if-nez v1, :cond_1

    .line 1275
    sget-object v9, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 1279
    :goto_0
    const-string v10, ".NameCardCropTemp.jpg"

    .line 1280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1281
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1282
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v11}, Lcom/sec/android/app/bcocr/OCRUtils;->addNewFileToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 1283
    iput-object v11, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 1284
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 1285
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1286
    .local v8, "intent":Landroid/content/Intent;
    const-string v1, "FILE_PATH"

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    invoke-virtual {v8, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1287
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v8}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1294
    .end local v6    # "bmp":Landroid/graphics/Bitmap;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "thumbnailDirectory":Ljava/lang/String;
    .end local v10    # "thumbnailFileName":Ljava/lang/String;
    .end local v11    # "thumbnailPath":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1277
    .restart local v6    # "bmp":Landroid/graphics/Bitmap;
    .restart local v9    # "thumbnailDirectory":Ljava/lang/String;
    .restart local v10    # "thumbnailFileName":Ljava/lang/String;
    .restart local v11    # "thumbnailPath":Ljava/lang/String;
    :cond_1
    sget-object v9, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_MMC:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1289
    .end local v6    # "bmp":Landroid/graphics/Bitmap;
    .end local v9    # "thumbnailDirectory":Ljava/lang/String;
    .end local v10    # "thumbnailFileName":Ljava/lang/String;
    .end local v11    # "thumbnailPath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1290
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 1291
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(I)V

    goto :goto_1
.end method

.method private getOCRWholeDataFromImageAsync(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2
    .param p1, "aGalleryFilePath"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1238
    sput-object p1, Lcom/sec/android/app/bcocr/PostViewActivity;->mStrRecogThreadParam:Ljava/lang/String;

    .line 1239
    sput-object p2, Lcom/sec/android/app/bcocr/PostViewActivity;->mIntRecogThreadParam:Landroid/content/Intent;

    .line 1240
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I

    .line 1242
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->showCaptureProgressTimer()V

    .line 1243
    const-string v0, "PostViewActivity"

    const-string v1, "recognitionThread [+++]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/bcocr/PostViewActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/PostViewActivity$1;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    .line 1257
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    const-string v1, "recognizeTextThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1258
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1259
    return-void
.end method

.method private handleIntentExtra(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 772
    const-string v0, "PostViewActivity"

    const-string v1, "#### handleIntent Extra #S"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    const-string v0, "MODE"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 774
    const-string v0, "SIP_TYPE"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSIPType:I

    .line 775
    const-string v0, "FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 776
    const-string v0, "SELECTED_RECOG_LANG_LIST"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    .line 778
    const-string v0, "PostViewActivity"

    const-string v1, "#### handleIntent Extra #E"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    return-void
.end method

.method private hideCaptureProgressTimer()V
    .locals 4

    .prologue
    .line 1743
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1744
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1745
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    if-eqz v1, :cond_0

    .line 1746
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1748
    :cond_0
    return-void
.end method

.method private initPostView()V
    .locals 3

    .prologue
    .line 754
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initPostView , mPostViewMode :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , mOCRSIPType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSIPType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    const v0, 0x7f0f0015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/PostImageView;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/PostImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 758
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 759
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 761
    :cond_0
    return-void
.end method

.method private onCreateErrorMsgDialog()V
    .locals 4

    .prologue
    .line 1753
    const v1, 0x7f0c002c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1755
    .local v0, "title_popup":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1756
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1757
    const v2, 0x7f0c0020

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1758
    const v2, 0x7f0c001b

    new-instance v3, Lcom/sec/android/app/bcocr/PostViewActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/PostViewActivity$2;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1764
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1765
    return-void
.end method

.method private onCreateErrorMsgDialogUnsupportedImageFormat()V
    .locals 4

    .prologue
    .line 1770
    const v1, 0x7f0c002c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1772
    .local v0, "title_popup":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1773
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1774
    const v2, 0x7f0c0021

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1775
    const v2, 0x7f0c001b

    new-instance v3, Lcom/sec/android/app/bcocr/PostViewActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/PostViewActivity$3;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1781
    new-instance v2, Lcom/sec/android/app/bcocr/PostViewActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/PostViewActivity$4;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1792
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1793
    return-void
.end method

.method private setImageAndDisplayWH()V
    .locals 13

    .prologue
    const/16 v10, 0x3c0

    const/16 v9, 0x2d0

    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v12, 0x0

    .line 523
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 524
    .local v2, "outSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 525
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 526
    iget v3, v2, Landroid/graphics/Point;->y:I

    .line 527
    .local v3, "windowHeight":I
    iget v4, v2, Landroid/graphics/Point;->x:I

    .line 528
    .local v4, "windowWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090041

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 530
    .local v0, "actionbarHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090041

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 536
    :goto_0
    iput v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayWidth:I

    .line 537
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayHeight:I

    .line 539
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    .line 540
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    if-eq v5, v7, :cond_0

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    if-ne v5, v8, :cond_5

    .line 541
    :cond_0
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    .line 542
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    .line 550
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_2

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    if-eq v5, v7, :cond_3

    :cond_2
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    if-eq v5, v8, :cond_3

    .line 551
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {p0, v12, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isFitImageHeight(II)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 552
    :cond_3
    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    .line 553
    iput v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 556
    sub-int v5, v3, v0

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    .line 557
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    if-eqz v5, :cond_6

    .line 558
    add-int/lit16 v5, v4, -0xa0

    int-to-double v6, v5

    sub-int v5, v3, v0

    int-to-double v8, v5

    int-to-double v10, v3

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    .line 565
    :goto_2
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    int-to-double v6, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-double v8, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-double v10, v5

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    .line 566
    sub-int v5, v3, v0

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    .line 568
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 569
    sub-int v5, v3, v0

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 570
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 571
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    sub-int v5, v4, v5

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 572
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    .line 605
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->calculateLoadImageDisplayWH()V

    .line 607
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 608
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldBitmapRect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 612
    :goto_4
    return-void

    .line 533
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090042

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    goto/16 :goto_0

    .line 544
    :cond_5
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    .line 545
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    goto/16 :goto_1

    .line 560
    :cond_6
    int-to-double v6, v4

    sub-int v5, v3, v0

    int-to-double v8, v5

    int-to-double v10, v3

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    goto/16 :goto_2

    .line 563
    :cond_7
    sub-int v5, v3, v0

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    goto/16 :goto_2

    .line 574
    :cond_8
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 575
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 576
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    sub-int v5, v4, v5

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 577
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto :goto_3

    .line 580
    :cond_9
    iput v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    .line 581
    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    .line 583
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 584
    sub-int v5, v3, v0

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    .line 585
    iput v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    .line 590
    :goto_5
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    int-to-double v6, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-double v8, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-double v10, v5

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    .line 591
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    int-to-double v6, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    int-to-double v8, v5

    int-to-double v10, v4

    div-double/2addr v8, v10

    div-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    .line 593
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 594
    sub-int v5, v3, v0

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 595
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 596
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 597
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto/16 :goto_3

    .line 587
    :cond_a
    sub-int v5, v3, v0

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    goto :goto_5

    .line 599
    :cond_b
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 600
    iput v12, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 601
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    sub-int v5, v4, v5

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 602
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto/16 :goto_3

    .line 610
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldBitmapRect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_4
.end method

.method private setImageView(Ljava/lang/String;)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 865
    const-string v6, "PostViewActivity"

    const-string v7, "setImageView #S"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    if-eqz v6, :cond_8

    .line 870
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_1

    .line 871
    iget-boolean v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v6, :cond_3

    .line 872
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 873
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v6, 0x1

    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 874
    invoke-static {p1, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 875
    const/4 v6, 0x0

    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 876
    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .local v5, "photoWidth":I
    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 878
    .local v4, "photoHeight":I
    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nBitmpaMaxSize:I

    if-gt v5, v6, :cond_0

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nBitmpaMaxSize:I

    if-le v4, v6, :cond_2

    .line 879
    :cond_0
    const/4 v6, 0x2

    iput v6, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 883
    :goto_0
    const/4 v6, 0x1

    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 885
    invoke-static {p1, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 925
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "photoHeight":I
    .end local v5    # "photoWidth":I
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_7

    .line 926
    const-string v6, "PostViewActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "rotationTest: mBitmap.getWidth(), Bitmap.getHeight()= "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 927
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , mImageOrientation : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 926
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/app/bcocr/PostImageView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    .line 944
    :goto_2
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 945
    const-string v6, "PostViewActivity"

    const-string v7, "setImageView #E"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    return-void

    .line 881
    .restart local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v4    # "photoHeight":I
    .restart local v5    # "photoWidth":I
    :cond_2
    const/4 v6, 0x1

    :try_start_1
    iput v6, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 890
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "photoHeight":I
    .end local v5    # "photoWidth":I
    :catch_0
    move-exception v0

    .line 891
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] IOException !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 887
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    invoke-static {v6, v7}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 892
    :catch_1
    move-exception v0

    .line 893
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 894
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] Out of memory 1st !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_4

    .line 897
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 898
    iput-object v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 900
    :cond_4
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 903
    :try_start_3
    iget-boolean v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v6, :cond_5

    .line 904
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 905
    .local v2, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v6, 0x1

    iput v6, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 906
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v6, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 908
    invoke-static {p1, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 912
    .end local v2    # "opt":Landroid/graphics/BitmapFactory$Options;
    :catch_2
    move-exception v1

    .line 913
    .local v1, "e2":Ljava/io/IOException;
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] IOException !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 910
    .end local v1    # "e2":Ljava/io/IOException;
    :cond_5
    :try_start_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    invoke-static {v6, v7}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 914
    :catch_3
    move-exception v1

    .line 915
    .local v1, "e2":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 916
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] Out of memory 2nd !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_6

    .line 918
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 919
    iput-object v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 921
    :cond_6
    invoke-static {}, Ljava/lang/System;->gc()V

    goto/16 :goto_1

    .line 938
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .end local v1    # "e2":Ljava/lang/OutOfMemoryError;
    :cond_7
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] Error, mBitmap == null !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 941
    :cond_8
    const-string v6, "PostViewActivity"

    const-string v7, "[setImageView()] Error, mImageView == null !"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private setImageViewOnOrientation(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 853
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 855
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 857
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/PostViewActivity;->setImageView(Ljava/lang/String;)V

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    if-eqz v0, :cond_1

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/PostImageView;->resetZoom()V

    .line 862
    :cond_1
    return-void
.end method

.method private showCaptureProgressTimer()V
    .locals 4

    .prologue
    .line 1735
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1736
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1737
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    if-eqz v1, :cond_0

    .line 1738
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1740
    :cond_0
    return-void
.end method


# virtual methods
.method public ReplySIPOCRResult(ILjava/lang/String;)V
    .locals 2
    .param p1, "DLPopupType"    # I
    .param p2, "DLPopupStr"    # Ljava/lang/String;

    .prologue
    .line 1095
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1096
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1097
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1098
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    .line 1099
    return-void
.end method

.method public calculateDisplayHeightRate()F
    .locals 2

    .prologue
    .line 979
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 980
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 982
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public calculateDisplayWidthRate()F
    .locals 3

    .prologue
    .line 970
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 971
    iget v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 975
    .local v0, "width":F
    :goto_0
    return v0

    .line 973
    .end local v0    # "width":F
    :cond_0
    iget v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .restart local v0    # "width":F
    goto :goto_0
.end method

.method public calculateLoadImageDisplayWH()V
    .locals 14

    .prologue
    const v10, 0x7f090041

    const/high16 v13, 0x40400000    # 3.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 615
    iget-object v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-nez v9, :cond_1

    sget-boolean v9, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    if-nez v9, :cond_1

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 619
    .local v6, "outSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    .line 620
    .local v5, "display":Landroid/view/Display;
    invoke-virtual {v5, v6}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 621
    iget v7, v6, Landroid/graphics/Point;->y:I

    .line 622
    .local v7, "windowHeight":I
    iget v8, v6, Landroid/graphics/Point;->x:I

    .line 623
    .local v8, "windowWidth":I
    const/4 v3, 0x0

    .line 624
    .local v3, "dispWidthRatio":F
    const/4 v1, 0x0

    .line 625
    .local v1, "dispHeightRatio":F
    const/4 v4, 0x0

    .line 626
    .local v4, "dispWidthScale":F
    const/4 v2, 0x0

    .line 627
    .local v2, "dispHeightScale":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v0, v9

    .line 629
    .local v0, "actionbarHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v0, v9

    .line 635
    :goto_1
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v9, v9

    int-to-float v10, v8

    div-float v3, v9, v10

    .line 636
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v9, v9

    sub-int v10, v7, v0

    int-to-float v10, v10

    div-float v1, v9, v10

    .line 637
    int-to-float v9, v8

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v10, v10

    div-float v4, v9, v10

    .line 638
    sub-int v9, v7, v0

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v10, v10

    div-float v2, v9, v10

    .line 639
    cmpl-float v9, v4, v13

    if-lez v9, :cond_2

    .line 640
    const/high16 v4, 0x40400000    # 3.0f

    .line 642
    :cond_2
    cmpl-float v9, v2, v13

    if-lez v9, :cond_3

    .line 643
    const/high16 v2, 0x40400000    # 3.0f

    .line 645
    :cond_3
    cmpl-float v9, v4, v2

    if-lez v9, :cond_8

    .line 646
    move v4, v2

    .line 651
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-nez v9, :cond_4

    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    const/4 v10, 0x6

    if-eq v9, v10, :cond_5

    .line 652
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {p0, v11, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isFitImageHeight(II)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 653
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 654
    cmpl-float v9, v1, v12

    if-lez v9, :cond_6

    .line 655
    sub-int v9, v7, v0

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    .line 656
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v9, v9

    div-float/2addr v9, v1

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    .line 657
    iput v11, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 658
    iput v11, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 659
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    sub-int v9, v8, v9

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 660
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    .line 678
    :cond_6
    :goto_3
    cmpg-float v9, v3, v12

    if-gez v9, :cond_0

    cmpg-float v9, v1, v12

    if-gez v9, :cond_0

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 680
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    .line 681
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v2

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    .line 682
    sub-int v9, v7, v0

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 683
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 684
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    sub-int v9, v8, v9

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 685
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto/16 :goto_0

    .line 632
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090042

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v0, v9

    goto/16 :goto_1

    .line 648
    :cond_8
    move v2, v4

    goto/16 :goto_2

    .line 665
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v9

    if-nez v9, :cond_6

    .line 667
    cmpl-float v9, v3, v12

    if-ltz v9, :cond_6

    .line 668
    iput v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    .line 669
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v9, v9

    div-float/2addr v9, v3

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    .line 670
    sub-int v9, v7, v0

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 671
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 672
    iput v11, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 673
    iput v11, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto :goto_3

    .line 687
    :cond_a
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    .line 688
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v2

    float-to-int v9, v9

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    .line 689
    sub-int v9, v7, v0

    iget v10, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    .line 690
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    .line 691
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    sub-int v9, v8, v9

    div-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    .line 692
    iget v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    goto/16 :goto_0
.end method

.method public compareSIPTypetoFoundQRType(II)Z
    .locals 6
    .param p1, "OCRType"    # I
    .param p2, "aQRType"    # I

    .prologue
    const/16 v5, 0xf

    const/16 v4, 0xe

    const/16 v3, 0xb

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 818
    if-ne p1, v0, :cond_2

    .line 819
    if-ne p2, v5, :cond_1

    .line 845
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 822
    goto :goto_0

    .line 824
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 825
    if-eq p2, v3, :cond_0

    move v0, v1

    .line 828
    goto :goto_0

    .line 830
    :cond_3
    const/4 v2, 0x3

    if-ne p1, v2, :cond_4

    .line 831
    if-eq p2, v4, :cond_0

    move v0, v1

    .line 834
    goto :goto_0

    .line 836
    :cond_4
    if-nez p1, :cond_5

    .line 837
    if-eq p2, v5, :cond_0

    .line 838
    if-eq p2, v3, :cond_0

    .line 839
    if-eq p2, v4, :cond_0

    move v0, v1

    .line 842
    goto :goto_0

    :cond_5
    move v0, v1

    .line 845
    goto :goto_0
.end method

.method public compareSIPTypetoFoundWordType(II)Z
    .locals 6
    .param p1, "OCRType"    # I
    .param p2, "aBCRType"    # I

    .prologue
    const/16 v5, 0xd

    const/16 v4, 0xc

    const/16 v3, 0xb

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 782
    if-ne p1, v0, :cond_2

    .line 783
    const/16 v2, 0xf

    if-ne p2, v2, :cond_1

    .line 813
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 786
    goto :goto_0

    .line 788
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 789
    if-eq p2, v5, :cond_0

    .line 790
    if-eq p2, v3, :cond_0

    .line 791
    if-eq p2, v4, :cond_0

    move v0, v1

    .line 794
    goto :goto_0

    .line 796
    :cond_3
    const/4 v2, 0x3

    if-ne p1, v2, :cond_4

    .line 797
    const/16 v2, 0xe

    if-eq p2, v2, :cond_0

    move v0, v1

    .line 800
    goto :goto_0

    .line 802
    :cond_4
    if-nez p1, :cond_5

    .line 803
    if-eq p2, v5, :cond_0

    .line 804
    if-eq p2, v3, :cond_0

    .line 805
    if-eq p2, v4, :cond_0

    .line 806
    const/16 v2, 0xf

    if-eq p2, v2, :cond_0

    .line 807
    const/16 v2, 0xe

    if-eq p2, v2, :cond_0

    move v0, v1

    .line 810
    goto :goto_0

    :cond_5
    move v0, v1

    .line 813
    goto :goto_0
.end method

.method public convertOrientation(I)I
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 1837
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 1838
    const/16 v0, 0x5a

    .line 1844
    :goto_0
    return v0

    .line 1839
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 1840
    const/16 v0, 0xb4

    goto :goto_0

    .line 1841
    :cond_1
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 1842
    const/16 v0, 0x10e

    goto :goto_0

    .line 1844
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageOrientation()I
    .locals 1

    .prologue
    .line 1118
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    return v0
.end method

.method public getImageViewRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    if-nez v0, :cond_0

    .line 1111
    const/4 v0, 0x0

    .line 1114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/PostImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getIsLoadImageMode()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getIsNameCardCaptureMode()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1849
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getIsScreenCropMode()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 764
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsScreenCropMode:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public getIsSentenseCropMode()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 768
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsScreenCropMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;
    .locals 1

    .prologue
    .line 1833
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    return-object v0
.end method

.method public declared-synchronized getOCRWholeDataFromImage(Ljava/lang/String;Landroid/content/Intent;)I
    .locals 35
    .param p1, "aGalleryFilePath"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1297
    monitor-enter p0

    const/16 v30, 0x1

    .line 1298
    .local v30, "result":Z
    const/4 v3, 0x0

    .line 1299
    .local v3, "imageWidth":I
    const/4 v4, 0x0

    .line 1301
    .local v4, "imageHeight":I
    :try_start_0
    const-string v2, "PostViewActivity"

    const-string v6, "getOCRWholeDataFromImage"

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeType:I

    .line 1305
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeText:Ljava/lang/String;

    .line 1306
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeRect:Landroid/graphics/Rect;

    .line 1308
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    .line 1309
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    .line 1310
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    .line 1312
    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v2, :cond_0

    .line 1313
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1316
    :cond_0
    if-nez p1, :cond_1

    .line 1317
    const/4 v2, 0x2

    .line 1656
    :goto_0
    monitor-exit p0

    return v2

    .line 1321
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v2, :cond_3

    .line 1322
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/bcocr/OCRUtils;->checkImageSize(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1323
    const/4 v2, 0x3

    goto :goto_0

    .line 1326
    :cond_2
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1327
    .local v21, "file":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1328
    const/4 v2, 0x2

    goto :goto_0

    .line 1331
    .end local v21    # "file":Ljava/io/File;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/sec/android/app/bcocr/OCRUtils;->checkImageSize(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1332
    const/4 v2, 0x3

    goto :goto_0

    .line 1337
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v2, :cond_6

    .line 1338
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/bcocr/OCRUtils;->checkImageFormat(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1339
    const/4 v2, 0x4

    goto :goto_0

    .line 1342
    :cond_5
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1343
    .restart local v21    # "file":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1344
    const/4 v2, 0x2

    goto :goto_0

    .line 1347
    .end local v21    # "file":Ljava/io/File;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/sec/android/app/bcocr/OCRUtils;->checkImageFormat(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1348
    const/4 v2, 0x4

    goto :goto_0

    .line 1353
    :cond_7
    new-instance v18, Lcom/sec/android/app/bcocr/DecodeImageUtils;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;-><init>()V

    .line 1354
    .local v18, "decodeImage":Lcom/sec/android/app/bcocr/DecodeImageUtils;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v2, :cond_8

    .line 1355
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->decodeFileToBitmap(Ljava/lang/String;)Z

    move-result v30

    .line 1360
    :goto_1
    if-nez v30, :cond_9

    .line 1361
    const-string v2, "PostViewActivity"

    const-string v6, "decode image fail!"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1363
    const/16 v18, 0x0

    .line 1364
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 1357
    :cond_8
    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v6}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->decodeURIToBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v30

    goto :goto_1

    .line 1367
    :cond_9
    const/16 v29, 0x0

    .line 1368
    .local v29, "orient":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    if-eqz v2, :cond_a

    .line 1369
    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImageOrientation(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v29

    .line 1372
    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->convertOrientation(I)I

    move-result v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->setRotation(I)V

    .line 1373
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->rotateBitmap()Z

    move-result v30

    .line 1375
    if-eqz v30, :cond_b

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_c

    .line 1376
    :cond_b
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1377
    const/16 v18, 0x0

    .line 1378
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 1381
    :cond_c
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1382
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1383
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    .line 1384
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    .line 1385
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getResizedRatio()F

    move-result v2

    sput v2, Lcom/sec/android/app/bcocr/PostViewActivity;->nLoadImageOrginalResizedRatio:F

    .line 1386
    const-string v2, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "nLoadImageOrginalResizedRatio : "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v8, Lcom/sec/android/app/bcocr/PostViewActivity;->nLoadImageOrginalResizedRatio:F

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    invoke-static {v3, v4}, Lcom/dmc/ocr/SecMOCR;->isSupported(II)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1389
    const-string v2, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Image size("

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " x "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") is bigger than "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v8, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v8, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    const v15, 0xf4240

    div-int/2addr v8, v15

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ")M"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1390
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1391
    const/16 v18, 0x0

    .line 1392
    const/4 v2, 0x3

    goto/16 :goto_0

    .line 1395
    :cond_d
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->convertRotateBitmapToArray()Z

    move-result v30

    .line 1397
    if-eqz v30, :cond_e

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v2

    if-nez v2, :cond_f

    .line 1398
    :cond_e
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1399
    const/16 v18, 0x0

    .line 1400
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 1403
    :cond_f
    const-string v2, "PostViewActivity"

    const-string v6, "[getOCRWholeDataFromImage()] decoding success "

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1406
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getInstance()Lcom/dmc/ocr/SecMOCR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 1408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v2, :cond_10

    .line 1409
    const-string v2, "PostViewActivity"

    const-string v6, "[getOCRWholeDataFromImage()] mOCR is null"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1410
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1411
    const/16 v18, 0x0

    .line 1412
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 1415
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1416
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->getCroppedImage(Lcom/sec/android/app/bcocr/DecodeImageUtils;II)V

    .line 1417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 1418
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1420
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->loadCurrentOCREngineLanguageByTransDic()V

    .line 1421
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 1422
    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    .line 1423
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->resetRecogResult()V

    .line 1424
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setRecognitionMode(I)V

    .line 1425
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c0014

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setDataBasePath(Ljava/lang/String;)V

    .line 1426
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c0014

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setLicenseBasePath(Ljava/lang/String;)V

    .line 1427
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    add-int/lit8 v7, v3, -0x1

    add-int/lit8 v8, v4, -0x1

    invoke-static/range {v2 .. v8}, Lcom/dmc/ocr/SecMOCR;->setJPEGData([IIIIIII)Z

    .line 1429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/dmc/ocr/SecMOCR;->startRecognition(Landroid/content/Context;)V

    .line 1430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->getRecogResult()I

    .line 1431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->getWholeRecogResult()I

    .line 1432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 1435
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDetectedQRCode:Z

    .line 1437
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    .line 1438
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    .line 1439
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordPerLine:[I

    .line 1440
    sget v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    .line 1441
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    .line 1442
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordType:[I

    .line 1443
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordInSpecial:[Z

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    .line 1445
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleOrinWordText:[Ljava/lang/String;

    .line 1446
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordLineIndex:[I

    .line 1447
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordBlockIndex:[I

    .line 1449
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    .line 1450
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    .line 1452
    sget v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfLookUpWord:I

    .line 1454
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    if-eqz v2, :cond_12

    .line 1455
    const-string v2, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "WholeNum:"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "  SpecialWordNum:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1456
    const-string v8, " mOCR.mLinePerWordNum:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1455
    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1459
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    if-eqz v2, :cond_13

    .line 1460
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, 0x1

    if-ne v2, v6, :cond_18

    .line 1461
    :cond_13
    const/16 v28, 0x0

    .line 1462
    .local v28, "numOfSamsung":I
    const/16 v25, 0x0

    .line 1464
    .local v25, "i":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    if-eqz v2, :cond_1e

    .line 1465
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    new-array v2, v2, [Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    .line 1466
    const/16 v25, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    move/from16 v0, v25

    if-lt v0, v2, :cond_1d

    .line 1473
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    if-eqz v2, :cond_20

    .line 1474
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    new-array v2, v2, [Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordRect:[Landroid/graphics/Rect;

    .line 1476
    const/16 v25, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeOrinWord:I

    move/from16 v0, v25

    if-lt v0, v2, :cond_1f

    .line 1483
    :goto_5
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CHECK_SAMSUNG_TEXT:Z

    if-eqz v2, :cond_14

    .line 1484
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, 0x4

    if-eq v2, v6, :cond_14

    .line 1485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    if-eqz v2, :cond_14

    .line 1486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    array-length v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    array-length v6, v6

    if-ne v2, v6, :cond_14

    .line 1487
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    array-length v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    array-length v6, v6

    if-ne v2, v6, :cond_14

    .line 1488
    const/16 v25, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    array-length v2, v2

    move/from16 v0, v25

    if-lt v0, v2, :cond_21

    .line 1495
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    add-int v2, v2, v28

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    .line 1498
    :cond_14
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    if-eqz v2, :cond_26

    .line 1499
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    new-array v2, v2, [Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    .line 1500
    const/16 v25, 0x0

    .line 1501
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_15

    .line 1502
    const/16 v25, 0x0

    :goto_7
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    array-length v2, v2

    move/from16 v0, v25

    if-lt v0, v2, :cond_23

    .line 1506
    :cond_15
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CHECK_SAMSUNG_TEXT:Z

    if-eqz v2, :cond_16

    .line 1507
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, 0x4

    if-eq v2, v6, :cond_16

    .line 1508
    if-lez v28, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 1509
    const/16 v27, 0x0

    .local v27, "j":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    array-length v2, v2

    move/from16 v0, v27

    if-ge v0, v2, :cond_16

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    move/from16 v0, v25

    if-lt v0, v2, :cond_24

    .line 1522
    .end local v27    # "j":I
    :cond_16
    :goto_9
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CHECK_SAMSUNG_TEXT:Z

    if-eqz v2, :cond_18

    .line 1523
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, 0x4

    if-eq v2, v6, :cond_18

    .line 1524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    array-length v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    array-length v6, v6

    if-ne v2, v6, :cond_18

    .line 1525
    if-lez v28, :cond_18

    .line 1528
    const/16 v27, 0x0

    .line 1529
    .restart local v27    # "j":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    new-array v0, v2, [I

    move-object/from16 v32, v0

    .line 1530
    .local v32, "tempType":[I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v31, v0

    .line 1532
    .local v31, "tempText":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    array-length v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    array-length v6, v6

    if-ne v2, v6, :cond_17

    .line 1533
    const/16 v27, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    array-length v2, v2

    move/from16 v0, v27

    if-lt v0, v2, :cond_27

    .line 1539
    :cond_17
    :goto_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    move/from16 v0, v27

    if-lt v0, v2, :cond_28

    .line 1544
    move-object/from16 v0, v32

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    .line 1545
    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    .line 1550
    .end local v25    # "i":I
    .end local v27    # "j":I
    .end local v28    # "numOfSamsung":I
    .end local v31    # "tempText":[Ljava/lang/String;
    .end local v32    # "tempType":[I
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_29

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    if-lez v2, :cond_29

    const/16 v26, 0x1

    .line 1551
    .local v26, "isRequestFromOCR":Z
    :goto_c
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->isValidSavetoContact(Z)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1552
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getAddToContactTempFile()Ljava/io/File;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->deleteFile(Ljava/io/File;)V

    .line 1555
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImage()[I

    move-result-object v2

    .line 1556
    const/4 v6, 0x0

    .line 1555
    invoke-static {v2, v3, v4, v6}, Lcom/dmc/ocr/SecMOCR;->MOCR_TrimCardImage([III[I)[I

    move-result-object v5

    .line 1557
    .local v5, "tmp":[I
    move v7, v3

    .line 1558
    .local v7, "croppedWidth":I
    move v9, v4

    .line 1560
    .local v9, "croppedHeight":I
    if-eqz v5, :cond_2a

    .line 1561
    const/4 v2, 0x0

    aget v7, v5, v2

    .line 1562
    const/4 v2, 0x1

    aget v9, v5, v2

    .line 1563
    invoke-static {v5, v7, v9}, Lcom/dmc/ocr/SecMOCR;->MOCR_EnhanceCardImage([III)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1569
    :goto_d
    :try_start_2
    const-string v10, ""

    .line 1570
    .local v10, "directory":Ljava/lang/String;
    const-string v11, ""

    .line 1571
    .local v11, "fileName":Ljava/lang/String;
    const-string v22, ""

    .line 1572
    .local v22, "fullPath":Ljava/lang/String;
    const/16 v23, 0x0

    .line 1574
    .local v23, "fullpathStream":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v2

    if-nez v2, :cond_2b

    .line 1575
    sget-object v10, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 1579
    :goto_e
    const-string v11, ".NameCardTemp.jpg"

    .line 1580
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1581
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1583
    .local v19, "dir":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_19

    .line 1584
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdir()Z

    .line 1587
    :cond_19
    if-eqz v5, :cond_2d

    .line 1588
    const/4 v6, 0x2

    .line 1589
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .end local v10    # "directory":Ljava/lang/String;
    move v8, v7

    .line 1588
    invoke-static/range {v5 .. v10}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    .line 1591
    .local v16, "bmp":Landroid/graphics/Bitmap;
    :try_start_3
    new-instance v24, Ljava/io/FileOutputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1592
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    .local v24, "fullpathStream":Ljava/io/FileOutputStream;
    :try_start_4
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1593
    const-string v2, "PostViewActivity"

    const-string v6, "Could not compress the bitmap to JPEG format"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1595
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/sec/android/app/bcocr/OCRUtils;->addNewFileToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 1596
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1600
    :try_start_5
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1602
    if-eqz v24, :cond_31

    .line 1603
    :try_start_6
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->flush()V

    .line 1604
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1605
    const/16 v23, 0x0

    .line 1639
    .end local v16    # "bmp":Landroid/graphics/Bitmap;
    .end local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :cond_1b
    :goto_f
    :try_start_7
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath_for_contact:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1646
    .end local v5    # "tmp":[I
    .end local v7    # "croppedWidth":I
    .end local v9    # "croppedHeight":I
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v19    # "dir":Ljava/io/File;
    .end local v22    # "fullPath":Ljava/lang/String;
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :cond_1c
    :goto_10
    :try_start_8
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1647
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 1648
    const/16 v18, 0x0

    .line 1649
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1652
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    if-nez v2, :cond_30

    .line 1653
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1467
    .end local v26    # "isRequestFromOCR":Z
    .restart local v25    # "i":I
    .restart local v28    # "numOfSamsung":I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    new-instance v6, Landroid/graphics/Rect;

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v25

    invoke-direct {v6, v8}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v6, v2, v25

    .line 1466
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_2

    .line 1470
    :cond_1e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 1297
    .end local v18    # "decodeImage":Lcom/sec/android/app/bcocr/DecodeImageUtils;
    .end local v25    # "i":I
    .end local v28    # "numOfSamsung":I
    .end local v29    # "orient":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1477
    .restart local v18    # "decodeImage":Lcom/sec/android/app/bcocr/DecodeImageUtils;
    .restart local v25    # "i":I
    .restart local v28    # "numOfSamsung":I
    .restart local v29    # "orient":I
    :cond_1f
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordRect:[Landroid/graphics/Rect;

    new-instance v6, Landroid/graphics/Rect;

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v25

    invoke-direct {v6, v8}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v6, v2, v25

    .line 1476
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_4

    .line 1480
    :cond_20
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeOrinWordRect:[Landroid/graphics/Rect;

    goto/16 :goto_5

    .line 1489
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    aget-boolean v2, v2, v25

    if-nez v2, :cond_22

    .line 1490
    const-string v2, "SAMSUNG"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    aget-object v6, v6, v25

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1491
    add-int/lit8 v28, v28, 0x1

    .line 1492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordInSpcial:[Z

    const/4 v6, 0x1

    aput-boolean v6, v2, v25

    .line 1488
    :cond_22
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_6

    .line 1503
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    new-instance v6, Landroid/graphics/Rect;

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v25

    invoke-direct {v6, v8}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v6, v2, v25

    .line 1502
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_7

    .line 1510
    .restart local v27    # "j":I
    :cond_24
    const-string v2, "SAMSUNG"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWoleWordText:[Ljava/lang/String;

    aget-object v6, v6, v27

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    new-instance v6, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v27

    invoke-direct {v6, v8}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v6, v2, v25

    .line 1512
    add-int/lit8 v25, v25, 0x1

    .line 1509
    :cond_25
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_8

    .line 1517
    .end local v27    # "j":I
    :cond_26
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    goto/16 :goto_9

    .line 1534
    .restart local v27    # "j":I
    .restart local v31    # "tempText":[Ljava/lang/String;
    .restart local v32    # "tempType":[I
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    aget v2, v2, v27

    aput v2, v32, v27

    .line 1535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    aget-object v2, v2, v27

    aput-object v2, v31, v27

    .line 1533
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_a

    .line 1540
    :cond_28
    const/16 v2, 0xe

    aput v2, v32, v27

    .line 1541
    new-instance v2, Ljava/lang/String;

    const-string v6, "SAMSUNG"

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v2, v31, v27

    .line 1539
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_b

    .line 1550
    .end local v25    # "i":I
    .end local v27    # "j":I
    .end local v28    # "numOfSamsung":I
    .end local v31    # "tempText":[Ljava/lang/String;
    .end local v32    # "tempType":[I
    :cond_29
    const/16 v26, 0x0

    goto/16 :goto_c

    .line 1565
    .restart local v5    # "tmp":[I
    .restart local v7    # "croppedWidth":I
    .restart local v9    # "croppedHeight":I
    .restart local v26    # "isRequestFromOCR":Z
    :cond_2a
    const-string v2, "PostViewActivity"

    const-string v6, "Namecard crop fail. Use full image"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_d

    .line 1577
    .restart local v10    # "directory":Ljava/lang/String;
    .restart local v11    # "fileName":Ljava/lang/String;
    .restart local v22    # "fullPath":Ljava/lang/String;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :cond_2b
    :try_start_a
    sget-object v10, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_MMC:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_e

    .line 1597
    .end local v10    # "directory":Ljava/lang/String;
    .restart local v16    # "bmp":Landroid/graphics/Bitmap;
    .restart local v19    # "dir":Ljava/io/File;
    :catch_0
    move-exception v20

    .line 1598
    .local v20, "e":Ljava/io/IOException;
    :goto_11
    :try_start_b
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1600
    :try_start_c
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1602
    if-eqz v23, :cond_1b

    .line 1603
    :try_start_d
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->flush()V

    .line 1604
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1605
    const/16 v23, 0x0

    goto/16 :goto_f

    .line 1607
    :catch_1
    move-exception v20

    .line 1608
    .local v20, "e":Ljava/lang/Exception;
    :try_start_e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_f

    .line 1640
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v16    # "bmp":Landroid/graphics/Bitmap;
    .end local v19    # "dir":Ljava/io/File;
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v22    # "fullPath":Ljava/lang/String;
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v20

    .line 1641
    .restart local v20    # "e":Ljava/lang/Exception;
    :try_start_f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_10

    .line 1599
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v11    # "fileName":Ljava/lang/String;
    .restart local v16    # "bmp":Landroid/graphics/Bitmap;
    .restart local v19    # "dir":Ljava/io/File;
    .restart local v22    # "fullPath":Ljava/lang/String;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    .line 1600
    :goto_12
    :try_start_10
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 1602
    if-eqz v23, :cond_2c

    .line 1603
    :try_start_11
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->flush()V

    .line 1604
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 1605
    const/16 v23, 0x0

    .line 1610
    :cond_2c
    :goto_13
    :try_start_12
    throw v2

    .line 1607
    :catch_3
    move-exception v20

    .line 1608
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_13

    .line 1607
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v20

    .line 1608
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    move-object/from16 v23, v24

    .line 1611
    .end local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    goto/16 :goto_f

    .line 1612
    .end local v16    # "bmp":Landroid/graphics/Bitmap;
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v10    # "directory":Ljava/lang/String;
    :cond_2d
    const-string v2, "PostViewActivity"

    const-string v6, "cropping failed. Use original image"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1615
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-object v14

    .line 1617
    .local v14, "reRotatedBitmap":Landroid/graphics/Bitmap;
    :try_start_13
    new-instance v17, Ljava/util/GregorianCalendar;

    invoke-direct/range {v17 .. v17}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1618
    .local v17, "calendar":Ljava/util/GregorianCalendar;
    new-instance v33, Landroid/text/format/Time;

    invoke-direct/range {v33 .. v33}, Landroid/text/format/Time;-><init>()V

    .line 1619
    .local v33, "time":Landroid/text/format/Time;
    move-object/from16 v0, v33

    iget-object v2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v34

    .line 1620
    .local v34, "timezone":Ljava/util/TimeZone;
    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1621
    invoke-virtual/range {v17 .. v17}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    .line 1623
    .local v12, "dateTaken":J
    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->addImage(Ljava/lang/String;Ljava/lang/String;JLandroid/graphics/Bitmap;[B)Z

    .line 1626
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/sec/android/app/bcocr/OCRUtils;->addNewFileToDB(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_2
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 1633
    .end local v12    # "dateTaken":J
    .end local v17    # "calendar":Ljava/util/GregorianCalendar;
    .end local v33    # "time":Landroid/text/format/Time;
    .end local v34    # "timezone":Ljava/util/TimeZone;
    :cond_2e
    :goto_14
    if-eqz v14, :cond_2f

    .line 1634
    :try_start_14
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 1635
    const/4 v14, 0x0

    .line 1637
    :cond_2f
    invoke-static {}, Ljava/lang/System;->gc()V

    goto/16 :goto_f

    .line 1627
    :catch_5
    move-exception v20

    .line 1628
    .local v20, "e":Ljava/lang/OutOfMemoryError;
    if-eqz v14, :cond_2e

    .line 1629
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 1630
    const/4 v14, 0x0

    goto :goto_14

    .line 1656
    .end local v5    # "tmp":[I
    .end local v7    # "croppedWidth":I
    .end local v9    # "croppedHeight":I
    .end local v10    # "directory":Ljava/lang/String;
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v14    # "reRotatedBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "dir":Ljava/io/File;
    .end local v20    # "e":Ljava/lang/OutOfMemoryError;
    .end local v22    # "fullPath":Ljava/lang/String;
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    :cond_30
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1599
    .restart local v5    # "tmp":[I
    .restart local v7    # "croppedWidth":I
    .restart local v9    # "croppedHeight":I
    .restart local v11    # "fileName":Ljava/lang/String;
    .restart local v16    # "bmp":Landroid/graphics/Bitmap;
    .restart local v19    # "dir":Ljava/io/File;
    .restart local v22    # "fullPath":Ljava/lang/String;
    .restart local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    goto :goto_12

    .line 1597
    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v20

    move-object/from16 v23, v24

    .end local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    goto/16 :goto_11

    .end local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    :cond_31
    move-object/from16 v23, v24

    .end local v24    # "fullpathStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fullpathStream":Ljava/io/FileOutputStream;
    goto/16 :goto_f
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public getPostViewMode()I
    .locals 1

    .prologue
    .line 1102
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1106
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldScale:F

    return v0
.end method

.method public getSelectedRectArea()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getUrifromFilePath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 732
    const-wide/16 v8, 0x0

    .line 733
    .local v8, "id":J
    const/4 v10, 0x0

    .line 734
    .local v10, "index":I
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 735
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    .line 738
    :cond_0
    const/4 v7, 0x0

    .line 739
    .local v7, "c":Landroid/database/Cursor;
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 740
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_data=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    .line 739
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 742
    if-eqz v7, :cond_2

    .line 743
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 744
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 745
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 747
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 749
    :cond_2
    const-string v0, "content://media/external/images/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 750
    .local v6, "baseUri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public initLanguageSelectorSpinnerRect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 519
    .local v0, "height":I
    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayWidth:I

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nLanguageSelectorRect:Landroid/graphics/Rect;

    .line 520
    return-void
.end method

.method public isDropBoxImage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1723
    if-nez p1, :cond_1

    .line 1730
    :cond_0
    :goto_0
    return v0

    .line 1727
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(?i).*"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mDropBoxDirectory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1728
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isFitImageHeight(II)Z
    .locals 6
    .param p1, "extraDispWidth"    # I
    .param p2, "extraDispHeight"    # I

    .prologue
    .line 1690
    const/4 v2, 0x0

    .line 1691
    .local v2, "result":Z
    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayWidth:I

    sub-int/2addr v4, p1

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 1692
    .local v1, "ratioWidth":F
    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeDisplayHeight:I

    sub-int/2addr v4, p2

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 1694
    .local v0, "ratioHeight":F
    const-string v3, "PostViewActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[isFitImageHeight] ratio width/height: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    cmpg-float v3, v1, v0

    if-gtz v3, :cond_0

    .line 1697
    const/4 v2, 0x1

    .line 1702
    :goto_0
    const-string v3, "PostViewActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[isFitImageHeight] result: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1703
    return v2

    .line 1699
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isJpeg(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1707
    if-nez p1, :cond_0

    .line 1718
    :goto_0
    return v2

    .line 1711
    :cond_0
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1712
    .local v0, "dotposition":I
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1714
    .local v1, "format":Ljava/lang/String;
    const-string v3, "jpg"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "jpeg"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1715
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 1717
    :cond_2
    const-string v3, "PostViewActivity"

    const-string v4, "we only support jpeg for rotation."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isPortrait()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 988
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValidSavetoContact(Z)Z
    .locals 6
    .param p1, "isRequestFromOCR"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    .line 698
    const/4 v0, 0x0

    .line 700
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    if-eqz v3, :cond_0

    .line 701
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    array-length v3, v3

    if-lt v1, v3, :cond_3

    .line 712
    .end local v1    # "i":I
    :cond_0
    if-eqz p1, :cond_1

    .line 713
    const/4 v0, 0x0

    .line 714
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    if-eqz v3, :cond_1

    .line 715
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    array-length v3, v3

    if-lt v1, v3, :cond_5

    .line 726
    .end local v1    # "i":I
    :cond_1
    const-string v2, "PostViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "There is no valid contact : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    const/4 v2, 0x0

    :cond_2
    return v2

    .line 702
    .restart local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    aget v3, v3, v1

    if-ltz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    aget v3, v3, v1

    const/16 v4, 0x10

    if-ge v3, v4, :cond_4

    .line 703
    add-int/lit8 v0, v0, 0x1

    .line 704
    if-ge v0, v5, :cond_2

    .line 701
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 716
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    aget v3, v3, v1

    const/16 v4, 0xb

    if-lt v3, v4, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    aget v3, v3, v1

    const/16 v4, 0xf

    if-ge v3, v4, :cond_6

    .line 717
    add-int/lit8 v0, v0, 0x1

    .line 718
    if-ge v0, v5, :cond_2

    .line 715
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public loadCurrentOCREngineLanguage()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1195
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070003

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 1197
    .local v0, "engine_lang_id":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v7, v7

    if-lt v1, v7, :cond_1

    .line 1201
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    if-eqz v7, :cond_6

    .line 1202
    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 1203
    const/4 v1, 0x0

    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    array-length v7, v7

    if-lt v1, v7, :cond_2

    .line 1235
    :cond_0
    return-void

    .line 1198
    :cond_1
    sget-object v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    aput v9, v7, v1

    .line 1197
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1204
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    aget v7, v7, v1

    if-lez v7, :cond_3

    .line 1205
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    aget v7, v7, v1

    const/16 v8, 0x4c

    if-lt v7, v8, :cond_4

    .line 1203
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1208
    :cond_4
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    array-length v7, v0

    if-ge v2, v7, :cond_3

    .line 1209
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    aget v7, v7, v1

    aget v8, v0, v2

    if-ne v7, v8, :cond_5

    .line 1210
    sget-object v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    aget v9, v0, v2

    aput v9, v7, v8

    .line 1211
    iget v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    goto :goto_2

    .line 1208
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1217
    .end local v2    # "j":I
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getEngineSelectedLanguage(Z)Ljava/lang/String;

    move-result-object v4

    .line 1218
    .local v4, "languageSet":Ljava/lang/String;
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, "^"

    invoke-direct {v6, v4, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    .local v6, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    .line 1220
    .local v5, "selectedLanguageNum":I
    const/16 v7, 0x1b

    if-le v5, v7, :cond_7

    .line 1221
    const/16 v5, 0x1b

    .line 1224
    :cond_7
    iput v9, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 1225
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v5, :cond_0

    .line 1226
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/app/bcocr/OCRDicManager;->getOCREngineIndexLanguageString(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    .line 1227
    .local v3, "languageIndex":I
    const/4 v7, -0x1

    if-eq v3, v7, :cond_8

    if-eqz v0, :cond_8

    array-length v7, v0

    if-ge v3, v7, :cond_8

    .line 1228
    sget-object v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    aget v9, v0, v3

    aput v9, v7, v8

    .line 1232
    :goto_5
    iget v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 1225
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1230
    :cond_8
    sget-object v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    iget v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    const/16 v9, 0x10

    aput v9, v7, v8

    goto :goto_5
.end method

.method public loadCurrentOCREngineLanguageByTransDic()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1161
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getDictionarySrcID()I

    move-result v2

    .line 1162
    .local v2, "langSrcID":I
    const/16 v3, 0x10

    .line 1163
    .local v3, "translatorLanguage":I
    const/4 v0, 0x0

    .line 1164
    .local v0, "duplicate":Z
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->loadCurrentOCREngineLanguage()V

    .line 1165
    invoke-static {p0, v2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getLangID2MocrID(Landroid/content/Context;I)I

    move-result v3

    .line 1167
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v4, v4

    if-lt v1, v4, :cond_1

    .line 1173
    if-nez v0, :cond_0

    .line 1174
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v4, v4

    add-int/lit8 v1, v4, -0x2

    :goto_1
    if-gez v1, :cond_3

    .line 1177
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    aput v3, v4, v7

    .line 1180
    :cond_0
    iput v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 1181
    const/4 v1, 0x0

    :goto_2
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v4, v4

    if-lt v1, v4, :cond_4

    .line 1186
    return-void

    .line 1168
    :cond_1
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    aget v4, v4, v1

    if-ne v4, v3, :cond_2

    .line 1169
    const/4 v0, 0x1

    .line 1167
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1175
    :cond_3
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    add-int/lit8 v5, v1, 0x1

    sget-object v6, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    aget v6, v6, v1

    aput v6, v4, v5

    .line 1174
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1182
    :cond_4
    sget-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREngineLanguageSelectedSet:[I

    aget v4, v4, v1

    if-eqz v4, :cond_5

    .line 1183
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCREnginelanguageSelectedNum:I

    .line 1181
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 330
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 331
    sget v5, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    .line 332
    const-string v5, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onCreate+("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v7, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRUtils;->copyDatabase(Landroid/content/Context;)Z

    .line 337
    invoke-static {p0}, Lcom/sec/android/app/bcocr/FeatureManage;->initFeature(Landroid/content/Context;)V

    .line 338
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageWidth:I

    .line 339
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedImageHeight:I

    .line 340
    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->DROPBOX_DIRECTORY_PATH:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mDropBoxDirectory:Ljava/lang/String;

    .line 342
    new-instance v5, Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-direct {v5, p0}, Lcom/sec/android/app/bcocr/OCRSettings;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 343
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->resetObservers()V

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 346
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "OCR_NAMECARD_CAPTURE_MODE"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    .line 347
    const-string v5, "OCR_NAMECARD_GET_DATA_MODE"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    .line 348
    const-string v5, "OCR_NAMECARD_CROP_IMAGE_MODE"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;

    .line 350
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 352
    .local v4, "preferences":Landroid/content/SharedPreferences;
    const/16 v5, 0x8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->requestWindowFeature(I)Z

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x400

    .line 354
    const/16 v7, 0x400

    .line 353
    invoke-virtual {v5, v6, v7}, Landroid/view/Window;->setFlags(II)V

    .line 356
    const v5, 0x7f030007

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->setContentView(I)V

    .line 357
    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->setVolumeControlStream(I)V

    .line 360
    new-instance v5, Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-direct {v5, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 363
    new-instance v5, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v5}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v5}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    .line 364
    .local v3, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    .line 367
    new-instance v5, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;)V

    sput-object v5, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    .line 368
    const v5, 0x7f0f0015

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/bcocr/PostImageView;

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    .line 369
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    const-string v6, " "

    invoke-virtual {v5, v6}, Lcom/sec/android/app/bcocr/PostImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 371
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/16 v7, -0x50

    const/16 v8, 0x500

    const/16 v9, 0x64

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nLanguageSelectorRect:Landroid/graphics/Rect;

    .line 373
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecogProgressingState:Z

    .line 375
    const-string v5, "ONLYFILEPATH"

    const-string v6, "WHERE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 376
    const-string v5, "PostViewActivity"

    const-string v6, "intent ONLYFILEPATH"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    .line 378
    const-string v5, "FILE_PATH"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 379
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 380
    const/4 v5, -0x1

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 384
    :goto_0
    const-string v5, "FROM"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromWhere:I

    .line 386
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 387
    const-string v5, "ocr_last_selected_funcmode"

    const/4 v6, -0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 388
    .local v2, "lastFuncMode":I
    if-ltz v2, :cond_9

    const/4 v5, 0x5

    if-gt v2, v5, :cond_9

    .line 389
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 394
    .end local v2    # "lastFuncMode":I
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    const-string v6, "(?i).*http://.*"

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    const-string v6, "(?i).*https://.*"

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 395
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLocalFilePath:Z

    .line 397
    :cond_2
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageUri:Landroid/net/Uri;

    .line 398
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->getOCRWholeDataFromImageAsync(Ljava/lang/String;Landroid/content/Intent;)V

    .line 399
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    .line 468
    :goto_2
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_3

    .line 469
    const/4 v5, 0x1

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsScreenCropMode:I

    .line 472
    :cond_3
    const-string v5, "ROTATIONMENU"

    const-string v6, "FROM"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 473
    const-string v5, "IDBYTIME"

    const-wide/16 v6, 0x0

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nIDByTime:J

    .line 474
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsImageRotatedByOption:Z

    .line 475
    const-string v5, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "nIDByTime, ROTATING : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nIDByTime:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    .line 484
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_15

    .line 485
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 486
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 487
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 489
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->show()V

    .line 496
    :cond_4
    :goto_4
    iget-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    if-nez v5, :cond_6

    .line 497
    sget-object v5, Lcom/sec/android/app/bcocr/PostViewActivity;->mContentResolver:Landroid/content/ContentResolver;

    .line 498
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 497
    invoke-static {v5, v6}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getImageOrientation(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageOrientation:I

    .line 499
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_5

    .line 500
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setImageAndDisplayWH()V

    .line 501
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->initPostView()V

    .line 504
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->setImageView(Ljava/lang/String;)V

    .line 506
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mLastOrientation:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_6

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mLastOrientation:I

    .line 511
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 512
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CheckMemory;->setStorageVolume(Landroid/content/Context;)V

    .line 514
    :cond_7
    return-void

    .line 382
    :cond_8
    const-string v5, "MODE"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    goto/16 :goto_0

    .line 391
    .restart local v2    # "lastFuncMode":I
    :cond_9
    const/4 v5, 0x5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    goto/16 :goto_1

    .line 400
    .end local v2    # "lastFuncMode":I
    :cond_a
    const-string v5, "WIDECAPTURE"

    const-string v6, "WHERE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 401
    const-string v5, "PostViewActivity"

    const-string v6, "intent Wide capture or ScanStitch"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    .line 403
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    .line 405
    const-string v5, "FILE_PATH"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 406
    const-string v5, "MODE"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 408
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_b

    .line 409
    const-string v5, "ocr_last_selected_funcmode"

    const/4 v6, -0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 410
    .restart local v2    # "lastFuncMode":I
    if-ltz v2, :cond_c

    const/4 v5, 0x5

    if-gt v2, v5, :cond_c

    .line 411
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 416
    .end local v2    # "lastFuncMode":I
    :cond_b
    :goto_5
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->getOCRWholeDataFromImageAsync(Ljava/lang/String;Landroid/content/Intent;)V

    .line 417
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    goto/16 :goto_2

    .line 413
    .restart local v2    # "lastFuncMode":I
    :cond_c
    const/4 v5, 0x5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    goto :goto_5

    .line 418
    .end local v2    # "lastFuncMode":I
    :cond_d
    const-string v5, "NAMECARDCAPTURE"

    const-string v6, "WHERE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 419
    const-string v5, "PostViewActivity"

    const-string v6, "intent NameCard Capture"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    .line 421
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardCaptureMode:Ljava/lang/Boolean;

    .line 423
    const-string v5, "FILE_PATH"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 424
    const-string v5, "MODE"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 426
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 428
    const-string v5, "OCR_NAMECARD_SELECTED_LANG_LIST"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedLangSet:[I

    .line 431
    const-string v5, "SPECIAL_WORD_NUM"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    .line 432
    const-string v5, "SPECIAL_WORD_TYPE"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    .line 433
    const-string v5, "SPECIAL_WORD_TEXT"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    .line 434
    const-string v5, "SPECIAL_WORD_RECT"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    .line 435
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x4

    if-lez v5, :cond_e

    .line 436
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x4

    new-array v5, v5, [Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    .line 438
    const-string v5, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "arrayList.size() : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_10

    .line 447
    .end local v0    # "i":I
    :cond_e
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_f

    .line 448
    const-string v5, "ocr_last_selected_funcmode"

    const/4 v6, -0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 449
    .restart local v2    # "lastFuncMode":I
    if-ltz v2, :cond_11

    const/4 v5, 0x5

    if-gt v2, v5, :cond_11

    .line 450
    iput v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    .line 455
    .end local v2    # "lastFuncMode":I
    :cond_f
    :goto_7
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->getOCRWholeDataFromImageAsync(Ljava/lang/String;Landroid/content/Intent;)V

    .line 457
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    goto/16 :goto_2

    .line 440
    .restart local v0    # "i":I
    :cond_10
    iget-object v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v7, v0, 0x4

    new-instance v8, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 441
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    add-int/lit8 v10, v0, 0x1

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    add-int/lit8 v11, v0, 0x2

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 442
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_DirectLinkArray:Ljava/util/ArrayList;

    add-int/lit8 v12, v0, 0x3

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v8, v9, v10, v11, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 440
    aput-object v8, v6, v7

    .line 439
    add-int/lit8 v0, v0, 0x4

    goto :goto_6

    .line 452
    .end local v0    # "i":I
    .restart local v2    # "lastFuncMode":I
    :cond_11
    const/4 v5, 0x5

    iput v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    goto :goto_7

    .line 458
    .end local v2    # "lastFuncMode":I
    :cond_12
    const-string v5, "OCR_CAPTURE"

    const-string v6, "WHERE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 459
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecogProgressingState:Z

    .line 460
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;

    .line 461
    invoke-direct {p0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->handleIntentExtra(Landroid/content/Intent;)V

    .line 462
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    goto/16 :goto_2

    .line 464
    :cond_13
    const-string v5, "PostViewActivity"

    const-string v6, "There is no information for starting capture result"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    goto/16 :goto_2

    .line 477
    :cond_14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nIDByTime:J

    .line 478
    const-string v5, "PostViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "nIDByTime, From device : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nIDByTime:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 491
    :cond_15
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v5, :cond_4

    .line 492
    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->hide()V

    goto/16 :goto_4
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1023
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1024
    const-string v1, "PostViewActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onDestroy("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    if-eqz v1, :cond_0

    .line 1027
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->clear()V

    .line 1028
    iput-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 1031
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 1032
    const-string v1, "PostViewActivity"

    const-string v2, "onPause mBitmap is null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    :cond_1
    sget v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    if-nez v1, :cond_5

    .line 1036
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    if-eqz v1, :cond_2

    .line 1037
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onDestroy()V

    .line 1038
    iput-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 1040
    :cond_2
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    if-eqz v1, :cond_3

    .line 1041
    sput-object v4, Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    .line 1043
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->destroyRecogData()V

    .line 1044
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    if-eqz v1, :cond_4

    .line 1045
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/PostImageView;->onDestroy()V

    .line 1046
    iput-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    .line 1049
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getBCRTempFile()Ljava/io/File;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/bcocr/PostViewActivity;->deleteFile(Ljava/io/File;)V

    .line 1050
    iput-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    .line 1053
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 1054
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1055
    iput-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 1058
    :cond_6
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    if-eqz v1, :cond_7

    .line 1060
    :try_start_0
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066
    :cond_7
    :goto_0
    sget v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mPostViewContextNum:I

    .line 1067
    return-void

    .line 1061
    :catch_0
    move-exception v0

    .line 1062
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 1077
    const-string v0, "PostViewActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 1079
    const-string v0, "PostViewActivity"

    const-string v1, "onPause mBitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    :cond_0
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "waitForRecognizeThread+++"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->waitForRecognizeThread()V

    .line 1084
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "waitForRecognizeThread---"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1086
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 997
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 998
    const-string v0, "PostViewActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mCallFromUnknownSource:Z

    if-eqz v0, :cond_0

    .line 1000
    const-string v0, "PostViewActivity"

    const-string v1, "Stop on Resume :Activity called from unknown source"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    .line 1004
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setSelectedRectArea()V

    .line 1005
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->invalidateOptionsMenu()V

    .line 1007
    iget v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mLastOrientation:I

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    .line 1008
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1011
    :cond_1
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    .line 1012
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThreadIsWaiting:Z

    if-eqz v0, :cond_2

    .line 1013
    sget-object v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1014
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThreadIsWaiting:Z

    .line 1019
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1090
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1091
    const-string v0, "PostViewActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    return-void
.end method

.method public savetoContact()Z
    .locals 26

    .prologue
    .line 2292
    new-instance v23, Landroid/content/Intent;

    const-string v24, "android.intent.action.INSERT"

    invoke-direct/range {v23 .. v24}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    .line 2293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v23, v0

    const-string v24, "vnd.android.cursor.dir/contact"

    invoke-virtual/range {v23 .. v24}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2295
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2296
    .local v10, "bundle":Landroid/os/Bundle;
    const/4 v15, 0x0

    .local v15, "nFoundNumber":I
    const/4 v12, 0x0

    .line 2298
    .local v12, "nFoundEmail":I
    const/16 v16, -0x1

    .line 2299
    .local v16, "nFoundPhone":I
    const/4 v14, -0x1

    .line 2300
    .local v14, "nFoundMobile":I
    const/4 v13, -0x1

    .line 2301
    .local v13, "nFoundFax":I
    const/4 v7, 0x0

    .line 2302
    .local v7, "bFoundName":Z
    const/4 v6, 0x0

    .line 2303
    .local v6, "bFoundDepartMent":Z
    const/4 v5, 0x0

    .line 2304
    .local v5, "bFoundCompany":Z
    const/4 v4, 0x0

    .line 2305
    .local v4, "bFoundAddress":Z
    const/4 v9, 0x0

    .line 2306
    .local v9, "bFoundURL":Z
    const/4 v8, 0x0

    .line 2307
    .local v8, "bFoundPostCode":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    move/from16 v17, v0

    .line 2310
    .local v17, "nNumberOfFoundLink":I
    const/16 v18, 0x0

    .line 2311
    .local v18, "postCode":Ljava/lang/String;
    const-string v3, ""

    .line 2314
    .local v3, "address":Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v17

    if-lt v11, v0, :cond_4

    .line 2349
    const/16 v23, -0x1

    move/from16 v0, v23

    if-eq v14, v0, :cond_0

    const/16 v23, -0x1

    move/from16 v0, v16

    move/from16 v1, v23

    if-eq v0, v1, :cond_0

    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_1

    .line 2350
    :cond_0
    const/4 v11, 0x0

    :goto_1
    move/from16 v0, v17

    if-lt v11, v0, :cond_b

    .line 2391
    :cond_1
    :goto_2
    const/4 v11, 0x0

    :goto_3
    move/from16 v0, v17

    if-lt v11, v0, :cond_11

    .line 2472
    if-eqz v8, :cond_2

    .line 2474
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2477
    :cond_2
    if-eqz v4, :cond_3

    .line 2478
    const-string v23, "postal_type"

    .line 2479
    const/16 v24, 0x2

    .line 2478
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2480
    const-string v23, "postal"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2483
    :cond_3
    if-nez v15, :cond_1d

    .line 2484
    if-nez v12, :cond_1d

    .line 2485
    if-nez v6, :cond_1d

    .line 2486
    if-nez v5, :cond_1d

    .line 2487
    if-nez v4, :cond_1d

    .line 2488
    if-nez v9, :cond_1d

    .line 2489
    const-string v23, "PostViewActivity"

    const-string v24, "savetoContact :  not found a item"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2490
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v23

    const v24, 0x7f0c0034

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/widget/Toast;->show()V

    .line 2491
    const/16 v23, 0x0

    .line 2501
    :goto_4
    return v23

    .line 2315
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v23, v0

    aget v22, v23, v11

    .line 2316
    .local v22, "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v23, v0

    aget-object v21, v23, v11

    .line 2318
    .local v21, "wordText":Ljava/lang/String;
    if-nez v21, :cond_6

    .line 2314
    :cond_5
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 2323
    :cond_6
    const/16 v23, 0xd

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 2324
    const/16 v23, 0xb

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 2325
    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    .line 2326
    :cond_7
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v14, v0, :cond_9

    const/16 v23, 0xd

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_9

    .line 2327
    move v14, v11

    .line 2328
    const-string v23, "phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2329
    const-string v23, "phone_type"

    .line 2330
    const/16 v24, 0x2

    .line 2329
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2345
    :cond_8
    :goto_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 2332
    :cond_9
    const/16 v23, -0x1

    move/from16 v0, v16

    move/from16 v1, v23

    if-ne v0, v1, :cond_a

    const/16 v23, 0xb

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_a

    .line 2333
    move/from16 v16, v11

    .line 2334
    const-string v23, "secondary_phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335
    const-string v23, "secondary_phone_type"

    .line 2336
    const/16 v24, 0x3

    .line 2335
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_6

    .line 2338
    :cond_a
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_8

    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    .line 2339
    move v13, v11

    .line 2340
    const-string v23, "tertiary_phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    const-string v23, "tertiary_phone_type"

    .line 2342
    const/16 v24, 0x4

    .line 2341
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_6

    .line 2351
    .end local v21    # "wordText":Ljava/lang/String;
    .end local v22    # "wordType":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v23, v0

    aget v22, v23, v11

    .line 2352
    .restart local v22    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v23, v0

    aget-object v21, v23, v11

    .line 2354
    .restart local v21    # "wordText":Ljava/lang/String;
    if-nez v21, :cond_d

    .line 2350
    :cond_c
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 2358
    :cond_d
    const/16 v23, 0xd

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_e

    const/16 v23, 0xb

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_e

    .line 2359
    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    .line 2360
    :cond_e
    if-eq v14, v11, :cond_c

    move/from16 v0, v16

    if-eq v0, v11, :cond_c

    if-eq v13, v11, :cond_c

    .line 2364
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v14, v0, :cond_f

    .line 2365
    move v14, v11

    .line 2366
    const-string v23, "phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2367
    const-string v23, "phone_type"

    .line 2368
    const/16 v24, 0x3

    .line 2367
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_7

    .line 2370
    :cond_f
    const/16 v23, -0x1

    move/from16 v0, v16

    move/from16 v1, v23

    if-ne v0, v1, :cond_10

    .line 2371
    move/from16 v16, v11

    .line 2372
    const-string v23, "secondary_phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    const-string v23, "secondary_phone_type"

    .line 2374
    const/16 v24, 0x3

    .line 2373
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_7

    .line 2376
    :cond_10
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_1

    .line 2377
    move v13, v11

    .line 2378
    const-string v23, "tertiary_phone"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    const-string v23, "tertiary_phone_type"

    .line 2380
    const/16 v24, 0x3

    .line 2379
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 2392
    .end local v21    # "wordText":Ljava/lang/String;
    .end local v22    # "wordType":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v23, v0

    aget v22, v23, v11

    .line 2393
    .restart local v22    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v23, v0

    aget-object v21, v23, v11

    .line 2395
    .restart local v21    # "wordText":Ljava/lang/String;
    if-nez v21, :cond_13

    .line 2391
    :cond_12
    :goto_8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 2399
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    move/from16 v23, v0

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_14

    .line 2400
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSIPType:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->compareSIPTypetoFoundWordType(II)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 2404
    :cond_14
    const/16 v23, 0xf

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_17

    .line 2405
    if-nez v12, :cond_15

    .line 2406
    const-string v23, "PostViewActivity"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "resultTestt email0: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2407
    const-string v23, "email"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408
    const-string v23, "email_type"

    .line 2409
    const/16 v24, 0x2

    .line 2408
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2410
    add-int/lit8 v12, v12, 0x1

    .line 2411
    goto :goto_8

    :cond_15
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v12, v0, :cond_16

    .line 2412
    const-string v23, "PostViewActivity"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "resultTestt email1: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2413
    const-string v23, "secondary_email"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2414
    const-string v23, "secondary_email_type"

    .line 2415
    const/16 v24, 0x2

    .line 2414
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2416
    add-int/lit8 v12, v12, 0x1

    .line 2417
    goto/16 :goto_8

    :cond_16
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v12, v0, :cond_12

    .line 2418
    const-string v23, "PostViewActivity"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "resultTestt email2: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2419
    const-string v23, "tertiary_email"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2420
    const-string v23, "tertiary_email_type"

    .line 2421
    const/16 v24, 0x2

    .line 2420
    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2422
    add-int/lit8 v12, v12, 0x1

    .line 2424
    goto/16 :goto_8

    :cond_17
    if-nez v22, :cond_18

    .line 2425
    if-nez v7, :cond_12

    .line 2428
    const-string v23, "name"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2429
    const/4 v7, 0x1

    .line 2431
    goto/16 :goto_8

    :cond_18
    const/16 v23, 0x4

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_19

    .line 2432
    if-nez v6, :cond_12

    .line 2435
    const-string v23, "job_title"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436
    const/4 v6, 0x1

    .line 2438
    goto/16 :goto_8

    :cond_19
    const/16 v23, 0x5

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1a

    .line 2439
    if-nez v5, :cond_12

    .line 2442
    const-string v23, "company"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443
    const/4 v5, 0x1

    .line 2445
    goto/16 :goto_8

    :cond_1a
    const/16 v23, 0x6

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1b

    .line 2446
    if-nez v4, :cond_12

    .line 2449
    move-object/from16 v3, v21

    .line 2450
    const/4 v4, 0x1

    .line 2452
    goto/16 :goto_8

    :cond_1b
    const/16 v23, 0xe

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1c

    .line 2453
    if-nez v9, :cond_12

    .line 2456
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 2457
    .local v19, "value":Landroid/content/ContentValues;
    const-string v23, "mimetype"

    const-string v24, "vnd.android.cursor.item/website"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458
    const-string v23, "data1"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 2461
    .local v20, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2462
    const-string v23, "data"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2464
    const/4 v9, 0x1

    .line 2466
    goto/16 :goto_8

    .end local v19    # "value":Landroid/content/ContentValues;
    .end local v20    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_1c
    const/16 v23, 0x7

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_12

    .line 2467
    const/4 v8, 0x1

    .line 2468
    move-object/from16 v18, v21

    goto/16 :goto_8

    .line 2494
    .end local v21    # "wordText":Ljava/lang/String;
    .end local v22    # "wordType":I
    :cond_1d
    const-string v23, "photomode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath_for_contact:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495
    const-string v23, "is_businesscard_contact"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2498
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    if-nez v23, :cond_1e

    .line 2499
    const/16 v23, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 2501
    :cond_1e
    const/16 v23, 0x1

    goto/16 :goto_4
.end method

.method public savetoContact(Z)Z
    .locals 1
    .param p1, "isRequestFromOCR"    # Z

    .prologue
    .line 1853
    if-eqz p1, :cond_0

    .line 1854
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->savetoContactFromOCR()Z

    move-result v0

    .line 1856
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->savetoContact()Z

    move-result v0

    goto :goto_0
.end method

.method public savetoContactFromOCR()Z
    .locals 34

    .prologue
    .line 1861
    new-instance v31, Landroid/content/Intent;

    const-string v32, "android.intent.action.INSERT"

    invoke-direct/range {v31 .. v32}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    .line 1862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v31, v0

    const-string v32, "vnd.android.cursor.dir/contact"

    invoke-virtual/range {v31 .. v32}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1864
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1865
    .local v10, "bundle":Landroid/os/Bundle;
    const/16 v16, 0x0

    .local v16, "nFoundNumber":I
    const/4 v13, 0x0

    .line 1867
    .local v13, "nFoundEmail":I
    const/16 v17, -0x1

    .line 1868
    .local v17, "nFoundPhone":I
    const/4 v15, -0x1

    .line 1869
    .local v15, "nFoundMobile":I
    const/4 v14, -0x1

    .line 1870
    .local v14, "nFoundFax":I
    const/4 v7, 0x0

    .line 1871
    .local v7, "bFoundName":Z
    const/4 v6, 0x0

    .line 1872
    .local v6, "bFoundDepartMent":Z
    const/4 v5, 0x0

    .line 1873
    .local v5, "bFoundCompany":Z
    const/4 v4, 0x0

    .line 1874
    .local v4, "bFoundAddress":Z
    const/4 v9, 0x0

    .line 1875
    .local v9, "bFoundURL":Z
    const/4 v8, 0x0

    .line 1876
    .local v8, "bFoundPostCode":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    move/from16 v18, v0

    .line 1879
    .local v18, "nNumberOfFoundLink":I
    const/16 v19, 0x0

    .line 1880
    .local v19, "postCode":Ljava/lang/String;
    const-string v3, ""

    .line 1882
    .local v3, "address":Ljava/lang/String;
    const/16 v26, 0x0

    .line 1883
    .local v26, "rFoundPhone":Landroid/graphics/Rect;
    const/16 v25, 0x0

    .line 1884
    .local v25, "rFoundMobile":Landroid/graphics/Rect;
    const/16 v24, 0x0

    .line 1885
    .local v24, "rFoundFax":Landroid/graphics/Rect;
    const/16 v21, 0x0

    .line 1886
    .local v21, "rFoundEmail1":Landroid/graphics/Rect;
    const/16 v22, 0x0

    .line 1887
    .local v22, "rFoundEmail2":Landroid/graphics/Rect;
    const/16 v23, 0x0

    .line 1888
    .local v23, "rFoundEmail3":Landroid/graphics/Rect;
    const/4 v11, 0x0

    .line 1890
    .local v11, "getContactLog":Z
    if-eqz v11, :cond_1

    .line 1893
    const-string v31, "PostViewActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "[contact]OCR number : "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1894
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    if-lez v31, :cond_0

    .line 1895
    const-string v20, "[contact]OCR type : "

    .line 1896
    .local v20, "printText":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_9

    .line 1899
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1900
    const-string v20, "[contact]OCR text : "

    .line 1901
    const/4 v12, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_a

    .line 1904
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1905
    const-string v20, "[contact]OCR rect : "

    .line 1906
    const/4 v12, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_b

    .line 1909
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1913
    .end local v12    # "i":I
    .end local v20    # "printText":Ljava/lang/String;
    :cond_0
    const-string v31, "PostViewActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "[contact]OCR number : "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1914
    if-lez v18, :cond_1

    .line 1915
    const-string v20, "[contact]OCR type : "

    .line 1916
    .restart local v20    # "printText":Ljava/lang/String;
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_c

    .line 1919
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1920
    const-string v20, "[contact]OCR text : "

    .line 1921
    const/4 v12, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_d

    .line 1924
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1925
    const-string v20, "[contact]OCR rect : "

    .line 1926
    const/4 v12, 0x0

    :goto_5
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_e

    .line 1929
    const-string v31, "PostViewActivity"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1935
    .end local v12    # "i":I
    .end local v20    # "printText":Ljava/lang/String;
    :cond_1
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_6
    move/from16 v0, v18

    if-lt v12, v0, :cond_f

    .line 1973
    const/16 v31, -0x1

    move/from16 v0, v31

    if-eq v15, v0, :cond_2

    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_2

    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_3

    .line 1974
    :cond_2
    const/4 v12, 0x0

    :goto_7
    move/from16 v0, v18

    if-lt v12, v0, :cond_16

    .line 2018
    :cond_3
    :goto_8
    const/4 v12, 0x0

    :goto_9
    move/from16 v0, v18

    if-lt v12, v0, :cond_1c

    .line 2099
    if-eqz v8, :cond_4

    .line 2101
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2104
    :cond_4
    if-eqz v4, :cond_5

    .line 2105
    const-string v31, "postal_type"

    .line 2106
    const/16 v32, 0x2

    .line 2105
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2107
    const-string v31, "postal"

    move-object/from16 v0, v31

    invoke-virtual {v10, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    if-lez v31, :cond_6

    .line 2112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    if-eqz v31, :cond_28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_28

    .line 2120
    :cond_6
    :goto_a
    const/4 v12, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_29

    .line 2162
    const/16 v31, -0x1

    move/from16 v0, v31

    if-eq v15, v0, :cond_7

    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_7

    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_8

    .line 2163
    :cond_7
    const/4 v12, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_34

    .line 2214
    :cond_8
    :goto_d
    const/4 v12, 0x0

    :goto_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-lt v12, v0, :cond_3f

    .line 2269
    if-nez v16, :cond_4a

    .line 2270
    if-nez v13, :cond_4a

    .line 2271
    if-nez v6, :cond_4a

    .line 2272
    if-nez v5, :cond_4a

    .line 2273
    if-nez v4, :cond_4a

    .line 2274
    if-nez v9, :cond_4a

    .line 2275
    const-string v31, "PostViewActivity"

    const-string v32, "savetoContact :  not found a item"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2276
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v31

    const v32, 0x7f0c0034

    const/16 v33, 0x0

    invoke-static/range {v31 .. v33}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/widget/Toast;->show()V

    .line 2277
    const/16 v31, 0x0

    .line 2287
    :goto_f
    return v31

    .line 1897
    .restart local v20    # "printText":Ljava/lang/String;
    :cond_9
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    move-object/from16 v32, v0

    aget v32, v32, v12

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1896
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 1902
    :cond_a
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1901
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 1907
    :cond_b
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]("

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, ","

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "-"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, ","

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1906
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 1917
    :cond_c
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v32, v0

    aget v32, v32, v12

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1916
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3

    .line 1922
    :cond_d
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v32, v0

    aget-object v32, v32, v12

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1921
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 1927
    :cond_e
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]("

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    sget-object v32, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, ","

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    sget-object v32, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "-"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    sget-object v32, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, ","

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    sget-object v32, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v32, v32, v12

    move-object/from16 v0, v32

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1926
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_5

    .line 1936
    .end local v20    # "printText":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 1937
    .local v30, "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 1939
    .local v29, "wordText":Ljava/lang/String;
    if-nez v29, :cond_11

    .line 1935
    :cond_10
    :goto_10
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_6

    .line 1944
    :cond_11
    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_12

    .line 1945
    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_12

    .line 1946
    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    .line 1947
    :cond_12
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v15, v0, :cond_14

    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_14

    .line 1948
    move v15, v12

    .line 1949
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v25, v31, v12

    .line 1950
    const-string v31, "phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    const-string v31, "phone_type"

    .line 1952
    const/16 v32, 0x2

    .line 1951
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1969
    :cond_13
    :goto_11
    add-int/lit8 v16, v16, 0x1

    goto :goto_10

    .line 1954
    :cond_14
    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_15

    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_15

    .line 1955
    move/from16 v17, v12

    .line 1956
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v26, v31, v12

    .line 1957
    const-string v31, "secondary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1958
    const-string v31, "secondary_phone_type"

    .line 1959
    const/16 v32, 0x3

    .line 1958
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_11

    .line 1961
    :cond_15
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_13

    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_13

    .line 1962
    move v14, v12

    .line 1963
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v24, v31, v12

    .line 1964
    const-string v31, "tertiary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    const-string v31, "tertiary_phone_type"

    .line 1966
    const/16 v32, 0x4

    .line 1965
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_11

    .line 1975
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 1976
    .restart local v30    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 1978
    .restart local v29    # "wordText":Ljava/lang/String;
    if-nez v29, :cond_18

    .line 1974
    :cond_17
    :goto_12
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_7

    .line 1982
    :cond_18
    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_19

    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_19

    .line 1983
    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_17

    .line 1984
    :cond_19
    if-eq v15, v12, :cond_17

    move/from16 v0, v17

    if-eq v0, v12, :cond_17

    if-eq v14, v12, :cond_17

    .line 1988
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v15, v0, :cond_1a

    .line 1989
    move v15, v12

    .line 1990
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v25, v31, v12

    .line 1991
    const-string v31, "phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992
    const-string v31, "phone_type"

    .line 1993
    const/16 v32, 0x3

    .line 1992
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_12

    .line 1995
    :cond_1a
    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_1b

    .line 1996
    move/from16 v17, v12

    .line 1997
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v26, v31, v12

    .line 1998
    const-string v31, "secondary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1999
    const-string v31, "secondary_phone_type"

    .line 2000
    const/16 v32, 0x3

    .line 1999
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_12

    .line 2002
    :cond_1b
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_3

    .line 2003
    move v14, v12

    .line 2004
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v24, v31, v12

    .line 2005
    const-string v31, "tertiary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006
    const-string v31, "tertiary_phone_type"

    .line 2007
    const/16 v32, 0x3

    .line 2006
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 2019
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 2020
    .restart local v30    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nFoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 2022
    .restart local v29    # "wordText":Ljava/lang/String;
    if-nez v29, :cond_1e

    .line 2018
    :cond_1d
    :goto_13
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_9

    .line 2026
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    move/from16 v31, v0

    const/16 v32, 0x4

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1f

    .line 2027
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOCRSIPType:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->compareSIPTypetoFoundWordType(II)Z

    move-result v31

    if-eqz v31, :cond_1d

    .line 2031
    :cond_1f
    const/16 v31, 0xf

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_22

    .line 2032
    if-nez v13, :cond_20

    .line 2033
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v21, v31, v12

    .line 2034
    const-string v31, "email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    const-string v31, "email_type"

    .line 2036
    const/16 v32, 0x2

    .line 2035
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2037
    add-int/lit8 v13, v13, 0x1

    .line 2038
    goto :goto_13

    :cond_20
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v13, v0, :cond_21

    .line 2039
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v22, v31, v12

    .line 2040
    const-string v31, "secondary_email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    const-string v31, "secondary_email_type"

    .line 2042
    const/16 v32, 0x2

    .line 2041
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2043
    add-int/lit8 v13, v13, 0x1

    .line 2044
    goto :goto_13

    :cond_21
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v13, v0, :cond_1d

    .line 2045
    sget-object v31, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    aget-object v23, v31, v12

    .line 2046
    const-string v31, "tertiary_email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    const-string v31, "tertiary_email_type"

    .line 2048
    const/16 v32, 0x2

    .line 2047
    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2049
    add-int/lit8 v13, v13, 0x1

    .line 2051
    goto/16 :goto_13

    :cond_22
    if-nez v30, :cond_23

    .line 2052
    if-nez v7, :cond_1d

    .line 2055
    const-string v31, "name"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2056
    const/4 v7, 0x1

    .line 2058
    goto/16 :goto_13

    :cond_23
    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_24

    .line 2059
    if-nez v6, :cond_1d

    .line 2062
    const-string v31, "job_title"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    const/4 v6, 0x1

    .line 2065
    goto/16 :goto_13

    :cond_24
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_25

    .line 2066
    if-nez v5, :cond_1d

    .line 2069
    const-string v31, "company"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070
    const/4 v5, 0x1

    .line 2072
    goto/16 :goto_13

    :cond_25
    const/16 v31, 0x6

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_26

    .line 2073
    if-nez v4, :cond_1d

    .line 2076
    move-object/from16 v3, v29

    .line 2077
    const/4 v4, 0x1

    .line 2079
    goto/16 :goto_13

    :cond_26
    const/16 v31, 0xe

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_27

    .line 2080
    if-nez v9, :cond_1d

    .line 2083
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 2084
    .local v27, "value":Landroid/content/ContentValues;
    const-string v31, "mimetype"

    const-string v32, "vnd.android.cursor.item/website"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2085
    const-string v31, "data1"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 2088
    .local v28, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2089
    const-string v31, "data"

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2091
    const/4 v9, 0x1

    .line 2093
    goto/16 :goto_13

    .end local v27    # "value":Landroid/content/ContentValues;
    .end local v28    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_27
    const/16 v31, 0x7

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1d

    .line 2094
    const/4 v8, 0x1

    .line 2095
    move-object/from16 v19, v29

    goto/16 :goto_13

    .line 2115
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_28
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    .line 2116
    const-string v31, "PostViewActivity"

    const-string v32, "save to contact : invaild param (wordRect length != link num)"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 2121
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 2122
    .restart local v30    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 2124
    .restart local v29    # "wordText":Ljava/lang/String;
    if-nez v29, :cond_2b

    .line 2120
    :cond_2a
    :goto_14
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_b

    .line 2128
    :cond_2b
    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_2c

    .line 2129
    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_2c

    .line 2130
    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2a

    .line 2132
    :cond_2c
    const/16 v31, -0x1

    move/from16 v0, v31

    if-eq v15, v0, :cond_2d

    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_2d

    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_2a

    .line 2135
    :cond_2d
    if-eqz v25, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_2a

    .line 2136
    :cond_2e
    if-eqz v26, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_2a

    .line 2137
    :cond_2f
    if-eqz v24, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_2a

    .line 2142
    :cond_30
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v15, v0, :cond_32

    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_32

    .line 2143
    move v15, v12

    .line 2144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v25, v31, v12

    .line 2145
    const-string v31, "phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2146
    const-string v31, "phone_type"

    const/16 v32, 0x2

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2158
    :cond_31
    :goto_15
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_14

    .line 2147
    :cond_32
    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_33

    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_33

    .line 2148
    move/from16 v17, v12

    .line 2149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v26, v31, v12

    .line 2150
    const-string v31, "secondary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151
    const-string v31, "secondary_phone_type"

    const/16 v32, 0x3

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_15

    .line 2152
    :cond_33
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_31

    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_31

    .line 2153
    move v14, v12

    .line 2154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v24, v31, v12

    .line 2155
    const-string v31, "tertiary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    const-string v31, "tertiary_phone_type"

    const/16 v32, 0x4

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_15

    .line 2164
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 2165
    .restart local v30    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 2167
    .restart local v29    # "wordText":Ljava/lang/String;
    if-nez v29, :cond_36

    .line 2163
    :cond_35
    :goto_16
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_c

    .line 2171
    :cond_36
    const/16 v31, 0xd

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_37

    .line 2172
    const/16 v31, 0xb

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_37

    .line 2173
    const/16 v31, 0xc

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_35

    .line 2178
    :cond_37
    const/16 v31, -0x1

    move/from16 v0, v31

    if-eq v15, v0, :cond_38

    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_38

    const/16 v31, -0x1

    move/from16 v0, v31

    if-eq v14, v0, :cond_38

    .line 2179
    const-string v31, "PostViewActivity"

    const-string v32, "[contact]max phone"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_16

    .line 2182
    :cond_38
    if-eqz v25, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_3b

    .line 2183
    :cond_39
    if-eqz v26, :cond_3a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_3b

    .line 2184
    :cond_3a
    if-eqz v24, :cond_3c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-eqz v31, :cond_3c

    .line 2185
    :cond_3b
    if-eqz v11, :cond_35

    .line 2186
    const-string v31, "PostViewActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "[contact]duplicate : "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v33, v0

    aget-object v33, v33, v12

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_16

    .line 2190
    :cond_3c
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v15, v0, :cond_3d

    .line 2191
    move v15, v12

    .line 2192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v25, v31, v12

    .line 2193
    const-string v31, "phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2194
    const-string v31, "phone_type"

    const/16 v32, 0x3

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_16

    .line 2195
    :cond_3d
    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_3e

    .line 2196
    move/from16 v17, v12

    .line 2197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v26, v31, v12

    .line 2198
    const-string v31, "secondary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2199
    const-string v31, "secondary_phone_type"

    const/16 v32, 0x3

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_16

    .line 2200
    :cond_3e
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_8

    .line 2201
    move v14, v12

    .line 2202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v24, v31, v12

    .line 2203
    const-string v31, "tertiary_phone"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    const-string v31, "tertiary_phone_type"

    const/16 v32, 0x3

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_d

    .line 2215
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_3f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordType:[I

    move-object/from16 v31, v0

    aget v30, v31, v12

    .line 2216
    .restart local v30    # "wordType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v31, v0

    aget-object v29, v31, v12

    .line 2218
    .restart local v29    # "wordText":Ljava/lang/String;
    if-nez v29, :cond_41

    .line 2214
    :cond_40
    :goto_17
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_e

    .line 2222
    :cond_41
    const/16 v31, 0xf

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_49

    .line 2224
    const/16 v31, 0x3

    move/from16 v0, v31

    if-lt v13, v0, :cond_42

    .line 2225
    const-string v31, "PostViewActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "[contact]max email : "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_17

    .line 2228
    :cond_42
    if-eqz v21, :cond_43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_45

    .line 2229
    :cond_43
    if-eqz v22, :cond_44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-nez v31, :cond_45

    .line 2230
    :cond_44
    if-eqz v23, :cond_46

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v31, v31, v12

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v31

    if-eqz v31, :cond_46

    .line 2231
    :cond_45
    if-eqz v11, :cond_40

    .line 2232
    const-string v31, "PostViewActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "[contact]duplicate : "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_FoundWordText:[Ljava/lang/String;

    move-object/from16 v33, v0

    aget-object v33, v33, v12

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_17

    .line 2237
    :cond_46
    if-nez v13, :cond_47

    .line 2238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v21, v31, v12

    .line 2239
    const-string v31, "email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2240
    const-string v31, "email_type"

    const/16 v32, 0x2

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2241
    add-int/lit8 v13, v13, 0x1

    .line 2242
    goto/16 :goto_17

    :cond_47
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v13, v0, :cond_48

    .line 2243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v22, v31, v12

    .line 2244
    const-string v31, "secondary_email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2245
    const-string v31, "secondary_email_type"

    const/16 v32, 0x2

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2246
    add-int/lit8 v13, v13, 0x1

    .line 2247
    goto/16 :goto_17

    :cond_48
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v13, v0, :cond_40

    .line 2248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_SelectedRecogWordRect:[Landroid/graphics/Rect;

    move-object/from16 v31, v0

    aget-object v23, v31, v12

    .line 2249
    const-string v31, "tertiary_email"

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    const-string v31, "tertiary_email_type"

    const/16 v32, 0x2

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2251
    add-int/lit8 v13, v13, 0x1

    .line 2253
    goto/16 :goto_17

    :cond_49
    const/16 v31, 0xe

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_40

    .line 2254
    if-nez v9, :cond_40

    .line 2257
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 2258
    .restart local v27    # "value":Landroid/content/ContentValues;
    const-string v31, "mimetype"

    const-string v32, "vnd.android.cursor.item/website"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259
    const-string v31, "data1"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2261
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 2262
    .restart local v28    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2263
    const-string v31, "data"

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2264
    const/4 v9, 0x1

    goto/16 :goto_17

    .line 2280
    .end local v27    # "value":Landroid/content/ContentValues;
    .end local v28    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v29    # "wordText":Ljava/lang/String;
    .end local v30    # "wordType":I
    :cond_4a
    const-string v31, "photomode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath_for_contact:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281
    const-string v31, "is_businesscard_contact"

    const/16 v32, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v31

    if-nez v31, :cond_4b

    .line 2285
    const/16 v31, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 2287
    :cond_4b
    const/16 v31, 0x1

    goto/16 :goto_f
.end method

.method public scaleHighlightView()V
    .locals 6

    .prologue
    .line 1122
    const/4 v2, 0x0

    .line 1123
    .local v2, "oldRect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .line 1124
    .local v3, "scale":F
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    if-nez v4, :cond_1

    .line 1152
    :cond_0
    :goto_0
    return-void

    .line 1127
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mImageView:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/PostImageView;->getScale()F

    move-result v3

    .line 1128
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldScale:F

    div-float v0, v3, v4

    .line 1129
    .local v0, "delta":F
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->scaleRect(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1131
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_2

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nPostViewMode:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 1132
    :cond_2
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    if-eqz v4, :cond_3

    .line 1133
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfFoundLink:I

    if-lt v1, v4, :cond_6

    .line 1137
    .end local v1    # "i":I
    :cond_3
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    if-eqz v4, :cond_4

    .line 1138
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nNumberOfWholeWord:I

    if-lt v1, v4, :cond_7

    .line 1142
    .end local v1    # "i":I
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDetectedQRCode:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeRect:Landroid/graphics/Rect;

    if-eqz v4, :cond_5

    .line 1143
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nQRCodeRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->scaleRect(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1147
    :cond_5
    iput v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldScale:F

    .line 1148
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->getImageViewRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 1149
    if-eqz v2, :cond_0

    .line 1150
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mOldBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 1134
    .restart local v1    # "i":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedRecogWordRect:[Landroid/graphics/Rect;

    aget-object v4, v4, v1

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->scaleRect(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1133
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1139
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nWholeWordRect:[Landroid/graphics/Rect;

    aget-object v4, v4, v1

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/bcocr/PostViewActivity;->scaleRect(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1138
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public scaleRect(FLandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4
    .param p1, "delta"    # F
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1155
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    mul-float/2addr v3, p1

    float-to-int v3, v3

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1157
    return-object p2
.end method

.method public sendResult(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    const/4 v3, 0x0

    .line 1664
    const-string v0, "PostViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendResult, result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1665
    packed-switch p1, :pswitch_data_0

    .line 1687
    :goto_0
    :pswitch_0
    return-void

    .line 1669
    :pswitch_1
    const-string v0, "PostViewActivity"

    const-string v1, "send result : POSTVIEW_RESULT_CANCEL"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "postview_result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1671
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto :goto_0

    .line 1674
    :pswitch_2
    const-string v0, "PostViewActivity"

    const-string v1, "send result : POSTVIEW_RESULT_CANCEL SIZE IS BIGGER THAN 8M"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1675
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "postview_result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1676
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto :goto_0

    .line 1679
    :pswitch_3
    const/4 p1, 0x2

    .line 1680
    const-string v0, "PostViewActivity"

    const-string v1, "send result : GET_RESULT_ERROR (unsupported image format)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "postview_result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1682
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto :goto_0

    .line 1665
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setSelectedRectArea()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 950
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    .line 965
    :goto_0
    return-void

    .line 954
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 955
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    div-int v2, v0, v2

    .line 956
    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYBottomExtra:I

    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    mul-int/2addr v0, v4

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    div-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthPort:I

    mul-int/2addr v0, v4

    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    div-int v4, v0, v4

    .line 958
    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nYTopExtra:I

    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightPort:I

    mul-int/2addr v0, v6

    iget v6, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    div-int/2addr v0, v6

    add-int/2addr v0, v5

    .line 955
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 960
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRcSelectedArea:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXLeftExtra:I

    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    mul-int/2addr v0, v3

    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    div-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    mul-int/2addr v0, v3

    iget v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    div-int v3, v0, v3

    .line 962
    iget v4, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nXRightExtra:I

    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgWidthLand:I

    mul-int/2addr v0, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaWidth:I

    div-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->selectedAreaArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nDispImgHeightLand:I

    mul-int/2addr v0, v5

    iget v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->nSelectedAreaHeight:I

    div-int/2addr v0, v5

    .line 960
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0
.end method

.method public waitForRecognizeThread()V
    .locals 2

    .prologue
    .line 1797
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 1800
    :try_start_0
    sget-object v1, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 1801
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostViewActivity;->mRecognitionThreadIsWaiting:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1806
    :cond_0
    :goto_0
    return-void

    .line 1802
    :catch_0
    move-exception v0

    .line 1803
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
