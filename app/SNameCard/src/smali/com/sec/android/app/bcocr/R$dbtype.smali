.class public final Lcom/sec/android/app/bcocr/R$dbtype;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dbtype"
.end annotation


# static fields
.field public static final DEDT_BERLITZ_DATOENG:I = 0x7f0b00bc

.field public static final DEDT_BERLITZ_ENGTODA:I = 0x7f0b00bd

.field public static final DEDT_BERLITZ_ENGTOFI:I = 0x7f0b00bf

.field public static final DEDT_BERLITZ_ENGTONL:I = 0x7f0b00c1

.field public static final DEDT_BERLITZ_ENGTONO:I = 0x7f0b00c3

.field public static final DEDT_BERLITZ_ENGTOSV:I = 0x7f0b00c5

.field public static final DEDT_BERLITZ_ENGTOTUR:I = 0x7f0b00bb

.field public static final DEDT_BERLITZ_FITOENG:I = 0x7f0b00be

.field public static final DEDT_BERLITZ_NLTOENG:I = 0x7f0b00c0

.field public static final DEDT_BERLITZ_NOTOENG:I = 0x7f0b00c2

.field public static final DEDT_BERLITZ_STANDARD_ENGTUR:I = 0x7f0b00c7

.field public static final DEDT_BERLITZ_STANDARD_TURENG:I = 0x7f0b00c6

.field public static final DEDT_BERLITZ_SVTOENG:I = 0x7f0b00c4

.field public static final DEDT_BERLITZ_TURTOENG:I = 0x7f0b00ba

.field public static final DEDT_BRITANNICA_CONCISE:I = 0x7f0b0126

.field public static final DEDT_COLLINS_ARABTOENG:I = 0x7f0b003c

.field public static final DEDT_COLLINS_ENGGRAMMAR:I = 0x7f0b001d

.field public static final DEDT_COLLINS_ENGIDIOM:I = 0x7f0b001f

.field public static final DEDT_COLLINS_ENGTHES:I = 0x7f0b001b

.field public static final DEDT_COLLINS_ENGTOARAB:I = 0x7f0b003b

.field public static final DEDT_COLLINS_ENGTOENG:I = 0x7f0b001a

.field public static final DEDT_COLLINS_ENGTOENGCHNJPNKOR:I = 0x7f0b0034

.field public static final DEDT_COLLINS_ENGTOFRA:I = 0x7f0b0026

.field public static final DEDT_COLLINS_ENGTOGER:I = 0x7f0b0024

.field public static final DEDT_COLLINS_ENGTOGRE:I = 0x7f0b0030

.field public static final DEDT_COLLINS_ENGTOITA:I = 0x7f0b0028

.field public static final DEDT_COLLINS_ENGTOMAS:I = 0x7f0b002a

.field public static final DEDT_COLLINS_ENGTOPIC:I = 0x7f0b001e

.field public static final DEDT_COLLINS_ENGTOPOL:I = 0x7f0b0032

.field public static final DEDT_COLLINS_ENGTOPOR:I = 0x7f0b0022

.field public static final DEDT_COLLINS_ENGTOSPN:I = 0x7f0b0020

.field public static final DEDT_COLLINS_ENGUSAGE:I = 0x7f0b001c

.field public static final DEDT_COLLINS_FRATOENG:I = 0x7f0b0027

.field public static final DEDT_COLLINS_GERTOENG:I = 0x7f0b0025

.field public static final DEDT_COLLINS_GRETOENG:I = 0x7f0b0031

.field public static final DEDT_COLLINS_ITATOENG:I = 0x7f0b0029

.field public static final DEDT_COLLINS_KORTOTHA:I = 0x7f0b002c

.field public static final DEDT_COLLINS_KORTOVIE:I = 0x7f0b002e

.field public static final DEDT_COLLINS_MASTOENG:I = 0x7f0b002b

.field public static final DEDT_COLLINS_POLTOENG:I = 0x7f0b0033

.field public static final DEDT_COLLINS_PORTOENG:I = 0x7f0b0023

.field public static final DEDT_COLLINS_SMALL_ARABTOENG:I = 0x7f0b003a

.field public static final DEDT_COLLINS_SMALL_ENGTOARAB:I = 0x7f0b0039

.field public static final DEDT_COLLINS_SPNTOENG:I = 0x7f0b0021

.field public static final DEDT_COLLINS_THATOKOR:I = 0x7f0b002d

.field public static final DEDT_COLLINS_UNABRIDGED_ENGTOSPA:I = 0x7f0b0035

.field public static final DEDT_COLLINS_UNABRIDGED_SPATOENG:I = 0x7f0b0036

.field public static final DEDT_COLLINS_UNA_ENGTOGER:I = 0x7f0b0037

.field public static final DEDT_COLLINS_UNA_GERTOENG:I = 0x7f0b0038

.field public static final DEDT_COLLINS_VIETOKOR:I = 0x7f0b002f

.field public static final DEDT_DD_ALZZA_BUSINESS_WORD:I = 0x7f0b009a

.field public static final DEDT_DD_CHNTOKOR:I = 0x7f0b0093

.field public static final DEDT_DD_ENCYBER_HISTORY_CHN:I = 0x7f0b00a6

.field public static final DEDT_DD_ENCYBER_HISTORY_EASTERN:I = 0x7f0b00a9

.field public static final DEDT_DD_ENCYBER_HISTORY_FRA:I = 0x7f0b00a7

.field public static final DEDT_DD_ENCYBER_HISTORY_GER:I = 0x7f0b00a1

.field public static final DEDT_DD_ENCYBER_HISTORY_KOR:I = 0x7f0b00a8

.field public static final DEDT_DD_ENCYBER_HISTORY_RUS:I = 0x7f0b00a2

.field public static final DEDT_DD_ENCYBER_HISTORY_UK:I = 0x7f0b00a5

.field public static final DEDT_DD_ENCYBER_HISTORY_US:I = 0x7f0b00a3

.field public static final DEDT_DD_ENCYBER_HISTORY_WESTERN:I = 0x7f0b00a4

.field public static final DEDT_DD_ENCYBER_HISTORY_WORLD:I = 0x7f0b00aa

.field public static final DEDT_DD_ENGTOKOR:I = 0x7f0b008c

.field public static final DEDT_DD_ENGTOKOR_EXAMPLE:I = 0x7f0b008f

.field public static final DEDT_DD_ENGTOKOR_IDIOM:I = 0x7f0b008e

.field public static final DEDT_DD_FRATOKOR:I = 0x7f0b009e

.field public static final DEDT_DD_GENX_SUNUNG_ENGTOKOR:I = 0x7f0b0098

.field public static final DEDT_DD_GERTOKOR:I = 0x7f0b00a0

.field public static final DEDT_DD_JPNTOKOR:I = 0x7f0b0096

.field public static final DEDT_DD_KANJITOKOR:I = 0x7f0b0097

.field public static final DEDT_DD_KORTOCHN:I = 0x7f0b0092

.field public static final DEDT_DD_KORTOENG:I = 0x7f0b008d

.field public static final DEDT_DD_KORTOFRA:I = 0x7f0b009d

.field public static final DEDT_DD_KORTOGER:I = 0x7f0b009f

.field public static final DEDT_DD_KORTOJPN:I = 0x7f0b0095

.field public static final DEDT_DD_KORTOKOR:I = 0x7f0b0090

.field public static final DEDT_DD_OLDKOR:I = 0x7f0b0091

.field public static final DEDT_DD_PINYINTOKOR:I = 0x7f0b0094

.field public static final DEDT_DD_TOEIC_KONGNAMUL_WORD:I = 0x7f0b0099

.field public static final DEDT_DD_YONSEI_ENGTOKOR:I = 0x7f0b009c

.field public static final DEDT_DD_YONSEI_KORTOKOR:I = 0x7f0b009b

.field public static final DEDT_DRWIT_ENGTOTHA:I = 0x7f0b00e8

.field public static final DEDT_DRWIT_POCKET_ENGTOTHA:I = 0x7f0b00e6

.field public static final DEDT_DRWIT_POCKET_THATOENG:I = 0x7f0b00e7

.field public static final DEDT_DRWIT_THATOENG:I = 0x7f0b00e9

.field public static final DEDT_FNAG_ENGTOGLE:I = 0x7f0b00df

.field public static final DEDT_FNAG_GLETOENG:I = 0x7f0b00e0

.field public static final DEDT_GRAMEDIA_ENGTOIND:I = 0x7f0b00ce

.field public static final DEDT_GRAMEDIA_INDTOENG:I = 0x7f0b00cf

.field public static final DEDT_GUMMERUS_ENG_FIN:I = 0x7f0b00f0

.field public static final DEDT_GUMMERUS_FIN_ENG:I = 0x7f0b00f1

.field public static final DEDT_GUMMERUS_POCKET_ENG_FIN:I = 0x7f0b00ee

.field public static final DEDT_GUMMERUS_POCKET_FIN_ENG:I = 0x7f0b00ef

.field public static final DEDT_GYLDENDAL_DEN_ENG:I = 0x7f0b00f7

.field public static final DEDT_GYLDENDAL_ENG_DEN:I = 0x7f0b00f6

.field public static final DEDT_GYLDENDAL_MINI_DEN_ENG:I = 0x7f0b00f9

.field public static final DEDT_GYLDENDAL_MINI_ENG_DEN:I = 0x7f0b00f8

.field public static final DEDT_KSSP_ENGTOSIMP:I = 0x7f0b0059

.field public static final DEDT_KSSP_ENGTOSIMPTRAD:I = 0x7f0b0061

.field public static final DEDT_KSSP_ENGTOSIMPTRAD_SIMP:I = 0x7f0b0062

.field public static final DEDT_KSSP_ENGTOSIMPTRAD_TRAD:I = 0x7f0b0063

.field public static final DEDT_KSSP_ENGTOTRAD:I = 0x7f0b005d

.field public static final DEDT_KSSP_INITIAL_SIMPTOENG:I = 0x7f0b005c

.field public static final DEDT_KSSP_INITIAL_SIMPTRADTOENG_SIMP:I = 0x7f0b0068

.field public static final DEDT_KSSP_INITIAL_SIMPTRADTOENG_TRAD:I = 0x7f0b006a

.field public static final DEDT_KSSP_INITIAL_TRADTOENG:I = 0x7f0b0060

.field public static final DEDT_KSSP_PINYIN_SIMPTOENG:I = 0x7f0b005b

.field public static final DEDT_KSSP_PINYIN_SIMPTRADTOENG_SIMP:I = 0x7f0b0067

.field public static final DEDT_KSSP_PINYIN_SIMPTRADTOENG_TRAD:I = 0x7f0b0069

.field public static final DEDT_KSSP_PINYIN_TRADTOENG:I = 0x7f0b005f

.field public static final DEDT_KSSP_SIMPTOENG:I = 0x7f0b005a

.field public static final DEDT_KSSP_SIMPTRADTOENG:I = 0x7f0b0064

.field public static final DEDT_KSSP_SIMPTRADTOENG_SIMP:I = 0x7f0b0065

.field public static final DEDT_KSSP_SIMPTRADTOENG_TRAD:I = 0x7f0b0066

.field public static final DEDT_KSSP_TRADTOENG:I = 0x7f0b005e

.field public static final DEDT_LACVIET_ENGTOVIE:I = 0x7f0b00d0

.field public static final DEDT_LACVIET_KORTOVIE:I = 0x7f0b00e1

.field public static final DEDT_LACVIET_VIETOENG:I = 0x7f0b00d1

.field public static final DEDT_LACVIET_VIETOKOR:I = 0x7f0b00e2

.field public static final DEDT_LANGEN_ENGTOGER:I = 0x7f0b00b0

.field public static final DEDT_LANGEN_FRATOGER:I = 0x7f0b00b2

.field public static final DEDT_LANGEN_GERTOENG:I = 0x7f0b00b1

.field public static final DEDT_LANGEN_GERTOFRA:I = 0x7f0b00b3

.field public static final DEDT_LANGEN_GERTOITA:I = 0x7f0b00b5

.field public static final DEDT_LANGEN_GERTOSPN:I = 0x7f0b00b7

.field public static final DEDT_LANGEN_GERTOTUR:I = 0x7f0b00b9

.field public static final DEDT_LANGEN_ITATOGER:I = 0x7f0b00b4

.field public static final DEDT_LANGEN_SPNTOGER:I = 0x7f0b00b6

.field public static final DEDT_LANGEN_TURTOGER:I = 0x7f0b00b8

.field public static final DEDT_LDSOFT_ENGTOSIMP:I = 0x7f0b0127

.field public static final DEDT_LDSOFT_ENGTOSIMPTRAD:I = 0x7f0b012b

.field public static final DEDT_LDSOFT_ENGTOTRAD:I = 0x7f0b0129

.field public static final DEDT_LDSOFT_SIMPTOENG:I = 0x7f0b0128

.field public static final DEDT_LDSOFT_SIMPTRADTOENG:I = 0x7f0b012c

.field public static final DEDT_LDSOFT_TRADTOENG:I = 0x7f0b012a

.field public static final DEDT_LINGVO_ENGTORUS:I = 0x7f0b003d

.field public static final DEDT_LINGVO_ENGTOUKR:I = 0x7f0b0041

.field public static final DEDT_LINGVO_GERTORUS:I = 0x7f0b0044

.field public static final DEDT_LINGVO_RUSTOENG:I = 0x7f0b003e

.field public static final DEDT_LINGVO_RUSTOGER:I = 0x7f0b0043

.field public static final DEDT_LINGVO_RUSTOSPN:I = 0x7f0b0045

.field public static final DEDT_LINGVO_RUSTOUKR:I = 0x7f0b003f

.field public static final DEDT_LINGVO_SPNTORUS:I = 0x7f0b0046

.field public static final DEDT_LINGVO_UKRTOENG:I = 0x7f0b0042

.field public static final DEDT_LINGVO_UKRTORUS:I = 0x7f0b0040

.field public static final DEDT_MACMILLAN_ENGTOENG:I = 0x7f0b0125

.field public static final DEDT_MALAYIN_ARATOENG:I = 0x7f0b00cd

.field public static final DEDT_MALAYIN_ENGTOARA:I = 0x7f0b00cc

.field public static final DEDT_MANTOU_INITIAL_SIMPTOKOR:I = 0x7f0b0120

.field public static final DEDT_MANTOU_INITIAL_TRADTOKOR:I = 0x7f0b0124

.field public static final DEDT_MANTOU_KORTOSIMP:I = 0x7f0b011d

.field public static final DEDT_MANTOU_KORTOTRAD:I = 0x7f0b0121

.field public static final DEDT_MANTOU_PINYIN_SIMPTOKOR:I = 0x7f0b011f

.field public static final DEDT_MANTOU_PINYIN_TRADTOKOR:I = 0x7f0b0123

.field public static final DEDT_MANTOU_SIMPTOKOR:I = 0x7f0b011e

.field public static final DEDT_MANTOU_TRADTOKOR:I = 0x7f0b0122

.field public static final DEDT_MAX:I = 0x7f0b012f

.field public static final DEDT_MIRROR_OXFORD_MINI_MAL_ENG:I = 0x7f0b0131

.field public static final DEDT_MOTECH_CHNTOENG:I = 0x7f0b0109

.field public static final DEDT_MOTECH_ENGTOCHN:I = 0x7f0b0108

.field public static final DEDT_MOTECH_ENGTOSIMP:I = 0x7f0b010c

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD:I = 0x7f0b0113

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD_SIMP:I = 0x7f0b0114

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD_TRAD:I = 0x7f0b0115

.field public static final DEDT_MOTECH_ENGTOTRAD:I = 0x7f0b010f

.field public static final DEDT_MOTECH_INITIAL_SIMPTOENG:I = 0x7f0b010b

.field public static final DEDT_MOTECH_INITIAL_SIMPTRADTOENG_SIMP:I = 0x7f0b011a

.field public static final DEDT_MOTECH_INITIAL_SIMPTRADTOENG_TRAD:I = 0x7f0b011c

.field public static final DEDT_MOTECH_INITIAL_TRADTOENG:I = 0x7f0b0112

.field public static final DEDT_MOTECH_PINYINTOENG:I = 0x7f0b010a

.field public static final DEDT_MOTECH_PINYIN_SIMPTOENG:I = 0x7f0b010e

.field public static final DEDT_MOTECH_PINYIN_SIMPTRADTOENG_SIMP:I = 0x7f0b0119

.field public static final DEDT_MOTECH_PINYIN_SIMPTRADTOENG_TRAD:I = 0x7f0b011b

.field public static final DEDT_MOTECH_PINYIN_TRADTOENG:I = 0x7f0b0111

.field public static final DEDT_MOTECH_SIMPTOENG:I = 0x7f0b010d

.field public static final DEDT_MOTECH_SIMPTRADTOENG:I = 0x7f0b0116

.field public static final DEDT_MOTECH_SIMPTRADTOENG_SIMP:I = 0x7f0b0117

.field public static final DEDT_MOTECH_SIMPTRADTOENG_TRAD:I = 0x7f0b0118

.field public static final DEDT_MOTECH_TRADTOENG:I = 0x7f0b0110

.field public static final DEDT_MYDIC_KSC:I = 0x7f0b012d

.field public static final DEDT_MYDIC_UCS2:I = 0x7f0b012e

.field public static final DEDT_NATKOREAN_KORTOKOR:I = 0x7f0b00e3

.field public static final DEDT_NATKOREAN_OLDKOR:I = 0x7f0b00e4

.field public static final DEDT_NEWACE_ENGTOKOR:I = 0x7f0b0102

.field public static final DEDT_NEWACE_ENGTOKOR_EXAMPLE:I = 0x7f0b0106

.field public static final DEDT_NEWACE_ENGTOKOR_IDIOM:I = 0x7f0b0105

.field public static final DEDT_NEWACE_JPNTOKOR:I = 0x7f0b00ff

.field public static final DEDT_NEWACE_KANJITOKOR:I = 0x7f0b0100

.field public static final DEDT_NEWACE_KORTOENG:I = 0x7f0b0103

.field public static final DEDT_NEWACE_KORTOJPN:I = 0x7f0b00fe

.field public static final DEDT_NEWACE_KORTOKOR:I = 0x7f0b0101

.field public static final DEDT_NEWACE_OLDKOR:I = 0x7f0b0104

.field public static final DEDT_NORSTEDTS_ENG_SWE:I = 0x7f0b00ec

.field public static final DEDT_NORSTEDTS_POCKET_ENG_SWE:I = 0x7f0b00ea

.field public static final DEDT_NORSTEDTS_POCKET_SWE_ENG:I = 0x7f0b00eb

.field public static final DEDT_NORSTEDTS_SWE_ENG:I = 0x7f0b00ed

.field public static final DEDT_OBUNSHA_ENGTOJPN:I = 0x7f0b00ad

.field public static final DEDT_OBUNSHA_JPNTOENG:I = 0x7f0b00ae

.field public static final DEDT_OBUNSHA_JPNTOJPN:I = 0x7f0b00ab

.field public static final DEDT_OBUNSHA_KANJITOENG:I = 0x7f0b00af

.field public static final DEDT_OBUNSHA_KANJITOJPN:I = 0x7f0b00ac

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR:I = 0x7f0b0089

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR_EXAMPLE:I = 0x7f0b008b

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR_IDIOM:I = 0x7f0b008a

.field public static final DEDT_OXFORDNEWACE_KORTOENG:I = 0x7f0b0107

.field public static final DEDT_OXFORD_AMERICAN_COLLEGE_DICTIONARY:I = 0x7f0b0074

.field public static final DEDT_OXFORD_CHN_ENG:I = 0x7f0b007f

.field public static final DEDT_OXFORD_CHN_ENG_MINI:I = 0x7f0b007b

.field public static final DEDT_OXFORD_COLLOCATIONS:I = 0x7f0b006d

.field public static final DEDT_OXFORD_CONCISE:I = 0x7f0b006f

.field public static final DEDT_OXFORD_ENGTOKOR:I = 0x7f0b006e

.field public static final DEDT_OXFORD_ENGTOKOR_EXAMPLE:I = 0x7f0b0076

.field public static final DEDT_OXFORD_ENGTOKOR_IDIOM:I = 0x7f0b0075

.field public static final DEDT_OXFORD_ENG_CHN:I = 0x7f0b007e

.field public static final DEDT_OXFORD_ENG_CHN_MINI:I = 0x7f0b0072

.field public static final DEDT_OXFORD_ENG_JPN_MINI:I = 0x7f0b0071

.field public static final DEDT_OXFORD_ENG_MAL:I = 0x7f0b0086

.field public static final DEDT_OXFORD_ENG_MINI:I = 0x7f0b0070

.field public static final DEDT_OXFORD_FLTRP_CHN_ENG:I = 0x7f0b0082

.field public static final DEDT_OXFORD_FLTRP_ENG_CHN:I = 0x7f0b0081

.field public static final DEDT_OXFORD_FLTRP_PINYIN_CHN_ENG:I = 0x7f0b0083

.field public static final DEDT_OXFORD_INITIAL_CHN_ENG_MINI:I = 0x7f0b007d

.field public static final DEDT_OXFORD_JPN_ENG_MINI:I = 0x7f0b0079

.field public static final DEDT_OXFORD_KANJITOENG_MINI:I = 0x7f0b007a

.field public static final DEDT_OXFORD_MINI_ENG_MAL:I = 0x7f0b0084

.field public static final DEDT_OXFORD_MINI_MAL_ENG:I = 0x7f0b0085

.field public static final DEDT_OXFORD_NEW_AMERICAN_DICTIONARY:I = 0x7f0b0073

.field public static final DEDT_OXFORD_OALD:I = 0x7f0b006b

.field public static final DEDT_OXFORD_OALD_EXAMPLE:I = 0x7f0b0078

.field public static final DEDT_OXFORD_OALD_IDIOM:I = 0x7f0b0077

.field public static final DEDT_OXFORD_PINYIN_CHN_ENG:I = 0x7f0b0080

.field public static final DEDT_OXFORD_PINYIN_CHN_ENG_MINI:I = 0x7f0b007c

.field public static final DEDT_OXFORD_THESAURUS:I = 0x7f0b006c

.field public static final DEDT_OXFORD_TRAD_CHN_ENG:I = 0x7f0b0088

.field public static final DEDT_OXFORD_TRAD_ENG_CHN:I = 0x7f0b0087

.field public static final DEDT_PAIBOON_ENGTOTHA:I = 0x7f0b00d2

.field public static final DEDT_PAIBOON_THAPHONTOENG:I = 0x7f0b00d4

.field public static final DEDT_PAIBOON_THATOENG:I = 0x7f0b00d3

.field public static final DEDT_STARPUBLICATIONS_BENTOENG:I = 0x7f0b00d8

.field public static final DEDT_STARPUBLICATIONS_ENGTOBEN:I = 0x7f0b00d7

.field public static final DEDT_STARPUBLICATIONS_ENGTOHIN:I = 0x7f0b00d5

.field public static final DEDT_STARPUBLICATIONS_ENGTOPER:I = 0x7f0b00db

.field public static final DEDT_STARPUBLICATIONS_ENGTOTGL:I = 0x7f0b00dd

.field public static final DEDT_STARPUBLICATIONS_ENGTOURD:I = 0x7f0b00d9

.field public static final DEDT_STARPUBLICATIONS_HINTOENG:I = 0x7f0b00d6

.field public static final DEDT_STARPUBLICATIONS_PERTOENG:I = 0x7f0b00dc

.field public static final DEDT_STARPUBLICATIONS_TGLTOENG:I = 0x7f0b00de

.field public static final DEDT_STARPUBLICATIONS_URDARABICTOENG:I = 0x7f0b00e5

.field public static final DEDT_STARPUBLICATIONS_URDTOENG:I = 0x7f0b00da

.field public static final DEDT_TOTAL_SEARCH:I = 0x7f0b0130

.field public static final DEDT_VANDALE_ENG_NETH:I = 0x7f0b00fc

.field public static final DEDT_VANDALE_MINI_ENG_NETH:I = 0x7f0b00fa

.field public static final DEDT_VANDALE_MINI_NETH_ENG:I = 0x7f0b00fb

.field public static final DEDT_VANDALE_NETH_ENG:I = 0x7f0b00fd

.field public static final DEDT_VEGA_FORLAG_ENGTONOR:I = 0x7f0b00f4

.field public static final DEDT_VEGA_FORLAG_NORTOENG:I = 0x7f0b00f5

.field public static final DEDT_VEGA_FORLAG_POCKET_ENGTONOR:I = 0x7f0b00f2

.field public static final DEDT_VEGA_FORLAG_POCKET_NORTOENG:I = 0x7f0b00f3

.field public static final DEDT_VOX_ENGTOSPN:I = 0x7f0b00ca

.field public static final DEDT_VOX_FRATOSPN:I = 0x7f0b00c8

.field public static final DEDT_VOX_SPNTOENG:I = 0x7f0b00cb

.field public static final DEDT_VOX_SPNTOFRA:I = 0x7f0b00c9

.field public static final DEDT_WYS_ENGTOSIMP:I = 0x7f0b0047

.field public static final DEDT_WYS_ENGTOSIMPTRAD:I = 0x7f0b004f

.field public static final DEDT_WYS_ENGTOSIMPTRAD_SIMP:I = 0x7f0b0050

.field public static final DEDT_WYS_ENGTOSIMPTRAD_TRAD:I = 0x7f0b0051

.field public static final DEDT_WYS_ENGTOTRAD:I = 0x7f0b004b

.field public static final DEDT_WYS_INITIAL_SIMPTOENG:I = 0x7f0b004a

.field public static final DEDT_WYS_INITIAL_SIMPTRADTOENG_SIMP:I = 0x7f0b0056

.field public static final DEDT_WYS_INITIAL_SIMPTRADTOENG_TRAD:I = 0x7f0b0058

.field public static final DEDT_WYS_INITIAL_TRADTOENG:I = 0x7f0b004e

.field public static final DEDT_WYS_PINYIN_SIMPTOENG:I = 0x7f0b0049

.field public static final DEDT_WYS_PINYIN_SIMPTRADTOENG_SIMP:I = 0x7f0b0055

.field public static final DEDT_WYS_PINYIN_SIMPTRADTOENG_TRAD:I = 0x7f0b0057

.field public static final DEDT_WYS_PINYIN_TRADTOENG:I = 0x7f0b004d

.field public static final DEDT_WYS_SIMPTOENG:I = 0x7f0b0048

.field public static final DEDT_WYS_SIMPTRADTOENG:I = 0x7f0b0052

.field public static final DEDT_WYS_SIMPTRADTOENG_SIMP:I = 0x7f0b0053

.field public static final DEDT_WYS_SIMPTRADTOENG_TRAD:I = 0x7f0b0054

.field public static final DEDT_WYS_TRADTOENG:I = 0x7f0b004c

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR:I = 0x7f0b0004

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR_EXAMPLE:I = 0x7f0b0007

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR_IDIOM:I = 0x7f0b0006

.field public static final DEDT_YBM_ALLINALL_JPNTOKOR:I = 0x7f0b0018

.field public static final DEDT_YBM_ALLINALL_KANJITOKOR:I = 0x7f0b0019

.field public static final DEDT_YBM_ALLINALL_KORTOENG:I = 0x7f0b0005

.field public static final DEDT_YBM_ALLINALL_KORTOJPN:I = 0x7f0b0017

.field public static final DEDT_YBM_BUSINESS:I = 0x7f0b0012

.field public static final DEDT_YBM_CHNTOKOR:I = 0x7f0b000b

.field public static final DEDT_YBM_E4U_ENGTOKOR:I = 0x7f0b0000

.field public static final DEDT_YBM_E4U_ENGTOKOR_EXAMPLE:I = 0x7f0b0003

.field public static final DEDT_YBM_E4U_ENGTOKOR_IDIOM:I = 0x7f0b0002

.field public static final DEDT_YBM_E4U_KORTOENG:I = 0x7f0b0001

.field public static final DEDT_YBM_ENGTOENG:I = 0x7f0b0014

.field public static final DEDT_YBM_JPNTOKOR:I = 0x7f0b000e

.field public static final DEDT_YBM_JUNIOR:I = 0x7f0b0015

.field public static final DEDT_YBM_KANJITOKOR:I = 0x7f0b000f

.field public static final DEDT_YBM_KORTOCHN:I = 0x7f0b000a

.field public static final DEDT_YBM_KORTOJPN:I = 0x7f0b000d

.field public static final DEDT_YBM_KORTOKOR:I = 0x7f0b0008

.field public static final DEDT_YBM_MIDDLE_WORD:I = 0x7f0b0016

.field public static final DEDT_YBM_OLDKOR:I = 0x7f0b0009

.field public static final DEDT_YBM_PINYINTOKOR:I = 0x7f0b000c

.field public static final DEDT_YBM_SUNUNG_WORD:I = 0x7f0b0010

.field public static final DEDT_YBM_TOEIC_IDIOM:I = 0x7f0b0013

.field public static final DEDT_YBM_TOEIC_WORD:I = 0x7f0b0011


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
