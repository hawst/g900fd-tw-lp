.class Lcom/sec/android/app/bcocr/OCR$3;
.super Landroid/os/Handler;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 1141
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/OCR$3;)Lcom/sec/android/app/bcocr/OCR;
    .locals 1

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v4, 0x7f0c0032

    const/4 v3, 0x0

    .line 1144
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] updateCheckHandler Message: what= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    if-nez v0, :cond_0

    .line 1147
    const-string v0, "OCR"

    const-string v1, "[OCR] updateCheckHandler Message: updateCheck thread is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    :goto_0
    return-void

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1152
    const-string v0, "OCR"

    const-string v1, "[OCR] updateCheckHandler Message: updateCheck thread is not running"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1156
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$0(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1157
    const-string v0, "OCR"

    const-string v1, "[OCR] updateCheckHandler - camera is destroying"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1161
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1275
    :pswitch_0
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCheckHandler : Unknown message : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1163
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const v1, 0x7f0c0033

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1164
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1167
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1168
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1169
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1171
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->access$5(Lcom/sec/android/app/bcocr/OCR;Landroid/app/ProgressDialog;)V

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 1173
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 1174
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const v2, 0x7f0c0024

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/bcocr/OCR$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$3$1;-><init>(Lcom/sec/android/app/bcocr/OCR$3;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, -0x2

    .line 1181
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const v3, 0x7f0c001a

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1182
    new-instance v3, Lcom/sec/android/app/bcocr/OCR$3$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$3$2;-><init>(Lcom/sec/android/app/bcocr/OCR$3;)V

    .line 1180
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1191
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    .line 1192
    new-instance v1, Lcom/sec/android/app/bcocr/OCR$3$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$3$3;-><init>(Lcom/sec/android/app/bcocr/OCR$3;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/bcocr/OCR$3$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$3$4;-><init>(Lcom/sec/android/app/bcocr/OCR$3;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0

    .line 1236
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1240
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v0, v3}, Lcom/sec/android/app/bcocr/OCR;->access$5(Lcom/sec/android/app/bcocr/OCR;Landroid/app/ProgressDialog;)V

    .line 1242
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCRUtils;->onLaunchBCOCRDownload(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1248
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1250
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1252
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v0, v3}, Lcom/sec/android/app/bcocr/OCR;->access$5(Lcom/sec/android/app/bcocr/OCR;Landroid/app/ProgressDialog;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$6(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$6(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1256
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$6(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1259
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v0, v3}, Lcom/sec/android/app/bcocr/OCR;->access$7(Lcom/sec/android/app/bcocr/OCR;Landroid/app/AlertDialog;)V

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 1262
    sget v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1263
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1264
    const v2, 0x7f0c0023

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1265
    const v2, 0x7f0c001b

    .line 1266
    new-instance v3, Lcom/sec/android/app/bcocr/OCR$3$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$3$5;-><init>(Lcom/sec/android/app/bcocr/OCR$3;)V

    .line 1265
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1271
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1261
    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->access$7(Lcom/sec/android/app/bcocr/OCR;Landroid/app/AlertDialog;)V

    goto/16 :goto_0

    .line 1161
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
