.class public Lcom/sec/android/app/bcocr/CeStateInitializing;
.super Lcom/sec/android/app/bcocr/AbstractCeState;
.source "CeStateInitializing.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CeStateInitializing"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V
    .locals 0
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;
    .param p2, "requestQueue"    # Lcom/sec/android/app/bcocr/CeRequestQueue;
    .param p3, "id"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/AbstractCeState;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public cancelRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    .line 35
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 58
    const-string v0, "CeStateInitializing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HandleMessage - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 68
    :goto_0
    return-void

    .line 61
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 65
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    const/4 v1, 0x1

    .line 39
    const-string v0, "CeStateInitializing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HandleRequest - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 50
    const-string v0, "CeStateInitializing"

    const-string v1, "invalid request id for current state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    .line 53
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getParam()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/bcocr/OCREngine;->doWaitAsync(I)V

    move v0, v1

    .line 43
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doProcessBackSync()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateInitializing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 47
    goto :goto_0

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x18 -> :sswitch_1
    .end sparse-switch
.end method
