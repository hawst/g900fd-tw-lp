.class public Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;
.super Landroid/os/Handler;
.source "OCRSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCRSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "NotificationHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCRSettings;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/bcocr/OCRSettings;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;->this$0:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 283
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;->this$0:Lcom/sec/android/app/bcocr/OCRSettings;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->handleNotification(II)V

    .line 286
    :cond_0
    return-void
.end method
