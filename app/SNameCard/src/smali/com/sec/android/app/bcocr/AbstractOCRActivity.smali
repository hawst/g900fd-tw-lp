.class public abstract Lcom/sec/android/app/bcocr/AbstractOCRActivity;
.super Landroid/app/Activity;
.source "AbstractOCRActivity.java"

# interfaces
.implements Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;


# static fields
.field protected static final CAM_AVAILABLE_LOW_TEMP:I = -0x64

.field protected static final CAM_FLASH_AVAILABLE_TEMP:I = -0x32

.field protected static final CHECK_CALL_DLG:I = 0x1

.field protected static final DLG_HIDE:Z = false

.field protected static final DLG_SHOW:Z = true

.field protected static final INACTIVITY_TIMEOUT:I = 0x78

.field protected static final INACTIVITY_TIMER_EXPIRED:I = 0x1

.field protected static LOW_BATTERY_THRESHOLD_VALUE:I = 0x0

.field protected static LOW_BATTERY_WARNING_LEVEL:I = 0x0

.field protected static LOW_TEMP_FLASH_THRESHOLD_VALUE:I = 0x0

.field protected static final MDNIE_CAMERA_MODE:I = 0x4

.field protected static final MDNIE_UI_MODE:I = 0x0

.field protected static final MILLIS_IN_SEC:I = 0x3e8

.field protected static final NUM_OF_DLG:I = 0x2

.field public static final OCR_FROM_NAMECARD_CAPTURE:I = 0x7da

.field public static final OCR_FROM_SIP:I = 0x7d7

.field public static final ONLY_FILEPATH:I = 0x3e8

.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I = 0x0

.field protected static final OVERHEAT_TIMEOUT:I = 0x5

.field protected static final OVERHEAT_TIMER_EXPIRED:I = 0x2

.field public static final PICK_FROM_ALBUM:I = 0x7d8

.field public static final REQUEST_OCR_DB_INSTALL:I = 0x7d9

.field protected static final STORAGE_STATUS_DLG:I = 0x0

.field protected static final STORAGE_STATUS_LOW:I = 0x1

.field protected static final STORAGE_STATUS_NONE:I = 0x2

.field protected static final STORAGE_STATUS_OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AbstractOCRActivity"

.field protected static mOCRContextNum:I

.field private static mOrientation:I


# instance fields
.field protected bFlagOverheat:Z

.field protected bIsCharging:Z

.field protected bTurnOnScrAB:Z

.field protected mAudioManager:Landroid/media/AudioManager;

.field protected mAutoCaptureEnabled:Z

.field protected mBaseLayout:Landroid/view/ViewGroup;

.field protected mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

.field protected mBatteryLevel:I

.field protected mBufferOverFlowPopup:Landroid/app/AlertDialog;

.field protected mCheckCalling:Z

.field protected mCheckMemory:Lcom/sec/android/app/bcocr/CheckMemory;

.field protected mCheckVoIPCalling:Z

.field protected mCommandIdMap:Lcom/sec/android/app/bcocr/command/CommandIdMap;

.field private mDlgStatus:[Z

.field protected mErrorPopup:Landroid/app/AlertDialog;

.field private mHideWaitingAnimationRunnable:Ljava/lang/Runnable;

.field protected mInactivityPopupHandler:Landroid/os/Handler;

.field protected mIsEdgeDetected:Z

.field protected mLayout:Landroid/widget/RelativeLayout;

.field protected mLowBatteryPopup:Landroid/app/AlertDialog;

.field protected mMainHandler:Landroid/os/Handler;

.field private mMediaMetadataRetriever:Landroid/media/MediaMetadataRetriever;

.field protected mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

.field protected mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

.field protected mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

.field protected mOCRFlashModeOn:Z

.field protected mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

.field protected mOnResumePending:Z

.field protected mOpenFailedPopup:Landroid/app/AlertDialog;

.field protected mOverheatPopup:Landroid/app/AlertDialog;

.field protected mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

.field protected mRecordingFailedPopup:Landroid/app/AlertDialog;

.field protected mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

.field protected mSpinnerDialog:Landroid/app/Dialog;

.field protected mStorageStatus:I

.field protected mSubMain:Landroid/widget/RelativeLayout;

.field protected mUsbMassStorageEnabled:Z

.field protected mViewStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/sec/android/app/bcocr/MenuBase;",
            ">;"
        }
    .end annotation
.end field

.field protected mbNeedToStartEngineSync:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOrientation:I

    .line 108
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    .line 122
    const/16 v0, 0xf

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_BATTERY_WARNING_LEVEL:I

    .line 123
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_BATTERY_THRESHOLD_VALUE:I

    .line 124
    const/16 v0, 0x64

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_TEMP_FLASH_THRESHOLD_VALUE:I

    .line 177
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 260
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 63
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    .line 89
    iput v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCheckCalling:Z

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mUsbMassStorageEnabled:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCheckVoIPCalling:Z

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    .line 103
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMediaMetadataRetriever:Landroid/media/MediaMetadataRetriever;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOpenFailedPopup:Landroid/app/AlertDialog;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mRecordingFailedPopup:Landroid/app/AlertDialog;

    .line 117
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBufferOverFlowPopup:Landroid/app/AlertDialog;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    .line 120
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 131
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCheckMemory:Lcom/sec/android/app/bcocr/CheckMemory;

    .line 140
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mbNeedToStartEngineSync:Z

    .line 142
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mIsEdgeDetected:Z

    .line 143
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLayout:Landroid/widget/RelativeLayout;

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mAutoCaptureEnabled:Z

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 149
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRFlashModeOn:Z

    .line 151
    new-instance v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$1;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    .line 163
    new-instance v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$2;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    .line 180
    new-instance v0, Lcom/sec/android/app/bcocr/command/CommandIdMap;

    invoke-direct {v0}, Lcom/sec/android/app/bcocr/command/CommandIdMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCommandIdMap:Lcom/sec/android/app/bcocr/command/CommandIdMap;

    .line 182
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    .line 266
    new-instance v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$3;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

    .line 278
    new-instance v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$4;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mHideWaitingAnimationRunnable:Ljava/lang/Runnable;

    .line 261
    return-void
.end method

.method public static getOCRActivityOrientation()I
    .locals 1

    .prologue
    .line 506
    sget v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOrientation:I

    return v0
.end method


# virtual methods
.method public checkStorageLow(I)I
    .locals 6
    .param p1, "storage"    # I

    .prologue
    .line 905
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getAvailableStorage(I)J

    move-result-wide v0

    .line 906
    .local v0, "lAvailableStorage":J
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 907
    const/4 v2, 0x2

    .line 913
    :goto_0
    return v2

    .line 910
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraQuality()I

    move-result v3

    .line 909
    invoke-static {v2, v3}, Lcom/sec/android/app/bcocr/CheckMemory;->getMaxSizeOfImage(II)J

    move-result-wide v2

    sub-long v2, v0, v2

    .line 910
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 911
    const/4 v2, 0x1

    goto :goto_0

    .line 913
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public abstract checkStorageLow()V
.end method

.method public dumpViewStack()Ljava/lang/String;
    .locals 5

    .prologue
    .line 412
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 413
    .local v1, "log":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 414
    .local v2, "view":Lcom/sec/android/app/bcocr/MenuBase;
    iget-object v4, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    move-result v3

    .line 415
    .local v3, "viewSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 429
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 416
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v4, v0}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "view":Lcom/sec/android/app/bcocr/MenuBase;
    check-cast v2, Lcom/sec/android/app/bcocr/MenuBase;

    .line 417
    .restart local v2    # "view":Lcom/sec/android/app/bcocr/MenuBase;
    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 419
    const-string v4, " => "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    const-string v4, "(Z:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 423
    const-string v4, ",C:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->isCaptureEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 425
    const-string v4, ",P:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->isPreviewTouchEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 427
    const-string v4, ")\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public finishOnError(I)V
    .locals 7
    .param p1, "error"    # I

    .prologue
    const v6, 0x7f0c0007

    const v5, 0x7f0c001b

    .line 818
    const-string v2, "AbstractOCRActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "finishOnError [Error type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 821
    const-string v2, "AbstractOCRActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OCR is finished [Error type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :goto_0
    return-void

    .line 825
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 826
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 828
    packed-switch p1, :pswitch_data_0

    .line 877
    :pswitch_0
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 882
    :goto_1
    :try_start_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    .line 883
    iget-object v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 884
    :catch_0
    move-exception v1

    .line 885
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->finish()V

    goto :goto_0

    .line 831
    .end local v1    # "ex":Ljava/lang/Exception;
    :pswitch_1
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 832
    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$5;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 840
    :pswitch_2
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 841
    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$6;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 849
    :pswitch_3
    const v2, 0x7f0c0009

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 850
    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$7;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 858
    :pswitch_4
    const-string v2, "The firmware is not latest.\nDo you want to continue?"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 859
    const v2, 0x7f0c0036

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 860
    const v2, 0x7f0c0037

    new-instance v3, Lcom/sec/android/app/bcocr/AbstractOCRActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$8;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 868
    :pswitch_5
    const v2, 0x7f0c0008

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 869
    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$9;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 828
    nop

    :pswitch_data_0
    .packed-switch -0x8
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public abstract fitEdgeCueForNameCard()V
.end method

.method public abstract getAdvancedMacroFocusActive()Z
.end method

.method public getAvailableStorage()J
    .locals 4

    .prologue
    .line 918
    iget-object v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/bcocr/CheckMemory;->getAvailableStorage(I)J

    move-result-wide v0

    .line 920
    .local v0, "lAvailableStorage":J
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 921
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    .line 923
    :cond_0
    return-wide v0
.end method

.method public getAvailableStorage(I)J
    .locals 4
    .param p1, "storage"    # I

    .prologue
    .line 927
    invoke-static {p1}, Lcom/sec/android/app/bcocr/CheckMemory;->getAvailableStorage(I)J

    move-result-wide v0

    .line 929
    .local v0, "lAvailableStorage":J
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 930
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    .line 932
    :cond_0
    return-wide v0
.end method

.method public getBaseLayout()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getBatteryLevel()I
    .locals 1

    .prologue
    .line 1073
    iget v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBatteryLevel:I

    return v0
.end method

.method public getCommandIdMap()Lcom/sec/android/app/bcocr/command/CommandIdMap;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCommandIdMap:Lcom/sec/android/app/bcocr/command/CommandIdMap;

    return-object v0
.end method

.method public getDepth()I
    .locals 1

    .prologue
    .line 383
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 387
    :goto_0
    return v0

    .line 384
    :catch_0
    move-exception v0

    .line 387
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLightMode()Z
    .locals 1

    .prologue
    .line 1107
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRFlashModeOn:Z

    return v0
.end method

.method public getMainHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getMediaMetadataRetriever()Landroid/media/MediaMetadataRetriever;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMediaMetadataRetriever:Landroid/media/MediaMetadataRetriever;

    return-object v0
.end method

.method public getMemoryStatus()I
    .locals 1

    .prologue
    .line 985
    iget v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    return v0
.end method

.method public getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    return-object v0
.end method

.method public getMenuResourceDepot()Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    return-object v0
.end method

.method public getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    return-object v0
.end method

.method public getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    return-object v0
.end method

.method public abstract getRemainStorage()I
.end method

.method public getScaleGestureDetector()Landroid/view/ScaleGestureDetector;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method public getSubMain()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSubMain:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public abstract getTouchAutoFocusActive()Z
.end method

.method public getUsbMassStorageEnabledStatus()V
    .locals 4

    .prologue
    .line 1085
    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 1086
    .local v0, "mStorageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->isUsbMassStorageEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mUsbMassStorageEnabled:Z

    .line 1087
    const-string v1, "AbstractOCRActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mUsbMassStorageEnabled = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mUsbMassStorageEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    return-void
.end method

.method public abstract getnOCRNameCardCaptureModeType()Z
.end method

.method protected handleBatteryOverheat()V
    .locals 6

    .prologue
    .line 1034
    const-string v1, "AbstractOCRActivity"

    const-string v2, "handleBatteryOverheat"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1070
    :cond_0
    :goto_0
    return-void

    .line 1040
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1041
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1042
    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 1043
    const v1, 0x7f0c0005

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1045
    const v1, 0x7f0c000b

    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$12;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1053
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1054
    new-instance v1, Lcom/sec/android/app/bcocr/AbstractOCRActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$13;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1064
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    .line 1065
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1067
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1068
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected handleLowBattery(Z)V
    .locals 5
    .param p1, "temp"    # Z

    .prologue
    const/4 v4, 0x0

    .line 989
    const-string v1, "AbstractOCRActivity"

    const-string v2, "handleLowBattery"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1031
    :goto_0
    return-void

    .line 995
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 996
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 997
    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 998
    if-eqz p1, :cond_1

    .line 999
    const v1, 0x7f0c0045

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1004
    :goto_1
    const v1, 0x7f0c000b

    new-instance v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$10;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1013
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1014
    new-instance v1, Lcom/sec/android/app/bcocr/AbstractOCRActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity$11;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1027
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 1028
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1029
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 1030
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    goto :goto_0

    .line 1001
    :cond_1
    const v1, 0x7f0c000a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public hideAllDlg()V
    .locals 5

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 966
    const-string v2, "AbstractOCRActivity"

    const-string v3, "hideAllDlg:isFinishing"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    :cond_0
    return-void

    .line 970
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 971
    iget-object v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_2

    .line 972
    iget-object v2, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    .line 974
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 970
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 976
    :catch_0
    move-exception v0

    .line 978
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "AbstractOCRActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dialog id("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") was not ever shown via showDialog"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public hideDlg(I)V
    .locals 4
    .param p1, "nId"    # I

    .prologue
    .line 952
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, p1

    .line 955
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 961
    :cond_0
    :goto_0
    return-void

    .line 956
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "AbstractOCRActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dialog id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") was not ever shown via showDialog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public hideStatusIcon()V
    .locals 2

    .prologue
    .line 1091
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1092
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 1093
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1094
    return-void
.end method

.method public hideWaitingAnimation()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mHideWaitingAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 301
    :cond_0
    return-void
.end method

.method public hideZoomBarAction()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method protected inflateWaitingView()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 304
    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    .line 305
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 306
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f03000c

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 307
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0f0030

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 308
    .local v1, "pBar4":Landroid/widget/ProgressBar;
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f020039

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 314
    return-void
.end method

.method public initFuncMode(IZ)V
    .locals 0
    .param p1, "funcMode"    # I
    .param p2, "bIsTextFinderMode"    # Z

    .prologue
    .line 433
    return-void
.end method

.method public abstract isActivityDestoying()Z
.end method

.method public abstract isAutoFocusing()Z
.end method

.method public isBatteryCharging()Z
    .locals 1

    .prologue
    .line 1077
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bIsCharging:Z

    return v0
.end method

.method public isCalling()Z
    .locals 1

    .prologue
    .line 1137
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCheckCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCheckVoIPCalling:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCaptureEnabled()Z
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 401
    :cond_0
    const/4 v0, 0x1

    .line 408
    :goto_0
    return v0

    .line 404
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->isCaptureEnabled()Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 405
    :catch_0
    move-exception v0

    .line 408
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isCaptureMode()Z
.end method

.method public abstract isCapturing()Z
.end method

.method public isKeyguardLocked()Z
    .locals 4

    .prologue
    .line 526
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 527
    .local v0, "kgm":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    .line 528
    const-string v1, "AbstractOCRActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "kgm.isKeyguardLocked()="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract isMediaScannerScanning()Z
.end method

.method public isNeedToStartEngineSync()Z
    .locals 1

    .prologue
    .line 1103
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mbNeedToStartEngineSync:Z

    return v0
.end method

.method public abstract isPreviewStarted()Z
.end method

.method public isPreviewTouchEnabled()Z
    .locals 1

    .prologue
    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->isPreviewTouchEnabled()Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 396
    :goto_0
    return v0

    .line 393
    :catch_0
    move-exception v0

    .line 396
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isShutterPressed()Z
.end method

.method public isUsbMassStorageEnabled()Z
    .locals 1

    .prologue
    .line 1081
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mUsbMassStorageEnabled:Z

    return v0
.end method

.method public mediaStorageDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 890
    iget v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    if-eqz v0, :cond_0

    .line 891
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->showDlg(I)V

    .line 895
    :goto_0
    return-void

    .line 893
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->hideDlg(I)V

    goto :goto_0
.end method

.method protected notifyOnPause()V
    .locals 1

    .prologue
    .line 633
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->onPause()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 636
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 727
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 728
    sget v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    .line 729
    const-string v0, "AbstractOCRActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate+("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-static {p0}, Lcom/sec/android/app/bcocr/FeatureManage;->initFeature(Landroid/content/Context;)V

    .line 732
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CheckMemory;->setStorageVolume(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    .line 734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bFlagOverheat:Z

    .line 735
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mbNeedToStartEngineSync:Z

    .line 737
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_ENABLE_STATUS_BAR:Z

    if-nez v0, :cond_0

    .line 738
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->hideStatusIcon()V

    .line 740
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 761
    const-string v0, "AbstractOCRActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDestroy("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuDimController;->clear()V

    .line 765
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    if-eqz v0, :cond_1

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCRSettings;->unregisterOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->clear()V

    .line 771
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 774
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 776
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mSpinnerDialog:Landroid/app/Dialog;

    .line 779
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 781
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mErrorPopup:Landroid/app/AlertDialog;

    .line 784
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOpenFailedPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOpenFailedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 786
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOpenFailedPopup:Landroid/app/AlertDialog;

    .line 789
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mRecordingFailedPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mRecordingFailedPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 791
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mRecordingFailedPopup:Landroid/app/AlertDialog;

    .line 794
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBufferOverFlowPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_6

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBufferOverFlowPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 796
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBufferOverFlowPopup:Landroid/app/AlertDialog;

    .line 799
    :cond_6
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mCommandIdMap:Lcom/sec/android/app/bcocr/command/CommandIdMap;

    .line 800
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mHideWaitingAnimationRunnable:Ljava/lang/Runnable;

    .line 801
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

    .line 802
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    .line 803
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    .line 804
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMediaMetadataRetriever:Landroid/media/MediaMetadataRetriever;

    .line 805
    iput-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    .line 807
    sget v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_7

    .line 808
    sget v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRContextNum:I

    .line 811
    :cond_7
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 812
    return-void
.end method

.method public onDirectlinkPopupSelect(I)V
    .locals 0
    .param p1, "selectedItem"    # I

    .prologue
    .line 237
    return-void
.end method

.method public onOCRFuncModeDetecttextChangeSelected()V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method public onOCRFuncModeDirectlinkChangeSelected()V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public onOCRFuncModeTextsearchChangeSelected()V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public onOCRFuncModeTranslatorChangeSelected()V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public onOCRLanguageSelected()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public onOCRRealModeChange()V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method public onOrientationLock(Z)V
    .locals 2
    .param p1, "bLock"    # Z

    .prologue
    .line 1111
    if-eqz p1, :cond_0

    .line 1112
    const/4 v0, -0x1

    .line 1114
    .local v0, "orientation":I
    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    sparse-switch v1, :sswitch_data_0

    .line 1126
    const/16 v0, 0x9

    .line 1130
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setRequestedOrientation(I)V

    .line 1134
    .end local v0    # "orientation":I
    :goto_1
    return-void

    .line 1116
    .restart local v0    # "orientation":I
    :sswitch_0
    const/4 v0, 0x0

    .line 1117
    goto :goto_0

    .line 1119
    :sswitch_1
    const/4 v0, 0x1

    .line 1120
    goto :goto_0

    .line 1122
    :sswitch_2
    const/16 v0, 0x8

    .line 1123
    goto :goto_0

    .line 1132
    .end local v0    # "orientation":I
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1114
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 643
    const-string v3, "AbstractOCRActivity"

    const-string v4, "onPause"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    const-string v3, "Camera_preview"

    invoke-virtual {p0, v3, v6}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setLcdBrightness(Ljava/lang/String;Z)V

    .line 646
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.bcocr.ACTION_STOP_CAMERA"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 648
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    if-eqz v3, :cond_0

    .line 649
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/MenuResourceDepot;->clearInvisibleViews()V

    .line 652
    :cond_0
    const-string v3, "AbstractOCRActivity"

    const-string v4, "GL Cleared!"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mbNeedToStartEngineSync:Z

    .line 655
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 656
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 660
    :cond_1
    const-string v3, "AbstractOCRActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onPause : SCREEN_BRIGHTNESS_MODE - bTurnOnScrAB:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    if-eqz v3, :cond_2

    .line 662
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 664
    .local v2, "pm":Landroid/os/PowerManager;
    const/4 v3, -0x1

    const/4 v4, -0x1

    :try_start_0
    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->setAutoBrightnessLimit(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    .end local v2    # "pm":Landroid/os/PowerManager;
    :cond_2
    :goto_0
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->IMPROVEMENT_SCREEN_QUALITY:Z

    if-nez v3, :cond_3

    .line 671
    invoke-static {v6}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 674
    :cond_3
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->SUPPORT_OUTDOOR_VISIBILITY:Z

    if-eqz v3, :cond_4

    .line 676
    const/4 v3, 0x0

    :try_start_1
    invoke-static {v3}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeOutDoor(Z)Z

    .line 678
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 684
    :cond_4
    :goto_1
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->AMOLED_DISPLAY:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->IMPROVEMENT_SCREEN_QUALITY:Z

    if-eqz v3, :cond_5

    .line 687
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v3}, Lcom/sec/android/hardware/SecHardwareInterface;->setAmoledVideoGamma(Z)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 693
    :cond_5
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 694
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 696
    :cond_6
    iput-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 698
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 699
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 701
    :cond_7
    iput-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 703
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 704
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 706
    :cond_8
    iput-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOverheatPopup:Landroid/app/AlertDialog;

    .line 708
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->stopInactivityTimer()V

    .line 709
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->stopOverheatTimer()V

    .line 711
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseLayout:Landroid/view/ViewGroup;

    if-eqz v3, :cond_9

    .line 712
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseLayout:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 716
    :cond_9
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 717
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string v3, "com.sec.android.intent.action.DVFS_LCD_FRAME_RATE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 718
    const-string v3, "RATE"

    const-string v4, "60"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 719
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 721
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 722
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    .line 723
    return-void

    .line 665
    .end local v0    # "dvfsLockIntent":Landroid/content/Intent;
    .restart local v2    # "pm":Landroid/os/PowerManager;
    :catch_0
    move-exception v1

    .line 666
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AbstractOCRActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "set brightness limit : error("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 688
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "pm":Landroid/os/PowerManager;
    :catch_1
    move-exception v3

    goto/16 :goto_2

    .line 679
    :catch_2
    move-exception v3

    goto/16 :goto_1
.end method

.method protected onResume()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 538
    const-string v7, "Camera_preview"

    invoke-virtual {p0, v7, v10}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setLcdBrightness(Ljava/lang/String;Z)V

    .line 540
    const-string v7, "AbstractOCRActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "onResume hasWindowFocus()="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->hasWindowFocus()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v7, 0x2

    if-lt v3, v7, :cond_6

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isKeyguardLocked()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 547
    const-string v7, "AbstractOCRActivity"

    const-string v8, "onResume. mOnResumePending=true"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iput-boolean v10, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    .line 552
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 553
    iget-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v7}, Lcom/sec/android/app/bcocr/MenuBase;->onResume()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_3

    .line 559
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->restartInactivityTimer()V

    .line 561
    iget-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseLayout:Landroid/view/ViewGroup;

    if-eqz v7, :cond_2

    .line 562
    iget-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mBaseLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v11}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 565
    :cond_2
    const/4 v5, 0x1

    .line 568
    .local v5, "mAutomatic":I
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 569
    const-string v8, "screen_brightness_mode"

    .line 568
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    .line 574
    :goto_2
    if-ne v5, v10, :cond_7

    .line 575
    iput-boolean v10, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    .line 577
    const-string v7, "power"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 578
    .local v6, "pm":Landroid/os/PowerManager;
    const-string v7, "persist.sys.default_brightness"

    const/16 v8, 0x78

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 580
    .local v0, "defBrightness":I
    const/4 v7, -0x1

    :try_start_2
    invoke-virtual {v6, v0, v7}, Landroid/os/PowerManager;->setAutoBrightnessLimit(II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 588
    .end local v0    # "defBrightness":I
    .end local v6    # "pm":Landroid/os/PowerManager;
    :goto_3
    const-string v7, "AbstractOCRActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "onResume : SCREEN_BRIGHTNESS_MODE - mAutomatic:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bTurnOnScrAB:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    sget-boolean v7, Lcom/sec/android/app/bcocr/Feature;->IMPROVEMENT_SCREEN_QUALITY:Z

    if-nez v7, :cond_3

    .line 591
    const/4 v7, 0x4

    invoke-static {v7}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 595
    :cond_3
    sget-boolean v7, Lcom/sec/android/app/bcocr/Feature;->AMOLED_DISPLAY:Z

    if-eqz v7, :cond_4

    sget-boolean v7, Lcom/sec/android/app/bcocr/Feature;->IMPROVEMENT_SCREEN_QUALITY:Z

    if-eqz v7, :cond_4

    .line 597
    const/4 v7, 0x1

    :try_start_3
    invoke-static {v7}, Lcom/sec/android/hardware/SecHardwareInterface;->setAmoledVideoGamma(Z)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 603
    :cond_4
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getUsbMassStorageEnabledStatus()V

    .line 606
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 607
    .local v1, "dvfsLockIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.intent.action.DVFS_LCD_FRAME_RATE"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    const-string v7, "RATE"

    const-string v8, "40"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 609
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 611
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    .line 612
    .local v4, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v4, :cond_5

    .line 613
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 616
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 617
    return-void

    .line 543
    .end local v1    # "dvfsLockIntent":Landroid/content/Intent;
    .end local v4    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v5    # "mAutomatic":I
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    aput-boolean v11, v7, v3

    .line 542
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 570
    .restart local v5    # "mAutomatic":I
    :catch_0
    move-exception v2

    .line 571
    .local v2, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v7, "AbstractOCRActivity"

    const-string v8, "to get SCREEN_BRIGHTNESS_MODE failed"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 581
    .end local v2    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .restart local v0    # "defBrightness":I
    .restart local v6    # "pm":Landroid/os/PowerManager;
    :catch_1
    move-exception v2

    .line 582
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "AbstractOCRActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "set brightness limit : error("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 585
    .end local v0    # "defBrightness":I
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "pm":Landroid/os/PowerManager;
    :cond_7
    iput-boolean v11, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bTurnOnScrAB:Z

    goto/16 :goto_3

    .line 598
    :catch_2
    move-exception v7

    goto :goto_4

    .line 555
    .end local v5    # "mAutomatic":I
    :catch_3
    move-exception v7

    goto/16 :goto_1
.end method

.method public onShootingModeMenuSelect(I)V
    .locals 0
    .param p1, "shootingMode"    # I

    .prologue
    .line 231
    return-void
.end method

.method public onStartingPreviewCompleted()V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 745
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->onStop()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 752
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->stopInactivityTimer()V

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->stopOverheatTimer()V

    .line 754
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->bFlagOverheat:Z

    .line 756
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 757
    return-void

    .line 748
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 0

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->restartInactivityTimer()V

    .line 483
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 484
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 621
    const-string v0, "AbstractOCRActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onWindowFocusChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    if-eqz p1, :cond_0

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->restartInactivityTimer()V

    .line 624
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    if-eqz v0, :cond_0

    .line 625
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    .line 628
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 629
    return-void
.end method

.method public abstract playCameraSound(II)V
.end method

.method public popMenu(I)V
    .locals 1
    .param p1, "zOrder"    # I

    .prologue
    .line 361
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 371
    :cond_0
    :goto_1
    return-void

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->hideMenu()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public processBack()V
    .locals 4

    .prologue
    .line 334
    const/4 v2, 0x0

    .line 337
    .local v2, "view":Lcom/sec/android/app/bcocr/MenuBase;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    move-object v2, v0
    :try_end_0
    .catch Ljava/util/EmptyStackException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 346
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->hideMenu()V

    .line 347
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->onBack()V

    .line 350
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/MenuBase;->restoreMenu()V
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    .line 357
    :cond_1
    :goto_1
    return-void

    .line 338
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/util/EmptyStackException;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->finish()V

    goto :goto_0

    .line 353
    .end local v1    # "e":Ljava/util/EmptyStackException;
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public pushMenu(Lcom/sec/android/app/bcocr/MenuBase;)V
    .locals 1
    .param p1, "menu"    # Lcom/sec/android/app/bcocr/MenuBase;

    .prologue
    .line 321
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->popMenu(I)V

    .line 323
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/MenuBase;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->hideMenu()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    return-void

    .line 326
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public abstract refreshButtonMenu()V
.end method

.method public removeMenu(Lcom/sec/android/app/bcocr/MenuBase;)V
    .locals 1
    .param p1, "menu"    # Lcom/sec/android/app/bcocr/MenuBase;

    .prologue
    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    return-void

    .line 376
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public resetStorageMedia()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setStorage(I)V

    .line 901
    :cond_0
    iput v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    .line 902
    return-void
.end method

.method public abstract resetTouchFocus()V
.end method

.method public restartInactivityTimer()V
    .locals 0

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->stopInactivityTimer()V

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->startInactivityTimer()V

    .line 460
    return-void
.end method

.method public setDefaultStorageStatus()V
    .locals 1

    .prologue
    .line 936
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mStorageStatus:I

    .line 937
    return-void
.end method

.method public setLastCapturedTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 255
    return-void
.end method

.method public setLastContentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 253
    return-void
.end method

.method public setLcdBrightness(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "statusName"    # Ljava/lang/String;
    .param p2, "statusValue"    # Z

    .prologue
    .line 1141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1142
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1143
    const-string v1, "SSRM_STATUS_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1144
    const-string v1, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1145
    const-string v1, "PackageName"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1146
    const-string v1, "PID"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1148
    return-void
.end method

.method public setOCRActivityOrientation(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 502
    sput p1, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOrientation:I

    .line 503
    return-void
.end method

.method public setZoomValue(I)V
    .locals 0
    .param p1, "zoomValue"    # I

    .prologue
    .line 233
    return-void
.end method

.method public abstract setmIsAdvanceFocusFocusing(Ljava/lang/Boolean;)V
.end method

.method public showDlg(I)V
    .locals 4
    .param p1, "nId"    # I

    .prologue
    .line 941
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->showDialog(ILandroid/os/Bundle;)Z

    move-result v0

    .line 942
    .local v0, "result":Z
    if-eqz v0, :cond_0

    .line 943
    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mDlgStatus:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    .line 948
    :goto_0
    return-void

    .line 946
    :cond_0
    const-string v1, "AbstractOCRActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "show dialog ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public abstract showFocusIndicator(I)V
.end method

.method public showStatusIcon()V
    .locals 2

    .prologue
    .line 1097
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1098
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 1099
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1100
    return-void
.end method

.method public showWaitingAnimation()V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mShowWaitingAnimationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 294
    :cond_0
    return-void
.end method

.method public abstract startCaptureAnimation()V
.end method

.method public abstract startContinuousAF()V
.end method

.method public startInactivityTimer()V
    .locals 4

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 454
    :cond_0
    return-void
.end method

.method protected startOverheatTimer()V
    .locals 4

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 472
    :cond_0
    return-void
.end method

.method public abstract stopCameraSound(I)V
.end method

.method public abstract stopContinuousAF()V
.end method

.method public stopInactivityTimer()V
    .locals 2

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getMainHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getMainHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 466
    :cond_0
    return-void
.end method

.method protected stopOverheatTimer()V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mInactivityPopupHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 478
    :cond_0
    return-void
.end method
