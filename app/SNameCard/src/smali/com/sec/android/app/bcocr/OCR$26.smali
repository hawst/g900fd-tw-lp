.class Lcom/sec/android/app/bcocr/OCR$26;
.super Landroid/view/View$AccessibilityDelegate;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->initTalkbackTTSForFlash()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 6240
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4
    .param p1, "host"    # Landroid/view/View;
    .param p2, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 6243
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 6244
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->isAccessibilityFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6245
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 6246
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    .line 6251
    :cond_0
    :goto_0
    return-void

    .line 6248
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$26;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    goto :goto_0
.end method
