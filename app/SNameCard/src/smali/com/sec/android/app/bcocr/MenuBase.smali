.class public Lcom/sec/android/app/bcocr/MenuBase;
.super Ljava/lang/Object;
.source "MenuBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;,
        Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String;

.field public static final Z_LEVEL_0:I = 0x0

.field public static final Z_LEVEL_1:I = 0x1

.field public static final Z_LEVEL_10:I = 0xb

.field public static final Z_LEVEL_11:I = 0xc

.field public static final Z_LEVEL_12:I = 0xd

.field public static final Z_LEVEL_13:I = 0xe

.field public static final Z_LEVEL_2:I = 0x2

.field public static final Z_LEVEL_3:I = 0x4

.field public static final Z_LEVEL_4:I = 0x5

.field public static final Z_LEVEL_5:I = 0x6

.field public static final Z_LEVEL_6:I = 0x7

.field public static final Z_LEVEL_7:I = 0x8

.field public static final Z_LEVEL_8:I = 0x9

.field public static final Z_LEVEL_9:I = 0xa

.field public static final Z_LEVEL_FULLSCREEN:I = 0x10

.field public static final Z_LEVEL_TOP:I = 0xf

.field public static final Z_LEVEL_TOUCH_FOCUS:I = 0x3


# instance fields
.field private mActive:Z

.field protected mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

.field private mCaptureEnabled:Z

.field protected mChild:Lcom/sec/android/app/bcocr/MenuBase;

.field private mEffect:Z

.field private mHideAnimation:Landroid/view/animation/Animation;

.field protected mLaunchX:F

.field protected mLaunchY:F

.field public mLayoutId:I

.field protected mMainView:Landroid/view/ViewGroup;

.field protected mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

.field private mOnHideListener:Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;

.field private mOnShowListener:Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;

.field private mPreviewTouchEnabled:Z

.field private final mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private final mShowAnimationSet:Landroid/view/animation/AnimationSet;

.field private final mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

.field public mViewId:I

.field protected mVisibility:Z

.field protected mZorder:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/MenuBase;->TAG:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)V
    .locals 7
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "layoutId"    # I
    .param p3, "baseLayout"    # Landroid/view/ViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .param p5, "zOrder"    # I

    .prologue
    .line 115
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/bcocr/MenuBase;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;IZ)V

    .line 116
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;IZ)V
    .locals 9
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "layoutId"    # I
    .param p3, "baseLayout"    # Landroid/view/ViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .param p5, "zOrder"    # I
    .param p6, "effect"    # Z

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mPreviewTouchEnabled:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mCaptureEnabled:Z

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mEffect:Z

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActive:Z

    .line 73
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 74
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v1, 0x3f666666    # 0.9f

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f666666    # 0.9f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    .line 75
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 91
    iput p2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLayoutId:I

    .line 92
    iput-object p4, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 93
    iput p5, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    .line 94
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLayoutId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    .line 99
    iput-boolean p6, p0, Lcom/sec/android/app/bcocr/MenuBase;->mEffect:Z

    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mEffect:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowScaleAnimation:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimationSet:Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimation:Landroid/view/animation/Animation;

    .line 111
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 272
    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    .line 275
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 276
    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    .line 277
    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 278
    return-void
.end method

.method public final getBaseResourceId()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLayoutId:I

    return v0
.end method

.method public final getBaseViewId()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mViewId:I

    return v0
.end method

.method public final getVisibility()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    return v0
.end method

.method public final getZorder()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    and-int/lit8 v0, v0, 0xf

    return v0
.end method

.method public hideMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 163
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mHideAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 170
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->hideMenu()V

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/MenuBase;->onHide()V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnHideListener:Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnHideListener:Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;->onHide(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 183
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActive:Z

    .line 184
    return-void

    .line 167
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mHideAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActive:Z

    return v0
.end method

.method public final isCaptureEnabled()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mCaptureEnabled:Z

    return v0
.end method

.method public final isFullScreen()Z
    .locals 2

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    and-int/lit16 v0, v0, 0xf0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPreviewTouchEnabled()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mPreviewTouchEnabled:Z

    return v0
.end method

.method public onActivityTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public onBack()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 266
    return-void
.end method

.method protected onHide()V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method protected onShow()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method public restoreMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 187
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    if-nez v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mMainView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 193
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mVisibility:Z

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuBase;->showMenu()V

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/MenuBase;->onShow()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnShowListener:Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;

    if-eqz v0, :cond_3

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnShowListener:Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;->onShow(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 204
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActive:Z

    .line 205
    return-void
.end method

.method public final setBaseResourceId(I)V
    .locals 0
    .param p1, "layoutId"    # I

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLayoutId:I

    .line 213
    return-void
.end method

.method public final setBaseViewId(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 220
    iput p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mViewId:I

    .line 221
    return-void
.end method

.method protected setCaptureEnabled(Z)V
    .locals 0
    .param p1, "capture"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mCaptureEnabled:Z

    .line 262
    return-void
.end method

.method public final setChild(Lcom/sec/android/app/bcocr/MenuBase;)V
    .locals 0
    .param p1, "child"    # Lcom/sec/android/app/bcocr/MenuBase;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mChild:Lcom/sec/android/app/bcocr/MenuBase;

    .line 229
    return-void
.end method

.method public setHideAnimation(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "a"    # Landroid/view/animation/Animation;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mHideAnimation:Landroid/view/animation/Animation;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mHideAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/bcocr/MenuBase$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/MenuBase$1;-><init>(Lcom/sec/android/app/bcocr/MenuBase;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 151
    return-void
.end method

.method public final setLaunchPosition(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 240
    iput p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLaunchX:F

    .line 241
    iput p2, p0, Lcom/sec/android/app/bcocr/MenuBase;->mLaunchY:F

    .line 242
    return-void
.end method

.method public setOnHideListener(Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnHideListener:Lcom/sec/android/app/bcocr/MenuBase$OnHideListener;

    .line 128
    return-void
.end method

.method public setOnShowListener(Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mOnShowListener:Lcom/sec/android/app/bcocr/MenuBase$OnShowListener;

    .line 124
    return-void
.end method

.method public setShowAnimation(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "a"    # Landroid/view/animation/Animation;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mShowAnimation:Landroid/view/animation/Animation;

    .line 132
    return-void
.end method

.method protected setTouchHandled(Z)V
    .locals 0
    .param p1, "handle"    # Z

    .prologue
    .line 257
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/MenuBase;->mPreviewTouchEnabled:Z

    .line 258
    return-void
.end method

.method public final setZorder(I)V
    .locals 1
    .param p1, "zOrder"    # I

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    and-int/lit16 v0, v0, 0xf0

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mZorder:I

    .line 237
    return-void
.end method

.method public showMenu()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-nez v0, :cond_0

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuBase;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->pushMenu(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/MenuBase;->restoreMenu()V

    goto :goto_0
.end method
