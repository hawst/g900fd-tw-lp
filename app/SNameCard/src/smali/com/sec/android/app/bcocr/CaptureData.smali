.class public Lcom/sec/android/app/bcocr/CaptureData;
.super Ljava/lang/Object;
.source "CaptureData.java"


# instance fields
.field private mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

.field private mCaptureOnlyData:[B

.field private mDecodingData:[B

.field private mOrientation:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/16 v0, 0x4000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mDecodingData:[B

    .line 24
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mDecodingData:[B

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 67
    :cond_0
    return-void
.end method

.method public getCaptureBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 75
    const/16 v0, 0x1e0

    const/16 v1, 0x168

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCaptureData()[B
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    return-object v0
.end method

.method public getCaptureSmallBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/16 v1, 0x54

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 82
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/app/bcocr/CaptureData;->mOrientation:I

    return v0
.end method

.method public setCaptureData([BI)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "sampleSize"    # I

    .prologue
    const/16 v5, 0x1e0

    const/16 v4, 0x168

    const/4 v3, 0x0

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    .line 32
    iput v3, p0, Lcom/sec/android/app/bcocr/CaptureData;->mOrientation:I

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    if-eqz v1, :cond_2

    .line 35
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 36
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 37
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 38
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 39
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mDecodingData:[B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 41
    iput v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 42
    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 46
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 49
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    iget-object v2, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyData:[B

    array-length v2, v2

    invoke-static {v1, v3, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 51
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    .line 56
    .end local v0    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    :goto_0
    return-void

    .line 54
    :cond_2
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mCaptureOnlyBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/bcocr/CaptureData;->mOrientation:I

    .line 89
    return-void
.end method
