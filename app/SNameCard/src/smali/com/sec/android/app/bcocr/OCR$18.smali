.class Lcom/sec/android/app/bcocr/OCR$18;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->onRecognitionStateChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 4970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 4973
    const/4 v0, 0x0

    .line 4974
    .local v0, "recog_completed":Z
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4975
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] previewRecogThread +++"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4978
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$27(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4979
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] error!! now ConfigurationChanging...."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5017
    :cond_1
    :goto_0
    return-void

    .line 4983
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    if-ne v1, v5, :cond_3

    .line 4984
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] error!! now processing...."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4987
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->SetRecognizeLock(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/bcocr/OCR;->access$28(Lcom/sec/android/app/bcocr/OCR;Z)V

    .line 4989
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput v5, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    .line 4990
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4991
    const-string v1, "OCR"

    const-string v2, "[mycheck]detect 3 : ocrRecognizePreviewData ++"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4993
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    # invokes: Lcom/sec/android/app/bcocr/OCR;->ocrRecognizePreviewData([B)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->access$29(Lcom/sec/android/app/bcocr/OCR;[B)Z

    move-result v0

    .line 4995
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4996
    const-string v1, "OCR"

    const-string v2, "[mycheck]detect 4 : ocrRecognizePreviewData --"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4998
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput v4, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    .line 5000
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 5001
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] [Recognition] previewRecogThread: recog skip /recog_completed = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5003
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 5005
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/bcocr/OCR;->SetRecognizeLock(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->access$28(Lcom/sec/android/app/bcocr/OCR;Z)V

    .line 5006
    if-eqz v0, :cond_8

    .line 5007
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->OCRHandler:Landroid/os/Handler;

    if-eqz v1, :cond_7

    .line 5008
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->OCRHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 5014
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5015
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] previewRecogThread ---"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5011
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$18;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/16 v2, 0xc

    # invokes: Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V
    invoke-static {v1, v4, v2}, Lcom/sec/android/app/bcocr/OCR;->access$17(Lcom/sec/android/app/bcocr/OCR;ZI)V

    goto :goto_1
.end method
