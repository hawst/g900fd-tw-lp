.class Lcom/sec/android/app/bcocr/OCREngine$1;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    .line 894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 897
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->cancelAutoFocus()V

    .line 901
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->setFocusParameter(I)V

    .line 902
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$1;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    .line 903
    return-void
.end method
