.class final Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JpegPictureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 589
    return-void
.end method


# virtual methods
.method public onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 4
    .param p1, "jpegData"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 595
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-TakePicture**EndU["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$0(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    move-result-object v0

    # invokes: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->startSavePicture([BLcom/sec/android/seccamera/SecCamera;)V
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$0(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[BLcom/sec/android/seccamera/SecCamera;)V

    .line 610
    :goto_0
    return-void

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$0(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto :goto_0
.end method
