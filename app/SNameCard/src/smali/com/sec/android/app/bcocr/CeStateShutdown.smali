.class public Lcom/sec/android/app/bcocr/CeStateShutdown;
.super Lcom/sec/android/app/bcocr/AbstractCeState;
.source "CeStateShutdown.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CeStateShuttingDown"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V
    .locals 0
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;
    .param p2, "requestQueue"    # Lcom/sec/android/app/bcocr/CeRequestQueue;
    .param p3, "id"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/AbstractCeState;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public cancelRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    .line 34
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 38
    const-string v0, "CeStateShuttingDown"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    const-string v0, "CeStateShuttingDown"

    const-string v1, "message-coming means engine is still alive.."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const-string v0, "CeStateShuttingDown"

    const-string v1, "stopping engine.."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 51
    return-void

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->imageStoringCompleted()V

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
    .locals 5
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    const/4 v4, 0x7

    const/4 v0, 0x1

    .line 55
    const-string v1, "CeStateShuttingDown"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HandleRequest - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 74
    const-string v0, "CeStateShuttingDown"

    const-string v1, "invalid request id for current state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    .line 77
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 58
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewDummySync()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 63
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 68
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateShutdown;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x4 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method
