.class public Lcom/sec/android/app/bcocr/OCRSettings;
.super Ljava/lang/Object;
.source "OCRSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;,
        Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;
    }
.end annotation


# static fields
.field public static final CALL_POSTVIEW_FROM_HISTORY:I = 0x1

.field public static final CAMERA_ANTI_BANDING_50HZ:Ljava/lang/String; = "50hz"

.field public static final CAMERA_ANTI_BANDING_60HZ:Ljava/lang/String; = "60hz"

.field public static final CAMERA_ANTI_BANDING_AUTO:Ljava/lang/String; = "auto"

.field public static final CAMERA_ANTI_BANDING_OFF:Ljava/lang/String; = "off"

.field protected static final CAMERA_SETTINGS_NOTOFICATION:I = 0x0

.field protected static final CSC_KEY_CAMERA_FLASH:Ljava/lang/String; = "csc_pref_camera_flash_key"

.field protected static final DEFAULT_BACK_CAMERA_FOCUS:I = 0x5

.field protected static final DEFAULT_CAMERA_FLASH:I = 0x0

.field protected static final DEFAULT_CAMERA_ID:I = 0x0

.field protected static final DEFAULT_CAMERA_QUALITY:I = 0x0

.field protected static final DEFAULT_DLPOPUP_ITEM:I = 0x0

.field protected static final DEFAULT_ENGINE_SELECTED_LANGUAGE:Ljava/lang/String; = ""

.field protected static final DEFAULT_LIGHT_MODE:I = 0x0

.field protected static final DEFAULT_SETUP_STORAGE:I = 0x0

.field protected static final DEFAULT_SHOOTINGMODE:I = 0x0

.field public static final DEFAULT_ZOOM_VALUE:I

.field public static final FLASHMODE_AUTO:I = 0x2

.field public static final FLASHMODE_OFF:I = 0x0

.field public static final FLASHMODE_ON:I = 0x1

.field public static final FLASHMODE_REDEYE:I = 0x3

.field public static final FLASHMODE_TORCH:I = 0x4

.field public static final FOCUSMODE_AF:I = 0x1

.field public static final FOCUSMODE_CONTINUOUS_PICTURE:I = 0x5

.field public static final FOCUSMODE_CONTINUOUS_PICTURE_MACRO:I = 0x6

.field public static final FOCUSMODE_CONTINUOUS_VIDEO:I = 0x4

.field public static final FOCUSMODE_FACEDETECTION:I = 0x3

.field public static final FOCUSMODE_MACRO:I = 0x2

.field public static final FOCUSMODE_OFF:I = 0x0

.field public static final ISO_AUTO:I = 0x0

.field protected static final KEY_CAMERA_FLASH:Ljava/lang/String; = "pref_camera_flash_key"

.field protected static final KEY_CAMERA_FOCUS:Ljava/lang/String; = "pref_camera_focus_key"

.field protected static final KEY_CAMERA_ID:Ljava/lang/String; = "pref_camera_id_key"

.field protected static final KEY_CAMERA_QUALITY:Ljava/lang/String; = "pref_camera_quality_key"

.field protected static final KEY_DICTIONARY_DST_ID:Ljava/lang/String; = "pref_dictionary_dst_id"

.field protected static final KEY_DICTIONARY_SRC_ID:Ljava/lang/String; = "pref_dictionary_src_id"

.field protected static final KEY_ENGINE_SELECTED_LANGUAGE:Ljava/lang/String; = "pref_engine_selected_language_key"

.field protected static final KEY_LIGHT_MODE:Ljava/lang/String; = "pref_light_mode_key"

.field protected static final KEY_SELECTED_MODE:Ljava/lang/String; = "pref_selected_mode_key"

.field protected static final KEY_SETUP_STORAGE:Ljava/lang/String; = "pref_setup_storage_key"

.field protected static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field protected static final KEY_VOICE_INPUT_CONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_bcr"

.field protected static final LIGHT_MODE_OFF:I = 0x0

.field protected static final LIGHT_MODE_ON:I = 0x1

.field public static final MENUID_CAPTURE_MODE:I = 0xc

.field public static final MENUID_FLASHMODE:I = 0x3

.field public static final MENUID_FOCUSMODE:I = 0x5

.field public static final MENUID_ISO:I = 0xa

.field public static final MENUID_MODE:I = 0x0

.field public static final MENUID_OCR_DLPOPUP:I = 0xca

.field public static final MENUID_OCR_FUNCMODE:I = 0xc9

.field public static final MENUID_OCR_REALMODE:I = 0xc8

.field public static final MENUID_QUALITY:I = 0x10

.field public static final MENUID_RESOLUTION:I = 0x4

.field public static final MENUID_SETTINGS:I = 0x1b

.field public static final MENUID_SHOOTINGMODE:I = 0x1

.field public static final MENUID_STORAGE:I = 0x16

.field public static final MENUID_VOICECOMMAND:I = 0x29

.field public static final MENUID_ZOOM:I = 0x12

.field public static final MODE_CAMERA:I = 0x0

.field public static final MODE_FUNC_ADDTOCONTACT:I = 0x6

.field public static final MODE_FUNC_ALL:I = 0x5

.field public static final MODE_FUNC_DETECTTEXT:I = 0x2

.field public static final MODE_FUNC_DIRECTLINK:I = 0x0

.field public static final MODE_FUNC_SCRAP_BUSINESS_CARD:I = 0x7

.field public static final MODE_FUNC_SIPOCR:I = 0x4

.field public static final MODE_FUNC_TEXTSEARCH:I = 0x3

.field public static final MODE_FUNC_TRANSLATOR:I = 0x1

.field public static final MODE_REAL_CAPTURE:I = 0x1

.field public static final MODE_REAL_REALTIME:I = 0x0

.field public static final MODE_REAL_RESTORE:I = 0x3

.field public static final MODE_REAL_TOGGLE:I = 0x2

.field public static final NO_VALUE:I = -0xffff

.field public static final OCR_DIRECTLINK_POPUPTYPE_ADDRESS:I = 0x6

.field public static final OCR_DIRECTLINK_POPUPTYPE_CITY:I = 0x9

.field public static final OCR_DIRECTLINK_POPUPTYPE_COMPANY:I = 0x5

.field public static final OCR_DIRECTLINK_POPUPTYPE_COUNTRY:I = 0x8

.field public static final OCR_DIRECTLINK_POPUPTYPE_DEPARTMENT:I = 0x4

.field public static final OCR_DIRECTLINK_POPUPTYPE_EMAIL:I = 0xf

.field public static final OCR_DIRECTLINK_POPUPTYPE_FAX:I = 0xc

.field public static final OCR_DIRECTLINK_POPUPTYPE_FIRSTNAME:I = 0x1

.field public static final OCR_DIRECTLINK_POPUPTYPE_LASTNAME:I = 0x2

.field public static final OCR_DIRECTLINK_POPUPTYPE_MOBILE:I = 0xd

.field public static final OCR_DIRECTLINK_POPUPTYPE_NAME:I = 0x0

.field public static final OCR_DIRECTLINK_POPUPTYPE_OTHER:I = 0x10

.field public static final OCR_DIRECTLINK_POPUPTYPE_PHONENUM:I = 0xb

.field public static final OCR_DIRECTLINK_POPUPTYPE_POSTCODE:I = 0x7

.field public static final OCR_DIRECTLINK_POPUPTYPE_QRCONTACT:I = 0x20

.field public static final OCR_DIRECTLINK_POPUPTYPE_QREMAIL:I = 0x23

.field public static final OCR_DIRECTLINK_POPUPTYPE_QRPHONENUM:I = 0x21

.field public static final OCR_DIRECTLINK_POPUPTYPE_QRSTR:I = 0x1f

.field public static final OCR_DIRECTLINK_POPUPTYPE_QRURL:I = 0x22

.field public static final OCR_DIRECTLINK_POPUPTYPE_STREET:I = 0xa

.field public static final OCR_DIRECTLINK_POPUPTYPE_TITLE:I = 0x3

.field public static final OCR_DIRECTLINK_POPUPTYPE_URL:I = 0xe

.field public static final OCR_DIRECTLINK_POPUPTYPE_WORD:I = 0x1e

.field public static final OCR_DIRECTLINK_POPUP_BROWSER:I = 0x4

.field public static final OCR_DIRECTLINK_POPUP_CALL:I = 0x3

.field public static final OCR_DIRECTLINK_POPUP_CONTACT:I = 0x0

.field public static final OCR_DIRECTLINK_POPUP_COPY:I = 0x5

.field public static final OCR_DIRECTLINK_POPUP_EMAIL:I = 0x1

.field public static final OCR_DIRECTLINK_POPUP_MESSAGE:I = 0x2

.field public static final OCR_SIP_TYPE_EMAIL:I = 0x1

.field public static final OCR_SIP_TYPE_LINK:I = 0x0

.field public static final OCR_SIP_TYPE_MULTITEXT:I = 0x4

.field public static final OCR_SIP_TYPE_PHONENUM:I = 0x2

.field public static final OCR_SIP_TYPE_URL:I = 0x3

.field public static final QUALITY_FINE:I = 0x1

.field public static final QUALITY_NORMAL:I = 0x2

.field public static final QUALITY_SUPERFINE:I = 0x0

.field public static final SHOOTINGMODE_CONTINUOUS:I = 0x1

.field public static final SHOOTINGMODE_PANORAMA:I = 0x2

.field public static final SHOOTINGMODE_SINGLE:I = 0x0

.field public static final STORAGE_MMC:I = 0x1

.field public static final STORAGE_PHONE:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "OCRSettings"

.field public static final TRANSLATOR_DIC_NUM:[I

.field public static final VOICECOMMAND_OFF:I = 0x0

.field public static final VOICECOMMAND_ON:I = 0x1


# instance fields
.field private mCameraId:I

.field protected mContext:Landroid/content/Context;

.field private mDLPopupRect:Landroid/graphics/Rect;

.field private mDLPopupStr:Ljava/lang/String;

.field private mDLPopupType:I

.field private mDictionaryDstID:I

.field private mDictionarySrcID:I

.field private mEngineSelectedLanguage:Ljava/lang/String;

.field private mFlashMode:I

.field private mFocusMode:I

.field private mFocusY:I

.field private mLightMode:I

.field private mMode:I

.field protected mNotificationHandler:Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;

.field private mOCRFuncMode:I

.field private mOCRRealMode:I

.field private mOCRSIPType:I

.field protected mObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;",
            ">;"
        }
    .end annotation
.end field

.field protected mPreferences:Landroid/content/SharedPreferences;

.field protected mProp:Ljava/util/Properties;

.field private mResolution:I

.field private mSelectedMode:I

.field public mShootingMode:I

.field private mStorage:I

.field private mTRPopupFullMeanStr:Ljava/lang/CharSequence;

.field private mTRPopupRect:Landroid/graphics/Rect;

.field private mTRPopupSimpleMeanStr:Ljava/lang/CharSequence;

.field private mTRPopupStr:Ljava/lang/String;

.field private mTRPopupType:I

.field private mVoiceCommand:I

.field private mZoomValue:I

.field private mbQRMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/bcocr/OCRSettings;->TRANSLATOR_DIC_NUM:[I

    .line 252
    sget v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_DEFAULT_ZOOM_VALUE:I

    sput v0, Lcom/sec/android/app/bcocr/OCRSettings;->DEFAULT_ZOOM_VALUE:I

    .line 277
    return-void

    .line 75
    :array_0
    .array-data 4
        0x7f0b001a
        0x7f0b0020
        0x7f0b0026
        0x7f0b0024
        0x7f0b0028
        0x7f0b0021
        0x7f0b0027
        0x7f0b0025
        0x7f0b0029
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    .line 89
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mMode:I

    .line 97
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRRealMode:I

    .line 108
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRFuncMode:I

    .line 119
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRSIPType:I

    .line 130
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mCameraId:I

    .line 140
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mShootingMode:I

    .line 161
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mResolution:I

    .line 181
    sget v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_DEFAULT_ZOOM_VALUE:I

    iput v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mZoomValue:I

    .line 216
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupType:I

    .line 217
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupStr:Ljava/lang/String;

    .line 219
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusY:I

    .line 220
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mbQRMode:Z

    .line 223
    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupType:I

    .line 224
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupStr:Ljava/lang/String;

    .line 225
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupSimpleMeanStr:Ljava/lang/CharSequence;

    .line 226
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupFullMeanStr:Ljava/lang/CharSequence;

    .line 267
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mEngineSelectedLanguage:Ljava/lang/String;

    .line 289
    new-instance v0, Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;-><init>(Lcom/sec/android/app/bcocr/OCRSettings;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mNotificationHandler:Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;

    .line 292
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    .line 293
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    .line 294
    return-void
.end method

.method public static getFlashModeString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 701
    packed-switch p0, :pswitch_data_0

    .line 711
    :pswitch_0
    const-string v0, "auto"

    :goto_0
    return-object v0

    .line 703
    :pswitch_1
    const-string v0, "auto"

    goto :goto_0

    .line 705
    :pswitch_2
    const-string v0, "on"

    goto :goto_0

    .line 707
    :pswitch_3
    const-string v0, "off"

    goto :goto_0

    .line 709
    :pswitch_4
    const-string v0, "torch"

    goto :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getFocusModeString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 716
    packed-switch p0, :pswitch_data_0

    .line 736
    const-string v0, "auto"

    :goto_0
    return-object v0

    .line 718
    :pswitch_0
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_INFINITY:Z

    if-eqz v0, :cond_0

    .line 719
    const-string v0, "infinity"

    goto :goto_0

    .line 721
    :cond_0
    const-string v0, "fixed"

    goto :goto_0

    .line 724
    :pswitch_1
    const-string v0, "auto"

    goto :goto_0

    .line 726
    :pswitch_2
    const-string v0, "macro"

    goto :goto_0

    .line 728
    :pswitch_3
    const-string v0, "face-priority"

    goto :goto_0

    .line 730
    :pswitch_4
    const-string v0, "continuous-video"

    goto :goto_0

    .line 732
    :pswitch_5
    const-string v0, "continuous-picture"

    goto :goto_0

    .line 734
    :pswitch_6
    const-string v0, "continuous-picture-macro"

    goto :goto_0

    .line 716
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getIsoString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 741
    .line 744
    const-string v0, "auto"

    return-object v0
.end method

.method public static getModeString(I)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # I

    .prologue
    .line 679
    sparse-switch p0, :sswitch_data_0

    .line 696
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 681
    :sswitch_0
    const-string v0, "flash-mode"

    goto :goto_0

    .line 683
    :sswitch_1
    const-string v0, "picture-size"

    goto :goto_0

    .line 685
    :sswitch_2
    const-string v0, "focus-mode"

    goto :goto_0

    .line 687
    :sswitch_3
    const-string v0, "iso"

    goto :goto_0

    .line 689
    :sswitch_4
    const-string v0, "jpeg-quality"

    goto :goto_0

    .line 691
    :sswitch_5
    const-string v0, "zoom"

    goto :goto_0

    .line 693
    :sswitch_6
    const-string v0, "shot-mode"

    goto :goto_0

    .line 679
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_6
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0xa -> :sswitch_3
        0x10 -> :sswitch_4
        0x12 -> :sswitch_5
    .end sparse-switch
.end method

.method public static getQualityValue()I
    .locals 1

    .prologue
    .line 749
    sget v0, Lcom/sec/android/app/bcocr/Feature;->OCR_CAMERA_QUALITY:I

    return v0
.end method

.method private initializeBackCamera()V
    .locals 3

    .prologue
    .line 301
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-nez v1, :cond_2

    .line 302
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 304
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_camera_focus_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 305
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 317
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-nez v1, :cond_1

    .line 318
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setStorage(I)V

    .line 321
    :cond_1
    invoke-static {}, Lcom/sec/android/app/bcocr/CameraHolder;->instance()Lcom/sec/android/app/bcocr/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CameraHolder;->getBackCameraId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraId(I)V

    .line 322
    return-void

    .line 307
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 310
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 312
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_camera_focus_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 313
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->resetObservers()V

    .line 769
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    .line 770
    return-void
.end method

.method public getAntiBanding()Ljava/lang/String;
    .locals 3

    .prologue
    .line 614
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->SUPPORT_CAMERA_CSC_FEATURE:Z

    if-eqz v1, :cond_7

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->isCSCUsedInManyCountriesForLatin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getAntibandingByMCCForLatin()Ljava/lang/String;

    move-result-object v1

    const-string v2, "50hz"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617
    const-string v1, "50hz"

    .line 640
    :goto_0
    return-object v1

    .line 619
    :cond_0
    const-string v1, "60hz"

    goto :goto_0

    .line 622
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Camera_EnableCameraDuringCall"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 623
    .local v0, "flicker":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 624
    const-string v1, "50hz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 625
    const-string v1, "50hz"

    goto :goto_0

    .line 626
    :cond_2
    const-string v1, "60hz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 627
    const-string v1, "60hz"

    goto :goto_0

    .line 628
    :cond_3
    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 629
    const-string v1, "auto"

    goto :goto_0

    .line 630
    :cond_4
    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 631
    const-string v1, "off"

    goto :goto_0

    .line 633
    :cond_5
    const-string v1, "50hz"

    goto :goto_0

    .line 636
    :cond_6
    const-string v1, "50hz"

    goto :goto_0

    .line 640
    .end local v0    # "flicker":Ljava/lang/String;
    :cond_7
    const-string v1, "50hz"

    goto :goto_0
.end method

.method public getAntibandingByMCCForLatin()Ljava/lang/String;
    .locals 10

    .prologue
    .line 811
    const-string v5, "000"

    .line 812
    .local v5, "mcc":Ljava/lang/String;
    const-string v7, "gsm.operator.numeric"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 814
    .local v6, "numeric":Ljava/lang/String;
    const-string v2, "338"

    .line 815
    .local v2, "MCC_Jamaica":Ljava/lang/String;
    const-string v0, "722"

    .line 816
    .local v0, "MCC_Argentina":Ljava/lang/String;
    const-string v1, "730"

    .line 817
    .local v1, "MCC_Chile":Ljava/lang/String;
    const-string v3, "744"

    .line 818
    .local v3, "MCC_Paraguay":Ljava/lang/String;
    const-string v4, "748"

    .line 820
    .local v4, "MCC_Uruguay":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x4

    if-le v7, v8, :cond_0

    .line 821
    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 822
    const-string v7, "OCRSettings"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "getAntibandingByMCCForLatin : mcc "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    :cond_0
    const-string v7, "338"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "722"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "730"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 826
    const-string v7, "744"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "748"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 827
    :cond_1
    const-string v7, "50hz"

    .line 829
    :goto_0
    return-object v7

    :cond_2
    const-string v7, "60hz"

    goto :goto_0
.end method

.method public getCameraFlashMode()I
    .locals 3

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_camera_flash_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultCameraFlashMode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getCameraFocusMode()I
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_camera_focus_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultCameraFocusMode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getCameraId()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mCameraId:I

    return v0
.end method

.method public getCameraQuality()I
    .locals 3

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_camera_quality_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultCameraQuality()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getCameraResolution()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mResolution:I

    return v0
.end method

.method public getCameraVoiceCommand()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 921
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "voice_input_control"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 922
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "voice_input_control_bcr"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 923
    const-string v1, "OCRSettings"

    const-string v2, "Voice command ON"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    :goto_0
    return v0

    .line 926
    :cond_0
    const-string v0, "OCRSettings"

    const-string v2, "Voice command OFF"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 927
    goto :goto_0
.end method

.method public getDefaultCameraFlashMode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 496
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->SUPPORT_CAMERA_CSC_FEATURE:Z

    if-eqz v2, :cond_0

    .line 497
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v1

    .line 501
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 502
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "csc_pref_camera_flash_key"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 503
    .local v1, "flashMode":I
    const-string v2, "OCRSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDefaultCameraFlashMode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 506
    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 507
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultCameraFocusMode()I
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x5

    return v0
.end method

.method public getDefaultCameraQuality()I
    .locals 1

    .prologue
    .line 587
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultStorage()I
    .locals 1

    .prologue
    .line 591
    const/4 v0, 0x0

    return v0
.end method

.method public getDictionaryDstID()I
    .locals 3

    .prologue
    .line 889
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 890
    .local v0, "currPhoneLangID":I
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_dictionary_dst_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public getDictionarySrcID()I
    .locals 3

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_dictionary_src_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getEngineSelectedLanguage(Z)Ljava/lang/String;
    .locals 4
    .param p1, "isDefaultChange"    # Z

    .prologue
    .line 849
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_engine_selected_language_key"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 851
    .local v0, "selectedLanguage":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 852
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCRDicManager;->getDefaultOCREngineLanguageSet(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 857
    :cond_0
    return-object v0
.end method

.method public getLightMode()I
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_light_mode_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mMode:I

    return v0
.end method

.method public getModeResourceString(I)Ljava/lang/String;
    .locals 2
    .param p1, "value"    # I

    .prologue
    const v1, 0x7f0c0001

    .line 753
    packed-switch p1, :pswitch_data_0

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 755
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 753
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getOCRDLPopupRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getOCRDLPopupString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupStr:Ljava/lang/String;

    return-object v0
.end method

.method public getOCRDLPopupType()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupType:I

    return v0
.end method

.method public getOCRFocusY()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusY:I

    return v0
.end method

.method public getOCRFuncMode()I
    .locals 1

    .prologue
    .line 386
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRFuncMode:I

    return v0
.end method

.method public getOCRRealMode()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRRealMode:I

    return v0
.end method

.method public getOCRSIPType()I
    .locals 1

    .prologue
    .line 408
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRSIPType:I

    return v0
.end method

.method public getOCRTRPopupFullMean()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupFullMeanStr:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getOCRTRPopupRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getOCRTRPopupSimpleMean()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupSimpleMeanStr:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getOCRTRPopupString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupStr:Ljava/lang/String;

    return-object v0
.end method

.method public getOCRTRPopupType()I
    .locals 1

    .prologue
    .line 469
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupType:I

    return v0
.end method

.method public getSelectedMode()I
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_selected_mode_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSettingValue(I)I
    .locals 1
    .param p1, "menuid"    # I

    .prologue
    .line 773
    sparse-switch p1, :sswitch_data_0

    .line 795
    const v0, -0xffff

    :goto_0
    return v0

    .line 775
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getMode()I

    move-result v0

    goto :goto_0

    .line 777
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    goto :goto_0

    .line 779
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v0

    goto :goto_0

    .line 781
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v0

    goto :goto_0

    .line 783
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    goto :goto_0

    .line 785
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraQuality()I

    move-result v0

    goto :goto_0

    .line 787
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getZoomValue()I

    move-result v0

    goto :goto_0

    .line 789
    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v0

    goto :goto_0

    .line 791
    :sswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRRealMode()I

    move-result v0

    goto :goto_0

    .line 793
    :sswitch_9
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v0

    goto :goto_0

    .line 773
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x10 -> :sswitch_5
        0x12 -> :sswitch_6
        0x16 -> :sswitch_7
        0xc8 -> :sswitch_8
        0xc9 -> :sswitch_9
    .end sparse-switch
.end method

.method public getShootingMode()I
    .locals 1

    .prologue
    .line 473
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mShootingMode:I

    return v0
.end method

.method public getShootingModeValueForISPset(I)I
    .locals 2
    .param p1, "shootingmode"    # I

    .prologue
    const/4 v0, 0x0

    .line 906
    packed-switch p1, :pswitch_data_0

    .line 916
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 908
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 909
    const/4 v0, 0x1

    goto :goto_0

    .line 914
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_0

    .line 906
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getStorage()I
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pref_setup_storage_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultStorage()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getZoomValue()I
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mZoomValue:I

    return v0
.end method

.method protected handleNotification(II)V
    .locals 3
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 669
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    monitor-enter v2

    .line 670
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 672
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 669
    monitor-exit v2

    .line 676
    return-void

    .line 673
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;->onOCRSettingsChanged(II)V

    goto :goto_0

    .line 669
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public initializeCamera()V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->initializeBackCamera()V

    .line 298
    return-void
.end method

.method public isBackCamera()Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraId()I

    move-result v0

    invoke-static {}, Lcom/sec/android/app/bcocr/CameraHolder;->instance()Lcom/sec/android/app/bcocr/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CameraHolder;->getBackCameraId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCSCUsedInManyCountriesForLatin()Z
    .locals 3

    .prologue
    .line 800
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 802
    .local v0, "code":Ljava/lang/String;
    const-string v1, "TFG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TPA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "JDI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PCI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 803
    :cond_0
    const-string v1, "OCRSettings"

    const-string v2, "isCSCUsedInManyCountriesForLatin : SalesCode = TFG, TPA, TTT, JDI, PCI "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    const/4 v1, 0x1

    .line 807
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOCRQRPopup()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mbQRMode:Z

    return v0
.end method

.method protected notifyOCRSettingsChanged(II)V
    .locals 3
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mNotificationHandler:Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mNotificationHandler:Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings$NotificationHandler;->sendMessage(Landroid/os/Message;)Z

    .line 658
    return-void
.end method

.method protected notifyOCRSettingsChanged(IIZ)V
    .locals 0
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I
    .param p3, "syncmode"    # Z

    .prologue
    .line 661
    if-eqz p3, :cond_0

    .line 662
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCRSettings;->handleNotification(II)V

    .line 666
    :goto_0
    return-void

    .line 664
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    goto :goto_0
.end method

.method public registerOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V
    .locals 2
    .param p1, "o"    # Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;

    .prologue
    .line 645
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    monitor-enter v1

    .line 646
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 645
    monitor-exit v1

    .line 648
    return-void

    .line 645
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resetObservers()V
    .locals 2

    .prologue
    .line 762
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    monitor-enter v1

    .line 763
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 762
    monitor-exit v1

    .line 765
    return-void

    .line 762
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setCameraFlashMode(I)V
    .locals 1
    .param p1, "flashMode"    # I

    .prologue
    .line 515
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraFlashMode(IZ)V

    .line 516
    return-void
.end method

.method public setCameraFlashMode(IZ)V
    .locals 4
    .param p1, "flashMode"    # I
    .param p2, "bForceSetting"    # Z

    .prologue
    .line 519
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_camera_flash_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultCameraFlashMode()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFlashMode:I

    .line 520
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFlashMode:I

    if-ne v1, p1, :cond_0

    .line 521
    if-eqz p2, :cond_1

    .line 522
    :cond_0
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCameraFlashMode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFlashMode:I

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 525
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_camera_flash_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFlashMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 526
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 528
    const/4 v1, 0x3

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 530
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/bcocr/OCR;->handleFlashModeChanged(I)V

    .line 531
    return-void
.end method

.method public setCameraFocusMode(I)V
    .locals 4
    .param p1, "focusMode"    # I

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_camera_focus_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultCameraFocusMode()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    .line 543
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    if-eq v1, p1, :cond_0

    .line 544
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCameraFocusMode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 547
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_camera_focus_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 548
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 550
    const/4 v1, 0x5

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 552
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setCameraId(I)V
    .locals 4
    .param p1, "cameraId"    # I

    .prologue
    .line 329
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 330
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCameraId - wrong camera id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 334
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_camera_id_key"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 335
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 338
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mCameraId:I

    goto :goto_0
.end method

.method public setCameraResolution(I)Z
    .locals 3
    .param p1, "resolution"    # I

    .prologue
    .line 560
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mResolution : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mResolution:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resolution : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mResolution:I

    if-eq v0, p1, :cond_0

    .line 562
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCameraResolution "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mResolution:I

    .line 564
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 565
    const/4 v0, 0x1

    .line 567
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCameraVoiceCommand(I)V
    .locals 1
    .param p1, "voicecommand"    # I

    .prologue
    .line 931
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraVoiceCommand(IZ)V

    .line 932
    return-void
.end method

.method public setCameraVoiceCommand(IZ)V
    .locals 5
    .param p1, "voicecommand"    # I
    .param p2, "showHelpPopup"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voice_input_control"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 936
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 937
    const-string v2, "voice_input_control_bcr"

    .line 936
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 938
    iput v3, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mVoiceCommand:I

    .line 943
    :goto_0
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mVoiceCommand:I

    if-eq v1, p1, :cond_0

    .line 944
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mVoiceCommand:I

    .line 945
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mVoiceCommand:I

    if-ne v1, v3, :cond_6

    .line 946
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    if-eqz v1, :cond_5

    .line 947
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 948
    const-string v2, "voice_input_control"

    .line 947
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_3

    .line 949
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/bcocr/Feature;->PACKAGE_NAME_SVOICE:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/Util;->isPkgEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 950
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    .line 951
    sget-object v2, Lcom/sec/android/app/bcocr/Feature;->PACKAGE_NAME_SVOICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->showApplicationDisabledPopup(Ljava/lang/String;)V

    .line 983
    :cond_0
    :goto_1
    return-void

    .line 940
    :cond_1
    iput v4, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mVoiceCommand:I

    goto :goto_0

    .line 954
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 955
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VOICE_SETTING_BARGEIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 958
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->isBisMicrophoneEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 959
    const-string v1, "OCRSettings"

    const-string v2, "###### isBisMicrophoneEnabled false"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 962
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 963
    const-string v2, "voice_input_control_bcr"

    .line 962
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_4

    .line 964
    const-string v1, "OCRSettings"

    const-string v2, "###### setCameraVoiceCommand ON"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 966
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voice_input_control_bcr"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 981
    :cond_4
    :goto_2
    const/16 v1, 0x29

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    goto :goto_1

    .line 969
    :cond_5
    const-string v1, "OCRSettings"

    const-string v2, "###### setCameraVoiceCommand ON"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 971
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 972
    const-string v2, "voice_input_control_bcr"

    .line 971
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    .line 975
    :cond_6
    const-string v1, "OCRSettings"

    const-string v2, "###### setCameraVoiceCommand OFF"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 977
    const-string v2, "voice_input_control_bcr"

    .line 976
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 978
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->stopVoiceRecognizer()V

    goto :goto_2
.end method

.method public setDictionaryDstID(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 894
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDictionaryDstID()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionaryDstID:I

    .line 896
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionaryDstID:I

    if-eq v1, p1, :cond_0

    .line 897
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setDictionaryDstID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionaryDstID:I

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 900
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_dictionary_dst_id"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionaryDstID:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 901
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 903
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setDictionarySrcID(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDictionarySrcID()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionarySrcID:I

    .line 879
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionarySrcID:I

    if-eq v1, p1, :cond_0

    .line 880
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setDictionarySrcID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionarySrcID:I

    .line 882
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 883
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_dictionary_src_id"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDictionarySrcID:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 884
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 886
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setEngineSelectedLanguage(Ljava/lang/String;)V
    .locals 4
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 861
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getEngineSelectedLanguage(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mEngineSelectedLanguage:Ljava/lang/String;

    .line 863
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mEngineSelectedLanguage:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 864
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setEngineSelectedLanguage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mEngineSelectedLanguage:Ljava/lang/String;

    .line 866
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 867
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_engine_selected_language_key"

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mEngineSelectedLanguage:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 868
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 870
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setLightMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getLightMode()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mLightMode:I

    .line 839
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mLightMode:I

    if-eq v1, p1, :cond_0

    .line 840
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setLightMode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mLightMode:I

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 843
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_light_mode_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mLightMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 844
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 846
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 350
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mMode:I

    if-eq v0, p1, :cond_0

    .line 351
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mMode:I

    .line 353
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 355
    :cond_0
    return-void
.end method

.method public setOCRDLPopupValue(ILjava/lang/String;Landroid/graphics/Rect;IZ)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "strValue"    # Ljava/lang/String;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "focusY"    # I
    .param p5, "bQRMode"    # Z

    .prologue
    .line 416
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupType:I

    .line 417
    iput-object p2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupStr:Ljava/lang/String;

    .line 418
    iput-object p3, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mDLPopupRect:Landroid/graphics/Rect;

    .line 419
    iput p4, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mFocusY:I

    .line 420
    iput-boolean p5, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mbQRMode:Z

    .line 421
    return-void
.end method

.method public setOCRFuncMode(I)V
    .locals 1
    .param p1, "ocrFuncMode"    # I

    .prologue
    .line 390
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    .line 391
    return-void
.end method

.method public setOCRFuncMode(IZ)V
    .locals 3
    .param p1, "ocrFuncMode"    # I
    .param p2, "bhandleChange"    # Z

    .prologue
    .line 394
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setOCRFuncMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRFuncMode:I

    .line 400
    const/16 v0, 0xc9

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 401
    if-eqz p2, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCR;->handleFuncModeChanged(I)V

    .line 405
    :cond_0
    return-void
.end method

.method public setOCRRealMode(I)V
    .locals 3
    .param p1, "ocrRealMode"    # I

    .prologue
    .line 377
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRRealMode:I

    if-eq v0, p1, :cond_0

    .line 378
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setOCRRealMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRRealMode:I

    .line 381
    const/16 v0, 0xc8

    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRRealMode:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 383
    :cond_0
    return-void
.end method

.method public setOCRSIPType(I)V
    .locals 0
    .param p1, "ocrSIPType"    # I

    .prologue
    .line 412
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mOCRSIPType:I

    .line 413
    return-void
.end method

.method public setOCRTRPopupValue(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Rect;IZ)V
    .locals 0
    .param p1, "aPopupStr"    # Ljava/lang/String;
    .param p2, "aSimpleMean"    # Ljava/lang/CharSequence;
    .param p3, "aFullMean"    # Ljava/lang/CharSequence;
    .param p4, "aRect"    # Landroid/graphics/Rect;
    .param p5, "type"    # I
    .param p6, "bQRMode"    # Z

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupStr:Ljava/lang/String;

    .line 445
    iput-object p2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupSimpleMeanStr:Ljava/lang/CharSequence;

    .line 446
    iput-object p3, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupFullMeanStr:Ljava/lang/CharSequence;

    .line 447
    iput-object p4, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupRect:Landroid/graphics/Rect;

    .line 448
    iput p5, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mTRPopupType:I

    .line 449
    iput-boolean p6, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mbQRMode:Z

    .line 450
    return-void
.end method

.method public setSelectedMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 362
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_selected_mode_key"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mSelectedMode:I

    .line 363
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mSelectedMode:I

    if-eq v1, p1, :cond_0

    .line 364
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setSelectedMode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mSelectedMode:I

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 367
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_selected_mode_key"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 368
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 370
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setShootingMode(I)V
    .locals 1
    .param p1, "shootingMode"    # I

    .prologue
    .line 477
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setShootingMode(IZ)V

    .line 478
    return-void
.end method

.method public setShootingMode(IZ)V
    .locals 3
    .param p1, "shootingMode"    # I
    .param p2, "exiting"    # Z

    .prologue
    .line 481
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mShootingMode:I

    if-eq v0, p1, :cond_0

    .line 482
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setShootingMode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mShootingMode:I

    .line 484
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 485
    if-nez p2, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCR;->handleShootingModeChanged(I)V

    .line 489
    :cond_0
    return-void
.end method

.method public setStorage(I)V
    .locals 4
    .param p1, "storage"    # I

    .prologue
    .line 599
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "pref_setup_storage_key"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCRSettings;->getDefaultStorage()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mStorage:I

    .line 600
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mStorage:I

    if-eq v1, p1, :cond_0

    .line 601
    const-string v1, "OCRSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setStorage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mStorage:I

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 604
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_setup_storage_key"

    iget v2, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mStorage:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 605
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 607
    iget v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mMode:I

    if-nez v1, :cond_0

    .line 608
    const/16 v1, 0x16

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 611
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setZoomValue(I)V
    .locals 3
    .param p1, "zoomvalue"    # I

    .prologue
    .line 579
    iget v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mZoomValue:I

    if-eq v0, p1, :cond_0

    .line 580
    const-string v0, "OCRSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setZoomValue "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    iput p1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mZoomValue:I

    .line 582
    const/16 v0, 0x12

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->notifyOCRSettingsChanged(II)V

    .line 584
    :cond_0
    return-void
.end method

.method public unregisterOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V
    .locals 2
    .param p1, "o"    # Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;

    .prologue
    .line 651
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    monitor-enter v1

    .line 652
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCRSettings;->mObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 651
    monitor-exit v1

    .line 654
    return-void

    .line 651
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
