.class public Lcom/sec/android/app/bcocr/widget/TwImageButton;
.super Landroid/widget/RelativeLayout;
.source "TwImageButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final ORIENTATION_0:I = 0x0

.field public static final ORIENTATION_180:I = 0x2

.field public static final ORIENTATION_270:I = 0x3

.field public static final ORIENTATION_90:I = 0x1


# instance fields
.field protected mButtonText:Landroid/widget/TextView;

.field public mDim:Z

.field protected mDimBackground:I

.field protected mDimImage:I

.field public mDisabled:Z

.field private mForward:Landroid/graphics/Matrix;

.field private final mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

.field protected mImageButton:Landroid/widget/ImageButton;

.field private mIsRotateAnimationEnabled:Z

.field public mLastOrientation:I

.field protected mNormalBackground:I

.field protected mNormalImage:I

.field protected mOnClickListener:Landroid/view/View$OnClickListener;

.field protected mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOrientationListener:Landroid/view/OrientationEventListener;

.field protected mPopupTextLayout:Landroid/widget/LinearLayout;

.field protected mPressedBackground:I

.field protected mPressedImage:I

.field private mReverse:Landroid/graphics/Matrix;

.field private mRotateAnimation:Landroid/view/animation/RotateAnimation;

.field private mRotation:I

.field private final mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

.field protected mTextColor:I

.field protected mTextSize:F

.field private pts:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->createButton()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;)V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const/high16 v1, 0x41400000    # 12.0f

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v1, 0x2

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v7, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v7, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->createButton()V

    .line 137
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 139
    .local v0, "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput p2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 140
    iput p3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "normalImage"    # I
    .param p3, "pressedImage"    # I
    .param p4, "dimmedImage"    # I

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->createButton()V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;)V

    .line 99
    iput p2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    .line 100
    iput p3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    .line 101
    iput p4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->refreshButtonImage()V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "normalImage"    # I
    .param p3, "pressedImage"    # I
    .param p4, "dimmedImage"    # I
    .param p5, "drawRotateAnim"    # Z

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 111
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->createButton()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;)V

    .line 116
    iput p2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    .line 117
    iput p3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    .line 118
    iput p4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->refreshButtonImage()V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 124
    invoke-virtual {p0, p5}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setRotateAnimation(Z)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 69
    invoke-virtual {p0, p2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->init(Landroid/util/AttributeSet;)V

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextSize:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTextColor:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 62
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 425
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 428
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    .line 429
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    .line 430
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    .line 432
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 434
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mShowAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 438
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mHideAlphaViewAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 443
    iput-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 77
    invoke-virtual {p0, p2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->init(Landroid/util/AttributeSet;)V

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setOrientationListener()V

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/widget/TwImageButton;I)I
    .locals 1

    .prologue
    .line 555
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->roundOrientation(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/widget/TwImageButton;II)V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->rotateButton(II)V

    return-void
.end method

.method private rotateButton(II)V
    .locals 7
    .param p1, "orientation"    # I
    .param p2, "lastOrientation"    # I

    .prologue
    const/16 v1, 0x10e

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 506
    if-ne p1, p2, :cond_0

    .line 525
    :goto_0
    return-void

    .line 509
    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    if-ne v0, v1, :cond_2

    .line 510
    const/16 p1, 0x168

    .line 515
    :cond_1
    :goto_1
    new-instance v0, Landroid/view/animation/RotateAnimation;

    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    rsub-int v1, v1, 0x168

    int-to-float v1, v1

    rsub-int v2, p1, 0x168

    int-to-float v2, v2

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/animation/RotateAnimation;->initialize(IIII)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setAnimation(Landroid/view/animation/Animation;)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 511
    :cond_2
    if-ne p1, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    if-nez v0, :cond_1

    .line 512
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLastOrientation:I

    goto :goto_1
.end method

.method private roundOrientation(I)I
    .locals 3
    .param p1, "orientationInput"    # I

    .prologue
    .line 556
    move v0, p1

    .line 557
    .local v0, "orientation":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 558
    const/4 v0, 0x0

    .line 561
    :cond_0
    rem-int/lit16 v0, v0, 0x168

    .line 563
    const/16 v2, 0x2d

    if-ge v0, v2, :cond_1

    .line 564
    const/4 v1, 0x0

    .line 574
    .local v1, "retVal":I
    :goto_0
    return v1

    .line 565
    .end local v1    # "retVal":I
    :cond_1
    const/16 v2, 0x87

    if-ge v0, v2, :cond_2

    .line 566
    const/16 v1, 0x5a

    .line 567
    .restart local v1    # "retVal":I
    goto :goto_0

    .end local v1    # "retVal":I
    :cond_2
    const/16 v2, 0xe1

    if-ge v0, v2, :cond_3

    .line 568
    const/16 v1, 0xb4

    .line 569
    .restart local v1    # "retVal":I
    goto :goto_0

    .end local v1    # "retVal":I
    :cond_3
    const/16 v2, 0x13b

    if-ge v0, v2, :cond_4

    .line 570
    const/16 v1, 0x10e

    .line 571
    .restart local v1    # "retVal":I
    goto :goto_0

    .line 572
    .end local v1    # "retVal":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "retVal":I
    goto :goto_0
.end method

.method private setOrientationListener()V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 529
    new-instance v0, Lcom/sec/android/app/bcocr/widget/TwImageButton$1;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/bcocr/widget/TwImageButton$1;-><init>(Lcom/sec/android/app/bcocr/widget/TwImageButton;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 552
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setRotateAnimation(Z)V

    .line 553
    return-void
.end method


# virtual methods
.method public SetClickSoundDisabled()V
    .locals 0

    .prologue
    .line 321
    return-void
.end method

.method public SetClickSoundEnabled()V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method protected actionInBounds(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    .line 278
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->getWidth()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 281
    :cond_1
    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->getHeight()I

    move-result v1

    if-gt p2, v1, :cond_0

    .line 284
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addTextLayout(IIIIII)V
    .locals 3
    .param p1, "xAxis"    # I
    .param p2, "yAxis"    # I
    .param p3, "gravity"    # I
    .param p4, "fontSize"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    const/4 v1, 0x0

    .line 225
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p5, p6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 226
    .local v0, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, p2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setGravity(I)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    int-to-float v2, p4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 176
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    .line 177
    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 180
    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 181
    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 182
    return-void
.end method

.method protected createButton()V
    .locals 2

    .prologue
    .line 185
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    .line 186
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 190
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 496
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    aput v1, v0, v2

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    aput v1, v0, v3

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    aget v0, v0, v2

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->pts:[F

    aget v1, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 502
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getNormalImage()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    return v0
.end method

.method public getPressedImage()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    return v0
.end method

.method public getRotateAnimation()Z
    .locals 1

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    return v0
.end method

.method protected init(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/bcocr/R$styleable;->TwImageButton:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 155
    .local v0, "attr":Landroid/content/res/TypedArray;
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    .line 156
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    .line 157
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    .line 159
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    .line 160
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    .line 161
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->createButton()V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->addView(Landroid/view/View;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->refreshButtonImage()V

    .line 167
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 168
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    if-nez v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 316
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 339
    iget-boolean v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    if-eqz v4, :cond_1

    move v1, v2

    .line 384
    :cond_0
    :goto_0
    return v1

    .line 343
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    if-eqz v4, :cond_2

    move v1, v2

    .line 344
    goto :goto_0

    .line 347
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    if-eqz v4, :cond_3

    .line 348
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->actionInBounds(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 349
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v4, p0, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    .line 350
    .local v1, "res":Z
    if-eqz v1, :cond_0

    .line 356
    .end local v1    # "res":Z
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_4
    :goto_1
    :pswitch_0
    move v1, v3

    .line 384
    goto :goto_0

    .line 358
    :pswitch_1
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setButtonImage(I)V

    .line 359
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    .line 360
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->showToolTip(Z)V

    goto :goto_1

    .line 363
    :pswitch_2
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setButtonImage(I)V

    .line 364
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    .line 365
    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->showToolTip(Z)V

    .line 367
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->actionInBounds(II)Z

    move-result v2

    if-nez v2, :cond_4

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    if-eqz v2, :cond_4

    .line 370
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 371
    .local v0, "ev":Landroid/view/MotionEvent;
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v2, p0, v0}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 377
    .end local v0    # "ev":Landroid/view/MotionEvent;
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    if-eqz v2, :cond_4

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v2, p0, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 356
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public refreshButtonImage()V
    .locals 2

    .prologue
    .line 295
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 296
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 303
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    if-eqz v0, :cond_2

    .line 304
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    .line 308
    :goto_1
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    .line 306
    :cond_2
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public registerPopupTextLayout(Landroid/widget/LinearLayout;)V
    .locals 1
    .param p1, "layout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPopupTextLayout:Landroid/widget/LinearLayout;

    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->showToolTip(Z)V

    .line 150
    return-void
.end method

.method public setBackgroundResources(III)V
    .locals 0
    .param p1, "normalBackground"    # I
    .param p2, "pressedBackground"    # I
    .param p3, "dimmedBackground"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    .line 267
    iput p2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    .line 268
    iput p3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    .line 269
    return-void
.end method

.method protected setButtonImage(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 397
    :cond_0
    return-void
.end method

.method public setDim(Z)V
    .locals 1
    .param p1, "dimmed"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    .line 194
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDim:Z

    if-eqz v0, :cond_1

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->SetClickSoundDisabled()V

    .line 196
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setButtonImage(I)V

    .line 197
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    if-eqz v0, :cond_0

    .line 198
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->SetClickSoundEnabled()V

    .line 202
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setButtonImage(I)V

    .line 203
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    if-eqz v0, :cond_0

    .line 204
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setDimBackground(I)V
    .locals 0
    .param p1, "dimmedBackground"    # I

    .prologue
    .line 262
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimBackground:I

    .line 263
    return-void
.end method

.method public setDimImage(I)V
    .locals 0
    .param p1, "dimmedImage"    # I

    .prologue
    .line 250
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    .line 251
    return-void
.end method

.method public setDisabled(Z)V
    .locals 1
    .param p1, "disabled"    # Z

    .prologue
    .line 210
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    .line 211
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDisabled:Z

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->SetClickSoundDisabled()V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->SetClickSoundEnabled()V

    goto :goto_0
.end method

.method public setImageResources(III)V
    .locals 0
    .param p1, "normalImage"    # I
    .param p2, "pressedImage"    # I
    .param p3, "dimmedImage"    # I

    .prologue
    .line 272
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    .line 273
    iput p2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    .line 274
    iput p3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mDimImage:I

    .line 275
    return-void
.end method

.method public setNormalBackground(I)V
    .locals 0
    .param p1, "normalBackground"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalBackground:I

    .line 255
    return-void
.end method

.method public setNormalImage(I)V
    .locals 0
    .param p1, "normalImage"    # I

    .prologue
    .line 242
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mNormalImage:I

    .line 243
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 331
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 336
    return-void
.end method

.method public setPressed()V
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setButtonImage(I)V

    .line 289
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    if-eqz v0, :cond_0

    .line 290
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwImageButton;->setBackgroundResource(I)V

    .line 292
    :cond_0
    return-void
.end method

.method public setPressedBackground(I)V
    .locals 0
    .param p1, "pressedBackground"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedBackground:I

    .line 259
    return-void
.end method

.method public setPressedImage(I)V
    .locals 0
    .param p1, "pressedImage"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPressedImage:I

    .line 247
    return-void
.end method

.method public setRotateAnimation(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 578
    if-eqz p1, :cond_0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 580
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    .line 585
    :goto_0
    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 583
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mIsRotateAnimationEnabled:Z

    goto :goto_0
.end method

.method public setRotation(I)V
    .locals 6
    .param p1, "rotation"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 448
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mForward:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    mul-int/lit8 v1, v1, 0x5a

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRight:I

    iget v3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLeft:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mBottom:I

    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTop:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mReverse:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRotation:I

    neg-int v1, v1

    mul-int/lit8 v1, v1, 0x5a

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mRight:I

    iget v3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mLeft:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mBottom:I

    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mTop:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 466
    return-void
.end method

.method public setSoundEffectsEnabled(Z)V
    .locals 1
    .param p1, "soundEffectsEnabled"    # Z

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 412
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setSoundEffectsEnabled(Z)V

    .line 413
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mButtonText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :cond_0
    return-void
.end method

.method protected showToolTip(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPopupTextLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwImageButton;->mPopupTextLayout:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 391
    :cond_0
    return-void

    .line 389
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method
