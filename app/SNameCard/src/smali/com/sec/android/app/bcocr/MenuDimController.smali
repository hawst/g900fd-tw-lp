.class public Lcom/sec/android/app/bcocr/MenuDimController;
.super Ljava/lang/Object;
.source "MenuDimController.java"


# static fields
.field private static final DEBUG:Z = false

.field public static final DIM_STATE_VISIBILITY:I = 0x8

.field private static final TAG:Ljava/lang/String; = "OCRMenuDimController"


# instance fields
.field private mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

.field private mDimArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mFlashStateArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public mbHideAllMenu:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V
    .locals 4
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mbHideAllMenu:Z

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    .line 44
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    .line 49
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 52
    return-void

    .line 45
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    const-string v0, "OCRMenuDimController"

    const-string v1, "clear"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 79
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    .line 80
    return-void
.end method

.method public getDim(I)Z
    .locals 1
    .param p1, "menu"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 108
    :cond_0
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public getFlashState(I)Z
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 138
    :cond_0
    const/4 v0, 0x0

    .line 140
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public getVisibility(I)I
    .locals 1
    .param p1, "menu"    # I

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mbHideAllMenu:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/MenuDimController;->getDim(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    const/16 v0, 0x8

    .line 119
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 56
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 60
    .end local v0    # "index":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 61
    const/4 v0, 0x0

    .restart local v0    # "index":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 65
    .end local v0    # "index":I
    :cond_1
    return-void

    .line 57
    .restart local v0    # "index":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public refreshButtonDim(IIZ)V
    .locals 7
    .param p1, "menu"    # I
    .param p2, "mode"    # I
    .param p3, "forceDim"    # Z

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-nez v1, :cond_0

    .line 148
    const-string v1, "OCRMenuDimController"

    const-string v2, "can not refresh menu-button because activity is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    .line 154
    .local v0, "os":Lcom/sec/android/app/bcocr/OCRSettings;
    if-eqz p3, :cond_1

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mbHideAllMenu:Z

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonMenu()V

    goto :goto_0

    .line 159
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mbHideAllMenu:Z

    .line 163
    packed-switch p1, :pswitch_data_0

    .line 200
    :cond_2
    :goto_1
    :pswitch_0
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_WIDE_CAPTURE_SUPPORT:Z

    if-nez v1, :cond_3

    .line 201
    new-array v1, v2, [I

    aput v2, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    .line 203
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-nez v1, :cond_4

    .line 204
    new-array v1, v2, [I

    aput v5, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    .line 206
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonMenu()V

    goto :goto_0

    .line 165
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 166
    new-array v1, v2, [I

    aput v6, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getnOCRNameCardCaptureModeType()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 168
    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    .line 172
    :goto_2
    if-ne p2, v2, :cond_2

    .line 173
    new-array v1, v2, [I

    aput v2, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getnOCRNameCardCaptureModeType()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    new-array v1, v2, [I

    aput v3, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    goto :goto_1

    .line 170
    :cond_5
    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    goto :goto_2

    .line 179
    :cond_6
    new-array v1, v2, [I

    aput v4, v1, v3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDimMulti([I)V

    .line 180
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    .line 182
    if-ne p2, v5, :cond_7

    .line 183
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    goto :goto_1

    .line 185
    :cond_7
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    goto :goto_1

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public refreshButtonMenu()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->refreshButtonMenu()V

    .line 213
    :cond_0
    return-void
.end method

.method public restore()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public setDim(IZ)V
    .locals 2
    .param p1, "menu"    # I
    .param p2, "dim"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setDimAll()V
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 104
    :cond_0
    return-void

    .line 101
    :cond_1
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mDimArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 102
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public varargs setDimMulti([I)V
    .locals 4
    .param p1, "indices"    # [I

    .prologue
    .line 92
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 95
    return-void

    .line 92
    :cond_0
    aget v0, p1, v1

    .line 93
    .local v0, "index":I
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setFlashState(IZ)V
    .locals 4
    .param p1, "reason"    # I
    .param p2, "dim"    # Z

    .prologue
    .line 124
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 129
    const/4 v0, 0x0

    .line 130
    .local v0, "flashDim":Z
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 133
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/bcocr/MenuDimController;->setDim(IZ)V

    goto :goto_0

    .line 131
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuDimController;->mFlashStateArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    or-int/2addr v0, v2

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
