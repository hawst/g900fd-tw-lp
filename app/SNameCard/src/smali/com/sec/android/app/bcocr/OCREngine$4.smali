.class Lcom/sec/android/app/bcocr/OCREngine$4;
.super Landroid/view/OrientationEventListener;
.source "OCREngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCREngine;->setOrientationListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Landroid/content/Context;)V
    .locals 0
    .param p2, "$anonymous0"    # Landroid/content/Context;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$4;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    .line 1978
    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 1981
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1982
    const-string v0, "OCREngine"

    const-string v1, "onOrientationChanged: orientation - unknown orientation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    :goto_0
    return-void

    .line 1985
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$4;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getLastOrientation()I

    move-result v0

    invoke-static {p1}, Lcom/sec/android/app/bcocr/OCREngine;->roundOrientation(I)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1986
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$4;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->startingPreviewCompleted()V

    .line 1988
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$4;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-static {p1}, Lcom/sec/android/app/bcocr/OCREngine;->roundOrientation(I)I

    move-result v1

    # invokes: Lcom/sec/android/app/bcocr/OCREngine;->setLastOrientation(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$17(Lcom/sec/android/app/bcocr/OCREngine;I)V

    goto :goto_0
.end method
