.class public Lcom/sec/android/app/bcocr/SettingLanguageActivity;
.super Landroid/app/Activity;
.source "SettingLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "SettinglanguageActivity"

.field private static mOCREngineLanguageSelectedSet:[I

.field private static mOCREnginelanguageSelectedNum:I


# instance fields
.field private Adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final LANG_ENGLISH_POSITION:I

.field private mIsAutoAdding_selectedSrcLang:Z

.field private mLanguageSetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mList:Landroid/widget/ListView;

.field private mOCREngineId_selectedSrcLang:I

.field private mOCREngineId_selectedSrcLangPosition:I

.field private mOCREngineLanguageSelected:[Z

.field private mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

.field private selectedItemOfListView:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    .line 40
    const/16 v0, 0x1b

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    iput v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->LANG_ENGLISH_POSITION:I

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->selectedItemOfListView:I

    .line 52
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    .line 53
    iput v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mIsAutoAdding_selectedSrcLang:Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 25
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/SettingLanguageActivity;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->selectedItemOfListView:I

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->selectedItemOfListView:I

    return v0
.end method

.method static synthetic access$4()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    return v0
.end method

.method static synthetic access$5(I)V
    .locals 0

    .prologue
    .line 28
    sput p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)[Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->setOCREngineLanguage()V

    return-void
.end method

.method private initCurrentOCREngineLanguage()V
    .locals 14

    .prologue
    const/16 v13, 0x1b

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070001

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "engine_language":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070003

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 208
    .local v0, "engine_lang_id":[I
    array-length v8, v1

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    .line 209
    const/4 v8, 0x7

    iput v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    .line 211
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getDictionarySrcID()I

    move-result v6

    .line 212
    .local v6, "selectedSrcLang":I
    invoke-static {p0, v6}, Lcom/sec/android/app/bcocr/OCRDicManager;->getLangID2MocrID(Landroid/content/Context;I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    .line 214
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    array-length v8, v8

    if-ge v2, v8, :cond_0

    array-length v8, v0

    if-lt v2, v8, :cond_3

    .line 220
    :cond_0
    const/4 v2, 0x0

    :goto_1
    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v8, v8

    if-lt v2, v8, :cond_5

    .line 224
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8, v12}, Lcom/sec/android/app/bcocr/OCRSettings;->getEngineSelectedLanguage(Z)Ljava/lang/String;

    move-result-object v4

    .line 225
    .local v4, "languageSet":Ljava/lang/String;
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v8, "^"

    invoke-direct {v7, v4, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .local v7, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    .line 227
    .local v5, "selectedLanguageNum":I
    if-le v5, v13, :cond_1

    .line 228
    const/16 v5, 0x1b

    .line 231
    :cond_1
    sput v11, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    .line 232
    iput-boolean v12, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mIsAutoAdding_selectedSrcLang:Z

    .line 234
    const/4 v2, 0x0

    :goto_2
    if-lt v2, v5, :cond_6

    .line 247
    iget-boolean v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mIsAutoAdding_selectedSrcLang:Z

    if-eqz v8, :cond_2

    .line 248
    sget v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    if-ge v8, v13, :cond_8

    .line 249
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    iget v9, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    aput-boolean v12, v8, v9

    .line 250
    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    sget v9, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    iget v10, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    aput v10, v8, v9

    .line 251
    sget v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v8, v8, 0x1

    sput v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    .line 272
    :cond_2
    :goto_3
    return-void

    .line 215
    .end local v4    # "languageSet":Ljava/lang/String;
    .end local v5    # "selectedLanguageNum":I
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    :cond_3
    aget v8, v0, v2

    iget v9, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    if-ne v8, v9, :cond_4

    .line 216
    iput v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    .line 218
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    aput-boolean v11, v8, v2

    .line 214
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 221
    :cond_5
    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    aput v11, v8, v2

    .line 220
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 236
    .restart local v4    # "languageSet":Ljava/lang/String;
    .restart local v5    # "selectedLanguageNum":I
    .restart local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    :cond_6
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 235
    invoke-static {p0, v8}, Lcom/sec/android/app/bcocr/OCRDicManager;->getOCREngineIndexLanguageString(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    .line 237
    .local v3, "languageIndex":I
    if-ltz v3, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    array-length v8, v8

    if-ge v3, v8, :cond_7

    .line 238
    iget-object v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    aput-boolean v12, v8, v3

    .line 239
    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    sget v9, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    aget v10, v0, v3

    aput v10, v8, v9

    .line 240
    sget v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v8, v8, 0x1

    sput v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    .line 241
    iget v8, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    aget v9, v0, v3

    if-ne v8, v9, :cond_7

    .line 242
    iput-boolean v11, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mIsAutoAdding_selectedSrcLang:Z

    .line 234
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 253
    .end local v3    # "languageIndex":I
    :cond_8
    const-string v9, "SettinglanguageActivity"

    .line 254
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "[OCR] initCurrentOCREngineLanguage - Too many langs are selected("

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 256
    sget v10, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 257
    const-string v10, ", remove:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 258
    sget v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    if-lez v8, :cond_9

    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    sget v11, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v11, v11, -0x1

    aget v8, v8, v11

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_4
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 259
    const-string v10, ")"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 254
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 253
    invoke-static {v9, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    sget v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    if-lez v8, :cond_2

    .line 261
    sget-object v8, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    sget v9, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v9, v9, -0x1

    iget v10, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLang:I

    aput v10, v8, v9

    goto/16 :goto_3

    .line 259
    :cond_9
    const-string v8, ""

    goto :goto_4
.end method

.method private setOCREngineLanguage()V
    .locals 7

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070001

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, "engine_language":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 169
    .local v0, "engine_lang_id":[I
    const-string v3, ""

    .line 170
    .local v3, "languageSelected":Ljava/lang/String;
    const/4 v4, 0x0

    .line 172
    .local v4, "numSelected":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v5, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v5, v5

    if-lt v2, v5, :cond_0

    .line 176
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    array-length v5, v5

    if-lt v2, v5, :cond_1

    .line 197
    iget-object v5, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/bcocr/OCRSettings;->setEngineSelectedLanguage(Ljava/lang/String;)V

    .line 198
    return-void

    .line 173
    :cond_0
    sget-object v5, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    const/4 v6, 0x0

    aput v6, v5, v2

    .line 172
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 177
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    aget-boolean v5, v5, v2

    if-eqz v5, :cond_5

    .line 179
    iget-boolean v5, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mIsAutoAdding_selectedSrcLang:Z

    if-eqz v5, :cond_2

    .line 180
    iget v5, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I

    if-eq v5, v2, :cond_3

    .line 182
    :cond_2
    const-string v5, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 183
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 190
    :cond_3
    :goto_2
    sget-object v5, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_4

    .line 191
    sget-object v5, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelectedSet:[I

    aget v6, v0, v2

    aput v6, v5, v4

    .line 193
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 176
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 185
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "^"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 186
    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 185
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v2, 0x7f030008

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->setContentView(I)V

    .line 62
    new-instance v2, Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCRSettings;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->resetObservers()V

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mLanguageSetList:Ljava/util/ArrayList;

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "engine_language":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->initCurrentOCREngineLanguage()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 73
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    .line 77
    new-instance v2, Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;

    const v3, 0x7f030005

    .line 78
    iget-object v4, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mLanguageSetList:Ljava/util/ArrayList;

    invoke-direct {v2, p0, p0, v3, v4}, Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;-><init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->Adapter:Landroid/widget/ArrayAdapter;

    .line 79
    const v2, 0x7f0f0016

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->Adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 83
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;-><init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity$2;-><init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 128
    return-void

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mLanguageSetList:Ljava/util/ArrayList;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineLanguageSelected:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_2

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 83
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 276
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 279
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 280
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 281
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 282
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 284
    return v2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->clear()V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 137
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 138
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 289
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 290
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 304
    :goto_0
    return v3

    .line 293
    :pswitch_0
    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "SettinglanguageActivity"

    const-string v2, "onOptionsItemSelected() error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 297
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
