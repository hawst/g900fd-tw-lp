.class final Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$ShutterCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ShutterCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;)V
    .locals 0

    .prologue
    .line 547
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    return-void
.end method


# virtual methods
.method public onShutter()V
    .locals 4

    .prologue
    .line 550
    const-string v0, "OCREngine"

    const-string v1, "ShutterCallback.onShutter"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-ShutterCallback**Point["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAPTURE_ANIMATION_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->startCaptureAnimation()V

    goto :goto_0
.end method
