.class public Lcom/sec/android/app/bcocr/CeRequest;
.super Ljava/lang/Object;
.source "CeRequest.java"


# static fields
.field public static final CE_CANCEL_VIDEO_RECORDING:I = 0x6a

.field public static final CE_CHANGE_PARAM:I = 0x7

.field public static final CE_DO_AUTOFOCUS:I = 0x5

.field public static final CE_DO_AUTOFOCUS_COMPLETED:I = 0x2

.field public static final CE_DO_CAPTURE:I = 0x6

.field public static final CE_DO_CAPTURE_COMPLETED:I = 0x3

.field public static final CE_HIDE_REVIEW_SCREEN:I = 0x17

.field public static final CE_IMAGE_STORING_COMPLETED:I = 0x7

.field public static final CE_INITIALIZE_RECORDER:I = 0x64

.field public static final CE_LAUNCH_GALLERY:I = 0x1a

.field public static final CE_PAUSE_VIDEO_RECORDING:I = 0x67

.field public static final CE_POST_INIT:I = 0x2

.field public static final CE_PREPARE_RECORDING_COMPLETED:I = 0x65

.field public static final CE_PREPARE_VIDEO_RECORDING:I = 0x65

.field public static final CE_PROCESS_BACK:I = 0x18

.field public static final CE_RESET_SETTINGS:I = 0x15

.field public static final CE_RESUME_VIDEO_RECORDING:I = 0x68

.field public static final CE_SET_ALL_PARAM:I = 0xa

.field public static final CE_SET_PARAM:I = 0x8

.field public static final CE_SHOW_REVIEW_SCREEN:I = 0x16

.field public static final CE_SMILE_DETECTION_CANCELED:I = 0x6

.field public static final CE_SMILE_DETECTION_COMPLETED:I = 0x5

.field public static final CE_START_ACTIONSHOT:I = 0x12

.field public static final CE_START_CONTINUOUS:I = 0xe

.field public static final CE_START_ENGINE:I = 0x0

.field public static final CE_START_ENGINE_COMPLETED:I = 0x0

.field public static final CE_START_PANORAMA:I = 0x10

.field public static final CE_START_PANORAMA_COMPLETED:I = 0x9

.field public static final CE_START_PREVIEW:I = 0x3

.field public static final CE_START_PREVIEW_COMPLETED:I = 0x1

.field public static final CE_START_PREVIEW_DUMMY:I = 0x1b

.field public static final CE_START_RECORDING_COMPLETED:I = 0x66

.field public static final CE_START_SMILE_DETECTION:I = 0xb

.field public static final CE_START_VIDEO_RECORDING:I = 0x66

.field public static final CE_STOP_ACTIONSHOT:I = 0x13

.field public static final CE_STOP_CONTINUOUS:I = 0xf

.field public static final CE_STOP_ENGINE:I = 0x1

.field public static final CE_STOP_PANORAMA:I = 0x11

.field public static final CE_STOP_PREVIEW:I = 0x4

.field public static final CE_STOP_PREVIEW_DUMMY:I = 0x19

.field public static final CE_STOP_SMILE_DETECTION:I = 0xc

.field public static final CE_STOP_VIDEO_RECORDING:I = 0x69

.field public static final CE_SURFACE_CREATED:I = 0x64

.field public static final CE_SWITCH_CAMERA:I = 0x14

.field public static final CE_WAIT:I = 0xd

.field public static final CE_WAIT_COMPLETED:I = 0x8

.field public static final CE_WAIT_FOR_SURFACE:I = 0x6b

.field private static final REQUEST_POOL_SIZE:I = 0xa

.field private static mPool:[Lcom/sec/android/app/bcocr/CeRequest;


# instance fields
.field private mParam:Ljava/lang/Object;

.field private mRequest:I

.field public mReserved:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/android/app/bcocr/CeRequest;

    sput-object v0, Lcom/sec/android/app/bcocr/CeRequest;->mPool:[Lcom/sec/android/app/bcocr/CeRequest;

    return-void
.end method

.method public constructor <init>(ILjava/lang/Object;)V
    .locals 1
    .param p1, "request"    # I
    .param p2, "param"    # Ljava/lang/Object;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CeRequest;->mParam:Ljava/lang/Object;

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    .line 107
    iput p1, p0, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    .line 108
    iput-object p2, p0, Lcom/sec/android/app/bcocr/CeRequest;->mParam:Ljava/lang/Object;

    .line 109
    return-void
.end method

.method public static obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;
    .locals 4
    .param p0, "request"    # I
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 87
    const/4 v1, 0x0

    .line 88
    .local v1, "req":Lcom/sec/android/app/bcocr/CeRequest;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    .line 103
    new-instance v2, Lcom/sec/android/app/bcocr/CeRequest;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/bcocr/CeRequest;-><init>(ILjava/lang/Object;)V

    :goto_1
    return-object v2

    .line 89
    :cond_0
    sget-object v2, Lcom/sec/android/app/bcocr/CeRequest;->mPool:[Lcom/sec/android/app/bcocr/CeRequest;

    aget-object v1, v2, v0

    .line 90
    if-nez v1, :cond_1

    .line 91
    new-instance v1, Lcom/sec/android/app/bcocr/CeRequest;

    .end local v1    # "req":Lcom/sec/android/app/bcocr/CeRequest;
    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/bcocr/CeRequest;-><init>(ILjava/lang/Object;)V

    .line 92
    .restart local v1    # "req":Lcom/sec/android/app/bcocr/CeRequest;
    iput-boolean v3, v1, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    .line 93
    sget-object v2, Lcom/sec/android/app/bcocr/CeRequest;->mPool:[Lcom/sec/android/app/bcocr/CeRequest;

    aput-object v1, v2, v0

    move-object v2, v1

    .line 94
    goto :goto_1

    .line 96
    :cond_1
    iget-boolean v2, v1, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    if-nez v2, :cond_2

    .line 97
    iput p0, v1, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    .line 98
    iput-object p1, v1, Lcom/sec/android/app/bcocr/CeRequest;->mParam:Ljava/lang/Object;

    .line 99
    iput-boolean v3, v1, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    move-object v2, v1

    .line 100
    goto :goto_1

    .line 88
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 125
    if-nez p1, :cond_1

    .line 136
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 129
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 133
    check-cast p1, Lcom/sec/android/app/bcocr/CeRequest;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    iget v2, p0, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    if-ne v1, v2, :cond_0

    .line 134
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequest;->mParam:Ljava/lang/Object;

    return-object v0
.end method

.method public getRequest()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    return v0
.end method

.method public setRequest(I)V
    .locals 0
    .param p1, "request"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/sec/android/app/bcocr/CeRequest;->mRequest:I

    .line 113
    return-void
.end method
