.class public Lcom/sec/android/app/bcocr/widget/TwZoomMenu;
.super Lcom/sec/android/app/bcocr/MenuBase;
.source "TwZoomMenu.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;


# static fields
.field private static final HANDLER_MENU_HIDE:I = 0x0

.field private static final HANDLER_REPEAT_CLICK:I = 0x1

.field private static final MENU_HIDE_DURATION:I = 0xbb8

.field private static final NUM_OF_MAX_HANDLER:I = 0x2

.field private static final REPEAT_CLICK_DURATION:I = 0x64

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIsRepeatClickAvailable:Z

.field private mLevelText:Landroid/widget/TextView;

.field private mMinusButton:Landroid/widget/ImageView;

.field private mPlusButton:Landroid/widget/ImageView;

.field private mSeekBar:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->TAG:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;II)V
    .locals 8
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "layoutId"    # I
    .param p3, "baseLayout"    # Landroid/view/ViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .param p5, "zOrder"    # I
    .param p6, "slideDirection"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 72
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/bcocr/MenuBase;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;IZ)V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    .line 36
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z

    .line 45
    new-instance v0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;-><init>(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    .line 74
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCRSettings;->registerOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V

    .line 76
    invoke-static {p6, v7}, Lcom/sec/android/app/bcocr/Util;->getSlideInAnimation(IZ)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setShowAnimation(Landroid/view/animation/Animation;)V

    .line 77
    invoke-static {p6, v7}, Lcom/sec/android/app/bcocr/Util;->getSlideOutAnimation(IZ)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setHideAnimation(Landroid/view/animation/Animation;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->init()V

    .line 80
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0032

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0031

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mPlusButton:Landroid/widget/ImageView;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mPlusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mPlusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mPlusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0033

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMinusButton:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMinusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMinusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMinusButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0034

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mLevelText:Landroid/widget/TextView;

    .line 94
    return-void
.end method

.method private setProgress(IZ)V
    .locals 7
    .param p1, "progress"    # I
    .param p2, "isSetting"    # Z

    .prologue
    const/4 v6, 0x0

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "x "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    int-to-float v4, p1

    sget v5, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->ZOOM_STEP:F

    div-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    :cond_1
    if-eqz p2, :cond_2

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setZoomValue(I)V

    .line 267
    :cond_2
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/bcocr/OCRSettings;->unregisterOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 124
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 129
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    .line 132
    .end local v0    # "i":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/bcocr/MenuBase;->clear()V

    .line 133
    return-void

    .line 125
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 124
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onActivityTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    .line 140
    :cond_0
    return v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    if-nez v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 176
    .local v0, "value":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 188
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setProgress(IZ)V

    goto :goto_0

    .line 178
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 179
    add-int/lit8 v0, v0, 0x1

    .line 181
    goto :goto_1

    .line 183
    :pswitch_2
    if-lez v0, :cond_2

    .line 184
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0031
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onHide()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 107
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->startContinuousAF()V

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->hideZoomBarAction()V

    .line 112
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 145
    sparse-switch p1, :sswitch_data_0

    .line 157
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 149
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mMinusButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 154
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mPlusButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 145
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
        0x45 -> :sswitch_0
        0x46 -> :sswitch_1
        0x9c -> :sswitch_0
        0x9d -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 162
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;

    invoke-static {v1, v4, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 215
    return v4
.end method

.method public onOCRSettingsChanged(II)V
    .locals 1
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 241
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 242
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setProgress(IZ)V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    .line 117
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekbar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 220
    if-eqz p3, :cond_0

    .line 221
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setProgress(IZ)V

    .line 223
    :cond_0
    return-void
.end method

.method protected onShow()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getZoomValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->setProgress(IZ)V

    .line 99
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 227
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 231
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 193
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 209
    :cond_0
    :goto_0
    return v1

    .line 195
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z

    goto :goto_0

    .line 199
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z

    goto :goto_0

    .line 205
    :pswitch_2
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
