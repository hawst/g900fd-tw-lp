.class public Lcom/sec/android/app/bcocr/EmptyView;
.super Lcom/sec/android/app/bcocr/MenuBase;
.source "EmptyView.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)V
    .locals 1
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "layoutId"    # I
    .param p3, "baseLayout"    # Landroid/view/ViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .param p5, "order"    # I

    .prologue
    const/4 v0, 0x1

    .line 26
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/bcocr/MenuBase;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)V

    .line 28
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/EmptyView;->setCaptureEnabled(Z)V

    .line 29
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/EmptyView;->setTouchHandled(Z)V

    .line 30
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/EmptyView;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 35
    invoke-super {p0}, Lcom/sec/android/app/bcocr/MenuBase;->clear()V

    .line 36
    return-void
.end method

.method public onBack()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/bcocr/EmptyView;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/EmptyView;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    .line 45
    invoke-super {p0}, Lcom/sec/android/app/bcocr/MenuBase;->onBack()V

    goto :goto_0
.end method
