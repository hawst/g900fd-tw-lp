.class Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SettingLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/SettingLanguageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LangArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    .line 142
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 143
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 147
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 148
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;->isEnabled(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 149
    const/4 v1, 0x7

    if-ne p1, v1, :cond_0

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 152
    :cond_0
    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$LangArrayAdapter;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mOCREngineId_selectedSrcLangPosition:I
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$0(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
