.class public Lcom/sec/android/app/bcocr/MonitoredActivity;
.super Landroid/app/Activity;
.source "MonitoredActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleAdapter;,
        Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;
    }
.end annotation


# instance fields
.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    .line 24
    return-void
.end method


# virtual methods
.method public addLifeCycleListener(Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    return-void

    .line 65
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .line 66
    .local v0, "listener":Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;
    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;->onActivityCreated(Lcom/sec/android/app/bcocr/MonitoredActivity;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 76
    return-void

    .line 73
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .line 74
    .local v0, "listener":Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;
    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;->onActivityDestroyed(Lcom/sec/android/app/bcocr/MonitoredActivity;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    return-void

    .line 81
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .line 82
    .local v0, "listener":Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;
    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;->onActivityStarted(Lcom/sec/android/app/bcocr/MonitoredActivity;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    return-void

    .line 89
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .line 90
    .local v0, "listener":Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;
    invoke-interface {v0, p0}, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;->onActivityStopped(Lcom/sec/android/app/bcocr/MonitoredActivity;)V

    goto :goto_0
.end method

.method public removeLifeCycleListener(Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MonitoredActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method
