.class Lcom/sec/android/app/bcocr/OCR$1;
.super Landroid/content/BroadcastReceiver;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 605
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 608
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 609
    .local v0, "action":Ljava/lang/String;
    const-string v8, "OCR"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[OCR] onReceive ["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z
    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCR;->access$0(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 612
    const-string v8, "OCR"

    const-string v9, "[OCR] onReceive - camera is destroying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    const-string v8, "com.samsung.cover.OPEN"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 617
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 618
    .local v4, "extra":Landroid/os/Bundle;
    const-string v8, "coverOpen"

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 620
    .local v3, "coverOpen":Z
    const-string v8, "OCR"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[OCR] ClearCover Open, coverOpen : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    if-nez v3, :cond_2

    .line 623
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 626
    .end local v3    # "coverOpen":Z
    .end local v4    # "extra":Landroid/os/Bundle;
    :cond_2
    const-string v8, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 627
    const/4 v5, 0x0

    .line 628
    .local v5, "flashState":Z
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9, v5}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 629
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v10}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 632
    .end local v5    # "flashState":Z
    :cond_3
    const-string v8, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 633
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 634
    .local v7, "myExtras":Landroid/os/Bundle;
    const-string v8, "flash_led_disable"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 635
    .local v1, "bLimitFlash":Z
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/bcocr/MenuDimController;->getFlashState(I)Z

    move-result v2

    .line 636
    .local v2, "bPrevFlashState":Z
    const-string v8, "OCR"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[OCR] bLimitFlash - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    if-eqz v1, :cond_9

    if-nez v2, :cond_9

    .line 638
    const-string v8, "OCR"

    const-string v9, "[OCR] bLimitFlash - so hot, flash is turned off"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->onFlashModeSelect(Z)V

    .line 640
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 641
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v10}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 642
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const v9, 0x7f0c0006

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 650
    .end local v1    # "bLimitFlash":Z
    .end local v2    # "bPrevFlashState":Z
    .end local v7    # "myExtras":Landroid/os/Bundle;
    :cond_4
    :goto_1
    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 651
    const-string v8, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 652
    const-string v8, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 653
    :cond_5
    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->OCR_EXTERNAL_STORAGE_NOT_SUPPORT:Z

    if-nez v8, :cond_7

    .line 656
    invoke-static {}, Landroid/media/MiniThumbFile;->reset()V

    .line 657
    const/4 v6, 0x0

    .line 658
    .local v6, "mIsSettingsMMC":Z
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 659
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 660
    const/4 v6, 0x1

    .line 663
    :cond_6
    if-eqz v6, :cond_7

    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->OCR_EXTERNAL_STORAGE_NOT_SUPPORT:Z

    if-nez v8, :cond_7

    .line 664
    const-string v8, "OCR"

    const-string v9, "GD/ Test"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/bcocr/OCR;->checkStorage(ZZ)V

    .line 666
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 726
    .end local v6    # "mIsSettingsMMC":Z
    :cond_7
    :goto_2
    const-string v8, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 727
    const-string v8, "OCR"

    const-string v9, "intent is comming from com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0c0022

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 729
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 732
    :cond_8
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v9, "CscFeature_Camera_SecurityMdmService"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 733
    const-string v8, "com.sktelecom.dmc.intent.action.DCMO_SET"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "CAMERA_ON"

    const/4 v9, 0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    if-nez v8, :cond_0

    .line 734
    const-string v8, "OCR"

    const-string v9, "[OCR] INTENT_MSG_DCMO"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto/16 :goto_0

    .line 643
    .restart local v1    # "bLimitFlash":Z
    .restart local v2    # "bPrevFlashState":Z
    .restart local v7    # "myExtras":Landroid/os/Bundle;
    :cond_9
    if-nez v1, :cond_4

    if-eqz v2, :cond_4

    .line 644
    const-string v8, "OCR"

    const-string v9, "[OCR] bLimitFlash - so cool, flash is available"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v8

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 646
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCR;->getLightMode()Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->onFlashModeSelect(Z)V

    goto/16 :goto_1

    .line 669
    .end local v1    # "bLimitFlash":Z
    .end local v2    # "bPrevFlashState":Z
    .end local v7    # "myExtras":Landroid/os/Bundle;
    :cond_a
    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 670
    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->OCR_EXTERNAL_STORAGE_NOT_SUPPORT:Z

    if-nez v8, :cond_7

    .line 673
    const-string v8, "OCR"

    const-string v9, "GD/ Test"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {}, Lcom/sec/android/app/bcocr/CheckMemory;->isStorageMounted()Z

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/bcocr/OCR;->checkStorage(ZZ)V

    goto/16 :goto_2

    .line 676
    :cond_b
    const-string v8, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 677
    const-string v8, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 678
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->handleBatteryChanged(Landroid/content/Intent;)V
    invoke-static {v8, p2}, Lcom/sec/android/app/bcocr/OCR;->access$1(Lcom/sec/android/app/bcocr/OCR;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 679
    :cond_c
    const-string v8, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 680
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->battLevel:I
    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCR;->access$2(Lcom/sec/android/app/bcocr/OCR;)I

    move-result v8

    sget v9, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_BATTERY_THRESHOLD_VALUE:I

    if-gt v8, v9, :cond_d

    .line 681
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->handleLowBattery(Z)V

    goto/16 :goto_2

    .line 683
    :cond_d
    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v8, :cond_7

    .line 684
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-boolean v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    if-nez v8, :cond_7

    .line 685
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    # invokes: Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V
    invoke-static {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->access$3(Lcom/sec/android/app/bcocr/OCR;Z)V

    goto/16 :goto_2

    .line 689
    :cond_e
    const-string v8, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 690
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->battLevel:I
    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCR;->access$2(Lcom/sec/android/app/bcocr/OCR;)I

    move-result v8

    sget v9, Lcom/sec/android/app/bcocr/OCR;->LOW_BATTERY_WARNING_LEVEL:I

    if-gt v8, v9, :cond_12

    .line 691
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->battLevel:I
    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCR;->access$2(Lcom/sec/android/app/bcocr/OCR;)I

    move-result v8

    sget v9, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_BATTERY_THRESHOLD_VALUE:I

    if-gt v8, v9, :cond_10

    .line 692
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 693
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->dismiss()V

    .line 695
    :cond_f
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->handleLowBattery(Z)V

    goto/16 :goto_2

    .line 697
    :cond_10
    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v8, :cond_7

    .line 698
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 699
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->dismiss()V

    .line 701
    :cond_11
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 702
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    # invokes: Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V
    invoke-static {v8, v9}, Lcom/sec/android/app/bcocr/OCR;->access$3(Lcom/sec/android/app/bcocr/OCR;Z)V

    goto/16 :goto_2

    .line 706
    :cond_12
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v8, :cond_13

    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 707
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->dismiss()V

    .line 709
    :cond_13
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR$1;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryPopup:Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 711
    :cond_14
    const-string v8, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 712
    const-string v8, "OCR"

    const-string v9, "[OCR] INTENT_MSG_SECURITY"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 722
    :cond_15
    const-string v8, "POWER_OFF_ANIMATION_START"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 723
    const-string v8, "OCR"

    const-string v9, "[OCR] onReceive shutdown"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
