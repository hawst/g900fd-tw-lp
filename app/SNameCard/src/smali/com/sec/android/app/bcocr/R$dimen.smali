.class public final Lcom/sec/android/app/bcocr/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final actionbarheight_land:I = 0x7f090042

.field public static final actionbarheight_number_port:I = 0x7f090046

.field public static final actionbarheight_port:I = 0x7f090041

.field public static final actionbarheight_url_port:I = 0x7f090045

.field public static final af_touch_rect_height:I = 0x7f090004

.field public static final af_touch_rect_width:I = 0x7f090003

.field public static final airview_popup_height:I = 0x7f090006

.field public static final airview_popup_width:I = 0x7f090005

.field public static final anchor_max_left_padding:I = 0x7f090076

.field public static final calculate_link_popup_editmode_position_y:I = 0x7f0900ec

.field public static final calculate_link_popup_editmode_position_y_type_phone_number:I = 0x7f0900ed

.field public static final calculate_word_popup_editmode_position_y:I = 0x7f0900ee

.field public static final capture_anim_rect_line_width:I = 0x7f09010c

.field public static final captureswitchbutton_bg_height:I = 0x7f0900ba

.field public static final captureswitchbutton_bg_posx:I = 0x7f0900bb

.field public static final captureswitchbutton_bg_posy:I = 0x7f0900bc

.field public static final captureswitchbutton_bg_width:I = 0x7f0900b9

.field public static final captureswitchbutton_image_height:I = 0x7f0900be

.field public static final captureswitchbutton_image_width:I = 0x7f0900bd

.field public static final captureswitchbutton_max_movement:I = 0x7f0900c3

.field public static final captureswitchbutton_padding_height:I = 0x7f0900c5

.field public static final captureswitchbutton_padding_width:I = 0x7f0900c4

.field public static final captureswitchbutton_switch_ball_height:I = 0x7f0900c0

.field public static final captureswitchbutton_switch_ball_posx:I = 0x7f0900c1

.field public static final captureswitchbutton_switch_ball_posy:I = 0x7f0900c2

.field public static final captureswitchbutton_switch_ball_width:I = 0x7f0900bf

.field public static final default_slider_bar_margin_top:I = 0x7f090001

.field public static final default_slider_focus_bar_margin_top:I = 0x7f090002

.field public static final default_slider_layout_width:I = 0x7f090000

.field public static final detectedAreaMargin:I = 0x7f09004a

.field public static final detectedAreaMarginBottom:I = 0x7f09004d

.field public static final detectedAreaMarginRight:I = 0x7f09004c

.field public static final detectedTopMargin:I = 0x7f09004b

.field public static final detectedWordMargin:I = 0x7f090049

.field public static final direct_link_edittext_layout_margin:I = 0x7f090065

.field public static final direct_link_edittext_margin:I = 0x7f090064

.field public static final direct_link_edittext_max_width:I = 0x7f090063

.field public static final direct_link_edittext_min_width:I = 0x7f090062

.field public static final direct_link_popup_closebutton_size:I = 0x7f090068

.field public static final direct_link_popup_max_width:I = 0x7f090067

.field public static final direct_link_popup_min_width:I = 0x7f090066

.field public static final directlink_popup_left_pos:I = 0x7f090080

.field public static final directlink_popup_max_pos:I = 0x7f09007f

.field public static final directlink_popup_min_pos:I = 0x7f09007e

.field public static final directlink_popup_right_pos:I = 0x7f090081

.field public static final focus_additional_margin:I = 0x7f090040

.field public static final focus_indicator_bracket_additional_height_margin:I = 0x7f09003d

.field public static final focus_indicator_bracket_additional_left_margin:I = 0x7f09003e

.field public static final focus_indicator_bracket_additional_top_margin:I = 0x7f09003f

.field public static final focus_indicator_bracket_additional_width_margin:I = 0x7f09003c

.field public static final focus_indicator_bracket_height:I = 0x7f090039

.field public static final focus_indicator_bracket_margin_left:I = 0x7f09003b

.field public static final focus_indicator_bracket_margin_top:I = 0x7f09003a

.field public static final focus_indicator_bracket_width:I = 0x7f090038

.field public static final focus_indicator_cross_margin_top:I = 0x7f090037

.field public static final focus_indicator_height:I = 0x7f090036

.field public static final focus_indicator_width:I = 0x7f090035

.field public static final history_expandable_list_indicator_left:I = 0x7f0900fb

.field public static final history_expandable_list_indicator_right:I = 0x7f0900fc

.field public static final history_no_history_description_height:I = 0x7f0900fa

.field public static final history_no_history_description_width:I = 0x7f0900f9

.field public static final history_no_history_description_x:I = 0x7f0900f7

.field public static final history_no_history_description_y:I = 0x7f0900f8

.field public static final history_no_history_image_height:I = 0x7f0900f2

.field public static final history_no_history_image_width:I = 0x7f0900f1

.field public static final history_no_history_image_x:I = 0x7f0900ef

.field public static final history_no_history_image_y:I = 0x7f0900f0

.field public static final history_no_history_title_height:I = 0x7f0900f6

.field public static final history_no_history_title_width:I = 0x7f0900f5

.field public static final history_no_history_title_x:I = 0x7f0900f3

.field public static final history_no_history_title_y:I = 0x7f0900f4

.field public static final langauge_selector_height:I = 0x7f090043

.field public static final link_popup_height_land:I = 0x7f090057

.field public static final link_popup_height_port:I = 0x7f090058

.field public static final link_popup_height_port_browser:I = 0x7f090059

.field public static final link_popup_margine_except_textarea:I = 0x7f090053

.field public static final link_popup_max_width_land:I = 0x7f090052

.field public static final link_popup_qr_atc_icon_width:I = 0x7f090050

.field public static final link_popup_title_padding_left:I = 0x7f090055

.field public static final link_popup_title_padding_right:I = 0x7f090056

.field public static final link_popup_width_land:I = 0x7f090051

.field public static final link_popup_width_port:I = 0x7f090054

.field public static final max_zoom_level:I = 0x7f090088

.field public static final max_zoom_ratio:I = 0x7f090086

.field public static final min_zoom_ratio:I = 0x7f090087

.field public static final normal_word_bar_height:I = 0x7f090048

.field public static final popupAnchorHeight:I = 0x7f09004f

.field public static final popupAnchorWidth:I = 0x7f09004e

.field public static final popup_dl_body_height:I = 0x7f0900a0

.field public static final popup_dl_body_padding:I = 0x7f0900a2

.field public static final popup_dl_body_width:I = 0x7f09009e

.field public static final popup_dl_divider_height:I = 0x7f0900a1

.field public static final popup_dl_min_body_width:I = 0x7f09009f

.field public static final popup_title_height_email_port:I = 0x7f090044

.field public static final postview_link_popup_background_height:I = 0x7f09005b

.field public static final postview_popup_min_margine_to_the_end:I = 0x7f09005a

.field public static final postview_popup_title_1line_height:I = 0x7f09005c

.field public static final postview_popup_title_2line_height:I = 0x7f09005d

.field public static final postview_popup_title_2line_margine:I = 0x7f09005e

.field public static final postview_sip_popup_body_height:I = 0x7f09005f

.field public static final preview_businesscard_edge_detect_cue_icon_height:I = 0x7f090034

.field public static final preview_businesscard_edge_detect_cue_icon_width:I = 0x7f090033

.field public static final preview_businesscard_edge_guide_margin_bottom:I = 0x7f09000e

.field public static final preview_businesscard_edge_guide_margin_left:I = 0x7f09000b

.field public static final preview_businesscard_edge_guide_margin_right:I = 0x7f09000d

.field public static final preview_businesscard_edge_guide_margin_top:I = 0x7f09000c

.field public static final preview_detected_area_extra_margin_bottom:I = 0x7f090027

.field public static final preview_detected_area_extra_margin_left:I = 0x7f090024

.field public static final preview_detected_area_extra_margin_right:I = 0x7f090026

.field public static final preview_detected_area_extra_margin_top:I = 0x7f090025

.field public static final preview_detected_area_extra_qr_margin_left:I = 0x7f090028

.field public static final preview_detected_area_extra_qr_margin_top:I = 0x7f090029

.field public static final preview_detected_area_margin_bottom:I = 0x7f090023

.field public static final preview_detected_area_margin_left:I = 0x7f090020

.field public static final preview_detected_area_margin_right:I = 0x7f090022

.field public static final preview_detected_area_margin_top:I = 0x7f090021

.field public static final preview_detected_area_sip_margin_bottom:I = 0x7f09001f

.field public static final preview_detected_area_sip_margin_left:I = 0x7f09001c

.field public static final preview_detected_area_sip_margin_right:I = 0x7f09001e

.field public static final preview_detected_area_sip_margin_top:I = 0x7f09001d

.field public static final preview_detected_area_tf_margin_top:I = 0x7f09001b

.field public static final preview_detected_rect_phonenumber_addional_margin:I = 0x7f090032

.field public static final preview_focus_bottom:I = 0x7f09002d

.field public static final preview_focus_boundary_down:I = 0x7f09002f

.field public static final preview_focus_boundary_up:I = 0x7f09002e

.field public static final preview_focus_left:I = 0x7f09002a

.field public static final preview_focus_right:I = 0x7f09002c

.field public static final preview_focus_top:I = 0x7f09002b

.field public static final preview_focus_x:I = 0x7f090030

.field public static final preview_focus_y:I = 0x7f090031

.field public static final preview_fragment_left_sidemenu_selected_funcmode_width:I = 0x7f090017

.field public static final preview_fragment_left_sidemenu_selected_funcmode_width_noshadow:I = 0x7f090018

.field public static final preview_fragment_left_sidemenu_selecting_funcmode_width:I = 0x7f090019

.field public static final preview_fragment_right_sidemenu_realtime_width:I = 0x7f09001a

.field public static final preview_popup_anchor_margin:I = 0x7f09008c

.field public static final preview_popup_anchor_margin_top:I = 0x7f09008d

.field public static final preview_popup_dl_body_height:I = 0x7f090096

.field public static final preview_popup_dl_body_width:I = 0x7f090094

.field public static final preview_popup_dl_divider_height:I = 0x7f090097

.field public static final preview_popup_dl_min_body_width:I = 0x7f090095

.field public static final preview_popup_edit_margin_top:I = 0x7f09008e

.field public static final preview_popup_qrcode_extra_margin_top:I = 0x7f09008f

.field public static final preview_popup_side_margin:I = 0x7f09008b

.field public static final preview_popup_title_1line_height:I = 0x7f09009c

.field public static final preview_popup_title_2line_height:I = 0x7f09009d

.field public static final preview_popup_title_padding_left:I = 0x7f090098

.field public static final preview_popup_title_padding_right_dl:I = 0x7f09009a

.field public static final preview_popup_title_padding_right_qr:I = 0x7f090099

.field public static final preview_popup_title_padding_right_tr:I = 0x7f09009b

.field public static final preview_popup_tr_default_padding:I = 0x7f090092

.field public static final preview_popup_tr_dic_top_padding:I = 0x7f090093

.field public static final preview_popup_tr_max_height:I = 0x7f090091

.field public static final preview_popup_tr_min_height:I = 0x7f090090

.field public static final preview_popup_width:I = 0x7f09008a

.field public static final screen_height:I = 0x7f09000a

.field public static final screen_width:I = 0x7f090009

.field public static final selected_word_bar_height:I = 0x7f090047

.field public static final sharevia_popup_width:I = 0x7f090060

.field public static final sidemenu_border_width:I = 0x7f090061

.field public static final splitview_image_height:I = 0x7f090007

.field public static final splitview_image_width:I = 0x7f090008

.field public static final translator_full_popup_land_height:I = 0x7f09006c

.field public static final translator_full_popup_port_height:I = 0x7f09006b

.field public static final translator_popup_acchor_down_position_y:I = 0x7f0900b1

.field public static final translator_popup_big_bg_margin_height:I = 0x7f0900a8

.field public static final translator_popup_big_bg_max_height:I = 0x7f0900a7

.field public static final translator_popup_big_bg_min_height:I = 0x7f0900a6

.field public static final translator_popup_big_extra_margin:I = 0x7f0900a5

.field public static final translator_popup_big_max_height:I = 0x7f0900a4

.field public static final translator_popup_big_min_height:I = 0x7f0900a3

.field public static final translator_popup_height_1line:I = 0x7f0900b0

.field public static final translator_popup_height_2line:I = 0x7f0900af

.field public static final translator_popup_land_height:I = 0x7f09006a

.field public static final translator_popup_long_x_land:I = 0x7f09006d

.field public static final translator_popup_long_x_port:I = 0x7f09006f

.field public static final translator_popup_long_y_land:I = 0x7f09006e

.field public static final translator_popup_long_y_port:I = 0x7f090070

.field public static final translator_popup_meaning_height_1line:I = 0x7f0900ae

.field public static final translator_popup_meaning_height_2line:I = 0x7f0900ad

.field public static final translator_popup_meaning_image_padding_top:I = 0x7f0900b6

.field public static final translator_popup_meaning_image_right_margin_1line:I = 0x7f0900b5

.field public static final translator_popup_meaning_image_right_margin_2line:I = 0x7f0900b3

.field public static final translator_popup_meaning_image_top_margin_1line:I = 0x7f0900b4

.field public static final translator_popup_meaning_image_top_margin_2line:I = 0x7f0900b2

.field public static final translator_popup_meaning_no_image_padding_top:I = 0x7f0900b7

.field public static final translator_popup_meaning_padding_top_1line:I = 0x7f0900b8

.field public static final translator_popup_padding_top_max:I = 0x7f0900ab

.field public static final translator_popup_padding_top_min:I = 0x7f0900ac

.field public static final translator_popup_port_height:I = 0x7f090069

.field public static final translator_popup_title_padding_left:I = 0x7f0900a9

.field public static final translator_popup_title_padding_right:I = 0x7f0900aa

.field public static final translator_sentense_underbar_height:I = 0x7f090071

.field public static final tutorial_auto_step_1_margin:I = 0x7f0900ce

.field public static final tutorial_auto_step_1_position_y:I = 0x7f0900cd

.field public static final tutorial_auto_step_2_margin:I = 0x7f0900cf

.field public static final tutorial_common_margin:I = 0x7f0900ea

.field public static final tutorial_detect_text_step_1_pointer_x:I = 0x7f0900d9

.field public static final tutorial_detect_text_step_1_pointer_y:I = 0x7f0900da

.field public static final tutorial_detect_text_step_1_popup_x:I = 0x7f0900db

.field public static final tutorial_detect_text_step_1_popup_y:I = 0x7f0900dc

.field public static final tutorial_detect_text_step_2_margin:I = 0x7f0900df

.field public static final tutorial_detect_text_step_2_pointer_x:I = 0x7f0900dd

.field public static final tutorial_detect_text_step_2_pointer_y:I = 0x7f0900de

.field public static final tutorial_detect_text_step_3_pointer_x:I = 0x7f0900e0

.field public static final tutorial_detect_text_step_3_pointer_y:I = 0x7f0900e1

.field public static final tutorial_detect_text_step_3_popup_x:I = 0x7f0900e2

.field public static final tutorial_detect_text_step_3_popup_y:I = 0x7f0900e3

.field public static final tutorial_detect_text_step_4_pointer_x:I = 0x7f0900e4

.field public static final tutorial_detect_text_step_4_pointer_y:I = 0x7f0900e5

.field public static final tutorial_detect_text_step_4_popup_x:I = 0x7f0900e6

.field public static final tutorial_detect_text_step_4_popup_y:I = 0x7f0900e7

.field public static final tutorial_detect_text_step_5_margin:I = 0x7f0900e8

.field public static final tutorial_popup_inside_margin_down:I = 0x7f0900c6

.field public static final tutorial_popup_inside_margin_left:I = 0x7f0900c9

.field public static final tutorial_popup_inside_margin_right:I = 0x7f0900c8

.field public static final tutorial_popup_inside_margin_up:I = 0x7f0900c7

.field public static final tutorial_popup_picker_alignment_default_margin:I = 0x7f0900ca

.field public static final tutorial_popup_width:I = 0x7f0900cc

.field public static final tutorial_popup_width_huge:I = 0x7f0900cb

.field public static final tutorial_text_defalt_margin:I = 0x7f0900e9

.field public static final tutorial_translator_step_1_pointer_x:I = 0x7f0900d0

.field public static final tutorial_translator_step_1_pointer_y:I = 0x7f0900d1

.field public static final tutorial_translator_step_1_popup_x:I = 0x7f0900d2

.field public static final tutorial_translator_step_1_popup_y:I = 0x7f0900d3

.field public static final tutorial_translator_step_2_margin:I = 0x7f0900d6

.field public static final tutorial_translator_step_2_pointer_x:I = 0x7f0900d4

.field public static final tutorial_translator_step_2_pointer_y:I = 0x7f0900d5

.field public static final tutorial_translator_step_3_margin:I = 0x7f0900d7

.field public static final tutorial_translator_step_4_margin:I = 0x7f0900d8

.field public static final tw_text_view_fade_out_size:I = 0x7f0900eb

.field public static final wide_capture_focus_horizontal_height:I = 0x7f090106

.field public static final wide_capture_focus_horizontal_width:I = 0x7f090105

.field public static final wide_capture_focus_vertical_height:I = 0x7f090108

.field public static final wide_capture_focus_vertical_width:I = 0x7f090107

.field public static final wide_capture_move_slowly_text_horizontal_margin:I = 0x7f09010a

.field public static final wide_capture_move_slowly_text_vertical_margin:I = 0x7f09010b

.field public static final wide_capture_thumbnail_horizontal_height:I = 0x7f090100

.field public static final wide_capture_thumbnail_horizontal_width:I = 0x7f0900ff

.field public static final wide_capture_thumbnail_horizontal_x:I = 0x7f0900fd

.field public static final wide_capture_thumbnail_horizontal_y:I = 0x7f0900fe

.field public static final wide_capture_thumbnail_vertical_height:I = 0x7f090104

.field public static final wide_capture_thumbnail_vertical_width:I = 0x7f090103

.field public static final wide_capture_thumbnail_vertical_x:I = 0x7f090101

.field public static final wide_capture_thumbnail_vertical_y:I = 0x7f090102

.field public static final wide_capture_warning_arrow_swing_distance:I = 0x7f090109

.field public static final word_big_popup_width:I = 0x7f09007c

.field public static final word_popup_center_pos:I = 0x7f090077

.field public static final word_popup_center_xpos:I = 0x7f090079

.field public static final word_popup_default_padding:I = 0x7f09007d

.field public static final word_popup_full_meaning_land_width:I = 0x7f090074

.field public static final word_popup_full_meaning_port_width:I = 0x7f090075

.field public static final word_popup_land_width:I = 0x7f090072

.field public static final word_popup_left_margin:I = 0x7f090078

.field public static final word_popup_port_width:I = 0x7f090073

.field public static final word_popup_right_xpos:I = 0x7f09007a

.field public static final word_popup_width:I = 0x7f09007b

.field public static final zoom_rect_line_width:I = 0x7f090082

.field public static final zoom_rect_width:I = 0x7f090089

.field public static final zoom_text_pos_x:I = 0x7f090085

.field public static final zoom_text_size:I = 0x7f090083

.field public static final zoom_text_upper_margin:I = 0x7f090084

.field public static final zoommenu_control_layout_height:I = 0x7f090012

.field public static final zoommenu_control_layout_width:I = 0x7f090011

.field public static final zoommenu_control_layout_x:I = 0x7f09000f

.field public static final zoommenu_control_layout_y:I = 0x7f090010

.field public static final zoommenu_level_text_font_size:I = 0x7f090016

.field public static final zoommenu_seekbar_height:I = 0x7f090014

.field public static final zoommenu_seekbar_padding:I = 0x7f090015

.field public static final zoommenu_seekbar_width:I = 0x7f090013


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
