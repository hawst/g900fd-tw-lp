.class public Lcom/sec/android/app/bcocr/CeStatePreviewing;
.super Lcom/sec/android/app/bcocr/AbstractCeState;
.source "CeStatePreviewing.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CeStatePreviewing"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V
    .locals 0
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;
    .param p2, "requestQueue"    # Lcom/sec/android/app/bcocr/CeRequestQueue;
    .param p3, "id"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/AbstractCeState;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public cancelRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    .line 34
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 84
    const-string v0, "CeStatePreviewing"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HandleMessage - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 87
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 93
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->imageStoringCompleted()V

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 99
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 102
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 105
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 109
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_1
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0x65 -> :sswitch_6
    .end sparse-switch
.end method

.method public handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    const/4 v1, 0x1

    .line 38
    const-string v0, "CeStatePreviewing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HandleRequest - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 76
    const-string v0, "CeStatePreviewing"

    const-string v1, "invalid request id for current state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    .line 79
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 41
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewDummySync()V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 43
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 47
    goto :goto_0

    .line 49
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 52
    goto :goto_0

    .line 54
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doAutoFocusAsync()V

    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getParam()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->doSetParametersSync(Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 59
    goto :goto_0

    .line 61
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getParam()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->doChangeParameterSync(Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 63
    goto :goto_0

    .line 65
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doSetAllParamsSync()V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 67
    goto :goto_0

    .line 69
    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getParam()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/bcocr/OCREngine;->doWaitAsync(I)V

    move v0, v1

    .line 70
    goto/16 :goto_0

    .line 72
    :sswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doProcessBackSync()V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStatePreviewing;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 74
    goto/16 :goto_0

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x4 -> :sswitch_1
        0x5 -> :sswitch_3
        0x7 -> :sswitch_5
        0x8 -> :sswitch_4
        0xa -> :sswitch_6
        0xd -> :sswitch_7
        0x18 -> :sswitch_8
        0x19 -> :sswitch_0
    .end sparse-switch
.end method
