.class Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
.super Landroid/os/Handler;
.source "CeRequestQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/CeRequestQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/CeRequestQueue;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/CeRequestQueue;Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;-><init>(Lcom/sec/android/app/bcocr/CeRequestQueue;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v2, 0xc8

    const/4 v1, 0x1

    .line 45
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    .line 46
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->removeMessages(I)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$0(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getLandscapeActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$1(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$1(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$0(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isOnResumePending()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$1(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$1(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$0(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # getter for: Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$0(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    goto :goto_0

    .line 59
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->this$0:Lcom/sec/android/app/bcocr/CeRequestQueue;

    # invokes: Lcom/sec/android/app/bcocr/CeRequestQueue;->startFirstRequest()V
    invoke-static {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->access$2(Lcom/sec/android/app/bcocr/CeRequestQueue;)V

    goto :goto_0
.end method
