.class public Lcom/sec/android/app/bcocr/widget/TwBaseMenu;
.super Lcom/sec/android/app/bcocr/MenuBase;
.source "TwBaseMenu.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;)V
    .locals 7
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "layoutId"    # I
    .param p3, "baseLayout"    # Landroid/view/ViewGroup;
    .param p4, "menuResourceDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .prologue
    .line 11
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/bcocr/MenuBase;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;IZ)V

    .line 12
    return-void
.end method


# virtual methods
.method public onBack()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-nez v0, :cond_0

    .line 21
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    goto :goto_0
.end method
