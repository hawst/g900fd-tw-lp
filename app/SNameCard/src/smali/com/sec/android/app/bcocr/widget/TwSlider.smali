.class public Lcom/sec/android/app/bcocr/widget/TwSlider;
.super Landroid/widget/RelativeLayout;
.source "TwSlider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;
    }
.end annotation


# instance fields
.field protected mFocusSliderBarImageResourceId:I

.field protected mFocusSliderBarMarginTop:I

.field protected mFocusSliderBarWidth:I

.field protected mFocusSliderPosition:I

.field protected mNumberOfGauge:I

.field protected mSliderBarImageResourceId:I

.field protected mSliderBarImageWidth:I

.field protected mSliderBarMarginTop:I

.field protected mSliderFocusImageHeight:I

.field protected mSliderFocusImageWidth:I

.field protected mSliderGuages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field protected mSliderImageHeight:I

.field protected mSliderImageWidth:I

.field protected mSliderLayoutWidth:I

.field private mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderGuages:Ljava/util/List;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    .line 70
    invoke-virtual {p0, p2}, Lcom/sec/android/app/bcocr/widget/TwSlider;->init(Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderGuages:Ljava/util/List;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    .line 65
    invoke-virtual {p0, p2}, Lcom/sec/android/app/bcocr/widget/TwSlider;->init(Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method private initAttrs(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/bcocr/R$styleable;->TwSlider:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 82
    .local v0, "attr":Landroid/content/res/TypedArray;
    const/4 v1, 0x2

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 81
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mNumberOfGauge:I

    .line 85
    const/4 v1, 0x3

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 84
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderLayoutWidth:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 88
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageResourceId:I

    .line 87
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 87
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageWidth:I

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 91
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarImageResourceId:I

    .line 90
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 90
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarWidth:I

    .line 94
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 95
    const v3, 0x7f090001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarMarginTop:I

    .line 98
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 99
    const v3, 0x7f090002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarMarginTop:I

    .line 101
    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageWidth:I

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 103
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageResourceId:I

    .line 102
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 102
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageHeight:I

    .line 104
    iget v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderFocusImageWidth:I

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 106
    iget v2, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarImageResourceId:I

    .line 105
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 105
    iput v1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderFocusImageHeight:I

    .line 108
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 109
    return-void
.end method

.method private initSliderLayout()V
    .locals 6

    .prologue
    .line 112
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderLayoutWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mNumberOfGauge:I

    add-int/lit8 v5, v5, 0x1

    int-to-float v5, v5

    div-float v3, v4, v5

    .line 113
    .local v3, "width":F
    const/4 v1, 0x1

    .local v1, "ix":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mNumberOfGauge:I

    if-le v1, v4, :cond_0

    .line 126
    return-void

    .line 114
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 115
    .local v0, "imageView":Landroid/widget/ImageView;
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageResourceId:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 116
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 118
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 119
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageWidth:I

    .line 120
    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageHeight:I

    .line 118
    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 121
    .local v2, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    int-to-float v4, v1

    mul-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 122
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarMarginTop:I

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 123
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/bcocr/widget/TwSlider;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderGuages:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCurrentSliderPosition()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    return v0
.end method

.method protected init(Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/widget/TwSlider;->initAttrs(Landroid/util/AttributeSet;)V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwSlider;->initSliderLayout()V

    .line 76
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 168
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnSliderValueChangedListener(Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;)V
    .locals 0
    .param p1, "sliderValueChangedListener"    # Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    .line 164
    return-void
.end method

.method public setSliderFocus(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 129
    if-ltz p1, :cond_0

    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mNumberOfGauge:I

    add-int/lit8 v4, v4, -0x1

    if-le p1, v4, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderGuages:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 134
    .local v0, "currentImageView":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 135
    .local v1, "currentlp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarWidth:I

    iget v6, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageWidth:I

    sub-int/2addr v5, v6

    add-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 136
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarMarginTop:I

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 137
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageWidth:I

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 138
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderImageHeight:I

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 139
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageResourceId:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 140
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    .end local v0    # "currentImageView":Landroid/widget/ImageView;
    .end local v1    # "currentlp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderGuages:Ljava/util/List;

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 145
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 146
    .local v3, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarWidth:I

    iget v6, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderBarImageWidth:I

    sub-int/2addr v5, v6

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 147
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarMarginTop:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 148
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderFocusImageHeight:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 149
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderFocusImageWidth:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 150
    iget v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderBarImageResourceId:I

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 151
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    if-eqz v4, :cond_0

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mSliderValueChangedListener:Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;

    iget v5, p0, Lcom/sec/android/app/bcocr/widget/TwSlider;->mFocusSliderPosition:I

    invoke-interface {v4, v5}, Lcom/sec/android/app/bcocr/widget/TwSlider$OnSliderValueChangedListener;->onSliderValueChanged(I)V

    goto :goto_0
.end method
