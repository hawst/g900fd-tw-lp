.class public Lcom/sec/android/app/bcocr/BitmapUtils;
.super Ljava/lang/Object;
.source "BitmapUtils.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "BitmapUtils"


# instance fields
.field private final DEBUG:Z

.field protected mBitmap:Landroid/graphics/Bitmap;

.field private mRotateBitmap:Landroid/graphics/Bitmap;

.field private mRotation:I

.field private nImage:[I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->DEBUG:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    .line 39
    iput v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotation"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->DEBUG:Z

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    .line 46
    rem-int/lit16 v0, p2, 0x168

    iput v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    .line 47
    return-void
.end method


# virtual methods
.method public convertRotateBitmapToArray()Z
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v12, 0x0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    move v0, v12

    .line 197
    :goto_0
    return v0

    .line 159
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    if-nez v0, :cond_3

    move v0, v12

    .line 178
    goto :goto_0

    .line 160
    :catch_0
    move-exception v8

    .line 161
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    const-string v0, "BitmapUtils"

    const-string v1, "[convertRotateBitmapToArray] Out of memory!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 164
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    .line 165
    const-string v0, "BitmapUtils"

    const-string v1, "[convertRotateBitmapToArray] Retry !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 166
    :catch_1
    move-exception v11

    .line 167
    .local v11, "er":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 170
    iput-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 172
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    .line 173
    invoke-static {}, Ljava/lang/System;->gc()V

    move v0, v12

    .line 174
    goto :goto_0

    .line 182
    .end local v8    # "e":Ljava/lang/OutOfMemoryError;
    .end local v11    # "er":Ljava/lang/OutOfMemoryError;
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_3

    .line 197
    const/4 v0, 0x1

    goto :goto_0

    .line 183
    :catch_2
    move-exception v9

    .line 184
    .local v9, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 185
    const-string v0, "BitmapUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[convertRotateBitmapToArray] IllegalArgumentException: x + width("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") must be <= bitmap.width():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v12

    .line 188
    goto/16 :goto_0

    .line 189
    .end local v9    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v10

    .line 190
    .local v10, "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v10}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 191
    const-string v0, "BitmapUtils"

    const-string v1, "[convertRotateBitmapToArray] Error ArrayIndexOutOfBoundsException !"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v12

    .line 192
    goto/16 :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 222
    :goto_0
    return v0

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->isOrientationChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getImage()[I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    return v0
.end method

.method public getResizedRatio()F
    .locals 4

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 206
    .local v0, "ratio":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->isOrientationChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 211
    :goto_0
    const-string v2, "BitmapUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Original width : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/Resized height : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->isOrientationChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    return v0

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    goto :goto_0

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto :goto_1
.end method

.method public getRotateBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 233
    :goto_0
    return v0

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->isOrientationChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public isOrientationChanged()Z
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    div-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 240
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 244
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    if-eqz v0, :cond_2

    .line 247
    iput-object v1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->nImage:[I

    .line 249
    :cond_2
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 250
    return-void
.end method

.method public rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "sourceBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotate"    # I

    .prologue
    .line 118
    const-string v0, "BitmapUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rotateBitmap E degree : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 123
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/4 v9, 0x0

    .line 124
    .local v9, "temp":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 126
    .local v8, "rotatedBitmap":Landroid/graphics/Bitmap;
    int-to-float v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 128
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 129
    const/4 v6, 0x1

    move-object v0, p1

    .line 128
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 130
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 131
    if-eqz v9, :cond_0

    .line 132
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    const/4 v9, 0x0

    .line 147
    :cond_0
    :goto_0
    const-string v0, "BitmapUtils"

    const-string v1, "rotateBitmap X"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-object v8

    .line 135
    :catch_0
    move-exception v7

    .line 136
    .local v7, "ex":Ljava/lang/OutOfMemoryError;
    if-eqz v8, :cond_1

    .line 137
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    const/4 v8, 0x0

    .line 140
    :cond_1
    if-eqz v9, :cond_2

    .line 141
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 142
    const/4 v9, 0x0

    .line 144
    :cond_2
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public rotateBitmap()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    move v0, v9

    .line 114
    :goto_0
    return v0

    .line 78
    :cond_0
    const-string v0, "BitmapUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rotateBitmap E mRotation : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 82
    .local v5, "matrix":Landroid/graphics/Matrix;
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 84
    iput-object v11, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 86
    :cond_1
    iget v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    if-eqz v0, :cond_5

    .line 87
    const/4 v8, 0x0

    .line 88
    .local v8, "temp":Landroid/graphics/Bitmap;
    iget v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 91
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 92
    if-eqz v8, :cond_2

    .line 93
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    const/4 v8, 0x0

    .end local v8    # "temp":Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    move v0, v10

    .line 114
    goto :goto_0

    .line 96
    .restart local v8    # "temp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 97
    .local v7, "ex":Ljava/lang/OutOfMemoryError;
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 99
    iput-object v11, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    .line 101
    :cond_3
    if-eqz v8, :cond_4

    .line 102
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 103
    const/4 v8, 0x0

    .line 105
    :cond_4
    invoke-static {}, Ljava/lang/System;->gc()V

    move v0, v9

    .line 106
    goto :goto_0

    .line 109
    .end local v7    # "ex":Ljava/lang/OutOfMemoryError;
    .end local v8    # "temp":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotateBitmap:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 63
    return-void
.end method

.method public setRotation(I)V
    .locals 0
    .param p1, "rotation"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/bcocr/BitmapUtils;->mRotation:I

    .line 51
    return-void
.end method
