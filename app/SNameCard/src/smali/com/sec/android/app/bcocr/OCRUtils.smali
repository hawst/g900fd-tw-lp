.class public Lcom/sec/android/app/bcocr/OCRUtils;
.super Ljava/lang/Object;
.source "OCRUtils.java"


# static fields
.field public static final DOWN_GOOGLEAPLAY_OCRFULLDB_PAGE:Ljava/lang/String; = "market://details?id=com.sec.android.app.ocr2.dbinstaller"

.field public static final DOWN_SAMSUNGAPPS_BCOCR_PAGE:Ljava/lang/String; = "samsungapps://ProductDetail/com.sec.android.app.bcocr"

.field public static final DOWN_SAMSUNGAPPS_OCRFULLDB_PAGE:Ljava/lang/String; = "samsungapps://ProductDetail/com.sec.android.app.ocr2.dbinstaller"

.field private static final LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_DATA_NUMBER:I = 0x3c

.field public static final OCR_FULLDBINSTALLER_ACTIVITY:Ljava/lang/String; = "com.sec.android.app.ocr2.dbinstaller.OCRDBInstaller"

.field public static final OCR_FULLDBINSTALLER_PACKAGE:Ljava/lang/String; = "com.sec.android.app.ocr2.dbinstaller"

.field public static final ORIENTATION_0:I = 0x0

.field public static final ORIENTATION_180:I = 0x2

.field public static final ORIENTATION_270:I = 0x1

.field public static final ORIENTATION_90:I = 0x3

.field public static final SAMSUNGAPPS_PACKAGE:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final SAMSUNG_FULL_TEXT:Ljava/lang/String; = "SAMSUNG (www.samsung.com)"

.field public static final SAMSUNG_TEXT:Ljava/lang/String; = "SAMSUNG"

.field public static final SAMSUNG_URL:Ljava/lang/String; = "http://www.samsung.com"

.field protected static final TAG:Ljava/lang/String; = "OCRUtils"

.field public static mLastTimeCallSharePopup:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/bcocr/OCRUtils;->mLastTimeCallSharePopup:J

    .line 320
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    .line 322
    sget-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/jpg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 323
    sget-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/jpeg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 324
    sget-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 325
    sget-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/x-ms-bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 327
    sget-object v0, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/png"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 328
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AddtoContactForQRCode(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 404
    new-instance v3, Landroid/content/Intent;

    const-string v10, "android.intent.action.INSERT"

    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 405
    .local v3, "intent":Landroid/content/Intent;
    const-string v10, "vnd.android.cursor.dir/contact"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    const/4 v5, 0x0

    .local v5, "nFoundNumber":I
    const/4 v4, 0x0

    .line 407
    .local v4, "nFoundEmail":I
    const/4 v1, 0x0

    .line 408
    .local v1, "bFoundURL":Z
    const/4 v0, 0x0

    .line 409
    .local v0, "bFoundName":Z
    invoke-static {}, Lcom/sec/barcode/QRcodeRecog;->GetQRResultType()[I

    move-result-object v7

    .line 410
    .local v7, "type":[I
    invoke-static {}, Lcom/sec/barcode/QRcodeRecog;->GetQRResultString()[Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, "text":[Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 413
    if-eqz v7, :cond_0

    .line 414
    if-eqz v6, :cond_0

    .line 415
    array-length v10, v7

    array-length v11, v6

    if-eq v10, v11, :cond_1

    .line 416
    :cond_0
    const-string v10, "OCRUtils"

    const-string v11, "add to contact : invalid param"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 470
    :goto_0
    return-object v10

    .line 420
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v10, v7

    if-lt v2, v10, :cond_2

    .line 462
    if-nez v5, :cond_d

    .line 463
    if-nez v4, :cond_d

    .line 464
    if-nez v1, :cond_d

    .line 465
    if-nez v0, :cond_d

    .line 466
    const-string v10, "OCRUtils"

    const-string v11, "add to contact :  not found a item"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_0

    .line 421
    :cond_2
    aget v10, v7, v2

    const/4 v11, 0x3

    if-ne v10, v11, :cond_7

    .line 422
    if-nez v5, :cond_5

    .line 423
    const-string v10, "phone"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 424
    const-string v10, "phone_type"

    const/4 v11, 0x3

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 432
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 420
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 425
    :cond_5
    const/4 v10, 0x1

    if-ne v5, v10, :cond_6

    .line 426
    const-string v10, "secondary_phone"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v10, "secondary_phone_type"

    const/4 v11, 0x3

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    .line 428
    :cond_6
    const/4 v10, 0x2

    if-ne v5, v10, :cond_3

    .line 429
    const-string v10, "tertiary_phone"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    const-string v10, "tertiary_phone_type"

    const/4 v11, 0x3

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    .line 433
    :cond_7
    aget v10, v7, v2

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b

    .line 434
    if-nez v4, :cond_9

    .line 435
    const-string v10, "email"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v10, "email_type"

    const/4 v11, 0x2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 444
    :cond_8
    :goto_4
    add-int/lit8 v4, v4, 0x1

    .line 445
    goto :goto_3

    .line 437
    :cond_9
    const/4 v10, 0x1

    if-ne v4, v10, :cond_a

    .line 438
    const-string v10, "secondary_email"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const-string v10, "secondary_email_type"

    const/4 v11, 0x2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_4

    .line 440
    :cond_a
    const/4 v10, 0x2

    if-ne v4, v10, :cond_8

    .line 441
    const-string v10, "tertiary_email"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    const-string v10, "tertiary_email_type"

    const/4 v11, 0x2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_4

    .line 445
    :cond_b
    aget v10, v7, v2

    const/4 v11, 0x2

    if-ne v10, v11, :cond_c

    .line 446
    if-nez v1, :cond_c

    .line 447
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 448
    .local v8, "value":Landroid/content/ContentValues;
    const-string v10, "mimetype"

    const-string v11, "vnd.android.cursor.item/website"

    invoke-virtual {v8, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v10, "data1"

    aget-object v11, v6, v2

    invoke-virtual {v8, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 452
    .local v9, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    const-string v10, "data"

    invoke-virtual {v3, v10, v9}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 454
    const/4 v1, 0x1

    .line 455
    goto/16 :goto_3

    .end local v8    # "value":Landroid/content/ContentValues;
    .end local v9    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_c
    aget v10, v7, v2

    const/4 v11, 0x4

    if-ne v10, v11, :cond_4

    .line 456
    if-nez v0, :cond_4

    .line 457
    const-string v10, "name"

    aget-object v11, v6, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 469
    :cond_d
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 470
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_0
.end method

.method public static addNewFileToDB(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 124
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    .line 125
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 124
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 126
    return-void
.end method

.method public static addNewFileToDB(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentResolver;IJ)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "contentResolver"    # Landroid/content/ContentResolver;
    .param p3, "exifOrientation"    # I
    .param p4, "dateTaken"    # J

    .prologue
    .line 129
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 130
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "orientation"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v3, "_data"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v3, "datetaken"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 133
    const-string v3, "date_modified"

    const-wide/16 v4, 0x3e8

    div-long v4, p4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 135
    const/4 v0, 0x0

    .line 138
    .local v0, "fileUri":Landroid/net/Uri;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p2, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 140
    if-eqz p0, :cond_0

    .line 141
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.camera.NEW_PICTURE"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v1

    .line 145
    .local v1, "sfe":Landroid/database/sqlite/SQLiteFullException;
    const-string v3, "OCRUtils"

    const-string v4, "Not enough space in database"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const v3, 0x7f0c003f

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static checkImageFormat(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/Boolean;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 366
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 367
    :cond_0
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : invalid param"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 399
    :goto_0
    return-object v2

    .line 371
    :cond_1
    sget-object v2, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    if-nez v2, :cond_2

    .line 372
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : Mime type is not set"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 376
    :cond_2
    const-string v2, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 377
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 379
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : ext is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 383
    :cond_3
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 388
    .end local v0    # "ext":Ljava/lang/String;
    .local v1, "mimeType":Ljava/lang/String;
    :goto_1
    if-nez v1, :cond_5

    .line 389
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : mimeType is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 385
    .end local v1    # "mimeType":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "mimeType":Ljava/lang/String;
    goto :goto_1

    .line 393
    :cond_5
    const-string v2, "OCRUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkImageFormat : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    sget-object v2, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 395
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : OK"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 398
    :cond_6
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : This format is not supported"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static checkImageFormat(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 331
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 332
    :cond_0
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : invalid param"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 359
    :goto_0
    return-object v2

    .line 336
    :cond_1
    sget-object v2, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    if-nez v2, :cond_2

    .line 337
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : Mime type is not set"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 341
    :cond_2
    const/16 v2, 0x2e

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 343
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : ext is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 347
    :cond_3
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 348
    .local v1, "mimeType":Ljava/lang/String;
    if-nez v1, :cond_4

    .line 349
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : mimeType is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 353
    :cond_4
    const-string v2, "OCRUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkImageFormat : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    sget-object v2, Lcom/sec/android/app/bcocr/OCRUtils;->LOAD_IMAGE_MIME_TYPES:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 355
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : OK"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 358
    :cond_5
    const-string v2, "OCRUtils"

    const-string v3, "checkImageFormat : This format is not supported"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method public static checkImageSize(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/Boolean;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, -0x1

    const/4 v2, 0x0

    .line 290
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 291
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    .local v10, "width":I
    const/4 v7, 0x0

    .line 292
    .local v7, "height":I
    if-eqz v0, :cond_3

    .line 293
    const/4 v6, 0x0

    .local v6, "c":Landroid/database/Cursor;
    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 294
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 295
    if-eqz v6, :cond_3

    .line 296
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 297
    const-string v1, "width"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 298
    .local v9, "indexWidth":I
    const-string v1, "height"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 300
    .local v8, "indexHeight":I
    if-eq v9, v11, :cond_0

    .line 301
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 304
    :cond_0
    if-eq v8, v11, :cond_1

    .line 305
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 307
    :cond_1
    const-string v1, "OCRUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "width and height : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    .end local v8    # "indexHeight":I
    .end local v9    # "indexWidth":I
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 313
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    const-string v1, "OCRUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "check imagesize : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-static {v10, v7}, Lcom/dmc/ocr/SecMOCR;->isSupported(II)Z

    move-result v1

    if-nez v1, :cond_4

    .line 315
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 317
    :goto_0
    return-object v1

    :cond_4
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method public static checkImageSize(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 264
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    .local v10, "width":I
    const/4 v7, 0x0

    .line 265
    .local v7, "height":I
    if-eqz v0, :cond_1

    .line 266
    const/4 v6, 0x0

    .line 267
    .local v6, "c":Landroid/database/Cursor;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 268
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_data=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    .line 267
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 270
    if-eqz v6, :cond_1

    .line 271
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "width"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 273
    .local v9, "indexWidth":I
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 274
    const-string v1, "height"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 275
    .local v8, "indexHeight":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 276
    const-string v1, "OCRUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "width and height : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v8    # "indexHeight":I
    .end local v9    # "indexWidth":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 282
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    const-string v1, "OCRUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "check imagesize : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-static {v10, v7}, Lcom/dmc/ocr/SecMOCR;->isSupported(II)Z

    move-result v1

    if-nez v1, :cond_2

    .line 284
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 286
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method public static checkNetworkConnectivity(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 596
    .line 597
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 596
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 598
    .local v0, "cManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 599
    .local v1, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 601
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 606
    :goto_0
    return v3

    .line 603
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    .line 604
    goto :goto_0

    .line 606
    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public static copyDatabase(Landroid/content/Context;)Z
    .locals 31
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 474
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS][copyDatabase()] START"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 479
    .local v4, "asset":Landroid/content/res/AssetManager;
    const/16 v21, 0x0

    .line 480
    .local v21, "nDataNum":I
    const/4 v5, 0x0

    .line 481
    .local v5, "bDataExist":Z
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f080001

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 482
    .local v9, "curr_apk_db_version":I
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v24

    .line 483
    .local v24, "preferences":Landroid/content/SharedPreferences;
    const/4 v6, 0x0

    .line 486
    .local v6, "bDataUpdate":Z
    const/16 v28, 0x3c

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    .line 488
    .local v25, "strDBname":[Ljava/lang/String;
    :try_start_0
    const-string v28, "ocrDB"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v25

    .line 489
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v21, v0

    .line 490
    const-string v28, "OCRUtils"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "[ASSETS] number of DB: "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 498
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    .line 499
    .local v26, "strDBpath":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 501
    .local v12, "file":Ljava/io/File;
    const/16 v19, 0x0

    .local v19, "ii":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v21

    if-lt v0, v1, :cond_5

    .line 518
    :goto_1
    if-eqz v5, :cond_0

    .line 521
    const-string v28, "ocr_engine_db_version"

    const/16 v29, 0x0

    .line 520
    move-object/from16 v0, v24

    move-object/from16 v1, v28

    move/from16 v2, v29

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v23

    .line 522
    .local v23, "pref_engine_db_version":I
    move/from16 v0, v23

    if-eq v9, v0, :cond_0

    .line 523
    const/4 v6, 0x1

    .line 524
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS]OCR DB version is changed"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    .end local v23    # "pref_engine_db_version":I
    :cond_0
    if-eqz v5, :cond_1

    if-eqz v6, :cond_e

    .line 530
    :cond_1
    const/16 v20, 0x0

    .line 531
    .local v20, "is":Ljava/io/InputStream;
    const/4 v15, 0x0

    .line 533
    .local v15, "fo":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    .line 534
    .local v13, "fileOut":Ljava/io/File;
    :try_start_1
    new-instance v27, Ljava/lang/String;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    sget-boolean v28, Lcom/sec/android/app/bcocr/Feature;->PRELOAD_DB:Z

    if-eqz v28, :cond_8

    const v28, 0x7f0c0010

    :goto_2
    move-object/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v30, 0x7f0c0013

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 536
    .local v27, "strPath":Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537
    .end local v13    # "fileOut":Ljava/io/File;
    .local v14, "fileOut":Ljava/io/File;
    :try_start_2
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    move-result v7

    .line 539
    .local v7, "bMake":Z
    const-string v28, "OCRUtils"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "[ASSETS] strpath: "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    const-string v28, "OCRUtils"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "[ASSETS] mkdir: "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 541
    const/4 v13, 0x0

    .line 542
    .end local v14    # "fileOut":Ljava/io/File;
    .restart local v13    # "fileOut":Ljava/io/File;
    const/16 v19, 0x0

    move-object v14, v13

    .local v14, "fileOut":Ljava/lang/Object;
    move-object/from16 v18, v15

    .end local v13    # "fileOut":Ljava/io/File;
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .local v18, "fo":Ljava/lang/Object;
    :goto_3
    move/from16 v0, v19

    move/from16 v1, v21

    if-lt v0, v1, :cond_9

    .line 563
    :try_start_3
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS] OCR DB Data have been copied!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    if-eqz v6, :cond_2

    .line 565
    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    .line 566
    .local v22, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v28, "ocr_engine_db_version"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 567
    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 575
    .end local v22    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    if-eqz v20, :cond_3

    .line 576
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 578
    :cond_3
    if-eqz v18, :cond_4

    .line 579
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 587
    .end local v7    # "bMake":Z
    .end local v14    # "fileOut":Ljava/lang/Object;
    .end local v18    # "fo":Ljava/lang/Object;
    .end local v20    # "is":Ljava/io/InputStream;
    .end local v27    # "strPath":Ljava/lang/String;
    :cond_4
    :goto_4
    const/16 v28, 0x1

    .end local v12    # "file":Ljava/io/File;
    .end local v19    # "ii":I
    .end local v26    # "strDBpath":[Ljava/lang/String;
    :goto_5
    return v28

    .line 491
    :catch_0
    move-exception v11

    .line 492
    .local v11, "e1":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    .line 493
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS] Error: Can\'t get assets\' db list!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/16 v28, 0x0

    goto :goto_5

    .line 502
    .end local v11    # "e1":Ljava/io/IOException;
    .restart local v12    # "file":Ljava/io/File;
    .restart local v19    # "ii":I
    .restart local v26    # "strDBpath":[Ljava/lang/String;
    :cond_5
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    .line 503
    sget-boolean v28, Lcom/sec/android/app/bcocr/Feature;->PRELOAD_DB:Z

    if-eqz v28, :cond_6

    const v28, 0x7f0c0010

    .line 502
    :goto_6
    move-object/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 505
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v30, 0x7f0c0013

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    .line 506
    aget-object v29, v25, v19

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 502
    aput-object v28, v26, v19

    .line 507
    new-instance v12, Ljava/io/File;

    .end local v12    # "file":Ljava/io/File;
    aget-object v28, v26, v19

    move-object/from16 v0, v28

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 508
    .restart local v12    # "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v28

    if-eqz v28, :cond_7

    .line 509
    const/4 v5, 0x1

    .line 515
    const/4 v12, 0x0

    .line 501
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 504
    :cond_6
    const v28, 0x7f0c0011

    goto :goto_6

    .line 511
    :cond_7
    const-string v28, "OCRUtils"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "[ASSETS] \'"

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v30, v26, v19

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "\' doesn\'t exist!"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const/4 v5, 0x0

    .line 513
    goto/16 :goto_1

    .line 534
    .restart local v13    # "fileOut":Ljava/io/File;
    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    .restart local v20    # "is":Ljava/io/InputStream;
    :cond_8
    const v28, 0x7f0c0011

    goto/16 :goto_2

    .line 543
    .end local v13    # "fileOut":Ljava/io/File;
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .restart local v7    # "bMake":Z
    .restart local v14    # "fileOut":Ljava/lang/Object;
    .restart local v18    # "fo":Ljava/lang/Object;
    .restart local v27    # "strPath":Ljava/lang/String;
    :cond_9
    :try_start_5
    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "ocrDB/"

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v29, v25, v19

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v20

    .line 544
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->available()I

    move-result v28

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 546
    .local v16, "fileSize":J
    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    new-array v8, v0, [B

    .line 547
    .local v8, "bufferData":[B
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    .line 548
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 550
    new-instance v13, Ljava/io/File;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v29, v25, v19

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 551
    .restart local v13    # "fileOut":Ljava/io/File;
    :try_start_6
    invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z

    .line 552
    .end local v14    # "fileOut":Ljava/lang/Object;
    new-instance v15, Ljava/io/FileOutputStream;

    invoke-direct {v15, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 553
    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    :try_start_7
    invoke-virtual {v15, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 554
    .end local v18    # "fo":Ljava/lang/Object;
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V

    .line 556
    const-string v28, "OCRUtils"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "[ASSETS] \'"

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v30, v25, v19

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    .line 557
    const-string v30, "\' has been copied!, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " bytes"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 556
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 558
    const/4 v8, 0x0

    .line 559
    const/4 v13, 0x0

    .line 560
    const/16 v20, 0x0

    .line 561
    const/4 v15, 0x0

    .line 542
    add-int/lit8 v19, v19, 0x1

    move-object v14, v13

    .restart local v14    # "fileOut":Ljava/lang/Object;
    move-object/from16 v18, v15

    .restart local v18    # "fo":Ljava/lang/Object;
    goto/16 :goto_3

    .line 569
    .end local v7    # "bMake":Z
    .end local v8    # "bufferData":[B
    .end local v14    # "fileOut":Ljava/lang/Object;
    .end local v16    # "fileSize":J
    .end local v18    # "fo":Ljava/lang/Object;
    .end local v27    # "strPath":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 570
    .local v10, "e":Ljava/io/IOException;
    :goto_7
    :try_start_8
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    .line 571
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS] Error: Can\'t copy the data file(s)!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 575
    if-eqz v20, :cond_a

    .line 576
    :try_start_9
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 578
    :cond_a
    if-eqz v15, :cond_b

    .line 579
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    .line 572
    :cond_b
    :goto_8
    const/16 v28, 0x0

    goto/16 :goto_5

    .line 573
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "fileOut":Ljava/io/File;
    :catchall_0
    move-exception v28

    .line 575
    :goto_9
    if-eqz v20, :cond_c

    .line 576
    :try_start_a
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 578
    :cond_c
    if-eqz v15, :cond_d

    .line 579
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    .line 583
    :cond_d
    :goto_a
    throw v28

    .line 585
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .end local v20    # "is":Ljava/io/InputStream;
    :cond_e
    const-string v28, "OCRUtils"

    const-string v29, "[ASSETS] OCR DB Data Exist!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 581
    .restart local v7    # "bMake":Z
    .restart local v14    # "fileOut":Ljava/lang/Object;
    .restart local v18    # "fo":Ljava/lang/Object;
    .restart local v20    # "is":Ljava/io/InputStream;
    .restart local v27    # "strPath":Ljava/lang/String;
    :catch_2
    move-exception v28

    goto/16 :goto_4

    .end local v7    # "bMake":Z
    .end local v14    # "fileOut":Ljava/lang/Object;
    .end local v18    # "fo":Ljava/lang/Object;
    .end local v27    # "strPath":Ljava/lang/String;
    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v29

    goto :goto_a

    .line 573
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .restart local v7    # "bMake":Z
    .restart local v18    # "fo":Ljava/lang/Object;
    .restart local v27    # "strPath":Ljava/lang/String;
    :catchall_1
    move-exception v28

    move-object/from16 v15, v18

    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    goto :goto_9

    .line 581
    .end local v7    # "bMake":Z
    .end local v18    # "fo":Ljava/lang/Object;
    .end local v27    # "strPath":Ljava/lang/String;
    .restart local v10    # "e":Ljava/io/IOException;
    .restart local v13    # "fileOut":Ljava/io/File;
    :catch_4
    move-exception v28

    goto :goto_8

    .line 569
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "fileOut":Ljava/io/File;
    .local v14, "fileOut":Ljava/io/File;
    .restart local v27    # "strPath":Ljava/lang/String;
    :catch_5
    move-exception v10

    move-object v13, v14

    .end local v14    # "fileOut":Ljava/io/File;
    .restart local v13    # "fileOut":Ljava/io/File;
    goto :goto_7

    .end local v13    # "fileOut":Ljava/io/File;
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .restart local v7    # "bMake":Z
    .local v14, "fileOut":Ljava/lang/Object;
    .restart local v18    # "fo":Ljava/lang/Object;
    :catch_6
    move-exception v10

    move-object v13, v14

    .restart local v13    # "fileOut":Ljava/io/File;
    move-object/from16 v15, v18

    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    goto :goto_7

    .end local v14    # "fileOut":Ljava/lang/Object;
    .end local v15    # "fo":Ljava/io/FileOutputStream;
    .restart local v8    # "bufferData":[B
    .restart local v16    # "fileSize":J
    :catch_7
    move-exception v10

    move-object/from16 v15, v18

    .restart local v15    # "fo":Ljava/io/FileOutputStream;
    goto :goto_7
.end method

.method public static getCSCCountry()Ljava/lang/String;
    .locals 3

    .prologue
    .line 173
    const-string v1, "ro.csc.country_code"

    .line 174
    const-string v2, "unknown"

    .line 173
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "country_code":Ljava/lang/String;
    const-string v1, "korea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    const-string v1, "korea"

    .line 189
    :goto_0
    return-object v1

    .line 177
    :cond_0
    const-string v1, "china"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 178
    const-string v1, "hong kong"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 179
    const-string v1, "taiwan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    :cond_1
    const-string v1, "china"

    goto :goto_0

    .line 181
    :cond_2
    const-string v1, "jp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 182
    const-string v1, "japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 183
    :cond_3
    const-string v1, "japan"

    goto :goto_0

    .line 184
    :cond_4
    const-string v1, "usa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 185
    const-string v1, "usa"

    goto :goto_0

    .line 186
    :cond_5
    const-string v1, "canada"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 187
    const-string v1, "usa"

    goto :goto_0

    .line 189
    :cond_6
    const-string v1, "global"

    goto :goto_0
.end method

.method public static getFileNameFromFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 115
    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 116
    .local v2, "startidx":I
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 117
    .local v0, "endidx":I
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "fileName":Ljava/lang/String;
    const-string v3, "OCRUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "original file name is : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-object v1
.end method

.method public static isPackageExist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 154
    if-nez p0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v4

    .line 158
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x2000

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    .line 159
    .local v1, "installedList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 160
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 162
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 163
    .local v3, "tmp":Landroid/content/pm/PackageInfo;
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 164
    const/4 v4, 0x1

    goto :goto_0

    .line 160
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static isSamsungAppsExist(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 63
    if-nez p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v2

    .line 67
    :cond_1
    const/4 v1, 0x0

    .line 69
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 70
    const-string v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x0

    .line 69
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 71
    if-eqz v1, :cond_0

    .line 72
    const/4 v2, 0x1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "OCRUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SamsungApps is not exist. ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSaveDetectedTextMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 194
    if-nez p0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v2

    .line 198
    :cond_1
    const v3, 0x7f0c0015

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "saveDetectedTextFullPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 201
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static onDownloadPhotoReader(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "market://details?id=com.sec.android.app.bcocr"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 111
    const/4 v1, 0x1

    return v1
.end method

.method public static onLaunchBCOCRDownload(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v6, 0x10000000

    .line 84
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRUtils;->isSamsungAppsExist(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    const-string v4, "samsungapps://ProductDetail/com.sec.android.app.bcocr"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 90
    .local v3, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 91
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 93
    :try_start_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v4, "OCRUtils"

    const-string v5, "[OCR] SamsungApps Activity not found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "OCRUtils"

    const-string v5, "[OCR] Browser Activity not found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 97
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    const-string v4, "com.android.browser"

    const-string v5, "com.android.browser.BrowserActivity"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    :try_start_1
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 101
    :catch_1
    move-exception v1

    .line 102
    .local v1, "e2":Landroid/content/ActivityNotFoundException;
    const-string v4, "OCRUtils"

    const-string v5, "[OCR] Browser Activity not found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static showSharePopup(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareText"    # Ljava/lang/String;
    .param p2, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 231
    .local v0, "currentTime":J
    sget-wide v4, Lcom/sec/android/app/bcocr/OCRUtils;->mLastTimeCallSharePopup:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    sget-wide v4, Lcom/sec/android/app/bcocr/OCRUtils;->mLastTimeCallSharePopup:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 232
    sput-wide v0, Lcom/sec/android/app/bcocr/OCRUtils;->mLastTimeCallSharePopup:J

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    sput-wide v0, Lcom/sec/android/app/bcocr/OCRUtils;->mLastTimeCallSharePopup:J

    .line 236
    if-eqz p0, :cond_0

    .line 240
    if-nez p1, :cond_2

    if-eqz p2, :cond_0

    .line 244
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 245
    .local v2, "sendIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 246
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    if-eqz p1, :cond_3

    .line 248
    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    :cond_3
    if-eqz p2, :cond_4

    .line 252
    const-string v4, "image/*"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 254
    .local v3, "uri":Landroid/net/Uri;
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 259
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_1
    const v4, 0x7f0c004e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 257
    :cond_4
    const-string v4, "text/plain"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static showShareTextPopup(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareText"    # Ljava/lang/String;

    .prologue
    .line 208
    if-nez p0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 212
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "sendIntent":Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 221
    const v1, 0x7f0c004e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
