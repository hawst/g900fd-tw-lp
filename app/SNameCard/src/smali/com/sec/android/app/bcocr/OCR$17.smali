.class Lcom/sec/android/app/bcocr/OCR$17;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$17;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 4719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 4722
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 4723
    const/16 v1, 0x52

    if-eq p2, v1, :cond_0

    const/16 v1, 0x17

    if-ne p2, v1, :cond_1

    .line 4724
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$17;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput-boolean v0, v1, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 4733
    :goto_0
    return v0

    .line 4726
    :cond_1
    const/16 v1, 0x42

    if-ne p2, v1, :cond_2

    .line 4727
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$17;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput-boolean v0, v1, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 4728
    const/4 v0, 0x0

    goto :goto_0

    .line 4729
    :cond_2
    const/4 v1, 0x4

    if-ne p2, v1, :cond_3

    .line 4730
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$17;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto :goto_0

    .line 4733
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$17;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/Window;->superDispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
