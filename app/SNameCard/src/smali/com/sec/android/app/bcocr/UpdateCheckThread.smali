.class public Lcom/sec/android/app/bcocr/UpdateCheckThread;
.super Ljava/lang/Thread;
.source "UpdateCheckThread.java"


# static fields
.field public static final APPS_STATUS_CMD_CONNECTING_SERVER:I = 0xa

.field public static final APPS_STATUS_ERROR:I = 0x3

.field public static final APPS_STATUS_ERROR_NETWORK:I = 0x4

.field public static final APPS_STATUS_ERROR_NOTFOUND_APP:I = 0x0

.field public static final APPS_STATUS_UPDATE_AVAILABLE:I = 0x2

.field public static final APPS_STATUS_UPDATE_NOTNEED:I = 0x1

.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final PD_TEST_PATH:Ljava/lang/String; = "/sdcard/go_to_andromeda.test"

.field protected static final SERVER_URL_CHECK:Ljava/lang/String; = "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

.field protected static final SERVER_URL_DOWNLOAD:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field private static final TAG:Ljava/lang/String; = "UpdateCheckThread"


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field protected mPackageName:Ljava/lang/String;

.field protected mVersionCode:I

.field private volatile running:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->running:Z

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method private checkUpdate(Ljava/lang/String;)I
    .locals 14
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 154
    const/4 v8, 0x3

    .line 157
    .local v8, "resultValue":I
    const/4 v0, 0x0

    .line 159
    .local v0, "Url":Ljava/net/URL;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 160
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 161
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4

    .line 163
    .end local v0    # "Url":Ljava/net/URL;
    .local v1, "Url":Ljava/net/URL;
    :try_start_1
    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v11

    const/4 v12, 0x0

    invoke-interface {v5, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 165
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 167
    .local v6, "parserEvent":I
    const-string v4, ""

    .local v4, "id":Ljava/lang/String;
    const-string v7, ""

    .line 168
    .local v7, "result":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 169
    new-instance v11, Ljava/lang/InterruptedException;

    invoke-direct {v11}, Ljava/lang/InterruptedException;-><init>()V

    throw v11
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_5

    .line 211
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "parserEvent":I
    .end local v7    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 212
    .end local v1    # "Url":Ljava/net/URL;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v0    # "Url":Ljava/net/URL;
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_0
    const-string v11, "UpdateCheckThread"

    const-string v12, "xml parsing error"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 229
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_1
    const/4 v0, 0x0

    .line 230
    return v8

    .line 173
    .end local v0    # "Url":Ljava/net/URL;
    .restart local v1    # "Url":Ljava/net/URL;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6    # "parserEvent":I
    .restart local v7    # "result":Ljava/lang/String;
    :cond_0
    const/4 v11, 0x2

    if-ne v6, v11, :cond_2

    .line 174
    :try_start_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    .line 175
    .local v9, "tag":Ljava/lang/String;
    const-string v11, "appId"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 176
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v10

    .line 177
    .local v10, "type":I
    const/4 v11, 0x4

    if-ne v10, v11, :cond_1

    .line 178
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v4

    .line 181
    .end local v10    # "type":I
    :cond_1
    const-string v11, "resultCode"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 182
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v10

    .line 183
    .restart local v10    # "type":I
    const/4 v11, 0x4

    if-ne v10, v11, :cond_2

    .line 184
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 188
    .end local v9    # "tag":Ljava/lang/String;
    .end local v10    # "type":I
    :cond_2
    const/4 v11, 0x3

    if-ne v6, v11, :cond_3

    .line 189
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    .line 190
    .restart local v9    # "tag":Ljava/lang/String;
    const-string v11, "resultCode"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 191
    iget-object v11, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 192
    const-string v11, "UpdateCheckThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown packageName : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :goto_2
    const-string v4, ""

    .line 203
    const-string v7, ""

    .line 206
    .end local v9    # "tag":Ljava/lang/String;
    :cond_3
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 172
    :cond_4
    const/4 v11, 0x1

    if-eq v6, v11, :cond_5

    iget-boolean v11, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->running:Z

    if-nez v11, :cond_0

    .line 208
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 209
    new-instance v11, Ljava/lang/InterruptedException;

    invoke-direct {v11}, Ljava/lang/InterruptedException;-><init>()V

    throw v11
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5

    .line 214
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "parserEvent":I
    .end local v7    # "result":Ljava/lang/String;
    :catch_1
    move-exception v2

    move-object v0, v1

    .line 215
    .end local v1    # "Url":Ljava/net/URL;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v0    # "Url":Ljava/net/URL;
    .local v2, "e":Ljava/net/SocketException;
    :goto_3
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 216
    const-string v11, "UpdateCheckThread"

    const-string v12, "network is unavailable"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 193
    .end local v0    # "Url":Ljava/net/URL;
    .end local v2    # "e":Ljava/net/SocketException;
    .restart local v1    # "Url":Ljava/net/URL;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6    # "parserEvent":I
    .restart local v7    # "result":Ljava/lang/String;
    .restart local v9    # "tag":Ljava/lang/String;
    :cond_6
    :try_start_3
    const-string v11, "2"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 194
    const/4 v8, 0x2

    .line 195
    goto :goto_2

    :cond_7
    const-string v11, "0"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 196
    const/4 v8, 0x0

    .line 197
    goto :goto_2

    :cond_8
    const-string v11, "1"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 198
    const/4 v8, 0x1

    .line 199
    goto :goto_2

    .line 200
    :cond_9
    const-string v11, "UpdateCheckThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown result code : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_2

    .line 217
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "parserEvent":I
    .end local v7    # "result":Ljava/lang/String;
    .end local v9    # "tag":Ljava/lang/String;
    :catch_2
    move-exception v2

    move-object v0, v1

    .line 218
    .end local v1    # "Url":Ljava/net/URL;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v0    # "Url":Ljava/net/URL;
    .local v2, "e":Ljava/net/UnknownHostException;
    :goto_4
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 219
    const-string v11, "UpdateCheckThread"

    const-string v12, "server is not response"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 220
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v2

    .line 221
    .local v2, "e":Ljava/io/IOException;
    :goto_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 222
    const-string v11, "UpdateCheckThread"

    const-string v12, "network error"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 223
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 224
    .local v2, "e":Ljava/lang/InterruptedException;
    :goto_6
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 225
    const-string v11, "UpdateCheckThread"

    const-string v12, "checkUpdate is interrupted"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 223
    .end local v0    # "Url":Ljava/net/URL;
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "Url":Ljava/net/URL;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "Url":Ljava/net/URL;
    .restart local v0    # "Url":Ljava/net/URL;
    goto :goto_6

    .line 220
    .end local v0    # "Url":Ljava/net/URL;
    .restart local v1    # "Url":Ljava/net/URL;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "Url":Ljava/net/URL;
    .restart local v0    # "Url":Ljava/net/URL;
    goto :goto_5

    .line 217
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_7
    move-exception v2

    goto :goto_4

    .line 214
    :catch_8
    move-exception v2

    goto :goto_3

    .line 211
    :catch_9
    move-exception v2

    goto/16 :goto_0

    .end local v0    # "Url":Ljava/net/URL;
    .restart local v1    # "Url":Ljava/net/URL;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6    # "parserEvent":I
    .restart local v7    # "result":Ljava/lang/String;
    :cond_a
    move-object v0, v1

    .end local v1    # "Url":Ljava/net/URL;
    .restart local v0    # "Url":Ljava/net/URL;
    goto/16 :goto_1
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 7

    .prologue
    .line 290
    const/4 v4, 0x0

    .line 291
    .local v4, "s":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    const-string v6, "/system/csc/sales_code.dat"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    .local v3, "mFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 293
    const/16 v6, 0x14

    new-array v0, v6, [B

    .line 294
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 297
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    .end local v1    # "in":Ljava/io/InputStream;
    .local v2, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-eqz v6, :cond_1

    .line 300
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    .end local v4    # "s":Ljava/lang/String;
    .local v5, "s":Ljava/lang/String;
    move-object v4, v5

    .line 305
    .end local v5    # "s":Ljava/lang/String;
    .restart local v4    # "s":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 313
    .end local v0    # "buffer":[B
    .end local v2    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v4

    .line 302
    .restart local v0    # "buffer":[B
    .restart local v2    # "in":Ljava/io/InputStream;
    :cond_1
    new-instance v5, Ljava/lang/String;

    const-string v6, "FAIL"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v4    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    move-object v4, v5

    .end local v5    # "s":Ljava/lang/String;
    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 308
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v6

    goto :goto_1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    goto :goto_1

    .line 306
    :catch_2
    move-exception v6

    goto :goto_1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_3
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 4

    .prologue
    .line 317
    const/4 v1, 0x0

    .line 318
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 322
    if-nez v1, :cond_0

    .line 323
    const-string v2, "UpdateCheckThread"

    const-string v3, "CSC is not exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_0
    :goto_0
    return v1

    .line 325
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->running:Z

    .line 65
    return-void
.end method

.method protected getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 263
    const-string v0, ""

    .line 264
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 267
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->isCSCExistFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    :goto_0
    return-object v0

    .line 271
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 272
    if-nez v1, :cond_1

    .line 273
    const-string v2, "UpdateCheckThread"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 278
    const-string v2, "UpdateCheckThread"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getIMEI()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 333
    const-string v2, ""

    .line 334
    .local v2, "imei":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 335
    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 334
    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 336
    .local v4, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 338
    .local v3, "md5":Ljava/lang/StringBuffer;
    if-eqz v4, :cond_0

    .line 342
    const/16 v5, 0xc

    :try_start_0
    new-array v0, v5, [B

    const/4 v5, 0x0

    .line 343
    const/4 v6, 0x1

    aput-byte v6, v0, v5

    const/4 v5, 0x1

    const/4 v6, 0x2

    aput-byte v6, v0, v5

    const/4 v5, 0x2

    const/4 v6, 0x3

    aput-byte v6, v0, v5

    const/4 v5, 0x3

    const/4 v6, 0x4

    aput-byte v6, v0, v5

    const/4 v5, 0x4

    const/4 v6, 0x5

    aput-byte v6, v0, v5

    const/4 v5, 0x5

    const/4 v6, 0x6

    aput-byte v6, v0, v5

    const/4 v5, 0x6

    const/4 v6, 0x7

    aput-byte v6, v0, v5

    const/4 v5, 0x7

    const/16 v6, 0x8

    aput-byte v6, v0, v5

    const/16 v5, 0x8

    const/16 v6, 0x9

    aput-byte v6, v0, v5

    const/16 v5, 0xa

    const/4 v6, 0x1

    aput-byte v6, v0, v5

    const/16 v5, 0xb

    const/4 v6, 0x2

    aput-byte v6, v0, v5

    .line 346
    .local v0, "digest":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v1, v5, :cond_1

    .line 354
    .end local v0    # "digest":[B
    .end local v1    # "i":I
    :cond_0
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v5, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 355
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 347
    .restart local v0    # "digest":[B
    .restart local v1    # "i":I
    :cond_1
    :try_start_1
    aget-byte v5, v0, v1

    and-int/lit16 v5, v5, 0xf0

    shr-int/lit8 v5, v5, 0x4

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 348
    aget-byte v5, v0, v1

    and-int/lit8 v5, v5, 0xf

    shr-int/lit8 v5, v5, 0x0

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 346
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 350
    .end local v0    # "digest":[B
    .end local v1    # "i":I
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method protected getMCC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 234
    const-string v0, ""

    .line 235
    .local v0, "mcc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 236
    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 235
    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 238
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 239
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 242
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 245
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected getMNC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 249
    const-string v0, "00"

    .line 250
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 251
    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 250
    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 253
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 254
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 256
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 259
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected getPD()Ljava/lang/String;
    .locals 4

    .prologue
    .line 359
    const-string v2, "0"

    .line 360
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 362
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 365
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 366
    if-eqz v1, :cond_0

    .line 367
    const-string v2, "1"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :cond_0
    :goto_0
    return-object v2

    .line 369
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->running:Z

    return v0
.end method

.method public run()V
    .locals 15

    .prologue
    .line 69
    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 70
    .local v8, "szModel":Ljava/lang/String;
    const-string v9, "SAMSUNG-"

    .line 71
    .local v9, "szPrefix":Ljava/lang/String;
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->running:Z

    .line 73
    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 74
    const-string v12, ""

    invoke-virtual {v8, v9, v12}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 78
    :cond_0
    const-string v6, ""

    .line 79
    .local v6, "szMCC":Ljava/lang/String;
    const-string v7, ""

    .line 85
    .local v7, "szMNC":Ljava/lang/String;
    const/4 v10, 0x0

    .line 86
    .local v10, "testMode":Z
    new-instance v2, Ljava/io/File;

    const-string v12, "/sdcard/go_to_andromeda.test"

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_1

    .line 89
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    .line 91
    :cond_1
    const/4 v2, 0x0

    .line 93
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 94
    const-string v13, "connectivity"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 93
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 95
    .local v0, "cManager":Landroid/net/ConnectivityManager;
    const/4 v12, 0x0

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 96
    .local v3, "mobile":Landroid/net/NetworkInfo;
    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v11

    .line 98
    .local v11, "wifi":Landroid/net/NetworkInfo;
    if-eqz v11, :cond_5

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 99
    if-eqz v10, :cond_4

    .line 100
    const-string v6, "000"

    .line 104
    :goto_0
    const-string v7, "00"

    .line 120
    :goto_1
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    if-eqz v12, :cond_2

    .line 121
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    const/16 v13, 0xa

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 125
    :cond_2
    const/4 v12, 0x0

    iput v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mVersionCode:I

    .line 127
    :try_start_0
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    .line 128
    iget-object v13, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    const/16 v14, 0x80

    .line 127
    invoke-virtual {v12, v13, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v12

    .line 128
    iget v12, v12, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 127
    iput v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mVersionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_2
    const-string v5, "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

    .line 134
    .local v5, "server_url":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "?appId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 135
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&versionCode="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mVersionCode:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 136
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&deviceId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 137
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&mcc="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 138
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&mnc="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 139
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&csc="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 140
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&sdkVer="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 141
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&pd="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 144
    const-string v12, "UpdateCheckThread"

    const-string v13, "checkUpdate++"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->checkUpdate(Ljava/lang/String;)I

    move-result v4

    .line 146
    .local v4, "result":I
    const-string v12, "UpdateCheckThread"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "checkUpdate-- : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    if-eqz v12, :cond_3

    .line 149
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v12, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 151
    .end local v4    # "result":I
    .end local v5    # "server_url":Ljava/lang/String;
    :cond_3
    :goto_3
    return-void

    .line 102
    :cond_4
    const-string v6, "505"

    goto/16 :goto_0

    .line 105
    :cond_5
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 106
    if-eqz v10, :cond_6

    .line 107
    const-string v6, "000"

    .line 111
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->getMNC()Ljava/lang/String;

    move-result-object v7

    .line 112
    goto/16 :goto_1

    .line 109
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->getMCC()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 113
    :cond_7
    const-string v12, "UpdateCheckThread"

    const-string v13, "Connection failed"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    if-eqz v12, :cond_3

    .line 115
    iget-object v12, p0, Lcom/sec/android/app/bcocr/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    .line 129
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    const-string v12, "UpdateCheckThread"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "get VersionCode : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
