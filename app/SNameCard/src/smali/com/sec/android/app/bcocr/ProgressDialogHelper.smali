.class public Lcom/sec/android/app/bcocr/ProgressDialogHelper;
.super Ljava/lang/Object;
.source "ProgressDialogHelper.java"


# instance fields
.field private final DEBUG:Z

.field private final DEFALT_PROGRESS_STYLE:I

.field public final STATE_END:I

.field public final STATE_INIT:I

.field public final STATE_PAUSE:I

.field public final STATE_PROGRESSED:I

.field public final STATE_PROGRESSING:I

.field public final STATE_START:I

.field public final STYLE_PROGRESS_BAR:I

.field public final STYLE_PROGRESS_CIRCLE:I

.field public final STYLE_PROGRESS_DEFAULT:I

.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mProgress:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressState:I

.field private mProgressStyle:I

.field private progChangedListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

.field private progDiaCancelListener:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "ProgressDialogHelper"

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->TAG:Ljava/lang/String;

    .line 14
    iput-object v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    .line 17
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->DEFALT_PROGRESS_STYLE:I

    .line 18
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_DEFAULT:I

    .line 19
    iput v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_BAR:I

    .line 20
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_CIRCLE:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_INIT:I

    .line 24
    iput v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_START:I

    .line 25
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PROGRESSING:I

    .line 26
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PROGRESSED:I

    .line 27
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PAUSE:I

    .line 28
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_END:I

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->DEBUG:Z

    .line 33
    iput-object v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 34
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    .line 37
    new-instance v0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;-><init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progChangedListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .line 59
    new-instance v0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper$2;-><init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progDiaCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onCreate()V

    .line 72
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->setProgressStyle(I)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "ProgressDialogHelper"

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->TAG:Ljava/lang/String;

    .line 14
    iput-object v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    .line 17
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->DEFALT_PROGRESS_STYLE:I

    .line 18
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_DEFAULT:I

    .line 19
    iput v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_BAR:I

    .line 20
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STYLE_PROGRESS_CIRCLE:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_INIT:I

    .line 24
    iput v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_START:I

    .line 25
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PROGRESSING:I

    .line 26
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PROGRESSED:I

    .line 27
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_PAUSE:I

    .line 28
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->STATE_END:I

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->DEBUG:Z

    .line 33
    iput-object v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 34
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    .line 37
    new-instance v0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper$1;-><init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progChangedListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .line 59
    new-instance v0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper$2;-><init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progDiaCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onCreate()V

    .line 78
    invoke-virtual {p0, p2}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->setProgressStyle(I)V

    .line 79
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/ProgressDialogHelper;I)V
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/bcocr/ProgressDialogHelper;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    return v0
.end method

.method private onInitialize()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 109
    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progDiaCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progChangedListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->registerProgressChangedListener(Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;)V

    .line 118
    return-void
.end method

.method private onPrepare()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 122
    iget v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    if-ne v0, v2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_0
.end method


# virtual methods
.method public isProgressing()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v2, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v0

    .line 260
    :cond_1
    const-string v2, "ProgressDialogHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isProgressing mProgressState:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    if-eq v2, v1, :cond_2

    iget v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    .line 262
    iget v2, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 263
    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 253
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/sec/android/app/bcocr/ProgressDialogHelper$3;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper$3;-><init>(Lcom/sec/android/app/bcocr/ProgressDialogHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onInitialize()V

    .line 106
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "ProgressDialogHelper"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->unregisterProgressChangedListener()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 140
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 142
    :cond_1
    return-void
.end method

.method public onHide()V
    .locals 3

    .prologue
    .line 238
    const-string v1, "ProgressDialogHelper"

    const-string v2, "hide"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->hide()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 184
    const-string v1, "ProgressDialogHelper"

    const-string v2, "onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 190
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 192
    :cond_0
    const/4 v1, 0x4

    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 200
    const-string v1, "ProgressDialogHelper"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->isProgressing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_2

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onCreate()V

    .line 212
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onPrepare()V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 215
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onShow()V
    .locals 4

    .prologue
    .line 225
    const-string v1, "ProgressDialogHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "show mProgress:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgress:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 146
    const-string v1, "ProgressDialogHelper"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onCreate()V

    .line 154
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onPrepare()V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 157
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->progChangedListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    invoke-static {v1}, Lcom/dmc/ocr/SecMOCR;->registerProgressChangedListener(Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;)V

    .line 163
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 167
    const-string v1, "ProgressDialogHelper"

    const-string v2, "onStop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->unregisterProgressChangedListener()V

    .line 172
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 174
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 176
    :cond_0
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgressStyle(I)V
    .locals 5
    .param p1, "style"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 83
    const-string v0, "ProgressDialogHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setProgressStyle "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    if-ne p1, v4, :cond_0

    .line 86
    iput v4, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 88
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    goto :goto_0

    .line 90
    :cond_1
    iput v3, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressStyle:I

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 272
    :cond_0
    return-void
.end method
