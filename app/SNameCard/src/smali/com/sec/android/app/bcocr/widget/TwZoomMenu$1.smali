.class Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;
.super Landroid/os/Handler;
.source "TwZoomMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/widget/TwZoomMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    .line 45
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$0(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$0(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 55
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$0(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    goto :goto_0

    .line 65
    :catch_0
    move-exception v1

    goto :goto_0

    .line 58
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mIsRepeatClickAvailable:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$1(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    .line 60
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->onClick(Landroid/view/View;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$2(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/widget/TwZoomMenu$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwZoomMenu;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/widget/TwZoomMenu;->access$2(Lcom/sec/android/app/bcocr/widget/TwZoomMenu;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
