.class final Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;)V
    .locals 0

    .prologue
    .line 613
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ILcom/sec/android/seccamera/SecCamera;)V
    .locals 9
    .param p1, "afMsg"    # I
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 616
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AutoFocusCallback.onAutoFocus : msg["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] focusState["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-Autofocus**EndU["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    if-ne v0, v8, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->closeCamera()V

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    if-ne p1, v7, :cond_2

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iput v6, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    goto :goto_0

    .line 635
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 636
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iput v8, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    goto :goto_0

    .line 640
    :cond_3
    if-ne p1, v6, :cond_9

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getAdvancedMacroFocusActive()Z

    move-result v0

    if-nez v0, :cond_4

    .line 642
    const-string v0, "OCREngine"

    const-string v1, "[AutoCapture] AutoFocusCallback PlaySound "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0, v6, v5}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->playCameraSound(II)V

    .line 646
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getTouchAutoFocusActive()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isShutterPressed()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$2(Lcom/sec/android/app/bcocr/OCREngine;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->unlockAEAWB()V

    .line 650
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    if-eq v0, v4, :cond_6

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iput v4, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 655
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mIsEdgeDetected:Z

    if-eqz v0, :cond_7

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iput-boolean v5, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mIsEdgeDetected:Z

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->fitEdgeCueForNameCard()V

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/16 v1, 0x1b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/16 v1, 0x1b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 685
    :cond_7
    :goto_1
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getTouchAutoFocusActive()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isShutterPressed()Z

    move-result v0

    if-nez v0, :cond_8

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->isTouchDown()Z

    move-result v0

    if-nez v0, :cond_8

    .line 687
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v0, :cond_8

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v6, v2, v3}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 693
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 661
    :cond_9
    if-ne p1, v4, :cond_a

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v1, 0x4

    iput v1, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    goto :goto_1

    .line 663
    :cond_a
    if-nez p1, :cond_7

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mIsEdgeDetected:Z

    if-eqz v0, :cond_b

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iput-boolean v5, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mIsEdgeDetected:Z

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mLayout:Landroid/widget/RelativeLayout;

    .line 671
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getAdvancedMacroFocusActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->playCameraSound(II)V

    .line 675
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getTouchAutoFocusActive()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isShutterPressed()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$2(Lcom/sec/android/app/bcocr/OCREngine;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->unlockAEAWB()V

    .line 679
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    if-eq v0, v7, :cond_7

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iput v7, v0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    goto/16 :goto_1
.end method
