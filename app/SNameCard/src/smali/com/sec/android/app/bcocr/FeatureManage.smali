.class public Lcom/sec/android/app/bcocr/FeatureManage;
.super Ljava/lang/Object;
.source "FeatureManage.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "FeatureManager"

.field public static bFeatureInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bcocr/FeatureManage;->bFeatureInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initFeature(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    sget-boolean v0, Lcom/sec/android/app/bcocr/FeatureManage;->bFeatureInitialized:Z

    if-eqz v0, :cond_0

    .line 23
    :goto_0
    return-void

    .line 17
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/bcocr/FeatureManage;->setBuildFeature(Landroid/content/Context;)V

    .line 18
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProjectFeature()V

    .line 19
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setCSCFeature()V

    .line 21
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/bcocr/FeatureManage;->bFeatureInitialized:Z

    .line 22
    const-string v0, "FeatureManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OCR Feature Name : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isChinaFeature()Z
    .locals 2

    .prologue
    .line 38
    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "CountryCode":Ljava/lang/String;
    const-string v1, "China"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "china"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    const-string v1, "CHINA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    :cond_0
    const/4 v1, 0x1

    .line 43
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static setBuildFeature(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->PRELOAD_DB:Z

    .line 35
    return-void
.end method

.method private static setCSCFeature()V
    .locals 3

    .prologue
    .line 26
    const-string v1, "ro.csc.sales_code"

    const-string v2, "unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "sale_code":Ljava/lang/String;
    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_SUPPORT:Z

    .line 31
    :cond_0
    return-void
.end method

.method private static setProjectFeature()V
    .locals 5

    .prologue
    .line 47
    const-string v2, "ro.product.device"

    const-string v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "product_name":Ljava/lang/String;
    const-string v2, "ro.csc.country_code"

    const-string v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "country_code":Ljava/lang/String;
    const-string v2, "FeatureManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setProjectFeature product_name : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "jp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    const-string v2, "ro.build.description"

    const-string v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ha3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 58
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "flte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 59
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "htdlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 60
    :cond_1
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_H()V

    .line 112
    :goto_0
    return-void

    .line 61
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hllte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 62
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hl3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 63
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "frescolte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 64
    :cond_3
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_FRESCO()V

    goto :goto_0

    .line 65
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "montblanc3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 66
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_MONTBLANC()V

    goto :goto_0

    .line 67
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "k3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 68
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "klte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 69
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kalte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 70
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kwifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 71
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kccat6"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 72
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kcat6"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 73
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kelte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 74
    :cond_6
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_K()V

    goto/16 :goto_0

    .line 75
    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kqlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 76
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "lentislte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 77
    :cond_8
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_KQ()V

    goto/16 :goto_0

    .line 78
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "pateklte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 79
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_PATEK()V

    goto/16 :goto_0

    .line 80
    :cond_a
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tblte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 81
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tbhplte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 82
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tr3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 83
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "trlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 84
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "trhplte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 85
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "trelte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 86
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "trhlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 87
    :cond_b
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_T()V

    goto/16 :goto_0

    .line 88
    :cond_c
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kminilte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 89
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kmini3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 90
    :cond_d
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_KMINI()V

    goto/16 :goto_0

    .line 91
    :cond_e
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chagalllte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 92
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chagallwifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 93
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chagall3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 94
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chagallhlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 95
    :cond_f
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_CHAGALL()V

    goto/16 :goto_0

    .line 96
    :cond_10
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "klimtlte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 97
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "klimtwifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 98
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "klimt3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 99
    :cond_11
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_KLIMT()V

    goto/16 :goto_0

    .line 100
    :cond_12
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mega2lte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 101
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mega23g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 102
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "vasta3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 103
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "vastalte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 104
    :cond_13
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_MEGA2()V

    goto/16 :goto_0

    .line 105
    :cond_14
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "slte"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 106
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "s3g"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 107
    :cond_15
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_BERLUTIMAX()V

    goto/16 :goto_0

    .line 109
    :cond_16
    const-string v2, "FeatureManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown product : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_UnKnown()V

    goto/16 :goto_0
.end method

.method private static setProject_BERLUTIMAX()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 268
    const-string v0, "1280x720"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 269
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 270
    const-string v0, "5312x2988"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 272
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 273
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 274
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 275
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 278
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 279
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 280
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 282
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    .line 283
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    .line 285
    const-string v0, "BERLUTIMAX"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 286
    return-void
.end method

.method private static setProject_CHAGALL()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 220
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 221
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 222
    const-string v0, "3264x2448"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 224
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 226
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 227
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    .line 228
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    .line 229
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 232
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 233
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 234
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 236
    const-string v0, "CHAGALL"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 237
    return-void
.end method

.method private static setProject_FRESCO()V
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_H()V

    .line 140
    const-string v0, "1280x720"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 142
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 143
    const-string v0, "FRESCO"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 144
    return-void
.end method

.method private static setProject_H()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 116
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 117
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 118
    const-string v0, "4128x3096"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 119
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 120
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 121
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 122
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_SAVING_OPTION:Z

    .line 123
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_SUPPORT:Z

    .line 125
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    .line 128
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 129
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 130
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 132
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    .line 133
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    .line 135
    const-string v0, "H"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 136
    return-void
.end method

.method private static setProject_K()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 148
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 149
    const-string v0, "5312x2988"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 151
    sput-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 152
    sput-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 153
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 154
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 155
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    .line 158
    const-string v0, "ro.product.device"

    const-string v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "klteaio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const-string v0, "ro.product.device"

    const-string v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "klteattactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 161
    sput-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 162
    const-string v0, "KAIO"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 168
    :goto_0
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 170
    sput v4, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    .line 171
    sput v4, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    .line 172
    return-void

    .line 164
    :cond_1
    sput-boolean v3, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 165
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 166
    const-string v0, "K"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    goto :goto_0
.end method

.method private static setProject_KLIMT()V
    .locals 1

    .prologue
    .line 240
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_CHAGALL()V

    .line 241
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    .line 243
    const-string v0, "KLIMT"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 244
    return-void
.end method

.method private static setProject_KMINI()V
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_K()V

    .line 197
    const-string v0, "1280x720"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 198
    const-string v0, "3264x2448"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 199
    return-void
.end method

.method private static setProject_KQ()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 175
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_K()V

    .line 176
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    .line 178
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 179
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 180
    return-void
.end method

.method private static setProject_MEGA2()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 247
    const-string v0, "1280x720"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 248
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 249
    const-string v0, "4128x3096"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 251
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 252
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 253
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 254
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 257
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 258
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 259
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 261
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    .line 262
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    .line 264
    const-string v0, "MEGA2"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 265
    return-void
.end method

.method private static setProject_MONTBLANC()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    const-string v0, "800x480"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 204
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 205
    const-string v0, "4128x3096"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 207
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 210
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 212
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 215
    const-string v0, "MONTBLANC"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 216
    return-void
.end method

.method private static setProject_PATEK()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 183
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->setProject_K()V

    .line 185
    const-string v0, "1280x720"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 187
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    .line 188
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 189
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 191
    const-string v0, "PATEK"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 192
    return-void
.end method

.method private static setProject_T()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 289
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 290
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 291
    const-string v0, "5312x2988"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 293
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 294
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 295
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    .line 296
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 298
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    .line 302
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 303
    sput-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 304
    sput-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 306
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    .line 307
    sput v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    .line 308
    const-string v0, "T"

    sput-object v0, Lcom/sec/android/app/bcocr/Feature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 309
    return-void
.end method

.method private static setProject_UnKnown()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method
