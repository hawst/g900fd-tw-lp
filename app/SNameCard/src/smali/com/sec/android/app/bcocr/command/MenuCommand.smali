.class public abstract Lcom/sec/android/app/bcocr/command/MenuCommand;
.super Ljava/lang/Object;
.source "MenuCommand.java"


# instance fields
.field protected mLaunchX:F

.field protected mLaunchY:F

.field protected mZOrder:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/bcocr/command/MenuCommand;->mZOrder:I

    .line 23
    return-void
.end method


# virtual methods
.method public abstract execute()V
.end method

.method public setLaunchPosition(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/bcocr/command/MenuCommand;->mLaunchX:F

    .line 32
    iput p2, p0, Lcom/sec/android/app/bcocr/command/MenuCommand;->mLaunchY:F

    .line 33
    return-void
.end method

.method public setZOrder(I)V
    .locals 0
    .param p1, "zOrder"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/bcocr/command/MenuCommand;->mZOrder:I

    .line 37
    return-void
.end method
