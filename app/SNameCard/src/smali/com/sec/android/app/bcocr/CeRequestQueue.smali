.class public Lcom/sec/android/app/bcocr/CeRequestQueue;
.super Ljava/lang/Object;
.source "CeRequestQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    }
.end annotation


# static fields
.field private static final ORIENTATION_CHANGE_DURATION:I = 0xc8

.field private static final START_FIRST_REQUEST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "CeRequestQueue"

.field private static final WINDOW_FOCUS_CHANGE_DURATION:I = 0xc8


# instance fields
.field private mLogBuilder:Ljava/lang/StringBuilder;

.field private mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

.field private mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

.field private mRequestQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/android/app/bcocr/CeRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 2
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    .line 65
    new-instance v0, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;-><init>(Lcom/sec/android/app/bcocr/CeRequestQueue;Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    .line 70
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/OCREngine;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/CeRequestQueue;)Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/CeRequestQueue;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->startFirstRequest()V

    return-void
.end method

.method private startFirstRequest()V
    .locals 2

    .prologue
    .line 123
    const-string v0, "CeRequestQueue"

    const-string v1, "startFirstRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->removeMessages(I)V

    .line 128
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getCurrentStateHandler()Lcom/sec/android/app/bcocr/AbstractCeState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 138
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getCurrentStateHandler()Lcom/sec/android/app/bcocr/AbstractCeState;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/CeRequest;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    goto :goto_0

    .line 135
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    const/4 v2, 0x1

    .line 96
    monitor-enter p0

    :try_start_0
    const-string v0, "CeRequestQueue"

    const-string v1, "addRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v0, "CeRequestQueue"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->dumpQueue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 103
    const-string v0, "CeRequestQueue"

    const-string v1, "sending START_FIRST_REQUEST"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->startFirstRequest()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 107
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    if-nez v0, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->startFirstRequest()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 115
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 210
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    monitor-exit p0

    return-void

    .line 211
    :cond_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/CeRequest;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 208
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized completeRequest()V
    .locals 6

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    const-string v2, "CeRequestQueue"

    const-string v3, "completeRequest"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 146
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 147
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/CeRequest;

    .line 148
    .local v0, "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    .end local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    :cond_0
    :goto_0
    :try_start_2
    const-string v2, "CeRequestQueue"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->dumpQueue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->getLandscapeActive()Z

    move-result v2

    if-nez v2, :cond_2

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    if-eqz v2, :cond_1

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    const/4 v3, 0x1

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 168
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 165
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 141
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 152
    .restart local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public declared-synchronized dumpQueue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 87
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 88
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/CeRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mLogBuilder:Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public firstElement()Lcom/sec/android/app/bcocr/CeRequest;
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/CeRequest;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-object v1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/util/NoSuchElementException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFirstRequest(I)Z
    .locals 4
    .param p1, "requestId"    # I

    .prologue
    const/4 v2, 0x0

    .line 277
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/CeRequest;

    .line 278
    .local v1, "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, p1, :cond_0

    .line 279
    const/4 v2, 0x1

    .line 284
    .end local v1    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    :cond_0
    :goto_0
    return v2

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Ljava/util/NoSuchElementException;
    goto :goto_0
.end method

.method public removeRequest(I)V
    .locals 1
    .param p1, "request"    # I

    .prologue
    .line 217
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->removeRequest(IZ)V

    .line 218
    return-void
.end method

.method public declared-synchronized removeRequest(IZ)V
    .locals 10
    .param p1, "request"    # I
    .param p2, "deleteFirst"    # Z

    .prologue
    const/4 v9, 0x1

    .line 221
    monitor-enter p0

    :try_start_0
    const-string v6, "CeRequestQueue"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "removeRequest - deleteFirst:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " deleteFirst:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v6, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v6

    if-ge v6, v9, :cond_1

    .line 224
    const-string v6, "CeRequestQueue"

    const-string v7, "removeRequest queue empty"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 228
    :cond_1
    const/4 v1, 0x0

    .line 229
    .local v1, "firstItemRemoved":Z
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    .local v3, "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 233
    .local v4, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    const/4 v0, 0x0

    .line 235
    .local v0, "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 236
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    check-cast v0, Lcom/sec/android/app/bcocr/CeRequest;

    .line 237
    .restart local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    if-eqz p2, :cond_2

    .line 238
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v6

    if-ne v6, p1, :cond_2

    .line 239
    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    .line 240
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    const/4 v1, 0x1

    .line 246
    :cond_2
    const/4 v2, 0x0

    .line 247
    .local v2, "item":Lcom/sec/android/app/bcocr/CeRequest;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 260
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 261
    .local v5, "iter2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-nez v6, :cond_5

    .line 268
    .end local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    .end local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    .end local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    .end local v5    # "iter2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :goto_3
    if-eqz v1, :cond_0

    .line 269
    :try_start_3
    iget-object v6, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    if-eqz v6, :cond_0

    .line 270
    iget-object v6, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mMainHandler:Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/bcocr/CeRequestQueue$MainHandler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 221
    .end local v1    # "firstItemRemoved":Z
    .end local v3    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 248
    .restart local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    .restart local v1    # "firstItemRemoved":Z
    .restart local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    .restart local v3    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/bcocr/CeRequest;>;"
    .restart local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :cond_4
    :try_start_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    check-cast v2, Lcom/sec/android/app/bcocr/CeRequest;

    .line 249
    .restart local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v6

    if-eqz v6, :cond_3

    .line 254
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v6

    if-ne v6, p1, :cond_3

    .line 255
    const/4 v6, 0x0

    iput-boolean v6, v2, Lcom/sec/android/app/bcocr/CeRequest;->mReserved:Z

    .line 256
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 264
    .end local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    .end local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    .end local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catch_0
    move-exception v6

    goto :goto_3

    .line 262
    .restart local v0    # "firstItem":Lcom/sec/android/app/bcocr/CeRequest;
    .restart local v2    # "item":Lcom/sec/android/app/bcocr/CeRequest;
    .restart local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    .restart local v5    # "iter2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/util/NoSuchElementException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized searchDuplicateRequest(I)Z
    .locals 5
    .param p1, "requestId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 186
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 188
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    const/4 v1, 0x0

    .line 191
    .local v1, "mCount":I
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-gt v2, v4, :cond_1

    move v2, v3

    .line 203
    :goto_0
    monitor-exit p0

    return v2

    .line 196
    :cond_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/bcocr/CeRequest;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 197
    add-int/lit8 v1, v1, 0x1

    .line 198
    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    move v2, v4

    .line 199
    goto :goto_0

    .line 195
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    .line 203
    goto :goto_0

    .line 186
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    .end local v1    # "mCount":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized searchRequest(I)Z
    .locals 4
    .param p1, "requestId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 173
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    iget-object v1, p0, Lcom/sec/android/app/bcocr/CeRequestQueue;->mRequestQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ge v1, v3, :cond_1

    move v1, v2

    .line 182
    :goto_0
    monitor-exit p0

    return v1

    .line 178
    :cond_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/CeRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v1

    if-ne v1, p1, :cond_1

    move v1, v3

    .line 179
    goto :goto_0

    .line 177
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 182
    goto :goto_0

    .line 171
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/bcocr/CeRequest;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
