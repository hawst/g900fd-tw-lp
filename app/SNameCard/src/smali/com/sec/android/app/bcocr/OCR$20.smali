.class Lcom/sec/android/app/bcocr/OCR$20;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 5367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5370
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5384
    :goto_0
    return-void

    .line 5373
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->hideDlg(I)V

    .line 5374
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v0

    if-nez v0, :cond_1

    .line 5375
    invoke-static {}, Lcom/sec/android/app/bcocr/CheckMemory;->isStorageMounted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5376
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 5377
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setStorage(I)V

    goto :goto_0

    .line 5378
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 5379
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 5380
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setStorage(I)V

    goto :goto_0

    .line 5382
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$20;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto :goto_0
.end method
