.class public Lcom/sec/android/app/bcocr/Constant;
.super Ljava/lang/Object;
.source "Constant.java"


# static fields
.field public static final DUMMY_RECT:Landroid/graphics/Rect;

.field public static final ENDOF_FLASH_DIM:I = 0x3

.field public static final ENDOF_MENU:I = 0x5

.field public static final FLASH_DIM_HIGH_TEMPORATURE:I = 0x1

.field public static final FLASH_DIM_LOW_BATTERY:I = 0x0

.field public static final FLASH_DIM_TORCH_LIGHT:I = 0x2

.field public static final GET_RESULT_ERROR:I = 0x2

.field public static final GET_RESULT_ERROR_FORMAT:I = 0x4

.field public static final GET_RESULT_ERROR_SIZE:I = 0x3

.field public static final GET_RESULT_NO_TEXT:I = 0x1

.field public static final GET_RESULT_OK:I = 0x0

.field public static final MENU_CAPTURE_MODE:I = 0x1

.field public static final MENU_FLASH_MODE:I = 0x2

.field public static final MENU_LANGUAGE_SETTING:I = 0x3

.field public static final MENU_OCR_MODE:I = 0x0

.field public static final MENU_SIP_LANGUAGE_SETTING:I = 0x4

.field public static final RECOGNIZE_INITIALIZED:I = 0x0

.field public static final REGOGNIZE_PROCESSING:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, Lcom/sec/android/app/bcocr/Constant;->DUMMY_RECT:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
