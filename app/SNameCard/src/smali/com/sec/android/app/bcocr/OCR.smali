.class public Lcom/sec/android/app/bcocr/OCR;
.super Lcom/sec/android/app/bcocr/AbstractOCRActivity;
.source "OCR.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;
.implements Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;
.implements Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;,
        Lcom/sec/android/app/bcocr/OCR$MainHandler;,
        Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;
    }
.end annotation


# static fields
.field public static final ACTIVE_REQUEST_TIMEOUT:I = 0xce

.field public static final ACTIVE_USER:I = 0xcb

.field private static final ACTIVITY_FINISH_DURATION:I = 0x2710

.field private static final ADVANCED_FOCUS_INITIAL_TIME:J = -0x1L

.field private static final AF_HIDE_RECT_TIMER:I = 0xd0

.field private static final AF_HIDE_RECT_TIMER_DURATION:I = 0x1f4

.field private static final AF_WAIT_TIMER_EXPIRED:I = 0x2

.field public static final CANCEL_ACTIVE_USER:I = 0xcc

.field private static CA_HARDKEY_FULL_PRESS:I = 0x0

.field private static CA_HARDKEY_FULL_UP:I = 0x0

.field private static CA_HARDKEY_HALF_PRESS:I = 0x0

.field private static CA_HARDKEY_NONE:I = 0x0

.field private static final CHECK_FINISH_THREAD_DURATION:I = 0x64

.field private static final CHECK_ORIENT_DURATION:I = 0x320

.field private static final CHECK_ORIENT_EXPIRED:I = 0x8

.field private static DEFAULT_CSC_FILE:Ljava/lang/String; = null

.field private static final DELAY_TIME_IMMEDIATELY:I = 0x0

.field private static final DELAY_TIME_TO_START_AUTO_FOCUS:I = 0x15e

.field public static final DELETE_DETECTED_WORD_RECT:I = 0xcf

.field public static final DIRECT_LINK_TAB:I = 0x0

.field private static final DM_CAMERA_OFF:I = 0x0

.field private static final DM_CAMERA_ON:I = 0x1

.field private static final DRAW_DETECTED_EDGE_FIT_FOR_CAPTURE:I = 0x16

.field private static final FORCE_CAMERA_RESTART:I = 0xb

.field private static final FRAGMENT_LEFT_SIDEMENU_DEFAULT_MODE:I = 0x1

.field public static final FRAGMENT_LEFT_SIDEMENU_SELECTED_MODE:I = 0x1

.field public static final FRAGMENT_LEFT_SIDEMENU_SELECTING_MODE:I = 0x2

.field public static final FRAGMENT_RIGHT_SIDEMENU_CAPTURE:I = 0x1

.field private static final FRAGMENT_RIGHT_SIDEMENU_DEFAULT_MODE:I = 0x2

.field public static final FRAGMENT_RIGHT_SIDEMENU_REALTIME:I = 0x2

.field public static final FRAGMENT_SIDEMENU_NO_CHANGE:I = 0x0

.field private static final HALF_SHUTTER_DURATION:I = 0x12c

.field private static final HANDLER_ACTIVITY_FINISH:I = 0x0

.field private static final INTENT_MSG_SECURITY:Ljava/lang/String; = "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

.field protected static final KEY_BCR_DIALOG_USE_MOBILE_NETWORK_MESSAGE:Ljava/lang/String; = "bcr_dialog_use_mobile_network_message"

.field protected static final KEY_BCR_DIALOG_USE_WIFI_MESSAGE:Ljava/lang/String; = "bcr_dialog_use_wifi_message"

.field protected static final KEY_DETECT_TEXT_LANGUAGE_SELECT_CHECK:Ljava/lang/String; = "detect_text_language_select_check"

.field protected static final KEY_DETECT_TEXT_LOCALE_LANG_CODE:Ljava/lang/String; = "detect_text_locale_lang_code"

.field protected static final KEY_OCR_COPIED_ASSET_DB_DATA:Ljava/lang/String; = "ocr_copied_asset_db_data_130519"

.field protected static final KEY_OCR_ENGINE_DB_VERSION:Ljava/lang/String; = "ocr_engine_db_version"

.field protected static final KEY_OCR_LAST_SELECTED_FUNCMODE:Ljava/lang/String; = "ocr_last_selected_funcmode"

.field protected static final KEY_OCR_LAST_SELECTED_RIGHTSIDEMENU_REALMODE:Ljava/lang/String; = "ocr_last_selected_rightsidemenu_realmode"

.field public static final KEY_OCR_LOCALE_PRELOAD_DIC_NUM:Ljava/lang/String; = "ocr_locale_preload_dic_num"

.field protected static final KEY_OCR_PREV_DIC_DOWNLOAD_INFO_SHOWN:Ljava/lang/String; = "ocr_prev_dic_download_info_shown"

.field private static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field private static final KEY_VOICE_INPUT_CONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_bcr"

.field private static final NUM_POINTER_ALLOWED_FOR_PINCH:I = 0x2

.field private static final NUM_SHUTTERSOUND_CHANNELS:I = 0x3

.field public static final OCRSTATE_IDLE:I = 0x0

.field public static final OCRSTATE_INITIALIZED:I = 0x1

.field public static final OCRSTATE_REGOGNIZE_PROCESSING:I = 0x2

.field private static final OCR_BUNDLE_FLASH_HIGH_TEMPOTURE_DIM:Ljava/lang/String; = "OCR_BUNDLE_FLASH_HIGH_TEMPOTURE_DIM"

.field private static final OCR_BUNDLE_FLASH_LOW_BATTERY_DIM:Ljava/lang/String; = "OCR_BUNDLE_FLASH_LOW_BATTERY_DIM"

.field private static final OCR_BUNDLE_FLASH_TORCH_LIGHT_DIM:Ljava/lang/String; = "OCR_BUNDLE_FLASH_TORCH_LIGHT_DIM"

.field private static final OCR_BUNDLE_FRAGMENT_LEFTSIDE_MODE:Ljava/lang/String; = "OCR_BUNDLE_FRAGMENT_LEFTSIDE_MODE"

.field private static final OCR_BUNDLE_FRAGMENT_RIGHTSIDE_MODE:Ljava/lang/String; = "OCR_BUNDLE_FRAGMENT_RIGHTSIDE_MODE"

.field private static final OCR_BUNDLE_LOW_BATTERY_POPUP_SHOW:Ljava/lang/String; = "OCR_BUNDLE_LOW_BATTERY_POPUP_SHOW"

.field private static final OCR_BUNDLE_MSG_INT_FUNCMODE:Ljava/lang/String; = "OCR_BUNDLE_MSG_INT_FUNCMODE"

.field private static final OCR_BUNDLE_OCRFLASH_MODE:Ljava/lang/String; = "OCR_BUNDLE_OCRFLASH_MODE"

.field private static final OCR_BUNDLE_SIDEMENU_SHOW_STATE:Ljava/lang/String; = "OCR_BUNDLE_SIDEMENU_SHOW_STATE"

.field private static OCR_DETECT_DURATION:I = 0x0

.field private static OCR_DETECT_DURATION_LONG:I = 0x0

.field private static final OCR_DETECT_EXPIRED:I = 0x6

.field public static final OCR_ENGINE_LANGUAGE_MAX_SELECT_NUM:I = 0x1b

.field public static final OCR_ENGINE_LANGUAGE_TOKEN_SEPERATOR:Ljava/lang/String; = "^"

.field private static final OCR_JPEG_RECOGNITION:I = 0x3

.field private static final OCR_PREV_EDGE_DETECTION:I = 0x2

.field private static final OCR_PREV_RECOGNITION:I = 0x1

.field private static final OCR_QRCODE_RECOGNITION:I = 0x4

.field public static final OCR_SIP_TYPE_EMAIL:I = 0x1

.field public static final OCR_SIP_TYPE_LINK:I = 0x0

.field public static final OCR_SIP_TYPE_MULTITEXT:I = 0x4

.field public static final OCR_SIP_TYPE_PHONENUM:I = 0x2

.field public static final OCR_SIP_TYPE_URL:I = 0x3

.field public static OCR_SOUND_RECOGNITION:I = 0x0

.field public static final OCR_TR_DICTIONARY_LANGUAGE_TOKEN_SEPERATOR:Ljava/lang/String; = "^"

.field private static final OPTION_AUTO_CAPTURE:I = 0x3

.field private static final OPTION_SAVE_OPTION:I = 0x2

.field private static final OPTION_UPDATE:I = 0x4

.field private static final RECOGNITION_THREAD_FINISHED:I = 0x15

.field private static final RECOGNITION_THREAD_WAIT_TIMER_EXPIRED:I = 0x14

.field public static final REFESH_VIEW_COMMAND:I = 0xcd

.field private static final RUN_DIALOG_TO_CONFIRM:I = 0xc8

.field private static final RUN_DIALOG_TO_NOTICE_COMFIRMED:I = 0xca

.field private static final SCAN_WAIT_TIMER_EXPIRED:I = 0x7

.field private static final START_AUTO_FOCUS:I = 0x5

.field private static final START_CAPTURE_PROGRESS_TIMER_EXPIRED:I = 0x9

.field private static final STOP_CAPTURE_PROGRESS_TIMER_EXPIRED:I = 0xa

.field protected static final TAG:Ljava/lang/String; = "OCR"

.field private static final TAG_BASE:Ljava/lang/String; = "Settings.Multimedia"

.field private static final TAG_CAMERA:Ljava/lang/String; = "Settings.Multimedia.Camera"

.field private static final TAG_CAMERA_SHUTTER:Ljava/lang/String; = "ShutterSound"

.field public static final TEXT_SEARCH_TAB:I = 0x1

.field public static final TRANSLATE_TAB:I = 0x2

.field public static mOCRSIPType:I

.field private static mStorageToast:Landroid/widget/Toast;

.field public static nOcrNameCardCaptureMode:Z


# instance fields
.field private DEBUG:Z

.field public OCRHandler:Landroid/os/Handler;

.field public final OCR_STATE_CAPTURE_DONE:I

.field public final OCR_STATE_CAPTURE_IMGSAVED:I

.field public final OCR_STATE_CAPTURE_IMGSAVING:I

.field public final OCR_STATE_CAPTURE_INIT:I

.field public final OCR_STATE_CAPTURE_RECOGNIZED:I

.field public final OCR_STATE_CAPTURE_RECOGNIZING:I

.field public final OCR_STATE_CAPTURE_SENDRESULT:I

.field private final OCR_VOICE_SET_STANDBY:I

.field public OCRtoastHandler:Landroid/os/Handler;

.field private final VOICE_FAIL:I

.field private final VOICE_STANDBY:I

.field public _tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

.field private battLevel:I

.field private battScale:I

.field private bisMicrophoneEnabled:Z

.field public devcommand:Lcom/sec/android/app/bcocr/command/MenuCommand;

.field private mAdvancedMacroFocusActive:Z

.field private mAnimation:Landroid/view/animation/Animation;

.field protected mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCaptureRecogThread:Ljava/lang/Thread;

.field private mCaptureStates:I

.field protected mCheckVTCalling:Z

.field private mChkAllowFocusTouch:Z

.field private mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

.field private mDetectedCardEdge:[I

.field private mDetectedCardMinimumEdge:Landroid/graphics/Rect;

.field protected mDvfsHelper:Landroid/os/DVFSHelper;

.field private mEdgeCue:Landroid/widget/RelativeLayout;

.field private mEdgeLeftBottom:Landroid/widget/ImageView;

.field private mEdgeLeftTop:Landroid/widget/ImageView;

.field private mEdgeRightBottom:Landroid/widget/ImageView;

.field private mEdgeRightTop:Landroid/widget/ImageView;

.field protected mEnableDuringCall:Z

.field private mFocusArrowLayout:Landroid/widget/RelativeLayout;

.field private mFocusRestart_DelayTime:J

.field private mFocusTime1:J

.field private mFocusTime2:J

.field private mFocus_indicator:Landroid/widget/ImageView;

.field private mFocus_indicator_base:Landroid/widget/ImageView;

.field private mFragmentLeftSidemenuMode:I

.field private mFragmentRightSidemenuMode:I

.field private mHardKeyStatus:I

.field mHideScaleZoomRect:Ljava/lang/Runnable;

.field private mInitialZoomValueOnScaleBegin:I

.field private mIsAdvanceFocusFocusing:Z

.field private mIsBlurDetection:Z

.field private mIsConfigurationChanged:Z

.field private mIsDestroying:Z

.field private mIsMusicPlaying:Z

.field private mIsTouchDown:Z

.field private mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

.field private mLoadImageCanceled:Landroid/app/AlertDialog;

.field protected mLowBatteryDisableFlashPopupDisplayed:Z

.field private mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

.field private mNetworkStateDialog:Landroid/app/AlertDialog;

.field private mNotSupportZoomToast:Landroid/widget/Toast;

.field private mNumberOfPointer:I

.field public mOCR:Lcom/dmc/ocr/SecMOCR;

.field protected mOCRActionState:I

.field private mOCRCaptureMode:Z

.field protected mOCRCapturingState:Z

.field mOCREngineLanguageSelect:[Z

.field private mOCREngineLanguageSelectedSet:[I

.field private mOCREngineLanguageSelectedSetByTransDic:[I

.field private mOCREnginelanguageSelectedNum:I

.field protected mOCROrientDetectState:Z

.field private mOCROrientation:I

.field private mOCRPreviewState:Z

.field protected mOCRRecogState:I

.field public mOCRSIPText:Ljava/lang/String;

.field public mOCRSelectedArea:Landroid/graphics/Rect;

.field private mOCRZoomingState:Z

.field private mOptSaveImageDialog:Landroid/app/AlertDialog;

.field protected mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

.field private mPopUpCloseBtn:Landroid/widget/Button;

.field private mPopUpTitle:Landroid/widget/TextView;

.field private mPopUpVoiceCommandContent:Landroid/widget/RelativeLayout;

.field private mPopUpVoiceCommandContentText:Landroid/widget/TextView;

.field private mPopUpVoiceCommandContentTitle:Landroid/widget/RelativeLayout;

.field private mPopUpVoiceCommandContentTitleText:Landroid/widget/TextView;

.field private mPopupContentText:Landroid/widget/TextView;

.field private mPreviewIconFlash:Landroid/widget/ImageButton;

.field private mPreviewIconMoreMenu:Landroid/widget/ImageButton;

.field private mPreviewPopupMenu:Landroid/widget/PopupMenu;

.field private mPreviewRecogThread:Ljava/lang/Thread;

.field private mPreviewRotateTemp:[B

.field private mPreviewVoiceControl:Landroid/widget/ImageButton;

.field private mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

.field private final mRecogLock:Ljava/util/concurrent/locks/Lock;

.field private mRecogLockNum:I

.field private mRestoreFuncMode:I

.field private mRotateOnPaused:I

.field private mSaveUri:Landroid/net/Uri;

.field private mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

.field private mScreenHeightRatio:F

.field private mScreenWidthRatio:F

.field private mSecurityToast:Landroid/widget/Toast;

.field private mSideMenuShowState:Z

.field private mSoundPool:Landroid/media/SoundPool;

.field private mSoundPoolId:[I

.field private final mStrCaptureStates:[Ljava/lang/String;

.field private mStreamId:[I

.field private mStreamVolume:F

.field private mTextRecognition:Z

.field private mTouchAutoFocusActive:Z

.field mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

.field private mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

.field mWaitingToPick:Z

.field private mWindowFocusState:Z

.field public m_tr_dic_id:[I

.field public m_tr_dic_name:[Ljava/lang/String;

.field public nCapturedImageHeight:I

.field public nCapturedImageWidth:I

.field public nOCRPreviewHeight:I

.field public nOCRPreviewMarginBottom:I

.field public nOCRPreviewMarginLeft:I

.field public nOCRPreviewMarginRight:I

.field public nOCRPreviewMarginTop:I

.field public nOCRPreviewSensorDetectHeight:I

.field public nOCRPreviewSensorDetectWidth:I

.field public nOCRPreviewSensorHeight:I

.field public nOCRPreviewSensorWidth:I

.field public nOCRPreviewWidth:I

.field ocr_voice_commands:[Ljava/lang/String;

.field public popup:Landroid/widget/PopupWindow;

.field popupview:Landroid/view/View;

.field prev_flat:I

.field prev_sharpness:I

.field private sVoiceLocale:Ljava/lang/String;

.field public sobject:Ljava/lang/Object;

.field public updateCheckHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    sput v1, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_NONE:I

    .line 175
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_HALF_PRESS:I

    .line 177
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_FULL_PRESS:I

    .line 178
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_FULL_UP:I

    .line 289
    sput v1, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    .line 290
    sput-boolean v1, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    .line 350
    sget v0, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    sput v0, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION:I

    .line 351
    sget v0, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    sput v0, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION_LONG:I

    .line 376
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/bcocr/OCR;->OCR_SOUND_RECOGNITION:I

    .line 408
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    .line 419
    invoke-static {}, Lcom/sec/android/app/bcocr/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/OCR;->DEFAULT_CSC_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;-><init>()V

    .line 138
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 141
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    .line 143
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLock:Ljava/util/concurrent/locks/Lock;

    .line 144
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    .line 145
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z

    .line 147
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    .line 148
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mTouchAutoFocusActive:Z

    .line 164
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPoolId:[I

    .line 165
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamId:[I

    .line 169
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsTouchDown:Z

    .line 170
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    .line 171
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    .line 179
    sget v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_NONE:I

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    .line 181
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    .line 192
    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    .line 193
    iput v7, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    .line 194
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mSideMenuShowState:Z

    .line 196
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->battScale:I

    .line 197
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    .line 198
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 213
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 224
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    .line 225
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCROrientation:I

    .line 232
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    .line 233
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    .line 246
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    .line 269
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    .line 271
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRActionState:I

    .line 272
    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    .line 273
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    .line 274
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCROrientDetectState:Z

    .line 275
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    .line 276
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z

    .line 277
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mWindowFocusState:Z

    .line 278
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCaptureMode:Z

    .line 280
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x140

    const/16 v2, 0x3bf

    const/16 v3, 0x2cf

    invoke-direct {v0, v1, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSelectedArea:Landroid/graphics/Rect;

    .line 293
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mWaitingToPick:Z

    .line 298
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    .line 299
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mNetworkStateDialog:Landroid/app/AlertDialog;

    .line 300
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    .line 301
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    .line 302
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    .line 317
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mAdvancedMacroFocusActive:Z

    .line 320
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    .line 321
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->prev_flat:I

    .line 322
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    .line 323
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    .line 328
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREnginelanguageSelectedNum:I

    .line 330
    const/16 v0, 0x1b

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSet:[I

    .line 331
    new-array v0, v8, [I

    .line 332
    const/16 v1, 0x10

    aput v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    .line 348
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 370
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    .line 375
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->sobject:Ljava/lang/Object;

    .line 378
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 379
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->bisMicrophoneEnabled:Z

    .line 381
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->VOICE_STANDBY:I

    .line 382
    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->VOICE_FAIL:I

    .line 383
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_VOICE_SET_STANDBY:I

    .line 396
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_INIT:I

    .line 397
    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_IMGSAVING:I

    .line 398
    iput v7, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_IMGSAVED:I

    .line 399
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_RECOGNIZING:I

    .line 400
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_RECOGNIZED:I

    .line 401
    iput v8, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_SENDRESULT:I

    .line 402
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCR_STATE_CAPTURE_DONE:I

    .line 403
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "init"

    aput-object v1, v0, v4

    const-string v1, "saving..."

    aput-object v1, v0, v6

    const-string v1, "saved"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 404
    const-string v2, "recognizing..."

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "recognized"

    aput-object v2, v0, v1

    const-string v1, "sendresult"

    aput-object v1, v0, v8

    const/4 v1, 0x6

    const-string v2, "done"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStrCaptureStates:[Ljava/lang/String;

    .line 409
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    .line 410
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    .line 413
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 414
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 421
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mEnableDuringCall:Z

    .line 422
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVTCalling:Z

    .line 428
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    .line 430
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRotateOnPaused:I

    .line 431
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    .line 435
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 436
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    .line 438
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nCapturedImageWidth:I

    .line 439
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nCapturedImageHeight:I

    .line 605
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$1;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 741
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$2;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1141
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$3;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    .line 3006
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$4;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    .line 4314
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    .line 4763
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->devcommand:Lcom/sec/android/app/bcocr/command/MenuCommand;

    .line 5176
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$5;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCRHandler:Landroid/os/Handler;

    .line 5205
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$6;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->OCRtoastHandler:Landroid/os/Handler;

    .line 131
    return-void
.end method

.method private SetRecognizeLock(Z)V
    .locals 3
    .param p1, "bSet"    # Z

    .prologue
    .line 5160
    if-eqz p1, :cond_1

    .line 5161
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    .line 5162
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 5163
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] [Recognition] Set RecognizeLock["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ++"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5165
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 5173
    :goto_0
    return-void

    .line 5167
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 5168
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] [Recognition] Set RecognizeLock["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] --"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5170
    :cond_2
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLockNum:I

    .line 5171
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRecogLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/OCR;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 3563
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->handleBatteryChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 5592
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->onFinishRecognitionThread()V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1743
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->onReStartPreview()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/bcocr/OCR;)I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCROrientation:I

    return v0
.end method

.method static synthetic access$16(Lcom/sec/android/app/bcocr/OCR;Z)V
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/app/bcocr/OCR;ZI)V
    .locals 0

    .prologue
    .line 4654
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 3681
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/OCR;)I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 4945
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopFocusArrowLayoutAnimation()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1491
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->hidePopupWindow()V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/PopupMenu;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/app/bcocr/OCR;Landroid/widget/PopupMenu;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 5891
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->createPopupMenu()V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z

    return v0
.end method

.method static synthetic access$28(Lcom/sec/android/app/bcocr/OCR;Z)V
    .locals 0

    .prologue
    .line 5159
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->SetRecognizeLock(Z)V

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/app/bcocr/OCR;[B)Z
    .locals 1

    .prologue
    .line 4811
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->ocrRecognizePreviewData([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/OCR;Z)V
    .locals 0

    .prologue
    .line 4699
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 5023
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->ocrRecognizeCaptureData()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/bcocr/OCR;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/bcocr/OCR;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/bcocr/OCR;)Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/bcocr/OCR;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 5586
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCR;->onChangeRecognitionThread(Ljava/lang/String;Z)V

    return-void
.end method

.method private calculateMinimunBizcardSize()V
    .locals 19

    .prologue
    .line 6447
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f09000b

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    float-to-int v4, v0

    .line 6448
    .local v4, "defaultLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f09000c

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    float-to-int v6, v0

    .line 6449
    .local v6, "defaultTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f090009

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    .line 6450
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f09000d

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    .line 6449
    sub-int v5, v16, v17

    .line 6451
    .local v5, "defaultRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f09000a

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    .line 6452
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f09000e

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    .line 6451
    sub-int v3, v16, v17

    .line 6455
    .local v3, "defaultBottom":I
    const-wide v12, 0x3feb333333333333L    # 0.85

    .line 6456
    .local v12, "minimumRatio":D
    const/4 v9, 0x0

    .line 6457
    .local v9, "left":I
    const/4 v15, 0x0

    .line 6458
    .local v15, "top":I
    const/4 v14, 0x0

    .line 6459
    .local v14, "right":I
    const/4 v2, 0x0

    .line 6461
    .local v2, "bottom":I
    const-string v16, "OCR"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "[EdgeCapture]  "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6462
    sub-int v10, v5, v4

    .line 6463
    .local v10, "lengthX":I
    sub-int v11, v3, v6

    .line 6464
    .local v11, "lengthY":I
    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v16, v16, v12

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    sub-int v16, v10, v16

    div-int/lit8 v7, v16, 0x2

    .line 6465
    .local v7, "gapX":I
    int-to-double v0, v11

    move-wide/from16 v16, v0

    mul-double v16, v16, v12

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    sub-int v16, v11, v16

    div-int/lit8 v8, v16, 0x2

    .line 6467
    .local v8, "gapY":I
    add-int v9, v4, v7

    .line 6468
    sub-int v14, v5, v7

    .line 6469
    add-int v15, v6, v8

    .line 6470
    sub-int v2, v3, v8

    .line 6472
    new-instance v16, Landroid/graphics/Rect;

    move-object/from16 v0, v16

    invoke-direct {v0, v9, v15, v14, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardMinimumEdge:Landroid/graphics/Rect;

    .line 6473
    return-void
.end method

.method private calculateScreenRatio()V
    .locals 9

    .prologue
    .line 6376
    sget-object v6, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v3

    .line 6377
    .local v3, "previewWidth":I
    sget-object v6, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v2

    .line 6378
    .local v2, "previewHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090009

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v5, v6

    .line 6379
    .local v5, "screenWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09000a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v4, v6

    .line 6380
    .local v4, "screenHeight":I
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 6388
    .local v1, "outSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 6389
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 6390
    iget v5, v1, Landroid/graphics/Point;->x:I

    .line 6391
    iget v4, v1, Landroid/graphics/Point;->y:I

    .line 6393
    int-to-float v6, v5

    int-to-float v7, v3

    div-float/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenWidthRatio:F

    .line 6394
    int-to-float v6, v4

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenHeightRatio:F

    .line 6395
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[EdgeCapture]screenWidth : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 6396
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 6395
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6397
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[EdgeCapture]mScreenWidthRatio : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenWidthRatio:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 6398
    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenHeightRatio:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 6397
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6399
    return-void
.end method

.method private checkBatteryStatus()V
    .locals 2

    .prologue
    .line 3558
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 3559
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3560
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3561
    return-void
.end method

.method private checkCaptureCondition()Z
    .locals 4

    .prologue
    .line 5284
    const/4 v0, 0x0

    .line 5285
    .local v0, "currentRect":Landroid/graphics/Rect;
    const/4 v1, 0x1

    .line 5287
    .local v1, "returnValue":Z
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 5289
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardMinimumEdge:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-le v2, v3, :cond_2

    .line 5290
    const/4 v1, 0x0

    .line 5299
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 5300
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->startFocusArrowLayoutAnimation()V

    .line 5303
    :cond_1
    return v1

    .line 5291
    :cond_2
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardMinimumEdge:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-le v2, v3, :cond_3

    .line 5292
    const/4 v1, 0x0

    .line 5293
    goto :goto_0

    :cond_3
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardMinimumEdge:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    if-ge v2, v3, :cond_4

    .line 5294
    const/4 v1, 0x0

    .line 5295
    goto :goto_0

    :cond_4
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardMinimumEdge:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    if-ge v2, v3, :cond_0

    .line 5296
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createPopupMenu()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5892
    const-string v2, "OCR"

    const-string v3, "popupMenuTest!! createPopupMenu #01"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5893
    instance-of v2, p0, Lcom/sec/android/app/bcocr/OCR;

    if-eqz v2, :cond_0

    .line 5894
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconMoreMenu:Landroid/widget/ImageButton;

    if-nez v2, :cond_1

    .line 5895
    :cond_0
    const-string v2, "OCR"

    const-string v3, "[OCR] createPopupMenu:invalid state(1)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5936
    :goto_0
    return-void

    .line 5899
    :cond_1
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconMoreMenu:Landroid/widget/ImageButton;

    invoke-direct {v1, p0, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 5900
    .local v1, "popupMenu":Landroid/widget/PopupMenu;
    if-nez v1, :cond_2

    .line 5901
    const-string v2, "OCR"

    const-string v3, "[OCR] createPopupMenu:invalid state(2)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5905
    :cond_2
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 5906
    .local v0, "menu":Landroid/view/Menu;
    if-nez v0, :cond_3

    .line 5907
    const-string v2, "OCR"

    const-string v3, "[OCR] createPopupMenu:invalid state(3)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5911
    :cond_3
    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 5913
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRUtils;->isSamsungAppsExist(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 5914
    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5917
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    if-nez v2, :cond_5

    .line 5918
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5921
    :cond_5
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_SAVING_OPTION:Z

    if-nez v2, :cond_6

    .line 5922
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5924
    :cond_6
    invoke-virtual {v1, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 5926
    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    .line 5928
    new-instance v2, Lcom/sec/android/app/bcocr/OCR$24;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$24;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 5934
    const-string v2, "OCR"

    const-string v3, "popupMenuTest!! createPopupMenu before Show"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5935
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method

.method private deleteOCRTempFile()V
    .locals 6

    .prologue
    .line 5323
    const/4 v1, 0x0

    .line 5324
    .local v1, "directory":Ljava/lang/String;
    const/4 v0, 0x0

    .line 5326
    .local v0, "OCRTempFileName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v3

    if-nez v3, :cond_1

    .line 5327
    sget-object v1, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 5332
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/.OCRTemp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 5333
    const-string v4, "OCRTemp.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 5332
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5336
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5338
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5340
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCAN"

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 5341
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR]file delete Success "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5348
    :cond_0
    :goto_1
    return-void

    .line 5329
    .end local v2    # "file":Ljava/io/File;
    :cond_1
    sget-object v1, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_MMC:Ljava/lang/String;

    goto :goto_0

    .line 5343
    .restart local v2    # "file":Ljava/io/File;
    :cond_2
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR]file delete Fail "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getDetectedRect()Landroid/graphics/Rect;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5239
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 5241
    .local v0, "currentRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    if-eqz v1, :cond_5

    .line 5243
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v3, 0x6

    aget v2, v2, v3

    if-ge v1, v2, :cond_1

    .line 5244
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v4

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 5249
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v5

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v2, v2, v7

    if-ge v1, v2, :cond_2

    .line 5250
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v7

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 5255
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v2, v2, v8

    if-ge v1, v2, :cond_3

    .line 5256
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v8

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 5261
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v3, 0x7

    aget v2, v2, v3

    if-ge v1, v2, :cond_4

    .line 5262
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 5274
    :goto_3
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    if-eqz v1, :cond_0

    .line 5275
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenWidthRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 5276
    iget v1, v0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenHeightRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 5277
    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenWidthRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 5278
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScreenHeightRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 5280
    :cond_0
    return-object v0

    .line 5246
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 5252
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v5

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 5258
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    aget v1, v1, v6

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_2

    .line 5264
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 5267
    :cond_5
    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 5268
    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 5269
    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 5270
    iput v4, v0, Landroid/graphics/Rect;->right:I

    goto :goto_3
.end method

.method private handleActivityFinish()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1498
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1499
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1501
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 1502
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_1

    .line 1503
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1505
    :cond_1
    return-void
.end method

.method private handleBatteryChanged(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v10, -0x32

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3564
    const-string v3, "status"

    invoke-virtual {p1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 3565
    .local v1, "battStatus":I
    const-string v3, "scale"

    const/16 v4, 0x64

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->battScale:I

    .line 3566
    const-string v3, "level"

    iget v4, p0, Lcom/sec/android/app/bcocr/OCR;->battScale:I

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    .line 3567
    const-string v3, "plugged"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 3568
    .local v0, "battPlugged":I
    const-string v3, "temperature"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3570
    .local v2, "battTemp":I
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] handleBatteryChanged : Level = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/bcocr/OCR;->battScale:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", battPlugged = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", battTemp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3572
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Camera_BatteryTemperatureCheck"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3573
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->bFlagOverheat:Z

    if-eqz v3, :cond_a

    .line 3574
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->bFlagOverheat:Z

    .line 3576
    sget v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_OVERHEAT_AVAILABLE_TEMP:I

    if-lt v2, v3, :cond_0

    .line 3577
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->handleBatteryOverheat()V

    .line 3586
    :cond_0
    :goto_0
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH_LIMITATION_IN_LOW_TEMP:Z

    if-eqz v3, :cond_1

    .line 3587
    if-gt v2, v10, :cond_1

    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_TEMP_FLASH_THRESHOLD_VALUE:I

    if-gt v3, v4, :cond_1

    .line 3588
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    if-nez v3, :cond_1

    .line 3589
    invoke-direct {p0, v7}, Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V

    .line 3594
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_LIMITATION_IN_LOW_TEMP:Z

    if-eqz v3, :cond_3

    .line 3595
    const/16 v3, -0x64

    if-gt v2, v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_TEMP_FLASH_THRESHOLD_VALUE:I

    if-gt v3, v4, :cond_3

    .line 3596
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3597
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 3599
    :cond_2
    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 3600
    invoke-virtual {p0, v7}, Lcom/sec/android/app/bcocr/OCR;->handleLowBattery(Z)V

    .line 3604
    :cond_3
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/OCR;->LOW_BATTERY_WARNING_LEVEL:I

    if-gt v3, v4, :cond_5

    .line 3605
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->LOW_BATTERY_THRESHOLD_VALUE:I

    if-gt v3, v4, :cond_b

    .line 3606
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3607
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 3610
    :cond_4
    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 3611
    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->handleLowBattery(Z)V

    .line 3621
    :cond_5
    :goto_1
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH_LIMITATION_IN_LOW_TEMP:Z

    if-eqz v3, :cond_c

    .line 3622
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/OCR;->LOW_BATTERY_WARNING_LEVEL:I

    if-le v3, v4, :cond_7

    if-le v2, v10, :cond_7

    .line 3623
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3624
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 3627
    :cond_6
    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 3629
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    if-eqz v3, :cond_7

    .line 3630
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 3631
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 3632
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v4

    invoke-virtual {v3, v9, v4, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 3649
    :cond_7
    :goto_2
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_ENABLE_STATUS_BAR:Z

    if-nez v3, :cond_9

    .line 3650
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p0, Lcom/sec/android/app/bcocr/OCR;->battScale:I

    div-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->mBatteryLevel:I

    .line 3651
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->bIsCharging:Z

    .line 3652
    if-eq v0, v9, :cond_8

    if-ne v0, v7, :cond_9

    .line 3653
    :cond_8
    iput-boolean v7, p0, Lcom/sec/android/app/bcocr/OCR;->bIsCharging:Z

    .line 3656
    :cond_9
    return-void

    .line 3580
    :cond_a
    sget v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_OVERHEAT_LIMITATION_TEMP:I

    if-lt v2, v3, :cond_0

    .line 3581
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->handleBatteryOverheat()V

    goto/16 :goto_0

    .line 3613
    :cond_b
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v3, :cond_5

    .line 3614
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    if-nez v3, :cond_5

    .line 3615
    invoke-direct {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->handlePluggedLowBattery(Z)V

    goto :goto_1

    .line 3635
    :cond_c
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->battLevel:I

    sget v4, Lcom/sec/android/app/bcocr/OCR;->LOW_BATTERY_WARNING_LEVEL:I

    if-le v3, v4, :cond_7

    .line 3636
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 3637
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 3640
    :cond_d
    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 3642
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    if-eqz v3, :cond_7

    .line 3643
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 3644
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 3645
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v4

    invoke-virtual {v3, v9, v4, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    goto :goto_2
.end method

.method private handlePluggedLowBattery(Z)V
    .locals 6
    .param p1, "temp"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4700
    const-string v2, "OCR"

    const-string v3, "[OCR] handlePluggedLowBattery"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4702
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4747
    :goto_0
    return-void

    .line 4706
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4707
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0c0004

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 4709
    if-eqz p1, :cond_2

    const v2, 0x7f0c0042

    :goto_1
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 4711
    const v2, 0x7f0c000b

    new-instance v3, Lcom/sec/android/app/bcocr/OCR$16;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$16;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 4718
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 4719
    new-instance v2, Lcom/sec/android/app/bcocr/OCR$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$17;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 4737
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 4738
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v4, v4}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 4740
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v1

    .line 4741
    .local v1, "flashMode":I
    if-eqz v1, :cond_1

    .line 4742
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraFlashMode(IZ)V

    .line 4745
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 4746
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 4709
    .end local v1    # "flashMode":I
    :cond_2
    const v2, 0x7f0c0041

    goto :goto_1
.end method

.method private handleShutterKey()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 4259
    const-string v1, "OCR"

    const-string v2, "[OCR] handleShutterKey"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4261
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] Total-Shot2Shot**StartU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4263
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] ShutterKeyPressed**Point["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4265
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 4266
    const-string v1, "OCR"

    const-string v2, "[OCR] handleShutterKey - CE_STATE_STARTING_PREVIEW"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4275
    :cond_0
    :goto_0
    return v0

    .line 4270
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4271
    const-string v0, "OCR"

    const-string v1, "[OCR] returning because it is capturing.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4272
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private handleShutterKeyReleased()Z
    .locals 2

    .prologue
    .line 4279
    const-string v0, "OCR"

    const-string v1, "[OCR] handleShutterKeyReleased"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4281
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4282
    const-string v0, "OCR"

    const-string v1, "[OCR] handleShutterKeyReleased - returning because it is capturing.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4283
    const/4 v0, 0x1

    .line 4286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideCaptureProgressTimer()V
    .locals 4

    .prologue
    .line 5315
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 5316
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 5317
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_0

    .line 5318
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 5320
    :cond_0
    return-void
.end method

.method private hidePopupWindow()V
    .locals 1

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1493
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1495
    :cond_0
    return-void
.end method

.method private initIntentFilter()V
    .locals 13

    .prologue
    .line 1522
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1524
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v11, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1525
    const-string v11, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1526
    const-string v11, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1527
    const-string v11, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1528
    const-string v11, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1529
    const-string v11, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1530
    const-string v11, "file"

    invoke-virtual {v2, v11}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1531
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v2}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1534
    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    .line 1535
    .local v7, "intentFilterSecurity":Landroid/content/IntentFilter;
    const-string v11, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v7, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1536
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1539
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v11

    const-string v12, "CscFeature_Camera_EnableSmsNotiPopup"

    invoke-virtual {v11, v12}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1540
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 1541
    .local v6, "intentFilterSMSReceives":Landroid/content/IntentFilter;
    const-string v11, "com.sec.mms.intent.action.SMS_RECEIVED"

    invoke-virtual {v6, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1542
    const-string v11, "com.sec.mms.intent.action.MMS_RECEIVED"

    invoke-virtual {v6, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1543
    const-string v11, "com.sec.mms.intent.action.PUSHSMS_RECEIVED"

    invoke-virtual {v6, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1544
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1548
    .end local v6    # "intentFilterSMSReceives":Landroid/content/IntentFilter;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v11

    const-string v12, "CscFeature_Camera_SecurityMdmService"

    invoke-virtual {v11, v12}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1549
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 1550
    .local v4, "intentFilterDcmoSet":Landroid/content/IntentFilter;
    const-string v11, "com.sktelecom.dmc.intent.action.DCMO_SET"

    invoke-virtual {v4, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1551
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v4}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1554
    .end local v4    # "intentFilterDcmoSet":Landroid/content/IntentFilter;
    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1555
    .local v1, "intentAssistiveOff":Landroid/content/IntentFilter;
    const-string v11, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-virtual {v1, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1556
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v1}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1558
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1559
    .local v0, "i":Landroid/content/IntentFilter;
    const-string v11, "com.android.shoot.userchanged"

    invoke-virtual {v0, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1560
    const-string v11, "com.android.shootshare.recvfile"

    invoke-virtual {v0, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1561
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v0}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1563
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 1564
    .local v10, "wifiDirectFilter":Landroid/content/IntentFilter;
    const-string v11, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v10, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1565
    const-string v11, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v10, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1566
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v10}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1569
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 1570
    .local v5, "intentFilterSIOP":Landroid/content/IntentFilter;
    const-string v11, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v5, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1571
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v5}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1573
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 1574
    .local v3, "intentFilterCover":Landroid/content/IntentFilter;
    const-string v11, "com.samsung.cover.OPEN"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1575
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v3}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1578
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 1579
    .local v8, "intentFilterShutdown":Landroid/content/IntentFilter;
    const-string v11, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v8, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1580
    const-string v11, "POWER_OFF_ANIMATION_START"

    invoke-virtual {v8, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1581
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v8}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1583
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 1584
    .local v9, "intentSideSync":Landroid/content/IntentFilter;
    const-string v11, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v9, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1585
    iget-object v11, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v9}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1588
    new-instance v11, Landroid/content/Intent;

    const-string v12, "intent.stop.app-in-app"

    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v11}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 1589
    return-void
.end method

.method private initIntentFilterBattery()V
    .locals 2

    .prologue
    .line 1593
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1594
    .local v0, "intentFilterBattery":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1595
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1596
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1597
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1598
    return-void
.end method

.method private initLayout()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    if-nez v1, :cond_0

    .line 767
    new-instance v1, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    const/high16 v2, 0x7f030000

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;)V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    .line 768
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;->showMenu()V

    .line 772
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->showShootingMenu(I)V

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_1

    .line 776
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 780
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    .line 781
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 784
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_CAMERA_RESIZE_PREVIEW_SUPPORT:Z

    if-eqz v1, :cond_2

    .line 785
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->resizePreviewAspectRatio()V

    .line 788
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    if-nez v1, :cond_3

    .line 789
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f001f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    .line 790
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    if-eqz v1, :cond_b

    const v1, 0x7f020032

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 794
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    if-nez v1, :cond_4

    .line 796
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0024

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    .line 799
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    if-nez v1, :cond_5

    .line 800
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0023

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    .line 803
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeCue:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_c

    .line 804
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0026

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeCue:Landroid/widget/RelativeLayout;

    .line 805
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f002b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    .line 806
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f002d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    .line 807
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f002c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    .line 808
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f002e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    .line 818
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    if-nez v1, :cond_6

    .line 819
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0020

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    .line 822
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_SUPPORT:Z

    if-nez v1, :cond_7

    .line 823
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 826
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 827
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    const v2, 0x7f020036

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 837
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLayout:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_8

    .line 838
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f001d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLayout:Landroid/widget/RelativeLayout;

    .line 843
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpCloseBtn:Landroid/widget/Button;

    if-eqz v1, :cond_9

    .line 844
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpCloseBtn:Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/app/bcocr/OCR$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$7;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 852
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0022

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconMoreMenu:Landroid/widget/ImageButton;

    .line 853
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    if-eqz v1, :cond_a

    .line 854
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconMoreMenu:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 856
    :cond_a
    return-void

    .line 791
    :cond_b
    const v1, 0x7f020031

    goto/16 :goto_0

    .line 811
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeCue:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 812
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 814
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 829
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    const v2, 0x7f020035

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method private initRemains()V
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine;->setOnFocusStateChangedListener(Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;)V

    .line 1603
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine;->setOnErrorCallbackListener(Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;)V

    .line 1604
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine;->setOnRecognitionStateChangedListener(Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;)V

    .line 1605
    invoke-static {}, Lcom/sec/android/app/bcocr/CheckMemory;->isStorageMounted()Z

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->checkStorage(ZZ)V

    .line 1606
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 1607
    return-void
.end method

.method private initTalkbackTTSForFlash()V
    .locals 3

    .prologue
    .line 6238
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f001f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 6240
    .local v0, "mPreviewFlashChangeBtn":Landroid/widget/ImageButton;
    new-instance v1, Lcom/sec/android/app/bcocr/OCR$26;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$26;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 6253
    return-void
.end method

.method private initTalkbackTTSForVoiceControl()V
    .locals 3

    .prologue
    .line 6218
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f0020

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 6220
    .local v0, "mPreviewVoiceControlChangeBtn":Landroid/widget/ImageButton;
    new-instance v1, Lcom/sec/android/app/bcocr/OCR$25;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$25;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 6235
    return-void
.end method

.method private isPrevRecogAvailable()Z
    .locals 1

    .prologue
    .line 3682
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailableExceptFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3683
    const/4 v0, 0x1

    .line 3685
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPrevRecogAvailableExceptFocus()Z
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 3659
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    if-eqz v1, :cond_0

    .line 3678
    :goto_0
    return v0

    .line 3661
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mWindowFocusState:Z

    if-nez v1, :cond_1

    .line 3662
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] isPrevRecogAvailable:false(winFocus:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mWindowFocusState:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3664
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v1

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v1

    if-nez v1, :cond_3

    .line 3665
    :cond_2
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] isPrevRecogAvailable:false(ocrActionState:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3667
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    if-eqz v1, :cond_4

    .line 3668
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] isPrevRecogAvailable:false(zooming state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3670
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v1

    if-ne v1, v2, :cond_5

    .line 3671
    const-string v1, "OCR"

    const-string v2, "[OCR] isPrevRecogAvailable:false(detect text mode)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3673
    :cond_5
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    if-ne v1, v2, :cond_6

    .line 3674
    const-string v1, "OCR"

    const-string v2, "[OCR] isPrevRecogAvailable:false(Left sidemenu is selecting mode)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3678
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private isTouchAutoFocusEnabled()Z
    .locals 2

    .prologue
    .line 3069
    const/4 v0, 0x0

    .line 3070
    .local v0, "shootingmode":Z
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3079
    :cond_0
    :goto_0
    return v0

    .line 3074
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3075
    const/4 v0, 0x1

    goto :goto_0

    .line 3070
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private loadLastFuncMode()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 2347
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2348
    .local v2, "preferences":Landroid/content/SharedPreferences;
    const-string v3, "ocr_last_selected_funcmode"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2349
    .local v0, "lastFuncMode":I
    const-string v3, "ocr_last_selected_rightsidemenu_realmode"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2351
    .local v1, "lastRealMode":I
    if-ne v0, v5, :cond_1

    .line 2352
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    .line 2357
    :goto_0
    if-ne v1, v5, :cond_2

    .line 2358
    iput v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    .line 2363
    :goto_1
    if-ltz v0, :cond_0

    if-gt v0, v4, :cond_0

    .line 2364
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    .line 2366
    :cond_0
    return-void

    .line 2354
    :cond_1
    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    goto :goto_0

    .line 2360
    :cond_2
    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    goto :goto_1
.end method

.method private declared-synchronized ocrRecognizeCaptureData()Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 5024
    monitor-enter p0

    :try_start_0
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition]ocrRecognizeCaptureData"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5025
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getInstance()Lcom/dmc/ocr/SecMOCR;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 5026
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v1, :cond_0

    .line 5027
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition]ocrRecognizeCaptureData) : mOCR instance is null => skip recognition !!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5077
    :goto_0
    monitor-exit p0

    return v0

    .line 5031
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->resetRecogResult()V

    .line 5032
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 5033
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/dmc/ocr/SecMOCR;->setRecognitionMode(I)V

    .line 5035
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v1, :cond_1

    .line 5036
    const-string v1, "OCR"

    .line 5037
    const-string v2, "[OCR] [Recognition]ocrRecognizeCaptureData : mOCREngine is null => skip recognition !!"

    .line 5036
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5041
    :cond_1
    :try_start_2
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    if-nez v1, :cond_2

    .line 5042
    const-string v1, "OCR"

    .line 5043
    const-string v2, "[OCR] [Recognition]ocrRecognizeCaptureData) : mOCREngine.myImageData is null => skip recognition !!"

    .line 5042
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5047
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSelectedArea:Landroid/graphics/Rect;

    if-nez v1, :cond_3

    .line 5048
    const-string v1, "OCR"

    .line 5049
    const-string v2, "[OCR] [Recognition]ocrRecognizeCaptureData) : mPreviewView or mOCRSelectedArea is null => skip recognition !!"

    .line 5048
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5053
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSelectedArea:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    .line 5054
    const/4 v2, 0x0

    .line 5055
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 5056
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090009

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 5057
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f090061

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5053
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 5058
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSelectedArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 5060
    sget-object v0, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    .line 5061
    sget v2, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    add-int/lit8 v5, v5, -0x1

    .line 5062
    sget v6, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    add-int/lit8 v6, v6, -0x1

    .line 5060
    invoke-static/range {v0 .. v6}, Lcom/dmc/ocr/SecMOCR;->setJPEGData([IIIIIII)Z

    .line 5064
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dmc/ocr/SecMOCR;->startRecognition(Landroid/content/Context;)V

    .line 5065
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v0}, Lcom/dmc/ocr/SecMOCR;->getRecogResult()I

    .line 5066
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v0}, Lcom/dmc/ocr/SecMOCR;->getWholeRecogResult()I

    .line 5067
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 5069
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->OCR_SUPPORT_MULTI_THREAD_TO_RECOGNIZE:Z

    if-nez v0, :cond_4

    .line 5071
    sget-object v0, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    if-eqz v0, :cond_4

    .line 5072
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 5076
    :cond_4
    const-string v0, "OCR"

    const-string v1, "[OCR] OCR Capture result"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v7

    .line 5077
    goto/16 :goto_0
.end method

.method private declared-synchronized ocrRecognizePreviewData([B)Z
    .locals 17
    .param p1, "previewData"    # [B

    .prologue
    .line 4812
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 4813
    .local v10, "rs":Landroid/content/res/Resources;
    const/4 v4, 0x0

    .line 4814
    .local v4, "margin_left":I
    const/4 v5, 0x0

    .line 4815
    .local v5, "margin_top":I
    const/4 v6, 0x0

    .line 4816
    .local v6, "margin_right":I
    const/4 v7, 0x0

    .line 4818
    .local v7, "margin_bottom":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 4819
    const-string v2, "OCR"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[OCR] [Recognition]ocrRecognizePreviewData: buffer:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4820
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4821
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4822
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4823
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewHeight:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4824
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4825
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4826
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4827
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4819
    invoke-static {v2, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4830
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_1

    .line 4831
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : mSubMain is null => skip recognition !!"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4832
    const/4 v2, 0x0

    .line 4941
    :goto_0
    monitor-exit p0

    return v2

    .line 4835
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    if-eqz v2, :cond_2

    .line 4836
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : Capturing... => skip !!"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4837
    const/4 v2, 0x0

    goto :goto_0

    .line 4840
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v2, :cond_3

    .line 4841
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : mOCREngine is null => skip recognition !!"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4842
    const/4 v2, 0x0

    goto :goto_0

    .line 4845
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    if-eqz v2, :cond_4

    .line 4846
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : mOCREngine is capturing state => skip recognition !!"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4847
    const/4 v2, 0x0

    goto :goto_0

    .line 4850
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    if-nez v2, :cond_5

    .line 4851
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : mOCREngine.mPreviewData is null"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4852
    const/4 v2, 0x0

    goto :goto_0

    .line 4855
    :cond_5
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getInstance()Lcom/dmc/ocr/SecMOCR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 4856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v2, :cond_6

    .line 4857
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]ocrRecognizePreviewData : mOCR instance is null => skip recognition !!"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4858
    const/4 v2, 0x0

    goto :goto_0

    .line 4861
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v2, :cond_7

    .line 4862
    const-string v2, "OCR"

    const-string v11, "[mycheck]detect 3-1 : setOCROrientationPreviewBuf ++"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4864
    :cond_7
    sget v3, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    .line 4866
    .local v3, "previewOrient":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v2, :cond_8

    .line 4867
    const-string v2, "OCR"

    const-string v11, "[mycheck]detect 3-2 : setOCROrientationPreviewBuf --"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4869
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRSelectedArea:Landroid/graphics/Rect;

    new-instance v11, Landroid/graphics/Rect;

    .line 4870
    const/4 v12, 0x0

    const v13, 0x7f090061

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    .line 4871
    const v14, 0x7f090009

    invoke-virtual {v10, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v14, v15

    float-to-int v14, v14

    .line 4872
    const v15, 0x7f09000a

    invoke-virtual {v10, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v15

    float-to-int v15, v15

    const v16, 0x7f090061

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    sub-int v15, v15, v16

    add-int/lit8 v15, v15, -0x1

    invoke-direct {v11, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4869
    invoke-virtual {v2, v11}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 4873
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->resetRecogResult()V

    .line 4876
    const/4 v4, 0x0

    .line 4877
    const/4 v5, 0x0

    .line 4878
    const/4 v6, 0x0

    .line 4879
    const/4 v7, 0x0

    move-object/from16 v2, p0

    .line 4881
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/bcocr/OCR;->setOCROrientationPreviewBuf(IIIII)Z

    move-result v2

    if-nez v2, :cond_9

    .line 4882
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v11, 0x0

    iput-object v11, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4883
    const-string v2, "OCR"

    const-string v11, "[OCR] [Recognition]buffer rotation fail ==> skip"

    invoke-static {v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4884
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4888
    :cond_9
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    if-eqz v2, :cond_d

    .line 4889
    sget v8, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_BLUR_THRESHOLD_PREVIEWMODE:I

    .line 4890
    .local v8, "blur_threshold":I
    sget v9, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_FLAT_THRESHOLD_PREVIEWMODE:I

    .line 4892
    .local v9, "flat_threshold":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    const/4 v11, 0x1

    if-ne v2, v11, :cond_a

    .line 4893
    sget v8, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_BLUR_THRESHOLD_CAPTUREMODE:I

    .line 4894
    sget v9, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_FLAT_THRESHOLD_CAPTUREMODE:I

    .line 4895
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4896
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4895
    invoke-static {v2, v11, v12}, Lcom/sec/android/secvision/imageprocessing/OcrDNR;->getGlobalSharpness([BII)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    .line 4897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4898
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4897
    invoke-static {v2, v11, v12}, Lcom/sec/android/secvision/imageprocessing/OcrDNR;->getGlobalStdDev([BII)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_flat:I

    .line 4907
    :goto_1
    const-string v11, "OCR"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] check camera focus : "

    invoke-direct {v12, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4908
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    const/4 v13, 0x1

    if-ne v2, v13, :cond_b

    const-string v2, "capture"

    :goto_2
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 4909
    const-string v12, " a="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, ", b="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->prev_flat:I

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4907
    invoke-static {v11, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4911
    const-string v2, "OCR"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[OCR] check camera focus : mIsBlurDetection : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4913
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    if-ge v2, v8, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_flat:I

    if-le v2, v9, :cond_c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    if-eqz v2, :cond_c

    .line 4914
    const-string v2, "OCR"

    const-string v11, "[OCR] check camera focus : threshold out (skip)"

    invoke-static {v2, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4915
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime1:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/bcocr/OCR;->doMacroFocus(J)Z

    .line 4916
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    .line 4917
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4900
    :cond_a
    sget v8, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_BLUR_THRESHOLD_PREVIEWMODE:I

    .line 4901
    sget v9, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_FLAT_THRESHOLD_PREVIEWMODE:I

    .line 4902
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4903
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4902
    invoke-static {v2, v11, v12}, Lcom/sec/android/secvision/imageprocessing/OcrDNR;->getCenterSharpness([BII)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    .line 4904
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4905
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4904
    invoke-static {v2, v11, v12}, Lcom/sec/android/secvision/imageprocessing/OcrDNR;->getCenterStdDev([BII)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_flat:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 4812
    .end local v3    # "previewOrient":I
    .end local v4    # "margin_left":I
    .end local v5    # "margin_top":I
    .end local v6    # "margin_right":I
    .end local v7    # "margin_bottom":I
    .end local v8    # "blur_threshold":I
    .end local v9    # "flat_threshold":I
    .end local v10    # "rs":Landroid/content/res/Resources;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 4909
    .restart local v3    # "previewOrient":I
    .restart local v4    # "margin_left":I
    .restart local v5    # "margin_top":I
    .restart local v6    # "margin_right":I
    .restart local v7    # "margin_bottom":I
    .restart local v8    # "blur_threshold":I
    .restart local v9    # "flat_threshold":I
    .restart local v10    # "rs":Landroid/content/res/Resources;
    :cond_b
    :try_start_2
    const-string v2, "preview"

    goto/16 :goto_2

    .line 4919
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->prev_sharpness:I

    if-lt v2, v8, :cond_d

    .line 4920
    const-wide/16 v12, -0x1

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    .line 4925
    .end local v8    # "blur_threshold":I
    .end local v9    # "flat_threshold":I
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mAutoCaptureEnabled:Z

    if-eqz v2, :cond_e

    .line 4926
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 4927
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4926
    invoke-static {v2, v11, v12}, Lcom/dmc/ocr/SecMOCR;->MOCR_DetectCardYUV([BII)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    .line 4932
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    if-nez v2, :cond_f

    .line 4933
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4929
    :cond_e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    goto :goto_3

    .line 4935
    :cond_f
    const-string v2, "OCR"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[EdgeCapture] detected edge : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x0

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x1

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4936
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x2

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x3

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x4

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 4937
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x5

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x6

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/bcocr/OCR;->mDetectedCardEdge:[I

    const/4 v13, 0x7

    aget v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4935
    invoke-static {v2, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4938
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mIsEdgeDetected:Z

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v2, :cond_10

    .line 4939
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v11, 0x16

    invoke-virtual {v2, v11}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4941
    :cond_10
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private onChangeRecognitionThread(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "state"    # Z

    .prologue
    .line 5587
    const-string v0, "Text"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5588
    iput-boolean p2, p0, Lcom/sec/android/app/bcocr/OCR;->mTextRecognition:Z

    .line 5590
    :cond_0
    return-void
.end method

.method private onFinishRecognitionThread()V
    .locals 4

    .prologue
    const/16 v2, 0x14

    .line 5593
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mTextRecognition:Z

    if-eqz v1, :cond_3

    .line 5595
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 5597
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    if-eqz v1, :cond_0

    .line 5598
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 5601
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mTextRecognition:Z

    .line 5602
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_1

    .line 5603
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 5605
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->hideCaptureProgressTimer()V

    .line 5607
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 5608
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x15

    iput v1, v0, Landroid/os/Message;->what:I

    .line 5609
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_2

    .line 5610
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 5620
    :cond_2
    :goto_0
    return-void

    .line 5614
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 5615
    .restart local v0    # "msg":Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->what:I

    .line 5616
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_2

    .line 5617
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private onPrepareRecognitionThread()V
    .locals 1

    .prologue
    .line 5583
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mTextRecognition:Z

    .line 5584
    return-void
.end method

.method private onReStartPreview()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1744
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v1, :cond_1

    .line 1745
    const-string v1, "OCR"

    const-string v2, "[OCR] onReStartPreview"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 1753
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->waitForEngineStartingThread()V

    .line 1754
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->waitForStartPreviewThreadComplete()V

    .line 1755
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->waitForCurrentSearchingLastContentUri()V

    .line 1756
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->waitForCurrentPictureSaving()V

    .line 1759
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    .line 1761
    .local v0, "oldState":I
    if-eqz v0, :cond_0

    .line 1764
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 1765
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 1766
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->clearRequest()V

    .line 1767
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1769
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartEngine()V

    .line 1770
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->schedulePostInit()V

    .line 1771
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleSetAllParams()V

    .line 1772
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartPreview()V

    .line 1774
    .end local v0    # "oldState":I
    :cond_1
    return-void
.end method

.method private onRotateClear()V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuResourceDepot;->clearAllMenusForRotate()V

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->unbindView(Landroid/view/View;)V

    .line 865
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 866
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 867
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    .line 869
    :cond_1
    return-void
.end method

.method private onRotateResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 872
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->restartInactivityTimer()V

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->setCameraDisplayOrientation()V

    .line 877
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setOCRActivityOrientation(I)V

    .line 879
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->initLayout()V

    .line 881
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 882
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR;->setSideMenu(IIZZ)V

    .line 884
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    .line 885
    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z

    .line 886
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->setPreviewButtonEnable()V

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isPreviewStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 889
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearRequest()V

    .line 892
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/bcocr/OCR;->initFuncMode(IZ)V

    .line 894
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    .line 895
    return-void
.end method

.method private resetFocusDueToZoom()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2984
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 2985
    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 2987
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    .line 2988
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2989
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 2991
    :cond_0
    return-void
.end method

.method private saveCurrentFuncMode()V
    .locals 5

    .prologue
    .line 2369
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2370
    .local v2, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2371
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v0

    .line 2372
    .local v0, "lastFuncMode":I
    if-ltz v0, :cond_0

    const/4 v3, 0x1

    if-gt v0, v3, :cond_0

    .line 2373
    const-string v3, "ocr_last_selected_funcmode"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2374
    const-string v3, "ocr_last_selected_rightsidemenu_realmode"

    iget v4, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2375
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2377
    :cond_0
    return-void
.end method

.method private setImmutableView(Lcom/sec/android/app/bcocr/MenuBase;)V
    .locals 4
    .param p1, "view"    # Lcom/sec/android/app/bcocr/MenuBase;

    .prologue
    .line 1508
    if-nez p1, :cond_1

    .line 1509
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/bcocr/MenuResourceDepot;->getMenuByLayoutId(ILandroid/view/ViewGroup;)Lcom/sec/android/app/bcocr/MenuBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/EmptyView;

    .line 1510
    .local v0, "emptyview":Lcom/sec/android/app/bcocr/EmptyView;
    if-eqz v0, :cond_0

    .line 1511
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;->setChild(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 1512
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/EmptyView;->showMenu()V

    .line 1518
    .end local v0    # "emptyview":Lcom/sec/android/app/bcocr/EmptyView;
    :cond_0
    :goto_0
    return-void

    .line 1515
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseMenu:Lcom/sec/android/app/bcocr/widget/TwBaseMenu;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/bcocr/widget/TwBaseMenu;->setChild(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 1516
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/MenuBase;->showMenu()V

    goto :goto_0
.end method

.method private setLocaleAsSvoice(Landroid/content/res/Resources;)V
    .locals 12
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 6297
    if-nez p1, :cond_1

    .line 6355
    :cond_0
    :goto_0
    return-void

    .line 6300
    :cond_1
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 6301
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 6302
    .local v3, "globalLocale":Ljava/util/Locale;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 6306
    sget-boolean v8, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    if-nez v8, :cond_6

    .line 6307
    const/16 v8, 0xa

    new-array v1, v8, [Ljava/lang/String;

    .line 6308
    const-string v8, "ko"

    aput-object v8, v1, v7

    const-string v8, "en"

    aput-object v8, v1, v10

    const-string v8, "zh"

    aput-object v8, v1, v11

    const/4 v8, 0x3

    const-string v9, "es"

    aput-object v9, v1, v8

    const/4 v8, 0x4

    const-string v9, "fr"

    aput-object v9, v1, v8

    const/4 v8, 0x5

    const-string v9, "de"

    aput-object v9, v1, v8

    const/4 v8, 0x6

    const-string v9, "it"

    aput-object v9, v1, v8

    const/4 v8, 0x7

    const-string v9, "ja"

    aput-object v9, v1, v8

    const/16 v8, 0x8

    const-string v9, "ru"

    aput-object v9, v1, v8

    const/16 v8, 0x9

    const-string v9, "pt"

    aput-object v9, v1, v8

    .line 6310
    .local v1, "availableLocale":[Ljava/lang/String;
    array-length v8, v1

    :goto_1
    if-lt v7, v8, :cond_4

    .line 6324
    :cond_2
    :goto_2
    new-instance v7, Ljava/util/Locale;

    const-string v8, "en_us"

    invoke-direct {v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 6354
    .end local v1    # "availableLocale":[Ljava/lang/String;
    :cond_3
    :goto_3
    const/4 v7, 0x0

    invoke-virtual {p1, v2, v7}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto :goto_0

    .line 6310
    .restart local v1    # "availableLocale":[Ljava/lang/String;
    :cond_4
    aget-object v4, v1, v7

    .line 6311
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 6312
    aget-object v7, v1, v11

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 6313
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->isChinaFeature()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 6314
    const-string v7, "CN"

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_2

    .line 6310
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 6326
    .end local v1    # "availableLocale":[Ljava/lang/String;
    .end local v4    # "s":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 6327
    const-string v9, "voicetalk_language"

    .line 6326
    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    .line 6328
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    if-nez v8, :cond_7

    .line 6329
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    .line 6332
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    const-string v9, "zh"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 6333
    const-string v7, "ro.csc.country_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6334
    .local v0, "ContryCode":Ljava/lang/String;
    if-eqz v0, :cond_8

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "china"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 6335
    const-string v7, "CN"

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 6336
    new-instance v7, Ljava/util/Locale;

    const-string v8, "zh"

    const-string v9, "CN"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_3

    .line 6338
    :cond_8
    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    invoke-direct {v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_3

    .line 6341
    .end local v0    # "ContryCode":Ljava/lang/String;
    :cond_9
    const-string v8, "pt-BR"

    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 6342
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    const-string v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 6343
    .local v5, "splitVoiceLocale":[Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 6344
    const/4 v6, 0x0

    .line 6345
    .local v6, "voicetalkLocale":Ljava/util/Locale;
    new-instance v6, Ljava/util/Locale;

    .end local v6    # "voicetalkLocale":Ljava/util/Locale;
    aget-object v7, v5, v7

    aget-object v8, v5, v10

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6346
    .restart local v6    # "voicetalkLocale":Ljava/util/Locale;
    iput-object v6, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_3

    .line 6349
    .end local v5    # "splitVoiceLocale":[Ljava/lang/String;
    .end local v6    # "voicetalkLocale":Ljava/util/Locale;
    :cond_a
    new-instance v7, Ljava/util/Locale;

    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->sVoiceLocale:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    .line 6350
    invoke-virtual {v3}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v9, v10}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6349
    iput-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_3
.end method

.method private setVoiceIndicatorStandBy()V
    .locals 4

    .prologue
    const/16 v1, 0x65

    .line 6211
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 6212
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 6213
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 6215
    :cond_0
    return-void
.end method

.method private showPopupWindow()V
    .locals 6

    .prologue
    .line 1462
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->hidePopupWindow()V

    .line 1463
    const/16 v0, 0x5dc

    .line 1464
    .local v0, "talkback_delay":I
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/bcocr/OCR$14;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$14;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    .line 1488
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x5dc

    :goto_0
    int-to-long v4, v1

    .line 1464
    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1489
    return-void

    .line 1488
    :cond_0
    const/16 v1, 0x32

    goto :goto_0
.end method

.method private showShootingMenu(I)V
    .locals 1
    .param p1, "shootingMode"    # I

    .prologue
    .line 3432
    const/4 v0, 0x0

    .line 3439
    .local v0, "menu":Lcom/sec/android/app/bcocr/MenuBase;
    invoke-direct {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setImmutableView(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 3440
    return-void
.end method

.method private showZoomBarAction()V
    .locals 1

    .prologue
    .line 2394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    .line 2395
    return-void
.end method

.method private startAFWaitTimer()V
    .locals 4

    .prologue
    .line 4640
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 4641
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 4642
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_0

    .line 4643
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 4645
    :cond_0
    return-void
.end method

.method private startFocusArrowLayoutAnimation()V
    .locals 4

    .prologue
    .line 4953
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 4962
    :goto_0
    return-void

    .line 4956
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 4957
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAnimation:Landroid/view/animation/Animation;

    .line 4958
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 4959
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAnimation:Landroid/view/animation/Animation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 4960
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAnimation:Landroid/view/animation/Animation;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 4961
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private startOCRDetectTimer(ZI)V
    .locals 4
    .param p1, "bShort"    # Z
    .param p2, "num"    # I

    .prologue
    .line 4655
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 4656
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "[OCR][mycheck] startOCRDetectTimer("

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4657
    if-eqz p1, :cond_2

    sget v1, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION:I

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4656
    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4658
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR][mycheck] OCR_DETECT_DURATION : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4660
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 4661
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopOCRDetectTimer()V

    .line 4662
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 4663
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 4665
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_1

    .line 4666
    if-eqz p1, :cond_3

    .line 4667
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    sget v2, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 4672
    :cond_1
    :goto_1
    return-void

    .line 4657
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    sget v1, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION_LONG:I

    goto :goto_0

    .line 4669
    .restart local v0    # "msg":Landroid/os/Message;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    sget v2, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION_LONG:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method private stopAFWaitTimer()V
    .locals 2

    .prologue
    .line 4649
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 4650
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 4652
    :cond_0
    return-void
.end method

.method private stopFocusArrowLayoutAnimation()V
    .locals 2

    .prologue
    .line 4946
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 4947
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 4948
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 4950
    :cond_0
    return-void
.end method

.method private stopOCRDetectTimer()V
    .locals 2

    .prologue
    .line 4675
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 4676
    const-string v0, "OCR"

    const-string v1, "[OCR][mycheck] stopOCRDetectTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4678
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_1

    .line 4679
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 4681
    :cond_1
    return-void
.end method

.method private unsetLocale(Landroid/content/res/Resources;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 6358
    if-nez p1, :cond_1

    .line 6368
    :cond_0
    :goto_0
    return-void

    .line 6361
    :cond_1
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 6362
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 6363
    .local v1, "globalLocale":Ljava/util/Locale;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 6366
    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 6367
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto :goto_0
.end method


# virtual methods
.method public CannotStartCamera()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5652
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVoIPCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVTCalling:Z

    if-eqz v0, :cond_1

    .line 5653
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->showDlg(I)V

    .line 5657
    :goto_0
    return-void

    .line 5655
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->hideDlg(I)V

    goto :goto_0
.end method

.method public OCRByteArrayRotate0([BI[BIIIIII)Z
    .locals 18
    .param p1, "srcBuf"    # [B
    .param p2, "srcBufLength"    # I
    .param p3, "tempBuf"    # [B
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "marginLeft"    # I
    .param p7, "marginTop"    # I
    .param p8, "marginRight"    # I
    .param p9, "marginBottom"    # I

    .prologue
    .line 4095
    const-string v12, "OCR"

    const-string v13, "[OCR] ===> OCRImgRotate : OCRByteArrayRotate180"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4101
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p2, :cond_1

    .line 4102
    :cond_0
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate0 : width("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") or height("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") is null"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4103
    const/4 v12, 0x0

    .line 4175
    :goto_0
    return v12

    .line 4106
    :cond_1
    mul-int v12, p4, p5

    move/from16 v0, p2

    if-ge v0, v12, :cond_2

    .line 4107
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate0 : srcBufLength("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") should be bigger than width*height*1.5("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    mul-int v14, p4, p5

    int-to-double v14, v14

    const-wide/high16 v16, 0x3ff8000000000000L    # 1.5

    mul-double v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4108
    const/4 v12, 0x0

    goto :goto_0

    .line 4111
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_4

    .line 4112
    :cond_3
    const-string v12, "OCR"

    const-string v13, "[OCR] ===> OCRByteArrayRotate0 : srcBuf or tempBuf is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4113
    const/4 v12, 0x0

    goto :goto_0

    .line 4116
    :cond_4
    move-object/from16 v0, p1

    array-length v12, v0

    move/from16 v0, p2

    if-lt v12, v0, :cond_5

    move-object/from16 v0, p3

    array-length v12, v0

    move/from16 v0, p2

    if-ge v12, v0, :cond_6

    .line 4117
    :cond_5
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate0 : srcBuf.length("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") or tempBuf.length("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") should be bigger than srcBufLength("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4118
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4122
    :cond_6
    sub-int v10, p4, p8

    .line 4123
    .local v10, "xEnd":I
    sub-int v11, p5, p9

    .line 4124
    .local v11, "yEnd":I
    sub-int v12, p4, p6

    sub-int v5, v12, p8

    .line 4125
    .local v5, "cropWidth":I
    sub-int v12, p5, p7

    sub-int v2, v12, p9

    .line 4126
    .local v2, "cropHeight":I
    const/4 v3, 0x0

    .line 4127
    .local v3, "cropI":I
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    .line 4128
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4130
    if-lez v5, :cond_7

    if-lez v2, :cond_7

    if-lez v10, :cond_7

    if-gtz v11, :cond_8

    .line 4131
    :cond_7
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate0 : invalid param : cropWidth="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", cropHeight="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", xEnd="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", yEnd="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4132
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4135
    :cond_8
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v12, :cond_c

    .line 4138
    move/from16 v7, p7

    .local v7, "i":I
    :goto_1
    if-lt v7, v11, :cond_a

    .line 4163
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v12

    const/4 v13, 0x2

    if-eq v12, v13, :cond_e

    .line 4164
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4139
    :cond_a
    mul-int v8, v7, p4

    .line 4141
    .local v8, "iWidth":I
    move/from16 v9, p6

    .local v9, "j":I
    move v4, v3

    .end local v3    # "cropI":I
    .local v4, "cropI":I
    :goto_2
    if-lt v9, v10, :cond_b

    .line 4138
    add-int/lit8 v7, v7, 0x2

    move v3, v4

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    goto :goto_1

    .line 4142
    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :cond_b
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    add-int v12, v8, v9

    aget-byte v12, p1, v12

    aput-byte v12, p3, v4

    .line 4141
    add-int/lit8 v9, v9, 0x2

    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    goto :goto_2

    .line 4148
    .end local v4    # "cropI":I
    .end local v7    # "i":I
    .end local v8    # "iWidth":I
    .end local v9    # "j":I
    .restart local v3    # "cropI":I
    :cond_c
    move/from16 v7, p7

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v11, :cond_9

    .line 4149
    mul-int v8, v7, p4

    .line 4151
    .restart local v8    # "iWidth":I
    move/from16 v9, p6

    .restart local v9    # "j":I
    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :goto_4
    if-lt v9, v10, :cond_d

    .line 4148
    add-int/lit8 v7, v7, 0x1

    move v3, v4

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    goto :goto_3

    .line 4158
    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :cond_d
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    add-int v12, v8, v9

    aget-byte v12, p1, v12

    aput-byte v12, p3, v4

    .line 4151
    add-int/lit8 v9, v9, 0x1

    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    goto :goto_4

    .line 4168
    .end local v4    # "cropI":I
    .end local v8    # "iWidth":I
    .end local v9    # "j":I
    .restart local v3    # "cropI":I
    :cond_e
    const/4 v12, 0x0

    const/4 v13, 0x0

    mul-int v14, v5, v2

    :try_start_0
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v12, v1, v13, v14}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4175
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 4169
    :catch_0
    move-exception v6

    .line 4170
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 4171
    const-string v12, "OCR"

    const-string v13, "[OCR] ===> OCRByteArrayRotate0 : arraycopy error"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4172
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public OCRByteArrayRotate180([BI[BIIIIII)Z
    .locals 18
    .param p1, "srcBuf"    # [B
    .param p2, "srcBufLength"    # I
    .param p3, "tempBuf"    # [B
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "marginLeft"    # I
    .param p7, "marginTop"    # I
    .param p8, "marginRight"    # I
    .param p9, "marginBottom"    # I

    .prologue
    .line 4003
    const-string v12, "OCR"

    const-string v13, "[OCR] ===> OCRByteArrayRotate180 : OCRByteArrayRotate180"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4009
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p2, :cond_1

    .line 4010
    :cond_0
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate180 : width("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") or height("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") is null"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4011
    const/4 v12, 0x0

    .line 4090
    :goto_0
    return v12

    .line 4014
    :cond_1
    mul-int v12, p4, p5

    move/from16 v0, p2

    if-ge v0, v12, :cond_2

    .line 4015
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate180 : srcBufLength("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") should be bigger than width*height*1.5("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    mul-int v14, p4, p5

    int-to-double v14, v14

    const-wide/high16 v16, 0x3ff8000000000000L    # 1.5

    mul-double v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4016
    const/4 v12, 0x0

    goto :goto_0

    .line 4019
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_4

    .line 4020
    :cond_3
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate180 : srcBuf("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") or tempBuf("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") is null"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4021
    const/4 v12, 0x0

    goto :goto_0

    .line 4024
    :cond_4
    move-object/from16 v0, p1

    array-length v12, v0

    move/from16 v0, p2

    if-lt v12, v0, :cond_5

    move-object/from16 v0, p3

    array-length v12, v0

    move/from16 v0, p2

    if-ge v12, v0, :cond_6

    .line 4025
    :cond_5
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate180 : srcBuf.length("

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") or tempBuf.length("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") should be bigger than srcBufLength("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4026
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4030
    :cond_6
    sub-int v10, p4, p6

    .line 4031
    .local v10, "xEnd":I
    sub-int v11, p5, p7

    .line 4032
    .local v11, "yEnd":I
    sub-int v12, p4, p6

    sub-int v5, v12, p8

    .line 4033
    .local v5, "cropWidth":I
    sub-int v12, p5, p7

    sub-int v2, v12, p9

    .line 4035
    .local v2, "cropHeight":I
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v12, :cond_8

    .line 4036
    mul-int v12, v5, v2

    div-int/lit8 v12, v12, 0x4

    add-int/lit8 v3, v12, -0x1

    .line 4041
    .local v3, "cropI":I
    :goto_1
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    .line 4042
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 4044
    if-lez v5, :cond_7

    if-lez v2, :cond_7

    if-lez v10, :cond_7

    if-gtz v11, :cond_9

    .line 4045
    :cond_7
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[OCR] ===> OCRByteArrayRotate180 : invalid param : cropWidth="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", cropHeight="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", xEnd="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", yEnd="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4046
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4038
    .end local v3    # "cropI":I
    :cond_8
    mul-int v12, v5, v2

    add-int/lit8 v3, v12, -0x1

    .restart local v3    # "cropI":I
    goto :goto_1

    .line 4049
    :cond_9
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v12, :cond_d

    .line 4052
    move/from16 v7, p9

    .local v7, "i":I
    :goto_2
    if-lt v7, v11, :cond_b

    .line 4077
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v12

    const/4 v13, 0x2

    if-eq v12, v13, :cond_f

    .line 4078
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 4053
    :cond_b
    mul-int v8, v7, p4

    .line 4055
    .local v8, "iWidth":I
    move/from16 v9, p8

    .local v9, "j":I
    move v4, v3

    .end local v3    # "cropI":I
    .local v4, "cropI":I
    :goto_3
    if-lt v9, v10, :cond_c

    .line 4052
    add-int/lit8 v7, v7, 0x2

    move v3, v4

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    goto :goto_2

    .line 4056
    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :cond_c
    add-int/lit8 v3, v4, -0x1

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    add-int v12, v8, v9

    aget-byte v12, p1, v12

    aput-byte v12, p3, v4

    .line 4055
    add-int/lit8 v9, v9, 0x2

    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    goto :goto_3

    .line 4062
    .end local v4    # "cropI":I
    .end local v7    # "i":I
    .end local v8    # "iWidth":I
    .end local v9    # "j":I
    .restart local v3    # "cropI":I
    :cond_d
    move/from16 v7, p9

    .restart local v7    # "i":I
    :goto_4
    if-ge v7, v11, :cond_a

    .line 4063
    mul-int v8, v7, p4

    .line 4065
    .restart local v8    # "iWidth":I
    move/from16 v9, p8

    .restart local v9    # "j":I
    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :goto_5
    if-lt v9, v10, :cond_e

    .line 4062
    add-int/lit8 v7, v7, 0x1

    move v3, v4

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    goto :goto_4

    .line 4072
    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    :cond_e
    add-int/lit8 v3, v4, -0x1

    .end local v4    # "cropI":I
    .restart local v3    # "cropI":I
    add-int v12, v8, v9

    aget-byte v12, p1, v12

    aput-byte v12, p3, v4

    .line 4065
    add-int/lit8 v9, v9, 0x1

    move v4, v3

    .end local v3    # "cropI":I
    .restart local v4    # "cropI":I
    goto :goto_5

    .line 4082
    .end local v4    # "cropI":I
    .end local v8    # "iWidth":I
    .end local v9    # "j":I
    .restart local v3    # "cropI":I
    :cond_f
    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 4083
    :try_start_0
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v12, :cond_10

    mul-int v12, v5, v2

    div-int/lit8 v12, v12, 0x4

    .line 4082
    :goto_6
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v13, v1, v14, v12}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4090
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 4083
    :cond_10
    mul-int v12, v5, v2

    goto :goto_6

    .line 4084
    :catch_0
    move-exception v6

    .line 4085
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 4086
    const-string v12, "OCR"

    const-string v13, "[OCR] ===> OCRByteArrayRotate180 : arraycopy error"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4087
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public OCRByteArrayRotate270([BI[BIIIIII)Z
    .locals 20
    .param p1, "srcBuf"    # [B
    .param p2, "srcBufLength"    # I
    .param p3, "tempBuf"    # [B
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "marginLeft"    # I
    .param p7, "marginTop"    # I
    .param p8, "marginRight"    # I
    .param p9, "marginBottom"    # I

    .prologue
    .line 3914
    const-string v13, "OCR"

    const-string v14, "[OCR] OCRByteArrayRotate270 : OCRByteArrayRotate270"

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3919
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p2, :cond_1

    .line 3920
    :cond_0
    const-string v13, "OCR"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "[OCR] ===> OCRByteArrayRotate270 : width("

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") or height("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p5

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is null"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3921
    const/4 v13, 0x0

    .line 3998
    :goto_0
    return v13

    .line 3924
    :cond_1
    mul-int v13, p4, p5

    move/from16 v0, p2

    if-ge v0, v13, :cond_2

    .line 3925
    const-string v13, "OCR"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "[OCR] ===> OCRByteArrayRotate270 : srcBufLength("

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") should be bigger than width*height*1.5("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    mul-int v15, p4, p5

    int-to-double v0, v15

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x3ff8000000000000L    # 1.5

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3926
    const/4 v13, 0x0

    goto :goto_0

    .line 3929
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_4

    .line 3930
    :cond_3
    const-string v13, "OCR"

    const-string v14, "[OCR] ===> OCRByteArrayRotate270 : srcBuf or tempBuf is null"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3931
    const/4 v13, 0x0

    goto :goto_0

    .line 3934
    :cond_4
    move-object/from16 v0, p1

    array-length v13, v0

    move/from16 v0, p2

    if-lt v13, v0, :cond_5

    move-object/from16 v0, p3

    array-length v13, v0

    move/from16 v0, p2

    if-ge v13, v0, :cond_6

    .line 3935
    :cond_5
    const-string v13, "OCR"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "[OCR] ===> OCRByteArrayRotate270 : srcBuf.length("

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") or tempBuf.length("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    array-length v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") should be bigger than srcBufLength("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3936
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 3940
    :cond_6
    sub-int v11, p4, p7

    .line 3941
    .local v11, "xEnd":I
    sub-int v12, p5, p8

    .line 3942
    .local v12, "yEnd":I
    sub-int v13, p4, p7

    sub-int v3, v13, p9

    .line 3943
    .local v3, "cropWidth":I
    sub-int v13, p5, p6

    sub-int v2, v13, p8

    .line 3944
    .local v2, "cropHeight":I
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    .line 3945
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 3947
    if-lez v3, :cond_7

    if-lez v2, :cond_7

    if-lez v11, :cond_7

    if-gtz v12, :cond_8

    .line 3948
    :cond_7
    const-string v13, "OCR"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "[OCR] ===> OCRByteArrayRotate270 : invalid param : cropWidth="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", cropHeight="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", xEnd="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", yEnd="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3949
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 3952
    :cond_8
    sget-boolean v13, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v13, :cond_c

    .line 3955
    move/from16 v7, p6

    .local v7, "i":I
    :goto_1
    if-lt v7, v12, :cond_a

    .line 3980
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v13

    const/4 v14, 0x2

    if-eq v13, v14, :cond_e

    .line 3981
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 3956
    :cond_a
    mul-int/lit8 v13, p9, -0x1

    add-int/lit8 v4, v13, 0x1

    .line 3957
    .local v4, "d1":I
    rsub-int/lit8 v13, v7, -0x1

    add-int v5, v13, p8

    .line 3959
    .local v5, "d2":I
    move/from16 v8, p9

    .local v8, "j":I
    :goto_2
    if-lt v8, v11, :cond_b

    .line 3955
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    .line 3960
    :cond_b
    add-int v13, v8, v4

    mul-int/2addr v13, v2

    add-int/2addr v13, v5

    div-int/lit8 v13, v13, 0x4

    mul-int v14, v7, p4

    add-int/2addr v14, v8

    aget-byte v14, p1, v14

    aput-byte v14, p3, v13

    .line 3959
    add-int/lit8 v8, v8, 0x2

    goto :goto_2

    .line 3967
    .end local v4    # "d1":I
    .end local v5    # "d2":I
    .end local v7    # "i":I
    .end local v8    # "j":I
    :cond_c
    move/from16 v7, p6

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v12, :cond_9

    .line 3969
    mul-int/lit8 v13, p9, -0x1

    add-int/lit8 v4, v13, 0x1

    .line 3970
    .restart local v4    # "d1":I
    rsub-int/lit8 v13, v7, -0x1

    add-int v5, v13, p8

    .line 3972
    .restart local v5    # "d2":I
    move/from16 v8, p9

    .restart local v8    # "j":I
    :goto_4
    if-lt v8, v11, :cond_d

    .line 3967
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 3974
    :cond_d
    add-int v13, v8, v4

    mul-int/2addr v13, v2

    add-int/2addr v13, v5

    mul-int v14, v7, p4

    add-int/2addr v14, v8

    aget-byte v14, p1, v14

    aput-byte v14, p3, v13

    .line 3972
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 3985
    .end local v4    # "d1":I
    .end local v5    # "d2":I
    .end local v8    # "j":I
    :cond_e
    mul-int v9, v3, v2

    .line 3986
    .local v9, "k":I
    const/4 v7, 0x0

    move v10, v9

    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    mul-int v13, v3, v2

    if-lt v7, v13, :cond_f

    .line 3998
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 3987
    :cond_f
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    :try_start_0
    aget-byte v13, p3, v10

    aput-byte v13, p1, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3986
    add-int/lit8 v7, v7, 0x1

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 3992
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :catch_0
    move-exception v6

    .line 3993
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 3994
    const-string v13, "OCR"

    const-string v14, "[OCR] ===> OCRByteArrayRotate270 : arraycopy error"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3995
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public OCRByteArrayRotate90([BI[BIIIIII)Z
    .locals 18
    .param p1, "srcBuf"    # [B
    .param p2, "srcBufLength"    # I
    .param p3, "tempBuf"    # [B
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "marginLeft"    # I
    .param p7, "marginTop"    # I
    .param p8, "marginRight"    # I
    .param p9, "marginBottom"    # I

    .prologue
    .line 3826
    const-string v11, "OCR"

    const-string v12, "[OCR] OCRByteArrayRotate90 : OCRByteArrayRotate90"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3831
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p2, :cond_1

    .line 3832
    :cond_0
    const-string v11, "OCR"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[OCR] ===> OCRByteArrayRotate90 : width("

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") or height("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p5

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") is null"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3833
    const/4 v11, 0x0

    .line 3910
    :goto_0
    return v11

    .line 3836
    :cond_1
    mul-int v11, p4, p5

    move/from16 v0, p2

    if-ge v0, v11, :cond_2

    .line 3837
    const-string v11, "OCR"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[OCR] ===> OCRByteArrayRotate90 : srcBufLength("

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") should be bigger than width*height*1.5("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    mul-int v13, p4, p5

    int-to-double v14, v13

    const-wide/high16 v16, 0x3ff8000000000000L    # 1.5

    mul-double v14, v14, v16

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3838
    const/4 v11, 0x0

    goto :goto_0

    .line 3841
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_4

    .line 3842
    :cond_3
    const-string v11, "OCR"

    const-string v12, "[OCR] ===> OCRByteArrayRotate90 : srcBuf or tempBuf is null"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3843
    const/4 v11, 0x0

    goto :goto_0

    .line 3846
    :cond_4
    move-object/from16 v0, p1

    array-length v11, v0

    move/from16 v0, p2

    if-lt v11, v0, :cond_5

    move-object/from16 v0, p3

    array-length v11, v0

    move/from16 v0, p2

    if-ge v11, v0, :cond_6

    .line 3847
    :cond_5
    const-string v11, "OCR"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[OCR] ===> OCRByteArrayRotate90 : srcBuf.length("

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v13, v0

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") or tempBuf.length("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    array-length v13, v0

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") should be bigger than srcBufLength("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3848
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 3852
    :cond_6
    sub-int v9, p4, p9

    .line 3853
    .local v9, "xEnd":I
    sub-int v10, p5, p6

    .line 3854
    .local v10, "yEnd":I
    sub-int v11, p4, p7

    sub-int v3, v11, p9

    .line 3855
    .local v3, "cropWidth":I
    sub-int v11, p5, p6

    sub-int v2, v11, p8

    .line 3856
    .local v2, "cropHeight":I
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    .line 3857
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 3859
    if-lez v3, :cond_7

    if-lez v2, :cond_7

    if-lez v9, :cond_7

    if-gtz v10, :cond_8

    .line 3860
    :cond_7
    const-string v11, "OCR"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[OCR] ===> OCRByteArrayRotate90 : invalid param : cropWidth="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", cropHeight="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", xEnd="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", yEnd="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3861
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 3864
    :cond_8
    sget-boolean v11, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v11, :cond_c

    .line 3867
    move/from16 v7, p8

    .local v7, "i":I
    :goto_1
    if-lt v7, v10, :cond_a

    .line 3897
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v11

    const/4 v12, 0x2

    if-eq v11, v12, :cond_e

    .line 3898
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 3868
    :cond_a
    mul-int/lit8 v11, p7, -0x1

    add-int/lit8 v4, v11, 0x1

    .line 3869
    .local v4, "d1":I
    rsub-int/lit8 v11, v7, -0x1

    add-int v5, v11, p6

    .line 3871
    .local v5, "d2":I
    move/from16 v8, p7

    .local v8, "j":I
    :goto_2
    if-lt v8, v9, :cond_b

    .line 3867
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    .line 3872
    :cond_b
    add-int v11, v8, v4

    mul-int/2addr v11, v2

    add-int/2addr v11, v5

    div-int/lit8 v11, v11, 0x4

    mul-int v12, v7, p4

    add-int/2addr v12, v8

    aget-byte v12, p1, v12

    aput-byte v12, p3, v11

    .line 3871
    add-int/lit8 v8, v8, 0x2

    goto :goto_2

    .line 3879
    .end local v4    # "d1":I
    .end local v5    # "d2":I
    .end local v7    # "i":I
    .end local v8    # "j":I
    :cond_c
    move/from16 v7, p8

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v10, :cond_9

    .line 3881
    mul-int/lit8 v11, p7, -0x1

    add-int/lit8 v4, v11, 0x1

    .line 3882
    .restart local v4    # "d1":I
    rsub-int/lit8 v11, v7, -0x1

    add-int v5, v11, p6

    .line 3884
    .restart local v5    # "d2":I
    move/from16 v8, p7

    .restart local v8    # "j":I
    :goto_4
    if-lt v8, v9, :cond_d

    .line 3879
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 3892
    :cond_d
    add-int v11, v8, v4

    mul-int/2addr v11, v2

    add-int/2addr v11, v5

    mul-int v12, v7, p4

    add-int/2addr v12, v8

    aget-byte v12, p1, v12

    aput-byte v12, p3, v11

    .line 3884
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 3902
    .end local v4    # "d1":I
    .end local v5    # "d2":I
    .end local v8    # "j":I
    :cond_e
    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 3903
    :try_start_0
    sget-boolean v11, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v11, :cond_f

    mul-int v11, v3, v2

    div-int/lit8 v11, v11, 0x4

    .line 3902
    :goto_5
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v12, v1, v13, v11}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3910
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 3903
    :cond_f
    mul-int v11, v3, v2

    goto :goto_5

    .line 3904
    :catch_0
    move-exception v6

    .line 3905
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 3906
    const-string v11, "OCR"

    const-string v12, "[OCR] ===> OCRByteArrayRotate90 : arraycopy error"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3907
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method public OCRVoiceIndicator(IZ)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "visible"    # Z

    .prologue
    const/4 v2, 0x0

    .line 6190
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 6191
    const-string v0, "OCR"

    const-string v1, "OCRVoiceIndicator, VoiceIndicator NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6208
    :cond_0
    :goto_0
    return-void

    .line 6195
    :cond_1
    if-nez p2, :cond_2

    .line 6196
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    const v1, 0x7f020035

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 6197
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 6199
    :cond_2
    if-nez p1, :cond_3

    .line 6200
    const-string v0, "OCR"

    const-string v1, "######VR OCRVoiceIndicator, VOICE_STANDBY "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6201
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    const v1, 0x7f020036

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 6202
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewVoiceControl:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 6203
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 6204
    const-string v0, "OCR"

    const-string v1, "######VR OCRVoiceIndicator, VOICE_FAIL "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public ReplySIPOCRResult()V
    .locals 4

    .prologue
    .line 5120
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bcocr.OCR_SIP_TEXT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5121
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "OCR_SIP_TYPE"

    sget v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5122
    const-string v1, "OCR_SIP_TEXT"

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5123
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR]ReplySIPOCRResult  : type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mOCRSIPText : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5124
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 5125
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 5126
    return-void
.end method

.method public appendOCRDataOnExif(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 5745
    const-string v1, "OCR"

    const-string v2, "appendOCRDataOnExif"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5746
    new-instance v0, Lcom/sec/android/app/bcocr/OCRExifAppender;

    invoke-direct {v0, p1}, Lcom/sec/android/app/bcocr/OCRExifAppender;-><init>(Ljava/lang/String;)V

    .line 5747
    .local v0, "image":Lcom/sec/android/app/bcocr/OCRExifAppender;
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRExifAppender;->ready()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5748
    sget v1, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-eqz v1, :cond_1

    .line 5749
    sget v1, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/OCRExifAppender;->append(I[Ljava/lang/String;)V

    .line 5753
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRExifAppender;->finish()V

    .line 5757
    :goto_1
    if-eqz v0, :cond_0

    .line 5758
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRExifAppender;->clear()V

    .line 5760
    :cond_0
    return-void

    .line 5751
    :cond_1
    sget v1, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/OCRExifAppender;->append(I[Ljava/lang/String;)V

    goto :goto_0

    .line 5755
    :cond_2
    const-string v1, "OCR"

    const-string v2, "[OCR] ImageTextSearch : Jpeg read fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cancelAutoFocus()V
    .locals 1

    .prologue
    .line 2888
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 2895
    :goto_0
    return-void

    .line 2892
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 2893
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2894
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    goto :goto_0
.end method

.method public cancelTouchAutoFocus()V
    .locals 1

    .prologue
    .line 2861
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_0

    .line 2862
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_1

    .line 2869
    :cond_0
    :goto_0
    return-void

    .line 2866
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->stopLongTouchAutoFocus()V

    .line 2867
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    goto :goto_0
.end method

.method protected checkCameraDuringCall()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5715
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Camera_EnableCameraDuringCall"

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5716
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCR;->mEnableDuringCall:Z

    .line 5718
    new-instance v0, Lcom/sec/android/app/bcocr/CscParser;

    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/sec/android/app/bcocr/CscParser;-><init>(Ljava/lang/String;)V

    .line 5719
    .local v0, "parser":Lcom/sec/android/app/bcocr/CscParser;
    if-eqz v0, :cond_0

    const-string v2, "Settings.Multimedia"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 5734
    .end local v0    # "parser":Lcom/sec/android/app/bcocr/CscParser;
    :cond_0
    :goto_0
    return-void

    .line 5723
    .restart local v0    # "parser":Lcom/sec/android/app/bcocr/CscParser;
    :cond_1
    const-string v2, "Settings.Multimedia.Camera.ShutterSound"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5724
    .local v1, "strValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 5728
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "on"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5729
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mEnableDuringCall:Z

    goto :goto_0

    .line 5732
    .end local v0    # "parser":Lcom/sec/android/app/bcocr/CscParser;
    .end local v1    # "strValue":Ljava/lang/String;
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mEnableDuringCall:Z

    goto :goto_0
.end method

.method public checkCameraStartCondition_Call()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 5682
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z

    .line 5685
    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    .line 5687
    .local v1, "phoneServ":Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_0

    .line 5688
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z

    .line 5692
    iget-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z

    if-eqz v3, :cond_0

    .line 5693
    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5701
    .end local v1    # "phoneServ":Lcom/android/internal/telephony/ITelephony;
    :cond_0
    :goto_0
    return v2

    .line 5697
    :catch_0
    move-exception v0

    .line 5698
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "OCR"

    const-string v4, "phoneServ.isOffhook() or phoneServ.isVoIPIdle() failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public checkCameraStartCondition_Security()Z
    .locals 2

    .prologue
    .line 5706
    const-string v1, "persist.sys.camera_lock"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5707
    .local v0, "dev_camera_lock_state":Ljava/lang/String;
    const-string v1, "camera_lock.enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5708
    const/4 v1, 0x1

    .line 5711
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public checkCameraStartCondition_VT()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5623
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVTCalling:Z

    .line 5642
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 5643
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    .line 5644
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isVideoCall()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVTCalling:Z

    .line 5645
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mCheckVTCalling:Z

    .line 5648
    :cond_0
    return v1
.end method

.method public checkSideSyncConnected()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6476
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 6479
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public checkStorage(ZZ)V
    .locals 2
    .param p1, "bMediaStorage"    # Z
    .param p2, "bBroadcastReceiver"    # Z

    .prologue
    .line 4603
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 4604
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow()V

    .line 4607
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->mediaStorageDialog()V

    .line 4608
    return-void
.end method

.method public checkStorageLow()V
    .locals 6

    .prologue
    .line 4587
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getAvailableStorage()J

    move-result-wide v0

    .line 4589
    .local v0, "lAvailableStorage":J
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 4590
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    .line 4600
    :goto_0
    return-void

    .line 4595
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraQuality()I

    move-result v3

    .line 4594
    invoke-static {v2, v3}, Lcom/sec/android/app/bcocr/CheckMemory;->getMaxSizeOfImage(II)J

    move-result-wide v2

    sub-long v2, v0, v2

    .line 4595
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 4596
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    goto :goto_0

    .line 4598
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    goto :goto_0
.end method

.method public doMacroFocus(J)Z
    .locals 11
    .param p1, "recognizedTime"    # J

    .prologue
    .line 5457
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v6, :cond_0

    .line 5458
    const/4 v6, 0x0

    .line 5520
    :goto_0
    return v6

    .line 5461
    :cond_0
    sget-boolean v6, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    if-eqz v6, :cond_6

    .line 5462
    const-string v6, "OCR"

    const-string v7, "[OCR] check camera focus ADV_FOCUS/ doMacroFocus"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5463
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/OCREngine;->getCAFStartTime()J

    move-result-wide v6

    sub-long v2, p1, v6

    .line 5465
    .local v2, "lTimeDiff":J
    iget-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 5466
    iput-wide p1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    .line 5469
    :cond_1
    iget-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    sub-long v4, p1, v6

    .line 5471
    .local v4, "lTimeDiff2":J
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[OCR] check camera focus ADV_FOCUS/ doMacroFocus time diff1="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ,diff2="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5472
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/OCREngine;->getCAFStartTime()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 5473
    sget-wide v6, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_UNRECOG_TIME:J

    cmp-long v6, v4, v6

    if-gez v6, :cond_2

    .line 5474
    const-string v6, "OCR"

    const-string v7, "[OCR] check camera focus : ADV_FOCUS/  : skip (unrecog_time)"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5475
    const/4 v6, 0x0

    goto :goto_0

    .line 5479
    :cond_2
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    .line 5480
    const-string v6, "OCR"

    const-string v7, "[OCR] check camera focus ADV_FOCUS/ doMacroFocus ###S"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5482
    sget-wide v6, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_RESTART_TIME:J

    cmp-long v6, v2, v6

    if-lez v6, :cond_6

    .line 5483
    const-string v6, "OCR"

    const-string v7, "[OCR] check camera focus ADV_FOCUS/ check"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5484
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v6

    if-nez v6, :cond_5

    .line 5485
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isTouchAutoFocusEnabled()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 5486
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 5487
    sget-wide v0, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_RESTART_DELAY_TIME_CAPTUREMODE:J

    .line 5489
    .local v0, "delaytime":J
    iget-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_3

    .line 5490
    iput-wide p1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    .line 5491
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[OCR] check camera focus : restart MacroFocus ### : skip (delay) : 0 < "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v8, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_RESTART_DELAY_TIME:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5492
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 5493
    :cond_3
    iget-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    sub-long v6, p1, v6

    cmp-long v6, v6, v0

    if-gez v6, :cond_4

    .line 5494
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[OCR] check camera focus : restart MacroFocus ### : skip (delay) : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    sub-long v8, p1, v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " < "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5495
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 5498
    :cond_4
    const-string v6, "OCR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[OCR] check camera focus : restart MacroFocus ### : start : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    sub-long v8, p1, v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5500
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const-wide/16 v8, -0x1

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    .line 5502
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 5503
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->setAdvancedMacroFocusActive(Z)V

    .line 5504
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->handleMacroFocus(Z)V

    .line 5505
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 5506
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 5507
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    .line 5508
    const/4 v6, 0x1

    const/16 v7, 0x8

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    .line 5509
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusRestart_DelayTime:J

    .line 5510
    const-string v6, "OCR"

    const-string v7, "[OCR] check camera focus ADV_FOCUS/ doMacroFocus done!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5515
    .end local v0    # "delaytime":J
    :cond_5
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 5520
    .end local v2    # "lTimeDiff":J
    .end local v4    # "lTimeDiff2":J
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method public fitEdgeCueForNameCard()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 6402
    const/4 v1, 0x0

    .line 6403
    .local v1, "iconWidth":I
    const/4 v0, 0x0

    .line 6405
    .local v0, "iconHeight":I
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeCue:Landroid/widget/RelativeLayout;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    .line 6406
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    .line 6407
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    .line 6408
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090034

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v0, v6

    .line 6410
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeCue:Landroid/widget/RelativeLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 6412
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    .line 6413
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 6412
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6414
    .local v3, "paramEdgeLeftTop":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v6, v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6415
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6418
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    .line 6419
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 6418
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6420
    .local v5, "paramEdgeRightTop":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v1

    .line 6421
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->top:I

    .line 6420
    invoke-virtual {v5, v6, v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6422
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6425
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    .line 6426
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 6425
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6427
    .local v2, "paramEdgeLeftBottom":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    .line 6428
    sub-int/2addr v7, v0

    .line 6427
    invoke-virtual {v2, v6, v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6430
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6433
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    .line 6434
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 6433
    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6435
    .local v4, "paramEdgeRightBottom":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v1

    .line 6436
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->getDetectedRect()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v0

    .line 6435
    invoke-virtual {v4, v6, v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6438
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6440
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftTop:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6441
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightTop:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6442
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeLeftBottom:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6443
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mEdgeRightBottom:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6444
    return-void
.end method

.method public getAdvancedMacroFocusActive()Z
    .locals 1

    .prologue
    .line 3043
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    if-eqz v0, :cond_0

    .line 3044
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAdvancedMacroFocusActive:Z

    .line 3046
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCaptureStates()I
    .locals 1

    .prologue
    .line 5562
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureStates:I

    return v0
.end method

.method public getChkSideMenuItemsShow()Z
    .locals 1

    .prologue
    .line 4635
    const/4 v0, 0x0

    return v0
.end method

.method public getExternalCacheDir()Ljava/io/File;
    .locals 1

    .prologue
    .line 4755
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 4760
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOCRActionState()I
    .locals 1

    .prologue
    .line 4801
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRActionState:I

    return v0
.end method

.method public getOCRTRLineString(I)Ljava/lang/String;
    .locals 6
    .param p1, "trIndex"    # I

    .prologue
    .line 5141
    const-string v2, ""

    .line 5142
    .local v2, "lineString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 5143
    .local v1, "lineIndex":I
    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    if-eqz v3, :cond_0

    .line 5144
    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    array-length v3, v3

    if-lt p1, v3, :cond_1

    .line 5145
    :cond_0
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR]getOCRTRLineString : invalid param : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5146
    const-string v3, ""

    .line 5156
    :goto_0
    return-object v3

    .line 5148
    :cond_1
    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v1, v3, p1

    .line 5149
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR]getOCRTRLineString : line index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    array-length v3, v3

    if-lt v0, v3, :cond_2

    .line 5155
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR]getOCRTRLineString : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 5156
    goto :goto_0

    .line 5151
    :cond_2
    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v3, v3, v0

    if-ne v3, v1, :cond_3

    .line 5152
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5150
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getRemainStorage()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 4566
    const/4 v0, 0x0

    .line 4567
    .local v0, "nRemainCount":I
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-eq v1, v4, :cond_0

    .line 4568
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow()V

    .line 4569
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-nez v1, :cond_0

    .line 4571
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v1

    .line 4572
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v2

    .line 4573
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraQuality()I

    move-result v3

    .line 4570
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/bcocr/CheckMemory;->getRemainCount(III)I

    move-result v0

    .line 4574
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 4575
    iput v4, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    .line 4579
    :cond_0
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-eqz v1, :cond_1

    .line 4580
    const/4 v0, 0x0

    .line 4582
    :cond_1
    return v0
.end method

.method public getTRLangByID(I)Ljava/lang/String;
    .locals 5
    .param p1, "langID"    # I

    .prologue
    .line 1282
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 1283
    .local v0, "dict_id":[I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 1285
    .local v1, "dict_string":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_1

    .line 1290
    :cond_0
    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 1291
    aget-object v3, v1, v2

    .line 1293
    :goto_1
    return-object v3

    .line 1286
    :cond_1
    aget v3, v0, v2

    if-eq v3, p1, :cond_0

    .line 1285
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1293
    :cond_2
    const-string v3, "UnKnown Language"

    goto :goto_1
.end method

.method public getTextRemovedSpecialChar(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "orinStr"    # Ljava/lang/String;

    .prologue
    .line 5129
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 5130
    :cond_0
    const-string v1, "OCR"

    const-string v2, "getTextRemovedSpecialChar:orinStr is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5131
    const-string v0, ""

    .line 5137
    :goto_0
    return-object v0

    .line 5133
    :cond_1
    const-string v0, ""

    .line 5136
    .local v0, "outputString":Ljava/lang/String;
    const-string v1, "[^0-9]"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5137
    goto :goto_0
.end method

.method public getTouchAutoFocusActive()Z
    .locals 1

    .prologue
    .line 3061
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_0

    .line 3062
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mTouchAutoFocusActive:Z

    .line 3064
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 6284
    const/4 v2, -0x1

    .line 6286
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 6287
    const-string v4, "com.samsung.android.providers.context"

    .line 6288
    const/16 v5, 0x80

    .line 6287
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 6289
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6293
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 6290
    :catch_0
    move-exception v0

    .line 6291
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "OCR"

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getnOCRNameCardCaptureModeType()Z
    .locals 1

    .prologue
    .line 5764
    sget-boolean v0, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    return v0
.end method

.method protected handleFlashModeChanged(I)V
    .locals 3
    .param p1, "flashMode"    # I

    .prologue
    .line 1322
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "flashIcon / handleFlashModeChanged, flashMode = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    if-nez p1, :cond_2

    .line 1329
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    const v1, 0x7f020031

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    .line 1331
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    const v1, 0x7f020032

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public handleFuncModeChanged(I)V
    .locals 6
    .param p1, "funcMode"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3475
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] handleFuncModeChanged: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3477
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 3478
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 3481
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 3482
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 3484
    packed-switch p1, :pswitch_data_0

    .line 3523
    :goto_0
    return-void

    .line 3486
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3487
    invoke-static {v3}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3488
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    .line 3489
    invoke-direct {p0, v4}, Lcom/sec/android/app/bcocr/OCR;->setImmutableView(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 3491
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3492
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 3493
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->setOCRPreviewDetectStart()V

    goto :goto_0

    .line 3495
    :cond_1
    const/16 v0, 0x9

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_0

    .line 3501
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3502
    invoke-static {v3}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3503
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    .line 3504
    invoke-direct {p0, v4}, Lcom/sec/android/app/bcocr/OCR;->setImmutableView(Lcom/sec/android/app/bcocr/MenuBase;)V

    goto :goto_0

    .line 3508
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3509
    invoke-static {v3}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3510
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    .line 3511
    invoke-direct {p0, v4}, Lcom/sec/android/app/bcocr/OCR;->setImmutableView(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 3513
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3514
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 3515
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->setOCRPreviewDetectStart()V

    goto :goto_0

    .line 3517
    :cond_2
    const/16 v0, 0xa

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_0

    .line 3484
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public handleMacroFocus(Z)V
    .locals 14
    .param p1, "SendEvent"    # Z

    .prologue
    .line 3162
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090030

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v7, v12

    .line 3163
    .local v7, "focus_x":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090031

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v8, v12

    .line 3165
    .local v8, "focus_y":I
    const-string v12, "OCR"

    const-string v13, "[OCR] handleTouchAutoFocusEvent"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3166
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v12, :cond_1

    .line 3167
    const-string v12, "OCR"

    const-string v13, "[OCR] handleTouchAutoFocusEvent:OCR engine is null:return"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3239
    :cond_0
    :goto_0
    return-void

    .line 3171
    :cond_1
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v12

    if-nez v12, :cond_2

    .line 3172
    const-string v12, "OCR"

    const-string v13, "[OCR] handleTouchAutoFocusEvent:it is not preview state:return"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3176
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isAutoFocusing()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 3177
    const-string v12, "OCR"

    const-string v13, "[OCR] handleTouchAutoFocusEvent:focusing state:return"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3181
    :cond_3
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v12, :cond_0

    .line 3184
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FIXED_CENTER_TOUCH_AF:Z

    if-eqz v12, :cond_7

    .line 3185
    move v1, v7

    .line 3186
    .local v1, "PtX":I
    move v2, v8

    .line 3192
    .local v2, "PtY":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090003

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    div-int/lit8 v4, v12, 0x2

    .line 3193
    .local v4, "allowLeftMargin":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090003

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    div-int/lit8 v5, v12, 0x2

    .line 3194
    .local v5, "allowTopMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getLeft()I

    move-result v9

    .line 3195
    .local v9, "leftMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getRight()I

    move-result v10

    .line 3196
    .local v10, "rightMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getTop()I

    move-result v11

    .line 3197
    .local v11, "topMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getBottom()I

    move-result v6

    .line 3199
    .local v6, "bottomMargin":I
    if-lt v1, v9, :cond_0

    if-gt v1, v10, :cond_0

    .line 3200
    add-int v12, v9, v4

    if-gt v1, v12, :cond_8

    .line 3201
    add-int v1, v9, v4

    .line 3206
    :cond_4
    :goto_2
    add-int v12, v11, v5

    if-gt v2, v12, :cond_9

    .line 3207
    add-int v2, v11, v5

    .line 3213
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v12

    invoke-static {v12}, Lcom/sec/android/app/bcocr/CameraResolution;->isWideResolution(I)Z

    move-result v12

    if-nez v12, :cond_a

    .line 3218
    sub-int v0, v1, v9

    .line 3220
    .local v0, "NormalPtX":I
    if-eqz p1, :cond_6

    .line 3221
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12, v0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->setTouchFocusPosition(II)V

    .line 3235
    .end local v0    # "NormalPtX":I
    :cond_6
    :goto_4
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->startTouchAutoFocus()V

    .line 3236
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    goto/16 :goto_0

    .line 3188
    .end local v1    # "PtX":I
    .end local v2    # "PtY":I
    .end local v4    # "allowLeftMargin":I
    .end local v5    # "allowTopMargin":I
    .end local v6    # "bottomMargin":I
    .end local v9    # "leftMargin":I
    .end local v10    # "rightMargin":I
    .end local v11    # "topMargin":I
    :cond_7
    const/4 v1, 0x0

    .line 3189
    .restart local v1    # "PtX":I
    const/4 v2, 0x0

    .restart local v2    # "PtY":I
    goto :goto_1

    .line 3202
    .restart local v4    # "allowLeftMargin":I
    .restart local v5    # "allowTopMargin":I
    .restart local v6    # "bottomMargin":I
    .restart local v9    # "leftMargin":I
    .restart local v10    # "rightMargin":I
    .restart local v11    # "topMargin":I
    :cond_8
    sub-int v12, v10, v4

    if-lt v1, v12, :cond_4

    .line 3203
    sub-int v1, v10, v4

    goto :goto_2

    .line 3208
    :cond_9
    sub-int v12, v6, v5

    if-lt v2, v12, :cond_5

    .line 3209
    sub-int v2, v6, v5

    goto :goto_3

    .line 3228
    :cond_a
    sub-int v3, v2, v11

    .line 3230
    .local v3, "WidePtY":I
    if-eqz p1, :cond_6

    .line 3231
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12, v1, v3}, Lcom/sec/android/app/bcocr/OCREngine;->setTouchFocusPosition(II)V

    goto :goto_4
.end method

.method public handleShootingModeChanged(I)V
    .locals 5
    .param p1, "shootingMode"    # I

    .prologue
    const/4 v4, 0x1

    .line 3396
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] handleShootingModeChanged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3398
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStopPreview()V

    .line 3400
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 3401
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 3404
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 3405
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 3407
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->showShootingMenu(I)V

    .line 3411
    packed-switch p1, :pswitch_data_0

    .line 3419
    sget-object v1, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    .line 3423
    .local v0, "resolutionId":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1, v4, p1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleChangeParameter(II)V

    .line 3424
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraResolution(I)Z

    .line 3428
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartPreview()V

    .line 3429
    return-void

    .line 3413
    .end local v0    # "resolutionId":I
    :pswitch_0
    sget v1, Lcom/sec/android/app/bcocr/OCRSettings;->DEFAULT_ZOOM_VALUE:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->setZoomValue(I)V

    .line 3414
    sget-object v1, Lcom/sec/android/app/bcocr/Feature;->PANORAMA_RESOLUTION:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v0

    .line 3415
    .restart local v0    # "resolutionId":I
    goto :goto_0

    .line 3411
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public handleTouchAutoFocusEvent(Landroid/view/MotionEvent;Z)V
    .locals 15
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "SendEvent"    # Z

    .prologue
    .line 3083
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090030

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v7, v12

    .line 3084
    .local v7, "focus_x":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090031

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v8, v12

    .line 3086
    .local v8, "focus_y":I
    const-string v12, "OCR"

    const-string v13, "[OCR] handleTouchAutoFocusEvent"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3089
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v12, :cond_1

    .line 3159
    :cond_0
    :goto_0
    return-void

    .line 3093
    :cond_1
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 3097
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v12, :cond_0

    .line 3101
    sget-boolean v12, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FIXED_CENTER_TOUCH_AF:Z

    if-eqz v12, :cond_5

    .line 3102
    move v1, v7

    .line 3103
    .local v1, "PtX":I
    move v2, v8

    .line 3109
    .local v2, "PtY":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090003

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    div-int/lit8 v4, v12, 0x2

    .line 3110
    .local v4, "allowLeftMargin":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090003

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    div-int/lit8 v5, v12, 0x2

    .line 3111
    .local v5, "allowTopMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getLeft()I

    move-result v9

    .line 3112
    .local v9, "leftMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getRight()I

    move-result v10

    .line 3113
    .local v10, "rightMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getTop()I

    move-result v11

    .line 3114
    .local v11, "topMargin":I
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/SurfaceView;->getBottom()I

    move-result v6

    .line 3115
    .local v6, "bottomMargin":I
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[FocusCheck] focus_x:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "focus_y"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3116
    const-string v12, "OCR"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[FocusCheck] allowLeftMargin : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/allowTopMargin : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 3117
    const-string v14, "/leftMargin :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/rightMargin : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/topMargin : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 3118
    const-string v14, "/bottomMargin : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3116
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3121
    if-lt v1, v9, :cond_0

    if-gt v1, v10, :cond_0

    .line 3122
    add-int v12, v9, v4

    if-gt v1, v12, :cond_6

    .line 3123
    add-int v1, v9, v4

    .line 3127
    :cond_2
    :goto_2
    add-int v12, v11, v5

    if-gt v2, v12, :cond_7

    .line 3128
    add-int v2, v11, v5

    .line 3133
    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v12

    invoke-static {v12}, Lcom/sec/android/app/bcocr/CameraResolution;->isWideResolution(I)Z

    move-result v12

    if-nez v12, :cond_8

    .line 3138
    sub-int v0, v1, v9

    .line 3140
    .local v0, "NormalPtX":I
    if-eqz p2, :cond_4

    .line 3141
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12, v0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->setTouchFocusPosition(II)V

    .line 3155
    .end local v0    # "NormalPtX":I
    :cond_4
    :goto_4
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12}, Lcom/sec/android/app/bcocr/OCREngine;->startTouchAutoFocus()V

    .line 3156
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    goto/16 :goto_0

    .line 3105
    .end local v1    # "PtX":I
    .end local v2    # "PtY":I
    .end local v4    # "allowLeftMargin":I
    .end local v5    # "allowTopMargin":I
    .end local v6    # "bottomMargin":I
    .end local v9    # "leftMargin":I
    .end local v10    # "rightMargin":I
    .end local v11    # "topMargin":I
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v1, v12

    .line 3106
    .restart local v1    # "PtX":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v2, v12

    .restart local v2    # "PtY":I
    goto/16 :goto_1

    .line 3124
    .restart local v4    # "allowLeftMargin":I
    .restart local v5    # "allowTopMargin":I
    .restart local v6    # "bottomMargin":I
    .restart local v9    # "leftMargin":I
    .restart local v10    # "rightMargin":I
    .restart local v11    # "topMargin":I
    :cond_6
    sub-int v12, v10, v4

    if-lt v1, v12, :cond_2

    .line 3125
    sub-int v1, v10, v4

    goto :goto_2

    .line 3129
    :cond_7
    sub-int v12, v6, v5

    if-lt v2, v12, :cond_3

    .line 3130
    sub-int v2, v6, v5

    goto :goto_3

    .line 3148
    :cond_8
    sub-int v3, v2, v11

    .line 3150
    .local v3, "WidePtY":I
    if-eqz p2, :cond_4

    .line 3151
    iget-object v12, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v12, v1, v3}, Lcom/sec/android/app/bcocr/OCREngine;->setTouchFocusPosition(II)V

    goto :goto_4
.end method

.method public hideZoomBarAction()V
    .locals 2

    .prologue
    .line 2381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    .line 2383
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2384
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2385
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 2386
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->setOCRPreviewDetectStart()V

    .line 2391
    :cond_0
    :goto_0
    return-void

    .line 2388
    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_0
.end method

.method public initCameraSound()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4211
    const-string v0, "OCR"

    const-string v1, "[OCR] Initialize Camera Sound"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4212
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x3

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v5}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    .line 4213
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPoolId:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f060001

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 4214
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPoolId:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 4215
    return-void
.end method

.method public initCurrentOCREngineLanguage()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3261
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070001

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 3262
    .local v1, "engine_language":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070003

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 3263
    .local v0, "engine_lang_id":[I
    array-length v7, v1

    new-array v7, v7, [Z

    iput-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelect:[Z

    .line 3265
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelect:[Z

    array-length v7, v7

    if-lt v2, v7, :cond_1

    .line 3269
    const/4 v2, 0x0

    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSet:[I

    array-length v7, v7

    if-lt v2, v7, :cond_2

    .line 3273
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/bcocr/OCRSettings;->getEngineSelectedLanguage(Z)Ljava/lang/String;

    move-result-object v4

    .line 3274
    .local v4, "languageSet":Ljava/lang/String;
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, "^"

    invoke-direct {v6, v4, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3275
    .local v6, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    .line 3277
    .local v5, "selectedLanguageNum":I
    const/16 v7, 0x1b

    if-le v5, v7, :cond_0

    .line 3278
    const/16 v5, 0x1b

    .line 3281
    :cond_0
    iput v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREnginelanguageSelectedNum:I

    .line 3283
    const/4 v2, 0x0

    :goto_2
    if-lt v2, v5, :cond_3

    .line 3292
    return-void

    .line 3266
    .end local v4    # "languageSet":Ljava/lang/String;
    .end local v5    # "selectedLanguageNum":I
    .end local v6    # "stringtokenizer":Ljava/util/StringTokenizer;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelect:[Z

    aput-boolean v9, v7, v2

    .line 3265
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3270
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSet:[I

    aput v9, v7, v2

    .line 3269
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3285
    .restart local v4    # "languageSet":Ljava/lang/String;
    .restart local v5    # "selectedLanguageNum":I
    .restart local v6    # "stringtokenizer":Ljava/util/StringTokenizer;
    :cond_3
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 3284
    invoke-static {p0, v7}, Lcom/sec/android/app/bcocr/OCRDicManager;->getOCREngineIndexLanguageString(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    .line 3286
    .local v3, "languageIndex":I
    if-ltz v3, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelect:[Z

    array-length v7, v7

    if-ge v3, v7, :cond_4

    .line 3287
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelect:[Z

    aput-boolean v10, v7, v3

    .line 3288
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSet:[I

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREnginelanguageSelectedNum:I

    aget v9, v0, v3

    aput v9, v7, v8

    .line 3289
    iget v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREnginelanguageSelectedNum:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREnginelanguageSelectedNum:I

    .line 3283
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public initFuncMode(IZ)V
    .locals 1
    .param p1, "funcMode"    # I
    .param p2, "bIsTextFinderMode"    # Z

    .prologue
    const/4 v0, 0x1

    .line 3444
    packed-switch p1, :pswitch_data_0

    .line 3472
    :goto_0
    :pswitch_0
    return-void

    .line 3446
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3447
    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3448
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    goto :goto_0

    .line 3453
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3454
    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3455
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    goto :goto_0

    .line 3460
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->onOCRRealModeChange()V

    .line 3461
    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3462
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    goto :goto_0

    .line 3467
    :pswitch_4
    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionType(I)V

    .line 3468
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v0}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    goto :goto_0

    .line 3444
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method insertLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 6263
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getVersionOfContextProviders()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 6281
    :goto_0
    return-void

    .line 6267
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 6271
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6272
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 6273
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6274
    const-string v4, "feature"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6275
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 6276
    const-string v4, "OCR"

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6277
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 6278
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "OCR"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6279
    const-string v4, "OCR"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isActivityDestoying()Z
    .locals 1

    .prologue
    .line 4685
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    return v0
.end method

.method public isAutoFocusing()Z
    .locals 1

    .prologue
    .line 2853
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 2854
    const/4 v0, 0x0

    .line 2857
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    goto :goto_0
.end method

.method public isBisMicrophoneEnabled()Z
    .locals 1

    .prologue
    .line 6256
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->bisMicrophoneEnabled:Z

    return v0
.end method

.method public isCaptureMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5738
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    if-ne v1, v0, :cond_0

    .line 5741
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCapturing()Z
    .locals 2

    .prologue
    .line 4612
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 4613
    const-string v0, "OCR"

    const-string v1, "[OCR] isCapturing - mOCREngine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4614
    const/4 v0, 0x0

    .line 4616
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    goto :goto_0
.end method

.method public isContinuousAFEnabled()Z
    .locals 1

    .prologue
    .line 4780
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    if-nez v0, :cond_0

    .line 4781
    const/4 v0, 0x0

    .line 4783
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDoneCRSR()Z
    .locals 2

    .prologue
    .line 5576
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getCaptureStates()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getCaptureStates()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 5577
    const/4 v0, 0x1

    .line 5579
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMediaScannerScanning()Z
    .locals 1

    .prologue
    .line 4796
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine;->isMediaScannerScanning(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isPreviewStarted()Z
    .locals 2

    .prologue
    .line 4620
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 4621
    const-string v0, "OCR"

    const-string v1, "[OCR] isPreviewStarted - mOCREngine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4622
    const/4 v0, 0x0

    .line 4624
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isPreviewStarted()Z

    move-result v0

    goto :goto_0
.end method

.method public isRecognizing()Z
    .locals 2

    .prologue
    .line 5566
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    if-nez v0, :cond_0

    .line 5567
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getCaptureStates()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getCaptureStates()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 5568
    const/4 v0, 0x1

    .line 5571
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isRmsConnected()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 5660
    const/4 v8, 0x0

    .line 5661
    .local v8, "status":I
    const/4 v6, 0x0

    .line 5663
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5664
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 5665
    const-string v1, "content://com.lguplus.rms/service"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 5666
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5667
    const-string v1, "connected"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 5673
    :cond_0
    if-eqz v6, :cond_1

    .line 5674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5675
    const/4 v6, 0x0

    .line 5678
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    if-ne v8, v9, :cond_3

    move v1, v9

    :goto_1
    return v1

    .line 5670
    :catch_0
    move-exception v7

    .line 5671
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "RMS"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5673
    if-eqz v6, :cond_1

    .line 5674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5675
    const/4 v6, 0x0

    goto :goto_0

    .line 5672
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 5673
    if-eqz v6, :cond_2

    .line 5674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5675
    const/4 v6, 0x0

    .line 5677
    :cond_2
    throw v1

    .line 5678
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isShutterPressed()Z
    .locals 1

    .prologue
    .line 3243
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    return v0
.end method

.method public isTouchDown()Z
    .locals 1

    .prologue
    .line 4390
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsTouchDown:Z

    return v0
.end method

.method public isVoiceInputSettingOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4787
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "voice_input_control"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 4788
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "voice_input_control_bcr"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 4791
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isZoomAvailable()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3541
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_ZOOM_SUPPORT:Z

    if-nez v1, :cond_1

    .line 3554
    :cond_0
    :goto_0
    return v0

    .line 3545
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 3546
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3547
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    if-nez v1, :cond_3

    .line 3548
    const v1, 0x7f0c000d

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    .line 3550
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNotSupportZoomToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3554
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 14
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 4419
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] --onActivityResult--requestCode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4420
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] --onActivityResult--resultCode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4421
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] --onActivityResult--data: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4422
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 4423
    .local v11, "myExtras":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSaveUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 4424
    if-eqz v11, :cond_0

    .line 4425
    const-string v1, "output"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSaveUri:Landroid/net/Uri;

    .line 4429
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 4543
    :cond_1
    :goto_0
    return-void

    .line 4431
    :sswitch_0
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v1, :cond_1

    .line 4432
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getLastCapturedFileName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 4433
    new-instance v8, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getLastCapturedFileName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4434
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4435
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4436
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    .line 4437
    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 4436
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 4443
    :cond_2
    const-string v1, "android.intent.extra.TEXT"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPText:Ljava/lang/String;

    .line 4444
    new-instance v10, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bcocr.OCR_SIP_TEXT"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4445
    .local v10, "intent":Landroid/content/Intent;
    const-string v1, "OCR_SIP_TYPE"

    sget v3, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4446
    const-string v1, "OCR_SIP_TEXT"

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPText:Ljava/lang/String;

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4449
    invoke-virtual {p0, v10}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 4450
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto :goto_0

    .line 4455
    .end local v8    # "file":Ljava/io/File;
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_1
    const-string v1, "OCR"

    const-string v3, "finish OCR_FROM_NAMECARD_CAPTURE"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4456
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    .line 4457
    const/4 v1, -0x1

    move-object/from16 v0, p3

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->setResult(ILandroid/content/Intent;)V

    .line 4458
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 4459
    .local v12, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "setting_image_auto_save"

    const/4 v3, 0x1

    invoke-interface {v12, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4460
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->deleteOCRTempFile()V

    .line 4462
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto/16 :goto_0

    .line 4466
    .end local v12    # "preferences":Landroid/content/SharedPreferences;
    :sswitch_2
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_8

    if-eqz p3, :cond_8

    .line 4467
    const/4 v2, 0x0

    .line 4468
    .local v2, "mImageCaptureUri":Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_4

    .line 4469
    const-string v1, "OCR"

    const-string v3, "[OCR] Something goes wrong!! Restart attach mode get data NULL"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4472
    :cond_4
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 4473
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4474
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_5

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4475
    :cond_5
    const-string v1, "OCR"

    const-string v3, "[OCR] Something goes wrong!! Restart attach mode."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4476
    if-eqz v7, :cond_1

    .line 4477
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4481
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4482
    const-string v1, "_data"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 4483
    .local v9, "filepath":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 4484
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] PICK_FROM_ALBUM myFile filepath:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4485
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 4486
    .restart local v10    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v10, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 4487
    const-string v1, "FILE_PATH"

    invoke-virtual {v10, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4488
    const-string v1, "WHERE"

    const-string v3, "ONLYFILEPATH"

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4489
    const-string v1, "MODE"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v3

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4490
    sget-boolean v1, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-eqz v1, :cond_7

    .line 4491
    const-string v1, "OCR_NAMECARD_CAPTURE_MODE"

    const/4 v3, 0x1

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4493
    :cond_7
    const-string v1, "SIP_TYPE"

    sget v3, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v10, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4494
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4495
    const/16 v1, 0x3e8

    invoke-virtual {p0, v10, v1}, Lcom/sec/android/app/bcocr/OCR;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 4497
    .end local v2    # "mImageCaptureUri":Landroid/net/Uri;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v9    # "filepath":Ljava/lang/String;
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_8
    if-nez p2, :cond_a

    if-eqz p3, :cond_a

    .line 4498
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4499
    const-string v1, "postview_result"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 4500
    .local v13, "result":I
    const/4 v1, 0x3

    if-ne v13, v1, :cond_9

    .line 4501
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED and Size is too BIG BIG BIG BIG BIG BIG"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4502
    :cond_9
    const/4 v1, 0x4

    if-ne v13, v1, :cond_1

    .line 4503
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED and unsupported image format"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4509
    .end local v13    # "result":I
    :cond_a
    :sswitch_3
    if-nez p2, :cond_e

    if-eqz p3, :cond_e

    .line 4510
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4511
    const-string v1, "postview_result"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 4512
    .restart local v13    # "result":I
    const/4 v1, 0x3

    if-ne v13, v1, :cond_c

    .line 4513
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED and Size is too BIG BIG BIG BIG BIG BIG"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4514
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    if-eqz v1, :cond_b

    .line 4515
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 4517
    :cond_b
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    .line 4518
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v1, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 4519
    const v3, 0x7f0c002c

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4520
    const v3, 0x7f0c0020

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4521
    const v3, 0x7f0c001b

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4522
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    .line 4518
    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 4523
    :cond_c
    const/4 v1, 0x4

    if-ne v13, v1, :cond_1

    .line 4524
    const-string v1, "OCR"

    const-string v3, "[OCR] RESULT_CANCELED and unsupported image format"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4525
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    if-eqz v1, :cond_d

    .line 4526
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 4528
    :cond_d
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    .line 4529
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget v3, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v1, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 4530
    const v3, 0x7f0c002c

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4531
    const v3, 0x7f0c0021

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4532
    const v3, 0x7f0c001b

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4533
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    .line 4529
    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 4535
    .end local v13    # "result":I
    :cond_e
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    if-eqz p3, :cond_1

    .line 4536
    sget-boolean v1, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-eqz v1, :cond_1

    .line 4537
    const/4 v1, -0x1

    move-object/from16 v0, p3

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->setResult(ILandroid/content/Intent;)V

    .line 4538
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    goto/16 :goto_0

    .line 4429
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_3
        0x7d7 -> :sswitch_0
        0x7d8 -> :sswitch_2
        0x7da -> :sswitch_1
    .end sparse-switch
.end method

.method public onClickEvent(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0c001d

    const v5, 0x7f0c0016

    const/16 v3, 0x1b

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5808
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 5889
    :cond_0
    :goto_0
    return-void

    .line 5810
    :pswitch_0
    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    if-eqz v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    .line 5811
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->onFlashModeSelect(Z)V

    .line 5813
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    if-eqz v0, :cond_2

    .line 5814
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5815
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 5816
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 5817
    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f0c0046

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5818
    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5817
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5815
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 5810
    goto :goto_1

    .line 5820
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5821
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 5822
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 5823
    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f0c0046

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5824
    const v3, 0x7f0c001e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5823
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5821
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5830
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5831
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraVoiceCommand(I)V

    .line 5832
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5833
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 5834
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 5835
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 5836
    const v5, 0x7f0c001e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 5835
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 5833
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    .line 5847
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContent:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5848
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentTitle:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5849
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopupContentText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5851
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 5852
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpTitle:Landroid/widget/TextView;

    const v3, 0x7f0c002a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5853
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentTitleText:Landroid/widget/TextView;

    const v3, 0x7f0c0047

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5854
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->setLocaleAsSvoice(Landroid/content/res/Resources;)V

    .line 5856
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->ocr_voice_commands:[Ljava/lang/String;

    .line 5857
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->ocr_voice_commands:[Ljava/lang/String;

    aget-object v0, v4, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5858
    const-string v0, ", "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5859
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->ocr_voice_commands:[Ljava/lang/String;

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5857
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5860
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->unsetLocale(Landroid/content/res/Resources;)V

    .line 5861
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->showPopupWindow()V

    .line 5862
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->handleActivityFinish()V

    goto/16 :goto_0

    .line 5839
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraVoiceCommand(I)V

    .line 5840
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5841
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 5842
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 5843
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 5844
    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 5843
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 5841
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 5864
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->hidePopupWindow()V

    goto/16 :goto_0

    .line 5870
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 5871
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5872
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->processBack()V

    goto :goto_3

    .line 5877
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/bcocr/OCR;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 5878
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/bcocr/OCR;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto/16 :goto_0

    .line 5883
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->createPopupMenu()V

    goto/16 :goto_0

    .line 5808
    :pswitch_data_0
    .packed-switch 0x7f0f001f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 899
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 901
    invoke-static {}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRActivityOrientation()I

    move-result v0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 902
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z

    .line 903
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->onRotateClear()V

    .line 904
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->onRotateResume()V

    .line 906
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1337
    const-string v9, "VerificationLog"

    const-string v10, "OnCreate"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    const/16 v9, 0x7d0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setDvfsBooster(I)V

    .line 1340
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onCreate(Landroid/os/Bundle;)V

    .line 1342
    invoke-static {p0}, Lcom/sec/android/app/bcocr/Util;->setResource(Landroid/content/Context;)V

    .line 1344
    invoke-super {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 1345
    .local v5, "intent":Landroid/content/Intent;
    const/high16 v9, 0x10000000

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1350
    const-string v9, "OCR"

    const-string v10, "[OCR] onCreate"

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1351
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 1352
    new-instance v9, Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v10, 0x0

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/bcocr/OCR$MainHandler;-><init>(Lcom/sec/android/app/bcocr/OCR;Lcom/sec/android/app/bcocr/OCR$MainHandler;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    .line 1355
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->ocr_voice_commands:[Ljava/lang/String;

    .line 1358
    const-string v9, "AXLOG"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[OCR] Total-CameraPreviewLoading**StartU["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]**"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    const-string v9, "AXLOG"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[OCR] Total-CameraUILoading(TSP)**StartU["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]**"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1366
    .local v2, "_axtime_st_1":J
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 1367
    .local v7, "win":Landroid/view/Window;
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 1368
    .local v8, "wl":Landroid/view/WindowManager$LayoutParams;
    iget v9, v8, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v9, v9, 0x2

    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 1371
    sget-boolean v9, Lcom/sec/android/app/bcocr/Feature;->CAMERA_ENABLE_STATUS_BAR:Z

    .line 1374
    invoke-virtual {v7, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1376
    new-instance v9, Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-direct {v9, p0}, Lcom/sec/android/app/bcocr/OCRSettings;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 1377
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->resetObservers()V

    .line 1378
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v9, p0}, Lcom/sec/android/app/bcocr/OCRSettings;->registerOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V

    .line 1379
    new-instance v9, Lcom/sec/android/app/bcocr/MenuDimController;

    invoke-direct {v9, p0}, Lcom/sec/android/app/bcocr/MenuDimController;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    .line 1382
    const v9, 0x7f030004

    const/4 v10, 0x0

    invoke-static {p0, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    .line 1383
    new-instance v9, Landroid/widget/PopupWindow;

    iget-object v10, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const/4 v11, -0x2

    const/4 v12, -0x2

    .line 1384
    const/4 v13, 0x1

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 1383
    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    .line 1385
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    .line 1386
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 1387
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1388
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 1389
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popup:Landroid/widget/PopupWindow;

    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1391
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f0009

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpTitle:Landroid/widget/TextView;

    .line 1392
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f0008

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpCloseBtn:Landroid/widget/Button;

    .line 1393
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f000a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopupContentText:Landroid/widget/TextView;

    .line 1395
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f000b

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentTitle:Landroid/widget/RelativeLayout;

    .line 1396
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f000c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentTitleText:Landroid/widget/TextView;

    .line 1398
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f000d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContent:Landroid/widget/RelativeLayout;

    .line 1399
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->popupview:Landroid/view/View;

    const v10, 0x7f0f000f

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mPopUpVoiceCommandContentText:Landroid/widget/TextView;

    .line 1401
    sget-boolean v9, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v9, :cond_4

    sget-boolean v9, Lcom/sec/android/app/bcocr/Feature;->OCR_LIGHTMODE_RESTORE:Z

    if-eqz v9, :cond_4

    .line 1402
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->getLightMode()I

    move-result v6

    .line 1403
    .local v6, "lightMode":I
    if-nez v6, :cond_3

    .line 1404
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    .line 1418
    .end local v6    # "lightMode":I
    :goto_0
    const v9, 0x7f030006

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setContentView(I)V

    .line 1419
    const v9, 0x7f0f0011

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    .line 1422
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setOCRActivityOrientation(I)V

    .line 1423
    const-string v9, "OCR"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[OCR] Activity orientation : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/bcocr/OCR;->getOCRActivityOrientation()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "(Landscape:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 1424
    const-string v11, ", Portrait:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "), Engine Orientation : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1423
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1425
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->loadLastFuncMode()V

    .line 1427
    sget-boolean v9, Lcom/sec/android/app/bcocr/Feature;->OCR_SUPPORT_VOLUME_CONTROL_FOR_SPEAK:Z

    if-eqz v9, :cond_0

    .line 1428
    const/4 v9, 0x3

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setVolumeControlStream(I)V

    .line 1431
    :cond_0
    new-instance v9, Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {v9, p0}, Lcom/sec/android/app/bcocr/OCREngine;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    .line 1434
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRUtils;->copyDatabase(Landroid/content/Context;)Z

    .line 1436
    new-instance v9, Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-direct {v9, p0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 1438
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 1439
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/app/bcocr/OCRSettings;->setMode(I)V

    .line 1440
    new-instance v9, Lcom/sec/android/app/bcocr/MenuResourceDepot;

    invoke-direct {v9, p0}, Lcom/sec/android/app/bcocr/MenuResourceDepot;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 1441
    new-instance v9, Landroid/media/AudioManager;

    invoke-direct {v9, p0}, Landroid/media/AudioManager;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    .line 1446
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->initializeCamera()V

    .line 1447
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->initCameraSound()V

    .line 1449
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->sobject:Ljava/lang/Object;

    if-nez v9, :cond_1

    .line 1450
    new-instance v9, Ljava/lang/Object;

    invoke-direct {v9}, Ljava/lang/Object;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->sobject:Ljava/lang/Object;

    .line 1452
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1453
    .local v0, "_axtime_ed_1":J
    const-string v9, "AXLOG"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[OCR] onCreate-End**End["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]**["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v0, v2

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]**[]**"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1454
    move-wide v2, v0

    .line 1456
    iget v9, p0, Lcom/sec/android/app/bcocr/OCR;->mRotateOnPaused:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_2

    .line 1457
    invoke-static {}, Lcom/sec/android/app/bcocr/OCR;->getOCRActivityOrientation()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/bcocr/OCR;->mRotateOnPaused:I

    .line 1459
    :cond_2
    return-void

    .line 1406
    .end local v0    # "_axtime_ed_1":J
    .restart local v6    # "lightMode":I
    :cond_3
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    goto/16 :goto_0

    .line 1409
    .end local v6    # "lightMode":I
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v4

    .line 1410
    .local v4, "flashMode":I
    if-nez v4, :cond_5

    .line 1411
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    .line 1415
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v4, v10}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraFlashMode(IZ)V

    goto/16 :goto_0

    .line 1413
    :cond_5
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .param p1, "id"    # I

    .prologue
    const v7, 0x7f0c000b

    const v6, 0x7f0c0004

    const/4 v5, 0x0

    .line 5362
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCreateDialog : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 5363
    packed-switch p1, :pswitch_data_0

    .line 5427
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 5365
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5366
    .local v0, "builder1":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c003a

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 5367
    new-instance v3, Lcom/sec/android/app/bcocr/OCR$20;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$20;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5386
    new-instance v2, Lcom/sec/android/app/bcocr/OCR$21;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$21;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 5397
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 5400
    .end local v0    # "builder1":Landroid/app/AlertDialog$Builder;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5401
    .local v1, "builder3":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0c000c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 5402
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 5403
    new-instance v3, Lcom/sec/android/app/bcocr/OCR$22;

    invoke-direct {v3, p0}, Lcom/sec/android/app/bcocr/OCR$22;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5411
    new-instance v2, Lcom/sec/android/app/bcocr/OCR$23;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$23;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 5422
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 5363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 910
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 912
    sget-boolean v1, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-nez v1, :cond_0

    .line 913
    const/4 v1, 0x0

    .line 921
    :goto_0
    return v1

    .line 916
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    if-eqz v1, :cond_1

    .line 917
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 918
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 921
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2233
    const-string v0, "OCR"

    const-string v1, "[OCR] onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2235
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->unbindView(Landroid/view/View;)V

    .line 2237
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 2238
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2239
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    .line 2242
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    if-eqz v0, :cond_1

    .line 2243
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onDestroy()V

    .line 2244
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    .line 2247
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    .line 2249
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    if-eqz v0, :cond_2

    .line 2250
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/MenuResourceDepot;->onDestroy()V

    .line 2251
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .line 2254
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_3

    .line 2255
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearCaptureImageData()V

    .line 2256
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->closeCamera()V

    .line 2257
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 2259
    :cond_3
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    .line 2261
    sget v0, Lcom/sec/android/app/bcocr/OCR;->mOCRContextNum:I

    if-gt v0, v3, :cond_4

    .line 2262
    invoke-static {}, Lcom/sec/barcode/QRcodeRecog;->resetQRcodeResult()V

    .line 2265
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_5

    .line 2266
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 2267
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    .line 2269
    :cond_5
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPoolId:[I

    .line 2271
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    .line 2273
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    .line 2275
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    .line 2276
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPText:Ljava/lang/String;

    .line 2278
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_6

    .line 2279
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2280
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    .line 2283
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    if-eqz v0, :cond_7

    .line 2284
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2285
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mLoadImageCanceled:Landroid/app/AlertDialog;

    .line 2288
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_8

    .line 2289
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2290
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPluggedLowBatteryPopup:Landroid/app/AlertDialog;

    .line 2293
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_9

    .line 2294
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2295
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    .line 2298
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_a

    .line 2299
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2300
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    .line 2303
    :cond_a
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    .line 2305
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    if-eqz v0, :cond_b

    .line 2306
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    .line 2309
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    if-eqz v0, :cond_c

    .line 2310
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    .line 2313
    :cond_c
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    .line 2315
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mSaveUri:Landroid/net/Uri;

    .line 2316
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 2317
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    .line 2319
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_d

    .line 2320
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2321
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2324
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_e

    .line 2325
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusArrowLayout:Landroid/widget/RelativeLayout;

    .line 2328
    :cond_e
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    .line 2330
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    if-eqz v0, :cond_f

    .line 2331
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    .line 2334
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_10

    .line 2335
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 2336
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 2339
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_11

    .line 2340
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 2343
    :cond_11
    invoke-super {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onDestroy()V

    .line 2344
    return-void
.end method

.method public onDirectLinkPreviewCancelled()V
    .locals 0

    .prologue
    .line 4179
    return-void
.end method

.method public onErrorCallback(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 3716
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onErrorCallback [Error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3717
    if-eqz p1, :cond_0

    .line 3718
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 3719
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v1, 0xb

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3722
    :cond_0
    return-void
.end method

.method public onFlashModeSelect(Z)V
    .locals 5
    .param p1, "onoff"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1300
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flashIcon / onFlashModeSelect, onoff = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-nez v2, :cond_1

    .line 1319
    :cond_0
    :goto_0
    return-void

    .line 1305
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1306
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->getDim(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1309
    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x4

    .line 1310
    .local v0, "flashMode":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setCameraFlashMode(IZ)V

    .line 1312
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_LIGHTMODE_RESTORE:Z

    if-eqz v2, :cond_0

    .line 1313
    if-eqz v0, :cond_4

    .line 1314
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setLightMode(I)V

    goto :goto_0

    .end local v0    # "flashMode":I
    :cond_3
    move v0, v1

    .line 1309
    goto :goto_1

    .line 1316
    .restart local v0    # "flashMode":I
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setLightMode(I)V

    goto :goto_0
.end method

.method public onFocusStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 3691
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onFocusStateChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3692
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    if-eqz v0, :cond_1

    .line 3712
    :cond_0
    :goto_0
    return-void

    .line 3696
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v0

    if-nez v0, :cond_2

    .line 3697
    const-string v0, "OCR"

    const-string v1, "[OCR] onFocusStateChanged : OCR state is idle => skip"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3700
    :cond_2
    if-nez p1, :cond_4

    .line 3701
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isTouchAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3702
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->stopTouchAutoFocus()V

    .line 3705
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 3708
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->showFocusIndicator(I)V

    .line 3709
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 3710
    const/4 v0, 0x1

    const/16 v1, 0x13

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_0
.end method

.method protected onImageStoringCompleted()V
    .locals 3

    .prologue
    .line 4188
    const-string v1, "OCR"

    const-string v2, "[OCR] onImageStoringCompleted"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4189
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mIsDestroying:Z

    if-eqz v1, :cond_1

    .line 4208
    :cond_0
    :goto_0
    return-void

    .line 4193
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 4195
    sget-boolean v1, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-eqz v1, :cond_0

    .line 4196
    const-string v1, "OCR"

    const-string v2, "Optical reader triggered by namecard Capture"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4197
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4198
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 4200
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getLastContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4201
    const-string v1, "OCR_NAMECARD_CAPTURE_MODE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4202
    const-string v1, "FILE_PATH"

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->getLastCapturedFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4203
    const-string v1, "WHERE"

    const-string v2, "NAMECARDCAPTURE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4205
    const/16 v1, 0x7da

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4206
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BOCR"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/bcocr/OCR;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v10, 0x15e

    const/16 v8, 0x1b

    const/4 v7, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2400
    const-string v3, "OCR"

    const-string v6, "[OCR] onKeyDown()"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2401
    const/4 v2, 0x1

    .line 2403
    .local v2, "shutterKey":Z
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    move v3, v4

    .line 2620
    :goto_0
    return v3

    .line 2408
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    if-eqz v3, :cond_2

    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/app/bcocr/MenuBase;->onKeyDown(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 2409
    goto :goto_0

    .line 2411
    :catch_0
    move-exception v3

    .line 2415
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    .line 2416
    goto :goto_0

    .line 2419
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isUsbMassStorageEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    .line 2420
    goto :goto_0

    .line 2423
    :cond_4
    const-string v3, "OCR"

    const-string v6, "[OCR] onKeyDown():handling onKeyDown event from Activity class"

    invoke-static {v3, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2425
    sparse-switch p1, :sswitch_data_0

    .line 2617
    :cond_5
    :goto_1
    if-eqz p2, :cond_28

    .line 2618
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2428
    goto :goto_0

    :sswitch_1
    move v3, v5

    .line 2432
    goto :goto_0

    .line 2434
    :sswitch_2
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-lez v3, :cond_6

    .line 2435
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown():KEYCODE_FOCUS is pressed. repeat..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2436
    goto :goto_0

    .line 2438
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 2439
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown():KEYCODE_FOCUS is pressed. Capture is disabled..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2440
    goto :goto_0

    .line 2442
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->handleShutterKey()Z

    move-result v3

    if-nez v3, :cond_b

    .line 2443
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_HALF_SHUTTER:Z

    if-nez v3, :cond_a

    .line 2444
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isContinuousAFEnabled()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2445
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v3, :cond_5

    .line 2446
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v3, v7, v10, v11}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 2451
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-nez v3, :cond_9

    .line 2452
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v3

    if-eqz v3, :cond_9

    .line 2453
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2454
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2456
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2457
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v3

    if-eq v3, v4, :cond_a

    .line 2458
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->startAFWaitTimer()V

    .line 2461
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->handleShutterEvent()V

    .line 2463
    :cond_b
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_5

    .line 2464
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v3

    if-nez v3, :cond_c

    .line 2465
    sget v3, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_HALF_PRESS:I

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    :cond_c
    move v3, v4

    .line 2467
    goto/16 :goto_0

    .line 2473
    :sswitch_3
    sget v3, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_FULL_PRESS:I

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    .line 2474
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v3

    if-nez v3, :cond_d

    .line 2475
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown(): Capture is disabled..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2476
    goto/16 :goto_0

    .line 2478
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isParamChanging()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 2479
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown(): Parameter is changing..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2480
    goto/16 :goto_0

    .line 2482
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isPreviewStarted()Z

    move-result v3

    if-nez v3, :cond_f

    .line 2483
    const-string v3, "OCR"

    .line 2484
    const-string v5, "[OCR] onKeyDown(): Preview is not ready..."

    .line 2483
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2485
    goto/16 :goto_0

    .line 2487
    :cond_f
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-nez v3, :cond_10

    .line 2488
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isFinishOneShotPreviewCallback()Z

    move-result v3

    if-nez v3, :cond_10

    .line 2489
    const-string v3, "OCR"

    .line 2490
    const-string v5, "[OCR] onKeyDown(): One shot preview callback is not finished..."

    .line 2489
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2491
    goto/16 :goto_0

    .line 2494
    :cond_10
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2495
    const-string v3, "OCR"

    .line 2496
    const-string v5, "[OCR] onKeyDown(): Camera is capturing..."

    .line 2495
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2497
    goto/16 :goto_0

    .line 2500
    :cond_11
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-eqz v3, :cond_12

    .line 2501
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->showDlg(I)V

    move v3, v4

    .line 2502
    goto/16 :goto_0

    .line 2504
    :cond_12
    if-eqz p2, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-lez v3, :cond_13

    .line 2505
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown(): repeat..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2506
    goto/16 :goto_0

    .line 2510
    :cond_13
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->handleShutterKey()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 2515
    :goto_2
    if-nez v2, :cond_17

    if-nez p2, :cond_17

    .line 2516
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_HALF_SHUTTER:Z

    if-nez v3, :cond_16

    .line 2517
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v3, :cond_14

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isContinuousAFEnabled()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2518
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v3, :cond_5

    .line 2519
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v3, v7, v10, v11}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 2511
    :catch_1
    move-exception v1

    .line 2512
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 2523
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-nez v3, :cond_15

    .line 2524
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v3

    if-eqz v3, :cond_15

    .line 2525
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2526
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2528
    :cond_15
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 2529
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v3

    if-eq v3, v4, :cond_16

    .line 2530
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->startAFWaitTimer()V

    .line 2533
    :cond_16
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->handleShutterEvent()V

    move v3, v4

    .line 2534
    goto/16 :goto_0

    .line 2536
    :cond_17
    if-eqz p2, :cond_1f

    .line 2538
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_HALF_SHUTTER:Z

    if-nez v3, :cond_18

    .line 2539
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v3

    if-eq v3, v4, :cond_18

    .line 2540
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopAFWaitTimer()V

    .line 2543
    :cond_18
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-lez v3, :cond_19

    move v3, v4

    .line 2544
    goto/16 :goto_0

    .line 2546
    :cond_19
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v3, :cond_1b

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isContinuousAFEnabled()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-nez v3, :cond_1b

    .line 2547
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v3, :cond_1a

    .line 2548
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v3, v7, v10, v11}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1a
    move v3, v4

    .line 2550
    goto/16 :goto_0

    .line 2552
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-nez v3, :cond_1c

    .line 2553
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v3

    if-eqz v3, :cond_1c

    .line 2554
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 2555
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v3

    if-nez v3, :cond_1c

    .line 2556
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v3

    if-nez v3, :cond_1c

    .line 2557
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2560
    :cond_1c
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v3

    if-eqz v3, :cond_1d

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v3

    if-eq v3, v4, :cond_1d

    .line 2561
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->startAFWaitTimer()V

    .line 2563
    :cond_1d
    if-eq v8, p1, :cond_1e

    .line 2564
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->handleShutterEvent()V

    move v3, v4

    .line 2565
    goto/16 :goto_0

    .line 2567
    :cond_1e
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->handleShutterKeyReleased()Z

    move-result v3

    if-nez v3, :cond_1f

    .line 2569
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->handleShutterReleaseEvent()V

    :cond_1f
    move v3, v4

    .line 2572
    goto/16 :goto_0

    .line 2575
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isPreviewStarted()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isShutterPressed()Z

    move-result v3

    if-nez v3, :cond_20

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v3

    if-eqz v3, :cond_21

    .line 2576
    :cond_20
    const-string v3, "OCR"

    const-string v5, "[OCR] onKeyDown():KEYCODE_VOLUME_UP/DOWN is pressed. Auto focusing..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 2577
    goto/16 :goto_0

    .line 2580
    :cond_21
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_ZOOM_SUPPORT:Z

    if-nez v3, :cond_22

    move v3, v5

    .line 2581
    goto/16 :goto_0

    .line 2584
    :cond_22
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_VOLUME_KEY_ZOOM_SUPPORT:Z

    if-nez v3, :cond_25

    .line 2585
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v3

    if-nez v3, :cond_24

    .line 2586
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v3

    if-eqz v3, :cond_23

    move v3, v5

    .line 2589
    goto/16 :goto_0

    .line 2587
    :cond_23
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->processBack()V

    goto :goto_3

    .line 2592
    :cond_24
    const/4 v3, 0x0

    invoke-virtual {p0, v8, v3}, Lcom/sec/android/app/bcocr/OCR;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 2593
    const/4 v3, 0x0

    invoke-virtual {p0, v8, v3}, Lcom/sec/android/app/bcocr/OCR;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move v3, v4

    .line 2594
    goto/16 :goto_0

    .line 2597
    :cond_25
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isZoomAvailable()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 2598
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->showZoomBarAction()V

    .line 2600
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mBaseLayout:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuResourceDepot:Lcom/sec/android/app/bcocr/MenuResourceDepot;

    const/4 v6, 0x6

    .line 2599
    invoke-static {v4, p0, v3, v5, v6}, Lcom/sec/android/app/bcocr/command/CommandBuilder;->buildCommand(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)Lcom/sec/android/app/bcocr/command/MenuCommand;

    move-result-object v0

    .line 2601
    .local v0, "cmd":Lcom/sec/android/app/bcocr/command/MenuCommand;
    if-eqz v0, :cond_26

    .line 2602
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/command/MenuCommand;->execute()V

    .line 2605
    :cond_26
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->resetFocusDueToZoom()V

    .line 2607
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    if-eqz v3, :cond_27

    .line 2609
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setVisibility(I)V

    .end local v0    # "cmd":Lcom/sec/android/app/bcocr/command/MenuCommand;
    :cond_27
    move v3, v4

    .line 2612
    goto/16 :goto_0

    :cond_28
    move v3, v4

    .line 2620
    goto/16 :goto_0

    .line 2425
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x17 -> :sswitch_3
        0x18 -> :sswitch_4
        0x19 -> :sswitch_4
        0x1a -> :sswitch_0
        0x1b -> :sswitch_3
        0x42 -> :sswitch_3
        0x50 -> :sswitch_2
        0x55 -> :sswitch_1
        0x59 -> :sswitch_1
        0x5a -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 2627
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2629
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    move v0, v1

    .line 2760
    :goto_0
    return v0

    .line 2634
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 2635
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/bcocr/MenuBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2636
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:Delivering onKeyUp to other view"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 2637
    goto :goto_0

    .line 2639
    :catch_0
    move-exception v0

    .line 2643
    :cond_1
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:handling onKeyUp event from Activity class"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2644
    sparse-switch p1, :sswitch_data_0

    .line 2757
    if-eqz p2, :cond_18

    .line 2758
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 2646
    :sswitch_0
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:BACK KEY PRESSED!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2647
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2648
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:event isCanceled() true"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2649
    goto :goto_0

    .line 2652
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2653
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateServerConnectingProgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    move v0, v1

    .line 2654
    goto :goto_0

    .line 2657
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2658
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mConnectingUpdateServerDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    move v0, v1

    .line 2659
    goto :goto_0

    .line 2662
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isStartingEngine()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isStartingPreview()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2663
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2667
    :cond_5
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:Ignoring BACK KEY because preview is being started!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2668
    goto/16 :goto_0

    .line 2671
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->processBack()V

    move v0, v1

    .line 2672
    goto/16 :goto_0

    .line 2677
    :sswitch_1
    sget v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_FULL_UP:I

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    .line 2678
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_7

    .line 2679
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 2682
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isParamChanging()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 2683
    goto/16 :goto_0

    .line 2686
    :cond_8
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCaptureMode:Z

    if-nez v0, :cond_9

    .line 2687
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:now key auto mode returned..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2688
    goto/16 :goto_0

    .line 2690
    :cond_9
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_HALF_SHUTTER:Z

    if-nez v0, :cond_a

    .line 2691
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopAFWaitTimer()V

    .line 2693
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isUsbMassStorageEnabled()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 2694
    goto/16 :goto_0

    .line 2696
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isPreviewStarted()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2697
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp: Preview is not ready..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2698
    goto/16 :goto_0

    .line 2701
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v0

    if-nez v0, :cond_d

    .line 2702
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp: OCR Preview is not ready..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2703
    goto/16 :goto_0

    .line 2706
    :cond_d
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-nez v0, :cond_e

    .line 2707
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isFinishOneShotPreviewCallback()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2708
    const-string v0, "OCR"

    .line 2709
    const-string v2, "[OCR] onKeyUp: One shot preview callback is not finished..."

    .line 2708
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2710
    goto/16 :goto_0

    .line 2713
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2714
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp: Camera is Capturing..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2715
    goto/16 :goto_0

    .line 2717
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v1

    .line 2718
    goto/16 :goto_0

    .line 2721
    :cond_10
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-eqz v0, :cond_11

    .line 2722
    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->showDlg(I)V

    move v0, v1

    .line 2723
    goto/16 :goto_0

    .line 2726
    :cond_11
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->handleShutterKeyReleased()Z

    move-result v0

    if-nez v0, :cond_12

    .line 2727
    const-string v0, "OCR"

    const-string v2, "[OCR] onKeyUp:handleShutterReleaseEvent"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2728
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    .line 2729
    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 2730
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->handleShutterReleaseEvent()V

    :cond_12
    move v0, v1

    .line 2732
    goto/16 :goto_0

    :sswitch_2
    move v0, v1

    .line 2734
    goto/16 :goto_0

    .line 2736
    :sswitch_3
    if-eqz p2, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_13

    move v0, v1

    .line 2737
    goto/16 :goto_0

    .line 2740
    :cond_13
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    if-nez v0, :cond_14

    move v0, v1

    .line 2741
    goto/16 :goto_0

    .line 2744
    :cond_14
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-nez v0, :cond_15

    .line 2745
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 2746
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2747
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 2750
    :cond_15
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    sget v2, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_FULL_UP:I

    if-eq v0, v2, :cond_16

    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    sget v2, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_HALF_PRESS:I

    if-ne v0, v2, :cond_17

    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v0

    if-nez v0, :cond_17

    .line 2751
    sget v0, Lcom/sec/android/app/bcocr/OCR;->CA_HARDKEY_NONE:I

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mHardKeyStatus:I

    move v0, v1

    .line 2752
    goto/16 :goto_0

    :cond_17
    move v0, v1

    .line 2754
    goto/16 :goto_0

    :cond_18
    move v0, v1

    .line 2760
    goto/16 :goto_0

    .line 2644
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x17 -> :sswitch_1
        0x1a -> :sswitch_2
        0x1b -> :sswitch_1
        0x42 -> :sswitch_1
        0x50 -> :sswitch_3
    .end sparse-switch
.end method

.method public onLaunchPost()V
    .locals 2

    .prologue
    .line 4290
    const-string v0, "OCR"

    const-string v1, "onLaunchPost"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4292
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 4293
    const-string v0, "OCR"

    const-string v1, "onLaunchPost - mOCREngine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4305
    :goto_0
    return-void

    .line 4297
    :cond_0
    new-instance v0, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    .line 4299
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getLastContentUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4300
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;->onCompleted()V

    .line 4301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    goto :goto_0

    .line 4303
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->searchForLastContentUri()V

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 6372
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1611
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1612
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/OCR;->setIntent(Landroid/content/Intent;)V

    .line 1613
    return-void
.end method

.method public onOCRFuncModeDetecttextChangeSelected()V
    .locals 3

    .prologue
    .line 3369
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(I)V

    .line 3370
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onOCRFuncModeDetecttextChangeSelected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3371
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3372
    const-string v0, "OCR"

    const-string v1, "[OCR] onOCRFuncModeDetecttextChangeSelected failed - queue is not empty"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3375
    :cond_0
    return-void
.end method

.method public onOCRFuncModeDirectlinkChangeSelected()V
    .locals 3

    .prologue
    .line 3344
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(I)V

    .line 3345
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onOCRFuncModeDirectlinkChangeSelected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3346
    return-void
.end method

.method public onOCRFuncModeTextsearchChangeSelected()V
    .locals 3

    .prologue
    .line 3350
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onOCRFuncModeTextsearchChangeSelected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3351
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3352
    const-string v0, "OCR"

    const-string v1, "[OCR] onSelfModeChangeSelected failed - queue is not empty"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3355
    :cond_0
    return-void
.end method

.method public onOCRFuncModeTranslatorChangeSelected()V
    .locals 3

    .prologue
    .line 3359
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(I)V

    .line 3360
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onOCRFuncModeTranslatorChangeSelected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3361
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3362
    const-string v0, "OCR"

    const-string v1, "[OCR] onOCRFuncModeTranslatorChangeSelected failed - queue is not empty"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3365
    :cond_0
    return-void
.end method

.method public onOCRRealModeChange()V
    .locals 2

    .prologue
    .line 3248
    const-string v0, "OCR"

    const-string v1, "[OCR] onOCRRealModeChange"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3250
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3251
    const-string v0, "OCR"

    const-string v1, "[OCR] onOCRRealModeChange failed - queue is not empty"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3254
    :cond_0
    return-void
.end method

.method public onOCRSettingsChanged(II)V
    .locals 5
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5787
    sparse-switch p1, :sswitch_data_0

    .line 5805
    :goto_0
    :sswitch_0
    return-void

    .line 5789
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->setDefaultStorageStatus()V

    goto :goto_0

    .line 5794
    :sswitch_2
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "######VR onOCRSettingsChanged, MENUID_VOICECOMMAND , modeid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5795
    if-ne p2, v4, :cond_0

    .line 5796
    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/bcocr/OCR;->OCRVoiceIndicator(IZ)V

    goto :goto_0

    .line 5798
    :cond_0
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/bcocr/OCR;->OCRVoiceIndicator(IZ)V

    goto :goto_0

    .line 5787
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x16 -> :sswitch_1
        0x29 -> :sswitch_2
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 20
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 928
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v17

    packed-switch v17, :pswitch_data_0

    .line 1092
    :goto_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v17

    return v17

    .line 931
    :pswitch_0
    const-string v17, "OCR"

    const-string v18, "[OCR] option_menu_loadimage"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 934
    .local v13, "pm":Landroid/content/pm/PackageManager;
    sget-object v17, Lcom/sec/android/app/bcocr/Feature;->PACKAGE_NAME_GALLERY3D:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v4

    .line 937
    .local v4, "ApplicationState":I
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v4, v0, :cond_0

    .line 938
    :try_start_0
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 939
    .local v8, "intent":Landroid/content/Intent;
    const-string v17, "com.sec.android.app.popupuireceiver"

    .line 940
    const-string v18, "com.sec.android.app.popupuireceiver.DisableApp"

    .line 939
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 941
    const-string v17, "app_package_name"

    sget-object v18, Lcom/sec/android/app/bcocr/Feature;->PACKAGE_NAME_GALLERY3D:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 942
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/bcocr/OCR;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 950
    .end local v8    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v6

    .line 951
    .local v6, "e":Landroid/content/ActivityNotFoundException;
    const-string v17, "OCR"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "[OCR] option_menu_loadimage : Album ActivityNotFoundException ("

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 944
    .end local v6    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    :try_start_1
    new-instance v8, Landroid/content/Intent;

    const-string v17, "android.intent.action.GET_CONTENT"

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 945
    .restart local v8    # "intent":Landroid/content/Intent;
    sget-object v17, Lcom/sec/android/app/bcocr/Feature;->PACKAGE_NAME_GALLERY3D:Ljava/lang/String;

    .line 946
    sget-object v18, Lcom/sec/android/app/bcocr/Feature;->ACTIVITY_CLASS_NAME_GALLERY3D:Ljava/lang/String;

    .line 945
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 947
    const-string v17, "image/*"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 948
    const/16 v17, 0x7d8

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/bcocr/OCR;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 956
    .end local v4    # "ApplicationState":I
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v13    # "pm":Landroid/content/pm/PackageManager;
    :pswitch_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 957
    .restart local v8    # "intent":Landroid/content/Intent;
    const-class v17, Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 958
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/bcocr/OCR;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 962
    .end local v8    # "intent":Landroid/content/Intent;
    :pswitch_2
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v11, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    const v18, 0x7f0c0030

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v11, v17

    const/16 v17, 0x1

    .line 963
    const v18, 0x7f0c0031

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v11, v17

    .line 964
    .local v11, "items":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 965
    .local v7, "index":I
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 966
    .local v15, "preferences":Landroid/content/SharedPreferences;
    const-string v17, "setting_image_auto_save"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 967
    const/4 v7, 0x0

    .line 972
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->dismiss()V

    .line 975
    :cond_1
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    .line 976
    new-instance v17, Landroid/app/AlertDialog$Builder;

    sget v18, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 977
    const v18, 0x7f0c002d

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 978
    new-instance v18, Lcom/sec/android/app/bcocr/OCR$8;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/bcocr/OCR$8;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v11, v7, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 996
    const v18, 0x7f0c001a

    new-instance v19, Lcom/sec/android/app/bcocr/OCR$9;

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/bcocr/OCR$9;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual/range {v17 .. v19}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 1002
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v17

    .line 976
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 969
    :cond_2
    const/4 v7, 0x1

    goto :goto_1

    .line 1006
    .end local v7    # "index":I
    .end local v11    # "items":[Ljava/lang/String;
    .end local v15    # "preferences":Landroid/content/SharedPreferences;
    :pswitch_3
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v5, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    .line 1007
    const v18, 0x7f0c001d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v17

    const/16 v17, 0x1

    .line 1008
    const v18, 0x7f0c001e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v17

    .line 1011
    .local v5, "caputreItems":[Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 1012
    .local v14, "preference":Landroid/content/SharedPreferences;
    const-string v17, "setting_image_auto_capture"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1013
    const/4 v7, 0x0

    .line 1018
    .restart local v7    # "index":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    .line 1019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->dismiss()V

    .line 1021
    :cond_3
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    .line 1022
    new-instance v17, Landroid/app/AlertDialog$Builder;

    .line 1023
    sget v18, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1024
    const v18, 0x7f0c001f

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 1025
    new-instance v18, Lcom/sec/android/app/bcocr/OCR$10;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/bcocr/OCR$10;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v5, v7, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 1053
    const v18, 0x7f0c001a

    .line 1054
    new-instance v19, Lcom/sec/android/app/bcocr/OCR$11;

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/bcocr/OCR$11;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    .line 1053
    invoke-virtual/range {v17 .. v19}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    .line 1060
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v17

    .line 1022
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1015
    .end local v7    # "index":I
    :cond_4
    const/4 v7, 0x1

    .restart local v7    # "index":I
    goto :goto_2

    .line 1064
    .end local v5    # "caputreItems":[Ljava/lang/String;
    .end local v7    # "index":I
    .end local v14    # "preference":Landroid/content/SharedPreferences;
    :pswitch_4
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 1065
    .local v16, "spreference":Landroid/content/SharedPreferences;
    const-string v17, "bcr_dialog_use_mobile_network_message"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 1066
    .local v9, "isMobileNetworkMessageEnabled":Z
    const-string v17, "bcr_dialog_use_wifi_message"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 1068
    .local v10, "isWifiNetworkMessageEnabled":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/bcocr/OCRUtils;->checkNetworkConnectivity(Landroid/content/Context;)I

    move-result v12

    .line 1070
    .local v12, "networkState":I
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->isChinaFeature()Z

    move-result v17

    if-eqz v17, :cond_5

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v12, v0, :cond_5

    .line 1071
    if-nez v10, :cond_5

    .line 1072
    const v17, 0x7f0c0027

    .line 1073
    const v18, 0x7f0c0029

    const-string v19, "bcr_dialog_use_wifi_message"

    .line 1072
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR;->showNetworkStateDialog(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 1074
    :cond_5
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->isChinaFeature()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1075
    if-nez v12, :cond_6

    .line 1076
    if-nez v9, :cond_6

    .line 1077
    const v17, 0x7f0c0026

    .line 1078
    const v18, 0x7f0c0028

    .line 1079
    const-string v19, "bcr_dialog_use_mobile_network_message"

    .line 1077
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR;->showNetworkStateDialog(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 1080
    :cond_6
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->isChinaFeature()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1081
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v12, v0, :cond_7

    .line 1082
    const v17, 0x7f0c0033

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1085
    :cond_7
    new-instance v17, Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getBaseContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Lcom/sec/android/app/bcocr/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    .line 1086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->start()V

    goto/16 :goto_0

    .line 928
    :pswitch_data_0
    .packed-switch 0x7f0f0035
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onPause()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x7

    const/4 v5, 0x0

    .line 1617
    const-string v2, "OCR"

    const-string v3, "[OCR] onPause"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1619
    const/16 v2, 0x7d0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->setDvfsBooster(I)V

    .line 1621
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v2, :cond_0

    .line 1622
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1623
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1624
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1625
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1626
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1627
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1628
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1629
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1630
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1631
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0x15

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1632
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v3, 0xd0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 1633
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopAFWaitTimer()V

    .line 1636
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 1637
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1638
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1639
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1640
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1641
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 1642
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1645
    :cond_1
    invoke-static {}, Lcom/sec/android/app/bcocr/OCR;->getOCRActivityOrientation()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/bcocr/OCR;->mRotateOnPaused:I

    .line 1646
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->notifyOnPause()V

    .line 1647
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 1648
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z

    .line 1649
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopOCRDetectTimer()V

    .line 1650
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->saveCurrentFuncMode()V

    .line 1651
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 1652
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1654
    .local v1, "win":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1655
    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1658
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCaptureMode:Z

    if-eqz v2, :cond_d

    .line 1659
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v2

    if-nez v2, :cond_3

    .line 1660
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "waitForRecognizeThread+++"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->waitForRecognizeThread()V

    .line 1662
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "waitForRecognizeThread---"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1671
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 1672
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setVisibility(I)V

    .line 1676
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    if-eqz v2, :cond_5

    .line 1677
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->stop()V

    .line 1681
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->stopVoiceRecognizer()V

    .line 1683
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_6

    .line 1684
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->IsWaitingAnimation()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1685
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->hideWaitingAnimation()V

    .line 1686
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/bcocr/OCREngine;->setWaitingAnimation(Z)V

    .line 1690
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_7

    .line 1691
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->waitForEngineStartingThread()V

    .line 1692
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->waitForStartPreviewThreadComplete()V

    .line 1693
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->waitForCurrentSearchingLastContentUri()V

    .line 1694
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->waitForCurrentPictureSaving()V

    .line 1700
    :cond_7
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_8

    .line 1701
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1707
    :cond_8
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->onFocusStateChanged(I)V

    .line 1708
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->hideAllDlg()V

    .line 1710
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_a

    .line 1711
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    .line 1712
    .local v0, "oldState":I
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1713
    if-eqz v0, :cond_9

    .line 1715
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 1716
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->doStopEngineSync()V

    .line 1717
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->clearRequest()V

    .line 1718
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1720
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->onPause()V

    .line 1730
    .end local v0    # "oldState":I
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    if-eqz v2, :cond_b

    .line 1731
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    .line 1734
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_c

    .line 1735
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->dismiss()V

    .line 1738
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->resumeAudioPlayback()V

    .line 1740
    invoke-super {p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onPause()V

    .line 1741
    return-void

    .line 1665
    :cond_d
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "waitForPrevRecognizeThread+++"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1666
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->waitForPrevRecognizeThread()V

    .line 1667
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "waitForPrevRecognizeThread---"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1703
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v5, 0x1

    .line 5432
    const-string v2, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onPrepareDialog : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 5433
    packed-switch p1, :pswitch_data_0

    .line 5454
    :goto_0
    return-void

    .line 5435
    :pswitch_0
    const v2, 0x7f0c003a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5436
    .local v1, "message":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStorageStatus:I

    if-ne v2, v5, :cond_0

    .line 5437
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v2

    if-nez v2, :cond_1

    .line 5438
    invoke-static {}, Lcom/sec/android/app/bcocr/CheckMemory;->isStorageMounted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5439
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow(I)I

    move-result v2

    if-nez v2, :cond_1

    .line 5440
    const-string v2, "OCR"

    const-string v3, "change_to_card_memory"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 5441
    const v2, 0x7f0c003c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_1
    move-object v0, p2

    .line 5451
    check-cast v0, Landroid/app/AlertDialog;

    .line 5452
    .local v0, "builder":Landroid/app/AlertDialog;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 5442
    .end local v0    # "builder":Landroid/app/AlertDialog;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 5443
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->checkStorageLow(I)I

    move-result v2

    if-nez v2, :cond_2

    .line 5444
    const-string v2, "OCR"

    const-string v3, "change_to_phone_memory"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 5445
    const v2, 0x7f0c003d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5446
    goto :goto_1

    .line 5447
    :cond_2
    const-string v2, "OCR"

    const-string v3, "not_enough_space"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 5448
    const v2, 0x7f0c003b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 5433
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x0

    .line 1844
    const-string v1, "OCR"

    const-string v2, "onPrepareOptionsMenu"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_MENU_HARD_KEY:Z

    if-nez v1, :cond_0

    .line 1846
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/bcocr/OCR$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/bcocr/OCR$15;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    .line 1857
    const-wide/16 v4, 0x5a

    .line 1846
    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1867
    :goto_0
    return v0

    .line 1860
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRUtils;->isSamsungAppsExist(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1861
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1864
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    if-nez v1, :cond_2

    .line 1865
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1867
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onRecognitionStateChanged()V
    .locals 2

    .prologue
    .line 4966
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 4967
    const-string v0, "OCR"

    const-string v1, "[OCR] [Recognition] onRecognitionStateChanged:"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4969
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime1:J

    .line 4970
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/bcocr/OCR$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$18;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    .line 5019
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    const-string v1, "previewRecogThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 5020
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5021
    return-void
.end method

.method public onRecognitionStateChangedForJPEG()V
    .locals 2

    .prologue
    .line 5082
    const-string v0, "OCR"

    const-string v1, "[OCR] [Recognition] onRecognitionStateChangedForJPEG"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5084
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->onPrepareRecognitionThread()V

    .line 5087
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->showCaptureProgressTimer()V

    .line 5088
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 5091
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/bcocr/OCR$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$19;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    .line 5115
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    const-string v1, "captureRecogThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 5116
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5117
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 3315
    const-string v0, "OCR"

    const-string v1, "[OCR] onRestoreInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3316
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 3317
    const-string v0, "OCR_BUNDLE_MSG_INT_FUNCMODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    .line 3318
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    if-nez v0, :cond_1

    .line 3319
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    .line 3328
    :cond_0
    :goto_0
    const-string v0, "OCR_BUNDLE_FRAGMENT_LEFTSIDE_MODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    .line 3329
    const-string v0, "OCR_BUNDLE_FRAGMENT_RIGHTSIDE_MODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    .line 3330
    const-string v0, "OCR_BUNDLE_SIDEMENU_SHOW_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSideMenuShowState:Z

    .line 3332
    const-string v0, "OCR_BUNDLE_OCRFLASH_MODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    .line 3333
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const-string v1, "OCR_BUNDLE_FLASH_LOW_BATTERY_DIM"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 3334
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const-string v1, "OCR_BUNDLE_FLASH_HIGH_TEMPOTURE_DIM"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 3335
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const-string v1, "OCR_BUNDLE_FLASH_TORCH_LIGHT_DIM"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 3336
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v1

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 3339
    const-string v0, "OCR_BUNDLE_LOW_BATTERY_POPUP_SHOW"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 3340
    return-void

    .line 3320
    :cond_1
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    if-ne v0, v4, :cond_2

    .line 3321
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    goto :goto_0

    .line 3322
    :cond_2
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    if-ne v0, v3, :cond_3

    .line 3323
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    goto :goto_0

    .line 3324
    :cond_3
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mRestoreFuncMode:I

    if-ne v0, v5, :cond_0

    .line 3325
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 20

    .prologue
    .line 1873
    const-string v2, "VerificationLog"

    const-string v4, "OnResume"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1874
    const-string v2, "OCR"

    const-string v4, "[OCR] onResume"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1876
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkSideSyncConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1877
    const-string v2, "OCR"

    const-string v4, "unable launch camera while sidesync"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1878
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0c0022

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1879
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 1882
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initLayout()V

    .line 1883
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->calculateMinimunBizcardSize()V

    .line 1885
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    if-eqz v2, :cond_1

    .line 1886
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->calculateScreenRatio()V

    .line 1889
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1890
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuDimController;->restore()V

    .line 1893
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->mRotateOnPaused:I

    invoke-static {}, Lcom/sec/android/app/bcocr/OCR;->getOCRActivityOrientation()I

    move-result v4

    if-eq v2, v4, :cond_3

    .line 1894
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->onRotateClear()V

    .line 1895
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->onRotateResume()V

    .line 1898
    :cond_3
    sget v2, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION:I

    sput v2, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION:I

    .line 1899
    sget v2, Lcom/sec/android/app/bcocr/Feature;->OCR_DETECT_DURATION_LONG:I

    sput v2, Lcom/sec/android/app/bcocr/OCR;->OCR_DETECT_DURATION_LONG:I

    .line 1901
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    if-eqz v2, :cond_18

    .line 1902
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1903
    const-string v4, "setting_image_auto_capture"

    const/4 v5, 0x1

    .line 1902
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mAutoCaptureEnabled:Z

    .line 1908
    :goto_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v4, "CscFeature_Camera_SecurityMdmService"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1909
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkCameraStartCondition_Security()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1910
    const-string v2, "OCR"

    const-string v4, "onResume CAMERA disable(0)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1911
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    if-nez v2, :cond_4

    .line 1912
    const v2, 0x7f0c0035

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    .line 1914
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1915
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 1920
    :cond_5
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.voicerecorder.rec_save"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->sendBroadcast(Landroid/content/Intent;)V

    .line 1922
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initTalkbackTTSForVoiceControl()V

    .line 1923
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initTalkbackTTSForFlash()V

    .line 1926
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 1927
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRZoomingState:Z

    .line 1928
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z

    .line 1929
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->setPreviewButtonEnable()V

    .line 1930
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1931
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 1934
    :cond_6
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    .line 1935
    .local v15, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1937
    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v16

    .line 1938
    .local v16, "intentClass":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1939
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] onResume:intentClass.equals(getClass().getName() : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1940
    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1939
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1946
    :goto_1
    const-string v2, "OCR_SIP_TYPE"

    const/4 v4, -0x1

    invoke-virtual {v15, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    .line 1947
    const-string v2, "OCR_NAMECARD_CAPTURE_TYPE"

    const/4 v4, 0x0

    invoke-virtual {v15, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    .line 1949
    sget-boolean v2, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-nez v2, :cond_7

    .line 1950
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 1953
    :cond_7
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] intent OCR_SIP_TYPE(-) : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1954
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] intent OCR_NAME_CARD_TYPE(-) : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v5, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1956
    sget v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_8

    sget-boolean v2, Lcom/sec/android/app/bcocr/OCR;->nOcrNameCardCaptureMode:Z

    if-eqz v2, :cond_a

    .line 1957
    :cond_8
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] intent OCR_SIP_TYPE(+) :  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1958
    sget v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    const/4 v4, 0x1

    if-ge v2, v4, :cond_9

    sget v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    const/4 v4, 0x3

    if-le v2, v4, :cond_9

    .line 1959
    const/4 v2, 0x4

    sput v2, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    .line 1962
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    sget v4, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v2, v4}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRSIPType(I)V

    .line 1963
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRFuncMode(IZ)V

    .line 1964
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/sec/android/app/bcocr/OCRSettings;->setOCRRealMode(I)V

    .line 1966
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] intent OCR_SIP_TYPE(-) :  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/sec/android/app/bcocr/OCR;->mOCRSIPType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1970
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initCurrentOCREngineLanguage()V

    .line 1973
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1975
    .local v10, "_axtime_st_1":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    if-nez v2, :cond_b

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/bcocr/Util;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1976
    new-instance v2, Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 1979
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v18

    .line 1980
    .local v18, "win":Landroid/view/Window;
    const/16 v2, 0x80

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 1982
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/android/app/bcocr/MenuDimController;->getFlashState(I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1983
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    .line 1984
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 1985
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 1988
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v2

    if-nez v2, :cond_1a

    const/4 v14, 0x0

    .line 1989
    .local v14, "flashMode":Z
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/bcocr/OCR;->onFlashModeSelect(Z)V

    .line 1991
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v4, "CscFeature_Camera_BatteryTemperatureCheck"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1992
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkBatteryStatus()V

    .line 1996
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "torch_light"

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1b

    const/4 v12, 0x1

    .line 1997
    .local v12, "bTorchLight":Z
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v12}, Lcom/sec/android/app/bcocr/MenuDimController;->setFlashState(IZ)V

    .line 1998
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 2000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_e

    .line 2001
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->onResume()V

    .line 2003
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->clearRequest()V

    .line 2004
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 2028
    :cond_e
    const-string v2, "content://com.sec.knox.provider/RestrictionPolicy1"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2029
    .local v3, "uri1":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 2030
    .local v13, "cr1":Landroid/database/Cursor;
    if-eqz v3, :cond_f

    .line 2031
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    .line 2032
    const-string v5, "isCameraEnabled"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 2033
    const-string v19, "true"

    aput-object v19, v6, v7

    .line 2034
    const/4 v7, 0x0

    .line 2031
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 2037
    :cond_f
    if-eqz v13, :cond_12

    .line 2041
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2043
    const-string v2, "isCameraEnabled"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2042
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2044
    const-string v4, "false"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2045
    const-string v2, "OCR"

    const-string v4, "onResume CAMERA disable(1)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2046
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    if-nez v2, :cond_10

    .line 2047
    const v2, 0x7f0c0035

    .line 2048
    const/4 v4, 0x0

    .line 2047
    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    .line 2050
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2051
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_11

    .line 2052
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleProcessBack()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2056
    :cond_11
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2069
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "device_policy"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/app/admin/DevicePolicyManager;

    .line 2070
    .local v17, "mDPM":Landroid/app/admin/DevicePolicyManager;
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->getAllowCamera(Landroid/content/ComponentName;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 2071
    const-string v2, "OCR"

    const-string v4, "[OCR] onResume:CAMERA disable(2)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    if-nez v2, :cond_13

    .line 2073
    const v2, 0x7f0c0035

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    .line 2075
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2076
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_14

    .line 2077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleProcessBack()V

    .line 2081
    :cond_14
    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/bcocr/OCR;->mFocusTime2:J

    .line 2082
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 2083
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCR;->setAdvancedMacroFocusActive(Z)V

    .line 2085
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_15

    .line 2086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2089
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v2, :cond_16

    .line 2090
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getInstance()Lcom/dmc/ocr/SecMOCR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 2091
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EngineVersion :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-eqz v2, :cond_17

    .line 2095
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngineLanguageSelectedSetByTransDic:[I

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setRecognitionLanguage([I)V

    .line 2096
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setRecognitionMode(I)V

    .line 2098
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2099
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->PRELOAD_DB:Z

    if-eqz v2, :cond_1c

    const v2, 0x7f0c0010

    :goto_4
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2100
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0c0013

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2098
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setDataBasePath(Ljava/lang/String;)V

    .line 2101
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c0014

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dmc/ocr/SecMOCR;->setLicenseBasePath(Ljava/lang/String;)V

    .line 2104
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initIntentFilter()V

    .line 2106
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->SUPPORT_CAMERA_CSC_FEATURE:Z

    if-eqz v2, :cond_1e

    .line 2107
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkCameraStartCondition_Call()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2108
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkCameraDuringCall()V

    .line 2109
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mEnableDuringCall:Z

    if-nez v2, :cond_1d

    .line 2110
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->CannotStartCamera()V

    .line 2111
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onResume()V

    .line 2204
    :goto_5
    return-void

    .line 1905
    .end local v3    # "uri1":Landroid/net/Uri;
    .end local v10    # "_axtime_st_1":J
    .end local v12    # "bTorchLight":Z
    .end local v13    # "cr1":Landroid/database/Cursor;
    .end local v14    # "flashMode":Z
    .end local v15    # "intent":Landroid/content/Intent;
    .end local v16    # "intentClass":Ljava/lang/String;
    .end local v17    # "mDPM":Landroid/app/admin/DevicePolicyManager;
    .end local v18    # "win":Landroid/view/Window;
    :cond_18
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mAutoCaptureEnabled:Z

    goto/16 :goto_0

    .line 1943
    .restart local v15    # "intent":Landroid/content/Intent;
    .restart local v16    # "intentClass":Ljava/lang/String;
    :cond_19
    const-string v2, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] onResume:intent Class = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1988
    .restart local v10    # "_axtime_st_1":J
    .restart local v18    # "win":Landroid/view/Window;
    :cond_1a
    const/4 v14, 0x1

    goto/16 :goto_2

    .line 1996
    .restart local v14    # "flashMode":Z
    :cond_1b
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 2055
    .restart local v3    # "uri1":Landroid/net/Uri;
    .restart local v12    # "bTorchLight":Z
    .restart local v13    # "cr1":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    .line 2056
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2057
    throw v2

    .line 2099
    .restart local v17    # "mDPM":Landroid/app/admin/DevicePolicyManager;
    :cond_1c
    const v2, 0x7f0c0011

    goto :goto_4

    .line 2116
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mCheckCalling:Z

    if-eqz v2, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkCameraStartCondition_VT()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2117
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->CannotStartCamera()V

    .line 2118
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onResume()V

    goto :goto_5

    .line 2122
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->checkCameraStartCondition_Call()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2123
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->CannotStartCamera()V

    .line 2124
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onResume()V

    goto :goto_5

    .line 2129
    :cond_1f
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v4, "mounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 2130
    const-string v2, "OCR"

    const-string v4, "[OCR] Camera is finished due to MEDIA_UNMOUNTED"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2131
    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    if-nez v2, :cond_20

    .line 2132
    const-string v2, ""

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    .line 2134
    :cond_20
    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    const v4, 0x7f0c0039

    invoke-virtual {v2, v4}, Landroid/widget/Toast;->setText(I)V

    .line 2135
    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2136
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->finish()V

    .line 2144
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_21

    .line 2145
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_START_ENGINE_SYNC:Z

    if-eqz v2, :cond_29

    .line 2146
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mbNeedToStartEngineSync:Z

    if-eqz v2, :cond_28

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isKeyguardLocked()Z

    move-result v2

    if-nez v2, :cond_28

    .line 2147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->doStartEngineAsync()V

    .line 2148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->doSetAllParamsSync()V

    .line 2163
    :cond_21
    :goto_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mbNeedToStartEngineSync:Z

    .line 2166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_22

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_22

    .line 2167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartPreview()V

    .line 2170
    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v2, :cond_23

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->isRecognizing()Z

    move-result v2

    if-nez v2, :cond_23

    .line 2171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->updateStorage()V

    .line 2173
    :cond_23
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/sec/android/app/bcocr/OCR;->initFuncMode(IZ)V

    .line 2174
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->initRemains()V

    .line 2175
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/sec/android/app/bcocr/OCR;->setSideMenu(IIZZ)V

    .line 2178
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 2179
    const-string v2, "OCR"

    const-string v4, "[OCR] OnResume:<GATE-M>CAMERA</GATE-M>"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    :cond_24
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    .line 2193
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2194
    const-string v4, "setting_image_auto_save"

    const/4 v5, 0x1

    .line 2193
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 2194
    if-nez v2, :cond_25

    .line 2195
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->deleteOCRTempFile()V

    .line 2198
    :cond_25
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onResume()V

    .line 2200
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2201
    .local v8, "_axtime_ed_1":J
    const-string v2, "AXLOG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] onResume-End**End["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]**["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v6, v8, v10

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]**[]**"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2202
    move-wide v10, v8

    .line 2203
    const-string v2, "VerificationLog"

    const-string v4, "Excuted"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2138
    .end local v8    # "_axtime_ed_1":J
    :cond_26
    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    if-eqz v2, :cond_27

    .line 2139
    sget-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 2141
    :cond_27
    const/4 v2, 0x0

    sput-object v2, Lcom/sec/android/app/bcocr/OCR;->mStorageToast:Landroid/widget/Toast;

    goto/16 :goto_6

    .line 2150
    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartEngine()V

    .line 2151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->schedulePostInit()V

    .line 2152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleSetAllParams()V

    .line 2153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v4, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleChangeParameter(II)V

    goto/16 :goto_7

    .line 2156
    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStartEngine()V

    .line 2157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->schedulePostInit()V

    .line 2158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleSetAllParams()V

    .line 2159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v4, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleChangeParameter(II)V

    goto/16 :goto_7
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 3296
    const-string v0, "OCR"

    const-string v1, "[OCR] onSaveInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3297
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 3298
    const-string v0, "OCR_BUNDLE_MSG_INT_FUNCMODE"

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getOCRFuncMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3300
    const-string v0, "OCR_BUNDLE_FRAGMENT_LEFTSIDE_MODE"

    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3301
    const-string v0, "OCR_BUNDLE_FRAGMENT_RIGHTSIDE_MODE"

    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3302
    const-string v0, "OCR_BUNDLE_SIDEMENU_SHOW_STATE"

    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSideMenuShowState:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3305
    const-string v0, "OCR_BUNDLE_OCRFLASH_MODE"

    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRFlashModeOn:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3307
    const-string v0, "OCR_BUNDLE_LOW_BATTERY_POPUP_SHOW"

    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mLowBatteryDisableFlashPopupDisplayed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3308
    const-string v0, "OCR_BUNDLE_FLASH_LOW_BATTERY_DIM"

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->getFlashState(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3309
    const-string v0, "OCR_BUNDLE_FLASH_HIGH_TEMPOTURE_DIM"

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->getFlashState(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3310
    const-string v0, "OCR_BUNDLE_FLASH_TORCH_LIGHT_DIM"

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMenuDimController:Lcom/sec/android/app/bcocr/MenuDimController;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->getFlashState(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3311
    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1, "s"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v6, 0x0

    .line 2899
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 2928
    :cond_0
    :goto_0
    return v6

    .line 2903
    :cond_1
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 2907
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mInitialZoomValueOnScaleBegin:I

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    add-int v0, v1, v2

    .line 2909
    .local v0, "newZoomValue":I
    if-gez v0, :cond_2

    .line 2910
    const/4 v0, 0x0

    .line 2913
    :cond_2
    sget v1, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    if-le v0, v1, :cond_3

    .line 2914
    sget v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    .line 2917
    :cond_3
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] onScale:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2919
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/OCRSettings;->setZoomValue(I)V

    .line 2920
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setZoomValue(I)V

    .line 2921
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->invalidate()V

    .line 2923
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_0

    .line 2924
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2925
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v5, -0x2

    const/4 v2, 0x0

    .line 2933
    const-string v3, "OCR"

    const-string v4, "[OCR] onScaleBegin"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2935
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isZoomAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2936
    const-string v3, "OCR"

    const-string v4, "[OCR] onScaleBegin : Zoom is not available => skip"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2980
    :goto_0
    return v2

    .line 2940
    :cond_0
    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 2941
    const-string v3, "OCR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] onScaleBegin : Pointer num is not allowed("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") => skip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2945
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isTouchAutoFocusing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2946
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->resetTouchFocus()V

    .line 2949
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->showZoomBarAction()V

    .line 2951
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v3, :cond_3

    .line 2952
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2955
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getZoomValue()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->mInitialZoomValueOnScaleBegin:I

    .line 2956
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->resetFocusDueToZoom()V

    .line 2958
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    if-nez v3, :cond_4

    .line 2959
    new-instance v3, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    .line 2960
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCREngine;->getLastOrientation()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setLastOrientation(I)V

    .line 2961
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2962
    .local v1, "win":Landroid/view/Window;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2964
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2965
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2966
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v1, v3, v0}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2969
    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "win":Landroid/view/Window;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v3, v2, v2}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setExtraSize(II)V

    .line 2971
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getZoomValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setZoomValue(I)V

    .line 2972
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setVisibility(I)V

    .line 2974
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v2, :cond_5

    .line 2975
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2976
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2980
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 2995
    const-string v0, "OCR"

    const-string v1, "[OCR] onScaleEnd"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2996
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 2997
    const-string v0, "OCR"

    const-string v1, "[OCR] onScaleEnd : Zoom is not available => skip"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3004
    :cond_0
    :goto_0
    return-void

    .line 3000
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 3001
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3002
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mHideScaleZoomRect:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onSearchingLastContentUriCompleted()V
    .locals 1

    .prologue
    .line 4308
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    if-eqz v0, :cond_0

    .line 4309
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;->onCompleted()V

    .line 4310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mLastContentUriCallback:Lcom/sec/android/app/bcocr/OCR$LastContentUriCallback;

    .line 4312
    :cond_0
    return-void
.end method

.method public onShootingModeMenuSelect(I)V
    .locals 3
    .param p1, "shootingMode"    # I

    .prologue
    .line 3379
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] onShootingModeChanged: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3381
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isPreviewStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3382
    const-string v0, "OCR"

    const-string v1, "return isStartPreview.."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3393
    :goto_0
    return-void

    .line 3386
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3387
    const-string v0, "OCR"

    const-string v1, "return isCapturing.."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3391
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->resetTouchFocus()V

    .line 3392
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->setShootingMode(I)V

    goto :goto_0
.end method

.method public onStartingPreviewCompleted()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 3533
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->initIntentFilterBattery()V

    .line 3535
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 3536
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 3537
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3539
    :cond_0
    return-void
.end method

.method public onTextSearchPreviewCancelled()V
    .locals 0

    .prologue
    .line 4182
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 2766
    const-string v0, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] caftest onTouchEvent:action="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2768
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 2769
    const-string v0, "OCR"

    const-string v1, "[OCR] touch return camera engine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2770
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2848
    :goto_0
    return v0

    .line 2774
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mViewStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/MenuBase;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/MenuBase;->onActivityTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2775
    const-string v0, "OCR"

    const-string v2, "[OCR] touch return view stack is ???"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2776
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->resetTouchFocus()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 2777
    goto :goto_0

    .line 2779
    :catch_0
    move-exception v0

    .line 2783
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2784
    const-string v0, "OCR"

    const-string v2, "[OCR] touch return camera engine is capturing..."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2785
    goto :goto_0

    .line 2787
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mNumberOfPointer:I

    .line 2788
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_3

    .line 2790
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2794
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2795
    const-string v0, "OCR"

    const-string v2, "[OCR] touch return scale zoom is working..."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2796
    goto :goto_0

    .line 2801
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2848
    :pswitch_0
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 2803
    :pswitch_1
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest onTouchEvent:MotionEvent2.ACTION_UP"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2804
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_4

    .line 2805
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsTouchDown:Z

    :cond_4
    move v0, v1

    .line 2808
    goto :goto_0

    .line 2810
    :pswitch_2
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest onTouchEvent:MotionEvent2.ACTION_DOWN"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2812
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_5

    .line 2813
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mIsTouchDown:Z

    .line 2814
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2815
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest is not AF focusing"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2816
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isTouchAutoFocusEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->getSurfaceView()Landroid/view/SurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getRight()I

    move-result v2

    if-gt v0, v2, :cond_7

    .line 2817
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest touch focus is Enabled"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2818
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2819
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest is preview"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2820
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 2821
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/bcocr/OCR;->handleTouchAutoFocusEvent(Landroid/view/MotionEvent;Z)V

    .line 2822
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 2823
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2825
    const/16 v0, 0x8

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    :cond_5
    :goto_2
    move v0, v1

    .line 2836
    goto/16 :goto_0

    .line 2827
    :cond_6
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest is not preview"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2830
    :cond_7
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest touch focus is not Enabled"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2833
    :cond_8
    const-string v0, "OCR"

    const-string v2, "[OCR] caftest is AF focusing"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2838
    :pswitch_3
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_9

    .line 2839
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsTouchDown:Z

    .line 2840
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 2841
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2842
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    :cond_9
    move v0, v1

    .line 2844
    goto/16 :goto_0

    .line 2791
    :catch_1
    move-exception v0

    goto/16 :goto_1

    .line 2801
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onTranslatorPreviewCancelled()V
    .locals 0

    .prologue
    .line 4185
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 2208
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onWindowFocusChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2209
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCR;->mWindowFocusState:Z

    .line 2210
    if-nez p1, :cond_1

    .line 2211
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2212
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->stopVoiceRecognizer()V

    .line 2228
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onWindowFocusChanged(Z)V

    .line 2229
    return-void

    .line 2215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2216
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2217
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 2218
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->setOCRPreviewDetectStart()V

    .line 2224
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 2225
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->startVoiceRecognizer()V

    goto :goto_0

    .line 2220
    :cond_3
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_1
.end method

.method public pauseAudioPlayback()V
    .locals 4

    .prologue
    .line 748
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    .line 751
    const/4 v3, 0x2

    .line 750
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 753
    :cond_0
    return-void
.end method

.method public playCameraSound(II)V
    .locals 9
    .param p1, "Sound"    # I
    .param p2, "loop"    # I

    .prologue
    .line 4219
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 4220
    const-string v0, "OCR"

    const-string v1, "[OCR] playCameraSound - mSoundPool is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4241
    :goto_0
    return-void

    .line 4224
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->isCalling()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4225
    const-string v0, "OCR"

    const-string v1, "Sound shouldn\'t be occured during call"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4229
    :cond_1
    const/16 v7, 0xf

    .line 4230
    .local v7, "MAX_VOLUME":I
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamVolume:F

    .line 4232
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_3

    .line 4233
    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 4234
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=4;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamVolume:F

    .line 4240
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamId:[I

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPoolId:[I

    aget v1, v1, p1

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamVolume:F

    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamVolume:F

    const/4 v4, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v0

    aput v0, v8, p1

    goto :goto_0

    .line 4235
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 4236
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=3;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamVolume:F

    goto :goto_1
.end method

.method public refreshButtonMenu()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5769
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v0

    if-nez v0, :cond_1

    .line 5770
    const-string v0, "OCR"

    const-string v1, "can not refresh menu-button"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5783
    :cond_0
    :goto_0
    return-void

    .line 5774
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 5775
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/MenuDimController;->getVisibility(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5776
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 5777
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 5778
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 5780
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewIconFlash:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public declared-synchronized refreshDeviceList()V
    .locals 2

    .prologue
    .line 4750
    monitor-enter p0

    :try_start_0
    const-string v0, "OCR"

    const-string v1, "[OCR] refreshDeviceList"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4751
    monitor-exit p0

    return-void

    .line 4750
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetScaleDetector()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3020
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    if-eqz v0, :cond_0

    .line 3021
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setVisibility(I)V

    .line 3022
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleZoomRect:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    .line 3025
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 3026
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 3028
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v0, :cond_1

    .line 3029
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->startContinuousAF()V

    .line 3031
    :cond_1
    return-void
.end method

.method public resetTouchFocus()V
    .locals 2

    .prologue
    .line 4547
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v0, :cond_0

    .line 4562
    :goto_0
    return-void

    .line 4551
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isTouchAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4552
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->stopTouchAutoFocus()V

    .line 4553
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_1

    .line 4554
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 4557
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 4558
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 4560
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 4561
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    goto :goto_0
.end method

.method public resizePreviewAspectRatio()V
    .locals 11

    .prologue
    const v10, 0x7f0f0013

    const/16 v9, 0x11

    .line 5999
    const v7, 0x7f0f0012

    invoke-virtual {p0, v7}, Lcom/sec/android/app/bcocr/OCR;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    .line 6000
    .local v5, "surfaceView":Landroid/view/SurfaceView;
    const/4 v3, 0x0

    .line 6002
    .local v3, "previousRatio":F
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v8

    if-le v7, v8, :cond_4

    .line 6003
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v8

    int-to-float v8, v8

    div-float v3, v7, v8

    .line 6008
    :goto_0
    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 6009
    const/4 v3, 0x0

    .line 6012
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v7}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v8

    int-to-float v8, v8

    div-float v4, v7, v8

    .line 6014
    .local v4, "ratio":F
    sub-float v7, v3, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const v8, 0x3dcccccd    # 0.1f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_8

    .line 6015
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 6016
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090009

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v6, v7

    .line 6017
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09000a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v1, v7

    .line 6019
    .local v1, "height":I
    invoke-static {}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRActivityOrientation()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    .line 6020
    cmpg-float v7, v3, v4

    if-gez v7, :cond_5

    .line 6021
    int-to-float v7, v1

    div-float/2addr v7, v4

    float-to-int v6, v7

    .line 6033
    :cond_1
    :goto_1
    iput v6, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 6034
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 6036
    invoke-virtual {v5, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6038
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_2

    .line 6039
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v2, v6, v1, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 6040
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6042
    :cond_2
    invoke-virtual {p0, v10}, Lcom/sec/android/app/bcocr/OCR;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 6043
    .local v0, "captureAnim":Landroid/widget/ImageView;
    if-eqz v0, :cond_3

    .line 6044
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v2, v6, v1, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 6045
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6065
    .end local v0    # "captureAnim":Landroid/widget/ImageView;
    .end local v1    # "height":I
    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v6    # "width":I
    :cond_3
    :goto_2
    return-void

    .line 6005
    .end local v4    # "ratio":F
    :cond_4
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float v3, v7, v8

    goto/16 :goto_0

    .line 6022
    .restart local v1    # "height":I
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v4    # "ratio":F
    .restart local v6    # "width":I
    :cond_5
    cmpl-float v7, v3, v4

    if-lez v7, :cond_1

    .line 6023
    int-to-float v7, v6

    mul-float/2addr v7, v4

    float-to-int v1, v7

    .line 6025
    goto :goto_1

    .line 6026
    :cond_6
    cmpg-float v7, v3, v4

    if-gez v7, :cond_7

    .line 6027
    int-to-float v7, v6

    div-float/2addr v7, v4

    float-to-int v1, v7

    .line 6028
    goto :goto_1

    :cond_7
    cmpl-float v7, v3, v4

    if-lez v7, :cond_1

    .line 6029
    int-to-float v7, v1

    mul-float/2addr v7, v4

    float-to-int v6, v7

    goto :goto_1

    .line 6048
    .end local v1    # "height":I
    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v6    # "width":I
    :cond_8
    iget-boolean v7, p0, Lcom/sec/android/app/bcocr/OCR;->mIsConfigurationChanged:Z

    if-eqz v7, :cond_3

    .line 6049
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 6050
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v7

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 6051
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v7

    iput v7, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 6052
    invoke-virtual {v5, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6054
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_9

    .line 6055
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    invoke-direct {v2, v7, v8, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 6056
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6058
    :cond_9
    invoke-virtual {p0, v10}, Lcom/sec/android/app/bcocr/OCR;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 6059
    .restart local v0    # "captureAnim":Landroid/widget/ImageView;
    if-eqz v0, :cond_3

    .line 6060
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    invoke-direct {v2, v7, v8, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 6061
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public restartTouchAF()V
    .locals 0

    .prologue
    .line 2872
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->cancelTouchAutoFocus()V

    .line 2873
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->restartTouchAutoFocus()V

    .line 2874
    return-void
.end method

.method public restartTouchAutoFocus()V
    .locals 2

    .prologue
    .line 2877
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_1

    .line 2878
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->isTouchAutoFocusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2879
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    if-eqz v0, :cond_0

    .line 2880
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 2881
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2883
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 2885
    :cond_1
    return-void
.end method

.method public resumeAudioPlayback()V
    .locals 2

    .prologue
    .line 756
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCR;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    .line 758
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 761
    :cond_0
    return-void
.end method

.method public saveDataToFile(Ljava/lang/String;[B)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "data"    # [B

    .prologue
    .line 4399
    const/4 v1, 0x0

    .line 4401
    .local v1, "f":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4402
    .end local v1    # "f":Ljava/io/FileOutputStream;
    .local v2, "f":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4406
    if-eqz v2, :cond_0

    .line 4408
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 4414
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v1, v2

    .end local v2    # "f":Ljava/io/FileOutputStream;
    .restart local v1    # "f":Ljava/io/FileOutputStream;
    :goto_1
    return v3

    .line 4403
    :catch_0
    move-exception v0

    .line 4406
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    if-eqz v1, :cond_1

    .line 4408
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4404
    :cond_1
    :goto_3
    const/4 v3, 0x0

    goto :goto_1

    .line 4405
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 4406
    :goto_4
    if-eqz v1, :cond_2

    .line 4408
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 4413
    :cond_2
    :goto_5
    throw v3

    .line 4409
    .restart local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    goto :goto_3

    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    goto :goto_5

    .end local v1    # "f":Ljava/io/FileOutputStream;
    .restart local v2    # "f":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v3

    goto :goto_0

    .line 4405
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "f":Ljava/io/FileOutputStream;
    .restart local v1    # "f":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 4403
    .end local v1    # "f":Ljava/io/FileOutputStream;
    .restart local v2    # "f":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "f":Ljava/io/FileOutputStream;
    .restart local v1    # "f":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public setAdvancedMacroFocusActive(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 3034
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->USE_CAMERA_FOCUS_ADVANCED:Z

    if-eqz v0, :cond_0

    .line 3035
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCR;->mAdvancedMacroFocusActive:Z

    .line 3039
    :goto_0
    return-void

    .line 3037
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mAdvancedMacroFocusActive:Z

    goto :goto_0
.end method

.method public setCaptureStates(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 5555
    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureStates:I

    if-eq v0, p1, :cond_0

    .line 5556
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CaptureState changed("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStrCaptureStates:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureStates:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR;->mStrCaptureStates:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5557
    iput p1, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureStates:I

    .line 5559
    :cond_0
    return-void
.end method

.method public setDvfsBooster(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    .line 1835
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] setDvfsBooster "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1836
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 1837
    new-instance v0, Landroid/os/DVFSHelper;

    const/4 v1, 0x1

    const-wide/16 v2, 0xa

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;IJ)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 1839
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mDvfsHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 1840
    return-void
.end method

.method public setLastCapturedTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 4395
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCREngine;->setLastCapturedTitle(Ljava/lang/String;)V

    .line 4396
    return-void
.end method

.method public setLastContentUri(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 4386
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCREngine;->setLastContentUri(Landroid/net/Uri;)V

    .line 4387
    return-void
.end method

.method public setOCRActionState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 4805
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] [mycheck]setOCRActionState("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4806
    iput p1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRActionState:I

    .line 4809
    return-void
.end method

.method public setOCROrientationPreviewBuf(IIIII)Z
    .locals 12
    .param p1, "orient"    # I
    .param p2, "marginLeft"    # I
    .param p3, "marginTop"    # I
    .param p4, "marginRight"    # I
    .param p5, "marginBottom"    # I

    .prologue
    .line 3725
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 3726
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] OCR setOCROrientationPreviewBuf : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3728
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    array-length v3, v1

    .line 3731
    .local v3, "previewBufSize":I
    if-lez v3, :cond_2

    .line 3732
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    array-length v1, v1

    if-eq v3, v1, :cond_2

    .line 3733
    :cond_1
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    .line 3736
    :cond_2
    const/16 v1, 0x5a

    if-ne p1, v1, :cond_4

    .line 3737
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    .line 3738
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    .line 3739
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    .line 3740
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewHeight:I

    .line 3742
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    if-ne v1, v2, :cond_3

    .line 3743
    iput p2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3744
    iput p3, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3745
    move/from16 v0, p4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3746
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    .line 3754
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v5, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v6, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    .line 3755
    iget v7, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    iget v9, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    iget v10, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    move-object v1, p0

    .line 3754
    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/app/bcocr/OCR;->OCRByteArrayRotate90([BI[BIIIIII)Z

    move-result v11

    .line 3818
    .local v11, "result":Z
    :goto_1
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    sub-int/2addr v1, p2

    sub-int v1, v1, p4

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectWidth:I

    .line 3819
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    sub-int/2addr v1, p3

    sub-int v1, v1, p5

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorDetectHeight:I

    .line 3821
    return v11

    .line 3748
    .end local v11    # "result":Z
    :cond_3
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p2

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3749
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p3

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3750
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p4

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3751
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p5

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    goto :goto_0

    .line 3756
    :cond_4
    const/16 v1, 0xb4

    if-ne p1, v1, :cond_6

    .line 3757
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    .line 3758
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    .line 3759
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    .line 3760
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewHeight:I

    .line 3762
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    if-ne v1, v2, :cond_5

    .line 3763
    iput p2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3764
    iput p3, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3765
    move/from16 v0, p4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3766
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    .line 3774
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v5, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v6, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    .line 3775
    iget v7, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    iget v9, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    iget v10, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    move-object v1, p0

    .line 3774
    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/app/bcocr/OCR;->OCRByteArrayRotate180([BI[BIIIIII)Z

    move-result v11

    .line 3776
    .restart local v11    # "result":Z
    goto :goto_1

    .line 3768
    .end local v11    # "result":Z
    :cond_5
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p2

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3769
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p3

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3770
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p4

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3771
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p5

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    goto :goto_2

    .line 3776
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x10e

    if-ne p1, v1, :cond_8

    .line 3777
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    .line 3778
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    .line 3779
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    .line 3780
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewHeight:I

    .line 3782
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    if-ne v1, v2, :cond_7

    .line 3783
    iput p2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3784
    iput p3, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3785
    move/from16 v0, p4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3786
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    .line 3794
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v5, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v6, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    .line 3795
    iget v7, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    iget v9, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    iget v10, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    move-object v1, p0

    .line 3794
    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/app/bcocr/OCR;->OCRByteArrayRotate270([BI[BIIIIII)Z

    move-result v11

    .line 3796
    .restart local v11    # "result":Z
    goto/16 :goto_1

    .line 3788
    .end local v11    # "result":Z
    :cond_7
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p2

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3789
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p3

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3790
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p4

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3791
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p5

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    goto :goto_3

    .line 3797
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    .line 3798
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorHeight:I

    .line 3799
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    .line 3800
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewHeight:I

    .line 3802
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    if-ne v1, v2, :cond_9

    .line 3803
    iput p2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3804
    iput p3, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3805
    move/from16 v0, p4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3806
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    .line 3814
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRotateTemp:[B

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v5, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    iget v6, v1, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    .line 3815
    iget v7, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    iget v8, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    iget v9, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    iget v10, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    move-object v1, p0

    .line 3814
    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/app/bcocr/OCR;->OCRByteArrayRotate0([BI[BIIIIII)Z

    move-result v11

    .restart local v11    # "result":Z
    goto/16 :goto_1

    .line 3808
    .end local v11    # "result":Z
    :cond_9
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p2

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginLeft:I

    .line 3809
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int/2addr v1, p3

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginTop:I

    .line 3810
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p4

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginRight:I

    .line 3811
    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewSensorWidth:I

    mul-int v1, v1, p5

    iget v2, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/bcocr/OCR;->nOCRPreviewMarginBottom:I

    goto :goto_4
.end method

.method public setPreviewButtonDisable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5356
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f001f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 5357
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f0021

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 5358
    return-void
.end method

.method public setPreviewButtonEnable()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5351
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f001f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 5352
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f0021

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 5353
    return-void
.end method

.method public setSideMenu(IIZZ)V
    .locals 6
    .param p1, "bLeftSidemenuMode"    # I
    .param p2, "bRightSidemenuMode"    # I
    .param p3, "bFadeAnimation"    # Z
    .param p4, "bSideMenuShow"    # Z

    .prologue
    const v5, 0x7f0f0021

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1777
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] setSideMenu : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1832
    :cond_0
    :goto_0
    return-void

    .line 1783
    :cond_1
    iput-boolean p4, p0, Lcom/sec/android/app/bcocr/OCR;->mSideMenuShowState:Z

    .line 1785
    if-eqz p4, :cond_2

    .line 1787
    if-ne p1, v3, :cond_3

    .line 1788
    iput p1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    .line 1789
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCROrientDetectState:Z

    if-eqz v0, :cond_2

    .line 1790
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRActionState()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 1791
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getTouchAutoFocusActive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1792
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1793
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1794
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_2

    .line 1795
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 1804
    :cond_2
    :goto_1
    if-nez p4, :cond_4

    .line 1805
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1807
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    invoke-virtual {v0, v4, v1, v3}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    .line 1826
    :goto_2
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    .line 1827
    invoke-virtual {p0, v4}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 1828
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 1829
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 1830
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->replaceFocusPosition()V

    goto :goto_0

    .line 1799
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1800
    iput p1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentLeftSidemenuMode:I

    goto :goto_1

    .line 1810
    :cond_4
    if-eq p2, v3, :cond_5

    .line 1811
    if-nez p2, :cond_7

    iget v0, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    if-ne v0, v3, :cond_7

    .line 1814
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1815
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 1816
    const/16 v1, 0x5155

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1818
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1820
    iput-boolean v3, p0, Lcom/sec/android/app/bcocr/OCR;->mOCRCaptureMode:Z

    .line 1821
    iput v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    .line 1823
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getMenuDimController()Lcom/sec/android/app/bcocr/MenuDimController;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFragmentRightSidemenuMode:I

    invoke-virtual {v0, v4, v1, v4}, Lcom/sec/android/app/bcocr/MenuDimController;->refreshButtonDim(IIZ)V

    goto :goto_2
.end method

.method public setTouchAutoFocusActive(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3051
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_TOUCH_AF:Z

    if-eqz v0, :cond_0

    .line 3052
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCR;->mTouchAutoFocusActive:Z

    .line 3053
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->setAdvancedMacroFocusActive(Z)V

    .line 3057
    :goto_0
    return-void

    .line 3055
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mTouchAutoFocusActive:Z

    goto :goto_0
.end method

.method public setZoomValue(I)V
    .locals 1
    .param p1, "zoomValue"    # I

    .prologue
    .line 3527
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->resetFocusDueToZoom()V

    .line 3528
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/OCRSettings;->setZoomValue(I)V

    .line 3529
    return-void
.end method

.method public setmIsAdvanceFocusFocusing(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "state"    # Ljava/lang/Boolean;

    .prologue
    .line 5939
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    .line 5940
    return-void
.end method

.method public showApplicationDisabledPopup(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 6109
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 6110
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    .line 6111
    const-string v2, "com.sec.android.app.popupuireceiver.DisableApp"

    .line 6110
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6112
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6113
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCR;->startActivityForResult(Landroid/content/Intent;I)V

    .line 6114
    return-void
.end method

.method public showCaptureProgressTimer()V
    .locals 4

    .prologue
    .line 5307
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 5308
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 5309
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v1, :cond_0

    .line 5310
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 5312
    :cond_0
    return-void
.end method

.method public showFocusIndicator(I)V
    .locals 10
    .param p1, "state"    # I

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f666666    # 0.9f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 5957
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    if-nez v3, :cond_1

    .line 5996
    :cond_0
    :goto_0
    return-void

    .line 5961
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5962
    if-ne p1, v5, :cond_2

    .line 5963
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5964
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    const v4, 0x7f020022

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 5965
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5966
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 5968
    .local v0, "anim_base":Landroid/view/animation/Animation;
    const-wide/16 v8, 0x12c

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 5969
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x42b40000    # 90.0f

    move v7, v5

    move v8, v6

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 5971
    .local v2, "anim":Landroid/view/animation/Animation;
    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 5972
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 5973
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 5974
    .end local v0    # "anim_base":Landroid/view/animation/Animation;
    .end local v2    # "anim":Landroid/view/animation/Animation;
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_4

    .line 5975
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    const v3, 0x7f020023

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 5976
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5977
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->startAFHideRectTimer()V

    .line 5979
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] check camera focus : showFocusIndicator success, mIsAdvanceFocusFocusing : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5980
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    if-eqz v1, :cond_3

    .line 5981
    iput-boolean v7, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    goto :goto_0

    .line 5983
    :cond_3
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    goto :goto_0

    .line 5985
    :cond_4
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 5986
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;

    const v3, 0x7f020025

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 5987
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->startAFHideRectTimer()V

    .line 5989
    const-string v1, "OCR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[OCR] check camera focus : showFocusIndicator success, mIsAdvanceFocusFocusing : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5990
    iget-boolean v1, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    if-eqz v1, :cond_5

    .line 5991
    iput-boolean v7, p0, Lcom/sec/android/app/bcocr/OCR;->mIsAdvanceFocusFocusing:Z

    goto/16 :goto_0

    .line 5993
    :cond_5
    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCR;->mIsBlurDetection:Z

    goto/16 :goto_0
.end method

.method public showNetworkStateDialog(IILjava/lang/String;)V
    .locals 7
    .param p1, "title"    # I
    .param p2, "message"    # I
    .param p3, "preferenceKey"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1096
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030001

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1097
    .local v1, "checkBoxLayout":Landroid/view/View;
    const v4, 0x7f0f0003

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1098
    .local v2, "messageView":Landroid/widget/TextView;
    const v4, 0x7f0f0004

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1100
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    .line 1102
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mNetworkStateDialog:Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    .line 1103
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mNetworkStateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->dismiss()V

    .line 1106
    :cond_0
    sget v4, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 1107
    const-string v3, "#000000"

    .line 1112
    .local v3, "textColor":Ljava/lang/String;
    :goto_0
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1113
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 1115
    iput-object v6, p0, Lcom/sec/android/app/bcocr/OCR;->mNetworkStateDialog:Landroid/app/AlertDialog;

    .line 1116
    new-instance v4, Landroid/app/AlertDialog$Builder;

    sget v5, Lcom/sec/android/app/bcocr/Feature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    invoke-direct {v4, p0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1117
    invoke-virtual {v4, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1119
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1120
    const v5, 0x7f0c001b

    new-instance v6, Lcom/sec/android/app/bcocr/OCR$12;

    invoke-direct {v6, p0, v0, p3}, Lcom/sec/android/app/bcocr/OCR$12;-><init>(Lcom/sec/android/app/bcocr/OCR;Landroid/widget/CheckBox;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1132
    const v5, 0x7f0c001a

    new-instance v6, Lcom/sec/android/app/bcocr/OCR$13;

    invoke-direct {v6, p0}, Lcom/sec/android/app/bcocr/OCR$13;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1137
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    .line 1116
    iput-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mNetworkStateDialog:Landroid/app/AlertDialog;

    .line 1138
    return-void

    .line 1109
    .end local v3    # "textColor":Ljava/lang/String;
    :cond_1
    const-string v3, "#f5f5f5"

    .restart local v3    # "textColor":Ljava/lang/String;
    goto :goto_0
.end method

.method public shutterButtonCancelAction()V
    .locals 2

    .prologue
    .line 4689
    const-string v0, "OCR"

    const-string v1, "[OCR] shutterButtonCancelAction"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4690
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->stopAFWaitTimer()V

    .line 4691
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 4692
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 4694
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->cancelAutoFocus()V

    .line 4695
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 4696
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 4697
    return-void
.end method

.method public startAFHideRectTimer()V
    .locals 4

    .prologue
    .line 5943
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 5945
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xd0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 5946
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 5947
    return-void
.end method

.method public declared-synchronized startCaptureAnimation()V
    .locals 2

    .prologue
    .line 4629
    monitor-enter p0

    const v1, 0x7f0f0013

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/OCR;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4630
    .local v0, "view":Landroid/widget/ImageView;
    const/high16 v1, 0x7f040000

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4631
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4632
    monitor-exit p0

    return-void

    .line 4629
    .end local v0    # "view":Landroid/widget/ImageView;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public startContinuousAF()V
    .locals 1

    .prologue
    .line 4767
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    .line 4768
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->startContinuousAF()V

    .line 4770
    :cond_0
    return-void
.end method

.method public startVoiceRecognizer()V
    .locals 8

    .prologue
    const/4 v7, 0x7

    .line 6117
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-nez v4, :cond_0

    .line 6118
    new-instance v4, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v4}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 6119
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    new-instance v5, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;-><init>(Lcom/sec/android/app/bcocr/OCR;Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->InitBargeInRecognizer(Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;)V

    .line 6122
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v4, :cond_2

    .line 6123
    const-string v4, "OCR"

    const-string v5, "######VR start voice recognition"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6124
    sget-boolean v4, Lcom/sec/android/app/bcocr/Feature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    if-nez v4, :cond_f

    .line 6125
    const/4 v3, 0x1

    .line 6126
    .local v3, "useLanguage":I
    const-string v2, ""

    .line 6127
    .local v2, "language":Ljava/lang/String;
    const-string v0, ""

    .line 6129
    .local v0, "country":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 6130
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v4, :cond_1

    .line 6131
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 6132
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 6135
    :cond_1
    const-string v4, "ko"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 6136
    const/4 v3, 0x0

    .line 6167
    :goto_0
    const-string v4, "OCR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "######VR start voice recognition , useLanguage : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6169
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    const/4 v5, 0x7

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6178
    .end local v0    # "country":Ljava/lang/String;
    .end local v2    # "language":Ljava/lang/String;
    .end local v3    # "useLanguage":I
    :cond_2
    :goto_1
    return-void

    .line 6137
    .restart local v0    # "country":Ljava/lang/String;
    .restart local v2    # "language":Ljava/lang/String;
    .restart local v3    # "useLanguage":I
    :cond_3
    const-string v4, "en"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 6138
    const-string v4, "gb"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 6139
    const/16 v3, 0xa

    .line 6140
    goto :goto_0

    .line 6141
    :cond_4
    const/4 v3, 0x1

    .line 6143
    goto :goto_0

    :cond_5
    const-string v4, "zh"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 6144
    invoke-static {}, Lcom/sec/android/app/bcocr/FeatureManage;->isChinaFeature()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "CN"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 6145
    const/4 v3, 0x2

    .line 6146
    goto :goto_0

    .line 6147
    :cond_6
    const/4 v3, 0x1

    .line 6149
    goto :goto_0

    :cond_7
    const-string v4, "es"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 6150
    const/4 v3, 0x3

    .line 6151
    goto :goto_0

    :cond_8
    const-string v4, "fr"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 6152
    const/4 v3, 0x4

    .line 6153
    goto :goto_0

    :cond_9
    const-string v4, "de"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 6154
    const/4 v3, 0x5

    .line 6155
    goto :goto_0

    :cond_a
    const-string v4, "it"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 6156
    const/4 v3, 0x6

    .line 6157
    goto :goto_0

    :cond_b
    const-string v4, "ja"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 6158
    const/4 v3, 0x7

    .line 6159
    goto :goto_0

    :cond_c
    const-string v4, "ru"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 6160
    const/16 v3, 0x8

    .line 6161
    goto/16 :goto_0

    :cond_d
    const-string v4, "pt"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 6162
    const/16 v3, 0x9

    .line 6163
    goto/16 :goto_0

    .line 6164
    :cond_e
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 6170
    :catch_0
    move-exception v1

    .line 6171
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 6172
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(I)V

    goto/16 :goto_1

    .line 6175
    .end local v0    # "country":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "language":Ljava/lang/String;
    .end local v3    # "useLanguage":I
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(I)V

    goto/16 :goto_1
.end method

.method public stopAFHideRectTimer()V
    .locals 2

    .prologue
    .line 5950
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    if-eqz v0, :cond_0

    .line 5951
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mMainHandler:Lcom/sec/android/app/bcocr/OCR$MainHandler;

    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;->removeMessages(I)V

    .line 5954
    :cond_0
    return-void
.end method

.method public stopCameraSound(I)V
    .locals 2
    .param p1, "Sound"    # I

    .prologue
    .line 4245
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 4246
    const-string v0, "OCR"

    const-string v1, "[OCR] stopCameraSound - mSoundPool is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4255
    :goto_0
    return-void

    .line 4250
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamId:[I

    if-nez v0, :cond_1

    .line 4251
    const-string v0, "OCR"

    const-string v1, "[OCR] stopCameraSound - mStreamId is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4254
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mSoundPool:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mStreamId:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    goto :goto_0
.end method

.method public stopContinuousAF()V
    .locals 1

    .prologue
    .line 4774
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    .line 4775
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->stopContinuousAF()V

    .line 4777
    :cond_0
    return-void
.end method

.method public stopVoiceRecognizer()V
    .locals 2

    .prologue
    .line 6181
    const-string v0, "OCR"

    const-string v1, "###### stop voice recognition"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6182
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v0, :cond_0

    .line 6183
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 6186
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 6187
    return-void
.end method

.method public updateDetectedNameCardEdge()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5213
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCR;->checkCaptureCondition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5214
    const-string v0, "OCR"

    const-string v1, "[EdgeCapture] checkCaptureCondition true"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5215
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mIsEdgeDetected:Z

    .line 5217
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5218
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 5219
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->setAdvancedMacroFocusActive(Z)V

    .line 5220
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCR;->handleMacroFocus(Z)V

    .line 5221
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCR;->mChkAllowFocusTouch:Z

    .line 5223
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 5236
    :goto_0
    return-void

    .line 5225
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-eqz v0, :cond_1

    .line 5226
    const-string v0, "OCR"

    const-string v1, "[EdgeCapture] OCREngine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5228
    :cond_1
    const-string v0, "OCR"

    const-string v1, "[EdgeCapture] auto focusing state"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5232
    :cond_2
    const-string v0, "OCR"

    const-string v1, "[EdgeCapture] checkCaptureCondition fail"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCR;->mIsEdgeDetected:Z

    .line 5234
    const/16 v0, 0xd

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V

    goto :goto_0
.end method

.method public waitForPrevRecognizeThread()V
    .locals 2

    .prologue
    .line 5525
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 5527
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mPreviewRecogThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5532
    :cond_0
    :goto_0
    return-void

    .line 5528
    :catch_0
    move-exception v0

    .line 5529
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public waitForRecognizeThread()V
    .locals 2

    .prologue
    .line 5535
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 5537
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mCaptureRecogThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5542
    :cond_0
    :goto_0
    return-void

    .line 5538
    :catch_0
    move-exception v0

    .line 5539
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public waitForUpdateCheckThread()V
    .locals 2

    .prologue
    .line 5545
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    if-eqz v1, :cond_0

    .line 5547
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5552
    :cond_0
    :goto_0
    return-void

    .line 5548
    :catch_0
    move-exception v0

    .line 5549
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
