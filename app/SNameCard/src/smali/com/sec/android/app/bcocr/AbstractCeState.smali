.class public abstract Lcom/sec/android/app/bcocr/AbstractCeState;
.super Ljava/lang/Object;
.source "AbstractCeState.java"


# static fields
.field public static final CE_STATE_END_OF_DEF:I = 0x7

.field public static final CE_STATE_IDLE:I = 0x0

.field public static final CE_STATE_INITIALIZED:I = 0x2

.field public static final CE_STATE_INITIALIZING:I = 0x1

.field public static final CE_STATE_PREVIEWING:I = 0x4

.field public static final CE_STATE_RECORDING:I = 0x5

.field public static final CE_STATE_SHUTDOWN:I = 0x7

.field public static final CE_STATE_STARTING_PREVIEW:I = 0x3

.field public static final CE_STATE_WAIT_FOR_SURFACE:I = 0x6

.field protected static final TAG:Ljava/lang/String; = "AbstractCeState"


# instance fields
.field private mId:I

.field private mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

.field private mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V
    .locals 0
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;
    .param p2, "requestQueue"    # Lcom/sec/android/app/bcocr/CeRequestQueue;
    .param p3, "id"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    .line 43
    iput p3, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mId:I

    .line 44
    return-void
.end method


# virtual methods
.method public abstract cancelRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mId:I

    return v0
.end method

.method protected getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    return-object v0
.end method

.method protected getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/bcocr/AbstractCeState;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    return-object v0
.end method

.method public abstract handleMessage(Landroid/os/Message;)V
.end method

.method public abstract handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
.end method
