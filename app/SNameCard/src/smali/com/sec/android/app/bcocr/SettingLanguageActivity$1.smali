.class Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;
.super Ljava/lang/Object;
.source "SettingLanguageActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/SettingLanguageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x7

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$2(Lcom/sec/android/app/bcocr/SettingLanguageActivity;I)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 95
    const/16 v0, 0x14

    if-ne p2, v0, :cond_2

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 103
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 97
    :cond_2
    const/16 v0, 0x13

    if-ne p2, v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$1(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/SettingLanguageActivity$1;->this$0:Lcom/sec/android/app/bcocr/SettingLanguageActivity;

    # getter for: Lcom/sec/android/app/bcocr/SettingLanguageActivity;->selectedItemOfListView:I
    invoke-static {v1}, Lcom/sec/android/app/bcocr/SettingLanguageActivity;->access$3(Lcom/sec/android/app/bcocr/SettingLanguageActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method
