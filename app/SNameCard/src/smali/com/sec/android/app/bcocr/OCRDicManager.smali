.class public Lcom/sec/android/app/bcocr/OCRDicManager;
.super Ljava/lang/Object;
.source "OCRDicManager.java"


# static fields
.field public static final ACTION_SEC_TRANSLATE:Ljava/lang/String; = "com.sec.android.app.translator.TRANSLATE"

.field public static final ACTION_SEC_TRANSLATE_GET_SUPPORTED_LANGUAGES:Ljava/lang/String; = "com.sec.android.app.translator.GET_SUPPORTED_LANGUAGES"

.field public static final ACTION_SEC_TRANSLATE_RESULT:Ljava/lang/String; = "com.sec.android.app.translator.TRANSLATE_RESULT"

.field public static final DEFAULT_DIC_LANG:I = 0x0

.field public static final EXTRA_NAME_CALLER:Ljava/lang/String; = "caller"

.field public static final EXTRA_NAME_EDITABLE_SOURCETEXT:Ljava/lang/String; = "editable_source_text"

.field public static final EXTRA_NAME_ENABLE_SHARE_VIA_MENU:Ljava/lang/String; = "enable_share_via_menu"

.field public static final EXTRA_NAME_HTML_SOURCE_TEXT:Ljava/lang/String; = "html_source_text"

.field public static final EXTRA_NAME_MODE:Ljava/lang/String; = "mode"

.field public static final EXTRA_NAME_RESULT_CODE:Ljava/lang/String; = "result_code"

.field public static final EXTRA_NAME_SOURCE_LANGUAGE:Ljava/lang/String; = "source_language"

.field public static final EXTRA_NAME_SOURCE_TEXT:Ljava/lang/String; = "source_text"

.field public static final EXTRA_NAME_SUPPORTED_LANGUAGES:Ljava/lang/String; = "supported_languages"

.field public static final EXTRA_NAME_TARGET_LANGUAGE:Ljava/lang/String; = "target_language"

.field public static final EXTRA_NAME_TARGET_TEXT:Ljava/lang/String; = "target_text"

.field public static final EXTRA_NAME_TUTORIAL_MODE:Ljava/lang/String; = "tutorial_mode"

.field public static final EXTRA_NAME_USE_BROADCAST_RECEIVER:Ljava/lang/String; = "use_broadcast_receiver"

.field public static final EXTRA_VALUE_INPUT_MODE:Ljava/lang/String; = "input"

.field public static final EXTRA_VALUE_RESULT_CANCEL:I = 0x0

.field public static final EXTRA_VALUE_RESULT_OK:I = 0x1

.field public static final EXTRA_VALUE_VIEWER_MODE:Ljava/lang/String; = "viewer"

.field public static final OCR_DIC_LANG_AFRIKAANS:I = 0x22

.field public static final OCR_DIC_LANG_ALBANIAN:I = 0x2f

.field public static final OCR_DIC_LANG_ARABIC:I = 0x2

.field public static final OCR_DIC_LANG_BASQUE:I = 0x26

.field public static final OCR_DIC_LANG_BULGARIAN:I = 0x23

.field public static final OCR_DIC_LANG_CATALAN:I = 0x24

.field public static final OCR_DIC_LANG_CHINESE_SIM:I = 0x3

.field public static final OCR_DIC_LANG_CHINESE_TRA:I = 0x4

.field public static final OCR_DIC_LANG_CROATIAN:I = 0x5

.field public static final OCR_DIC_LANG_CZECH:I = 0x6

.field public static final OCR_DIC_LANG_DANISH:I = 0x7

.field public static final OCR_DIC_LANG_DUTCH:I = 0x8

.field public static final OCR_DIC_LANG_ENGLISH_UK:I = 0x1

.field public static final OCR_DIC_LANG_ENGLISH_US:I = 0x0

.field public static final OCR_DIC_LANG_ESTONIAN:I = 0x25

.field public static final OCR_DIC_LANG_FARSI:I = 0x15

.field public static final OCR_DIC_LANG_FINNISH:I = 0x9

.field public static final OCR_DIC_LANG_FRENCH:I = 0xa

.field public static final OCR_DIC_LANG_GERMAN:I = 0xb

.field public static final OCR_DIC_LANG_GREEK:I = 0xc

.field public static final OCR_DIC_LANG_HINDI:I = 0xd

.field public static final OCR_DIC_LANG_HUNGARIAN:I = 0x27

.field public static final OCR_DIC_LANG_ICELANDIC:I = 0x28

.field public static final OCR_DIC_LANG_INDONESIA:I = 0xe

.field public static final OCR_DIC_LANG_IRISH:I = 0x10

.field public static final OCR_DIC_LANG_ITALIAN:I = 0xf

.field public static final OCR_DIC_LANG_JAPANESE:I = 0x11

.field public static final OCR_DIC_LANG_KOREAN:I = 0x12

.field public static final OCR_DIC_LANG_LATVIAN:I = 0x2a

.field public static final OCR_DIC_LANG_LITHUANIAN:I = 0x29

.field public static final OCR_DIC_LANG_MACEDONIAN:I = 0x2b

.field public static final OCR_DIC_LANG_MALAY:I = 0x13

.field public static final OCR_DIC_LANG_NORWEGIAN:I = 0x14

.field public static final OCR_DIC_LANG_POLISH:I = 0x16

.field public static final OCR_DIC_LANG_PORTUGUESE:I = 0x17

.field public static final OCR_DIC_LANG_PORTUGUESE_BRA:I = 0x18

.field public static final OCR_DIC_LANG_ROMANIAN:I = 0x2c

.field public static final OCR_DIC_LANG_RUSSIAN:I = 0x19

.field public static final OCR_DIC_LANG_SERBIAN:I = 0x30

.field public static final OCR_DIC_LANG_SLOVAK:I = 0x2d

.field public static final OCR_DIC_LANG_SLOVENIAN:I = 0x2e

.field public static final OCR_DIC_LANG_SPANISH:I = 0x1a

.field public static final OCR_DIC_LANG_SPANISH_RATIN:I = 0x1b

.field public static final OCR_DIC_LANG_SWEDISH:I = 0x1c

.field public static final OCR_DIC_LANG_THAI:I = 0x1d

.field public static final OCR_DIC_LANG_TURKISH:I = 0x1e

.field public static final OCR_DIC_LANG_UKRAINIAN:I = 0x1f

.field public static final OCR_DIC_LANG_URDU:I = 0x20

.field public static final OCR_DIC_LANG_VIETNAMESE:I = 0x21

.field public static final REQUEST_TRANSLATE:I = 0xc9

.field public static final SECOND_DIC_LANG:I = 0xa

.field protected static final TAG:Ljava/lang/String; = "OCRDicManager"

.field public static final TRANSLATORLANGUAGE_CODE_ENGLISH_US:Ljava/lang/String; = "en-US"

.field public static final TRANSLATORLANGUAGE_CODE_FRENCH:Ljava/lang/String; = "fr-FR"

.field public static final TRANSLATORLANGUAGE_CODE_GERMAN:Ljava/lang/String; = "de-DE"

.field public static final TRANSLATORLANGUAGE_CODE_ITALIAN:Ljava/lang/String; = "it-IT"

.field public static final TRANSLATORLANGUAGE_CODE_JAPANESE:Ljava/lang/String; = "ja-JP"

.field public static final TRANSLATORLANGUAGE_CODE_KOREAN:Ljava/lang/String; = "ko-KR"

.field public static final TRANSLATORLANGUAGE_CODE_PORTUGUESE_BR:Ljava/lang/String; = "pt-BR"

.field public static final TRANSLATORLANGUAGE_CODE_SPANISH:Ljava/lang/String; = "es-ES"

.field public static final TRANSLATOR_LANGUAGE_CODE_AUTO:Ljava/lang/String; = "unknown"

.field public static final TRANSLATOR_LANGUAGE_CODE_CHINESE:Ljava/lang/String; = "zh-CN"

.field public static final TRANSLATOR_LANGUAGE_CODE_ENGLISH_UK:Ljava/lang/String; = "en-GB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultOCREngineLanguageSet(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 375
    if-nez p0, :cond_1

    .line 376
    const/4 v0, 0x0

    .line 403
    :cond_0
    return-object v0

    .line 381
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070001

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 382
    .local v3, "engine_language":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f070000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 383
    .local v2, "engine_default_language":[I
    const-string v0, ""

    .line 385
    .local v0, "defaultLanguage":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v2

    if-ge v4, v5, :cond_0

    .line 386
    const/16 v5, 0x1b

    .line 385
    if-ge v4, v5, :cond_0

    .line 387
    aget v5, v2, v4

    invoke-static {p0, v5}, Lcom/sec/android/app/bcocr/OCRDicManager;->getOCREngineIndexLanguageID(Landroid/content/Context;I)I

    move-result v1

    .line 388
    .local v1, "engineLangIndex":I
    if-ltz v1, :cond_2

    array-length v5, v3

    if-ge v1, v5, :cond_2

    .line 389
    if-nez v4, :cond_3

    .line 390
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 391
    :cond_3
    aget v5, v2, v4

    const/16 v6, 0x46

    if-ge v5, v6, :cond_2

    .line 392
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "^"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 393
    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 392
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static getDicDstLangListIncDiodic(Landroid/content/Context;[I)[I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dic_id"    # [I

    .prologue
    .line 600
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07000d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 601
    .local v2, "dic_w2w_noway_list":[I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07000e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    .line 602
    .local v3, "dic_w2w_only_dst_list":[I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f070009

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    .line 607
    .local v4, "dict_dst_lang_id":[I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 609
    .local v0, "dicLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v10, "OCRDicManager"

    const-string v11, "[OCR] getDicDstLangListIncDiodic++"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v10, v2

    if-lt v7, v10, :cond_0

    .line 615
    const/4 v7, 0x0

    :goto_1
    array-length v10, v3

    if-lt v7, v10, :cond_1

    .line 619
    const/4 v7, 0x0

    :goto_2
    array-length v10, p1

    if-lt v7, v10, :cond_2

    .line 637
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v1, v10, [I

    .line 638
    .local v1, "dic_list":[I
    const/4 v7, 0x0

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v7, v10, :cond_6

    .line 641
    return-object v1

    .line 612
    .end local v1    # "dic_list":[I
    :cond_0
    aget v10, v2, v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 616
    :cond_1
    aget v10, v3, v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 620
    :cond_2
    const/4 v6, 0x0

    .line 621
    .local v6, "duplicate":Z
    aget v10, p1, v7

    invoke-static {p0, v10}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTRIndexBydicID(Landroid/content/Context;I)I

    move-result v8

    .line 622
    .local v8, "index":I
    if-lez v8, :cond_3

    .line 623
    aget v5, v4, v8

    .line 624
    .local v5, "dstLangId":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v9, v10, :cond_4

    .line 631
    :goto_5
    if-nez v6, :cond_3

    .line 632
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    .end local v5    # "dstLangId":I
    .end local v9    # "j":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 625
    .restart local v5    # "dstLangId":I
    .restart local v9    # "j":I
    :cond_4
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ne v5, v10, :cond_5

    .line 626
    const/4 v6, 0x1

    .line 627
    goto :goto_5

    .line 624
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 639
    .end local v5    # "dstLangId":I
    .end local v6    # "duplicate":Z
    .end local v8    # "index":I
    .end local v9    # "j":I
    .restart local v1    # "dic_list":[I
    :cond_6
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    aput v10, v1, v7

    .line 638
    add-int/lit8 v7, v7, 0x1

    goto :goto_3
.end method

.method public static getDicSrcLangListIncDiodic(Landroid/content/Context;[I)[I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dic_id"    # [I

    .prologue
    .line 559
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07000d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 560
    .local v2, "dic_w2w_noway_list":[I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070008

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    .line 565
    .local v3, "dict_src_lang_id":[I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 567
    .local v0, "dicLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v9, "OCRDicManager"

    const-string v10, "[OCR] getDicSrcLangListIncDiodic++"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v9, v2

    if-lt v5, v9, :cond_0

    .line 573
    const/4 v5, 0x0

    :goto_1
    array-length v9, p1

    if-lt v5, v9, :cond_1

    .line 591
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v1, v9, [I

    .line 592
    .local v1, "dic_list":[I
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v5, v9, :cond_5

    .line 595
    return-object v1

    .line 570
    .end local v1    # "dic_list":[I
    :cond_0
    aget v9, v2, v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 574
    :cond_1
    const/4 v4, 0x0

    .line 575
    .local v4, "duplicate":Z
    aget v9, p1, v5

    invoke-static {p0, v9}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTRIndexBydicID(Landroid/content/Context;I)I

    move-result v6

    .line 576
    .local v6, "index":I
    if-lez v6, :cond_2

    .line 577
    aget v8, v3, v6

    .line 578
    .local v8, "srcLangId":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    array-length v9, v2

    if-lt v7, v9, :cond_3

    .line 585
    :goto_4
    if-nez v4, :cond_2

    .line 586
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    .end local v7    # "j":I
    .end local v8    # "srcLangId":I
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 579
    .restart local v7    # "j":I
    .restart local v8    # "srcLangId":I
    :cond_3
    aget v9, v2, v7

    if-ne v8, v9, :cond_4

    .line 580
    const/4 v4, 0x1

    .line 581
    goto :goto_4

    .line 578
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 593
    .end local v4    # "duplicate":Z
    .end local v6    # "index":I
    .end local v7    # "j":I
    .end local v8    # "srcLangId":I
    .restart local v1    # "dic_list":[I
    :cond_5
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v1, v5

    .line 592
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public static getDicSrcLangListIncSelectedSrcLangByRecogLang(Landroid/content/Context;[II)[I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "OCREngineLanguageSelectedSet"    # [I
    .param p2, "langSrcID"    # I

    .prologue
    const/4 v1, 0x0

    .line 408
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07000d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 413
    .local v2, "dic_w2w_noway_list":[I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 414
    .local v4, "engineLangSelectedSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 416
    .local v0, "dicLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v9, "OCRDicManager"

    const-string v10, "[OCR] getDicSrcLangListIncDiodic++"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    if-eqz v4, :cond_0

    if-nez v0, :cond_2

    .line 419
    :cond_0
    const-string v9, "OCRDicManager"

    const-string v10, "[OCR] getDicSrcLangListIncDiodic:dicLists or engineLangList is null"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_1
    :goto_0
    return-object v1

    .line 423
    :cond_2
    if-nez p1, :cond_3

    .line 424
    const-string v9, "OCRDicManager"

    const-string v10, "[OCR] getDicSrcLangListIncDiodic:engineLangSet is null"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 428
    :cond_3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v9, p1

    if-lt v5, v9, :cond_4

    .line 551
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v1, v9, [I

    .line 552
    .local v1, "dic_list":[I
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v5, v9, :cond_1

    .line 553
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v1, v5

    .line 552
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 431
    .end local v1    # "dic_list":[I
    :cond_4
    aget v9, p1, v5

    if-eqz v9, :cond_8

    .line 432
    aget v9, p1, v5

    const/16 v10, 0x10

    if-ne v9, v10, :cond_11

    .line 434
    const/4 v7, 0x0

    .line 436
    .local v7, "langId":I
    const/4 v3, 0x0

    .line 437
    .local v3, "duplicate":Z
    const/4 v8, 0x0

    .line 438
    .local v8, "supportw2w":Z
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_9

    .line 444
    :goto_4
    if-nez v3, :cond_5

    .line 445
    const/4 v6, 0x0

    :goto_5
    array-length v9, v2

    if-lt v6, v9, :cond_b

    .line 452
    :cond_5
    :goto_6
    if-nez v3, :cond_6

    if-eqz v8, :cond_6

    .line 453
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    :cond_6
    const/4 v7, 0x1

    .line 459
    const/4 v3, 0x0

    .line 460
    const/4 v8, 0x0

    .line 461
    const/4 v6, 0x0

    :goto_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_d

    .line 467
    :goto_8
    if-nez v3, :cond_7

    .line 468
    const/4 v6, 0x0

    :goto_9
    array-length v9, v2

    if-lt v6, v9, :cond_f

    .line 475
    :cond_7
    :goto_a
    if-nez v3, :cond_8

    if-eqz v8, :cond_8

    .line 476
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    .end local v3    # "duplicate":Z
    .end local v6    # "j":I
    .end local v7    # "langId":I
    .end local v8    # "supportw2w":Z
    :cond_8
    :goto_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 439
    .restart local v3    # "duplicate":Z
    .restart local v6    # "j":I
    .restart local v7    # "langId":I
    .restart local v8    # "supportw2w":Z
    :cond_9
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v7, v9, :cond_a

    .line 440
    const/4 v3, 0x1

    .line 441
    goto :goto_4

    .line 438
    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 446
    :cond_b
    aget v9, v2, v6

    if-ne v7, v9, :cond_c

    .line 447
    const/4 v8, 0x1

    .line 448
    goto :goto_6

    .line 445
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 462
    :cond_d
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v7, v9, :cond_e

    .line 463
    const/4 v3, 0x1

    .line 464
    goto :goto_8

    .line 461
    :cond_e
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 469
    :cond_f
    aget v9, v2, v6

    if-ne v7, v9, :cond_10

    .line 470
    const/4 v8, 0x1

    .line 471
    goto :goto_a

    .line 468
    :cond_10
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 478
    .end local v3    # "duplicate":Z
    .end local v6    # "j":I
    .end local v7    # "langId":I
    .end local v8    # "supportw2w":Z
    :cond_11
    aget v9, p1, v5

    const/16 v10, 0x37

    if-ne v9, v10, :cond_1d

    .line 480
    const/16 v7, 0x1a

    .line 482
    .restart local v7    # "langId":I
    const/4 v3, 0x0

    .line 483
    .restart local v3    # "duplicate":Z
    const/4 v8, 0x0

    .line 484
    .restart local v8    # "supportw2w":Z
    const/4 v6, 0x0

    .restart local v6    # "j":I
    :goto_c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_15

    .line 490
    :goto_d
    if-nez v3, :cond_12

    .line 491
    const/4 v6, 0x0

    :goto_e
    array-length v9, v2

    if-lt v6, v9, :cond_17

    .line 498
    :cond_12
    :goto_f
    if-nez v3, :cond_13

    if-eqz v8, :cond_13

    .line 499
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_13
    const/16 v7, 0x1b

    .line 505
    const/4 v3, 0x0

    .line 506
    const/4 v8, 0x0

    .line 507
    const/4 v6, 0x0

    :goto_10
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_19

    .line 513
    :goto_11
    if-nez v3, :cond_14

    .line 514
    const/4 v6, 0x0

    :goto_12
    array-length v9, v2

    if-lt v6, v9, :cond_1b

    .line 521
    :cond_14
    :goto_13
    if-nez v3, :cond_8

    if-eqz v8, :cond_8

    .line 522
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 485
    :cond_15
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v7, v9, :cond_16

    .line 486
    const/4 v3, 0x1

    .line 487
    goto :goto_d

    .line 484
    :cond_16
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    .line 492
    :cond_17
    aget v9, v2, v6

    if-ne v7, v9, :cond_18

    .line 493
    const/4 v8, 0x1

    .line 494
    goto :goto_f

    .line 491
    :cond_18
    add-int/lit8 v6, v6, 0x1

    goto :goto_e

    .line 508
    :cond_19
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v7, v9, :cond_1a

    .line 509
    const/4 v3, 0x1

    .line 510
    goto :goto_11

    .line 507
    :cond_1a
    add-int/lit8 v6, v6, 0x1

    goto :goto_10

    .line 515
    :cond_1b
    aget v9, v2, v6

    if-ne v7, v9, :cond_1c

    .line 516
    const/4 v8, 0x1

    .line 517
    goto :goto_13

    .line 514
    :cond_1c
    add-int/lit8 v6, v6, 0x1

    goto :goto_12

    .line 525
    .end local v3    # "duplicate":Z
    .end local v6    # "j":I
    .end local v7    # "langId":I
    .end local v8    # "supportw2w":Z
    :cond_1d
    aget v9, p1, v5

    invoke-static {p0, v9}, Lcom/sec/android/app/bcocr/OCRDicManager;->getMocrID2LangID(Landroid/content/Context;I)I

    move-result v7

    .line 526
    .restart local v7    # "langId":I
    const/4 v9, -0x1

    if-eq v7, v9, :cond_8

    .line 527
    const/4 v3, 0x0

    .line 528
    .restart local v3    # "duplicate":Z
    const/4 v8, 0x0

    .line 529
    .restart local v8    # "supportw2w":Z
    const/4 v6, 0x0

    .restart local v6    # "j":I
    :goto_14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_1f

    .line 535
    :goto_15
    if-nez v3, :cond_1e

    .line 536
    const/4 v6, 0x0

    :goto_16
    array-length v9, v2

    if-lt v6, v9, :cond_21

    .line 543
    :cond_1e
    :goto_17
    if-nez v3, :cond_8

    if-eqz v8, :cond_8

    .line 544
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    .line 530
    :cond_1f
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v7, v9, :cond_20

    .line 531
    const/4 v3, 0x1

    .line 532
    goto :goto_15

    .line 529
    :cond_20
    add-int/lit8 v6, v6, 0x1

    goto :goto_14

    .line 537
    :cond_21
    aget v9, v2, v6

    if-ne v7, v9, :cond_22

    .line 538
    const/4 v8, 0x1

    .line 539
    goto :goto_17

    .line 536
    :cond_22
    add-int/lit8 v6, v6, 0x1

    goto :goto_16
.end method

.method public static getLangID2LangIDForDiodic(I)I
    .locals 0
    .param p0, "langID"    # I

    .prologue
    .line 661
    sparse-switch p0, :sswitch_data_0

    .line 669
    .end local p0    # "langID":I
    :goto_0
    return p0

    .line 663
    .restart local p0    # "langID":I
    :sswitch_0
    const/4 p0, 0x0

    goto :goto_0

    .line 665
    :sswitch_1
    const/16 p0, 0x17

    goto :goto_0

    .line 667
    :sswitch_2
    const/16 p0, 0x1a

    goto :goto_0

    .line 661
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1b -> :sswitch_2
    .end sparse-switch
.end method

.method public static getLangID2MocrID(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aLangID"    # I

    .prologue
    const/16 v1, 0x37

    const/16 v0, 0x10

    .line 130
    if-nez p0, :cond_0

    .line 235
    :goto_0
    :pswitch_0
    return v0

    .line 134
    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 142
    :pswitch_2
    const/16 v0, 0x41

    goto :goto_0

    .line 144
    :pswitch_3
    const/16 v0, 0x42

    goto :goto_0

    .line 146
    :pswitch_4
    const/16 v0, 0xb

    goto :goto_0

    .line 148
    :pswitch_5
    const/16 v0, 0xc

    goto :goto_0

    .line 150
    :pswitch_6
    const/16 v0, 0xd

    goto :goto_0

    .line 152
    :pswitch_7
    const/16 v0, 0xe

    goto :goto_0

    .line 154
    :pswitch_8
    const/16 v0, 0x13

    goto :goto_0

    .line 156
    :pswitch_9
    const/16 v0, 0x14

    goto :goto_0

    .line 158
    :pswitch_a
    const/16 v0, 0x15

    goto :goto_0

    .line 160
    :pswitch_b
    const/16 v0, 0x17

    goto :goto_0

    .line 164
    :pswitch_c
    const/16 v0, 0x1b

    goto :goto_0

    .line 166
    :pswitch_d
    const/16 v0, 0x1d

    goto :goto_0

    .line 168
    :pswitch_e
    const/16 v0, 0x1c

    goto :goto_0

    .line 170
    :pswitch_f
    const/16 v0, 0x43

    goto :goto_0

    .line 172
    :pswitch_10
    const/16 v0, 0x44

    goto :goto_0

    .line 174
    :pswitch_11
    const/16 v0, 0x23

    goto :goto_0

    .line 176
    :pswitch_12
    const/16 v0, 0x28

    goto :goto_0

    .line 180
    :pswitch_13
    const/16 v0, 0x2c

    goto :goto_0

    .line 182
    :pswitch_14
    const/16 v0, 0x2d

    goto :goto_0

    .line 184
    :pswitch_15
    const/16 v0, 0x2e

    goto :goto_0

    .line 186
    :pswitch_16
    const/16 v0, 0x32

    goto :goto_0

    :pswitch_17
    move v0, v1

    .line 188
    goto :goto_0

    :pswitch_18
    move v0, v1

    .line 190
    goto :goto_0

    .line 192
    :pswitch_19
    const/16 v0, 0x39

    goto :goto_0

    .line 196
    :pswitch_1a
    const/16 v0, 0x3c

    goto :goto_0

    .line 198
    :pswitch_1b
    const/16 v0, 0x3d

    goto :goto_0

    .line 205
    :pswitch_1c
    const/4 v0, 0x2

    goto :goto_0

    .line 207
    :pswitch_1d
    const/4 v0, 0x6

    goto :goto_0

    .line 209
    :pswitch_1e
    const/16 v0, 0x8

    goto :goto_0

    .line 211
    :pswitch_1f
    const/16 v0, 0x11

    goto :goto_0

    .line 213
    :pswitch_20
    const/4 v0, 0x4

    goto :goto_0

    .line 215
    :pswitch_21
    const/16 v0, 0x19

    goto :goto_0

    .line 217
    :pswitch_22
    const/16 v0, 0x1a

    goto :goto_0

    .line 219
    :pswitch_23
    const/16 v0, 0x21

    goto :goto_0

    .line 221
    :pswitch_24
    const/16 v0, 0x20

    goto :goto_0

    .line 223
    :pswitch_25
    const/16 v0, 0x22

    goto :goto_0

    .line 225
    :pswitch_26
    const/16 v0, 0x31

    goto :goto_0

    .line 227
    :pswitch_27
    const/16 v0, 0x35

    goto :goto_0

    .line 229
    :pswitch_28
    const/16 v0, 0x36

    goto :goto_0

    .line 231
    :pswitch_29
    const/4 v0, 0x3

    goto :goto_0

    .line 233
    :pswitch_2a
    const/16 v0, 0x34

    goto :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_1
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1
        :pswitch_1a
        :pswitch_1b
        :pswitch_1
        :pswitch_1
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
    .end packed-switch
.end method

.method public static getMocrID2LangID(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aEngineLangID"    # I

    .prologue
    const/4 v0, -0x1

    .line 241
    if-nez p0, :cond_0

    .line 341
    :goto_0
    return v0

    .line 245
    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 311
    :pswitch_1
    const/16 v0, 0x22

    goto :goto_0

    .line 247
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 251
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 253
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 255
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 257
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 259
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 261
    :pswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 263
    :pswitch_9
    const/16 v0, 0x9

    goto :goto_0

    .line 265
    :pswitch_a
    const/16 v0, 0xa

    goto :goto_0

    .line 267
    :pswitch_b
    const/16 v0, 0xb

    goto :goto_0

    .line 269
    :pswitch_c
    const/16 v0, 0xc

    goto :goto_0

    .line 273
    :pswitch_d
    const/16 v0, 0xe

    goto :goto_0

    .line 275
    :pswitch_e
    const/16 v0, 0xf

    goto :goto_0

    .line 277
    :pswitch_f
    const/16 v0, 0x10

    goto :goto_0

    .line 279
    :pswitch_10
    const/16 v0, 0x11

    goto :goto_0

    .line 281
    :pswitch_11
    const/16 v0, 0x12

    goto :goto_0

    .line 283
    :pswitch_12
    const/16 v0, 0x13

    goto :goto_0

    .line 285
    :pswitch_13
    const/16 v0, 0x14

    goto :goto_0

    .line 289
    :pswitch_14
    const/16 v0, 0x16

    goto :goto_0

    .line 291
    :pswitch_15
    const/16 v0, 0x17

    goto :goto_0

    .line 293
    :pswitch_16
    const/16 v0, 0x18

    goto :goto_0

    .line 295
    :pswitch_17
    const/16 v0, 0x19

    goto :goto_0

    .line 297
    :pswitch_18
    const/16 v0, 0x1a

    goto :goto_0

    .line 299
    :pswitch_19
    const/16 v0, 0x1c

    goto :goto_0

    .line 303
    :pswitch_1a
    const/16 v0, 0x1e

    goto :goto_0

    .line 305
    :pswitch_1b
    const/16 v0, 0x1f

    goto :goto_0

    .line 313
    :pswitch_1c
    const/16 v0, 0x23

    goto :goto_0

    .line 315
    :pswitch_1d
    const/16 v0, 0x24

    goto :goto_0

    .line 317
    :pswitch_1e
    const/16 v0, 0x25

    goto :goto_0

    .line 319
    :pswitch_1f
    const/16 v0, 0x26

    goto :goto_0

    .line 321
    :pswitch_20
    const/16 v0, 0x27

    goto :goto_0

    .line 323
    :pswitch_21
    const/16 v0, 0x28

    goto :goto_0

    .line 325
    :pswitch_22
    const/16 v0, 0x29

    goto :goto_0

    .line 327
    :pswitch_23
    const/16 v0, 0x2a

    goto :goto_0

    .line 329
    :pswitch_24
    const/16 v0, 0x2b

    goto :goto_0

    .line 331
    :pswitch_25
    const/16 v0, 0x2c

    goto :goto_0

    .line 333
    :pswitch_26
    const/16 v0, 0x2d

    goto :goto_0

    .line 335
    :pswitch_27
    const/16 v0, 0x2e

    goto :goto_0

    .line 337
    :pswitch_28
    const/16 v0, 0x2f

    goto :goto_0

    .line 339
    :pswitch_29
    const/16 v0, 0x30

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_28
        :pswitch_1f
        :pswitch_0
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_2
        :pswitch_1e
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_d
        :pswitch_f
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_22
        :pswitch_24
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_25
        :pswitch_17
        :pswitch_0
        :pswitch_29
        :pswitch_26
        :pswitch_27
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static getOCREngineIndexLanguageID(Landroid/content/Context;I)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "engineID"    # I

    .prologue
    const/4 v2, -0x1

    .line 346
    if-nez p0, :cond_1

    move v1, v2

    .line 356
    :cond_0
    :goto_0
    return v1

    .line 350
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 351
    .local v0, "engine_id":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-lt v1, v3, :cond_2

    move v1, v2

    .line 356
    goto :goto_0

    .line 352
    :cond_2
    aget v3, v0, v1

    if-eq v3, p1, :cond_0

    .line 351
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getOCREngineIndexLanguageString(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 360
    if-nez p0, :cond_1

    move v1, v2

    .line 370
    :cond_0
    :goto_0
    return v1

    .line 364
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 365
    .local v0, "engine_language":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-lt v1, v3, :cond_2

    move v1, v2

    .line 370
    goto :goto_0

    .line 366
    :cond_2
    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 365
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getOCRTranslatorDictIndexByLangID(Landroid/content/Context;[III)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tr_dic_id"    # [I
    .param p2, "leftLangID"    # I
    .param p3, "rightLangID"    # I

    .prologue
    const/4 v4, -0x1

    .line 677
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070008

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 678
    .local v2, "dict_src_lang_id":[I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070009

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    .line 680
    .local v1, "dict_dst_lang_id":[I
    const-string v5, "OCRDicManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Dic lang :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getLangID2LangIDForDiodic(I)I

    move-result p2

    .line 683
    invoke-static {p3}, Lcom/sec/android/app/bcocr/OCRDicManager;->getLangID2LangIDForDiodic(I)I

    move-result p3

    .line 685
    if-nez p1, :cond_1

    .line 686
    const-string v5, "OCRDicManager"

    const-string v6, "getOCRTranslatorDictIndexByLangID : tr_dic is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 703
    :cond_0
    :goto_0
    return v3

    .line 690
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v5, p1

    if-lt v3, v5, :cond_2

    move v3, v4

    .line 703
    goto :goto_0

    .line 692
    :cond_2
    aget v5, p1, v3

    invoke-static {p0, v5}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTRIndexBydicID(Landroid/content/Context;I)I

    move-result v0

    .line 694
    .local v0, "dictIndex":I
    if-ltz v0, :cond_3

    .line 695
    aget v5, v2, v0

    if-ne v5, p2, :cond_3

    .line 697
    aget v5, v1, v0

    if-eq v5, p3, :cond_0

    .line 690
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static getTRIndexBydicID(Landroid/content/Context;I)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dicID"    # I

    .prologue
    const/4 v2, -0x1

    .line 645
    if-nez p0, :cond_1

    move v1, v2

    .line 657
    :cond_0
    :goto_0
    return v1

    .line 649
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 651
    .local v0, "dict_id":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-lt v1, v3, :cond_2

    move v1, v2

    .line 657
    goto :goto_0

    .line 652
    :cond_2
    aget v3, v0, v1

    if-eq v3, p1, :cond_0

    .line 651
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getTranslateDstLang(Landroid/content/Context;II)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcLangID"    # I
    .param p2, "dstLangID"    # I

    .prologue
    .line 825
    const/4 v1, 0x0

    .line 826
    .local v1, "isSrcLangAuto":Z
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f080000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 827
    .local v0, "currPhoneLangID":I
    const-string v2, "OCRDicManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getTranslateDstLang:phone lang = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    invoke-static {p1}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 830
    const/4 v1, 0x1

    .line 834
    :cond_0
    invoke-static {p1, p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->isSupportTranslateLang(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 835
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    .line 876
    :goto_0
    return-object v2

    .line 839
    :cond_1
    if-eqz v1, :cond_2

    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 840
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 844
    :cond_2
    move p2, v0

    .line 845
    invoke-static {p1, p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->isSupportTranslateLang(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 846
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 850
    :cond_3
    if-eqz v1, :cond_4

    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 851
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 855
    :cond_4
    const/4 p2, 0x0

    .line 856
    invoke-static {p1, p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->isSupportTranslateLang(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 857
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 861
    :cond_5
    if-eqz v1, :cond_6

    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 862
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 866
    :cond_6
    const/16 p2, 0xa

    .line 867
    invoke-static {p1, p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->isSupportTranslateLang(II)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 868
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 872
    :cond_7
    if-eqz v1, :cond_8

    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 873
    invoke-static {p2}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 876
    :cond_8
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getTranslateLang(I)Ljava/lang/String;
    .locals 1
    .param p0, "langID"    # I

    .prologue
    .line 706
    sparse-switch p0, :sswitch_data_0

    .line 731
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 708
    :sswitch_0
    const-string v0, "en-US"

    goto :goto_0

    .line 710
    :sswitch_1
    const-string v0, "en-GB"

    goto :goto_0

    .line 712
    :sswitch_2
    const-string v0, "fr-FR"

    goto :goto_0

    .line 714
    :sswitch_3
    const-string v0, "de-DE"

    goto :goto_0

    .line 716
    :sswitch_4
    const-string v0, "it-IT"

    goto :goto_0

    .line 718
    :sswitch_5
    const-string v0, "ko-KR"

    goto :goto_0

    .line 721
    :sswitch_6
    const-string v0, "zh-CN"

    goto :goto_0

    .line 723
    :sswitch_7
    const-string v0, "ja-JP"

    goto :goto_0

    .line 726
    :sswitch_8
    const-string v0, "es-ES"

    goto :goto_0

    .line 729
    :sswitch_9
    const-string v0, "pt-BR"

    goto :goto_0

    .line 706
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_6
        0xa -> :sswitch_2
        0xb -> :sswitch_3
        0xf -> :sswitch_4
        0x11 -> :sswitch_7
        0x12 -> :sswitch_5
        0x18 -> :sswitch_9
        0x1a -> :sswitch_8
    .end sparse-switch
.end method

.method public static getTranslateSrcLang(I)Ljava/lang/String;
    .locals 1
    .param p0, "langID"    # I

    .prologue
    .line 735
    invoke-static {p0}, Lcom/sec/android/app/bcocr/OCRDicManager;->getTranslateLang(I)Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "srcLang":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 740
    .end local v0    # "srcLang":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "srcLang":Ljava/lang/String;
    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private static isSupportTranslateLang(II)Z
    .locals 3
    .param p0, "srcLangID"    # I
    .param p1, "dstLangID"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 745
    if-nez p0, :cond_0

    .line 746
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 820
    :goto_0
    :pswitch_0
    :sswitch_0
    return v0

    .line 762
    :cond_0
    if-ne p0, v0, :cond_1

    .line 763
    sparse-switch p1, :sswitch_data_1

    move v0, v1

    .line 773
    goto :goto_0

    .line 775
    :cond_1
    const/16 v2, 0x12

    if-ne p0, v2, :cond_2

    .line 776
    sparse-switch p1, :sswitch_data_2

    move v0, v1

    .line 783
    goto :goto_0

    .line 785
    :cond_2
    const/4 v2, 0x3

    if-ne p0, v2, :cond_3

    .line 787
    sparse-switch p1, :sswitch_data_3

    move v0, v1

    .line 793
    goto :goto_0

    .line 795
    :cond_3
    const/16 v2, 0x11

    if-ne p0, v2, :cond_4

    .line 796
    sparse-switch p1, :sswitch_data_4

    move v0, v1

    .line 803
    goto :goto_0

    .line 805
    :cond_4
    const/16 v2, 0xa

    if-eq p0, v2, :cond_5

    .line 806
    const/16 v2, 0xb

    if-eq p0, v2, :cond_5

    .line 807
    const/16 v2, 0xf

    if-eq p0, v2, :cond_5

    .line 808
    const/16 v2, 0x1a

    if-eq p0, v2, :cond_5

    .line 811
    const/16 v2, 0x18

    if-ne p0, v2, :cond_6

    .line 812
    :cond_5
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 817
    goto :goto_0

    :cond_6
    move v0, v1

    .line 820
    goto :goto_0

    .line 746
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xf -> :sswitch_0
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x18 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch

    .line 763
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xf -> :sswitch_0
        0x18 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch

    .line 776
    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch

    .line 787
    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_0
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
    .end sparse-switch

    .line 796
    :sswitch_data_4
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_0
        0x12 -> :sswitch_0
    .end sparse-switch

    .line 812
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static onTranslate(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sourceText"    # Ljava/lang/String;
    .param p2, "srcTRLang"    # Ljava/lang/String;
    .param p3, "dstTRLang"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 880
    const-string v4, "OCRDicManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onTranslate:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    if-nez p0, :cond_0

    .line 883
    const-string v3, "OCRDicManager"

    const-string v4, "onTranslate : invalidate param"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    .end local p0    # "context":Landroid/content/Context;
    :goto_0
    return v2

    .line 889
    .restart local p0    # "context":Landroid/content/Context;
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 890
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.sec.android.app.translator.TRANSLATE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 891
    const-string v4, "mode"

    const-string v5, "viewer"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 892
    const-string v4, "source_text"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 893
    const-string v4, "source_language"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 894
    const-string v4, "target_language"

    invoke-virtual {v1, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 895
    const-string v4, "editable_source_text"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 896
    const-string v4, "enable_share_via_menu"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 899
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    const/16 v4, 0xc9

    invoke-virtual {p0, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 908
    goto :goto_0

    .line 901
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 902
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "OCRDicManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onTranslate : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
