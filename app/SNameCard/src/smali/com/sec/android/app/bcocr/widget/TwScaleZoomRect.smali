.class public Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;
.super Landroid/view/View;
.source "TwScaleZoomRect.java"


# static fields
.field public static final MAX_ZOOM_LEVEL:I

.field public static final MAX_ZOOM_RATIO:I

.field public static final MIN_ZOOM_RATIO:I

.field public static final ZOOM_STEP:F

.field private static final ZOOM_TEXT_MARGIN:F


# instance fields
.field private mExtraHeight:I

.field private mExtraWidth:I

.field private mLastOrientation:I

.field private mLinePaint:Landroid/graphics/Paint;

.field private mOrientationListener:Landroid/view/OrientationEventListener;

.field private mRect:Landroid/graphics/RectF;

.field private mTextPaint:Landroid/graphics/Paint;

.field private zoomValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const v0, 0x7f090086

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_RATIO:I

    .line 41
    const v0, 0x7f090087

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MIN_ZOOM_RATIO:I

    .line 42
    const v0, 0x7f090088

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    .line 43
    sget v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_RATIO:I

    sget v1, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MIN_ZOOM_RATIO:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->ZOOM_STEP:F

    .line 45
    const v0, 0x7f090084

    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->ZOOM_TEXT_MARGIN:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 47
    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    .line 53
    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraWidth:I

    .line 54
    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraHeight:I

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 129
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->init()V

    .line 59
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;I)I
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->roundOrientation(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;I)V
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I

    return-void
.end method

.method private roundOrientation(I)I
    .locals 3
    .param p1, "orientationInput"    # I

    .prologue
    .line 167
    move v0, p1

    .line 168
    .local v0, "orientation":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 172
    :cond_0
    rem-int/lit16 v0, v0, 0x168

    .line 174
    const/16 v2, 0x2d

    if-ge v0, v2, :cond_1

    .line 175
    const/4 v1, 0x0

    .line 185
    .local v1, "retVal":I
    :goto_0
    return v1

    .line 176
    .end local v1    # "retVal":I
    :cond_1
    const/16 v2, 0x87

    if-ge v0, v2, :cond_2

    .line 177
    const/16 v1, 0x5a

    .line 178
    .restart local v1    # "retVal":I
    goto :goto_0

    .end local v1    # "retVal":I
    :cond_2
    const/16 v2, 0xe1

    if-ge v0, v2, :cond_3

    .line 179
    const/16 v1, 0xb4

    .line 180
    .restart local v1    # "retVal":I
    goto :goto_0

    .end local v1    # "retVal":I
    :cond_3
    const/16 v2, 0x13b

    if-ge v0, v2, :cond_4

    .line 181
    const/16 v1, 0x10e

    .line 182
    .restart local v1    # "retVal":I
    goto :goto_0

    .line 183
    .end local v1    # "retVal":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "retVal":I
    goto :goto_0
.end method

.method private setOrientationListener()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;-><init>(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 164
    return-void
.end method


# virtual methods
.method init()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0xff

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    const v1, 0x7f090082

    invoke-static {v1}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    const v1, 0x7f090083

    invoke-static {v1}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setOrientationListener()V

    .line 75
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    const/4 v5, 0x0

    .line 95
    .local v5, "tempWidth":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->getHeight()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float v1, v8, v9

    .line 96
    .local v1, "hcenter":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->getWidth()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float v7, v8, v9

    .line 98
    .local v7, "wcenter":F
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraWidth:I

    if-eqz v8, :cond_0

    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraHeight:I

    if-eqz v8, :cond_0

    .line 99
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraHeight:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float v1, v8, v9

    .line 100
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraWidth:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float v7, v8, v9

    .line 103
    :cond_0
    const v8, 0x7f090082

    invoke-static {v8}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    div-int/lit8 v3, v8, 0x2

    .line 104
    .local v3, "offset":I
    const v8, 0x7f090089

    invoke-static {v8}, Lcom/sec/android/app/bcocr/Util;->getDimension(I)F

    move-result v5

    .line 106
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    mul-float/2addr v8, v5

    sub-float v2, v7, v8

    .line 107
    .local v2, "left":F
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    mul-float/2addr v8, v5

    sub-float v6, v1, v8

    .line 108
    .local v6, "top":F
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    mul-float/2addr v8, v5

    add-float v4, v7, v8

    .line 109
    .local v4, "right":F
    iget v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    int-to-float v8, v8

    sget v9, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->MAX_ZOOM_LEVEL:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    mul-float/2addr v8, v5

    add-float v0, v1, v8

    .line 111
    .local v0, "bottom":F
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v2, v6, v4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mRect:Landroid/graphics/RectF;

    .line 113
    iget-object v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mRect:Landroid/graphics/RectF;

    int-to-float v9, v3

    int-to-float v10, v3

    invoke-virtual {v8, v9, v10}, Landroid/graphics/RectF;->inset(FF)V

    .line 116
    const/high16 v8, 0x43b40000    # 360.0f

    add-float v9, v2, v4

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float v10, v6, v0

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 117
    iget-object v8, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mRect:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 118
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "%.1f"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    int-to-float v12, v12

    sget v13, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->ZOOM_STEP:F

    div-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    add-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    iget-object v10, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v9, v1

    sget v10, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->ZOOM_TEXT_MARGIN:F

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v7, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 119
    return-void
.end method

.method public setExtraSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraWidth:I

    .line 87
    iput p2, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mExtraHeight:I

    .line 88
    return-void
.end method

.method public setLastOrientation(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 78
    sget v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_LCD_ORIENTATION:I

    sget v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_LCD_ORIENTATION_PORTRAIT:I

    if-ne v0, v1, :cond_0

    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->roundOrientation(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x5a

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->roundOrientation(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I

    goto :goto_0
.end method

.method public setZoomValue(I)V
    .locals 0
    .param p1, "currentzoomValue"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->zoomValue:I

    .line 123
    return-void
.end method
