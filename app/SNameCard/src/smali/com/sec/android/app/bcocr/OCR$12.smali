.class Lcom/sec/android/app/bcocr/OCR$12;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->showNetworkStateDialog(IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;

.field private final synthetic val$checkBox:Landroid/widget/CheckBox;

.field private final synthetic val$preferenceKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;Landroid/widget/CheckBox;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput-object p2, p0, Lcom/sec/android/app/bcocr/OCR$12;->val$checkBox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/android/app/bcocr/OCR$12;->val$preferenceKey:Ljava/lang/String;

    .line 1120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1123
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    new-instance v2, Lcom/sec/android/app/bcocr/UpdateCheckThread;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCR;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v4, v4, Lcom/sec/android/app/bcocr/OCR;->updateCheckHandler:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/bcocr/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, v1, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    .line 1124
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->start()V

    .line 1125
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$12;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1126
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$12;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1127
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$12;->val$preferenceKey:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1130
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method
