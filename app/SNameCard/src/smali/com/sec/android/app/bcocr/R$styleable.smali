.class public final Lcom/sec/android/app/bcocr/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final TwImageButton:[I

.field public static final TwImageButton_twimagebutton_background_d:I = 0x11

.field public static final TwImageButton_twimagebutton_background_height:I = 0xb

.field public static final TwImageButton_twimagebutton_background_n:I = 0xf

.field public static final TwImageButton_twimagebutton_background_p:I = 0x10

.field public static final TwImageButton_twimagebutton_background_width:I = 0xa

.field public static final TwImageButton_twimagebutton_image_d:I = 0xe

.field public static final TwImageButton_twimagebutton_image_height:I = 0x9

.field public static final TwImageButton_twimagebutton_image_n:I = 0xc

.field public static final TwImageButton_twimagebutton_image_p:I = 0xd

.field public static final TwImageButton_twimagebutton_image_width:I = 0x8

.field public static final TwImageButton_twimagebutton_image_x:I = 0x6

.field public static final TwImageButton_twimagebutton_image_y:I = 0x7

.field public static final TwImageButton_twimagebutton_text:I = 0x0

.field public static final TwImageButton_twimagebutton_text_height:I = 0x4

.field public static final TwImageButton_twimagebutton_text_size:I = 0x5

.field public static final TwImageButton_twimagebutton_text_width:I = 0x3

.field public static final TwImageButton_twimagebutton_text_x:I = 0x1

.field public static final TwImageButton_twimagebutton_text_y:I = 0x2

.field public static final TwSideMenu:[I

.field public static final TwSideMenu_twsidemenu_bottom_bg:I = 0x2

.field public static final TwSideMenu_twsidemenu_mid_bg:I = 0x1

.field public static final TwSideMenu_twsidemenu_top_bg:I = 0x0

.field public static final TwSlider:[I

.field public static final TwSlider_slider_bar_image:I = 0x0

.field public static final TwSlider_slider_bar_margin_top:I = 0x4

.field public static final TwSlider_slider_focus_bar_image:I = 0x1

.field public static final TwSlider_slider_focus_bar_margin_top:I = 0x5

.field public static final TwSlider_slider_focus_image_height:I = 0x9

.field public static final TwSlider_slider_focus_image_width:I = 0x8

.field public static final TwSlider_slider_image_height:I = 0x7

.field public static final TwSlider_slider_image_width:I = 0x6

.field public static final TwSlider_slider_layout_width:I = 0x3

.field public static final TwSlider_slider_number_of_guage:I = 0x2

.field public static final TwTextView:[I

.field public static final TwTextView_textFadeOut:I = 0x3

.field public static final TwTextView_textViewIsMoreText:I = 0x2

.field public static final TwTextView_textViewMaxHeight:I = 0x1

.field public static final TwTextView_textViewMinHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6002
    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/bcocr/R$styleable;->TwImageButton:[I

    .line 6297
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/bcocr/R$styleable;->TwSideMenu:[I

    .line 6370
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/bcocr/R$styleable;->TwSlider:[I

    .line 6555
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/bcocr/R$styleable;->TwTextView:[I

    .line 6621
    return-void

    .line 6002
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
    .end array-data

    .line 6297
    :array_1
    .array-data 4
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 6370
    :array_2
    .array-data 4
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
    .end array-data

    .line 6555
    :array_3
    .array-data 4
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
