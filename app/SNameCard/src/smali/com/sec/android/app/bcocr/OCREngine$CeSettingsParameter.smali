.class public Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;
.super Ljava/lang/Object;
.source "OCREngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CeSettingsParameter"
.end annotation


# instance fields
.field private mKey:I

.field private mValue:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    iput p1, p0, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->mKey:I

    .line 342
    iput p2, p0, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->mValue:I

    .line 343
    return-void
.end method


# virtual methods
.method public getKey()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->mKey:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->mValue:I

    return v0
.end method
