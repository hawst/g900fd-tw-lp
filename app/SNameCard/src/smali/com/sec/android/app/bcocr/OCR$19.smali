.class Lcom/sec/android/app/bcocr/OCR$19;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->onRecognitionStateChangedForJPEG()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 5091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5094
    const/4 v0, 0x0

    .line 5095
    .local v0, "recog_completed":Z
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] captureRecogThread +++"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5097
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->SetRecognizeLock(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/bcocr/OCR;->access$28(Lcom/sec/android/app/bcocr/OCR;Z)V

    .line 5099
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v2, 0x2

    iput v2, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    .line 5100
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->ocrRecognizeCaptureData()Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$30(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v0

    .line 5101
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput v3, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRRecogState:I

    .line 5102
    if-nez v0, :cond_0

    .line 5103
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] captureRecogThread: recog skip !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5105
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 5107
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/bcocr/OCR;->SetRecognizeLock(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->access$28(Lcom/sec/android/app/bcocr/OCR;Z)V

    .line 5108
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->OCRHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 5109
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$19;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->OCRHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 5112
    :cond_1
    const-string v1, "OCR"

    const-string v2, "[OCR] [Recognition] captureRecogThread ---"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5113
    return-void
.end method
