.class Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "TextToSpeechAssist.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/TextToSpeechAssist;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 177
    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 180
    const-string v0, "OCR_UTT_ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$2(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;->onDone()V

    .line 186
    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 189
    const-string v0, "OCR_UTT_ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$2(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;->onError()V

    .line 195
    :cond_0
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 4
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 198
    const-string v0, "OCR_UTT_ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$2(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 200
    const/4 v2, 0x3

    const/4 v3, 0x2

    .line 199
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;->onStart()V

    .line 205
    :cond_0
    return-void
.end method
