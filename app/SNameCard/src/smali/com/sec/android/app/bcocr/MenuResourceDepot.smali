.class public Lcom/sec/android/app/bcocr/MenuResourceDepot;
.super Ljava/lang/Object;
.source "MenuResourceDepot.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MenuResourceDepot"


# instance fields
.field protected mActivityContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/bcocr/AbstractOCRActivity;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

.field public mMenus:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/bcocr/MenuBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V
    .locals 1
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    .line 40
    return-void
.end method

.method private clearAllMenus()V
    .locals 5

    .prologue
    .line 94
    const-string v3, "MenuResourceDepot"

    const-string v4, "clearAllMenus"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 97
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    return-void

    .line 98
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 99
    .local v0, "id":I
    iget-object v3, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/bcocr/MenuBase;

    .line 100
    .local v2, "menu":Lcom/sec/android/app/bcocr/MenuBase;
    if-eqz v2, :cond_1

    .line 101
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->clear()V

    .line 103
    :cond_1
    const-string v3, "MenuResourceDepot"

    const-string v4, "clearing..."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public clearAllMenusForRotate()V
    .locals 6

    .prologue
    .line 108
    const-string v4, "MenuResourceDepot"

    const-string v5, "clearAllMenusForRotate"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 112
    .local v3, "menusClone":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/app/bcocr/MenuBase;>;"
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 114
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 123
    return-void

    .line 115
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 116
    .local v0, "id":I
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/bcocr/MenuBase;

    .line 117
    .local v2, "menu":Lcom/sec/android/app/bcocr/MenuBase;
    if-eqz v2, :cond_0

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->removeMenu(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 120
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->clear()V

    goto :goto_0
.end method

.method public clearInvisibleViews()V
    .locals 6

    .prologue
    .line 43
    const-string v4, "MenuResourceDepot"

    const-string v5, "clearInvisibleViews"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 47
    .local v3, "menusClone":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/app/bcocr/MenuBase;>;"
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 49
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 58
    return-void

    .line 50
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 51
    .local v0, "id":I
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/bcocr/MenuBase;

    .line 52
    .local v2, "menu":Lcom/sec/android/app/bcocr/MenuBase;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->isActive()Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v4, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->removeMenu(Lcom/sec/android/app/bcocr/MenuBase;)V

    .line 55
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->clear()V

    goto :goto_0
.end method

.method public closeVisibleViews()V
    .locals 5

    .prologue
    .line 61
    const-string v3, "MenuResourceDepot"

    const-string v4, "closeVisibleViews"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v3, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 65
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 72
    return-void

    .line 66
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 67
    .local v0, "id":I
    iget-object v3, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/bcocr/MenuBase;

    .line 68
    .local v2, "menu":Lcom/sec/android/app/bcocr/MenuBase;
    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->getVisibility()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/MenuBase;->getZorder()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    goto :goto_0
.end method

.method public getMenuByLayoutId(ILandroid/view/ViewGroup;)Lcom/sec/android/app/bcocr/MenuBase;
    .locals 6
    .param p1, "id"    # I
    .param p2, "baseLayout"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v0, 0x0

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_0

    .line 89
    :goto_0
    return-object v0

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 82
    .local v1, "context":Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 84
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    if-nez v0, :cond_1

    .line 85
    new-instance v0, Lcom/sec/android/app/bcocr/EmptyView;

    const v2, 0x7f030003

    const/4 v5, 0x5

    move-object v3, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/bcocr/EmptyView;-><init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;ILandroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    const-string v0, "MenuResourceDepot"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iput-object v2, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mActivityContext:Ljava/lang/ref/WeakReference;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/EmptyView;->clear()V

    .line 132
    iput-object v2, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mEmptyView:Lcom/sec/android/app/bcocr/EmptyView;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/MenuResourceDepot;->clearAllMenus()V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 138
    iput-object v2, p0, Lcom/sec/android/app/bcocr/MenuResourceDepot;->mMenus:Ljava/util/HashMap;

    .line 140
    :cond_1
    return-void
.end method
