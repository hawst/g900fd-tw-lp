.class Lcom/sec/android/app/bcocr/OCR$3$4;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/bcocr/OCR$3;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$3$4;->this$1:Lcom/sec/android/app/bcocr/OCR$3;

    .line 1198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/OCR$3$4;)Lcom/sec/android/app/bcocr/OCR$3;
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3$4;->this$1:Lcom/sec/android/app/bcocr/OCR$3;

    return-object v0
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keycode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1200
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "back connect :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3$4;->this$1:Lcom/sec/android/app/bcocr/OCR$3;

    # getter for: Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR$3;->access$0(Lcom/sec/android/app/bcocr/OCR$3;)Lcom/sec/android/app/bcocr/OCR;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    if-eqz v0, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3$4;->this$1:Lcom/sec/android/app/bcocr/OCR$3;

    # getter for: Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR$3;->access$0(Lcom/sec/android/app/bcocr/OCR$3;)Lcom/sec/android/app/bcocr/OCR;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->cancel()V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$3$4;->this$1:Lcom/sec/android/app/bcocr/OCR$3;

    # getter for: Lcom/sec/android/app/bcocr/OCR$3;->this$0:Lcom/sec/android/app/bcocr/OCR;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR$3;->access$0(Lcom/sec/android/app/bcocr/OCR$3;)Lcom/sec/android/app/bcocr/OCR;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->mUpdateCheckThread:Lcom/sec/android/app/bcocr/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/UpdateCheckThread;->interrupt()V

    .line 1207
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/bcocr/OCR$3$4$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCR$3$4$1;-><init>(Lcom/sec/android/app/bcocr/OCR$3$4;)V

    .line 1215
    const-wide/16 v2, 0xc8

    .line 1207
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1225
    const/4 v0, 0x1

    .line 1227
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
