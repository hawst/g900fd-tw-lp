.class public Lcom/sec/android/app/bcocr/CameraResolution;
.super Ljava/lang/Object;
.source "CameraResolution.java"


# static fields
.field public static final RESOLUTION_1248X672:I = 0x1d

.field public static final RESOLUTION_1280X720:I = 0xe

.field public static final RESOLUTION_1280X800:I = 0x16

.field public static final RESOLUTION_1280X960:I = 0xa

.field public static final RESOLUTION_1392X1392:I = 0x18

.field public static final RESOLUTION_1440X1080:I = 0x1f

.field public static final RESOLUTION_1536X864:I = 0x1c

.field public static final RESOLUTION_1600X1200:I = 0x8

.field public static final RESOLUTION_1600X960:I = 0x9

.field public static final RESOLUTION_1632X880:I = 0x1b

.field public static final RESOLUTION_176X144:I = 0x13

.field public static final RESOLUTION_1920X1080:I = 0xd

.field public static final RESOLUTION_2048X1104:I = 0x1a

.field public static final RESOLUTION_2048X1152:I = 0x17

.field public static final RESOLUTION_2048X1232:I = 0x7

.field public static final RESOLUTION_2048X1536:I = 0x6

.field public static final RESOLUTION_2560X1440:I = 0x15

.field public static final RESOLUTION_2560X1536:I = 0x5

.field public static final RESOLUTION_2560X1920:I = 0x4

.field public static final RESOLUTION_2592X1944:I = 0x19

.field public static final RESOLUTION_3072X1856:I = 0x3

.field public static final RESOLUTION_3072X2304:I = 0x2

.field public static final RESOLUTION_320X240:I = 0x12

.field public static final RESOLUTION_3264X1836:I = 0x14

.field public static final RESOLUTION_3264X1968:I = 0x1

.field public static final RESOLUTION_3264X2448:I = 0x0

.field public static final RESOLUTION_352X288:I = 0x11

.field public static final RESOLUTION_400X240:I = 0x10

.field public static final RESOLUTION_4096X2304:I = 0x24

.field public static final RESOLUTION_4096X3072:I = 0x1e

.field public static final RESOLUTION_4128X2322:I = 0x21

.field public static final RESOLUTION_4128X3096:I = 0x20

.field public static final RESOLUTION_5312X2988:I = 0x25

.field public static final RESOLUTION_640X480:I = 0xb

.field public static final RESOLUTION_720X480:I = 0xf

.field public static final RESOLUTION_800X450:I = 0x23

.field public static final RESOLUTION_800X480:I = 0xc

.field public static final RESOLUTION_960X720:I = 0x22


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compare(II)I
    .locals 4
    .param p0, "res1"    # I
    .param p1, "res2"    # I

    .prologue
    .line 524
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v2

    invoke-static {p1}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v3

    sub-int v1, v2, v3

    .line 525
    .local v1, "dw":I
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v2

    invoke-static {p1}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v3

    sub-int v0, v2, v3

    .line 527
    .local v0, "dh":I
    if-lez v1, :cond_1

    .line 536
    .end local v1    # "dw":I
    :cond_0
    :goto_0
    return v1

    .line 529
    .restart local v1    # "dw":I
    :cond_1
    if-nez v1, :cond_0

    .line 530
    if-lez v0, :cond_2

    move v1, v0

    .line 531
    goto :goto_0

    .line 532
    :cond_2
    if-nez v0, :cond_0

    .line 533
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getCamcorderProfileQualityLevel(I)I
    .locals 1
    .param p0, "resolution"    # I

    .prologue
    .line 501
    packed-switch p0, :pswitch_data_0

    .line 515
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 503
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_0

    .line 505
    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    .line 507
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 509
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 511
    :pswitch_5
    const/4 v0, 0x7

    goto :goto_0

    .line 513
    :pswitch_6
    const/4 v0, 0x2

    goto :goto_0

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getIntHeight(I)I
    .locals 1
    .param p0, "resid"    # I

    .prologue
    const/16 v0, 0x1e0

    .line 135
    packed-switch p0, :pswitch_data_0

    .line 205
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    :pswitch_1
    return v0

    .line 137
    :pswitch_2
    const/16 v0, 0xbac

    goto :goto_0

    .line 139
    :pswitch_3
    const/16 v0, 0xc18

    goto :goto_0

    .line 141
    :pswitch_4
    const/16 v0, 0xc00

    goto :goto_0

    .line 143
    :pswitch_5
    const/16 v0, 0x990

    goto :goto_0

    .line 145
    :pswitch_6
    const/16 v0, 0x912

    goto :goto_0

    .line 148
    :pswitch_7
    const/16 v0, 0x900

    goto :goto_0

    .line 150
    :pswitch_8
    const/16 v0, 0x7b0

    goto :goto_0

    .line 152
    :pswitch_9
    const/16 v0, 0x798

    goto :goto_0

    .line 154
    :pswitch_a
    const/16 v0, 0x780

    goto :goto_0

    .line 156
    :pswitch_b
    const/16 v0, 0x740

    goto :goto_0

    .line 158
    :pswitch_c
    const/16 v0, 0x72c

    goto :goto_0

    .line 161
    :pswitch_d
    const/16 v0, 0x600

    goto :goto_0

    .line 163
    :pswitch_e
    const/16 v0, 0x5a0

    goto :goto_0

    .line 165
    :pswitch_f
    const/16 v0, 0x4d0

    goto :goto_0

    .line 167
    :pswitch_10
    const/16 v0, 0x4b0

    goto :goto_0

    .line 169
    :pswitch_11
    const/16 v0, 0x570

    goto :goto_0

    .line 171
    :pswitch_12
    const/16 v0, 0x480

    goto :goto_0

    .line 173
    :pswitch_13
    const/16 v0, 0x450

    goto :goto_0

    .line 176
    :pswitch_14
    const/16 v0, 0x438

    goto :goto_0

    .line 179
    :pswitch_15
    const/16 v0, 0x3c0

    goto :goto_0

    .line 181
    :pswitch_16
    const/16 v0, 0x370

    goto :goto_0

    .line 183
    :pswitch_17
    const/16 v0, 0x360

    goto :goto_0

    .line 185
    :pswitch_18
    const/16 v0, 0x320

    goto :goto_0

    .line 190
    :pswitch_19
    const/16 v0, 0x1c2

    goto :goto_0

    .line 193
    :pswitch_1a
    const/16 v0, 0x2d0

    goto :goto_0

    .line 195
    :pswitch_1b
    const/16 v0, 0x2a0

    goto :goto_0

    .line 200
    :pswitch_1c
    const/16 v0, 0xf0

    goto :goto_0

    .line 202
    :pswitch_1d
    const/16 v0, 0x90

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_8
        :pswitch_7
        :pswitch_b
        :pswitch_a
        :pswitch_d
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_15
        :pswitch_15
        :pswitch_1
        :pswitch_1
        :pswitch_14
        :pswitch_1a
        :pswitch_1
        :pswitch_1c
        :pswitch_0
        :pswitch_1c
        :pswitch_1d
        :pswitch_c
        :pswitch_e
        :pswitch_18
        :pswitch_12
        :pswitch_11
        :pswitch_9
        :pswitch_13
        :pswitch_16
        :pswitch_17
        :pswitch_1b
        :pswitch_4
        :pswitch_14
        :pswitch_3
        :pswitch_6
        :pswitch_1a
        :pswitch_19
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method

.method public static getIntResolution(Ljava/lang/String;)I
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 540
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v1

    .line 541
    .local v1, "width":I
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v0

    .line 542
    .local v0, "height":I
    const-string v2, "CameraResolution"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "!!!!! value : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , width : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , height : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    mul-int v2, v1, v0

    return v2
.end method

.method public static getIntWidth(I)I
    .locals 1
    .param p0, "resid"    # I

    .prologue
    .line 68
    packed-switch p0, :pswitch_data_0

    .line 131
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 70
    :pswitch_1
    const/16 v0, 0x14c0

    goto :goto_0

    .line 73
    :pswitch_2
    const/16 v0, 0x1020

    goto :goto_0

    .line 76
    :pswitch_3
    const/16 v0, 0x1000

    goto :goto_0

    .line 80
    :pswitch_4
    const/16 v0, 0xcc0

    goto :goto_0

    .line 83
    :pswitch_5
    const/16 v0, 0xc00

    goto :goto_0

    .line 85
    :pswitch_6
    const/16 v0, 0xa20

    goto :goto_0

    .line 89
    :pswitch_7
    const/16 v0, 0xa00

    goto :goto_0

    .line 94
    :pswitch_8
    const/16 v0, 0x800

    goto :goto_0

    .line 96
    :pswitch_9
    const/16 v0, 0x780

    goto :goto_0

    .line 98
    :pswitch_a
    const/16 v0, 0x660

    goto :goto_0

    .line 101
    :pswitch_b
    const/16 v0, 0x640

    goto :goto_0

    .line 103
    :pswitch_c
    const/16 v0, 0x600

    goto :goto_0

    .line 105
    :pswitch_d
    const/16 v0, 0x5a0

    goto :goto_0

    .line 107
    :pswitch_e
    const/16 v0, 0x570

    goto :goto_0

    .line 111
    :pswitch_f
    const/16 v0, 0x500

    goto :goto_0

    .line 113
    :pswitch_10
    const/16 v0, 0x4e0

    goto :goto_0

    .line 115
    :pswitch_11
    const/16 v0, 0x3c0

    goto :goto_0

    .line 118
    :pswitch_12
    const/16 v0, 0x320

    goto :goto_0

    .line 120
    :pswitch_13
    const/16 v0, 0x2d0

    goto :goto_0

    .line 122
    :pswitch_14
    const/16 v0, 0x280

    goto :goto_0

    .line 124
    :pswitch_15
    const/16 v0, 0x190

    goto :goto_0

    .line 126
    :pswitch_16
    const/16 v0, 0x140

    goto :goto_0

    .line 128
    :pswitch_17
    const/16 v0, 0xb0

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_b
        :pswitch_b
        :pswitch_f
        :pswitch_14
        :pswitch_12
        :pswitch_9
        :pswitch_f
        :pswitch_13
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_4
        :pswitch_7
        :pswitch_f
        :pswitch_8
        :pswitch_e
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_c
        :pswitch_10
        :pswitch_3
        :pswitch_d
        :pswitch_2
        :pswitch_2
        :pswitch_11
        :pswitch_12
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public static getResolutionFullString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 340
    packed-switch p0, :pswitch_data_0

    .line 414
    :pswitch_0
    const-string v0, "176x144"

    :goto_0
    return-object v0

    .line 342
    :pswitch_1
    const-string v0, "13.0M (4128x3096)"

    goto :goto_0

    .line 344
    :pswitch_2
    const-string v0, "w9.6M (4128x2322)"

    goto :goto_0

    .line 346
    :pswitch_3
    const-string v0, "13.0M (4096x3072)"

    goto :goto_0

    .line 348
    :pswitch_4
    const-string v0, "w9.4M (4096x2304)"

    goto :goto_0

    .line 350
    :pswitch_5
    const-string v0, "8.0M (3264x2448)"

    goto :goto_0

    .line 352
    :pswitch_6
    const-string v0, "w6.5M (3264x1968)"

    goto :goto_0

    .line 354
    :pswitch_7
    const-string v0, "w6.0M (3264x1836)"

    goto :goto_0

    .line 356
    :pswitch_8
    const-string v0, "7.1M (3072x2304)"

    goto :goto_0

    .line 358
    :pswitch_9
    const-string v0, "w5.7M (3072x1856)"

    goto :goto_0

    .line 360
    :pswitch_a
    const-string v0, "5.1M (2592x1944)"

    goto :goto_0

    .line 362
    :pswitch_b
    const-string v0, "5.0M (2560x1920)"

    goto :goto_0

    .line 364
    :pswitch_c
    const-string v0, "w4.0M (2560x1536)"

    goto :goto_0

    .line 366
    :pswitch_d
    const-string v0, "w3.7M (2560x1440)"

    goto :goto_0

    .line 368
    :pswitch_e
    const-string v0, "3.0M (2048x1536)"

    goto :goto_0

    .line 370
    :pswitch_f
    const-string v0, "w2.6M (2048x1232)"

    goto :goto_0

    .line 372
    :pswitch_10
    const-string v0, "w2.4M (2048x1152)"

    goto :goto_0

    .line 374
    :pswitch_11
    const-string v0, "w2.3M 2048x1104"

    goto :goto_0

    .line 376
    :pswitch_12
    const-string v0, "w2.0M (1920x1080)"

    goto :goto_0

    .line 378
    :pswitch_13
    const-string v0, "w1.5M (1632x880)"

    goto :goto_0

    .line 380
    :pswitch_14
    const-string v0, "2.0M (1600x1200)"

    goto :goto_0

    .line 382
    :pswitch_15
    const-string v0, "w1.6M (1600x960)"

    goto :goto_0

    .line 384
    :pswitch_16
    const-string v0, "w1.4M 1536x864"

    goto :goto_0

    .line 386
    :pswitch_17
    const-string v0, "1.9M (1392x1392)"

    goto :goto_0

    .line 388
    :pswitch_18
    const-string v0, "1.6M (1440x1080)"

    goto :goto_0

    .line 390
    :pswitch_19
    const-string v0, "1.3M (1280x960)"

    goto :goto_0

    .line 392
    :pswitch_1a
    const-string v0, "w1.0M (1280x800)"

    goto :goto_0

    .line 394
    :pswitch_1b
    const-string v0, "w0.4M (800x480)"

    goto :goto_0

    .line 396
    :pswitch_1c
    const-string v0, "w0.4M (800x450)"

    goto :goto_0

    .line 398
    :pswitch_1d
    const-string v0, "0.3M (640x480)"

    goto :goto_0

    .line 400
    :pswitch_1e
    const-string v0, "w0.8M (1248x672)"

    goto :goto_0

    .line 402
    :pswitch_1f
    const-string v0, "w0.9M (1280x720)"

    goto :goto_0

    .line 404
    :pswitch_20
    const-string v0, "0.7M (960x720)"

    goto :goto_0

    .line 406
    :pswitch_21
    const-string v0, "0.4M (720x480)"

    goto :goto_0

    .line 408
    :pswitch_22
    const-string v0, "400x240"

    goto :goto_0

    .line 410
    :pswitch_23
    const-string v0, "320x240"

    goto :goto_0

    .line 412
    :pswitch_24
    const-string v0, "176x144"

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_14
        :pswitch_15
        :pswitch_19
        :pswitch_1d
        :pswitch_1b
        :pswitch_12
        :pswitch_1f
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_7
        :pswitch_d
        :pswitch_1a
        :pswitch_10
        :pswitch_17
        :pswitch_a
        :pswitch_11
        :pswitch_13
        :pswitch_16
        :pswitch_1e
        :pswitch_3
        :pswitch_18
        :pswitch_1
        :pswitch_2
        :pswitch_20
        :pswitch_1c
        :pswitch_4
    .end packed-switch
.end method

.method public static getResolutionID(Ljava/lang/String;)I
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/16 v0, 0x23

    .line 419
    const-string v1, "5312x2988"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 420
    const/16 v0, 0x25

    .line 496
    :cond_0
    :goto_0
    return v0

    .line 421
    :cond_1
    const-string v1, "4128x3096"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 422
    const/16 v0, 0x20

    goto :goto_0

    .line 423
    :cond_2
    const-string v1, "4128x2322"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 424
    const/16 v0, 0x21

    goto :goto_0

    .line 425
    :cond_3
    const-string v1, "4096x3072"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 426
    const/16 v0, 0x1e

    goto :goto_0

    .line 427
    :cond_4
    const-string v1, "4096x2304"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 428
    const/16 v0, 0x24

    goto :goto_0

    .line 429
    :cond_5
    const-string v1, "3264x2448"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 430
    const/4 v0, 0x0

    goto :goto_0

    .line 431
    :cond_6
    const-string v1, "3264x1968"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 432
    const/4 v0, 0x1

    goto :goto_0

    .line 433
    :cond_7
    const-string v1, "3264x1836"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 434
    const/16 v0, 0x14

    goto :goto_0

    .line 435
    :cond_8
    const-string v1, "3072x2304"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 436
    const/4 v0, 0x2

    goto :goto_0

    .line 437
    :cond_9
    const-string v1, "3072x1856"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 438
    const/4 v0, 0x3

    goto :goto_0

    .line 439
    :cond_a
    const-string v1, "2592x1944"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 440
    const/16 v0, 0x19

    goto :goto_0

    .line 441
    :cond_b
    const-string v1, "2560x1920"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 442
    const/4 v0, 0x4

    goto :goto_0

    .line 443
    :cond_c
    const-string v1, "2560x1536"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 444
    const/4 v0, 0x5

    goto :goto_0

    .line 445
    :cond_d
    const-string v1, "2560x1440"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 446
    const/16 v0, 0x15

    goto/16 :goto_0

    .line 447
    :cond_e
    const-string v1, "2048x1536"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 448
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 449
    :cond_f
    const-string v1, "2048x1232"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 450
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 451
    :cond_10
    const-string v1, "2048x1152"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 452
    const/16 v0, 0x17

    goto/16 :goto_0

    .line 453
    :cond_11
    const-string v1, "2048x1104"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 454
    const/16 v0, 0x1a

    goto/16 :goto_0

    .line 455
    :cond_12
    const-string v1, "1920x1080"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 456
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 457
    :cond_13
    const-string v1, "1440x1080"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 458
    const/16 v0, 0x1f

    goto/16 :goto_0

    .line 459
    :cond_14
    const-string v1, "1632x880"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 460
    const/16 v0, 0x1b

    goto/16 :goto_0

    .line 461
    :cond_15
    const-string v1, "1600x1200"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 462
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 463
    :cond_16
    const-string v1, "1600x960"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 464
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 465
    :cond_17
    const-string v1, "1536x864"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 466
    const/16 v0, 0x1c

    goto/16 :goto_0

    .line 467
    :cond_18
    const-string v1, "1392x1392"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 468
    const/16 v0, 0x18

    goto/16 :goto_0

    .line 469
    :cond_19
    const-string v1, "1280x960"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 470
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 471
    :cond_1a
    const-string v1, "960x720"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 472
    const/16 v0, 0x22

    goto/16 :goto_0

    .line 473
    :cond_1b
    const-string v1, "800x480"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 474
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 475
    :cond_1c
    const-string v1, "800x450"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 477
    const-string v1, "640x480"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 478
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 479
    :cond_1d
    const-string v1, "1280x800"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 480
    const/16 v0, 0x16

    goto/16 :goto_0

    .line 481
    :cond_1e
    const-string v1, "1280x720"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 482
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 483
    :cond_1f
    const-string v1, "1248x672"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 484
    const/16 v0, 0x1d

    goto/16 :goto_0

    .line 485
    :cond_20
    const-string v1, "800x450"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 487
    const-string v0, "720x480"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 488
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 489
    :cond_21
    const-string v0, "400x240"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 490
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 491
    :cond_22
    const-string v0, "320x240"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 492
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 493
    :cond_23
    const-string v0, "176x144"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 494
    const/16 v0, 0x13

    goto/16 :goto_0

    .line 496
    :cond_24
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public static getResolutionString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 261
    packed-switch p0, :pswitch_data_0

    .line 335
    :pswitch_0
    const-string v0, "176x144"

    :goto_0
    return-object v0

    .line 263
    :pswitch_1
    const-string v0, "4128x3096"

    goto :goto_0

    .line 265
    :pswitch_2
    const-string v0, "4128x2322"

    goto :goto_0

    .line 267
    :pswitch_3
    const-string v0, "4096x3072"

    goto :goto_0

    .line 269
    :pswitch_4
    const-string v0, "4096x2304"

    goto :goto_0

    .line 271
    :pswitch_5
    const-string v0, "3264x2448"

    goto :goto_0

    .line 273
    :pswitch_6
    const-string v0, "3264x1968"

    goto :goto_0

    .line 275
    :pswitch_7
    const-string v0, "3264x1836"

    goto :goto_0

    .line 277
    :pswitch_8
    const-string v0, "3072x2304"

    goto :goto_0

    .line 279
    :pswitch_9
    const-string v0, "3072x1856"

    goto :goto_0

    .line 281
    :pswitch_a
    const-string v0, "2592x1944"

    goto :goto_0

    .line 283
    :pswitch_b
    const-string v0, "2560x1920"

    goto :goto_0

    .line 285
    :pswitch_c
    const-string v0, "2560x1536"

    goto :goto_0

    .line 287
    :pswitch_d
    const-string v0, "2560x1440"

    goto :goto_0

    .line 289
    :pswitch_e
    const-string v0, "2048x1536"

    goto :goto_0

    .line 291
    :pswitch_f
    const-string v0, "2048x1232"

    goto :goto_0

    .line 293
    :pswitch_10
    const-string v0, "2048x1152"

    goto :goto_0

    .line 295
    :pswitch_11
    const-string v0, "2048x1104"

    goto :goto_0

    .line 297
    :pswitch_12
    const-string v0, "1920x1080"

    goto :goto_0

    .line 299
    :pswitch_13
    const-string v0, "1632x880"

    goto :goto_0

    .line 301
    :pswitch_14
    const-string v0, "1600x1200"

    goto :goto_0

    .line 303
    :pswitch_15
    const-string v0, "1600x960"

    goto :goto_0

    .line 305
    :pswitch_16
    const-string v0, "1536x864"

    goto :goto_0

    .line 307
    :pswitch_17
    const-string v0, "1392x1392"

    goto :goto_0

    .line 309
    :pswitch_18
    const-string v0, "1440x1080"

    goto :goto_0

    .line 311
    :pswitch_19
    const-string v0, "1280x960"

    goto :goto_0

    .line 313
    :pswitch_1a
    const-string v0, "1280x800"

    goto :goto_0

    .line 315
    :pswitch_1b
    const-string v0, "800x480"

    goto :goto_0

    .line 317
    :pswitch_1c
    const-string v0, "800x450"

    goto :goto_0

    .line 319
    :pswitch_1d
    const-string v0, "640x480"

    goto :goto_0

    .line 321
    :pswitch_1e
    const-string v0, "1248x672"

    goto :goto_0

    .line 323
    :pswitch_1f
    const-string v0, "1280x720"

    goto :goto_0

    .line 325
    :pswitch_20
    const-string v0, "960x720"

    goto :goto_0

    .line 327
    :pswitch_21
    const-string v0, "720x480"

    goto :goto_0

    .line 329
    :pswitch_22
    const-string v0, "400x240"

    goto :goto_0

    .line 331
    :pswitch_23
    const-string v0, "320x240"

    goto :goto_0

    .line 333
    :pswitch_24
    const-string v0, "176x144"

    goto :goto_0

    .line 261
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_14
        :pswitch_15
        :pswitch_19
        :pswitch_1d
        :pswitch_1b
        :pswitch_12
        :pswitch_1f
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_7
        :pswitch_d
        :pswitch_1a
        :pswitch_10
        :pswitch_17
        :pswitch_a
        :pswitch_11
        :pswitch_13
        :pswitch_16
        :pswitch_1e
        :pswitch_3
        :pswitch_18
        :pswitch_1
        :pswitch_2
        :pswitch_20
        :pswitch_1c
        :pswitch_4
    .end packed-switch
.end method

.method public static isWideResolution(I)Z
    .locals 3
    .param p0, "resid"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 209
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_HVGA_UI_LAYOUT:Z

    if-eqz v2, :cond_1

    .line 256
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 212
    :cond_1
    packed-switch p0, :pswitch_data_0

    :pswitch_1
    move v0, v1

    .line 256
    goto :goto_0

    .line 234
    :pswitch_2
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_REALIGN_720X480_PREVIEW_LAYOUT_FOR_TABLET:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 235
    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
