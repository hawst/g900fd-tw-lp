.class public Lcom/sec/android/app/bcocr/TextToSpeechAssist;
.super Ljava/lang/Object;
.source "TextToSpeechAssist.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    }
.end annotation


# static fields
.field private static final SAMSUNG_TTS_ENGINE:Ljava/lang/String; = "com.samsung.SMT"

.field protected static final TAG:Ljava/lang/String; = "TextToSpeechAssist"

.field private static final TTS_SPEECH_FAIL:I = -0x1

.field private static final TTS_SPEECH_SUCCESS:I = 0x0

.field private static final UTT_ID:Ljava/lang/String; = "OCR_UTT_ID"


# instance fields
.field protected mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private mIsReady:Z

.field private mLanguage:Ljava/util/Locale;

.field private mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

.field private mNotSupportedLanguageToast:Landroid/widget/Toast;

.field protected mPreference:Landroid/content/SharedPreferences;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;-><init>(Landroid/content/Context;Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    .line 167
    new-instance v0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$1;-><init>(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 177
    new-instance v0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$2;-><init>(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    .line 207
    new-instance v0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;-><init>(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    .line 65
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    const-string v2, "com.samsung.SMT"

    invoke-direct {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioManager:Landroid/media/AudioManager;

    .line 68
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mPreference:Landroid/content/SharedPreferences;

    .line 69
    iput-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    .line 70
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/TextToSpeechAssist;Z)V
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mIsReady:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mListener:Lcom/sec/android/app/bcocr/TextToSpeechAssist$OnTextToSpeechHelperStatusListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method private isLanguageAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    const/4 v2, -0x2

    if-eq v1, v2, :cond_0

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 117
    :cond_0
    const-string v1, "TextToSpeechAssist"

    const-string v2, "language is not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_2

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 126
    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mIsReady:Z

    return v0
.end method

.method public isSpeaking()Z
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    .line 79
    :cond_0
    return v0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 3
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 95
    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->isLanguageAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    const-string v0, "TextToSpeechAssist"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setLanguage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v0, "en_gb"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    new-instance v0, Ljava/util/Locale;

    const-string v1, "UK"

    invoke-direct {v0, p1, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    goto :goto_0

    .line 103
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    .line 104
    const-string v0, "TextToSpeechAssist"

    const-string v1, "language is not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    if-nez v0, :cond_3

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    .line 108
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mIsReady:Z

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 85
    const-string v0, "TextToSpeechAssist"

    const-string v1, "shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 92
    :cond_1
    return-void
.end method

.method public speak(Ljava/lang/String;)I
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    if-nez v0, :cond_2

    .line 132
    const-string v0, "TextToSpeechAssist"

    const-string v1, "language is wrong"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 139
    :cond_1
    const/4 v0, -0x1

    .line 141
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mLanguage:Ljava/util/Locale;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public speak(Ljava/util/Locale;Ljava/lang/String;)I
    .locals 6
    .param p1, "language"    # Ljava/util/Locale;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v3, p1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 147
    .local v0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "utteranceId"

    const-string v4, "OCR_UTT_ID"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v3, p2, v2, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v1

    .line 149
    .local v1, "ret":I
    if-nez v1, :cond_0

    .line 159
    :goto_0
    return v2

    .line 152
    :cond_0
    const-string v3, "TextToSpeechAssist"

    const-string v4, "TTS is failed"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    if-nez v3, :cond_1

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    .line 156
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_2

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mNotSupportedLanguageToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 159
    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 166
    :cond_0
    return-void
.end method
