.class public final Lcom/sec/android/app/bcocr/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final btn_language_color:I = 0x7f020000

.field public static final btn_popup_close:I = 0x7f020001

.field public static final btn_popup_close_focused:I = 0x7f020002

.field public static final btn_popup_close_pressed:I = 0x7f020003

.field public static final camera_rec_camera:I = 0x7f020004

.field public static final camera_rec_camera_dim:I = 0x7f020005

.field public static final camera_rec_camera_press:I = 0x7f020006

.field public static final camera_zoom_minus:I = 0x7f020007

.field public static final camera_zoom_minus_press:I = 0x7f020008

.field public static final camera_zoom_plus:I = 0x7f020009

.field public static final camera_zoom_plus_press:I = 0x7f02000a

.field public static final camera_zoom_progress:I = 0x7f02000b

.field public static final camera_zoom_progress_bg:I = 0x7f02000c

.field public static final capture_rect:I = 0x7f02000d

.field public static final optical_reader_popup_icon_voice_on:I = 0x7f02000e

.field public static final option_menu_upload:I = 0x7f02000f

.field public static final option_menu_upload_dim:I = 0x7f020010

.field public static final photo_reader_capture_icon_flash_dim:I = 0x7f020011

.field public static final photo_reader_capture_icon_flash_off:I = 0x7f020012

.field public static final photo_reader_capture_icon_flash_off_press:I = 0x7f020013

.field public static final photo_reader_capture_icon_flash_strong:I = 0x7f020014

.field public static final photo_reader_capture_icon_flash_strong_dim:I = 0x7f020015

.field public static final photo_reader_capture_icon_flash_strong_press:I = 0x7f020016

.field public static final photo_reader_capture_icon_more:I = 0x7f020017

.field public static final photo_reader_capture_icon_more_dim:I = 0x7f020018

.field public static final photo_reader_capture_icon_more_press:I = 0x7f020019

.field public static final photo_reader_capture_icon_voice_off:I = 0x7f02001a

.field public static final photo_reader_capture_icon_voice_off_press:I = 0x7f02001b

.field public static final photo_reader_capture_icon_voice_on:I = 0x7f02001c

.field public static final photo_reader_capture_icon_voice_on_press:I = 0x7f02001d

.field public static final photo_reader_capture_l_focus:I = 0x7f02001e

.field public static final photo_reader_close_icon_sel:I = 0x7f02001f

.field public static final photo_reader_popup_full:I = 0x7f020020

.field public static final s_name_card_af_base:I = 0x7f020021

.field public static final s_name_card_af_control:I = 0x7f020022

.field public static final s_name_card_af_green:I = 0x7f020023

.field public static final s_name_card_af_green_l:I = 0x7f020024

.field public static final s_name_card_af_red:I = 0x7f020025

.field public static final s_name_card_af_red_l:I = 0x7f020026

.field public static final s_name_card_main_icon:I = 0x7f020027

.field public static final s_name_card_point_green_01:I = 0x7f020028

.field public static final s_name_card_point_green_02:I = 0x7f020029

.field public static final s_name_card_point_green_03:I = 0x7f02002a

.field public static final s_name_card_point_green_04:I = 0x7f02002b

.field public static final s_name_card_point_white_01:I = 0x7f02002c

.field public static final s_name_card_point_white_02:I = 0x7f02002d

.field public static final s_name_card_point_white_03:I = 0x7f02002e

.field public static final s_name_card_point_white_04:I = 0x7f02002f

.field public static final selector_capture_button_l:I = 0x7f020030

.field public static final selector_flash_off:I = 0x7f020031

.field public static final selector_flash_on:I = 0x7f020032

.field public static final selector_more_menu_button:I = 0x7f020033

.field public static final selector_option_menu_upload:I = 0x7f020034

.field public static final selector_voice_off:I = 0x7f020035

.field public static final selector_voice_on:I = 0x7f020036

.field public static final selector_zoom_minus:I = 0x7f020037

.field public static final selector_zoom_plus:I = 0x7f020038

.field public static final spinner_bg:I = 0x7f020039

.field public static final spinner_white_76:I = 0x7f02003a

.field public static final tw_action_bar_autoctn_holo_dim:I = 0x7f02003b

.field public static final tw_action_bar_autoctn_holo_nor:I = 0x7f02003c

.field public static final tw_action_bar_icon_auto_save_holo_light:I = 0x7f02003d

.field public static final tw_action_bar_icon_load_image_holo_light:I = 0x7f02003e

.field public static final tw_action_bar_icon_settings_holo_light:I = 0x7f02003f

.field public static final tw_scrubber_control_holo_dark:I = 0x7f020040

.field public static final tw_toast_frame_holo_dark:I = 0x7f020041


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
