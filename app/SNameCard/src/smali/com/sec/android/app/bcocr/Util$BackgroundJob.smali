.class Lcom/sec/android/app/bcocr/Util$BackgroundJob;
.super Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleAdapter;
.source "Util.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BackgroundJob"
.end annotation


# instance fields
.field private final mActivity:Lcom/sec/android/app/bcocr/MonitoredActivity;

.field private final mCleanupRunner:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private final mJob:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/MonitoredActivity;Ljava/lang/Runnable;Landroid/os/Handler;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;
    .param p2, "job"    # Ljava/lang/Runnable;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleAdapter;-><init>()V

    .line 511
    new-instance v0, Lcom/sec/android/app/bcocr/Util$BackgroundJob$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/Util$BackgroundJob$1;-><init>(Lcom/sec/android/app/bcocr/Util$BackgroundJob;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mCleanupRunner:Ljava/lang/Runnable;

    .line 519
    iput-object p1, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mActivity:Lcom/sec/android/app/bcocr/MonitoredActivity;

    .line 520
    iput-object p2, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mJob:Ljava/lang/Runnable;

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mActivity:Lcom/sec/android/app/bcocr/MonitoredActivity;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/MonitoredActivity;->addLifeCycleListener(Lcom/sec/android/app/bcocr/MonitoredActivity$LifeCycleListener;)V

    .line 522
    iput-object p3, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    .line 523
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/Util$BackgroundJob;)Lcom/sec/android/app/bcocr/MonitoredActivity;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mActivity:Lcom/sec/android/app/bcocr/MonitoredActivity;

    return-object v0
.end method


# virtual methods
.method public onActivityDestroyed(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mCleanupRunner:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mCleanupRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 544
    :cond_0
    return-void
.end method

.method public onActivityStarted(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 554
    return-void
.end method

.method public onActivityStopped(Lcom/sec/android/app/bcocr/MonitoredActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/app/bcocr/MonitoredActivity;

    .prologue
    .line 549
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 528
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mJob:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mCleanupRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 534
    :cond_0
    return-void

    .line 529
    :catchall_0
    move-exception v0

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 531
    iget-object v1, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/Util$BackgroundJob;->mCleanupRunner:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 533
    :cond_1
    throw v0
.end method
