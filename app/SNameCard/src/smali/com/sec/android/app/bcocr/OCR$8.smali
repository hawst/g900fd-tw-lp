.class Lcom/sec/android/app/bcocr/OCR$8;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$8;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    .line 981
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$8;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 983
    .local v1, "preferences":Landroid/content/SharedPreferences;
    if-nez p2, :cond_1

    .line 984
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 985
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "setting_image_auto_save"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 986
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 992
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$8;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/OCR;->access$23(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$8;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/OCR;->access$23(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 993
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$8;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mOptSaveImageDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/OCR;->access$23(Lcom/sec/android/app/bcocr/OCR;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 995
    :cond_0
    return-void

    .line 988
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 989
    .restart local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "setting_image_auto_save"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 990
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
