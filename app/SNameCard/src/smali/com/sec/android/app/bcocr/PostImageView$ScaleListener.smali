.class public Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PostImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/PostImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/PostImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/PostImageView;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    iget v1, v1, Lcom/sec/android/app/bcocr/PostImageView;->mLastScaleFactor:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float v0, v1, v2

    .line 317
    .local v0, "targetScale":F
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/PostImageView;->getMaxZoom()F

    move-result v1

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 318
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/PostImageView;->zoomTo(FFF)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostImageView;->getMaxZoom()F

    move-result v2

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v1, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/PostImageView;->invalidate()V

    .line 322
    const/4 v1, 0x0

    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/PostImageView;->access$15(Lcom/sec/android/app/bcocr/PostImageView;Z)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    iget v1, v1, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    iput v1, v0, Lcom/sec/android/app/bcocr/PostImageView;->mLastScaleFactor:F

    .line 330
    return-void
.end method
