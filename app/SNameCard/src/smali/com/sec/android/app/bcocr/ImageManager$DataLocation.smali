.class public final enum Lcom/sec/android/app/bcocr/ImageManager$DataLocation;
.super Ljava/lang/Enum;
.source "ImageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/ImageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataLocation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/bcocr/ImageManager$DataLocation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

.field public static final enum EXTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

.field public static final enum INTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

.field public static final enum NONE:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    new-instance v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->NONE:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    new-instance v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->INTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    new-instance v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->EXTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    new-instance v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->ALL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    .line 114
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    sget-object v1, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->NONE:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->INTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->EXTERNAL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->ALL:Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->ENUM$VALUES:[Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/bcocr/ImageManager$DataLocation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/bcocr/ImageManager$DataLocation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/app/bcocr/ImageManager$DataLocation;->ENUM$VALUES:[Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/bcocr/ImageManager$DataLocation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
