.class Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->startSavePicture([BLcom/sec/android/seccamera/SecCamera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

.field private final synthetic val$jpegData:[B


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    iput-object p2, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->val$jpegData:[B

    .line 2134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2137
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 2139
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$5(Lcom/sec/android/app/bcocr/OCREngine;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$6(Lcom/sec/android/app/bcocr/OCREngine;I)V

    .line 2141
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getnOCRNameCardCaptureModeType()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2142
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->val$jpegData:[B

    # invokes: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->ocrRecognizeCapturedImage([B)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$1(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2143
    const-string v0, "OCREngine"

    const-string v1, "Captured Image Recognization Error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2146
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mOnRecogStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

    if-eqz v0, :cond_1

    .line 2147
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mOnRecogStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

    invoke-interface {v0}, Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;->onRecognitionStateChangedForJPEG()V

    .line 2151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->val$jpegData:[B

    # invokes: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->storeImage([B)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$2(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)Z

    .line 2152
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->clearCaptureImageData()V

    .line 2153
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    .line 2155
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;->this$1:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$5(Lcom/sec/android/app/bcocr/OCREngine;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$6(Lcom/sec/android/app/bcocr/OCREngine;I)V

    .line 2157
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-ImageSavingEnd**Point["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2158
    return-void
.end method
