.class abstract Lcom/sec/android/app/bcocr/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;
    }
.end annotation


# static fields
.field static final SCALE_RATE:F = 1.25f

.field private static final TAG:Ljava/lang/String; = "ImageViewTouchBase"


# instance fields
.field protected mBaseMatrix:Landroid/graphics/Matrix;

.field protected final mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

.field protected mBitmapRect:Landroid/graphics/RectF;

.field protected mCenterRect:Landroid/graphics/RectF;

.field private final mDisplayMatrix:Landroid/graphics/Matrix;

.field protected mHandler:Landroid/os/Handler;

.field protected mLastXTouchPos:I

.field protected mLastYTouchPos:I

.field private final mMatrixValues:[F

.field mMaxZoom:F

.field private mOnLayoutRunnable:Ljava/lang/Runnable;

.field private mRecycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

.field protected mSuppMatrix:Landroid/graphics/Matrix;

.field mThisHeight:I

.field mThisWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 235
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    .line 47
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 51
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    .line 54
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMatrixValues:[F

    .line 57
    new-instance v0, Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-direct {v0, v2}, Lcom/sec/android/app/bcocr/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    .line 59
    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisHeight:I

    .line 62
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    .line 63
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    .line 118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    .line 148
    iput-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->init()V

    .line 237
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 240
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    .line 47
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 51
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    .line 54
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMatrixValues:[F

    .line 57
    new-instance v0, Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-direct {v0, v2}, Lcom/sec/android/app/bcocr/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    .line 59
    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisHeight:I

    .line 62
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    .line 63
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    .line 118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    .line 148
    iput-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->init()V

    .line 242
    return-void
.end method

.method private getProperBaseMatrix(Lcom/sec/android/app/bcocr/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1, "bitmap"    # Lcom/sec/android/app/bcocr/RotateBitmap;
    .param p2, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    const/high16 v9, 0x40000000    # 2.0f

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v7

    int-to-float v4, v7

    .line 297
    .local v4, "viewWidth":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v7

    int-to-float v3, v7

    .line 299
    .local v3, "viewHeight":F
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getWidth()I

    move-result v7

    int-to-float v5, v7

    .line 300
    .local v5, "w":F
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    .line 301
    .local v0, "h":F
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 305
    div-float v7, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 306
    .local v6, "widthScale":F
    div-float v7, v3, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 307
    .local v1, "heightScale":F
    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 309
    .local v2, "scale":F
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getRotateMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 310
    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 313
    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    .line 314
    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    .line 312
    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 315
    return-void
.end method

.method private setImageBitmap(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotation"    # I

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 131
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 132
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 135
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 136
    .local v1, "old":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/bcocr/RotateBitmap;->setRotation(I)V

    .line 139
    if-eqz v1, :cond_1

    if-eq v1, p1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    if-eqz v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    invoke-interface {v2, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;->recycle(Landroid/graphics/Bitmap;)V

    .line 142
    :cond_1
    return-void
.end method


# virtual methods
.method protected center(ZZ)V
    .locals 12
    .param p1, "horizontal"    # Z
    .param p2, "vertical"    # Z

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 191
    iget-object v8, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-nez v8, :cond_0

    .line 232
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 197
    .local v3, "m":Landroid/graphics/Matrix;
    new-instance v4, Landroid/graphics/RectF;

    .line 198
    iget-object v8, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    .line 199
    iget-object v9, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    .line 197
    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 201
    .local v4, "rect":Landroid/graphics/RectF;
    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 203
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    .line 204
    .local v2, "height":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 206
    .local v7, "width":F
    const/4 v0, 0x0

    .local v0, "deltaX":F
    const/4 v1, 0x0

    .line 208
    .local v1, "deltaY":F
    if-eqz p2, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v5

    .line 210
    .local v5, "viewHeight":I
    int-to-float v8, v5

    cmpg-float v8, v2, v8

    if-gez v8, :cond_3

    .line 211
    int-to-float v8, v5

    sub-float/2addr v8, v2

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    .line 219
    .end local v5    # "viewHeight":I
    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v6

    .line 221
    .local v6, "viewWidth":I
    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    .line 222
    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    .line 230
    .end local v6    # "viewWidth":I
    :cond_2
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->postTranslate(FF)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 212
    .restart local v5    # "viewHeight":I
    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    .line 213
    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    .line 214
    goto :goto_1

    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    .line 223
    .end local v5    # "viewHeight":I
    .restart local v6    # "viewWidth":I
    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    .line 224
    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    .line 225
    goto :goto_2

    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 226
    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 145
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    .line 146
    return-void
.end method

.method public easeOut(DDDD)D
    .locals 5
    .param p1, "time"    # D
    .param p3, "start"    # D
    .param p5, "end"    # D
    .param p7, "duration"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 573
    div-double v0, p1, p7

    sub-double p1, v0, v2

    mul-double v0, p1, p1

    mul-double/2addr v0, p1

    add-double/2addr v0, v2

    mul-double/2addr v0, p5

    add-double/2addr v0, p3

    return-wide v0
.end method

.method protected getBitmapDisplayedRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    .line 268
    const/4 v1, 0x0

    .line 278
    :goto_0
    return-object v1

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 273
    .local v0, "m":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 273
    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 277
    .local v1, "rect":Landroid/graphics/RectF;
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method protected getBitmapRect()Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 257
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 258
    const/4 v2, 0x0

    .line 263
    :goto_0
    return-object v2

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 261
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method protected getCenter(ZZ)Landroid/graphics/RectF;
    .locals 12
    .param p1, "horizontal"    # Z
    .param p2, "vertical"    # Z

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 484
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 485
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 518
    :goto_0
    return-object v8

    .line 488
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 490
    .local v4, "rect":Landroid/graphics/RectF;
    if-nez v4, :cond_1

    .line 491
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 494
    :cond_1
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 495
    .local v3, "height":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 496
    .local v7, "width":F
    const/4 v0, 0x0

    .local v0, "deltaX":F
    const/4 v1, 0x0

    .line 497
    .local v1, "deltaY":F
    if-eqz p2, :cond_2

    .line 498
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v5

    .line 499
    .local v5, "viewHeight":I
    int-to-float v8, v5

    cmpg-float v8, v3, v8

    if-gez v8, :cond_4

    .line 500
    int-to-float v8, v5

    sub-float/2addr v8, v3

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    .line 507
    .end local v5    # "viewHeight":I
    :cond_2
    :goto_1
    if-eqz p1, :cond_3

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v6

    .line 509
    .local v6, "viewWidth":I
    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_6

    .line 510
    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    .line 517
    .end local v6    # "viewWidth":I
    :cond_3
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v0, v1, v10, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 518
    iget-object v8, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    goto :goto_0

    .line 501
    .restart local v5    # "viewHeight":I
    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_5

    .line 502
    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    .line 503
    goto :goto_1

    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 504
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    .line 511
    .end local v5    # "viewHeight":I
    .restart local v6    # "viewWidth":I
    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_7

    .line 512
    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    .line 513
    goto :goto_2

    :cond_7
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_3

    .line 514
    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method protected getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getMaxZoom()F
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    return v0
.end method

.method protected getScale()F
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getScale(Landroid/graphics/Matrix;)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .param p2, "whichValue"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method protected init()V
    .locals 1

    .prologue
    .line 245
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 246
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    .line 247
    return-void
.end method

.method protected maxZoom()F
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 334
    const/high16 v0, 0x3f800000    # 1.0f

    .line 336
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 96
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 97
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 99
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 106
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 111
    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomTo(F)V

    .line 112
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 80
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 81
    sub-int v1, p4, p2

    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisWidth:I

    .line 82
    sub-int v1, p5, p3

    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mThisHeight:I

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 84
    .local v0, "r":Ljava/lang/Runnable;
    if-eqz v0, :cond_0

    .line 85
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 86
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getProperBaseMatrix(Lcom/sec/android/app/bcocr/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 92
    :cond_1
    return-void
.end method

.method protected onZoom(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 526
    return-void
.end method

.method protected panBy(DD)V
    .locals 3
    .param p1, "dx"    # D
    .param p3, "dy"    # D

    .prologue
    .line 534
    double-to-float v0, p1

    double-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->postTranslate(FF)V

    .line 535
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 536
    return-void
.end method

.method protected panBy(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 529
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->postTranslate(FF)V

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 531
    return-void
.end method

.method protected postTranslate(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 524
    return-void
.end method

.method public scrollBy(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 539
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->panBy(FF)V

    .line 540
    return-void
.end method

.method protected scrollBy(FFD)V
    .locals 11
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F
    .param p3, "durationMs"    # D

    .prologue
    .line 543
    float-to-double v6, p1

    .line 544
    .local v6, "dx":D
    float-to-double v8, p2

    .line 545
    .local v8, "dy":D
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 546
    .local v4, "startTime":J
    iget-object v10, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/bcocr/ImageViewTouchBase$3;

    move-object v1, p0

    move-wide v2, p3

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/bcocr/ImageViewTouchBase$3;-><init>(Lcom/sec/android/app/bcocr/ImageViewTouchBase;DJDD)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 570
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    .line 126
    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resetSupp"    # Z

    .prologue
    .line 154
    new-instance v0, Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-direct {v0, p1}, Lcom/sec/android/app/bcocr/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/android/app/bcocr/RotateBitmap;Z)V

    .line 155
    return-void
.end method

.method public setImageRotateBitmapResetBase(Lcom/sec/android/app/bcocr/RotateBitmap;Z)V
    .locals 3
    .param p1, "bitmap"    # Lcom/sec/android/app/bcocr/RotateBitmap;
    .param p2, "resetSupp"    # Z

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v0

    .line 161
    .local v0, "viewWidth":I
    if-gtz v0, :cond_0

    .line 162
    new-instance v1, Lcom/sec/android/app/bcocr/ImageViewTouchBase$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase$1;-><init>(Lcom/sec/android/app/bcocr/ImageViewTouchBase;Lcom/sec/android/app/bcocr/RotateBitmap;Z)V

    iput-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 183
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getProperBaseMatrix(Lcom/sec/android/app/bcocr/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 172
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/RotateBitmap;->getOrientation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    .line 178
    :goto_1
    if-eqz p2, :cond_1

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 181
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->maxZoom()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    goto :goto_0

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 175
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setRecycler(Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;)V
    .locals 0
    .param p1, "r"    # Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    .line 73
    return-void
.end method

.method protected zoomIn()V
    .locals 1

    .prologue
    .line 398
    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomIn(F)V

    .line 399
    return-void
.end method

.method protected zoomIn(F)V
    .locals 5
    .param p1, "rate"    # F

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v4

    .line 414
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v4

    .line 416
    .local v1, "cy":F
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method protected zoomIn(FFF)V
    .locals 7
    .param p1, "rate"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v4

    .line 451
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v4

    .line 452
    .local v1, "cy":F
    sub-float v2, v0, p2

    div-float/2addr v2, v5

    sub-float v3, v1, p3

    div-float/2addr v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->panBy(FF)V

    .line 454
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 455
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 456
    invoke-virtual {p0, v6, v6}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0
.end method

.method protected zoomOut()V
    .locals 1

    .prologue
    .line 402
    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomOut(F)V

    .line 403
    return-void
.end method

.method protected zoomOut(F)V
    .locals 7
    .param p1, "rate"    # F

    .prologue
    const/4 v6, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 421
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    .line 439
    :goto_0
    return-void

    .line 425
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    .line 426
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    .line 429
    .local v1, "cy":F
    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 430
    .local v2, "tmp":Landroid/graphics/Matrix;
    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 432
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 433
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 437
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 438
    invoke-virtual {p0, v6, v6}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0

    .line 435
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_1
.end method

.method protected zoomOut(FFF)V
    .locals 8
    .param p1, "rate"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/4 v7, 0x1

    const/high16 v6, 0x40a00000    # 5.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 460
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/bcocr/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    .line 479
    :goto_0
    return-void

    .line 464
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    .line 465
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    .line 466
    .local v1, "cy":F
    sub-float v3, v0, p2

    div-float/2addr v3, v6

    sub-float v4, v1, p3

    div-float/2addr v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->panBy(FF)V

    .line 469
    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 470
    .local v2, "tmp":Landroid/graphics/Matrix;
    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 472
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 473
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 477
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 478
    invoke-virtual {p0, v7, v7}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0

    .line 475
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_1
.end method

.method protected zoomTo(F)V
    .locals 4
    .param p1, "scale"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 378
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 380
    .local v1, "cy":F
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomTo(FFF)V

    .line 381
    return-void
.end method

.method public zoomTo(FF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "durationMs"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 385
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 386
    .local v1, "cy":F
    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomTo(FFFF)V

    .line 387
    return-void
.end method

.method protected zoomTo(FFF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .prologue
    const/4 v3, 0x1

    .line 340
    iget v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 341
    iget p1, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mMaxZoom:F

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v1

    .line 345
    .local v1, "oldScale":F
    div-float v0, p1, v1

    .line 347
    .local v0, "deltaScale":F
    iget-object v2, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 349
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->center(ZZ)V

    .line 350
    return-void
.end method

.method protected zoomTo(FFFF)V
    .locals 10
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F
    .param p4, "durationMs"    # F

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v0

    sub-float v0, p1, v0

    div-float v7, v0, p4

    .line 355
    .local v7, "incrementPerMs":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getScale()F

    move-result v6

    .line 356
    .local v6, "oldScale":F
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 358
    .local v4, "startTime":J
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/bcocr/ImageViewTouchBase$2;

    move-object v2, p0

    move v3, p4

    move v8, p2

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/bcocr/ImageViewTouchBase$2;-><init>(Lcom/sec/android/app/bcocr/ImageViewTouchBase;FJFFFF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 374
    :cond_0
    return-void
.end method

.method protected zoomToPoint(FFF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 391
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 393
    .local v1, "cy":F
    sub-float v2, v0, p2

    sub-float v3, v1, p3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->panBy(FF)V

    .line 394
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomTo(FFF)V

    .line 395
    return-void
.end method
