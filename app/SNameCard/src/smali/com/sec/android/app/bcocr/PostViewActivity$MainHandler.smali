.class Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;
.super Landroid/os/Handler;
.source "PostViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/PostViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/PostViewActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/PostViewActivity;Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;)V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;-><init>(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x3

    const/4 v2, 0x0

    .line 267
    const-string v3, "PostViewActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[OCR] handleMessage :: msg.what = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 270
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$1(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->setTitle(Ljava/lang/String;)V

    .line 274
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onStart()V

    goto :goto_0

    .line 278
    :pswitch_1
    const-string v3, "PostViewActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "!!!!! result of recognizing : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I
    invoke-static {v5}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$2(Lcom/sec/android/app/bcocr/PostViewActivity;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$0(Lcom/sec/android/app/bcocr/PostViewActivity;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onStop()V

    .line 283
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$3(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget v3, v3, Lcom/sec/android/app/bcocr/PostViewActivity;->nGetText_NumberOfFoundLink:I

    if-lez v3, :cond_4

    const/4 v0, 0x1

    .line 284
    .local v0, "isRequestFromOCR":Z
    :goto_1
    if-nez v0, :cond_3

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$2(Lcom/sec/android/app/bcocr/PostViewActivity;)I

    move-result v3

    if-ne v3, v6, :cond_5

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->onCreateErrorMsgDialog()V
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$4(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    .line 294
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIsLoadImageMode:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$6(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetCroppedImageMode:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$1(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_7

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->invalidateOptionsMenu()V

    .line 296
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->setImageAndDisplayWH()V
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$7(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->initPostView()V
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$8(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->isValidSavetoContact(Z)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/bcocr/PostViewActivity;->savetoContact(Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/PostViewActivity;->nCapturedFilePath:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->setImageViewOnOrientation(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$9(Lcom/sec/android/app/bcocr/PostViewActivity;Ljava/lang/String;)V

    .line 301
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 302
    .local v1, "msgFinish":Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    .line 303
    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mMainHandler:Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;
    invoke-static {}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$10()Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;

    move-result-object v2

    const-wide/16 v4, 0x5dc

    invoke-virtual {v2, v1, v4, v5}, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 309
    .end local v1    # "msgFinish":Landroid/os/Message;
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$2(Lcom/sec/android/app/bcocr/PostViewActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->sendResult(I)V

    goto/16 :goto_0

    .end local v0    # "isRequestFromOCR":Z
    :cond_4
    move v0, v2

    .line 283
    goto :goto_1

    .line 288
    .restart local v0    # "isRequestFromOCR":Z
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mResultOfGetDataFromImage:I
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$2(Lcom/sec/android/app/bcocr/PostViewActivity;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # invokes: Lcom/sec/android/app/bcocr/PostViewActivity;->onCreateErrorMsgDialogUnsupportedImageFormat()V
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$5(Lcom/sec/android/app/bcocr/PostViewActivity;)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto :goto_2

    .line 305
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0034

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto :goto_3

    .line 311
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto/16 :goto_0

    .line 315
    .end local v0    # "isRequestFromOCR":Z
    :pswitch_2
    const-string v2, "PostViewActivity"

    const-string v3, "Handler : FINISH_NAMECARD_CAPTURE_MODE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mIsNameCardGetDataMode:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$3(Lcom/sec/android/app/bcocr/PostViewActivity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    # getter for: Lcom/sec/android/app/bcocr/PostViewActivity;->mSavetoContactIntent:Landroid/content/Intent;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->access$11(Lcom/sec/android/app/bcocr/PostViewActivity;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/PostViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 319
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostViewActivity$MainHandler;->this$0:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->finish()V

    goto/16 :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
