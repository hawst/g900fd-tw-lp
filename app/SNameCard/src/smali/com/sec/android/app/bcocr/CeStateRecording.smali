.class public Lcom/sec/android/app/bcocr/CeStateRecording;
.super Lcom/sec/android/app/bcocr/AbstractCeState;
.source "CeStateRecording.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CeStateRecording"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V
    .locals 0
    .param p1, "ocrEngine"    # Lcom/sec/android/app/bcocr/OCREngine;
    .param p2, "requestQueue"    # Lcom/sec/android/app/bcocr/CeRequestQueue;
    .param p3, "id"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/AbstractCeState;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public cancelRequest(Lcom/sec/android/app/bcocr/CeRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    .line 35
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 39
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 53
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 42
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 47
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    goto :goto_0

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x8 -> :sswitch_1
        0x66 -> :sswitch_0
    .end sparse-switch
.end method

.method public handleRequest(Lcom/sec/android/app/bcocr/CeRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/sec/android/app/bcocr/CeRequest;

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 74
    const-string v0, "CeStateRecording"

    const-string v1, "invalid request id for current state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    .line 77
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 60
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/CeRequest;->getParam()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/bcocr/OCREngine;->doWaitAsync(I)V

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doProcessBackSync()V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doAutoFocusAsync()V

    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/CeStateRecording;->getOCREngine()Lcom/sec/android/app/bcocr/OCREngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->doTakePictureAsync()V

    move v0, v1

    .line 72
    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0xd -> :sswitch_0
        0x18 -> :sswitch_1
    .end sparse-switch
.end method
