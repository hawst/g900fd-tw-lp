.class public Lcom/sec/android/app/bcocr/PostImageView;
.super Lcom/sec/android/app/bcocr/ImageViewTouchBase;
.source "PostImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/PostImageView$GestureListener;,
        Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;
    }
.end annotation


# static fields
.field static final BORDER_BOTTOM:I = 0x8

.field static final BORDER_LEFT:I = 0x2

.field static final BORDER_NONE:I = 0x0

.field static final BORDER_RIGHT:I = 0x4

.field static final BORDER_TOP:I = 0x1

.field static final MIN_ZOOM:F = 1.0f

.field public static TRANS_SLIDE_IN_LEFT:I


# instance fields
.field private mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

.field private mBorder:I

.field private mBorderBottom:Landroid/graphics/Bitmap;

.field private mBorderLeft:Landroid/graphics/Bitmap;

.field private mBorderRight:Landroid/graphics/Bitmap;

.field private mBorderTop:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field protected mCurrentScaleFactor:F

.field protected mDoubleTapDirection:I

.field protected mDoubleTapEnabled:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field protected mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mIsScaleActivated:Z

.field private mIsScrolling:Z

.field protected mLastScaleFactor:F

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field protected mScaleFactor:F

.field protected mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field protected mScrollEnabled:Z

.field public recycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mDoubleTapEnabled:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScrollEnabled:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScrolling:Z

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScaleActivated:Z

    .line 57
    iput v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    .line 63
    new-instance v0, Lcom/sec/android/app/bcocr/PostImageView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/PostImageView$1;-><init>(Lcom/sec/android/app/bcocr/PostImageView;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->recycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->recycler:Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;

    invoke-super {p0, v0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;)V

    .line 78
    iput-object p1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mContext:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/bcocr/PostViewActivity;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->PostImageView_Init()V

    .line 81
    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/app/bcocr/PostImageView;)Landroid/view/ScaleGestureDetector;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/bcocr/PostImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/PostImageView;->checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/app/bcocr/PostImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/PostImageView;->checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result v0

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/bcocr/PostImageView;Z)V
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScrolling:Z

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/app/bcocr/PostImageView;Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScaleActivated:Z

    return-void
.end method

.method private checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 206
    .local v0, "bitmapRect":Landroid/graphics/RectF;
    if-nez v0, :cond_1

    move p3, v1

    .line 232
    .end local p3    # "distanceX":F
    :cond_0
    :goto_0
    return p3

    .line 209
    .restart local p3    # "distanceX":F
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    move p3, v1

    .line 211
    goto :goto_0

    .line 213
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-lez v2, :cond_4

    .line 214
    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    .line 215
    invoke-direct {p0, v4}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    move p3, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_3
    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, p3

    cmpl-float v1, v2, v1

    if-lez v1, :cond_0

    .line 219
    invoke-direct {p0, v4}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    .line 220
    iget p3, v0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 223
    :cond_4
    iget v2, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    .line 224
    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    move p3, v1

    .line 225
    goto :goto_0

    .line 227
    :cond_5
    iget v1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 228
    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    .line 229
    iget v1, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p3

    goto :goto_0
.end method

.method private checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 7
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceY"    # F

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 239
    .local v0, "bitmapRect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 240
    .local v1, "extraHeight":I
    if-nez v0, :cond_1

    move p3, v2

    .line 267
    .end local p3    # "distanceY":F
    :cond_0
    :goto_0
    return p3

    .line 244
    .restart local p3    # "distanceY":F
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_2

    move p3, v2

    .line 246
    goto :goto_0

    .line 248
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v2

    if-lez v3, :cond_4

    .line 249
    iget v3, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v2

    if-lez v3, :cond_3

    .line 250
    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    move p3, v2

    .line 251
    goto :goto_0

    .line 253
    :cond_3
    iget v3, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, p3

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    .line 254
    invoke-direct {p0, v5}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    .line 255
    iget p3, v0, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 258
    :cond_4
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v1

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 259
    invoke-direct {p0, v6}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    move p3, v2

    .line 260
    goto :goto_0

    .line 262
    :cond_5
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    int-to-float v3, v1

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 263
    invoke-direct {p0, v6}, Lcom/sec/android/app/bcocr/PostImageView;->setBorder(I)V

    .line 264
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    int-to-float v3, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result p3

    goto :goto_0
.end method

.method private setBorder(I)V
    .locals 1
    .param p1, "border"    # I

    .prologue
    .line 200
    iget v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    .line 201
    return-void
.end method


# virtual methods
.method public PostImageView_Init()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 109
    invoke-super {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->init()V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getGestureListener()Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 113
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 114
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 116
    iput v4, p0, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    .line 117
    iput v4, p0, Lcom/sec/android/app/bcocr/PostImageView;->mLastScaleFactor:F

    .line 118
    iput v5, p0, Lcom/sec/android/app/bcocr/PostImageView;->mDoubleTapDirection:I

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->loadBorderResources()V

    .line 121
    return-void
.end method

.method public bridge synthetic clear()V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->clear()V

    return-void
.end method

.method public bridge synthetic easeOut(DDDD)D
    .locals 3

    .prologue
    .line 1
    invoke-super/range {p0 .. p8}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->easeOut(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method protected getGestureListener()Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;-><init>(Lcom/sec/android/app/bcocr/PostImageView;)V

    return-object v0
.end method

.method public bridge synthetic getMaxZoom()F
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->getMaxZoom()F

    move-result v0

    return v0
.end method

.method protected getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/PostImageView$ScaleListener;-><init>(Lcom/sec/android/app/bcocr/PostImageView;)V

    return-object v0
.end method

.method public loadBorderResources()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderLeft:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderLeft:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderLeft:Landroid/graphics/Bitmap;

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderTop:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderTop:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderTop:Landroid/graphics/Bitmap;

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderRight:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderRight:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderRight:Landroid/graphics/Bitmap;

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderBottom:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderBottom:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderBottom:Landroid/graphics/Bitmap;

    .line 104
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->clear()V

    .line 105
    invoke-super {p0, v1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;)V

    .line 106
    return-void
.end method

.method protected onDoubleTapPost()F
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 190
    iget v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_0

    .line 191
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mDoubleTapDirection:I

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getMaxZoom()F

    move-result v0

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mDoubleTapDirection:I

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 180
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/PostImageView;->showBoundaryEffect(Landroid/graphics/Canvas;)V

    .line 181
    return-void
.end method

.method public bridge synthetic onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 133
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 135
    .local v0, "action":I
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/PostViewActivity;->getIsScreenCropMode()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v1

    .line 139
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v2, :cond_0

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 146
    and-int/lit16 v2, v0, 0xff

    packed-switch v2, :pswitch_data_0

    .line 174
    :cond_2
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 149
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getScale()F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 150
    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/bcocr/PostImageView;->zoomTo(FF)V

    .line 153
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScrolling:Z

    if-eqz v2, :cond_4

    .line 155
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScrolling:Z

    .line 156
    iput v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    .line 160
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScrolling:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScaleActivated:Z

    if-nez v2, :cond_2

    .line 162
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScaleActivated:Z

    .line 163
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 169
    :pswitch_1
    iput-boolean v1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mIsScaleActivated:Z

    goto :goto_1

    .line 146
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onZoom(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->onZoom(F)V

    .line 186
    iput p1, p0, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    .line 187
    return-void
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 272
    const/high16 v0, 0x3f800000    # 1.0f

    .line 273
    .local v0, "targetScale":F
    iput v0, p0, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    .line 274
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/PostImageView;->zoomTo(F)V

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->invalidate()V

    .line 276
    return-void
.end method

.method public bridge synthetic scrollBy(FF)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->scrollBy(FF)V

    return-void
.end method

.method public bridge synthetic setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public bridge synthetic setImageRotateBitmapResetBase(Lcom/sec/android/app/bcocr/RotateBitmap;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/android/app/bcocr/RotateBitmap;Z)V

    return-void
.end method

.method public bridge synthetic setRecycler(Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/bcocr/ImageViewTouchBase$Recycler;)V

    return-void
.end method

.method public showBoundaryEffect(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 334
    iget v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    if-nez v8, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/PostViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 339
    .local v1, "disp":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 341
    .local v3, "point":Landroid/graphics/Point;
    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 342
    iget v7, v3, Landroid/graphics/Point;->x:I

    .line 343
    .local v7, "windowWidth":I
    iget v6, v3, Landroid/graphics/Point;->y:I

    .line 345
    .local v6, "windowHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/PostImageView;->getBitmapDisplayedRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 346
    .local v4, "rect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 347
    .local v2, "dispRect":Landroid/graphics/RectF;
    const/16 v0, 0xb9

    .line 349
    .local v0, "borderThick":I
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/PostViewActivity;->getPostViewMode()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    const/16 v5, 0x5a

    .line 351
    .local v5, "translatorHeight":I
    :goto_1
    iget v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_2

    .line 352
    new-instance v8, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 353
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-int v8, v8

    if-le v8, v6, :cond_6

    .line 354
    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->left:F

    int-to-float v11, v0

    add-float/2addr v10, v11

    int-to-float v11, v6

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 358
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderLeft:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v9, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 360
    :cond_2
    iget v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_3

    .line 361
    new-instance v8, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 362
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/PostViewActivity;->getPostViewMode()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/bcocr/PostImageView;->mActivityContext:Lcom/sec/android/app/bcocr/PostViewActivity;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    .line 363
    iget v8, v4, Landroid/graphics/RectF;->top:F

    int-to-float v9, v5

    cmpl-float v8, v8, v9

    if-lez v8, :cond_7

    .line 364
    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    iget v11, v4, Landroid/graphics/RectF;->top:F

    int-to-float v12, v0

    add-float/2addr v11, v12

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 375
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderTop:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v9, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 377
    :cond_3
    iget v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    and-int/lit8 v8, v8, 0x4

    if-eqz v8, :cond_4

    .line 378
    new-instance v8, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 379
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-int v8, v8

    if-le v8, v6, :cond_a

    .line 380
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v0

    sub-float/2addr v8, v9

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    int-to-float v11, v6

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 384
    :goto_4
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderRight:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v9, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 386
    :cond_4
    iget v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorder:I

    and-int/lit8 v8, v8, 0x8

    if-eqz v8, :cond_0

    .line 387
    new-instance v8, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 388
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v8

    float-to-int v8, v8

    if-le v8, v7, :cond_b

    .line 389
    const/4 v8, 0x0

    iget v9, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    add-float/2addr v9, v10

    int-to-float v10, v0

    sub-float/2addr v9, v10

    int-to-float v10, v7

    iget v11, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v11, v12

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 393
    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/bcocr/PostImageView;->mBorderBottom:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v9, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 349
    .end local v5    # "translatorHeight":I
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 356
    .restart local v5    # "translatorHeight":I
    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->left:F

    int-to-float v11, v0

    add-float/2addr v10, v11

    iget v11, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 366
    :cond_7
    iget v8, v4, Landroid/graphics/RectF;->left:F

    int-to-float v9, v5

    iget v10, v4, Landroid/graphics/RectF;->right:F

    add-int v11, v5, v0

    int-to-float v11, v11

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_3

    .line 369
    :cond_8
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v8

    float-to-int v8, v8

    if-le v8, v7, :cond_9

    .line 370
    const/4 v8, 0x0

    iget v9, v4, Landroid/graphics/RectF;->top:F

    int-to-float v10, v5

    add-float/2addr v9, v10

    int-to-float v10, v7

    iget v11, v4, Landroid/graphics/RectF;->top:F

    int-to-float v12, v0

    add-float/2addr v11, v12

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_3

    .line 372
    :cond_9
    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    iget v11, v4, Landroid/graphics/RectF;->top:F

    int-to-float v12, v0

    add-float/2addr v11, v12

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_3

    .line 382
    :cond_a
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v0

    sub-float/2addr v8, v9

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    iget v11, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_4

    .line 391
    :cond_b
    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    add-float/2addr v9, v10

    int-to-float v10, v0

    sub-float/2addr v9, v10

    iget v10, v4, Landroid/graphics/RectF;->right:F

    iget v11, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v11, v12

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_5
.end method

.method public bridge synthetic zoomTo(FF)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/bcocr/ImageViewTouchBase;->zoomTo(FF)V

    return-void
.end method
