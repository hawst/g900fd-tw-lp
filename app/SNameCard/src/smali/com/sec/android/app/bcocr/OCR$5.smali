.class Lcom/sec/android/app/bcocr/OCR$5;
.super Landroid/os/Handler;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 5176
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 5179
    const-string v0, "OCR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[OCR] OCRHandler Message: what= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5180
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 5202
    :cond_0
    :goto_0
    return-void

    .line 5182
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5183
    const-string v0, "OCR"

    const-string v1, "[mycheck]detect 5 : OCRHandler(OCR_PREV_RECOGNITION) ++"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5187
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->updateDetectedNameCardEdge()V

    goto :goto_0

    .line 5190
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const-string v1, "Text"

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onChangeRecognitionThread(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/app/bcocr/OCR;->access$9(Lcom/sec/android/app/bcocr/OCR;Ljava/lang/String;Z)V

    .line 5191
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onFinishRecognitionThread()V
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$10(Lcom/sec/android/app/bcocr/OCR;)V

    goto :goto_0

    .line 5194
    :pswitch_3
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->OCR_SUPPORT_MULTI_THREAD_TO_RECOGNIZE:Z

    if-eqz v0, :cond_0

    .line 5195
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const-string v1, "QRCode"

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onChangeRecognitionThread(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/app/bcocr/OCR;->access$9(Lcom/sec/android/app/bcocr/OCR;Ljava/lang/String;Z)V

    .line 5196
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$5;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onFinishRecognitionThread()V
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCR;->access$10(Lcom/sec/android/app/bcocr/OCR;)V

    goto :goto_0

    .line 5180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
