.class public Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;
.super Landroid/os/Handler;
.source "OCREngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "StateMessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 227
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got message..."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/bcocr/AbstractCeState;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
