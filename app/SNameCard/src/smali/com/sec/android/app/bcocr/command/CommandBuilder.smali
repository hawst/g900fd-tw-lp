.class public Lcom/sec/android/app/bcocr/command/CommandBuilder;
.super Ljava/lang/Object;
.source "CommandBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildCommand(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;)Lcom/sec/android/app/bcocr/command/MenuCommand;
    .locals 1
    .param p0, "id"    # I
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "parentView"    # Landroid/view/ViewGroup;
    .param p3, "menuDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;

    .prologue
    .line 32
    const/4 v0, 0x2

    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/android/app/bcocr/command/CommandBuilder;->buildCommand(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)Lcom/sec/android/app/bcocr/command/MenuCommand;

    move-result-object v0

    return-object v0
.end method

.method public static buildCommand(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;I)Lcom/sec/android/app/bcocr/command/MenuCommand;
    .locals 1
    .param p0, "id"    # I
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;
    .param p2, "parentView"    # Landroid/view/ViewGroup;
    .param p3, "menuDepot"    # Lcom/sec/android/app/bcocr/MenuResourceDepot;
    .param p4, "zOrder"    # I

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 39
    .local v0, "command":Lcom/sec/android/app/bcocr/command/MenuCommand;
    packed-switch p0, :pswitch_data_0

    .line 47
    :goto_0
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0, p4}, Lcom/sec/android/app/bcocr/command/MenuCommand;->setZOrder(I)V

    .line 51
    :cond_0
    return-object v0

    .line 41
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;

    .end local v0    # "command":Lcom/sec/android/app/bcocr/command/MenuCommand;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/bcocr/command/LaunchZoomMenuCommand;-><init>(ILcom/sec/android/app/bcocr/AbstractOCRActivity;Landroid/view/ViewGroup;Lcom/sec/android/app/bcocr/MenuResourceDepot;)V

    .line 42
    .restart local v0    # "command":Lcom/sec/android/app/bcocr/command/MenuCommand;
    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
