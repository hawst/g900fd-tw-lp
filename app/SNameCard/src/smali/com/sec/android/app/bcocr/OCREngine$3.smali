.class Lcom/sec/android/app/bcocr/OCREngine$3;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCREngine;->doStartPreviewAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    .line 1184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1187
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "StartPreview**StartU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    const-string v1, "OCREngine"

    const-string v2, "starting preview..."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    :try_start_0
    const-string v1, "OCREngine"

    const-string v2, "mCameraDevice.startPreview()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera;->startPreview()V

    .line 1192
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-nez v1, :cond_0

    .line 1193
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 1194
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine;->access$3(Lcom/sec/android/app/bcocr/OCREngine;Z)V

    .line 1195
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/OCREngine$CameraPreviewCallback;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {v2, v3}, Lcom/sec/android/app/bcocr/OCREngine$CameraPreviewCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/seccamera/SecCamera;->setOneShotPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1212
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    .line 1217
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "StartPreview**EndU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Total-CameraPreviewLoading**EndU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Total-Shot2Shot**EndU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    :goto_1
    return-void

    .line 1197
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine;->access$3(Lcom/sec/android/app/bcocr/OCREngine;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1205
    :catch_0
    move-exception v0

    .line 1209
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$3;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    const/4 v2, -0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
