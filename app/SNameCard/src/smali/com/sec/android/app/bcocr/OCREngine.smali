.class public Lcom/sec/android/app/bcocr/OCREngine;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$CameraPreviewCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;,
        Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;,
        Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;,
        Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$MainHandler;,
        Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;,
        Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;,
        Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;,
        Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;,
        Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;,
        Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;,
        Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;
    }
.end annotation


# static fields
.field private static final ADVANCED_FOCUS_INITIAL_TIME:J = -0x1L

.field private static final ADVANCED_MACRO_FOCUS_COMMAND:I = 0x669

.field private static final ADVANCED_MACRO_FOCUS_OFF:I = 0x0

.field private static final ADVANCED_MACRO_FOCUS_ON:I = 0x1

.field protected static final AF_FAIL_SOUND:I = 0x2

.field protected static final AF_SUCCESS_SOUND:I = 0x1

.field protected static final CHANGE_STORAGE_TO_PHONE_MEMORY:I = 0x64

.field protected static final DEFAULT_IMEI_KTF:Ljava/lang/String; = "004400152020002"

.field protected static final DEFAULT_IMEI_SKT:Ljava/lang/String; = "357858010034783"

.field protected static final DELAY_TIME_TO_HIDE_FOCUS_RECT:I = 0x3e8

.field protected static final DELAY_TIME_TO_RESET_TOUCH_FOCUS:I = 0xbb8

.field public static final ERROR_BUFFER_OVERFLOW_FROM_RECORDER:I = -0x6

.field public static final ERROR_CAMCORDER_OPEN:I = -0x2

.field public static final ERROR_CAMERA_OPEN:I = -0x1

.field public static final ERROR_INVALID_FIRMWARE_VERSION:I = -0x7

.field public static final ERROR_MEDIA_SERVER_DIED:I = -0x8

.field public static final ERROR_RECORDING:I = -0x4

.field public static final ERROR_START_PREVIEW:I = -0x3

.field public static final ERROR_UNKOWN_CALLBACK_FROM_DEVICE:I = -0x5

.field public static final FOCUSING:I = 0x1

.field public static final FOCUS_CANCELLED:I = 0x4

.field public static final FOCUS_FAIL:I = 0x3

.field private static final FOCUS_LOCATION_CENTER:I = 0x0

.field private static final FOCUS_LOCATION_COMMAND:I = 0x66a

.field private static final FOCUS_LOCATION_UP:I = 0x1

.field public static final FOCUS_NOT_STARTED:I = 0x0

.field public static final FOCUS_RESTART:I = 0x5

.field public static final FOCUS_SUCCESS:I = 0x2

.field protected static final HIDE_FOCUS_RECT:I = 0x2

.field public static final OPEN_RETRY_NUMBER:I = 0x0

.field protected static final RESET_TOUCH_FOCUS:I = 0x1

.field public static final RES_AUTOFOCUS_CANCELED:I = 0x2

.field public static final RES_AUTOFOCUS_FAIL:I = 0x0

.field public static final RES_AUTOFOCUS_FOCUSING:I = 0x3

.field public static final RES_AUTOFOCUS_RESTART:I = 0x4

.field public static final RES_AUTOFOCUS_SUCCESS:I = 0x1

.field public static final SPECIAL_CHAR_EMAIL:I = 0x1

.field public static final SPECIAL_CHAR_NORMAL:I = 0x0

.field public static final SPECIAL_CHAR_PHONE:I = 0x3

.field public static final SPECIAL_CHAR_URL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "OCREngine"

.field protected static final URI_SEARCHING_ID:I = 0x0

.field protected static final URI_SEARCHING_INTERVAL:I = 0x1e

.field private static mContentResolver:Landroid/content/ContentResolver;

.field public static mImageBmp:Landroid/graphics/Bitmap;

.field public static mImgHeight:I

.field public static mImgWidth:I

.field public static mOrientDegree:I

.field public static mOrientForExif:I

.field private static m_bIsTouchAutoFocusing:Z

.field private static m_bIsTouchFocusPositioned:Z

.field public static m_bRestartTouchAF:Z

.field public static myImageData:[I


# instance fields
.field private DEBUG:Z

.field private bIsAeAwbLocked:Z

.field protected mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

.field private mAeLockSupported:Z

.field private mAutoFocusCallback:Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

.field private mAwbLockSupported:Z

.field private mCAFStartTime:J

.field private mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

.field private mCaptureInitiated:Z

.field protected mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

.field protected mDateTaken:J

.field protected mDisplayOrientation:I

.field public mEnableWaitingAnimation:Z

.field private mErrorCallback:Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;

.field protected mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

.field protected mFocusState:I

.field protected mFocusToneGenerator:Landroid/media/ToneGenerator;

.field protected mInformedAboutFirmwareVersion:Z

.field private mIsFinishOneShotPreviewCallback:Z

.field private mJpegPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

.field private mLandscapeActive:Z

.field protected mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

.field private mLastCapturedFileName:Ljava/lang/String;

.field private mLastCapturedTitle:Ljava/lang/String;

.field protected mLastContentUri:Landroid/net/Uri;

.field private mLastOrientation:I

.field protected mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

.field private mNumberOfPictureSavingThread:I

.field protected mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

.field protected mOldViewFinderHeight:I

.field protected mOldViewFinderWidth:I

.field protected mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

.field protected mOnFocusStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

.field protected mOnRecogStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

.field protected mOpenCameraThread:Ljava/lang/Thread;

.field protected mOrientationListener:Landroid/view/OrientationEventListener;

.field private mOrientationOnTake:I

.field protected mOriginalViewFinderHeight:I

.field protected mOriginalViewFinderWidth:I

.field private mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

.field private mPictureSavingThread:Ljava/lang/Thread;

.field public mPreviewData:[B

.field protected mPreviewHeight:I

.field protected mPreviewWidth:I

.field private mRawPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

.field protected mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

.field protected mRetry:I

.field private mSearchingLastContentUriThread:Ljava/lang/Thread;

.field private mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

.field private mShutterCallback:Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

.field protected mStartPreviewThread:Ljava/lang/Thread;

.field protected mStateDepot:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/bcocr/AbstractCeState;",
            ">;"
        }
    .end annotation
.end field

.field protected mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

.field protected mSurfaceHolder:Landroid/view/SurfaceHolder;

.field protected mSurfaceView:Landroid/view/SurfaceView;

.field private mTapArea:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Area;",
            ">;"
        }
    .end annotation
.end field

.field protected mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 145
    sput-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    .line 146
    sput-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchFocusPositioned:Z

    .line 147
    sput-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bRestartTouchAF:Z

    .line 151
    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mContentResolver:Landroid/content/ContentResolver;

    .line 171
    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    .line 175
    sput v0, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    .line 176
    sput v0, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    .line 178
    sput v0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    .line 179
    sput v0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientForExif:I

    .line 301
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V
    .locals 9
    .param p1, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .prologue
    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->DEBUG:Z

    .line 96
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 99
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 106
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    .line 107
    new-instance v0, Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/CeRequestQueue;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    .line 109
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mInformedAboutFirmwareVersion:Z

    .line 117
    iput v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 121
    iput v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    .line 135
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;

    .line 136
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSearchingLastContentUriThread:Ljava/lang/Thread;

    .line 137
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    .line 138
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    .line 139
    iput v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I

    .line 143
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mEnableWaitingAnimation:Z

    .line 149
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 153
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    .line 154
    iput-boolean v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLandscapeActive:Z

    .line 158
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mIsFinishOneShotPreviewCallback:Z

    .line 162
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCAFStartTime:J

    .line 190
    iput v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mDisplayOrientation:I

    .line 192
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewData:[B

    .line 198
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnFocusStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

    .line 209
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStateIdle;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-direct {v2, p0, v3, v6}, Lcom/sec/android/app/bcocr/CeStateIdle;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStateInitializing;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/bcocr/CeStateInitializing;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStateInitialized;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-direct {v2, p0, v3, v8}, Lcom/sec/android/app/bcocr/CeStateInitialized;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStateStartingPreview;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v4, 0x3

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/bcocr/CeStateStartingPreview;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStatePreviewing;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v4, 0x4

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/bcocr/CeStatePreviewing;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/bcocr/CeStateShutdown;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v4, 0x7

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/bcocr/CeStateShutdown;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/CeRequestQueue;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    .line 260
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    .line 273
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    .line 275
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCaptureInitiated:Z

    .line 278
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAutoFocusCallback:Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

    .line 279
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShutterCallback:Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

    .line 280
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRawPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

    .line 281
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorCallback:Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;

    .line 282
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mJpegPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

    .line 284
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 285
    iput v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastOrientation:I

    .line 286
    iput v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationOnTake:I

    .line 290
    iput-boolean v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z

    .line 292
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    .line 293
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;

    .line 294
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedTitle:Ljava/lang/String;

    .line 316
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    .line 2635
    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnRecogStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

    .line 355
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .line 356
    invoke-virtual {p1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const v1, 0x7f0f0012

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/bcocr/OCRSettings;->registerOCRSettingsChangedObserver(Lcom/sec/android/app/bcocr/OCRSettings$OnOCRSettingsChangedObserver;)V

    .line 359
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    .line 361
    invoke-virtual {p0, v6}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/OCREngine;->mContentResolver:Landroid/content/ContentResolver;

    .line 364
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->INSERT_MEDIA_DB_DIRECT:Z

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->access$4(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;Landroid/content/ContentResolver;)V

    .line 367
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/app/bcocr/OCREngine;Z)V
    .locals 0

    .prologue
    .line 275
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCaptureInitiated:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShutterCallback:Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRawPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mJpegPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 986
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCREngine;->openCameraDevice()V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/app/bcocr/OCREngine;I)V
    .locals 0

    .prologue
    .line 1999
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine;->setLastOrientation(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/OCREngine;)Z
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/bcocr/OCREngine;Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mIsFinishOneShotPreviewCallback:Z

    return-void
.end method

.method static synthetic access$4()Z
    .locals 1

    .prologue
    .line 145
    sget-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/bcocr/OCREngine;)I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/bcocr/OCREngine;I)V
    .locals 0

    .prologue
    .line 139
    iput p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedTitle:Ljava/lang/String;

    return-void
.end method

.method public static clamp(III)I
    .locals 0
    .param p0, "x"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 1864
    if-le p0, p2, :cond_0

    .line 1870
    .end local p2    # "max":I
    :goto_0
    return p2

    .line 1867
    .restart local p2    # "max":I
    :cond_0
    if-ge p0, p1, :cond_1

    move p2, p1

    .line 1868
    goto :goto_0

    :cond_1
    move p2, p0

    .line 1870
    goto :goto_0
.end method

.method private getCurrentStateId()I
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->getCurrentStateHandler()Lcom/sec/android/app/bcocr/AbstractCeState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v0

    return v0
.end method

.method private isPortraitMode()Z
    .locals 2

    .prologue
    .line 2664
    sget v0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    .line 2665
    const/4 v0, 0x1

    .line 2667
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openCameraDevice()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 987
    const-string v2, "OCREngine"

    const-string v3, "opening camera device..."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    const-string v2, "AXLOG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HW Open**StartU["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]**"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    const/4 v0, 0x0

    .line 994
    .local v0, "cameraId":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraId()I

    move-result v0

    .line 995
    const-string v2, "OCREngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "openCamera - Dual_OFF cameraId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    const-string v2, "OCREngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "openCamera - cameraId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-static {v2, v0}, Lcom/sec/android/app/bcocr/Util;->openCamera(Landroid/app/Activity;I)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 999
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1000
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    new-instance v3, Landroid/content/Intent;

    .line 1001
    const-string v4, "com.sec.android.app.bcocr.ACTION_START_BACK_CAMERA"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    :cond_0
    :goto_0
    iput v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    .line 1025
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v2, :cond_7

    .line 1026
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorCallback:Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->setErrorCallback(Lcom/sec/android/seccamera/SecCamera$ErrorCallback;)V

    .line 1027
    const-string v2, "OCREngine"

    const-string v3, "camera device is opened."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1029
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    .line 1031
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v2, :cond_5

    .line 1032
    const-string v2, "OCREngine"

    const-string v3, "CameraParameters is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;->sendEmptyMessage(I)Z

    .line 1046
    :cond_1
    :goto_1
    return-void

    .line 1003
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    new-instance v3, Landroid/content/Intent;

    .line 1004
    const-string v4, "com.sec.android.app.bcocr.ACTION_START_FRONT_CAMERA"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1003
    invoke-virtual {v2, v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1006
    :catch_0
    move-exception v1

    .line 1007
    .local v1, "e":Ljava/lang/Exception;
    iget v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    .line 1008
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_3

    iget v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    if-gez v2, :cond_3

    .line 1009
    const-string v2, "OCREngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "service cannot connect. retry "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCREngine;->openCameraDevice()V

    .line 1013
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_1

    .line 1015
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_4

    iget v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    if-ltz v2, :cond_4

    .line 1016
    const-string v2, "OCREngine"

    const-string v3, "service cannot connect. critical exception occured."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    iput v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    .line 1018
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1020
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRetry:I

    if-gez v2, :cond_0

    goto :goto_1

    .line 1036
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    sget-boolean v2, Lcom/sec/android/app/bcocr/Feature;->DUMP_CAMERA_PARAMETERS:Z

    if-eqz v2, :cond_6

    .line 1037
    const-string v2, "OCREngine"

    const-string v3, "doStartEngineAsync - Dump all parameters after getParameters()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->dump()V

    .line 1041
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->setCameraDisplayOrientation()V

    .line 1045
    :cond_7
    const-string v2, "AXLOG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HW Open**EndU["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]**"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public static roundOrientation(I)I
    .locals 1
    .param p0, "orientation"    # I

    .prologue
    .line 1996
    add-int/lit8 v0, p0, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method private setLastOrientation(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 2000
    iput p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastOrientation:I

    .line 2001
    return-void
.end method


# virtual methods
.method public IsWaitingAnimation()Z
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mEnableWaitingAnimation:Z

    return v0
.end method

.method protected calculateOrientationForPicture(I)I
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    .line 2016
    const/4 v1, 0x0

    .line 2017
    .local v1, "rotation":I
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 2018
    invoke-static {}, Lcom/sec/android/app/bcocr/CameraHolder;->instance()Lcom/sec/android/app/bcocr/CameraHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/CameraHolder;->getCameraInfo()[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraId()I

    move-result v3

    aget-object v0, v2, v3

    .line 2019
    .local v0, "info":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    iget v2, v0, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->orientation:I

    add-int/2addr v2, p1

    rem-int/lit16 v1, v2, 0x168

    .line 2022
    .end local v0    # "info":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    :cond_0
    return v1
.end method

.method public cancelAutoFocus()V
    .locals 2

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v0, :cond_1

    .line 888
    const-string v0, "OCREngine"

    const-string v1, "cancelAutoFocus - this cmd is skiped because mCameraDevice is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    :cond_0
    :goto_0
    return-void

    .line 892
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    if-eqz v0, :cond_2

    .line 893
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    new-instance v1, Lcom/sec/android/app/bcocr/OCREngine$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCREngine$1;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->post(Ljava/lang/Runnable;)Z

    .line 908
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    if-eqz v0, :cond_0

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public cancelFocusing()V
    .locals 1

    .prologue
    .line 1824
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 1825
    return-void
.end method

.method public changeEngineState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 456
    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/AbstractCeState;

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    if-eqz v0, :cond_0

    .line 464
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "changeEngineState => "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkFocusMode(I)I
    .locals 1
    .param p1, "focusMode"    # I

    .prologue
    .line 1517
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1518
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    if-eqz v0, :cond_1

    .line 1519
    const/4 p1, 0x1

    .line 1524
    :cond_0
    :goto_0
    return p1

    .line 1521
    :cond_1
    const/4 p1, 0x2

    goto :goto_0
.end method

.method public clearCaptureImageData()V
    .locals 1

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    if-eqz v0, :cond_0

    .line 1858
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CaptureData;->clear()V

    .line 1859
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 1861
    :cond_0
    return-void
.end method

.method public clearFocusState()V
    .locals 1

    .prologue
    .line 1820
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 1821
    return-void
.end method

.method public clearLastContentUri()V
    .locals 1

    .prologue
    .line 1849
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    .line 1850
    return-void
.end method

.method public clearRequest()V
    .locals 2

    .prologue
    .line 1095
    const-string v0, "OCREngine"

    const-string v1, "clearRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1099
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1100
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1103
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->removeMessages(I)V

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->clear()V

    .line 1109
    return-void
.end method

.method public closeCamera()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 479
    const-string v0, "OCREngine"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v2}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 481
    invoke-static {}, Lcom/sec/android/app/bcocr/CameraHolder;->instance()Lcom/sec/android/app/bcocr/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CameraHolder;->release()V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v2}, Lcom/sec/android/seccamera/SecCamera;->setZoomChangeListener(Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v2}, Lcom/sec/android/seccamera/SecCamera;->setFaceDetectionListener(Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v2}, Lcom/sec/android/seccamera/SecCamera;->setErrorCallback(Lcom/sec/android/seccamera/SecCamera$ErrorCallback;)V

    .line 485
    iput-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 487
    :cond_0
    sput-object v2, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 488
    return-void
.end method

.method public convertExifOrientationToMediaOrientation(I)I
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 2040
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 2041
    const/16 v0, 0x5a

    .line 2047
    :goto_0
    return v0

    .line 2042
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 2043
    const/16 v0, 0xb4

    goto :goto_0

    .line 2044
    :cond_1
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 2045
    const/16 v0, 0x10e

    goto :goto_0

    .line 2047
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public convertToExifInterfaceOrientation(I)I
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    const/4 v0, 0x1

    .line 2026
    const/16 v1, 0x5a

    if-ne p1, v1, :cond_1

    .line 2027
    const/4 v0, 0x6

    .line 2036
    :cond_0
    :goto_0
    return v0

    .line 2028
    :cond_1
    const/16 v1, 0xb4

    if-ne p1, v1, :cond_2

    .line 2029
    const/4 v0, 0x3

    goto :goto_0

    .line 2030
    :cond_2
    const/16 v1, 0x10e

    if-ne p1, v1, :cond_3

    .line 2031
    const/16 v0, 0x8

    goto :goto_0

    .line 2032
    :cond_3
    const/16 v1, 0x168

    if-eq p1, v1, :cond_0

    if-nez p1, :cond_0

    goto :goto_0
.end method

.method public countForCurrenPictureSaving()I
    .locals 1

    .prologue
    .line 2062
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mNumberOfPictureSavingThread:I

    return v0
.end method

.method public doAutoFocusAsync()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x5

    .line 844
    const-string v0, "OCREngine"

    const-string v1, "doAutoFocusAsync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-Autofocus**StartU["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    iput v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    .line 849
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->removeMessages(I)V

    .line 853
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v0, :cond_1

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 856
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    if-eqz v0, :cond_3

    .line 857
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->showFocusIndicator(I)V

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_2

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getTouchAutoFocusActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAutoFocusCallback:Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->autoFocus(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V

    .line 884
    :cond_2
    return-void

    .line 859
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 862
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getTouchAutoFocusActive()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 863
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    if-eqz v0, :cond_5

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 866
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 870
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    goto :goto_0
.end method

.method public doChangeParameterSync(II)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 1354
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->doChangeParameterSync(Ljava/lang/Object;)V

    .line 1355
    return-void
.end method

.method public doChangeParameterSync(Ljava/lang/Object;)V
    .locals 7
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    .line 1358
    const-string v3, "OCREngine"

    const-string v4, "doChangeParameterSync"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v3, :cond_1

    .line 1361
    const-string v3, "OCREngine"

    const-string v4, "returning because mCameraDevice is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1430
    :cond_0
    :goto_0
    return-void

    .line 1365
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v3, :cond_2

    .line 1366
    const-string v3, "OCREngine"

    const-string v4, "mParameters is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 1370
    check-cast v1, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;

    .line 1371
    .local v1, "p":Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;
    const/4 v0, 0x0

    .line 1373
    .local v0, "fpsRange":[I
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1416
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchDuplicateRequest(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1417
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->DUMP_CAMERA_PARAMETERS:Z

    if-eqz v3, :cond_3

    .line 1418
    const-string v3, "OCREngine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "doChangeParameterSync() - key: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1419
    const-string v5, "- Dump all parameters before setParameters() call"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1418
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->dump()V

    .line 1422
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v3, v4}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1427
    :goto_2
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 1428
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->setShootingMode(I)V

    goto :goto_0

    .line 1377
    :sswitch_0
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 1378
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    sget v4, Lcom/sec/android/app/bcocr/Feature;->CAMERA_PREVIEW_FPS_MIN:I

    sget v5, Lcom/sec/android/app/bcocr/Feature;->CAMERA_PREVIEW_FPS_MIN:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewFpsRange(II)V

    .line 1388
    :cond_4
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getOCRSettings()Lcom/sec/android/app/bcocr/OCRSettings;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingModeValueForISPset(I)I

    move-result v2

    .line 1389
    .local v2, "shotmode":I
    const-string v3, "OCREngine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Shot mode : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1390
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1380
    .end local v2    # "shotmode":I
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    sget v4, Lcom/sec/android/app/bcocr/Feature;->CAMERA_PREVIEW_FPS_MIN:I

    .line 1381
    sget v5, Lcom/sec/android/app/bcocr/Feature;->CAMERA_PREVIEW_FPS_MAX:I

    .line 1380
    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->findBestFpsRange(Lcom/sec/android/seccamera/SecCamera$Parameters;II)[I

    move-result-object v0

    .line 1382
    if-eqz v0, :cond_4

    .line 1383
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v4, 0x0

    aget v4, v0, v4

    .line 1384
    aget v5, v0, v6

    .line 1383
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewFpsRange(II)V

    goto :goto_3

    .line 1393
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    .line 1394
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionString(I)Ljava/lang/String;

    move-result-object v5

    .line 1393
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1397
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    .line 1398
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getFlashModeString(I)Ljava/lang/String;

    move-result-object v5

    .line 1397
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1401
    :sswitch_3
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v3, :cond_6

    .line 1402
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAutoFocusCallback:Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

    invoke-virtual {v3, v4}, Lcom/sec/android/seccamera/SecCamera;->setAutoFocusCb(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V

    .line 1403
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    .line 1404
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCREngine;->checkFocusMode(I)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v5

    .line 1403
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    goto/16 :goto_1

    .line 1407
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    .line 1408
    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v5

    .line 1407
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1412
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getKey()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;->getValue()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1424
    :cond_7
    const-string v3, "OCREngine"

    const-string v4, "parameter will set next operation coming"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1373
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x5 -> :sswitch_3
        0x12 -> :sswitch_4
    .end sparse-switch
.end method

.method public doProcessBackSync()V
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->processBack()V

    .line 1329
    return-void
.end method

.method public doSetAllParamsSync()V
    .locals 2

    .prologue
    .line 922
    const-string v0, "OCREngine"

    const-string v1, "doSetAllParamsSync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->initialize()V

    .line 924
    return-void
.end method

.method public doSetParametersSync(Ljava/lang/Object;)V
    .locals 4
    .param p1, "param"    # Ljava/lang/Object;

    .prologue
    .line 1338
    const-string v1, "OCREngine"

    const-string v2, "doSetParametersSync"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1341
    check-cast v0, Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;

    .line 1342
    .local v0, "p":Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v1, v2}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1345
    .end local v0    # "p":Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;
    :cond_0
    return-void
.end method

.method public doStartEngineAsync()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1049
    const-string v0, "OCREngine"

    const-string v1, "doStartEngineAsync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    iput-boolean v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCaptureInitiated:Z

    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    if-nez v0, :cond_0

    .line 1055
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    .line 1058
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1060
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_1

    .line 1061
    const-string v0, "OCREngine"

    const-string v1, "camera device is already conntected for some reason (eg. onResume is called twice without calling onPause)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    const-string v0, "OCREngine"

    const-string v1, "skip connecting"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    .line 1080
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->setOrientationListener()V

    .line 1082
    return-void

    .line 1065
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_START_ENGINE_SYNC:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isNeedToStartEngineSync()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1066
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCREngine;->openCameraDevice()V

    .line 1067
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    goto :goto_0

    .line 1069
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/bcocr/OCREngine$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCREngine$2;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    const-string v1, "openCameraThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1077
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public doStartPreviewAsync()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 1140
    const-string v5, "OCREngine"

    const-string v6, "doStartPreviewAsync"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1145
    .local v2, "_axtime_st_1":J
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v5, :cond_1

    .line 1146
    const-string v5, "OCREngine"

    const-string v6, "return because mSurfaceHolder is null."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    if-eqz v5, :cond_0

    .line 1149
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/bcocr/CeRequestQueue;->removeRequest(I)V

    .line 1237
    :cond_0
    :goto_0
    return-void

    .line 1156
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v5, :cond_2

    .line 1157
    const-string v5, "OCREngine"

    const-string v6, "return because mCameraDevice is null."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1160
    :cond_2
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_DIS:Z

    if-eqz v5, :cond_3

    .line 1161
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_3

    .line 1162
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->turnOnDISMode()V

    .line 1164
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->resetPreviewSize()V

    .line 1167
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v5, v6}, Lcom/sec/android/seccamera/SecCamera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1176
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v5, :cond_4

    .line 1177
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    const/16 v6, 0x669

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 1179
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->replaceFocusPosition()V

    .line 1183
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v5, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/OCR;->setPreviewButtonEnable()V

    .line 1184
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/bcocr/OCREngine$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/bcocr/OCREngine$3;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    .line 1224
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    const-string v6, "startPreviewThread"

    invoke-virtual {v5, v6}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1225
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 1226
    invoke-virtual {p0, v9}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1228
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v5, :cond_5

    .line 1229
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v5}, Landroid/view/OrientationEventListener;->enable()V

    .line 1234
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1235
    .local v0, "_axtime_ed_1":J
    const-string v5, "AXLOG"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PrepareStartPreview-End**End["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]**["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v0, v2

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]**[]**"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1236
    move-wide v2, v0

    .line 1237
    goto/16 :goto_0

    .line 1168
    .end local v0    # "_axtime_ed_1":J
    :catch_0
    move-exception v4

    .line 1169
    .local v4, "exception":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->closeCamera()V

    .line 1172
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v5}, Lcom/sec/android/app/bcocr/CeRequestQueue;->completeRequest()V

    .line 1173
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mErrorMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;

    const/4 v6, -0x3

    invoke-virtual {v5, v6}, Lcom/sec/android/app/bcocr/OCREngine$ErrorMessageHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1231
    .end local v4    # "exception":Ljava/io/IOException;
    :cond_5
    const-string v5, "OCREngine"

    const-string v6, "doStartPreviewAsync : mOrientationListener is null"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public doStartPreviewDummySync()V
    .locals 2

    .prologue
    .line 1284
    const-string v0, "OCREngine"

    const-string v1, "doStartPreviewDummySync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1287
    return-void
.end method

.method public doStopEngineSync()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1118
    const-string v0, "OCREngine"

    const-string v1, "doStopEngineSync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    if-eqz v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;->removeMessages(I)V

    .line 1122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    .line 1124
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    if-eqz v0, :cond_1

    .line 1125
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->stopTouchAutoFocus()V

    .line 1128
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->closeCamera()V

    .line 1130
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1131
    return-void
.end method

.method public doStopPreviewDummySync()V
    .locals 2

    .prologue
    .line 1267
    const-string v0, "OCREngine"

    const-string v1, "doStopPreviewDummySync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1276
    :goto_0
    return-void

    .line 1274
    :cond_0
    const-string v0, "OCREngine"

    const-string v1, "doStopPreviewSync : mOrientationListener is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public doStopPreviewSync()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1290
    const-string v4, "OCREngine"

    const-string v5, "doStopPreviewSync"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1294
    .local v2, "_axtime_st_1":J
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    .line 1295
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v5, 0x3

    invoke-static {v5}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, Lcom/sec/android/app/bcocr/OCRSettings;->getFlashModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_0

    .line 1297
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v4, v5}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1301
    :cond_0
    sget-boolean v4, Lcom/sec/android/app/bcocr/Feature;->ADVANCED_FOCUS_DIS:Z

    if-eqz v4, :cond_1

    .line 1302
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->turnOffDISMode()V

    .line 1304
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_2

    .line 1305
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    const/16 v5, 0x669

    invoke-virtual {v4, v5, v7, v7}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 1306
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->stopPreview()V

    .line 1309
    :cond_2
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/bcocr/OCREngine;->changeEngineState(I)V

    .line 1311
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v4, :cond_3

    .line 1312
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v4}, Landroid/view/OrientationEventListener;->disable()V

    .line 1316
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1317
    .local v0, "_axtime_ed_1":J
    const-string v4, "AXLOG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "StopPreview**End["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]**["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v0, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]**[]**"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1318
    move-wide v2, v0

    .line 1319
    return-void

    .line 1314
    .end local v0    # "_axtime_ed_1":J
    :cond_3
    const-string v4, "OCREngine"

    const-string v5, "doStopPreviewSync : mOrientationListener is null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public doTakePictureAsync()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 946
    const-string v0, "CameraPerformance"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[AutoCapture][Shot2Shot] - TakePicture start::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shot2Shot-TakePicture**StartU : ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->removeMessages(I)V

    .line 952
    :cond_0
    sget v0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->setLastOrientation(I)V

    .line 953
    sget v0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->setOrientationOnTake(I)V

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setRotation(I)V

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getFlashModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_2

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->setPreviewButtonDisable()V

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 964
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShutterCallback:Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRawPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mJpegPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/seccamera/SecCamera;->takePicture(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V

    .line 968
    :cond_2
    return-void
.end method

.method public final doWaitAsync(I)V
    .locals 4
    .param p1, "milisec"    # I

    .prologue
    .line 977
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/16 v1, 0x8

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 978
    return-void
.end method

.method protected findBestFpsRange(Lcom/sec/android/seccamera/SecCamera$Parameters;II)[I
    .locals 10
    .param p1, "parameters"    # Lcom/sec/android/seccamera/SecCamera$Parameters;
    .param p2, "requestedMinFps"    # I
    .param p3, "requestedMaxFps"    # I

    .prologue
    .line 1433
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Requsted fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1435
    const/4 v1, 0x0

    .line 1436
    .local v1, "MIN_IDX":I
    const/4 v0, 0x1

    .line 1437
    .local v0, "MAX_IDX":I
    const/4 v7, 0x2

    new-array v3, v7, [I

    .line 1438
    .local v3, "fpsRange":[I
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 1440
    .local v2, "bestFpsRange":[I
    invoke-virtual {p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v4

    .line 1442
    .local v4, "fpsRangeList":Ljava/util/List;, "Ljava/util/List<[I>;"
    if-nez v4, :cond_0

    .line 1443
    const-string v7, "OCREngine"

    const-string v8, "supported preview fps range is null"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1444
    const/4 v3, 0x0

    .line 1495
    .end local v3    # "fpsRange":[I
    :goto_0
    return-object v3

    .line 1447
    .restart local v3    # "fpsRange":[I
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v5, v7, -0x1

    .local v5, "i":I
    :goto_1
    if-gez v5, :cond_1

    .line 1493
    move-object v2, v3

    .line 1494
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1448
    :cond_1
    const/4 v8, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 1449
    const/4 v8, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 1451
    const/4 v7, 0x1

    aget v7, v3, v7

    if-ne p3, v7, :cond_7

    .line 1452
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_2

    .line 1454
    move-object v2, v3

    .line 1455
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1457
    :cond_2
    if-nez v5, :cond_3

    .line 1459
    move-object v2, v3

    .line 1460
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1464
    :cond_3
    move v6, v5

    .local v6, "j":I
    :goto_2
    if-gez v6, :cond_4

    .line 1481
    move-object v2, v3

    .line 1482
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1465
    :cond_4
    const/4 v8, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 1466
    const/4 v8, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 1468
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_5

    .line 1470
    move-object v2, v3

    .line 1471
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1473
    :cond_5
    const/4 v7, 0x0

    aget v7, v3, v7

    if-le p2, v7, :cond_6

    .line 1474
    move-object v2, v3

    .line 1475
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1464
    :cond_6
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_2

    .line 1485
    .end local v6    # "j":I
    :cond_7
    const/4 v7, 0x1

    aget v7, v3, v7

    if-le p3, v7, :cond_8

    .line 1486
    move-object v2, v3

    .line 1487
    const-string v7, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "find best fps range : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1447
    :cond_8
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_1
.end method

.method public findOptimalPreviewSize(Ljava/util/List;D)Lcom/sec/android/seccamera/SecCamera$Size;
    .locals 18
    .param p2, "targetRatio"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Size;",
            ">;D)",
            "Lcom/sec/android/seccamera/SecCamera$Size;"
        }
    .end annotation

    .prologue
    .line 1662
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1663
    .local v2, "ASPECT_TOLERANCE":D
    if-nez p1, :cond_1

    .line 1664
    const/4 v5, 0x0

    .line 1713
    :cond_0
    return-object v5

    .line 1667
    :cond_1
    const/4 v5, 0x0

    .line 1668
    .local v5, "optimalSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1669
    .local v6, "minDiff":D
    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    .line 1677
    .local v8, "outSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 1678
    .local v4, "display":Landroid/view/Display;
    invoke-virtual {v4, v8}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1679
    iget v13, v8, Landroid/graphics/Point;->y:I

    iget v14, v8, Landroid/graphics/Point;->x:I

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 1681
    .local v12, "targetHeight":I
    const-string v13, "OCREngine"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "display.getHeight() = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v15, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " display.getWidth() = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1682
    iget v15, v8, Landroid/graphics/Point;->x:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1681
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    if-gtz v12, :cond_2

    .line 1686
    iget v12, v8, Landroid/graphics/Point;->y:I

    .line 1690
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_5

    .line 1703
    if-nez v5, :cond_0

    .line 1704
    const-string v13, "OCREngine"

    const-string v14, "No preview size match the aspect ratio"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1706
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_4
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1707
    .local v9, "size":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v14, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    sub-int/2addr v14, v12

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-double v14, v14

    cmpg-double v14, v14, v6

    if-gez v14, :cond_4

    .line 1708
    move-object v5, v9

    .line 1709
    iget v14, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    sub-int/2addr v14, v12

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-double v6, v14

    goto :goto_1

    .line 1690
    .end local v9    # "size":Lcom/sec/android/seccamera/SecCamera$Size;
    :cond_5
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1691
    .restart local v9    # "size":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v14, v9, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    int-to-double v14, v14

    iget v0, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v10, v14, v16

    .line 1692
    .local v10, "ratio":D
    sub-double v14, v10, p2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v14, v14, v16

    if-gtz v14, :cond_3

    .line 1695
    iget v14, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    sub-int/2addr v14, v12

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-double v14, v14

    cmpg-double v14, v14, v6

    if-gez v14, :cond_3

    .line 1696
    move-object v5, v9

    .line 1697
    iget v14, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    sub-int/2addr v14, v12

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-double v6, v14

    goto :goto_0
.end method

.method public findThumbnailSize(Ljava/util/List;II)Lcom/sec/android/seccamera/SecCamera$Size;
    .locals 16
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Size;",
            ">;II)",
            "Lcom/sec/android/seccamera/SecCamera$Size;"
        }
    .end annotation

    .prologue
    .line 1599
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    if-nez p1, :cond_0

    .line 1600
    const/4 v9, 0x0

    .line 1621
    :goto_0
    return-object v9

    .line 1603
    :cond_0
    const/4 v9, 0x0

    .line 1604
    .local v9, "thumbnailSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    .line 1605
    .local v4, "minRatioDiff":D
    const v3, 0x186a0

    .line 1606
    .local v3, "minGap":I
    const/4 v2, 0x0

    .line 1608
    .local v2, "index":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1619
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "thumbnailSize":Lcom/sec/android/seccamera/SecCamera$Size;
    check-cast v9, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1620
    .restart local v9    # "thumbnailSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-string v11, "OCREngine"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "setJPEGThumbnailSize: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, v9, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v9, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1608
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1609
    .local v8, "size":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v12, v8, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    int-to-double v12, v12

    iget v14, v8, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    int-to-double v14, v14

    div-double v6, v12, v14

    .line 1610
    .local v6, "ratio":D
    move/from16 v0, p2

    int-to-double v12, v0

    move/from16 v0, p3

    int-to-double v14, v0

    div-double/2addr v12, v14

    sub-double v12, v6, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    cmpl-double v12, v12, v4

    if-gtz v12, :cond_1

    .line 1613
    iget v12, v8, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    sub-int v12, v12, p2

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .line 1614
    .local v10, "widthGap":I
    if-le v3, v10, :cond_1

    .line 1615
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1616
    move v3, v10

    goto :goto_1
.end method

.method public findePreviewSize(Ljava/util/List;II)Lcom/sec/android/seccamera/SecCamera$Size;
    .locals 16
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Size;",
            ">;II)",
            "Lcom/sec/android/seccamera/SecCamera$Size;"
        }
    .end annotation

    .prologue
    .line 1625
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    if-nez p1, :cond_0

    .line 1626
    const/4 v6, 0x0

    .line 1657
    :goto_0
    return-object v6

    .line 1629
    :cond_0
    const/4 v6, 0x0

    .line 1630
    .local v6, "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    .line 1631
    .local v4, "minRatioDiff":D
    const v3, 0x186a0

    .line 1632
    .local v3, "minGap":I
    const/4 v2, 0x0

    .line 1634
    .local v2, "index":I
    const-string v11, "OCREngine"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "setPreviewSize: width, height"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " , "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1651
    const/16 v11, 0x500

    move/from16 v0, p2

    if-ne v0, v11, :cond_2

    const/16 v11, 0x300

    move/from16 v0, p3

    if-ne v0, v11, :cond_2

    .line 1652
    const/4 v2, 0x2

    .line 1655
    :cond_2
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    check-cast v6, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1656
    .restart local v6    # "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-string v11, "OCREngine"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "setPreviewSize: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, v6, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1636
    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 1637
    .local v7, "size":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v12, v7, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    int-to-double v12, v12

    iget v14, v7, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    int-to-double v14, v14

    div-double v8, v12, v14

    .line 1638
    .local v8, "ratio":D
    const-string v12, "OCREngine"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setPreviewSize: size.width, size.height / ratio"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, v7, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v7, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " / "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    move/from16 v0, p2

    int-to-double v12, v0

    move/from16 v0, p3

    int-to-double v14, v0

    div-double/2addr v12, v14

    sub-double v12, v8, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    cmpl-double v12, v12, v4

    if-gtz v12, :cond_1

    .line 1642
    iget v12, v7, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    sub-int v12, v12, p2

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .line 1643
    .local v10, "widthGap":I
    if-le v3, v10, :cond_1

    .line 1644
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1645
    const-string v12, "OCREngine"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setPreviewSize: index : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    move v3, v10

    goto/16 :goto_1
.end method

.method public getCAFStartTime()J
    .locals 2

    .prologue
    .line 707
    iget-wide v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCAFStartTime:J

    return-wide v0
.end method

.method public getCameraDevice()Lcom/sec/android/seccamera/SecCamera;
    .locals 1

    .prologue
    .line 2652
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    return-object v0
.end method

.method public getCurrentStateHandler()Lcom/sec/android/app/bcocr/AbstractCeState;
    .locals 1

    .prologue
    .line 1756
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    return-object v0
.end method

.method public getDisplayOrientation()I
    .locals 1

    .prologue
    .line 2604
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mDisplayOrientation:I

    return v0
.end method

.method public getFocusState()I
    .locals 1

    .prologue
    .line 1828
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    return v0
.end method

.method public getLandscapeActive()Z
    .locals 1

    .prologue
    .line 1973
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLandscapeActive:Z

    return v0
.end method

.method public getLastCaptureData()Lcom/sec/android/app/bcocr/CaptureData;
    .locals 1

    .prologue
    .line 1853
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    return-object v0
.end method

.method public getLastCapturedFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected getLastDateTaken()J
    .locals 2

    .prologue
    .line 1752
    iget-wide v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mDateTaken:J

    return-wide v0
.end method

.method public getLastOrientation()I
    .locals 1

    .prologue
    .line 2004
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastOrientation:I

    return v0
.end method

.method public getOrientationOnTake()I
    .locals 1

    .prologue
    .line 2012
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationOnTake:I

    return v0
.end method

.method public getPreviewHeight()I
    .locals 1

    .prologue
    .line 2612
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    return v0
.end method

.method public getPreviewWidth()I
    .locals 1

    .prologue
    .line 2608
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    return v0
.end method

.method public getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;
    .locals 1

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    return-object v0
.end method

.method public getSurfaceView()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 2624
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method public getTouchFocusPositioned()Z
    .locals 1

    .prologue
    .line 1916
    sget-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchFocusPositioned:Z

    return v0
.end method

.method public handleShutterEvent()V
    .locals 2

    .prologue
    .line 1528
    const-string v0, "OCREngine"

    const-string v1, "handleShuttreEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->handleShutterEvent()V

    .line 1530
    return-void
.end method

.method public handleShutterReleaseEvent()V
    .locals 2

    .prologue
    .line 1533
    const-string v0, "OCREngine"

    const-string v1, "handleShutterReleaseEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->handleShutterReleaseEvent()V

    .line 1535
    return-void
.end method

.method public imageStoringCompleted()V
    .locals 1

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->onImageStoringCompleted()V

    .line 1539
    return-void
.end method

.method public initialize()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v4, 0x1

    const/4 v8, 0x5

    const/4 v3, 0x0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    .line 397
    .local v0, "camSettings":Lcom/sec/android/app/bcocr/OCRSettings;
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 399
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v5, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->isBackCamera()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 404
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/16 v6, 0x12

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getZoomValue()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 406
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FLASH:Z

    if-eqz v5, :cond_5

    .line 407
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFlashMode()I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/bcocr/OCRSettings;->getFlashModeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v6, 0x4

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v5, :cond_6

    .line 414
    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/bcocr/OCREngine;->checkFocusMode(I)I

    move-result v1

    .line 415
    .local v1, "focus":I
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAutoFocusCallback:Lcom/sec/android/app/bcocr/OCREngine$AutoFocusCallback;

    invoke-virtual {v5, v6}, Lcom/sec/android/seccamera/SecCamera;->setAutoFocusCb(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V

    .line 417
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    .line 424
    .end local v1    # "focus":I
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/16 v6, 0xa

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getIsoString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/16 v6, 0x10

    invoke-static {v6}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/bcocr/OCRSettings;->getQualityValue()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v6, "video_recording_gamma"

    const-string v7, "off"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v6, "slow_ae"

    const-string v7, "off"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v5, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setVideoStabilization(Z)V

    .line 431
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v6, "sw-vdis"

    const-string v7, "off"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getAntiBanding()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setAntibanding(Ljava/lang/String;)V

    .line 436
    :cond_2
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->DUMP_CAMERA_PARAMETERS:Z

    if-eqz v5, :cond_3

    .line 437
    const-string v5, "OCREngine"

    const-string v6, "initialize() - Dump all parameters before setParameters() call"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->dump()V

    .line 441
    :cond_3
    const-string v5, "true"

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v7, "auto-exposure-lock-supported"

    invoke-virtual {v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAeLockSupported:Z

    .line 442
    const-string v5, "true"

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v7, "auto-whitebalance-lock-supported"

    invoke-virtual {v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAwbLockSupported:Z

    .line 444
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->setShootingMode(I)V

    .line 445
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v5, v6}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 447
    const-string v5, "357858010034783"

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "004400152020002"

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 448
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v5, v4}, Lcom/sec/android/seccamera/SecCamera;->setDefaultIMEI(I)V

    .line 452
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isCalling()Z

    move-result v6

    if-eqz v6, :cond_9

    :goto_4
    invoke-virtual {v5, v3}, Lcom/sec/android/seccamera/SecCamera;->setShutterSoundEnable(Z)V

    goto/16 :goto_0

    .line 409
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getFlashModeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 418
    :cond_6
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-eqz v5, :cond_7

    .line 419
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 421
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 450
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v5, v3}, Lcom/sec/android/seccamera/SecCamera;->setDefaultIMEI(I)V

    goto :goto_3

    :cond_9
    move v3, v4

    .line 452
    goto :goto_4
.end method

.method public isAutoFocusing()Z
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    const/4 v0, 0x1

    .line 751
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCapturing()Z
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 758
    :cond_0
    const/4 v0, 0x1

    .line 760
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCurrentState(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 800
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCREngine;->getCurrentStateId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 801
    const/4 v0, 0x1

    .line 803
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFinishOneShotPreviewCallback()Z
    .locals 1

    .prologue
    .line 2616
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mIsFinishOneShotPreviewCallback:Z

    return v0
.end method

.method public isMediaScannerScanning(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 2522
    const/4 v7, 0x0

    .line 2523
    .local v7, "result":Z
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2524
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v1

    .line 2525
    new-array v2, v10, [Ljava/lang/String;

    const-string v4, "volume"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    .line 2524
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2526
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 2527
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v10, :cond_1

    .line 2528
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2529
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2530
    .local v8, "volumeName":Ljava/lang/String;
    const-string v1, "external"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "internal"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2531
    :cond_0
    const/4 v7, 0x1

    .line 2534
    .end local v8    # "volumeName":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2536
    :cond_2
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MediaScanning..."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2537
    return v7
.end method

.method public isOnResumePending()Z
    .locals 1

    .prologue
    .line 2620
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->mOnResumePending:Z

    return v0
.end method

.method public isParamChanging()Z
    .locals 2

    .prologue
    .line 2656
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->getRequestQueue()Lcom/sec/android/app/bcocr/CeRequestQueue;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2657
    const/4 v0, 0x1

    .line 2659
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrepareRecording()Z
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    :cond_0
    const/4 v0, 0x1

    .line 474
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviewStarted()Z
    .locals 1

    .prologue
    .line 783
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    const/4 v0, 0x1

    .line 787
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStartingEngine()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 764
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 767
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isStartingPreview()Z
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 771
    invoke-virtual {p0, v2}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 779
    :cond_0
    :goto_0
    return v0

    .line 775
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 779
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStopPreviewPending()Z
    .locals 2

    .prologue
    .line 791
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequestQueue;->firstElement()Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/CeRequest;->getRequest()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 793
    const/4 v0, 0x1

    .line 796
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTouchAutoFocusing()Z
    .locals 1

    .prologue
    .line 1948
    sget-boolean v0, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    return v0
.end method

.method public lockAEAWB()V
    .locals 2

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v0, :cond_1

    .line 2711
    :cond_0
    :goto_0
    return-void

    .line 2709
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->setAEAWBLockParameter(Z)V

    .line 2710
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto :goto_0
.end method

.method public onOCRSettingsChanged(II)V
    .locals 3
    .param p1, "menuid"    # I
    .param p2, "modeid"    # I

    .prologue
    .line 1765
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onOCRSettingsChanged menuid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " modeid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1767
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768
    const-string v0, "OCREngine"

    const-string v1, "capture in progress, setparameters are not allowed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1811
    :goto_0
    return-void

    .line 1772
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1806
    :goto_1
    :sswitch_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1808
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    iput p2, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mStorage:I

    goto :goto_0

    .line 1776
    :sswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine;->doChangeParameterSync(II)V

    goto :goto_1

    .line 1779
    :sswitch_2
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->OCR_CAMERA_RESIZE_PREVIEW_SUPPORT:Z

    if-eqz v0, :cond_1

    .line 1780
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->resizePreviewAspectRatio()V

    .line 1782
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine;->doChangeParameterSync(II)V

    goto :goto_1

    .line 1787
    :sswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleChangeParameter(II)V

    goto :goto_1

    .line 1772
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_1
        0xa -> :sswitch_3
        0x10 -> :sswitch_3
        0x12 -> :sswitch_3
        0x16 -> :sswitch_0
    .end sparse-switch

    .line 1806
    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 382
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->removeMessages(I)V

    .line 386
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v1, :cond_2

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 388
    .local v0, "holder":Landroid/view/SurfaceHolder;
    if-eqz v0, :cond_1

    .line 389
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 391
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 393
    .end local v0    # "holder":Landroid/view/SurfaceHolder;
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 370
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 372
    .local v0, "holder":Landroid/view/SurfaceHolder;
    const-string v1, "AXLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "surfaceCreate**StartU["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    if-eqz v0, :cond_0

    .line 374
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 375
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 377
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 379
    .end local v0    # "holder":Landroid/view/SurfaceHolder;
    :cond_1
    return-void
.end method

.method public replaceFocusPosition()V
    .locals 5

    .prologue
    const/16 v4, 0x66a

    const/4 v3, 0x0

    .line 2672
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "replaceFocusPosition="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureMode()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2673
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 2674
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->isCaptureMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isPortraitMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2675
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1, v3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 2680
    :cond_0
    :goto_0
    return-void

    .line 2677
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v4, v3, v3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method protected resetPreviewSize()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1542
    const-string v4, "OCREngine"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "resetPreviewSize()- WH: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v4, :cond_8

    .line 1545
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v1

    .line 1546
    .local v1, "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    if-nez v1, :cond_0

    .line 1547
    const-string v4, "OCREngine"

    const-string v5, "supported preview size is null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    .end local v1    # "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    :goto_0
    return-void

    .line 1550
    .restart local v1    # "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setRecordingHint(Z)V

    .line 1551
    const/4 v0, 0x0

    .line 1552
    .local v0, "previewsize":Lcom/sec/android/seccamera/SecCamera$Size;
    sget-boolean v4, Lcom/sec/android/app/bcocr/Feature;->ENABLE_ENHANCE_SWITCHING_TIME:Z

    if-eqz v4, :cond_4

    .line 1553
    iget v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    if-le v4, v5, :cond_3

    .line 1554
    iget v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    invoke-virtual {p0, v1, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->findePreviewSize(Ljava/util/List;II)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v0

    .line 1562
    :goto_1
    if-eqz v0, :cond_1

    .line 1563
    iget v4, v0, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iput v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    .line 1564
    iget v4, v0, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    iput v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    .line 1567
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v4}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 1568
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    sget-object v5, Lcom/sec/android/app/bcocr/Feature;->PANORAMA_RESOLUTION:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v5

    .line 1569
    sget-object v6, Lcom/sec/android/app/bcocr/Feature;->PANORAMA_RESOLUTION:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getResolutionID(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v6

    .line 1568
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    .line 1575
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedJpegThumbnailSizes()Ljava/util/List;

    move-result-object v3

    .line 1576
    .local v3, "thumbnailSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v7, :cond_2

    .line 1577
    const/4 v2, 0x0

    .line 1578
    .local v2, "thumbnaiSize":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->findThumbnailSize(Ljava/util/List;II)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v2

    .line 1579
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    iget v5, v2, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v6, v2, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setJpegThumbnailSize(II)V

    .line 1582
    .end local v2    # "thumbnaiSize":Lcom/sec/android/seccamera/SecCamera$Size;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_7

    .line 1583
    invoke-virtual {p0, v7}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1584
    const-string v4, "OCREngine"

    const-string v5, "resetPreviewSize()- isCurrentState is INITIALIZING"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1556
    .end local v3    # "thumbnailSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    :cond_3
    iget v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    invoke-virtual {p0, v1, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->findePreviewSize(Ljava/util/List;II)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v0

    .line 1558
    goto :goto_1

    .line 1559
    :cond_4
    sget-wide v4, Lcom/sec/android/app/bcocr/Feature;->WIDE_SCREEN_RATIO:D

    invoke-virtual {p0, v1, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine;->findOptimalPreviewSize(Ljava/util/List;D)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v0

    goto :goto_1

    .line 1571
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewWidth:I

    iget v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPreviewHeight:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    goto :goto_2

    .line 1587
    .restart local v3    # "thumbnailSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v4, v5}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto/16 :goto_0

    .line 1589
    :cond_7
    const-string v4, "OCREngine"

    const-string v5, "resetPreviewSize()- mCameraDevice is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1593
    .end local v0    # "previewsize":Lcom/sec/android/seccamera/SecCamera$Size;
    .end local v1    # "sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    .end local v3    # "thumbnailSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    :cond_8
    const-string v4, "OCREngine"

    const-string v5, "resetPreviewSize()- mParameters is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public final scheduleAutoFocus()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 807
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isStartingPreview()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 808
    :cond_0
    const-string v0, "OCREngine"

    const-string v1, "[EdgeCapture] scheduleAutoFocus is skipped"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 810
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setmIsAdvanceFocusFocusing(Ljava/lang/Boolean;)V

    .line 841
    :goto_0
    return-void

    .line 814
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    if-nez v0, :cond_2

    .line 815
    const-string v0, "OCREngine"

    const-string v1, "[EdgeCapture] scheduleAutoFocus - current focus mode is off"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setmIsAdvanceFocusFocusing(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 820
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 821
    const-string v0, "OCREngine"

    const-string v1, "[EdgeCapture] scheduleAutoFocus - focusing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->setmIsAdvanceFocusFocusing(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 837
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/bcocr/CeRequestQueue;->removeRequest(IZ)V

    .line 838
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 840
    sput-boolean v2, Lcom/sec/android/app/bcocr/OCREngine;->m_bRestartTouchAF:Z

    goto :goto_0
.end method

.method public final scheduleChangeParameter(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 1348
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "scheduleChangeParameter key="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x7

    new-instance v2, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;

    invoke-direct {v2, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$CeSettingsParameter;-><init>(II)V

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1351
    return-void
.end method

.method public final schedulePostInit()V
    .locals 3

    .prologue
    .line 2683
    const-string v0, "OCREngine"

    const-string v1, "schedulePostInit"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2684
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 2685
    return-void
.end method

.method public scheduleProcessBack()V
    .locals 3

    .prologue
    .line 1322
    const-string v0, "OCREngine"

    const-string v1, "scheduleProcessBack"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x18

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1325
    return-void
.end method

.method public final scheduleSetAllParams()V
    .locals 3

    .prologue
    .line 916
    const-string v0, "OCREngine"

    const-string v1, "scheduleSetAllParams"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 919
    return-void
.end method

.method public final scheduleSetParameters(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1332
    const-string v0, "OCREngine"

    const-string v1, "scheduleSetParameter"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1334
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x8

    new-instance v2, Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;

    invoke-direct {v2, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$CeSecCameraParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1335
    return-void
.end method

.method public final scheduleStartEngine()V
    .locals 3

    .prologue
    .line 981
    const-string v0, "OCREngine"

    const-string v1, "scheduleStartEngine"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 984
    return-void
.end method

.method public final scheduleStartPreview()V
    .locals 3

    .prologue
    .line 1134
    const-string v0, "OCREngine"

    const-string v1, "scheduleStartPreview"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1137
    return-void
.end method

.method public final scheduleStartPreviewDummy()V
    .locals 3

    .prologue
    .line 1279
    const-string v0, "OCREngine"

    const-string v1, "scheduleStartPreviewDummy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x1b

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1281
    return-void
.end method

.method public final scheduleStopEngine()V
    .locals 3

    .prologue
    .line 1112
    const-string v0, "OCREngine"

    const-string v1, "scheduleStopEngine"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1115
    return-void
.end method

.method public final scheduleStopPreview()V
    .locals 3

    .prologue
    .line 1261
    const-string v0, "OCREngine"

    const-string v1, "scheduleStopPreview"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1263
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1264
    return-void
.end method

.method public final scheduleStopPreviewDummy()V
    .locals 3

    .prologue
    .line 1256
    const-string v0, "OCREngine"

    const-string v1, "scheduleStopPreviewDummy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0x19

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 1258
    return-void
.end method

.method public scheduleTakePicture()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 936
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/bcocr/CeRequestQueue;->searchDuplicateRequest(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    const-string v0, "OCREngine"

    const-string v1, "scheduleTakePicture - Now capturing, retun capture request. "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    :goto_0
    return-void

    .line 940
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    goto :goto_0
.end method

.method public final scheduleWait(I)V
    .locals 3
    .param p1, "milisec"    # I

    .prologue
    .line 971
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "scheduleWait: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mili seconds"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/16 v1, 0xd

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/bcocr/CeRequest;->obtainCeRequest(ILjava/lang/Object;)Lcom/sec/android/app/bcocr/CeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->addRequest(Lcom/sec/android/app/bcocr/CeRequest;)V

    .line 974
    return-void
.end method

.method protected searchForLastContentUri()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    if-nez v0, :cond_0

    .line 745
    :goto_0
    return-void

    .line 715
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "title = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 716
    .local v3, "selection":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/bcocr/OCREngine;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 718
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 719
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 720
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 721
    .local v8, "idIndex":I
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 723
    .local v7, "dataIndex":I
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->onSearchingLastContentUriCompleted()V

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.camera.NEW_PICTURE"

    iget-object v4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 726
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;

    .line 727
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 730
    .end local v7    # "dataIndex":I
    .end local v8    # "idIndex":I
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCaptureInitiated:Z

    if-eqz v0, :cond_3

    .line 731
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    if-nez v0, :cond_2

    .line 732
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 735
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;->removeMessages(I)V

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mUriSearchingHandler:Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;

    const-wide/16 v4, 0x1e

    invoke-virtual {v0, v9, v4, v5}, Lcom/sec/android/app/bcocr/OCREngine$UriSearchingHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 741
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 738
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->onSearchingLastContentUriCompleted()V

    goto :goto_1

    .line 743
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->onSearchingLastContentUriCompleted()V

    goto/16 :goto_0
.end method

.method public setAEAWBLockParameter(Z)V
    .locals 3
    .param p1, "lock"    # Z

    .prologue
    .line 2688
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAEAWBLockParameter : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2690
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v0, :cond_1

    .line 2703
    :cond_0
    :goto_0
    return-void

    .line 2694
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAeLockSupported:Z

    if-eqz v0, :cond_2

    .line 2695
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setAutoExposureLock(Z)V

    .line 2696
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z

    .line 2699
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mAwbLockSupported:Z

    if-eqz v0, :cond_0

    .line 2700
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setAutoWhiteBalanceLock(Z)V

    .line 2701
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->bIsAeAwbLocked:Z

    goto :goto_0
.end method

.method public setCAFStartTime(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 700
    iput-wide p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCAFStartTime:J

    .line 702
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ADV_FOCUS/ CAF time="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCAFStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    return-void
.end method

.method public setCameraDisplayOrientation()V
    .locals 6

    .prologue
    .line 2554
    const/4 v0, 0x0

    .line 2555
    .local v0, "degrees":I
    const/4 v2, 0x0

    .line 2557
    .local v2, "rotation":I
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 2559
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->DEVICE_TABLET:Z

    if-nez v3, :cond_4

    .line 2560
    packed-switch v2, :pswitch_data_0

    .line 2566
    :goto_0
    if-eqz v2, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 2567
    :cond_0
    const/16 v1, 0x5a

    .line 2594
    .local v1, "result":I
    :goto_1
    sput v1, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    .line 2596
    const-string v3, "OCREngine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setCameraDisplayOrientation : mOrientDegree is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rotation "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2598
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v3, :cond_1

    .line 2599
    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v3, v1}, Lcom/sec/android/seccamera/SecCamera;->setDisplayOrientation(I)V

    .line 2601
    :cond_1
    return-void

    .line 2561
    .end local v1    # "result":I
    :pswitch_0
    const/16 v0, 0x5a

    goto :goto_0

    .line 2562
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2563
    :pswitch_2
    const/16 v0, 0x10e

    goto :goto_0

    .line 2564
    :pswitch_3
    const/16 v0, 0xb4

    goto :goto_0

    .line 2570
    :cond_2
    const/16 v3, 0xb4

    if-ne v0, v3, :cond_3

    .line 2571
    const/16 v1, 0xb4

    .line 2572
    .restart local v1    # "result":I
    goto :goto_1

    .line 2573
    .end local v1    # "result":I
    :cond_3
    const/4 v1, 0x0

    .line 2576
    .restart local v1    # "result":I
    goto :goto_1

    .line 2577
    .end local v1    # "result":I
    :cond_4
    packed-switch v2, :pswitch_data_1

    .line 2583
    :goto_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 2584
    const/16 v1, 0x5a

    .line 2585
    .restart local v1    # "result":I
    goto :goto_1

    .line 2578
    .end local v1    # "result":I
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2579
    :pswitch_5
    const/16 v0, 0x5a

    goto :goto_2

    .line 2580
    :pswitch_6
    const/16 v0, 0xb4

    goto :goto_2

    .line 2581
    :pswitch_7
    const/16 v0, 0x10e

    goto :goto_2

    .line 2586
    :cond_5
    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 2587
    const/16 v1, 0x10e

    .line 2588
    .restart local v1    # "result":I
    goto :goto_1

    .line 2590
    .end local v1    # "result":I
    :cond_6
    move v1, v0

    .restart local v1    # "result":I
    goto :goto_1

    .line 2560
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 2577
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setFocusParameter(I)V
    .locals 4
    .param p1, "focus"    # I

    .prologue
    const/4 v3, 0x5

    .line 1499
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v0, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return-void

    .line 1502
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-eqz v0, :cond_2

    .line 1503
    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine;->checkFocusMode(I)I

    move-result p1

    .line 1505
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1508
    if-eq p1, v3, :cond_3

    .line 1509
    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    .line 1510
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 1511
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    .line 1513
    :cond_4
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFocusParameter : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setHDMICableConnected(Z)V
    .locals 4
    .param p1, "connected"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2541
    const-string v0, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setHDMICableConnected. connected="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2543
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v0, :cond_0

    .line 2544
    const-string v0, "OCREngine"

    const-string v1, "mCameraDevice is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2549
    :goto_0
    return-void

    .line 2548
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    const/16 v3, 0x461

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public setLandscapeActive(Z)V
    .locals 0
    .param p1, "active"    # Z

    .prologue
    .line 1969
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLandscapeActive:Z

    .line 1970
    return-void
.end method

.method public setLastCapturedTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1840
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedTitle:Ljava/lang/String;

    .line 1841
    return-void
.end method

.method public setLastContentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1836
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    .line 1837
    return-void
.end method

.method public setOCRPreviewDetectStart()V
    .locals 2

    .prologue
    .line 2642
    iget-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2643
    const-string v0, "OCREngine"

    const-string v1, "[mycheck]detect 1 : setOCRPreviewDetectStart"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2645
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mIsFinishOneShotPreviewCallback:Z

    .line 2646
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_1

    .line 2647
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    new-instance v1, Lcom/sec/android/app/bcocr/OCREngine$CameraPreviewCallback;

    invoke-direct {v1, p0}, Lcom/sec/android/app/bcocr/OCREngine$CameraPreviewCallback;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setOneShotPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 2649
    :cond_1
    return-void
.end method

.method public final setOnErrorCallbackListener(Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    .line 212
    return-void
.end method

.method public final setOnFocusStateChangedListener(Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnFocusStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

    .line 202
    return-void
.end method

.method public final setOnRecognitionStateChangedListener(Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

    .prologue
    .line 2638
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnRecogStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnRecognitionStateChangedListener;

    .line 2639
    return-void
.end method

.method public setOrientationAndSendItToFramework()V
    .locals 3

    .prologue
    .line 927
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setOrientationAndSendItToFramework - mOrientDegree : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mOrientDegree:I

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setRotation(I)V

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 930
    return-void
.end method

.method protected setOrientationListener()V
    .locals 2

    .prologue
    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 1978
    new-instance v0, Lcom/sec/android/app/bcocr/OCREngine$4;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/bcocr/OCREngine$4;-><init>(Lcom/sec/android/app/bcocr/OCREngine;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 1992
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1993
    return-void
.end method

.method protected setOrientationOnTake(I)V
    .locals 0
    .param p1, "orientationOnTake"    # I

    .prologue
    .line 2008
    iput p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOrientationOnTake:I

    .line 2009
    return-void
.end method

.method public setTouchFocusPosition(II)V
    .locals 13
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v12, 0x1

    const/high16 v11, 0x44fa0000    # 2000.0f

    const/high16 v10, 0x447a0000    # 1000.0f

    const/4 v9, 0x0

    .line 1874
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v6, :cond_1

    .line 1913
    :cond_0
    :goto_0
    return-void

    .line 1881
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    .line 1882
    .local v1, "focusWidth":I
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v0, v6

    .line 1883
    .local v0, "focusHeight":I
    iget v5, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    .line 1884
    .local v5, "width":I
    iget v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    .line 1886
    .local v2, "height":I
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    if-nez v6, :cond_2

    .line 1887
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    .line 1888
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    new-instance v7, Lcom/sec/android/seccamera/SecCamera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {v7, v8, v12}, Lcom/sec/android/seccamera/SecCamera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1891
    :cond_2
    div-int/lit8 v6, v1, 0x2

    sub-int v6, p1, v6

    sub-int v7, v5, v1

    invoke-static {v6, v9, v7}, Lcom/sec/android/app/bcocr/OCREngine;->clamp(III)I

    move-result v3

    .line 1892
    .local v3, "left":I
    div-int/lit8 v6, v0, 0x2

    sub-int v6, p2, v6

    sub-int v7, v2, v0

    invoke-static {v6, v9, v7}, Lcom/sec/android/app/bcocr/OCREngine;->clamp(III)I

    move-result v4

    .line 1894
    .local v4, "top":I
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/seccamera/SecCamera$Area;

    iget-object v6, v6, Lcom/sec/android/seccamera/SecCamera$Area;->rect:Landroid/graphics/Rect;

    int-to-float v7, v3

    int-to-float v8, v5

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    sub-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 1895
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/seccamera/SecCamera$Area;

    iget-object v6, v6, Lcom/sec/android/seccamera/SecCamera$Area;->rect:Landroid/graphics/Rect;

    int-to-float v7, v4

    int-to-float v8, v2

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    sub-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->top:I

    .line 1896
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/seccamera/SecCamera$Area;

    iget-object v6, v6, Lcom/sec/android/seccamera/SecCamera$Area;->rect:Landroid/graphics/Rect;

    add-int v7, v3, v1

    int-to-float v7, v7

    int-to-float v8, v5

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    sub-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 1897
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/seccamera/SecCamera$Area;

    iget-object v6, v6, Lcom/sec/android/seccamera/SecCamera$Area;->rect:Landroid/graphics/Rect;

    add-int v7, v4, v0

    int-to-float v7, v7

    int-to-float v8, v2

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    sub-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 1899
    sget-boolean v6, Lcom/sec/android/app/bcocr/Feature;->CAMERA_SUPPORT_TOUCH_FOCUSMODE:Z

    if-nez v6, :cond_3

    .line 1901
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v7, 0x5

    invoke-static {v7}, Lcom/sec/android/app/bcocr/OCRSettings;->getModeString(I)Ljava/lang/String;

    move-result-object v7

    .line 1902
    invoke-static {v12}, Lcom/sec/android/app/bcocr/OCRSettings;->getFocusModeString(I)Ljava/lang/String;

    move-result-object v8

    .line 1901
    invoke-virtual {v6, v7, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1905
    :cond_3
    sget-boolean v6, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS_SET_AREA_FOR_TOUCH_FOCUS:Z

    if-eqz v6, :cond_4

    .line 1906
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mTapArea:Ljava/util/List;

    invoke-virtual {v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1910
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v7, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v6, v7}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1912
    sput-boolean v12, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchFocusPositioned:Z

    goto/16 :goto_0

    .line 1908
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusAreas(Ljava/util/List;)V

    goto :goto_1
.end method

.method public setWaitingAnimation(Z)V
    .locals 0
    .param p1, "waitinganimation"    # Z

    .prologue
    .line 567
    iput-boolean p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mEnableWaitingAnimation:Z

    .line 568
    return-void
.end method

.method public startContinuousAF()V
    .locals 1

    .prologue
    .line 1952
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->startContinuousAF()V

    .line 1955
    :cond_0
    return-void
.end method

.method public startTouchAutoFocus()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1920
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v0, :cond_0

    .line 1921
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->removeMessages(I)V

    .line 1923
    :cond_0
    sput-boolean v1, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    .line 1924
    return-void
.end method

.method public startingPreviewCompleted()V
    .locals 1

    .prologue
    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onStartingPreviewCompleted()V

    .line 1253
    return-void
.end method

.method public stopContinuousAF()V
    .locals 2

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 1959
    const-string v0, "CAFLOG"

    const-string v1, "stopCAF called in engine"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1960
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->stopContinuousAF()V

    .line 1962
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mRequestQueue:Lcom/sec/android/app/bcocr/CeRequestQueue;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/CeRequestQueue;->isFirstRequest(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1963
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    .line 1965
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 1966
    return-void
.end method

.method public stopLongTouchAutoFocus()V
    .locals 1

    .prologue
    .line 1927
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 1928
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->stopTouchAutoFocus()V

    .line 1930
    :cond_0
    return-void
.end method

.method public stopTouchAutoFocus()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v0, :cond_1

    .line 1934
    :cond_0
    const-string v0, "OCREngine"

    const-string v1, "Parameters or CameraDevice is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1945
    :goto_0
    return-void

    .line 1938
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1941
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->setFocusParameter(I)V

    .line 1942
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->setCAFStartTime(J)V

    .line 1943
    sput-boolean v2, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z

    .line 1944
    sput-boolean v2, Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchFocusPositioned:Z

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 1718
    const-string v0, "OCREngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "surfaceChanged w="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1719
    const-string v0, "AXLOG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "surfaceCreate**EndU["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    if-ne v0, p3, :cond_0

    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    if-ne v0, p4, :cond_0

    .line 1726
    :goto_0
    return-void

    .line 1724
    :cond_0
    iput p3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    .line 1725
    iput p4, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1730
    const-string v0, "OCREngine"

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1733
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    .line 1734
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1738
    const-string v0, "OCREngine"

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1740
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1741
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->isCurrentState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1742
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine;->doStopPreviewSync()V

    .line 1743
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1745
    :cond_1
    return-void
.end method

.method public turnOffDISMode()V
    .locals 3

    .prologue
    .line 2730
    const-string v0, "OCREngine"

    const-string v1, "DIS Turn Off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2731
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 2732
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v1, "video-stabilization-ocr"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2733
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 2735
    :cond_0
    return-void
.end method

.method public turnOnDISMode()V
    .locals 3

    .prologue
    .line 2722
    const-string v0, "OCREngine"

    const-string v1, "DIS Turn On"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2723
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 2724
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v1, "video-stabilization-ocr"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2725
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 2727
    :cond_0
    return-void
.end method

.method public unlockAEAWB()V
    .locals 2

    .prologue
    .line 2714
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v0, :cond_1

    .line 2719
    :cond_0
    :goto_0
    return-void

    .line 2717
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/OCREngine;->setAEAWBLockParameter(Z)V

    .line 2718
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto :goto_0
.end method

.method public updateFocusIndicator()V
    .locals 2

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnFocusStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

    if-eqz v0, :cond_0

    .line 1815
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOnFocusStateChangedListener:Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;

    iget v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine$OnFocusStateChangedListener;->onFocusStateChanged(I)V

    .line 1817
    :cond_0
    return-void
.end method

.method public updateStorage()V
    .locals 2

    .prologue
    .line 1748
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mShootingModeManager:Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mStorage:I

    .line 1749
    return-void
.end method

.method public waitForCurrentPictureSaving()V
    .locals 1

    .prologue
    .line 2053
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 2054
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2059
    :cond_0
    :goto_0
    return-void

    .line 2056
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public waitForCurrentSearchingLastContentUri()V
    .locals 1

    .prologue
    .line 2094
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSearchingLastContentUriThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 2095
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mSearchingLastContentUriThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2100
    :cond_0
    :goto_0
    return-void

    .line 2097
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public waitForEngineStartingThread()V
    .locals 1

    .prologue
    .line 1086
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mOpenCameraThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1092
    :cond_0
    :goto_0
    return-void

    .line 1089
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public waitForLastFileToSync()V
    .locals 6

    .prologue
    .line 2066
    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 2080
    :goto_0
    return-void

    .line 2071
    :cond_0
    :try_start_0
    const-string v2, "OCREngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "before file sync::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2072
    sget-object v2, Lcom/sec/android/app/bcocr/OCREngine;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    const-string v4, "rw"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 2073
    .local v1, "fd":Ljava/io/FileDescriptor;
    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V

    .line 2074
    const-string v2, "OCREngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "after file sync::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2075
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    :catch_0
    move-exception v0

    .line 2076
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2077
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 2078
    .local v0, "e":Ljava/io/SyncFailedException;
    invoke-virtual {v0}, Ljava/io/SyncFailedException;->printStackTrace()V

    goto :goto_0
.end method

.method public waitForStartPreviewThreadComplete()V
    .locals 1

    .prologue
    .line 2084
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 2085
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine;->mStartPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2090
    :cond_0
    :goto_0
    return-void

    .line 2087
    :catch_0
    move-exception v0

    goto :goto_0
.end method
