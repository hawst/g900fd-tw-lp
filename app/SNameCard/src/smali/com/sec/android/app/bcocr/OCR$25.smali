.class Lcom/sec/android/app/bcocr/OCR$25;
.super Landroid/view/View$AccessibilityDelegate;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/OCR;->initTalkbackTTSForVoiceControl()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    .line 6220
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4
    .param p1, "host"    # Landroid/view/View;
    .param p2, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 6223
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 6224
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->isAccessibilityFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6225
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->isVoiceInputSettingOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6226
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    .line 6227
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 6226
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    .line 6233
    :cond_0
    :goto_0
    return-void

    .line 6229
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCR;->_tts:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCR$25;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCR;->mSubMain:Landroid/widget/RelativeLayout;

    .line 6230
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 6229
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->speak(Ljava/util/Locale;Ljava/lang/String;)I

    goto :goto_0
.end method
