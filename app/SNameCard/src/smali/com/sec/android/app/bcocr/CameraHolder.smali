.class public Lcom/sec/android/app/bcocr/CameraHolder;
.super Ljava/lang/Object;
.source "CameraHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bcocr/CameraHolder$MyHandler;
    }
.end annotation


# static fields
.field private static final RELEASE_CAMERA:I = 0x1

.field private static final TAG:Ljava/lang/String; = "CameraHolder"

.field private static sHolder:Lcom/sec/android/app/bcocr/CameraHolder;


# instance fields
.field private mBackCameraId:I

.field private mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

.field private mCameraId:I

.field private mFrontCameraId:I

.field private final mHandler:Landroid/os/Handler;

.field private mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

.field private mKeepBeforeTime:J

.field private mNumberOfCameras:I

.field private mUsers:I


# direct methods
.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mKeepBeforeTime:J

    .line 50
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    .line 52
    iput v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I

    .line 53
    iput v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mBackCameraId:I

    iput v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mFrontCameraId:I

    .line 91
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "CameraHolder"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "ht":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 93
    new-instance v2, Lcom/sec/android/app/bcocr/CameraHolder$MyHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/bcocr/CameraHolder$MyHandler;-><init>(Lcom/sec/android/app/bcocr/CameraHolder;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mHandler:Landroid/os/Handler;

    .line 94
    invoke-static {}, Lcom/sec/android/seccamera/SecCamera;->getNumberOfCameras()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mNumberOfCameras:I

    .line 95
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mNumberOfCameras:I

    new-array v2, v2, [Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mNumberOfCameras:I

    if-lt v1, v2, :cond_0

    .line 106
    return-void

    .line 97
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    new-instance v3, Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    invoke-direct {v3}, Lcom/sec/android/seccamera/SecCamera$CameraInfo;-><init>()V

    aput-object v3, v2, v1

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    aget-object v2, v2, v1

    invoke-static {v1, v2}, Lcom/sec/android/seccamera/SecCamera;->getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V

    .line 99
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mBackCameraId:I

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->facing:I

    if-nez v2, :cond_1

    .line 100
    iput v1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mBackCameraId:I

    .line 102
    :cond_1
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mFrontCameraId:I

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 103
    iput v1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mFrontCameraId:I

    .line 96
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/CameraHolder;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/CameraHolder;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/CameraHolder;->releaseCamera()V

    return-void
.end method

.method public static declared-synchronized instance()Lcom/sec/android/app/bcocr/CameraHolder;
    .locals 2

    .prologue
    .line 59
    const-class v1, Lcom/sec/android/app/bcocr/CameraHolder;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/bcocr/CameraHolder;->sHolder:Lcom/sec/android/app/bcocr/CameraHolder;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/app/bcocr/CameraHolder;

    invoke-direct {v0}, Lcom/sec/android/app/bcocr/CameraHolder;-><init>()V

    sput-object v0, Lcom/sec/android/app/bcocr/CameraHolder;->sHolder:Lcom/sec/android/app/bcocr/CameraHolder;

    .line 62
    :cond_0
    sget-object v0, Lcom/sec/android/app/bcocr/CameraHolder;->sHolder:Lcom/sec/android/app/bcocr/CameraHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized releaseCamera()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 185
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-nez v4, :cond_1

    move v4, v2

    :goto_0
    invoke-static {v4}, Lcom/sec/android/app/bcocr/Util;->Assert(Z)V

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_2

    :goto_1
    invoke-static {v2}, Lcom/sec/android/app/bcocr/Util;->Assert(Z)V

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 188
    .local v0, "now":J
    iget-wide v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mKeepBeforeTime:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mKeepBeforeTime:J

    sub-long/2addr v4, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    .end local v0    # "now":J
    :cond_1
    move v4, v3

    .line 185
    goto :goto_0

    :cond_2
    move v2, v3

    .line 186
    goto :goto_1

    .line 195
    .restart local v0    # "now":J
    :cond_3
    :try_start_1
    const-string v2, "CameraHolder"

    const-string v3, "mCameraDevice.release();"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 198
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 201
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 185
    .end local v0    # "now":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method public getBackCameraId()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mBackCameraId:I

    return v0
.end method

.method public getCameraInfo()[Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mInfo:[Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    return-object v0
.end method

.method public getFrontCameraId()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mFrontCameraId:I

    return v0
.end method

.method public getNumberOfCameras()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mNumberOfCameras:I

    return v0
.end method

.method public declared-synchronized keep()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 208
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->Assert(Z)V

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0xbb8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mKeepBeforeTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized open(I)Lcom/sec/android/seccamera/SecCamera;
    .locals 5
    .param p1, "cameraId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/bcocr/CameraHardwareException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 117
    monitor-enter p0

    :try_start_0
    iget v3, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-nez v3, :cond_2

    :goto_0
    invoke-static {v2}, Lcom/sec/android/app/bcocr/Util;->Assert(Z)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I

    if-eq v2, p1, :cond_0

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 121
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 122
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I

    .line 125
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_3

    .line 127
    :try_start_1
    const-string v2, "CameraHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "open camera "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {p1}, Lcom/sec/android/seccamera/SecCamera;->open(I)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 129
    iput p1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :goto_1
    :try_start_2
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 156
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mKeepBeforeTime:J

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v2

    .line 117
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/RuntimeException;
    const-wide/16 v2, 0xc8

    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 134
    invoke-static {p1}, Lcom/sec/android/seccamera/SecCamera;->open(I)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 135
    iput p1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraId:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 136
    :catch_1
    move-exception v1

    .line 137
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_4
    new-instance v2, Lcom/sec/android/app/bcocr/CameraHardwareException;

    invoke-direct {v2, v0}, Lcom/sec/android/app/bcocr/CameraHardwareException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 117
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 142
    :cond_3
    :try_start_5
    const-string v2, "CameraHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reconnect camera "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->reconnect()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 144
    :catch_2
    move-exception v0

    .line 145
    .local v0, "e":Ljava/io/IOException;
    :try_start_6
    const-string v2, "CameraHolder"

    const-string v3, "reconnect failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v2, Lcom/sec/android/app/bcocr/CameraHardwareException;

    invoke-direct {v2, v0}, Lcom/sec/android/app/bcocr/CameraHardwareException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public declared-synchronized release()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 178
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/sec/android/app/bcocr/Util;->Assert(Z)V

    .line 179
    iget v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->stopPreview()V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/bcocr/CameraHolder;->releaseCamera()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized tryOpen(I)Lcom/sec/android/seccamera/SecCamera;
    .locals 4
    .param p1, "cameraId"    # I

    .prologue
    const/4 v1, 0x0

    .line 166
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/sec/android/app/bcocr/CameraHolder;->mUsers:I

    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/bcocr/CameraHolder;->open(I)Lcom/sec/android/seccamera/SecCamera;
    :try_end_0
    .catch Lcom/sec/android/app/bcocr/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 173
    :cond_0
    monitor-exit p0

    return-object v1

    .line 167
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Lcom/sec/android/app/bcocr/CameraHardwareException;
    :try_start_1
    const-string v2, "eng"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    .end local v0    # "e":Lcom/sec/android/app/bcocr/CameraHardwareException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
