.class public Lcom/sec/android/app/bcocr/CheckMemory;
.super Ljava/lang/Object;
.source "CheckMemory.java"


# static fields
.field public static final CANNOT_STAT_ERROR:I = -0x2

.field public static final LOW_STORAGE_THRESHOLD:J = 0xa00000L

.field public static final NO_STORAGE_ERROR:I = -0x1

.field private static NUM_OF_QUALITIES:I = 0x0

.field private static NUM_OF_RESOLUTIONS:I = 0x0

.field private static QUALITY_INDEX:I = 0x0

.field private static RESOLUTION_INDEX:I = 0x0

.field private static SIZE_INDEX:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "CheckMemory"

.field private static mImageSizeList:[[I

.field protected static mStorageManager:Landroid/os/storage/StorageManager;

.field protected static mStorageVolumes:[Landroid/os/storage/StorageVolume;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xa00

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x3

    .line 39
    const/16 v0, 0x5a

    new-array v0, v0, [[I

    .line 40
    new-array v1, v4, [I

    const/16 v2, 0x20

    aput v2, v1, v6

    aput v8, v1, v5

    aput-object v1, v0, v6

    .line 41
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v7

    .line 42
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 43
    new-array v1, v4, [I

    const/16 v2, 0x21

    aput v2, v1, v6

    aput v8, v1, v5

    aput-object v1, v0, v4

    const/4 v1, 0x4

    .line 44
    new-array v2, v4, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 45
    new-array v2, v4, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 46
    new-array v2, v4, [I

    const/16 v3, 0x1e

    aput v3, v2, v6

    aput v8, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 47
    new-array v2, v4, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 48
    new-array v2, v4, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 49
    new-array v2, v4, [I

    const/16 v3, 0x24

    aput v3, v2, v6

    aput v8, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 50
    new-array v2, v4, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 51
    new-array v2, v4, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 52
    new-array v2, v4, [I

    aput v8, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 53
    new-array v2, v4, [I

    aput v7, v2, v7

    const/16 v3, 0x820

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 54
    new-array v2, v4, [I

    aput v5, v2, v7

    const/16 v3, 0x640

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 55
    new-array v2, v4, [I

    aput v7, v2, v6

    const/16 v3, 0x820

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 56
    new-array v2, v4, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 57
    new-array v2, v4, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 58
    new-array v2, v4, [I

    aput v5, v2, v6

    const/16 v3, 0x8c0

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 59
    new-array v2, v4, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 60
    new-array v2, v4, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 61
    new-array v2, v4, [I

    aput v4, v2, v6

    const/16 v3, 0x720

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 62
    new-array v2, v4, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 63
    new-array v2, v4, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 64
    new-array v2, v4, [I

    const/16 v3, 0x19

    aput v3, v2, v6

    const/16 v3, 0x668

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 65
    new-array v2, v4, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 66
    new-array v2, v4, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 67
    new-array v2, v4, [I

    const/4 v3, 0x4

    aput v3, v2, v6

    const/16 v3, 0x640

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 68
    new-array v2, v4, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 69
    new-array v2, v4, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 70
    new-array v2, v4, [I

    const/4 v3, 0x5

    aput v3, v2, v6

    const/16 v3, 0x4b0

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 71
    new-array v2, v4, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 72
    new-array v2, v4, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 73
    new-array v2, v4, [I

    const/4 v3, 0x6

    aput v3, v2, v6

    const/16 v3, 0x39a

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 74
    new-array v2, v4, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 75
    new-array v2, v4, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 76
    new-array v2, v4, [I

    const/4 v3, 0x7

    aput v3, v2, v6

    const/16 v3, 0x320

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 77
    new-array v2, v4, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 78
    new-array v2, v4, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 79
    new-array v2, v4, [I

    const/16 v3, 0x17

    aput v3, v2, v6

    const/16 v3, 0x2ec

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 80
    new-array v2, v4, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 81
    new-array v2, v4, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 82
    new-array v2, v4, [I

    const/16 v3, 0x1a

    aput v3, v2, v6

    const/16 v3, 0x2ec

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 83
    new-array v2, v4, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 84
    new-array v2, v4, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 85
    new-array v2, v4, [I

    const/16 v3, 0x1b

    aput v3, v2, v6

    const/16 v3, 0x1f4

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 86
    new-array v2, v4, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 87
    new-array v2, v4, [I

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 88
    new-array v2, v4, [I

    const/16 v3, 0x8

    aput v3, v2, v6

    const/16 v3, 0x258

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 89
    new-array v2, v4, [I

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 90
    new-array v2, v4, [I

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 91
    new-array v2, v4, [I

    const/16 v3, 0x9

    aput v3, v2, v6

    const/16 v3, 0x1f4

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 92
    new-array v2, v4, [I

    fill-array-data v2, :array_20

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 93
    new-array v2, v4, [I

    fill-array-data v2, :array_21

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 94
    new-array v2, v4, [I

    const/16 v3, 0x1c

    aput v3, v2, v6

    const/16 v3, 0x1f4

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 95
    new-array v2, v4, [I

    fill-array-data v2, :array_22

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 96
    new-array v2, v4, [I

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 97
    new-array v2, v4, [I

    const/16 v3, 0x18

    aput v3, v2, v6

    const/16 v3, 0x258

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    .line 98
    new-array v2, v4, [I

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 99
    new-array v2, v4, [I

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 100
    new-array v2, v4, [I

    const/16 v3, 0x1f

    aput v3, v2, v6

    const/16 v3, 0x258

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    .line 101
    new-array v2, v4, [I

    fill-array-data v2, :array_26

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 102
    new-array v2, v4, [I

    fill-array-data v2, :array_27

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    .line 103
    new-array v2, v4, [I

    const/16 v3, 0xa

    aput v3, v2, v6

    const/16 v3, 0x168

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x40

    .line 104
    new-array v2, v4, [I

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 105
    new-array v2, v4, [I

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    const/16 v1, 0x42

    .line 106
    new-array v2, v4, [I

    const/16 v3, 0xc

    aput v3, v2, v6

    const/16 v3, 0x82

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x43

    .line 107
    new-array v2, v4, [I

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 108
    new-array v2, v4, [I

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/16 v1, 0x45

    .line 109
    new-array v2, v4, [I

    const/16 v3, 0xb

    aput v3, v2, v6

    const/16 v3, 0x64

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 110
    new-array v2, v4, [I

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    const/16 v1, 0x47

    .line 111
    new-array v2, v4, [I

    fill-array-data v2, :array_2d

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 112
    new-array v2, v4, [I

    const/16 v3, 0x10

    aput v3, v2, v6

    const/16 v3, 0x28

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x49

    .line 113
    new-array v2, v4, [I

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    .line 114
    new-array v2, v4, [I

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 115
    new-array v2, v4, [I

    const/16 v3, 0x12

    aput v3, v2, v6

    const/16 v3, 0x20

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    .line 116
    new-array v2, v4, [I

    fill-array-data v2, :array_30

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 117
    new-array v2, v4, [I

    fill-array-data v2, :array_31

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    .line 118
    new-array v2, v4, [I

    const/16 v3, 0x14

    aput v3, v2, v6

    const/16 v3, 0x820

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 119
    new-array v2, v4, [I

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    const/16 v1, 0x50

    .line 120
    new-array v2, v4, [I

    fill-array-data v2, :array_33

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 121
    new-array v2, v4, [I

    const/16 v3, 0x15

    aput v3, v2, v6

    const/16 v3, 0x4b0

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x52

    .line 122
    new-array v2, v4, [I

    fill-array-data v2, :array_34

    aput-object v2, v0, v1

    const/16 v1, 0x53

    .line 123
    new-array v2, v4, [I

    fill-array-data v2, :array_35

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 124
    new-array v2, v4, [I

    const/16 v3, 0xe

    aput v3, v2, v6

    const/16 v3, 0x82

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x55

    .line 125
    new-array v2, v4, [I

    fill-array-data v2, :array_36

    aput-object v2, v0, v1

    const/16 v1, 0x56

    .line 126
    new-array v2, v4, [I

    fill-array-data v2, :array_37

    aput-object v2, v0, v1

    const/16 v1, 0x57

    .line 127
    new-array v2, v4, [I

    const/16 v3, 0x1d

    aput v3, v2, v6

    const/16 v3, 0x82

    aput v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x58

    .line 128
    new-array v2, v4, [I

    fill-array-data v2, :array_38

    aput-object v2, v0, v1

    const/16 v1, 0x59

    .line 129
    new-array v2, v4, [I

    fill-array-data v2, :array_39

    aput-object v2, v0, v1

    .line 39
    sput-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mImageSizeList:[[I

    .line 131
    sput v4, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    .line 132
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mImageSizeList:[[I

    array-length v0, v0

    sget v1, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    div-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_RESOLUTIONS:I

    .line 133
    sput v6, Lcom/sec/android/app/bcocr/CheckMemory;->RESOLUTION_INDEX:I

    .line 134
    sput v7, Lcom/sec/android/app/bcocr/CheckMemory;->QUALITY_INDEX:I

    .line 135
    sput v5, Lcom/sec/android/app/bcocr/CheckMemory;->SIZE_INDEX:I

    return-void

    .line 41
    nop

    :array_0
    .array-data 4
        0x20
        0x1
        0x820
    .end array-data

    .line 42
    :array_1
    .array-data 4
        0x20
        0x2
        0x640
    .end array-data

    .line 44
    :array_2
    .array-data 4
        0x21
        0x1
        0x820
    .end array-data

    .line 45
    :array_3
    .array-data 4
        0x21
        0x2
        0x640
    .end array-data

    .line 47
    :array_4
    .array-data 4
        0x1e
        0x1
        0x820
    .end array-data

    .line 48
    :array_5
    .array-data 4
        0x1e
        0x2
        0x640
    .end array-data

    .line 50
    :array_6
    .array-data 4
        0x24
        0x1
        0x820
    .end array-data

    .line 51
    :array_7
    .array-data 4
        0x24
        0x2
        0x640
    .end array-data

    .line 56
    :array_8
    .array-data 4
        0x1
        0x1
        0x69a
    .end array-data

    .line 57
    :array_9
    .array-data 4
        0x1
        0x2
        0x514
    .end array-data

    .line 59
    :array_a
    .array-data 4
        0x2
        0x1
        0x71c
    .end array-data

    .line 60
    :array_b
    .array-data 4
        0x2
        0x2
        0x578
    .end array-data

    .line 62
    :array_c
    .array-data 4
        0x3
        0x1
        0x5ca
    .end array-data

    .line 63
    :array_d
    .array-data 4
        0x3
        0x2
        0x474
    .end array-data

    .line 65
    :array_e
    .array-data 4
        0x19
        0x1
        0x53c
    .end array-data

    .line 66
    :array_f
    .array-data 4
        0x19
        0x2
        0x401
    .end array-data

    .line 68
    :array_10
    .array-data 4
        0x4
        0x1
        0x514
    .end array-data

    .line 69
    :array_11
    .array-data 4
        0x4
        0x2
        0x3e8
    .end array-data

    .line 71
    :array_12
    .array-data 4
        0x5
        0x1
        0x384
    .end array-data

    .line 72
    :array_13
    .array-data 4
        0x5
        0x2
        0x1f4
    .end array-data

    .line 74
    :array_14
    .array-data 4
        0x6
        0x1
        0x266
    .end array-data

    .line 75
    :array_15
    .array-data 4
        0x6
        0x2
        0x1cd
    .end array-data

    .line 77
    :array_16
    .array-data 4
        0x7
        0x1
        0x28a
    .end array-data

    .line 78
    :array_17
    .array-data 4
        0x7
        0x2
        0x226
    .end array-data

    .line 80
    :array_18
    .array-data 4
        0x17
        0x1
        0x25f
    .end array-data

    .line 81
    :array_19
    .array-data 4
        0x17
        0x2
        0x202
    .end array-data

    .line 83
    :array_1a
    .array-data 4
        0x1a
        0x1
        0x25f
    .end array-data

    .line 84
    :array_1b
    .array-data 4
        0x1a
        0x2
        0x202
    .end array-data

    .line 86
    :array_1c
    .array-data 4
        0x1b
        0x1
        0x190
    .end array-data

    .line 87
    :array_1d
    .array-data 4
        0x1b
        0x2
        0x12c
    .end array-data

    .line 89
    :array_1e
    .array-data 4
        0x8
        0x1
        0x1f4
    .end array-data

    .line 90
    :array_1f
    .array-data 4
        0x8
        0x2
        0x190
    .end array-data

    .line 92
    :array_20
    .array-data 4
        0x9
        0x1
        0x190
    .end array-data

    .line 93
    :array_21
    .array-data 4
        0x9
        0x2
        0x12c
    .end array-data

    .line 95
    :array_22
    .array-data 4
        0x1c
        0x1
        0x190
    .end array-data

    .line 96
    :array_23
    .array-data 4
        0x1c
        0x2
        0x12c
    .end array-data

    .line 98
    :array_24
    .array-data 4
        0x18
        0x1
        0x1f4
    .end array-data

    .line 99
    :array_25
    .array-data 4
        0x18
        0x2
        0x190
    .end array-data

    .line 101
    :array_26
    .array-data 4
        0x1f
        0x1
        0x1f4
    .end array-data

    .line 102
    :array_27
    .array-data 4
        0x1f
        0x2
        0x190
    .end array-data

    .line 104
    :array_28
    .array-data 4
        0xa
        0x1
        0xb4
    .end array-data

    .line 105
    :array_29
    .array-data 4
        0xa
        0x2
        0x81
    .end array-data

    .line 107
    :array_2a
    .array-data 4
        0xc
        0x1
        0x64
    .end array-data

    .line 108
    :array_2b
    .array-data 4
        0xc
        0x2
        0x50
    .end array-data

    .line 110
    :array_2c
    .array-data 4
        0xb
        0x1
        0x50
    .end array-data

    .line 111
    :array_2d
    .array-data 4
        0xb
        0x2
        0x37
    .end array-data

    .line 113
    :array_2e
    .array-data 4
        0x10
        0x1
        0x20
    .end array-data

    .line 114
    :array_2f
    .array-data 4
        0x10
        0x2
        0x1c
    .end array-data

    .line 116
    :array_30
    .array-data 4
        0x12
        0x1
        0x1c
    .end array-data

    .line 117
    :array_31
    .array-data 4
        0x12
        0x2
        0x19
    .end array-data

    .line 119
    :array_32
    .array-data 4
        0x14
        0x1
        0x69a
    .end array-data

    .line 120
    :array_33
    .array-data 4
        0x14
        0x2
        0x514
    .end array-data

    .line 122
    :array_34
    .array-data 4
        0x15
        0x1
        0x384
    .end array-data

    .line 123
    :array_35
    .array-data 4
        0x15
        0x2
        0x1f4
    .end array-data

    .line 125
    :array_36
    .array-data 4
        0xe
        0x1
        0x64
    .end array-data

    .line 126
    :array_37
    .array-data 4
        0xe
        0x2
        0x50
    .end array-data

    .line 128
    :array_38
    .array-data 4
        0x1d
        0x1
        0x64
    .end array-data

    .line 129
    :array_39
    .array-data 4
        0x1d
        0x2
        0x50
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkInternalLowDBUpdateStorage(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 278
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    int-to-long v2, v8

    .line 279
    .local v2, "limit_low_storage_db_size":J
    const/4 v6, 0x0

    .line 281
    .local v6, "storageDirectory":Ljava/lang/String;
    :try_start_0
    sget-object v8, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 282
    sget-object v8, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 286
    :goto_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 288
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long v4, v8, v10

    .line 289
    .local v4, "remain":J
    cmp-long v8, v4, v2

    if-gez v8, :cond_1

    .line 290
    const-string v8, "CheckMemory"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "checkInternalLowDBUpdateStorage() : Not enough memory. remain : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v4    # "remain":J
    :goto_1
    return v7

    .line 284
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0011

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 294
    .restart local v1    # "stat":Landroid/os/StatFs;
    .restart local v4    # "remain":J
    :cond_1
    const/4 v7, 0x1

    goto :goto_1

    .line 295
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v4    # "remain":J
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v8, "CheckMemory"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "checkInternalLowDBUpdateStorage() : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getAvailableStorage(I)J
    .locals 8
    .param p0, "storage"    # I

    .prologue
    const/4 v3, 0x1

    .line 187
    const/4 v2, 0x0

    .line 188
    .local v2, "storageDirectory":Ljava/lang/String;
    if-ne p0, v3, :cond_1

    .line 189
    :try_start_0
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v3, v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 190
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 197
    :goto_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 200
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v6, v3

    mul-long/2addr v4, v6

    const-wide/32 v6, 0xa00000

    sub-long/2addr v4, v6

    .line 206
    .end local v1    # "stat":Landroid/os/StatFs;
    :goto_1
    return-wide v4

    .line 192
    :cond_0
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 194
    goto :goto_0

    .line 195
    :cond_1
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 205
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "CheckMemory"

    const-string v4, "cannot stat the filesystem then we don\'t know how many free bytes exist"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const-wide/16 v4, -0x2

    goto :goto_1
.end method

.method public static getExternalSDStoragePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 271
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getExternalStoragePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMaxSizeOfImage(II)J
    .locals 6
    .param p0, "resolution"    # I
    .param p1, "quality"    # I

    .prologue
    .line 242
    const-wide/16 v2, 0x0

    .line 244
    .local v2, "nMaxSize":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v4, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_RESOLUTIONS:I

    if-lt v0, v4, :cond_0

    .line 257
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_3

    .line 258
    const-string v4, "CheckMemory"

    const-string v5, "Resolution or Quality setting maybe wrong"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const-wide/32 v4, 0x100000

    .line 261
    :goto_1
    return-wide v4

    .line 245
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    sget v4, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    if-lt v1, v4, :cond_1

    .line 244
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_1
    sget-object v4, Lcom/sec/android/app/bcocr/CheckMemory;->mImageSizeList:[[I

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    mul-int/2addr v5, v0

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->RESOLUTION_INDEX:I

    aget v4, v4, v5

    if-ne v4, p0, :cond_2

    .line 247
    sget-object v4, Lcom/sec/android/app/bcocr/CheckMemory;->mImageSizeList:[[I

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    mul-int/2addr v5, v0

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->QUALITY_INDEX:I

    aget v4, v4, v5

    if-ne v4, p1, :cond_2

    .line 248
    sget-object v4, Lcom/sec/android/app/bcocr/CheckMemory;->mImageSizeList:[[I

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->NUM_OF_QUALITIES:I

    mul-int/2addr v5, v0

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    sget v5, Lcom/sec/android/app/bcocr/CheckMemory;->SIZE_INDEX:I

    aget v4, v4, v5

    int-to-long v2, v4

    .line 249
    goto :goto_3

    .line 245
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 261
    .end local v1    # "j":I
    :cond_3
    const-wide/16 v4, 0x400

    mul-long/2addr v4, v2

    goto :goto_1
.end method

.method public static getRemainCount(III)I
    .locals 8
    .param p0, "storage"    # I
    .param p1, "resolution"    # I
    .param p2, "quality"    # I

    .prologue
    const/4 v4, -0x1

    .line 156
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CheckMemory;->getAvailableStorage(I)J

    move-result-wide v2

    .line 158
    .local v2, "lAvailableStorage":J
    const-wide/16 v6, -0x2

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    .line 159
    const/4 v1, 0x0

    .line 161
    .local v1, "nRemainCount":I
    :try_start_0
    invoke-static {p1, p2}, Lcom/sec/android/app/bcocr/CheckMemory;->getMaxSizeOfImage(II)J

    move-result-wide v6

    div-long v4, v2, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    long-to-int v1, v4

    .line 167
    .end local v1    # "nRemainCount":I
    :goto_0
    return v1

    .line 163
    .restart local v1    # "nRemainCount":I
    :catch_0
    move-exception v0

    .local v0, "ex":Ljava/lang/Exception;
    move v1, v4

    .line 164
    goto :goto_0

    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "nRemainCount":I
    :cond_0
    move v1, v4

    .line 167
    goto :goto_0
.end method

.method public static getRemainTime(II)I
    .locals 8
    .param p0, "storage"    # I
    .param p1, "bitrate"    # I

    .prologue
    const/4 v4, -0x1

    .line 171
    invoke-static {p0}, Lcom/sec/android/app/bcocr/CheckMemory;->getAvailableStorage(I)J

    move-result-wide v2

    .line 173
    .local v2, "lAvailableStorage":J
    const-wide/16 v6, -0x2

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    .line 174
    const/4 v1, 0x0

    .line 176
    .local v1, "nRemainTime":I
    int-to-long v6, p1

    :try_start_0
    div-long v4, v2, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    long-to-int v1, v4

    .line 182
    .end local v1    # "nRemainTime":I
    :goto_0
    return v1

    .line 178
    .restart local v1    # "nRemainTime":I
    :catch_0
    move-exception v0

    .local v0, "ex":Ljava/lang/Exception;
    move v1, v4

    .line 179
    goto :goto_0

    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "nRemainTime":I
    :cond_0
    move v1, v4

    .line 182
    goto :goto_0
.end method

.method public static getTotalStorage(I)J
    .locals 8
    .param p0, "storage"    # I

    .prologue
    const/4 v3, 0x1

    .line 212
    const/4 v2, 0x0

    .line 213
    .local v2, "storageDirectory":Ljava/lang/String;
    if-ne p0, v3, :cond_1

    .line 214
    :try_start_0
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v3, v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 215
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 223
    :goto_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 224
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v6, v3

    mul-long/2addr v4, v6

    const-wide/32 v6, 0xa00000

    sub-long/2addr v4, v6

    .line 230
    .end local v1    # "stat":Landroid/os/StatFs;
    :goto_1
    return-wide v4

    .line 217
    :cond_0
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 219
    goto :goto_0

    .line 220
    :cond_1
    sget-object v3, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 229
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "CheckMemory"

    const-string v4, "cannot stat the filesystem then we don\'t know how many free bytes exist"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const-wide/16 v4, -0x2

    goto :goto_1
.end method

.method public static isStorageMounted()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 235
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v1, v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    .line 236
    sget-object v1, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    sget-object v2, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v0

    .line 236
    :cond_1
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    sget-object v1, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 238
    :cond_2
    sget-object v1, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    sget-object v2, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    sget-object v2, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v0, v2, v0

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static setStorageVolume(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    sput-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 148
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    .line 149
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->createName(J)Ljava/lang/String;

    .line 150
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v0, v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 151
    const-string v0, "CheckMemory"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: INTERNAL_SD is not available. mStorageVolumes.length = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    return-void
.end method

.method public static setStorageVolume(Lcom/sec/android/app/bcocr/AbstractOCRActivity;)V
    .locals 3
    .param p0, "activityContext"    # Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    .prologue
    .line 138
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    sput-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 139
    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    .line 140
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->createName(J)Ljava/lang/String;

    .line 141
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->INTERNAL_SD:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v0, v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 142
    const-string v0, "CheckMemory"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: INTERNAL_SD is not available. mStorageVolumes.length = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/bcocr/CheckMemory;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_0
    return-void
.end method
