.class Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;
.super Ljava/lang/Object;
.source "TextToSpeechAssist.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/TextToSpeechAssist;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 210
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 211
    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    .line 212
    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$3(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/bcocr/TextToSpeechAssist$3;->this$0:Lcom/sec/android/app/bcocr/TextToSpeechAssist;

    # getter for: Lcom/sec/android/app/bcocr/TextToSpeechAssist;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/TextToSpeechAssist;->access$3(Lcom/sec/android/app/bcocr/TextToSpeechAssist;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 217
    :cond_1
    return-void
.end method
