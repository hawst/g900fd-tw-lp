.class public final Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;
.super Ljava/lang/Object;
.source "OCREngine.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$ErrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ErrorCallback"
.end annotation


# static fields
.field private static final CAMERA_ERROR_MSG_NO_ERROR:I = 0x0

.field private static final CAMERA_ERROR_WRONG_FW:I = 0x3e8


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILcom/sec/android/seccamera/SecCamera;)V
    .locals 4
    .param p1, "error"    # I
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 497
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ErrorCallback.onError ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 502
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->closeCamera()V

    .line 544
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 506
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 510
    sparse-switch p1, :sswitch_data_0

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    if-eqz v1, :cond_0

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    invoke-interface {v1, p1}, Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;->onErrorCallback(I)V

    goto :goto_0

    .line 514
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 515
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onError : CAMERA_ERROR_WRONG_FW ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-boolean v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mInformedAboutFirmwareVersion:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 516
    const-string v3, "] IMEI:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 515
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-boolean v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mInformedAboutFirmwareVersion:Z

    if-nez v1, :cond_2

    .line 518
    const-string v1, "357858010034783"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 519
    :cond_2
    const-string v1, "004400152020002"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 520
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/4 v2, -0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->finishOnError(I)V

    .line 521
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/bcocr/OCREngine;->mInformedAboutFirmwareVersion:Z

    goto :goto_0

    .line 523
    :cond_4
    const-string v1, "OCREngine"

    const-string v2, "onError : CAMERA_ERROR_WRONG_FW"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 527
    .end local v0    # "tm":Landroid/telephony/TelephonyManager;
    :sswitch_2
    const-string v1, "OCREngine"

    const-string v2, "!!!Camera retry!!! - start!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    if-eqz v1, :cond_5

    .line 529
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOnErrorCallbackListener:Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;

    invoke-interface {v1, p1}, Lcom/sec/android/app/bcocr/OCREngine$OnErrorCallbackListener;->onErrorCallback(I)V

    .line 532
    :cond_5
    const-string v1, "OCREngine"

    const-string v2, "!!!Camera retry!!! before return"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 535
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ErrorCallback;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/4 v2, -0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->finishOnError(I)V

    goto/16 :goto_0

    .line 510
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_3
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_2
    .end sparse-switch
.end method
