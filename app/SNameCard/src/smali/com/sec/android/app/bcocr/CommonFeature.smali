.class public Lcom/sec/android/app/bcocr/CommonFeature;
.super Ljava/lang/Object;
.source "CommonFeature.java"


# static fields
.field public static ACTIVITY_CLASS_NAME_GALLERY3D:Ljava/lang/String;

.field public static ADVANCED_FOCUS_BLUR_THRESHOLD_CAPTUREMODE:I

.field public static ADVANCED_FOCUS_BLUR_THRESHOLD_PREVIEWMODE:I

.field public static ADVANCED_FOCUS_DIS:Z

.field public static ADVANCED_FOCUS_FLAT_THRESHOLD_CAPTUREMODE:I

.field public static ADVANCED_FOCUS_FLAT_THRESHOLD_PREVIEWMODE:I

.field public static ADVANCED_FOCUS_RESTART_DELAY_TIME:J

.field public static ADVANCED_FOCUS_RESTART_DELAY_TIME_CAPTUREMODE:J

.field public static ADVANCED_FOCUS_RESTART_TIME:J

.field public static ADVANCED_FOCUS_UNRECOG_TIME:J

.field public static AMOLED_DISPLAY:Z

.field public static BACK_CAMERA_DEFAULT_ZOOM_VALUE:I

.field public static BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

.field public static BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

.field public static BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

.field public static CAMERA_BACK_KEY:Z

.field public static CAMERA_CONTINUOUS_AF:Z

.field public static CAMERA_ENABLE_STATUS_BAR:Z

.field public static CAMERA_FIXED_CENTER_TOUCH_AF:Z

.field public static CAMERA_FIX_PREVIEWFPSRANGE:Z

.field public static CAMERA_FLASH:Z

.field public static CAMERA_FLASH_LIMITATION_IN_LOW_TEMP:Z

.field public static CAMERA_FOCUS:Z

.field public static CAMERA_FOCUS_ADVANCED_AUTO:Z

.field public static CAMERA_FOCUS_INFINITY:Z

.field public static CAMERA_FOCUS_SET_AREA_FOR_TOUCH_FOCUS:Z

.field public static CAMERA_FOCUS_SUPPORT_CAF_UP:Z

.field public static CAMERA_HALF_SHUTTER:Z

.field public static CAMERA_HARD_KEY:Z

.field public static CAMERA_HAS_KEYPAD:Z

.field public static CAMERA_HVGA_UI_LAYOUT:Z

.field public static CAMERA_LCD_ORIENTATION:I

.field public static CAMERA_LCD_ORIENTATION_LANDSCAPE:I

.field public static CAMERA_LCD_ORIENTATION_PORTRAIT:I

.field public static CAMERA_LIMITATION_IN_LOW_TEMP:Z

.field public static CAMERA_OVERHEAT_AVAILABLE_TEMP:I

.field public static CAMERA_OVERHEAT_LIMITATION_TEMP:I

.field public static CAMERA_PREVIEW_FPS_MAX:I

.field public static CAMERA_PREVIEW_FPS_MIN:I

.field public static CAMERA_REALIGN_720X480_PREVIEW_LAYOUT_FOR_TABLET:Z

.field public static CAMERA_SHOW_POST_PROGRESS_POPUP_DURING_PANORAMA:Z

.field public static CAMERA_START_ENGINE_SYNC:Z

.field public static CAMERA_SUPPORT_TOUCH_FOCUSMODE:Z

.field public static CAMERA_TOUCH_AF:Z

.field public static CAMERA_VOLUME_KEY_ZOOM_SUPPORT:Z

.field public static CAMERA_ZOOM_SUPPORT:Z

.field public static CAPTUREMODE_DEFAULT_ZOOM_VALUE:I

.field public static CAPTURE_ANIMATION_SUPPORT:Z

.field public static DEVICE_TABLET:Z

.field public static DROPBOX_DIRECTORY_PATH:Ljava/lang/String;

.field public static DUMP_CAMERA_PARAMETERS:Z

.field public static ENABLE_ENHANCE_SWITCHING_TIME:Z

.field public static IMPROVEMENT_SCREEN_QUALITY:Z

.field public static INSERT_MEDIA_DB_DIRECT:Z

.field public static INTERNAL_SD:Z

.field public static IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

.field public static IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

.field public static IS_VOICE_COMMAND_SUPPORT:Z

.field public static NORMAL_RATIO:D

.field public static OCR_BUSINESSCARD_AUTO_CAPTURE:Z

.field public static OCR_CAMERA_QUALITY:I

.field public static OCR_CAMERA_RESIZE_PREVIEW_SUPPORT:Z

.field public static OCR_CAPTURE_PREPROCESS_IMAGE:Z

.field public static OCR_DB_FORCE_COPY:Z

.field public static OCR_DEBUG_LEVEL_HIGH:Z

.field public static OCR_DETECT_DURATION:I

.field public static OCR_DETECT_DURATION_LONG:I

.field public static OCR_DIALOG_DEVICE_DEFAULT_THEME:I

.field public static OCR_EXTERNAL_STORAGE_NOT_SUPPORT:Z

.field public static final OCR_FEATURE_HOVERING:Z

.field public static OCR_FEATURE_NAME:Ljava/lang/String;

.field public static OCR_LIGHTMODE_RESTORE:Z

.field public static OCR_OPTION_MENU_LINKING_SETTINGS:Z

.field public static OCR_POPUP_TITLE_COLOR:Ljava/lang/String;

.field public static OCR_POPUP_TITLE_EDIT_COLOR:Ljava/lang/String;

.field public static OCR_SUPPORT_MULTI_THREAD_TO_RECOGNIZE:Z

.field public static OCR_SUPPORT_SENTENCE_TRANSLATE_MODE:Z

.field public static OCR_SUPPORT_VOLUME_CONTROL_FOR_SPEAK:Z

.field public static OCR_USE_MENU_HARD_KEY:Z

.field public static OCR_USE_SAVING_OPTION:Z

.field public static OCR_WIDE_CAPTURE_SUPPORT:Z

.field public static PACKAGE_NAME_GALLERY3D:Ljava/lang/String;

.field public static PACKAGE_NAME_SVOICE:Ljava/lang/String;

.field public static PANORAMA_RESOLUTION:Ljava/lang/String;

.field public static PRELOAD_DB:Z

.field public static PREVIEW_DUPLICATION_DETECTED_INDICATOR:Z

.field public static REALTIMEMODE_DEFAULT_ZOOM_VALUE:I

.field public static SCREEN_RESOLUTION:Ljava/lang/String;

.field public static SUPPORT_CAMERA_CSC_FEATURE:Z

.field public static SUPPORT_CAMERA_FIRMWARE:Z

.field public static SUPPORT_EXPLAIN_POPUP:Z

.field public static SUPPORT_OUTDOOR_VISIBILITY:Z

.field public static TABLET_SIDEMENU:Z

.field public static USE_CAMERA_FOCUS_ADVANCED:Z

.field public static USE_CHECK_SAMSUNG_TEXT:Z

.field public static USE_PREVIEW_RECOG_FAST_MODE:Z

.field public static USE_SAVE_DETECTED_TEXT_FOR_TEST:Z

.field public static WIDE_SCREEN_RATIO:D


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const/16 v5, 0x50

    const/16 v4, 0x32

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->SUPPORT_CAMERA_FIRMWARE:Z

    .line 30
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_BACK_KEY:Z

    .line 31
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_HALF_SHUTTER:Z

    .line 32
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_HARD_KEY:Z

    .line 33
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_HAS_KEYPAD:Z

    .line 36
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FLASH:Z

    .line 39
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_LIGHTMODE_RESTORE:Z

    .line 42
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FLASH_LIMITATION_IN_LOW_TEMP:Z

    .line 45
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_LIMITATION_IN_LOW_TEMP:Z

    .line 48
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FOCUS:Z

    .line 49
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FOCUS_INFINITY:Z

    .line 52
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->USE_CAMERA_FOCUS_ADVANCED:Z

    .line 53
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FOCUS_ADVANCED_AUTO:Z

    .line 54
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FOCUS_SUPPORT_CAF_UP:Z

    .line 55
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FOCUS_SET_AREA_FOR_TOUCH_FOCUS:Z

    .line 56
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_DIS:Z

    .line 57
    const-wide/16 v0, 0x5dc

    sput-wide v0, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_RESTART_TIME:J

    .line 58
    const-wide/16 v0, 0xa

    sput-wide v0, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_UNRECOG_TIME:J

    .line 59
    sput-wide v6, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_RESTART_DELAY_TIME:J

    .line 60
    sput-wide v6, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_RESTART_DELAY_TIME_CAPTUREMODE:J

    .line 61
    sput v5, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_BLUR_THRESHOLD_PREVIEWMODE:I

    .line 62
    sput v5, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_BLUR_THRESHOLD_CAPTUREMODE:I

    .line 63
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_FLAT_THRESHOLD_PREVIEWMODE:I

    .line 64
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->ADVANCED_FOCUS_FLAT_THRESHOLD_CAPTUREMODE:I

    .line 67
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->BACK_CAMERA_PREVIEW_RESOLUTION:Ljava/lang/String;

    .line 68
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->SCREEN_RESOLUTION:Ljava/lang/String;

    .line 69
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->IS_DIFFERENT_RESOLUTION_BETWEEN_LCD_AND_PREVIEW:Z

    .line 70
    const-string v0, "2048x1152"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 71
    const-string v0, "4128x3096"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    .line 73
    sput v2, Lcom/sec/android/app/bcocr/CommonFeature;->BACK_CAMERA_DEFAULT_ZOOM_VALUE:I

    .line 74
    sput v2, Lcom/sec/android/app/bcocr/CommonFeature;->REALTIMEMODE_DEFAULT_ZOOM_VALUE:I

    .line 75
    sput v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAPTUREMODE_DEFAULT_ZOOM_VALUE:I

    .line 78
    const-string v0, "1920x1080"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->PANORAMA_RESOLUTION:Ljava/lang/String;

    .line 81
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FIXED_CENTER_TOUCH_AF:Z

    .line 82
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_TOUCH_AF:Z

    .line 83
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_SUPPORT_TOUCH_FOCUSMODE:Z

    .line 86
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_CONTINUOUS_AF:Z

    .line 89
    const/16 v0, 0x64

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_CAMERA_QUALITY:I

    .line 92
    const/16 v0, 0x3a98

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_PREVIEW_FPS_MIN:I

    .line 93
    const/16 v0, 0x7530

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_PREVIEW_FPS_MAX:I

    .line 96
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_FIX_PREVIEWFPSRANGE:Z

    .line 99
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_ZOOM_SUPPORT:Z

    .line 101
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_VOLUME_KEY_ZOOM_SUPPORT:Z

    .line 104
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_EXTERNAL_STORAGE_NOT_SUPPORT:Z

    .line 106
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAPTURE_ANIMATION_SUPPORT:Z

    .line 109
    const-wide v0, 0x3ff5555555555555L    # 1.3333333333333333

    sput-wide v0, Lcom/sec/android/app/bcocr/CommonFeature;->NORMAL_RATIO:D

    .line 110
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    sput-wide v0, Lcom/sec/android/app/bcocr/CommonFeature;->WIDE_SCREEN_RATIO:D

    .line 113
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->DUMP_CAMERA_PARAMETERS:Z

    .line 114
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_DEBUG_LEVEL_HIGH:Z

    .line 118
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->SUPPORT_CAMERA_CSC_FEATURE:Z

    .line 121
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->TABLET_SIDEMENU:Z

    .line 124
    sput v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_LCD_ORIENTATION_PORTRAIT:I

    .line 125
    sput v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_LCD_ORIENTATION_LANDSCAPE:I

    .line 126
    sget v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_LCD_ORIENTATION_PORTRAIT:I

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_LCD_ORIENTATION:I

    .line 129
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_SHOW_POST_PROGRESS_POPUP_DURING_PANORAMA:Z

    .line 132
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_ENABLE_STATUS_BAR:Z

    .line 135
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_START_ENGINE_SYNC:Z

    .line 138
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_REALIGN_720X480_PREVIEW_LAYOUT_FOR_TABLET:Z

    .line 142
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->IMPROVEMENT_SCREEN_QUALITY:Z

    .line 143
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->SUPPORT_OUTDOOR_VISIBILITY:Z

    .line 144
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->AMOLED_DISPLAY:Z

    .line 145
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->INTERNAL_SD:Z

    .line 146
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->INSERT_MEDIA_DB_DIRECT:Z

    .line 148
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->SUPPORT_EXPLAIN_POPUP:Z

    .line 151
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->ENABLE_ENHANCE_SWITCHING_TIME:Z

    .line 153
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->PREVIEW_DUPLICATION_DETECTED_INDICATOR:Z

    .line 154
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    .line 157
    const-string v0, "com.sec.android.gallery3d"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->PACKAGE_NAME_GALLERY3D:Ljava/lang/String;

    .line 158
    const-string v0, "com.sec.android.gallery3d.app.Gallery"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->ACTIVITY_CLASS_NAME_GALLERY3D:Ljava/lang/String;

    .line 159
    const-string v0, "com.vlingo.midas"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->PACKAGE_NAME_SVOICE:Ljava/lang/String;

    .line 162
    const-string v0, "cloudagent/cache/dropbox"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->DROPBOX_DIRECTORY_PATH:Ljava/lang/String;

    .line 165
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_HVGA_UI_LAYOUT:Z

    .line 168
    const/16 v0, 0x1fe

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_OVERHEAT_LIMITATION_TEMP:I

    .line 169
    const/16 v0, 0x1e0

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->CAMERA_OVERHEAT_AVAILABLE_TEMP:I

    .line 172
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_SUPPORT_MULTI_THREAD_TO_RECOGNIZE:Z

    .line 174
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->USE_CHECK_SAMSUNG_TEXT:Z

    .line 175
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->USE_SAVE_DETECTED_TEXT_FOR_TEST:Z

    .line 178
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_SUPPORT_VOLUME_CONTROL_FOR_SPEAK:Z

    .line 180
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_SUPPORT_SENTENCE_TRANSLATE_MODE:Z

    .line 183
    sput v4, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_DETECT_DURATION:I

    .line 184
    sput v4, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_DETECT_DURATION_LONG:I

    .line 187
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_DIALOG_DEVICE_DEFAULT_THEME:I

    .line 189
    const-string v0, "#F5F5F5"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_POPUP_TITLE_EDIT_COLOR:Ljava/lang/String;

    .line 190
    const-string v0, "#00CCFF"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_POPUP_TITLE_COLOR:Ljava/lang/String;

    .line 193
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_WIDE_CAPTURE_SUPPORT:Z

    .line 195
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_DB_FORCE_COPY:Z

    .line 198
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_OPTION_MENU_LINKING_SETTINGS:Z

    .line 201
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    .line 204
    const-string v0, "UnKnown"

    sput-object v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_FEATURE_NAME:Ljava/lang/String;

    .line 207
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->PRELOAD_DB:Z

    .line 209
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->DEVICE_TABLET:Z

    .line 211
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_CAMERA_RESIZE_PREVIEW_SUPPORT:Z

    .line 213
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_BUSINESSCARD_AUTO_CAPTURE:Z

    .line 215
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_USE_MENU_HARD_KEY:Z

    .line 217
    sput-boolean v2, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_USE_SAVING_OPTION:Z

    .line 219
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->IS_VOICE_COMMAND_LOCALE_SAME_SVOICE:Z

    .line 221
    sput-boolean v3, Lcom/sec/android/app/bcocr/CommonFeature;->IS_VOICE_COMMAND_SUPPORT:Z

    .line 223
    invoke-static {}, Lcom/sec/android/app/bcocr/CommonFeature;->hasHoveringFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/bcocr/CommonFeature;->OCR_FEATURE_HOVERING:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static hasHoveringFeature()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 226
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    .line 227
    .local v1, "pm":Landroid/content/pm/IPackageManager;
    if-nez v1, :cond_0

    .line 233
    :goto_0
    return v2

    .line 231
    :cond_0
    :try_start_0
    const-string v3, "com.sec.feature.hovering_ui"

    invoke-interface {v1, v3}, Landroid/content/pm/IPackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Landroid/os/RemoteException;
    goto :goto_0
.end method
