.class Lcom/sec/android/app/bcocr/OCR$MainHandler;
.super Landroid/os/Handler;
.source "OCR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/OCR;Lcom/sec/android/app/bcocr/OCR$MainHandler;)V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR$MainHandler;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 448
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] handleMessage :: msg.what = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 592
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 453
    :sswitch_1
    const-string v1, "OCR"

    const-string v2, "onLaunchPost #1"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->onLaunchPost()V

    goto :goto_0

    .line 457
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCR;->setCaptureStates(I)V

    .line 458
    const-string v1, "OCR"

    const-string v2, "onLaunchPost #2"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->onLaunchPost()V

    goto :goto_0

    .line 462
    :sswitch_3
    const-string v1, "OCR"

    const-string v2, "[OCR] handleMessage :: wait for finishing thread"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onFinishRecognitionThread()V
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$10(Lcom/sec/android/app/bcocr/OCR;)V

    goto :goto_0

    .line 466
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$11(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$11(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onStart()V

    goto :goto_0

    .line 471
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$11(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mProgressBar:Lcom/sec/android/app/bcocr/ProgressDialogHelper;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$11(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/bcocr/ProgressDialogHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/ProgressDialogHelper;->onStop()V

    goto :goto_0

    .line 476
    :sswitch_6
    const-string v1, "OCR"

    const-string v2, "[OCR] onFocusStateChanged : AF_WAIT_TIMER_EXPIRED"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->cancelTouchAutoFocus()V

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->restartTouchAutoFocus()V

    goto :goto_0

    .line 481
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$12(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator_base:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$13(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mFocus_indicator:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$12(Lcom/sec/android/app/bcocr/OCR;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 485
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->stopAFHideRectTimer()V

    goto/16 :goto_0

    .line 488
    :sswitch_8
    const-string v1, "OCR"

    const-string v2, "[OCR] onFocusStateChanged : START_AUTO_FOCUS"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v1, :cond_3

    .line 490
    const-string v1, "OCR"

    const-string v2, "[OCR] MainHandler(START_AUTO_FOCUS) : mOCREngine is null => skip !!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 494
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    goto/16 :goto_0

    .line 497
    :sswitch_9
    const-string v1, "OCR"

    const-string v2, "[OCR] Force camera restart"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput-boolean v4, v1, Lcom/sec/android/app/bcocr/OCR;->mOCROrientDetectState:Z

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->onReStartPreview()V
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$14(Lcom/sec/android/app/bcocr/OCR;)V

    goto/16 :goto_0

    .line 503
    :sswitch_a
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v1, :cond_4

    .line 504
    const-string v1, "OCR"

    const-string v2, "[OCR] MainHandler(CHECK_ORIENT_EXPIRED) : mOCREngine is null => skip !!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 508
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 510
    .local v0, "nOrientation":I
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[OCR] CHECK_ORIENT_EXPIRED : last orientation : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mOCROrientation:I
    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCR;->access$15(Lcom/sec/android/app/bcocr/OCR;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", current orientation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-static {v1, v4}, Lcom/sec/android/app/bcocr/OCR;->access$16(Lcom/sec/android/app/bcocr/OCR;Z)V

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->setCameraDisplayOrientation()V

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iput-boolean v5, v1, Lcom/sec/android/app/bcocr/OCR;->mOCROrientDetectState:Z

    .line 531
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 532
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V
    invoke-static {v1, v5, v4}, Lcom/sec/android/app/bcocr/OCR;->access$17(Lcom/sec/android/app/bcocr/OCR;ZI)V

    .line 533
    const-string v1, "OCR"

    const-string v2, "[mycheck]CHECK_ORIENT_EXPIRED : preview start complete"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 536
    .end local v0    # "nOrientation":I
    :sswitch_b
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$8(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 537
    const-string v1, "OCR"

    const-string v2, "[mycheck]0. OCR_DETECT_EXPIRED : in"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mOCRPreviewState:Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$18(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 541
    const-string v1, "OCR"

    const-string v2, "[OCR] MainHandler(OCR_DETECT_EXPIRED) : Detect state : false => cancel recognition !!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 545
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-boolean v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCRCapturingState:Z

    if-eqz v1, :cond_7

    .line 546
    const-string v1, "OCR"

    const-string v2, "[OCR] MainHandler(OCR_DETECT_EXPIRED) : Capturing... => cancel recognition !!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 550
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    if-nez v1, :cond_8

    .line 551
    const-string v1, "OCR"

    const-string v2, "[OCR] MainHandler(OCR_DETECT_EXPIRED) : mOCREngine is null => cancel recognition !!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 556
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->isPrevRecogAvailable()Z
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$19(Lcom/sec/android/app/bcocr/OCR;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 558
    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_9

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->stopFocusArrowLayoutAnimation()V
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$20(Lcom/sec/android/app/bcocr/OCR;)V

    .line 564
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/bcocr/OCR;->setOCRActionState(I)V

    .line 565
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCR;->mOCREngine:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->setOCRPreviewDetectStart()V

    .line 567
    const-string v1, "OCR"

    const-string v2, "[OCR] OCR_DETECT_EXPIRED : OCRPreviewDetectStart"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 569
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->startOCRDetectTimer(ZI)V
    invoke-static {v1, v5, v6}, Lcom/sec/android/app/bcocr/OCR;->access$17(Lcom/sec/android/app/bcocr/OCR;ZI)V

    goto/16 :goto_0

    .line 580
    :sswitch_c
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/bcocr/OCR;->OCRVoiceIndicator(IZ)V

    goto/16 :goto_0

    .line 584
    :sswitch_d
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # invokes: Lcom/sec/android/app/bcocr/OCR;->hidePopupWindow()V
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$21(Lcom/sec/android/app/bcocr/OCR;)V

    goto/16 :goto_0

    .line 587
    :sswitch_e
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$MainHandler;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->fitEdgeCueForNameCard()V

    goto/16 :goto_0

    .line 451
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_d
        0x2 -> :sswitch_6
        0x5 -> :sswitch_8
        0x6 -> :sswitch_b
        0x7 -> :sswitch_1
        0x8 -> :sswitch_a
        0x9 -> :sswitch_4
        0xa -> :sswitch_5
        0xb -> :sswitch_9
        0x14 -> :sswitch_3
        0x15 -> :sswitch_2
        0x16 -> :sswitch_e
        0x65 -> :sswitch_c
        0xc8 -> :sswitch_0
        0xca -> :sswitch_0
        0xcf -> :sswitch_0
        0xd0 -> :sswitch_7
    .end sparse-switch
.end method
