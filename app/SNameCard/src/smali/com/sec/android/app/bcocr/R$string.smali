.class public final Lcom/sec/android/app/bcocr/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final DL_BOOKMARK:I = 0x7f0c007d

.field public static final DL_BROWSER:I = 0x7f0c007c

.field public static final DL_CALL:I = 0x7f0c007b

.field public static final DL_COPY:I = 0x7f0c007e

.field public static final Dialog_No:I = 0x7f0c0037

.field public static final Dialog_Yes:I = 0x7f0c0036

.field public static final Title_ChangeMemoryInUseDailog:I = 0x7f0c0038

.field public static final app_application_name:I = 0x7f0c0000

.field public static final app_name:I = 0x7f0c0002

.field public static final battery_low:I = 0x7f0c0045

.field public static final battery_overheat:I = 0x7f0c0005

.field public static final camera_label:I = 0x7f0c004c

.field public static final camera_security_enter:I = 0x7f0c0035

.field public static final cannot_start_camera_msg:I = 0x7f0c000c

.field public static final change_to_card_memory:I = 0x7f0c003c

.field public static final change_to_device_memory:I = 0x7f0c003e

.field public static final change_to_phone_memory:I = 0x7f0c003d

.field public static final check_test_mode_path:I = 0x7f0c0015

.field public static final checkbox_text:I = 0x7f0c0076

.field public static final close:I = 0x7f0c000b

.field public static final close_camera_for_unmounted_memory:I = 0x7f0c0039

.field public static final db_download_root:I = 0x7f0c0011

.field public static final db_install_check_file:I = 0x7f0c0012

.field public static final db_preload_root:I = 0x7f0c0010

.field public static final error_detected_language_is_not_supported:I = 0x7f0c000e

.field public static final gettext_context_web_search:I = 0x7f0c0070

.field public static final gettext_menu_title:I = 0x7f0c006f

.field public static final insert_mmc_msg:I = 0x7f0c003a

.field public static final low_batt_msg:I = 0x7f0c000a

.field public static final low_database_storage_view_text:I = 0x7f0c003f

.field public static final low_internal_storage:I = 0x7f0c0040

.field public static final low_temp_msg:I = 0x7f0c0042

.field public static final manage_dictionary:I = 0x7f0c0078

.field public static final media_server_died_msg:I = 0x7f0c0008

.field public static final no_link_itemrecognized:I = 0x7f0c0073

.field public static final no_text_recognized:I = 0x7f0c0072

.field public static final not_enough_space:I = 0x7f0c003b

.field public static final not_supported_zoom_toast_popup:I = 0x7f0c000d

.field public static final null_string:I = 0x7f0c004b

.field public static final ocr_add_to_bookmark:I = 0x7f0c0055

.field public static final ocr_add_to_contact:I = 0x7f0c0050

.field public static final ocr_auto_capture:I = 0x7f0c001f

.field public static final ocr_call:I = 0x7f0c0052

.field public static final ocr_cancel:I = 0x7f0c001a

.field public static final ocr_change_language:I = 0x7f0c0019

.field public static final ocr_clear:I = 0x7f0c004d

.field public static final ocr_connect_to_wlan:I = 0x7f0c0027

.field public static final ocr_connect_to_wlan_body:I = 0x7f0c0029

.field public static final ocr_connect_via_mobile_network:I = 0x7f0c0026

.field public static final ocr_connect_via_mobile_network_body:I = 0x7f0c0028

.field public static final ocr_cropping:I = 0x7f0c0018

.field public static final ocr_data_path:I = 0x7f0c0013

.field public static final ocr_delete_popup_items_deleted:I = 0x7f0c0064

.field public static final ocr_delete_popup_items_will_be_deleted:I = 0x7f0c0063

.field public static final ocr_dic_chinese_english:I = 0x7f0c00a1

.field public static final ocr_dic_chinese_korean:I = 0x7f0c009f

.field public static final ocr_dic_danish_english:I = 0x7f0c009b

.field public static final ocr_dic_download_more:I = 0x7f0c00bb

.field public static final ocr_dic_dutch_english:I = 0x7f0c009d

.field public static final ocr_dic_english_arabic:I = 0x7f0c00b1

.field public static final ocr_dic_english_chinese:I = 0x7f0c00a0

.field public static final ocr_dic_english_danish:I = 0x7f0c009a

.field public static final ocr_dic_english_dutch:I = 0x7f0c009c

.field public static final ocr_dic_english_english:I = 0x7f0c007f

.field public static final ocr_dic_english_finnish:I = 0x7f0c0096

.field public static final ocr_dic_english_french:I = 0x7f0c0086

.field public static final ocr_dic_english_german:I = 0x7f0c0084

.field public static final ocr_dic_english_greek:I = 0x7f0c00aa

.field public static final ocr_dic_english_hindi:I = 0x7f0c00b3

.field public static final ocr_dic_english_indonesian:I = 0x7f0c0090

.field public static final ocr_dic_english_irish:I = 0x7f0c0092

.field public static final ocr_dic_english_italian:I = 0x7f0c0088

.field public static final ocr_dic_english_japanese:I = 0x7f0c00b8

.field public static final ocr_dic_english_korean:I = 0x7f0c00a2

.field public static final ocr_dic_english_malay:I = 0x7f0c00ae

.field public static final ocr_dic_english_norwegian:I = 0x7f0c0098

.field public static final ocr_dic_english_parsi:I = 0x7f0c00b4

.field public static final ocr_dic_english_polish:I = 0x7f0c008a

.field public static final ocr_dic_english_portuguese:I = 0x7f0c0082

.field public static final ocr_dic_english_russian:I = 0x7f0c008c

.field public static final ocr_dic_english_simplified_chinese:I = 0x7f0c00a7

.field public static final ocr_dic_english_spanish:I = 0x7f0c0080

.field public static final ocr_dic_english_swedish:I = 0x7f0c0094

.field public static final ocr_dic_english_thai:I = 0x7f0c00b0

.field public static final ocr_dic_english_traditional_chinese:I = 0x7f0c00a9

.field public static final ocr_dic_english_turkey:I = 0x7f0c00ad

.field public static final ocr_dic_english_ukrainian:I = 0x7f0c008e

.field public static final ocr_dic_english_urdu:I = 0x7f0c00b5

.field public static final ocr_dic_english_vietnamese:I = 0x7f0c00b2

.field public static final ocr_dic_finnish_english:I = 0x7f0c0097

.field public static final ocr_dic_french_english:I = 0x7f0c0087

.field public static final ocr_dic_german_english:I = 0x7f0c0085

.field public static final ocr_dic_greek_english:I = 0x7f0c00ab

.field public static final ocr_dic_indonesian_english:I = 0x7f0c0091

.field public static final ocr_dic_irish_english:I = 0x7f0c0093

.field public static final ocr_dic_italian_english:I = 0x7f0c0089

.field public static final ocr_dic_japanese_english:I = 0x7f0c00ba

.field public static final ocr_dic_japanese_korean:I = 0x7f0c00b9

.field public static final ocr_dic_korean_chinese:I = 0x7f0c009e

.field public static final ocr_dic_korean_english:I = 0x7f0c00a3

.field public static final ocr_dic_korean_japanese:I = 0x7f0c00b6

.field public static final ocr_dic_korean_korean:I = 0x7f0c00b7

.field public static final ocr_dic_korean_simplified_chinese:I = 0x7f0c00a5

.field public static final ocr_dic_malay_english:I = 0x7f0c00af

.field public static final ocr_dic_norwegian_english:I = 0x7f0c0099

.field public static final ocr_dic_polish_english:I = 0x7f0c008b

.field public static final ocr_dic_portuguese_english:I = 0x7f0c0083

.field public static final ocr_dic_russian_english:I = 0x7f0c008d

.field public static final ocr_dic_simplified_chinese_english:I = 0x7f0c00a6

.field public static final ocr_dic_simplified_chinese_korean:I = 0x7f0c00a4

.field public static final ocr_dic_spanish_english:I = 0x7f0c0081

.field public static final ocr_dic_swedish_english:I = 0x7f0c0095

.field public static final ocr_dic_traditional_chinese_english:I = 0x7f0c00a8

.field public static final ocr_dic_turkey_english:I = 0x7f0c00ac

.field public static final ocr_dic_ukrainian_english:I = 0x7f0c008f

.field public static final ocr_do_not_show_again:I = 0x7f0c0025

.field public static final ocr_done:I = 0x7f0c001c

.field public static final ocr_history_context_menu:I = 0x7f0c0060

.field public static final ocr_history_directlink:I = 0x7f0c005a

.field public static final ocr_history_menu_clear_history:I = 0x7f0c005f

.field public static final ocr_history_menu_delete:I = 0x7f0c005e

.field public static final ocr_history_menu_search:I = 0x7f0c005d

.field public static final ocr_history_menu_selectall:I = 0x7f0c0077

.field public static final ocr_history_textsearch:I = 0x7f0c005b

.field public static final ocr_history_translator:I = 0x7f0c005c

.field public static final ocr_image:I = 0x7f0c0058

.field public static final ocr_label:I = 0x7f0c0001

.field public static final ocr_lang_afrikaans:I = 0x7f0c00de

.field public static final ocr_lang_albanian:I = 0x7f0c00eb

.field public static final ocr_lang_arabic:I = 0x7f0c00be

.field public static final ocr_lang_basque:I = 0x7f0c00e2

.field public static final ocr_lang_bulgarian:I = 0x7f0c00df

.field public static final ocr_lang_catalan:I = 0x7f0c00e0

.field public static final ocr_lang_chinese_sim:I = 0x7f0c00bf

.field public static final ocr_lang_chinese_tra:I = 0x7f0c00c0

.field public static final ocr_lang_croatian:I = 0x7f0c00c1

.field public static final ocr_lang_czech:I = 0x7f0c00c2

.field public static final ocr_lang_danish:I = 0x7f0c00c3

.field public static final ocr_lang_dutch:I = 0x7f0c00c4

.field public static final ocr_lang_english_uk:I = 0x7f0c00bd

.field public static final ocr_lang_english_us:I = 0x7f0c00bc

.field public static final ocr_lang_estonian:I = 0x7f0c00e1

.field public static final ocr_lang_finnish:I = 0x7f0c00c5

.field public static final ocr_lang_french:I = 0x7f0c00c6

.field public static final ocr_lang_german:I = 0x7f0c00c7

.field public static final ocr_lang_greek:I = 0x7f0c00c8

.field public static final ocr_lang_hindi:I = 0x7f0c00c9

.field public static final ocr_lang_hungarian:I = 0x7f0c00e3

.field public static final ocr_lang_icelandic:I = 0x7f0c00e4

.field public static final ocr_lang_indonesia:I = 0x7f0c00ca

.field public static final ocr_lang_irish:I = 0x7f0c00cc

.field public static final ocr_lang_italian:I = 0x7f0c00cb

.field public static final ocr_lang_japanese:I = 0x7f0c00cd

.field public static final ocr_lang_korean:I = 0x7f0c00ce

.field public static final ocr_lang_latvian:I = 0x7f0c00e6

.field public static final ocr_lang_lithuanian:I = 0x7f0c00e5

.field public static final ocr_lang_macedonian:I = 0x7f0c00e7

.field public static final ocr_lang_malay:I = 0x7f0c00cf

.field public static final ocr_lang_norwegian:I = 0x7f0c00d0

.field public static final ocr_lang_polish:I = 0x7f0c00d1

.field public static final ocr_lang_portuguese:I = 0x7f0c00d2

.field public static final ocr_lang_portuguese_bra:I = 0x7f0c00d3

.field public static final ocr_lang_romanian:I = 0x7f0c00e8

.field public static final ocr_lang_russian:I = 0x7f0c00d4

.field public static final ocr_lang_serbian:I = 0x7f0c00ec

.field public static final ocr_lang_slovak:I = 0x7f0c00e9

.field public static final ocr_lang_slovenian:I = 0x7f0c00ea

.field public static final ocr_lang_spanish:I = 0x7f0c00d5

.field public static final ocr_lang_spanish_ratin:I = 0x7f0c00d6

.field public static final ocr_lang_spanish_ratin_america:I = 0x7f0c00d7

.field public static final ocr_lang_swedish:I = 0x7f0c00d8

.field public static final ocr_lang_thai:I = 0x7f0c00d9

.field public static final ocr_lang_turkish:I = 0x7f0c00da

.field public static final ocr_lang_ukrainian:I = 0x7f0c00db

.field public static final ocr_lang_urdu:I = 0x7f0c00dc

.field public static final ocr_lang_vietnamese:I = 0x7f0c00dd

.field public static final ocr_langauge_setting_entry_msg:I = 0x7f0c002f

.field public static final ocr_language_setting_maximum_limit:I = 0x7f0c0062

.field public static final ocr_language_setting_minimum_limit:I = 0x7f0c000f

.field public static final ocr_last_7_days:I = 0x7f0c0057

.field public static final ocr_license_path:I = 0x7f0c0014

.field public static final ocr_no_translator_history_description:I = 0x7f0c0061

.field public static final ocr_normalcapture:I = 0x7f0c0049

.field public static final ocr_nothing_recognized:I = 0x7f0c0034

.field public static final ocr_off:I = 0x7f0c001e

.field public static final ocr_ok:I = 0x7f0c001b

.field public static final ocr_on:I = 0x7f0c001d

.field public static final ocr_open_url:I = 0x7f0c0054

.field public static final ocr_recognizing:I = 0x7f0c0017

.field public static final ocr_send_email:I = 0x7f0c004f

.field public static final ocr_send_message:I = 0x7f0c0053

.field public static final ocr_settings_do_not_save_images:I = 0x7f0c0031

.field public static final ocr_settings_image_auto_save:I = 0x7f0c0066

.field public static final ocr_settings_language:I = 0x7f0c0068

.field public static final ocr_settings_language_settings:I = 0x7f0c00ed

.field public static final ocr_settings_loadimage:I = 0x7f0c002c

.field public static final ocr_settings_manage_dictionaries:I = 0x7f0c0067

.field public static final ocr_settings_save_captured_images:I = 0x7f0c0030

.field public static final ocr_settings_save_option:I = 0x7f0c002d

.field public static final ocr_settings_set_target_language:I = 0x7f0c002e

.field public static final ocr_settings_title:I = 0x7f0c0065

.field public static final ocr_settings_update:I = 0x7f0c0032

.field public static final ocr_share:I = 0x7f0c0051

.field public static final ocr_share_via:I = 0x7f0c004e

.field public static final ocr_show_image:I = 0x7f0c0056

.field public static final ocr_text:I = 0x7f0c0059

.field public static final ocr_the_image_is_too_large:I = 0x7f0c0020

.field public static final ocr_tts_preview_capture:I = 0x7f0c0047

.field public static final ocr_tts_preview_flash:I = 0x7f0c0046

.field public static final ocr_tts_preview_more_menu:I = 0x7f0c0048

.field public static final ocr_tts_preview_voice:I = 0x7f0c0016

.field public static final ocr_unsupported_image_format:I = 0x7f0c0021

.field public static final ocr_update_connecting_to_server:I = 0x7f0c0024

.field public static final ocr_update_latest_version:I = 0x7f0c0023

.field public static final ocr_update_unable_network:I = 0x7f0c0033

.field public static final open_hw_failed_msg:I = 0x7f0c0007

.field public static final plugged_low_batt_msg:I = 0x7f0c0041

.field public static final postview_found:I = 0x7f0c006b

.field public static final postview_menu_get_text:I = 0x7f0c006d

.field public static final postview_menu_history:I = 0x7f0c006e

.field public static final postview_menu_screen_crop:I = 0x7f0c006c

.field public static final postview_menu_tutorials:I = 0x7f0c0079

.field public static final postview_nomatches:I = 0x7f0c0071

.field public static final postview_number_of_links:I = 0x7f0c0069

.field public static final postview_number_of_words:I = 0x7f0c006a

.field public static final preview_captureandshoot_guide:I = 0x7f0c002b

.field public static final preview_default_popup:I = 0x7f0c0074

.field public static final preview_voicecommands_guide:I = 0x7f0c002a

.field public static final stms_appgroup:I = 0x7f0c004a

.field public static final storage_setting_dialog_message:I = 0x7f0c0044

.field public static final storage_setting_dialog_title:I = 0x7f0c0043

.field public static final temperature_too_high_flash_off:I = 0x7f0c0006

.field public static final text_finder:I = 0x7f0c007a

.field public static final traslate_default_popup:I = 0x7f0c0075

.field public static final unable_launch_camera_while_sidesync:I = 0x7f0c0022

.field public static final unknown_error_callback_msg:I = 0x7f0c0009

.field public static final wait:I = 0x7f0c0003

.field public static final warning_msg:I = 0x7f0c0004


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
