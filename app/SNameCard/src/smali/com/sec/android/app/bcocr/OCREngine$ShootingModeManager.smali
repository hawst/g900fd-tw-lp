.class Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;
.super Ljava/lang/Object;
.source "OCREngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCREngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShootingModeManager"
.end annotation


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentShootingMode:I

.field public mStorage:I

.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCREngine;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;)V
    .locals 1

    .prologue
    .line 2102
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 2104
    iget-object v0, p1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mCurrentShootingMode:I

    .line 2105
    iget-object v0, p1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCRSettings;->getStorage()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mStorage:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/OCREngine;Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)V
    .locals 0

    .prologue
    .line 2102
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;-><init>(Lcom/sec/android/app/bcocr/OCREngine;)V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[BLcom/sec/android/seccamera/SecCamera;)V
    .locals 0

    .prologue
    .line 2133
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->startSavePicture([BLcom/sec/android/seccamera/SecCamera;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)Z
    .locals 1

    .prologue
    .line 2164
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->ocrRecognizeCapturedImage([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)Z
    .locals 1

    .prologue
    .line 2264
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->storeImage([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 2103
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;)Lcom/sec/android/app/bcocr/OCREngine;
    .locals 1

    .prologue
    .line 2102
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    return-object v0
.end method

.method private declared-synchronized ocrRecognizeCapturedImage([B)Z
    .locals 16
    .param p1, "jpegData"    # [B

    .prologue
    .line 2165
    monitor-enter p0

    :try_start_0
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage()]"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2167
    const/4 v15, 0x1

    .line 2169
    .local v15, "result":Z
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 2171
    if-nez p1, :cond_0

    .line 2172
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage() : jpegData is null !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2173
    const/4 v1, 0x0

    .line 2260
    :goto_0
    monitor-exit p0

    return v1

    .line 2176
    :cond_0
    :try_start_1
    new-instance v9, Lcom/sec/android/app/bcocr/DecodeImageUtils;

    invoke-direct {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;-><init>()V

    .line 2177
    .local v9, "decodeImage":Lcom/sec/android/app/bcocr/DecodeImageUtils;
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->decodeByteArrayToBitmap([B)Z

    move-result v15

    .line 2179
    if-eqz v15, :cond_1

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2180
    :cond_1
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage() : bitmap is null !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2181
    const/4 v1, 0x0

    goto :goto_0

    .line 2184
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCREngine;->getOrientationOnTake()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine;->convertToExifInterfaceOrientation(I)I

    move-result v14

    .line 2185
    .local v14, "orientationForExif":I
    sput v14, Lcom/sec/android/app/bcocr/OCREngine;->mOrientForExif:I

    .line 2187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getOrientationOnTake()I

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->setRotation(I)V

    .line 2188
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->rotateBitmap()Z

    move-result v15

    .line 2190
    if-eqz v15, :cond_5

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2191
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sput v1, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    .line 2192
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sput v1, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    .line 2193
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->getRotateBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    .line 2201
    if-eqz v15, :cond_3

    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    .line 2203
    :try_start_2
    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    sget v2, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    mul-int/2addr v1, v2

    new-array v1, v1, [I

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2232
    :cond_3
    :goto_1
    if-eqz v15, :cond_8

    :try_start_3
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_8

    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_8

    .line 2234
    :try_start_4
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    sget-object v2, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    const/4 v3, 0x0

    sget v4, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget v7, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    sget v8, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2250
    :goto_2
    :try_start_5
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2251
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    .line 2253
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 2254
    const/4 v9, 0x0

    .line 2256
    if-nez v15, :cond_4

    .line 2257
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    :cond_4
    move v1, v15

    .line 2260
    goto/16 :goto_0

    .line 2195
    :cond_5
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage() : rotation is failed !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2196
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 2197
    const/4 v9, 0x0

    .line 2198
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2204
    :catch_0
    move-exception v10

    .line 2205
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage() : myImageData] Out of memory(1) ! <= retry!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2207
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    if-eqz v1, :cond_6

    .line 2208
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 2211
    :cond_6
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2214
    :try_start_6
    sget v1, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    sget v2, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    mul-int/2addr v1, v2

    new-array v1, v1, [I

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 2215
    const-string v1, "OCREngine"

    const-string v2, "[getOCRWholeDataFromImage() : myImageData] Out of memory(2) !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 2216
    :catch_1
    move-exception v13

    .line 2217
    .local v13, "er":Ljava/lang/OutOfMemoryError;
    :try_start_7
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 2218
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_7

    .line 2219
    const-string v1, "OCREngine"

    const-string v2, "OCR_CAPTURE_PREPROCESS_IMAGE mImageBmp recycled 1"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    sget-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2221
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    .line 2223
    :cond_7
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    .line 2224
    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->recycle()V

    .line 2225
    const/4 v9, 0x0

    .line 2226
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2227
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2235
    .end local v10    # "e":Ljava/lang/OutOfMemoryError;
    .end local v13    # "er":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v11

    .line 2236
    .local v11, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 2237
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[ocrRecognizeCapturedImage()] IllegalArgumentException: x + width("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2238
    sget v3, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") must be <= bitmap.width():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2237
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2239
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2240
    .end local v11    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v12

    .line 2241
    .local v12, "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v12}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 2242
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage()] Error ArrayIndexOutOfBoundsException !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2243
    const/4 v15, 0x0

    .line 2245
    goto/16 :goto_2

    .line 2246
    .end local v12    # "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_8
    const-string v1, "OCREngine"

    const-string v2, "[ocrRecognizeCapturedImage()] mBmpStill == null !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2247
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 2165
    .end local v9    # "decodeImage":Lcom/sec/android/app/bcocr/DecodeImageUtils;
    .end local v14    # "orientationForExif":I
    .end local v15    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private final startSavePicture([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 3
    .param p1, "jpegData"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 2134
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager$1;-><init>(Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;[B)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v0, v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$7(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/Thread;)V

    .line 2160
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$8(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "pictureSavingThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2161
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mPictureSavingThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$8(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2162
    return-void
.end method

.method private declared-synchronized storeImage([B)Z
    .locals 30
    .param p1, "data"    # [B

    .prologue
    .line 2265
    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 2266
    .local v10, "DontSave":Ljava/lang/Boolean;
    const/4 v2, 0x0

    .line 2268
    .local v2, "resultImage":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v23

    .line 2269
    .local v23, "preferences":Landroid/content/SharedPreferences;
    const-string v8, "setting_image_auto_save"

    .line 2270
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_USE_SAVING_OPTION:Z

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    .line 2269
    :goto_0
    move-object/from16 v0, v23

    invoke-interface {v0, v8, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 2270
    if-nez v3, :cond_0

    .line 2271
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 2274
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    if-eqz v3, :cond_1

    .line 2275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/CaptureData;->clear()V

    .line 2276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v8, 0x0

    iput-object v8, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 2279
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_2

    .line 2281
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    new-instance v8, Lcom/sec/android/app/bcocr/CaptureData;

    invoke-direct {v8}, Lcom/sec/android/app/bcocr/CaptureData;-><init>()V

    iput-object v8, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 2282
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 2283
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v9, v9, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v9}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v9

    invoke-static {v9}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v9

    mul-int/2addr v8, v9

    int-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v8, v8

    .line 2284
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget v9, v9, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderWidth:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mOriginalViewFinderHeight:I

    move/from16 v28, v0

    mul-int v9, v9, v28

    int-to-double v0, v9

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v9, v0

    .line 2282
    invoke-static {v8, v9}, Lcom/sec/android/app/bcocr/Util;->calculateSampleSize(II)I

    move-result v8

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/app/bcocr/CaptureData;->setCaptureData([BI)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2295
    :cond_2
    :goto_1
    :try_start_2
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getnOCRNameCardCaptureModeType()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2296
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v3

    const/4 v8, 0x2

    if-eq v3, v8, :cond_4

    .line 2297
    sget v3, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    sget v8, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2298
    sget-object v3, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_e

    .line 2300
    :try_start_3
    sget-object v3, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget v8, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    sget v9, Lcom/sec/android/app/bcocr/OCREngine;->mImgHeight:I

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2315
    :goto_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getOrientationOnTake()I

    move-result v3

    if-eqz v3, :cond_3

    .line 2316
    new-instance v3, Lcom/sec/android/app/bcocr/DecodeImageUtils;

    invoke-direct {v3}, Lcom/sec/android/app/bcocr/DecodeImageUtils;-><init>()V

    .line 2317
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCREngine;->getOrientationOnTake()I

    move-result v8

    rsub-int v8, v8, 0x168

    .line 2316
    invoke-virtual {v3, v2, v8}, Lcom/sec/android/app/bcocr/DecodeImageUtils;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2320
    :cond_3
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    if-nez v3, :cond_4

    sget-object v3, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I

    if-eqz v3, :cond_4

    .line 2321
    const/4 v3, 0x0

    sput-object v3, Lcom/sec/android/app/bcocr/OCREngine;->myImageData:[I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2325
    :cond_4
    const/4 v11, 0x1

    .line 2328
    .local v11, "bSuccess":Z
    :try_start_5
    new-instance v12, Ljava/util/GregorianCalendar;

    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V

    .line 2329
    .local v12, "calendar":Ljava/util/GregorianCalendar;
    new-instance v25, Landroid/text/format/Time;

    invoke-direct/range {v25 .. v25}, Landroid/text/format/Time;-><init>()V

    .line 2330
    .local v25, "time":Landroid/text/format/Time;
    move-object/from16 v0, v25

    iget-object v3, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v26

    .line 2331
    .local v26, "timezone":Ljava/util/TimeZone;
    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2332
    invoke-virtual {v12}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    .line 2334
    .local v6, "dateTaken":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iput-wide v6, v3, Lcom/sec/android/app/bcocr/OCREngine;->mDateTaken:J

    .line 2337
    invoke-static {v6, v7}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->createName(J)Ljava/lang/String;

    move-result-object v20

    .line 2339
    .local v20, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$9(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V

    .line 2343
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ".jpg"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2348
    .local v5, "filename":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mStorage:I

    if-nez v3, :cond_f

    .line 2349
    sget-object v4, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 2354
    .local v4, "directory":Ljava/lang/String;
    :goto_3
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2355
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/.OCRTemp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 2356
    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "OCRTemp.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2355
    invoke-static {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$10(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V

    .line 2357
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/.OCRTemp"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2358
    const-string v5, "OCRTemp.jpg"

    .line 2359
    const-string v20, "OCRTemp.jpg"

    .line 2360
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const-string v8, "OCRTemp.jpg"

    invoke-static {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$9(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V

    .line 2366
    :goto_4
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2367
    .local v16, "f":Ljava/io/File;
    const/16 v18, 0x0

    .local v18, "filenumber":I
    move/from16 v19, v18

    .line 2369
    .end local v18    # "filenumber":I
    .local v19, "filenumber":I
    :goto_5
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2378
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->getOrientationOnTake()I

    move-result v22

    .line 2380
    .local v22, "orientationForPicture":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    if-eqz v3, :cond_6

    .line 2381
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/sec/android/app/bcocr/CaptureData;->setOrientation(I)V

    .line 2384
    :cond_6
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->OCR_CAPTURE_PREPROCESS_IMAGE:Z

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->getnOCRNameCardCaptureModeType()Z

    move-result v3

    if-nez v3, :cond_12

    .line 2385
    const/4 v9, 0x0

    move-object v8, v2

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->addImage(Ljava/lang/String;Ljava/lang/String;JLandroid/graphics/Bitmap;[B)Z

    move-result v11

    .line 2390
    :goto_6
    const-string v3, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "storeImage bSuccess: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    if-nez v11, :cond_7

    .line 2393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    if-eqz v3, :cond_7

    .line 2394
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mMainHandler:Lcom/sec/android/app/bcocr/OCREngine$MainHandler;

    const/16 v8, 0x64

    invoke-virtual {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine$MainHandler;->sendEmptyMessage(I)Z

    .line 2398
    :cond_7
    if-eqz v11, :cond_b

    .line 2400
    sget-boolean v3, Lcom/sec/android/app/bcocr/Feature;->INSERT_MEDIA_DB_DIRECT:Z

    if-eqz v3, :cond_13

    .line 2401
    new-instance v27, Landroid/content/ContentValues;

    const/4 v3, 0x7

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 2402
    .local v27, "values":Landroid/content/ContentValues;
    const-string v3, "title"

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407
    const-string v3, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[PreProcessImage] orientationForPicture : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2408
    const-string v3, "_display_name"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2409
    const-string v3, "datetaken"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2410
    const-string v3, "mime_type"

    const-string v8, "image/jpeg"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411
    const-string v3, "orientation"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2412
    const-string v3, "_data"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v3

    const/4 v8, 0x2

    if-eq v3, v8, :cond_8

    .line 2414
    const-string v3, "height"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntHeight(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2415
    const-string v3, "width"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v8, v8, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v8}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraResolution()I

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntWidth(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2418
    :cond_8
    const-string v3, "date_modified"

    const-wide/16 v8, 0x3e8

    div-long v8, v6, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2420
    const/16 v17, 0x0

    .line 2423
    .local v17, "fileUri":Landroid/net/Uri;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v27

    invoke-virtual {v3, v8, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v17

    .line 2426
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2427
    const-string v3, "GATE"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "<GATE-M>PICTURE_TAKEN : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " </GATE-M>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2431
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 2432
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 2431
    move-object/from16 v0, v17

    invoke-static {v3, v0, v8, v9}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->setImageSize(Landroid/content/ContentResolver;Landroid/net/Uri;J)V

    .line 2433
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v0, v17

    iput-object v0, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    .line 2434
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-eqz v3, :cond_a

    .line 2435
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.camera.NEW_PICTURE"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mLastContentUri:Landroid/net/Uri;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-direct {v8, v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v8}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2449
    .end local v17    # "fileUri":Landroid/net/Uri;
    .end local v27    # "values":Landroid/content/ContentValues;
    :cond_a
    :goto_7
    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v8, 0x1

    invoke-static {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$12(Lcom/sec/android/app/bcocr/OCREngine;Z)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2455
    .end local v4    # "directory":Ljava/lang/String;
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "dateTaken":J
    .end local v11    # "bSuccess":Z
    .end local v12    # "calendar":Ljava/util/GregorianCalendar;
    .end local v16    # "f":Ljava/io/File;
    .end local v19    # "filenumber":I
    .end local v20    # "name":Ljava/lang/String;
    .end local v22    # "orientationForPicture":I
    .end local v25    # "time":Landroid/text/format/Time;
    .end local v26    # "timezone":Ljava/util/TimeZone;
    :cond_b
    :goto_8
    monitor-exit p0

    return v11

    .line 2270
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2285
    :catch_0
    move-exception v21

    .line 2286
    .local v21, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_8
    const-string v3, "OCREngine"

    const-string v8, "Out of memory while creating bitmap."

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    if-eqz v3, :cond_d

    .line 2288
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/CaptureData;->clear()V

    .line 2290
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    const/4 v8, 0x0

    iput-object v8, v3, Lcom/sec/android/app/bcocr/OCREngine;->mLastCaptureData:Lcom/sec/android/app/bcocr/CaptureData;

    .line 2291
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 2265
    .end local v2    # "resultImage":Landroid/graphics/Bitmap;
    .end local v10    # "DontSave":Ljava/lang/Boolean;
    .end local v21    # "oom":Ljava/lang/OutOfMemoryError;
    .end local v23    # "preferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 2301
    .restart local v2    # "resultImage":Landroid/graphics/Bitmap;
    .restart local v10    # "DontSave":Ljava/lang/Boolean;
    .restart local v23    # "preferences":Landroid/content/SharedPreferences;
    :catch_1
    move-exception v13

    .line 2302
    .local v13, "e1":Ljava/lang/IllegalArgumentException;
    :try_start_9
    invoke-virtual {v13}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 2303
    const-string v3, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[ocrRecognizeCapturedImage()] IllegalArgumentException: x + width("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2304
    sget v9, Lcom/sec/android/app/bcocr/OCREngine;->mImgWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") must be <= bitmap.width():"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 2305
    sget-object v9, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/bcocr/OCREngine;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2303
    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2306
    const/4 v11, 0x0

    goto :goto_8

    .line 2307
    .end local v13    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v14

    .line 2308
    .local v14, "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v14}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 2309
    const-string v3, "OCREngine"

    const-string v8, "[ocrRecognizeCapturedImage()] Error ArrayIndexOutOfBoundsException !"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2312
    .end local v14    # "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_e
    const-string v3, "OCREngine"

    const-string v8, "[ocrRecognizeCapturedImage()] mBmpStill == null !"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2

    .line 2351
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v6    # "dateTaken":J
    .restart local v11    # "bSuccess":Z
    .restart local v12    # "calendar":Ljava/util/GregorianCalendar;
    .restart local v20    # "name":Ljava/lang/String;
    .restart local v25    # "time":Landroid/text/format/Time;
    .restart local v26    # "timezone":Ljava/util/TimeZone;
    :cond_f
    :try_start_a
    sget-object v4, Lcom/sec/android/app/bcocr/ImageSavingUtils;->CAMERA_IMAGE_BUCKET_NAME_MMC:Ljava/lang/String;

    .restart local v4    # "directory":Ljava/lang/String;
    goto/16 :goto_3

    .line 2362
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$10(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_4

    .line 2451
    .end local v4    # "directory":Ljava/lang/String;
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "dateTaken":J
    .end local v12    # "calendar":Ljava/util/GregorianCalendar;
    .end local v20    # "name":Ljava/lang/String;
    .end local v25    # "time":Landroid/text/format/Time;
    .end local v26    # "timezone":Ljava/util/TimeZone;
    :catch_3
    move-exception v15

    .line 2452
    .local v15, "ex":Ljava/lang/Exception;
    :try_start_b
    const-string v3, "OCREngine"

    const-string v8, "Exception while compressing image."

    invoke-static {v3, v8, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_8

    .line 2370
    .end local v15    # "ex":Ljava/lang/Exception;
    .restart local v4    # "directory":Ljava/lang/String;
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v6    # "dateTaken":J
    .restart local v12    # "calendar":Ljava/util/GregorianCalendar;
    .restart local v16    # "f":Ljava/io/File;
    .restart local v19    # "filenumber":I
    .restart local v20    # "name":Ljava/lang/String;
    .restart local v25    # "time":Landroid/text/format/Time;
    .restart local v26    # "timezone":Ljava/util/TimeZone;
    :cond_11
    :try_start_c
    const-string v3, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Duplicated file name found: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2371
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "("

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v18, v19, 0x1

    .end local v19    # "filenumber":I
    .restart local v18    # "filenumber":I
    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ")"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ".jpg"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2372
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/android/app/bcocr/OCREngine;->access$10(Lcom/sec/android/app/bcocr/OCREngine;Ljava/lang/String;)V

    .line 2374
    const-string v3, "OCREngine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "New file name created: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2375
    new-instance v16, Ljava/io/File;

    .end local v16    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v16    # "f":Ljava/io/File;
    move/from16 v19, v18

    .end local v18    # "filenumber":I
    .restart local v19    # "filenumber":I
    goto/16 :goto_5

    .line 2387
    .restart local v22    # "orientationForPicture":I
    :cond_12
    const/4 v8, 0x0

    move-object/from16 v9, p1

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/bcocr/ImageSavingUtils;->addImage(Ljava/lang/String;Ljava/lang/String;JLandroid/graphics/Bitmap;[B)Z

    move-result v11

    goto/16 :goto_6

    .line 2437
    .restart local v17    # "fileUri":Landroid/net/Uri;
    .restart local v27    # "values":Landroid/content/ContentValues;
    :catch_4
    move-exception v24

    .line 2438
    .local v24, "sfe":Landroid/database/sqlite/SQLiteFullException;
    const-string v3, "OCREngine"

    const-string v8, "Not enough space in database"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2439
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const v8, 0x7f0c003f

    const/4 v9, 0x0

    invoke-static {v3, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 2442
    .end local v17    # "fileUri":Landroid/net/Uri;
    .end local v24    # "sfe":Landroid/database/sqlite/SQLiteFullException;
    .end local v27    # "values":Landroid/content/ContentValues;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    if-eqz v3, :cond_a

    .line 2443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    .line 2444
    new-instance v28, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mLastCapturedFileName:Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/bcocr/OCREngine;->access$11(Lcom/sec/android/app/bcocr/OCREngine;)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v28 .. v28}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-direct {v8, v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2443
    invoke-virtual {v3, v8}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_7
.end method


# virtual methods
.method public handleShutterEvent()V
    .locals 4

    .prologue
    .line 2459
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 2460
    :cond_0
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleShutterEvent - isCapturing:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " current state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2466
    :goto_0
    return-void

    .line 2464
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    .line 2465
    .local v0, "shootingMode":I
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleShutterEvent"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public handleShutterReleaseEvent()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 2469
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleShutterReleaseEvent - mFocusState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mFocusState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2471
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->isCapturing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->isStartingPreview()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2472
    :cond_0
    const-string v1, "OCREngine"

    const-string v2, "handleShutterReleaseEvent returning.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2497
    :goto_0
    return-void

    .line 2476
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v1

    if-eq v1, v4, :cond_2

    .line 2477
    const-string v1, "OCREngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong state for take picture: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v3, v3, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    invoke-virtual {v3}, Lcom/sec/android/app/bcocr/AbstractCeState;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2481
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-nez v1, :cond_4

    .line 2482
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->isAutoFocusing()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->getFocusState()I

    move-result v1

    if-ne v1, v4, :cond_5

    .line 2483
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleAutoFocus()V

    .line 2489
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    .line 2491
    .local v0, "shootingMode":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2493
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/AbstractOCRActivity;->onOrientationLock(Z)V

    .line 2494
    invoke-virtual {p0}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->handleSingleShutterWithNoShutterLag()V

    goto :goto_0

    .line 2484
    .end local v0    # "shootingMode":I
    :cond_5
    sget-boolean v1, Lcom/sec/android/app/bcocr/OCREngine;->m_bRestartTouchAF:Z

    if-eqz v1, :cond_4

    .line 2485
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->restartTouchAF()V

    goto :goto_1

    .line 2491
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public handleSingleShutterWithNoShutterLag()V
    .locals 3

    .prologue
    .line 2503
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mStateDepot:Ljava/util/HashMap;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/bcocr/AbstractCeState;

    iput-object v0, v1, Lcom/sec/android/app/bcocr/OCREngine;->mCurrentState:Lcom/sec/android/app/bcocr/AbstractCeState;

    .line 2504
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleTakePicture()V

    .line 2505
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCREngine;->scheduleStopPreview()V

    .line 2506
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v0, v0, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v0, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/OCR;->pauseAudioPlayback()V

    .line 2507
    return-void
.end method

.method public onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 4
    .param p1, "jpegData"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    const/4 v3, 0x0

    .line 2109
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCRSettings;->getShootingMode()I

    move-result v0

    .line 2111
    .local v0, "shootingMode":I
    packed-switch v0, :pswitch_data_0

    .line 2118
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->unlockAEAWB()V

    .line 2120
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_CONTINUOUS_AF:Z

    if-nez v1, :cond_0

    .line 2121
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->clearFocusState()V

    .line 2122
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCREngine;->updateFocusIndicator()V

    .line 2125
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mActivityContext:Lcom/sec/android/app/bcocr/AbstractOCRActivity;

    check-cast v1, Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/bcocr/OCR;->setTouchAutoFocusActive(Z)V

    .line 2126
    sget-boolean v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_FOCUS:Z

    if-nez v1, :cond_2

    .line 2127
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/bcocr/OCREngine;->setFocusParameter(I)V

    .line 2131
    :cond_1
    :goto_1
    return-void

    .line 2113
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->startSavePicture([BLcom/sec/android/seccamera/SecCamera;)V

    .line 2114
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v1, v1, Lcom/sec/android/app/bcocr/OCREngine;->mStateMessageHandler:Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine$StateMessageHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2128
    :cond_2
    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->m_bIsTouchAutoFocusing:Z
    invoke-static {}, Lcom/sec/android/app/bcocr/OCREngine;->access$4()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2129
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    iget-object v2, v2, Lcom/sec/android/app/bcocr/OCREngine;->mOCRSettings:Lcom/sec/android/app/bcocr/OCRSettings;

    invoke-virtual {v2}, Lcom/sec/android/app/bcocr/OCRSettings;->getCameraFocusMode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/bcocr/OCREngine;->setFocusParameter(I)V

    goto :goto_1

    .line 2111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setShootingMode(I)V
    .locals 4
    .param p1, "shootingMode"    # I

    .prologue
    .line 2510
    iget v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mCurrentShootingMode:I

    .line 2515
    iput p1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->mCurrentShootingMode:I

    .line 2516
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/seccamera/SecCamera;->setShootingMode(I)V

    .line 2517
    iget-object v0, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v0}, Lcom/sec/android/app/bcocr/OCREngine;->access$1(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mShutterCallback:Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCREngine;->access$13(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$ShutterCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mRawPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;
    invoke-static {v2}, Lcom/sec/android/app/bcocr/OCREngine;->access$14(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$RawPictureCallback;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/bcocr/OCREngine$ShootingModeManager;->this$0:Lcom/sec/android/app/bcocr/OCREngine;

    # getter for: Lcom/sec/android/app/bcocr/OCREngine;->mJpegPictureCallback:Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;
    invoke-static {v3}, Lcom/sec/android/app/bcocr/OCREngine;->access$15(Lcom/sec/android/app/bcocr/OCREngine;)Lcom/sec/android/app/bcocr/OCREngine$JpegPictureCallback;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/seccamera/SecCamera;->setShootingModeCallbacks(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V

    .line 2518
    return-void
.end method
