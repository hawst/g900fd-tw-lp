.class Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;
.super Ljava/lang/Object;
.source "OCR.java"

# interfaces
.implements Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/OCR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VoiceRecognizer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/OCR;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bcocr/OCR;)V
    .locals 0

    .prologue
    .line 6067
    iput-object p1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bcocr/OCR;Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;)V
    .locals 0

    .prologue
    .line 6067
    invoke-direct {p0, p1}, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;-><init>(Lcom/sec/android/app/bcocr/OCR;)V

    return-void
.end method


# virtual methods
.method public onResults([Ljava/lang/String;)V
    .locals 7
    .param p1, "strResult"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x1b

    const/4 v4, 0x1

    .line 6069
    const/4 v0, -0x1

    .line 6071
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$22(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6072
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    # getter for: Lcom/sec/android/app/bcocr/OCR;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/OCR;->access$22(Lcom/sec/android/app/bcocr/OCR;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getIntBargeInResult()I

    move-result v0

    .line 6074
    :cond_0
    const-string v1, "OCR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "######VR VoiceRecognition result string : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 6075
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6074
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6077
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 6078
    packed-switch v0, :pswitch_data_0

    .line 6097
    const-string v1, "OCR"

    const-string v2, "###### VoiceRecognizer, VOICE_FAIL "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6098
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/bcocr/OCR;->OCRVoiceIndicator(IZ)V

    .line 6105
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 6084
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->restartInactivityTimer()V

    .line 6085
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 6086
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->isCaptureEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 6087
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/OCR;->processBack()V

    goto :goto_1

    .line 6091
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/app/bcocr/OCR;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 6092
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/app/bcocr/OCR;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 6102
    :cond_3
    const-string v1, "OCR"

    const-string v2, "###### VoiceRecognizer, VOICE_FAIL "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6103
    iget-object v1, p0, Lcom/sec/android/app/bcocr/OCR$VoiceRecognizer;->this$0:Lcom/sec/android/app/bcocr/OCR;

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/bcocr/OCR;->OCRVoiceIndicator(IZ)V

    goto :goto_0

    .line 6078
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
