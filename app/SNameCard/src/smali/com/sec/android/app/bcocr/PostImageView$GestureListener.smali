.class public Lcom/sec/android/app/bcocr/PostImageView$GestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PostImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bcocr/PostImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/PostImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/bcocr/PostImageView;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 281
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/PostImageView;->onDoubleTapPost()F

    move-result v0

    .line 283
    .local v0, "targetScale":F
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    iput v0, v1, Lcom/sec/android/app/bcocr/PostImageView;->mCurrentScaleFactor:F

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/bcocr/PostImageView;->zoomTo(FFF)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/PostImageView;->invalidate()V

    .line 286
    const/4 v1, 0x0

    return v1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 291
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v3, :cond_0

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    # getter for: Lcom/sec/android/app/bcocr/PostImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v1}, Lcom/sec/android/app/bcocr/PostImageView;->access$11(Lcom/sec/android/app/bcocr/PostImageView;)Landroid/view/ScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    # invokes: Lcom/sec/android/app/bcocr/PostImageView;->checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/app/bcocr/PostImageView;->access$12(Lcom/sec/android/app/bcocr/PostImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result p3

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    # invokes: Lcom/sec/android/app/bcocr/PostImageView;->checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    invoke-static {v0, p1, p2, p4}, Lcom/sec/android/app/bcocr/PostImageView;->access$13(Lcom/sec/android/app/bcocr/PostImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result p4

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    neg-float v1, p3

    neg-float v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/bcocr/PostImageView;->scrollBy(FF)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-static {v0, v3}, Lcom/sec/android/app/bcocr/PostImageView;->access$14(Lcom/sec/android/app/bcocr/PostImageView;Z)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/bcocr/PostImageView$GestureListener;->this$0:Lcom/sec/android/app/bcocr/PostImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/bcocr/PostImageView;->invalidate()V

    .line 309
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method
