.class public Lcom/sec/android/app/bcocr/DecodeImageUtils;
.super Lcom/sec/android/app/bcocr/BitmapUtils;
.source "DecodeImageUtils.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "DecodeImageUtils"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/bcocr/BitmapUtils;-><init>(Landroid/graphics/Bitmap;)V

    .line 35
    return-void
.end method

.method public static getImageOrientation(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 9
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 151
    const/4 v7, 0x0

    .line 153
    .local v7, "exifOrientation":I
    if-eqz p0, :cond_1

    .line 154
    const/4 v6, 0x0

    .line 155
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "_data=\""

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 155
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 158
    if-eqz v6, :cond_1

    .line 159
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "orientation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 162
    .local v8, "index2":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 164
    .end local v8    # "index2":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-static {v7}, Lcom/sec/android/app/bcocr/ImageManager;->changeOrienationToExifValue(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public decodeByteArrayToBitmap([B)Z
    .locals 6
    .param p1, "jpeg"    # [B

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 118
    const/4 v3, 0x0

    :try_start_0
    array-length v4, p1

    invoke-static {p1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_2

    .line 147
    :cond_0
    :goto_0
    return v2

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeByteToBitmap] Out of memory !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 127
    iput-object v5, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 128
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeByteToBitmap] mBitmapTemp = null! Retry"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 132
    const/4 v3, 0x0

    :try_start_1
    array-length v4, p1

    invoke-static {p1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v3, :cond_0

    .line 147
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 136
    .restart local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e2":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 138
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeByteToBitmap/retry] Out of memory !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    iput-object v5, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 143
    :cond_3
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public decodeFileToBitmap(Ljava/lang/String;)Z
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 43
    const/4 v3, 0x0

    :try_start_0
    invoke-static {p1, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 44
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_2

    .line 73
    :cond_0
    :goto_0
    return v2

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeFileToBitmap] Out of memory !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 51
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 52
    iput-object v6, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 53
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeFileToBitmap] mBitmapTemp = null! Retry"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 57
    const/4 v3, 0x0

    :try_start_1
    invoke-static {p1, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 58
    const-string v3, "DecodeImageUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file path = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v3, :cond_0

    .line 73
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 62
    .restart local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 63
    .local v1, "e2":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 64
    const-string v3, "DecodeImageUtils"

    const-string v4, "[decodeFileToBitmap/retry] Out of memory !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    iput-object v6, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 69
    :cond_3
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public decodeURIToBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
    .locals 7
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "contentURI"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 78
    :try_start_0
    invoke-static {p1, p2}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v4, :cond_2

    .line 113
    :cond_0
    :goto_0
    return v3

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "DecodeImageUtils"

    const-string v5, "[decodeURIToBitmap] IOException !"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v4, "DecodeImageUtils"

    const-string v5, "[decodeURIToBitmap] Out of memory !"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    .line 89
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 90
    iput-object v6, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 91
    const-string v4, "DecodeImageUtils"

    const-string v5, "[decodeURIToBitmap] mBitmapTemp = null! Retry"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 95
    :try_start_1
    invoke-static {p1, p2}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v4, :cond_0

    .line 113
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 99
    .restart local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v1

    .line 100
    .local v1, "e1":Ljava/io/IOException;
    const-string v4, "DecodeImageUtils"

    const-string v5, "[decodeURIToBitmap/retry] IOException !"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 102
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 103
    .local v2, "e2":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 104
    const-string v4, "DecodeImageUtils"

    const-string v5, "[decodeURIToBitmap/retry] Out of memory !"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 107
    iput-object v6, p0, Lcom/sec/android/app/bcocr/DecodeImageUtils;->mBitmap:Landroid/graphics/Bitmap;

    .line 109
    :cond_3
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public recycle()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/sec/android/app/bcocr/BitmapUtils;->recycle()V

    .line 39
    return-void
.end method
