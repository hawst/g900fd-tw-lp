.class Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;
.super Landroid/view/OrientationEventListener;
.source "TwScaleZoomRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->setOrientationListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;Landroid/content/Context;)V
    .locals 0
    .param p2, "$anonymous0"    # Landroid/content/Context;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    .line 133
    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    .line 137
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    const/4 v0, 0x0

    .line 141
    .local v0, "degree":I
    :try_start_0
    sget v1, Lcom/sec/android/app/bcocr/Feature;->CAMERA_LCD_ORIENTATION:I

    sget v2, Lcom/sec/android/app/bcocr/Feature;->CAMERA_LCD_ORIENTATION_PORTRAIT:I

    if-ne v1, v2, :cond_3

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    # invokes: Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->roundOrientation(I)I
    invoke-static {v1, p1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->access$0(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;I)I

    move-result v1

    add-int/lit8 v0, v1, 0x5a

    .line 147
    :goto_1
    const/16 v1, 0x168

    if-lt v0, v1, :cond_2

    .line 148
    add-int/lit16 v0, v0, -0x168

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    # getter for: Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->mLastOrientation:I
    invoke-static {v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->access$1(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-static {v1, v0}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->access$2(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;I)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    invoke-virtual {v1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->invalidate()V

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    goto :goto_0

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect$1;->this$0:Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;

    # invokes: Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->roundOrientation(I)I
    invoke-static {v1, p1}, Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;->access$0(Lcom/sec/android/app/bcocr/widget/TwScaleZoomRect;I)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_1
.end method
