.class public Lcom/sec/android/secvision/imageprocessing/OcrDNR;
.super Ljava/lang/Object;
.source "OcrDNR.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-string v0, "BCOcrDNRLib"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native ProcessDNR([III)I
.end method

.method public static native getCenterSharpness([BII)I
.end method

.method public static native getCenterStdDev([BII)I
.end method

.method public static native getGlobalSharpness([BII)I
.end method

.method public static native getGlobalStdDev([BII)I
.end method

.method public static native getImageStdDev([BII)I
.end method

.method public static native getSharpness([BII)I
.end method
