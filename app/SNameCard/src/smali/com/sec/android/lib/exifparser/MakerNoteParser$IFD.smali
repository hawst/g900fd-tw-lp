.class Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/lib/exifparser/MakerNoteParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IFD"
.end annotation


# instance fields
.field private mAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;",
            ">;"
        }
    .end annotation
.end field

.field private mIfdOffset:J

.field final synthetic this$0:Lcom/sec/android/lib/exifparser/MakerNoteParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/lib/exifparser/MakerNoteParser;I)V
    .locals 2
    .param p2, "ifdOffset"    # I

    .prologue
    .line 607
    iput-object p1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->this$0:Lcom/sec/android/lib/exifparser/MakerNoteParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 608
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    .line 609
    int-to-long v0, p2

    iput-wide v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mIfdOffset:J

    .line 610
    return-void
.end method


# virtual methods
.method public get(I)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    .line 626
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public put(Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    .locals 2
    .param p1, "tag"    # Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    .prologue
    .line 631
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    .line 634
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 620
    :cond_0
    return-void
.end method

.method public unflatten(Ljava/nio/ByteBuffer;)V
    .locals 14
    .param p1, "bb"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 639
    if-eqz p1, :cond_0

    .line 640
    const/4 v9, 0x0

    .line 641
    .local v9, "offset":I
    const/4 v0, 0x4

    new-array v5, v0, [B

    .line 644
    .local v5, "value":[B
    const/4 v10, 0x0

    .line 645
    .local v10, "tagCount":I
    const/4 v2, 0x0

    .line 646
    .local v2, "code":I
    const/4 v3, 0x0

    .line 647
    .local v3, "type":I
    const/4 v4, 0x0

    .line 649
    .local v4, "count":I
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    .line 650
    const/16 v0, 0x1e

    .line 649
    if-lt v10, v0, :cond_1

    .line 679
    .end local v2    # "code":I
    .end local v3    # "type":I
    .end local v4    # "count":I
    .end local v5    # "value":[B
    .end local v9    # "offset":I
    .end local v10    # "tagCount":I
    :cond_0
    return-void

    .line 651
    .restart local v2    # "code":I
    .restart local v3    # "type":I
    .restart local v4    # "count":I
    .restart local v5    # "value":[B
    .restart local v9    # "offset":I
    .restart local v10    # "tagCount":I
    :cond_1
    const/4 v9, 0x0

    .line 653
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 654
    .local v8, "b":Ljava/nio/ByteBuffer;
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 655
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 656
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p1, v5, v0, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 657
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 658
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 659
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 661
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 662
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 663
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 664
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p1, v5, v0, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 665
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 666
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 667
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 669
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 670
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    .line 672
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p1, v5, v0, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 675
    new-instance v0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->this$0:Lcom/sec/android/lib/exifparser/MakerNoteParser;

    int-to-long v6, v9

    iget-wide v12, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->mIfdOffset:J

    add-long/2addr v6, v12

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;-><init>(Lcom/sec/android/lib/exifparser/MakerNoteParser;III[BJ)V

    invoke-virtual {p0, v0}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->put(Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    .line 676
    add-int/lit8 v10, v10, 0x1

    goto :goto_0
.end method
