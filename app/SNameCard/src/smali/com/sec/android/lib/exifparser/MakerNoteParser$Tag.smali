.class Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/lib/exifparser/MakerNoteParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Tag"
.end annotation


# instance fields
.field private mCode:I

.field private mOffset:J

.field private mValue:[B

.field final synthetic this$0:Lcom/sec/android/lib/exifparser/MakerNoteParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/lib/exifparser/MakerNoteParser;III[BJ)V
    .locals 2
    .param p2, "code"    # I
    .param p3, "type"    # I
    .param p4, "count"    # I
    .param p5, "value"    # [B
    .param p6, "offset"    # J

    .prologue
    .line 692
    iput-object p1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->this$0:Lcom/sec/android/lib/exifparser/MakerNoteParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 689
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    .line 693
    iput p2, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mCode:I

    .line 696
    iput-wide p6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mOffset:J

    .line 698
    if-eqz p5, :cond_0

    array-length v1, p5

    if-lez v1, :cond_0

    .line 699
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 701
    .local v0, "bb":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_0

    .line 702
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 704
    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 705
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    .line 708
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    :cond_0
    return-void
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 711
    iget v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mCode:I

    return v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 727
    iget-wide v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mOffset:J

    return-wide v0
.end method

.method public getValue()[B
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    return-object v0
.end method

.method public putValue([B)V
    .locals 3
    .param p1, "value"    # [B

    .prologue
    .line 731
    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    array-length v1, v1

    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 732
    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->mValue:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 733
    .local v0, "destB":Ljava/nio/ByteBuffer;
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 734
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 736
    .end local v0    # "destB":Ljava/nio/ByteBuffer;
    :cond_0
    return-void
.end method
