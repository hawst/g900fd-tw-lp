.class public Lcom/sec/android/lib/exifparser/MakerNoteParser;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;,
        Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    }
.end annotation


# static fields
.field private static final APP1_MARKER:B = -0x1ft

.field private static final EXIF_HEADER:I = 0x6

.field private static final EXIF_PRIVATE_TAGCOUNT:I = 0x3b

.field private static final MARKER:I = 0x2

.field private static final MARKER_PREFIX:B = -0x1t

.field private static final SEC_MAKERNOTE_TAGCOUNT:I = 0x1e

.field private static final SOI_MARKER:B = -0x28t

.field private static final TAG:Ljava/lang/String; = "MakerNoteParser"

.field public static final TAG_AE:I = 0x60

.field public static final TAG_AF:I = 0x80

.field public static final TAG_AWB01:I = 0xa0

.field public static final TAG_AWB02:I = 0xa1

.field public static final TAG_COLORINFO:I = 0x20

.field public static final TAG_COLORSPACE:I = 0x50

.field public static final TAG_CONTINUOUSSHOTINTO:I = 0xb

.field public static final TAG_CTWEATHER:I = 0xc

.field private static final TAG_EXIF_EXIFIFD:I = 0x8769

.field private static final TAG_EXIF_MAKERNOTEIFD:I = 0x927c

.field public static final TAG_FACEDETECTION:I = 0x100

.field public static final TAG_FACEFEAT01:I = 0x101

.field public static final TAG_FACEFEAT02:I = 0x102

.field public static final TAG_FACENAMEINFO:I = 0x123

.field public static final TAG_FACERECOGNITION:I = 0x120

.field public static final TAG_FAVORITETAGGING:I = 0x40

.field public static final TAG_GPSINFO01:I = 0x30

.field public static final TAG_GPSINFO02:I = 0x31

.field public static final TAG_IMAGECOUNT:I = 0x25

.field public static final TAG_IPC:I = 0xc0

.field public static final TAG_LENSINFO:I = 0x140

.field public static final TAG_MAKERNOTEVERSION:I = 0x1

.field public static final TAG_MODELSERIALNUMBER:I = 0x23

.field public static final TAG_PREVIEWIMAGEINFO:I = 0x35

.field public static final TAG_SAMSUNGCONTENTID:I = 0x4

.field public static final TAG_SAMSUNGDEVICEID:I = 0x2

.field public static final TAG_SAMSUNGMODELID:I = 0x3

.field public static final TAG_SHOTMODE:I = 0xa

.field public static final TAG_SMARTAUTO:I = 0xe0

.field public static final TAG_SRMCOMPRESSIONMODE:I = 0x45

.field public static final TAG_THIRDPARTY:I = 0xa000

.field private static final TIFF_HEADER_OFFSET:I = 0xc

.field private static final TIFF_TAGCOUNT:I = 0x23


# instance fields
.field private mFile:Ljava/io/RandomAccessFile;

.field private mLittleEndian:Z

.field private mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

.field private mSupported:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z

    .line 101
    return-void
.end method

.method private IsExifFormat()Z
    .locals 20

    .prologue
    .line 309
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v15, :cond_0

    .line 310
    const/4 v15, 0x0

    .line 591
    :goto_0
    return v15

    .line 313
    :cond_0
    const/16 v15, 0x14

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 314
    .local v5, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 315
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 316
    .local v4, "buffer":[B
    const/16 v15, 0x8

    new-array v14, v15, [B

    .line 319
    .local v14, "temp":[B
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const-wide/16 v16, 0x2

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 320
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    array-length v0, v4

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 321
    const/4 v15, 0x0

    goto :goto_0

    .line 324
    :cond_1
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 327
    const/16 v15, -0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->checkMarker([BB)Z

    move-result v15

    if-nez v15, :cond_2

    .line 328
    const/4 v15, 0x0

    goto :goto_0

    .line 332
    :cond_2
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 337
    const/4 v15, 0x0

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 339
    const/4 v15, 0x0

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x45

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 340
    const/4 v15, 0x1

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x78

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 341
    const/4 v15, 0x2

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    const/4 v15, 0x3

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x66

    move/from16 v0, v16

    if-eq v15, v0, :cond_4

    .line 342
    :cond_3
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 346
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 349
    const/4 v15, 0x0

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x49

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 350
    const/4 v15, 0x1

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x49

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 351
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 352
    const-string v15, "MakerNoteParser"

    const-string v16, "Little endian"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :goto_1
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 364
    const/4 v15, 0x0

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 365
    const/4 v15, 0x1

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    if-eqz v15, :cond_a

    .line 367
    :cond_5
    const/4 v15, 0x0

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    if-nez v15, :cond_6

    .line 368
    const/4 v15, 0x1

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-eq v15, v0, :cond_a

    .line 371
    :cond_6
    const-string v15, "MakerNoteParser"

    const-string v16, "TIFF mark - error"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 353
    :cond_7
    const/4 v15, 0x0

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x4d

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 354
    const/4 v15, 0x1

    aget-byte v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    const/16 v16, 0x4d

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 355
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 356
    const-string v15, "MakerNoteParser"

    const-string v16, "Big endian"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 586
    :catch_0
    move-exception v6

    .line 588
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 591
    .end local v6    # "e":Ljava/io/IOException;
    :cond_8
    :goto_2
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 358
    :cond_9
    :try_start_1
    const-string v15, "MakerNoteParser"

    const-string v16, "Endian error"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 376
    :cond_a
    const/4 v15, 0x0

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 378
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mLittleEndian:Z

    if-nez v15, :cond_b

    .line 379
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 382
    :cond_b
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v15

    add-int/lit8 v9, v15, 0x2

    .line 384
    .local v9, "firstIfdOffset":I
    if-ltz v9, :cond_c

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-ltz v15, :cond_d

    .line 385
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 388
    :cond_d
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 390
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 391
    .local v2, "b":Ljava/nio/ByteBuffer;
    const/4 v7, 0x0

    .line 392
    .local v7, "entryCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v9

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 393
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 394
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 395
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 396
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 397
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 398
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 399
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 400
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 401
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 405
    :cond_e
    const/16 v15, 0x23

    if-le v7, v15, :cond_f

    .line 406
    const-string v15, "MakerNoteParser"

    const-string v16, "Tiff tag count is big."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 410
    :cond_f
    const/4 v8, 0x0

    .line 411
    .local v8, "exifIfdOffset":I
    const/16 v15, 0xc

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 412
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 413
    const/4 v13, 0x0

    .line 414
    .local v13, "tagCode":I
    const/4 v12, 0x0

    .line 416
    .local v12, "seekPos":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    const/16 v15, 0x23

    if-lt v10, v15, :cond_11

    .line 458
    :cond_10
    :goto_4
    if-nez v8, :cond_13

    .line 459
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 417
    :cond_11
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 418
    add-int/lit8 v15, v9, 0x2

    mul-int/lit8 v16, v10, 0xc

    add-int v12, v15, v16

    .line 420
    if-ltz v12, :cond_10

    int-to-long v0, v12

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-gez v15, :cond_10

    .line 424
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 426
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    const/16 v17, 0xc

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_12

    .line 427
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 428
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 429
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 430
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 431
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 432
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 433
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 434
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    .line 437
    const v15, 0x8769

    if-ne v13, v15, :cond_12

    .line 440
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 441
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 442
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 443
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 444
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 445
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 446
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 447
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    .line 449
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    .line 450
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    .line 453
    goto/16 :goto_4

    .line 416
    :cond_12
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 463
    :cond_13
    add-int/lit8 v12, v8, 0xc

    .line 464
    if-ltz v12, :cond_14

    int-to-long v0, v12

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-ltz v15, :cond_15

    .line 465
    :cond_14
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 468
    :cond_15
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 470
    const/4 v7, 0x0

    .line 471
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 472
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_16

    .line 473
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 474
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 475
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 476
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 477
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 478
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 479
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 480
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 484
    :cond_16
    const/16 v15, 0x3b

    if-le v7, v15, :cond_17

    .line 485
    const-string v15, "MakerNoteParser"

    const-string v16, "Exif tag count is big."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 489
    :cond_17
    const/16 v15, 0xc

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 490
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 491
    const/4 v13, 0x0

    .line 492
    const/4 v10, 0x0

    :goto_5
    const/16 v15, 0x3b

    if-lt v10, v15, :cond_1a

    .line 519
    :cond_18
    const/4 v11, 0x0

    .line 521
    .local v11, "makerNoteIfdOffset":I
    const v15, 0x927c

    if-ne v13, v15, :cond_19

    .line 522
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getShort()S

    .line 523
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    .line 524
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 529
    :cond_19
    if-nez v11, :cond_1c

    .line 530
    const-string v15, "MakerNoteParser"

    const-string v16, "MakerNoteIfd is not found"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 493
    .end local v11    # "makerNoteIfdOffset":I
    :cond_1a
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 494
    add-int/lit8 v15, v8, 0xc

    add-int/lit8 v15, v15, 0x2

    mul-int/lit8 v16, v10, 0xc

    add-int v12, v15, v16

    .line 496
    if-ltz v12, :cond_18

    int-to-long v0, v12

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-gez v15, :cond_18

    .line 500
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 502
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    const/16 v17, 0xc

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_1b

    .line 503
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 504
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 505
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 506
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 507
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 508
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 509
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 510
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    .line 512
    const v15, 0x927c

    if-eq v13, v15, :cond_18

    .line 492
    :cond_1b
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_5

    .line 534
    .restart local v11    # "makerNoteIfdOffset":I
    :cond_1c
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 536
    const/4 v7, 0x0

    .line 537
    add-int/lit8 v12, v11, 0xc

    .line 539
    if-ltz v12, :cond_1d

    int-to-long v0, v12

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-ltz v15, :cond_1e

    .line 540
    :cond_1d
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 543
    :cond_1e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 544
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_1f

    .line 545
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 546
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 547
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 548
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 549
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 550
    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 551
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 552
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 558
    :cond_1f
    const/16 v15, 0x1e

    if-le v7, v15, :cond_20

    .line 559
    const/16 v7, 0x1e

    .line 560
    const-string v15, "MakerNoteParser"

    const-string v16, "MakerNote tag count is big."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    :cond_20
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-nez v15, :cond_22

    .line 564
    new-instance v15, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    .line 565
    add-int/lit8 v16, v11, 0xc

    add-int/lit8 v16, v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;-><init>(Lcom/sec/android/lib/exifparser/MakerNoteParser;I)V

    .line 564
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    .line 570
    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-eqz v15, :cond_8

    .line 571
    add-int/lit8 v15, v11, 0xc

    add-int/lit8 v12, v15, 0x2

    .line 573
    if-ltz v12, :cond_21

    int-to-long v0, v12

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-ltz v15, :cond_23

    .line 574
    :cond_21
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 567
    :cond_22
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v15}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->reset()V

    goto :goto_6

    .line 577
    :cond_23
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 578
    mul-int/lit8 v15, v7, 0xc

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 579
    .local v3, "bb":Ljava/nio/ByteBuffer;
    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 580
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 581
    array-length v15, v4

    mul-int/lit8 v16, v7, 0xc

    move/from16 v0, v16

    if-lt v15, v0, :cond_8

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/16 v16, 0x0

    mul-int/lit8 v17, v7, 0xc

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v4, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_8

    .line 583
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v15, v3}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->unflatten(Ljava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private IsJpegFormat()Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 202
    iget-object v3, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v3, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v2

    .line 206
    :cond_1
    const/4 v2, 0x0

    .line 207
    .local v2, "isJpeg":Z
    new-array v0, v4, [B

    .line 210
    .local v0, "buffer":[B
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 212
    const/16 v3, -0x28

    invoke-direct {p0, v0, v3}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->checkMarker([BB)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    const/4 v2, 0x1

    goto :goto_0

    .line 226
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMarker([BB)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "marker"    # B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 235
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 242
    :cond_1
    :goto_0
    return v0

    .line 239
    :cond_2
    aget-byte v2, p1, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    aget-byte v2, p1, v0

    if-eq v2, p2, :cond_1

    :cond_3
    move v0, v1

    .line 242
    goto :goto_0
.end method

.method private reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 104
    iput-object v2, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    .line 110
    iput-boolean v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 112
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v0}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->reset()V

    .line 116
    :cond_0
    iput-object v2, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z

    .line 118
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 190
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->reset()V

    .line 199
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMakerNote(I)I
    .locals 5
    .param p1, "code"    # I

    .prologue
    const/4 v2, 0x0

    .line 250
    iget-boolean v3, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z

    if-nez v3, :cond_1

    .line 251
    const-string v3, "MakerNoteParser"

    const-string v4, "Not supported code"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    :goto_0
    return v2

    .line 257
    :cond_1
    iget-object v3, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-eqz v3, :cond_0

    .line 261
    iget-object v3, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v3, p1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    move-result-object v1

    .line 263
    .local v1, "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    if-eqz v1, :cond_0

    .line 264
    invoke-virtual {v1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->getValue()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 265
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 266
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    goto :goto_0
.end method

.method public isSupportedVersion([B)Z
    .locals 7
    .param p1, "version"    # [B

    .prologue
    const/16 v4, 0x30

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    .line 139
    :cond_1
    :goto_0
    return v1

    .line 125
    :cond_2
    const/4 v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 126
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 128
    invoke-virtual {v0, v2, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 129
    const/16 v3, 0x31

    invoke-virtual {v0, v1, v3}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 130
    invoke-virtual {v0, v5, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 131
    invoke-virtual {v0, v6, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 133
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 135
    aget-byte v3, p1, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_3

    aget-byte v3, p1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_3

    .line 136
    aget-byte v3, p1, v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_3

    aget-byte v3, p1, v6

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eq v3, v4, :cond_1

    :cond_3
    move v1, v2

    .line 139
    goto :goto_0
.end method

.method public open(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 144
    const/4 v2, 0x0

    .line 147
    .local v2, "isOpened":Z
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 185
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v4

    .line 155
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    invoke-direct {v4, p1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    .line 157
    iget-object v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v4, :cond_2

    .line 158
    invoke-direct {p0}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->IsJpegFormat()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 159
    invoke-direct {p0}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->IsExifFormat()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 160
    const/4 v3, 0x0

    .line 162
    .local v3, "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    iget-object v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-eqz v4, :cond_1

    .line 163
    iget-object v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    move-result-object v3

    .line 166
    :cond_1
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->getValue()[B

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->isSupportedVersion([B)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 167
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    const/4 v2, 0x1

    .line 181
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/lib/exifparser/MakerNoteParser;->close()V

    :cond_3
    move v4, v2

    .line 185
    goto :goto_0

    .line 170
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    :cond_4
    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 171
    const/4 v2, 0x0

    goto :goto_1

    .line 176
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v4, "MakerNoteParser"

    const-string v5, "File is not founded."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setMakerNote(II)V
    .locals 9
    .param p1, "code"    # I
    .param p2, "value"    # I

    .prologue
    .line 272
    iget-boolean v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mSupported:Z

    if-nez v6, :cond_1

    .line 273
    const-string v6, "MakerNoteParser"

    const-string v7, "Not supported code"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    if-eqz v6, :cond_0

    .line 283
    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v6, p1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;

    move-result-object v3

    .line 285
    .local v3, "tag":Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;
    if-eqz v3, :cond_0

    .line 286
    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v6, :cond_0

    .line 288
    :try_start_0
    invoke-virtual {v3}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->getOffset()J

    move-result-wide v4

    .line 289
    .local v4, "seekPos":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    .line 290
    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 292
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 293
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 294
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 295
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 296
    .local v1, "buffer":[B
    iget-object v6, p0, Lcom/sec/android/lib/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v6, v1, v7, v8}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 298
    invoke-virtual {v3, v1}, Lcom/sec/android/lib/exifparser/MakerNoteParser$Tag;->putValue([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 300
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "buffer":[B
    .end local v4    # "seekPos":J
    :catch_0
    move-exception v2

    .line 302
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
