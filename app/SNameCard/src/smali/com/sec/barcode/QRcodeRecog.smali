.class public Lcom/sec/barcode/QRcodeRecog;
.super Ljava/lang/Object;
.source "QRcodeRecog.java"


# static fields
.field public static BarStr:Ljava/lang/String; = null

.field public static MAX_RECOG_NUM:I = 0x0

.field public static final MAX_RECOG_SIZE:I

.field public static final QRCODE_SUCCESS:I = 0x101

.field public static final QR_FIRSTPOS:I = 0x2

.field public static final QR_LASTPOS:I = 0x3

.field public static final QR_RETURNTYPE:I = 0x4

.field public static final QR_TOTALNUM:I = 0x1

.field public static final QR_TYPE_EMAIL:I = 0x1

.field public static final QR_TYPE_NAME:I = 0x4

.field public static final QR_TYPE_TEL:I = 0x3

.field public static final QR_TYPE_UNKNOWN:I = 0x0

.field public static final QR_TYPE_URL:I = 0x2

.field public static QR_Type:[I = null

.field private static final TAG:Ljava/lang/String; = "QRcodeRecog"

.field public static barcodelib:Lcom/sec/barcode/BarcodeLib;

.field private static mJpegDataQRcode:[I

.field private static mPreviewCropMode:Z

.field private static mPreviewDataQRcode:[B

.field public static mQRcodeRect:Landroid/graphics/Rect;

.field public static mQRcodeString:[Ljava/lang/String;

.field public static mQRcodeStringFull:[Ljava/lang/String;

.field public static mQRcodeType:[I

.field private static mRecogHeight:I

.field private static mRecogWidth:I

.field public static mjpegHeight:I

.field public static mjpegWidth:I

.field public static mpreviewDetectHeight:I

.field public static mpreviewDetectWidth:I

.field private static mpreviewExtraMarginBottom:I

.field private static mpreviewExtraMarginLeft:I

.field private static mpreviewExtraMarginRight:I

.field private static mpreviewExtraMarginTop:I

.field private static mpreviewMarginBottom:I

.field private static mpreviewMarginLeft:I

.field private static mpreviewMarginRight:I

.field private static mpreviewMarginTop:I

.field private static mpreviewSensorHeight:I

.field private static mpreviewSensorWidth:I

.field public static x1:I

.field public static x2:I

.field public static x3:I

.field public static x4:I

.field public static y1:I

.field public static y2:I

.field public static y3:I

.field public static y4:I


# instance fields
.field public rec:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 13
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    .line 14
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->BarStr:Ljava/lang/String;

    .line 22
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    .line 29
    const/4 v0, 0x1

    sput v0, Lcom/sec/barcode/QRcodeRecog;->x1:I

    .line 30
    const/4 v0, 0x3

    sput v0, Lcom/sec/barcode/QRcodeRecog;->x2:I

    .line 31
    sput v3, Lcom/sec/barcode/QRcodeRecog;->x3:I

    .line 32
    const/4 v0, 0x7

    sput v0, Lcom/sec/barcode/QRcodeRecog;->x4:I

    .line 34
    const/4 v0, 0x2

    sput v0, Lcom/sec/barcode/QRcodeRecog;->y1:I

    .line 35
    const/4 v0, 0x4

    sput v0, Lcom/sec/barcode/QRcodeRecog;->y2:I

    .line 36
    const/4 v0, 0x6

    sput v0, Lcom/sec/barcode/QRcodeRecog;->y3:I

    .line 37
    const/16 v0, 0x8

    sput v0, Lcom/sec/barcode/QRcodeRecog;->y4:I

    .line 57
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    .line 58
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    .line 59
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginRight:I

    .line 60
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginBottom:I

    .line 62
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    .line 63
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    .line 64
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginRight:I

    .line 65
    sput v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginBottom:I

    .line 67
    sput-boolean v1, Lcom/sec/barcode/QRcodeRecog;->mPreviewCropMode:Z

    .line 69
    sget-object v0, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntResolution(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_SIZE:I

    .line 71
    sput v3, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_NUM:I

    .line 72
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 74
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    .line 76
    sput-object v2, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/16 v0, 0x101

    iput v0, p0, Lcom/sec/barcode/QRcodeRecog;->rec:I

    .line 10
    return-void
.end method

.method public static GetQRResultRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 387
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static GetQRResultString()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    return-object v0
.end method

.method public static GetQRResultTotalNum()I
    .locals 2

    .prologue
    .line 375
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    if-eqz v0, :cond_0

    .line 376
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    .line 378
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static GetQRResultType()[I
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    return-object v0
.end method

.method public static GetQRResultTypeString()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 286
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    if-eqz v4, :cond_f

    .line 287
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    new-array v4, v4, [I

    sput-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    .line 288
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    new-array v4, v4, [Ljava/lang/String;

    sput-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    .line 289
    const/4 v3, 0x0

    .line 292
    .local v3, "mTempString":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    const/4 v1, 0x0

    .local v1, "j":I
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_0
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    if-lt v0, v4, :cond_2

    .line 357
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    if-eqz v4, :cond_0

    .line 358
    const/4 v1, 0x0

    :goto_1
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    array-length v4, v4

    if-lt v1, v4, :cond_d

    .line 363
    :cond_0
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 364
    const/4 v1, 0x0

    :goto_2
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_e

    .line 373
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "k":I
    .end local v3    # "mTempString":Ljava/lang/String;
    :cond_1
    :goto_3
    return-void

    .line 293
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    .restart local v2    # "k":I
    .restart local v3    # "mTempString":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x4

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 334
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_a

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    add-int/lit8 v5, v0, 0x1

    mul-int/lit8 v5, v5, 0x3

    add-int/lit8 v5, v5, 0x4

    aget v4, v4, v5

    if-eqz v4, :cond_a

    .line 335
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    aput v9, v4, v0

    .line 336
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 337
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v2, 0x3

    add-int/lit8 v7, v7, 0x2

    aget v6, v6, v7

    sget-object v7, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v8, v1, 0x3

    add-int/lit8 v8, v8, 0x3

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 339
    :cond_3
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 292
    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 295
    :pswitch_0
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    aput v10, v4, v0

    .line 296
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_5

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 297
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x2

    aget v6, v6, v7

    sget-object v7, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v8, v0, 0x3

    add-int/lit8 v8, v8, 0x3

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 299
    :cond_5
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 300
    goto :goto_4

    .line 303
    :pswitch_1
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    const/4 v5, 0x2

    aput v5, v4, v0

    .line 304
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_6

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_6

    .line 305
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v4, v4, v9

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v6, v0, 0x3

    add-int/lit8 v6, v6, 0x2

    aget v5, v5, v6

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x3

    aget v6, v6, v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 306
    const-string v4, ".*mailto:.*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 307
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    const-string v5, "mailto:"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 308
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    aput v10, v4, v0

    .line 314
    :cond_6
    :goto_5
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 315
    goto/16 :goto_4

    .line 311
    :cond_7
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    const-string v5, "URI:"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    goto :goto_5

    .line 318
    :pswitch_2
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    const/4 v5, 0x3

    aput v5, v4, v0

    .line 319
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_8

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_8

    .line 320
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x2

    aget v6, v6, v7

    sget-object v7, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v8, v0, 0x3

    add-int/lit8 v8, v8, 0x3

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 322
    :cond_8
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 323
    goto/16 :goto_4

    .line 326
    :pswitch_3
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    const/4 v5, 0x4

    aput v5, v4, v0

    .line 327
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_9

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_9

    .line 328
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x2

    aget v6, v6, v7

    sget-object v7, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v8, v0, 0x3

    add-int/lit8 v8, v8, 0x3

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 330
    :cond_9
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 331
    goto/16 :goto_4

    .line 341
    :cond_a
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_b

    .line 342
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    aput v9, v4, v0

    .line 343
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    aget v4, v4, v5

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 344
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    sget-object v5, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v5, v5, v9

    sget-object v6, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v7, v2, 0x3

    add-int/lit8 v7, v7, 0x2

    aget v6, v6, v7

    sget-object v7, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    mul-int/lit8 v8, v1, 0x3

    add-int/lit8 v8, v8, 0x3

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    goto/16 :goto_4

    .line 348
    :cond_b
    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    aget v4, v4, v10

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_c

    sget-object v4, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    add-int/lit8 v5, v0, 0x1

    mul-int/lit8 v5, v5, 0x3

    add-int/lit8 v5, v5, 0x4

    aget v4, v4, v5

    if-eqz v4, :cond_c

    .line 349
    move v2, v0

    .line 351
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    .line 358
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 364
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 371
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "k":I
    .end local v3    # "mTempString":Ljava/lang/String;
    :cond_f
    const-string v4, "QRcodeRecog"

    const-string v5, "[QR_Type()] null error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static close()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    .line 92
    return-void
.end method

.method public static encodeYUV420SP([B[III)V
    .locals 17
    .param p0, "yuv420sp"    # [B
    .param p1, "argb"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 395
    mul-int v7, p2, p3

    .line 397
    .local v7, "frameSize":I
    const/4 v13, 0x0

    .line 398
    .local v13, "yIndex":I
    move v11, v7

    .line 401
    .local v11, "uvIndex":I
    const/4 v9, 0x0

    .line 402
    .local v9, "index":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    move/from16 v0, p3

    if-lt v10, v0, :cond_0

    .line 424
    return-void

    .line 403
    :cond_0
    const/4 v8, 0x0

    .local v8, "i":I
    move v12, v11

    .end local v11    # "uvIndex":I
    .local v12, "uvIndex":I
    move v14, v13

    .end local v13    # "yIndex":I
    .local v14, "yIndex":I
    :goto_1
    move/from16 v0, p2

    if-lt v8, v0, :cond_1

    .line 402
    add-int/lit8 v10, v10, 0x1

    move v11, v12

    .end local v12    # "uvIndex":I
    .restart local v11    # "uvIndex":I
    move v13, v14

    .end local v14    # "yIndex":I
    .restart local v13    # "yIndex":I
    goto :goto_0

    .line 406
    .end local v11    # "uvIndex":I
    .end local v13    # "yIndex":I
    .restart local v12    # "uvIndex":I
    .restart local v14    # "yIndex":I
    :cond_1
    aget v15, p1, v9

    const/high16 v16, 0xff0000

    and-int v15, v15, v16

    shr-int/lit8 v3, v15, 0x10

    .line 407
    .local v3, "R":I
    aget v15, p1, v9

    const v16, 0xff00

    and-int v15, v15, v16

    shr-int/lit8 v2, v15, 0x8

    .line 408
    .local v2, "G":I
    aget v15, p1, v9

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v1, v15, 0x0

    .line 411
    .local v1, "B":I
    mul-int/lit8 v15, v3, 0x42

    mul-int/lit16 v0, v2, 0x81

    move/from16 v16, v0

    add-int v15, v15, v16

    mul-int/lit8 v16, v1, 0x19

    add-int v15, v15, v16

    add-int/lit16 v15, v15, 0x80

    shr-int/lit8 v15, v15, 0x8

    add-int/lit8 v6, v15, 0x10

    .line 412
    .local v6, "Y":I
    mul-int/lit8 v15, v3, -0x26

    mul-int/lit8 v16, v2, 0x4a

    sub-int v15, v15, v16

    mul-int/lit8 v16, v1, 0x70

    add-int v15, v15, v16

    add-int/lit16 v15, v15, 0x80

    shr-int/lit8 v15, v15, 0x8

    add-int/lit16 v4, v15, 0x80

    .line 413
    .local v4, "U":I
    mul-int/lit8 v15, v3, 0x70

    mul-int/lit8 v16, v2, 0x5e

    sub-int v15, v15, v16

    mul-int/lit8 v16, v1, 0x12

    sub-int v15, v15, v16

    add-int/lit16 v15, v15, 0x80

    shr-int/lit8 v15, v15, 0x8

    add-int/lit16 v5, v15, 0x80

    .line 415
    .local v5, "V":I
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "yIndex":I
    .restart local v13    # "yIndex":I
    if-gez v6, :cond_6

    const/4 v6, 0x0

    .end local v6    # "Y":I
    :cond_2
    :goto_2
    int-to-byte v15, v6

    aput-byte v15, p0, v14

    .line 416
    rem-int/lit8 v15, v10, 0x2

    if-nez v15, :cond_5

    rem-int/lit8 v15, v9, 0x2

    if-nez v15, :cond_5

    .line 417
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "uvIndex":I
    .restart local v11    # "uvIndex":I
    if-gez v5, :cond_7

    const/4 v5, 0x0

    .end local v5    # "V":I
    :cond_3
    :goto_3
    int-to-byte v15, v5

    aput-byte v15, p0, v12

    .line 418
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "uvIndex":I
    .restart local v12    # "uvIndex":I
    if-gez v4, :cond_8

    const/4 v4, 0x0

    .end local v4    # "U":I
    :cond_4
    :goto_4
    int-to-byte v15, v4

    aput-byte v15, p0, v11

    :cond_5
    move v11, v12

    .line 421
    .end local v12    # "uvIndex":I
    .restart local v11    # "uvIndex":I
    add-int/lit8 v9, v9, 0x1

    .line 403
    add-int/lit8 v8, v8, 0x1

    move v12, v11

    .end local v11    # "uvIndex":I
    .restart local v12    # "uvIndex":I
    move v14, v13

    .end local v13    # "yIndex":I
    .restart local v14    # "yIndex":I
    goto :goto_1

    .line 415
    .end local v14    # "yIndex":I
    .restart local v4    # "U":I
    .restart local v5    # "V":I
    .restart local v6    # "Y":I
    .restart local v13    # "yIndex":I
    :cond_6
    const/16 v15, 0xff

    if-le v6, v15, :cond_2

    const/16 v6, 0xff

    goto :goto_2

    .line 417
    .end local v6    # "Y":I
    .end local v12    # "uvIndex":I
    .restart local v11    # "uvIndex":I
    :cond_7
    const/16 v15, 0xff

    if-le v5, v15, :cond_3

    const/16 v5, 0xff

    goto :goto_3

    .line 418
    .end local v5    # "V":I
    .end local v11    # "uvIndex":I
    .restart local v12    # "uvIndex":I
    :cond_8
    const/16 v15, 0xff

    if-le v4, v15, :cond_4

    const/16 v4, 0xff

    goto :goto_4
.end method

.method public static init()V
    .locals 2

    .prologue
    .line 83
    :try_start_0
    new-instance v1, Lcom/sec/barcode/BarcodeLib;

    invoke-direct {v1}, Lcom/sec/barcode/BarcodeLib;-><init>()V

    sput-object v1, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 84
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 86
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static resetQRcodeResult()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 171
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 172
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    .line 173
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeString:[Ljava/lang/String;

    .line 174
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeType:[I

    .line 175
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    .line 176
    return-void
.end method

.method public static setJPEGDataQRcode([IIIIIII)Z
    .locals 4
    .param p0, "ImgData"    # [I
    .param p1, "nImgWidth"    # I
    .param p2, "nImgHeight"    # I
    .param p3, "nRegionLeft"    # I
    .param p4, "nRegionTop"    # I
    .param p5, "nRegionRight"    # I
    .param p6, "nRegionBottom"    # I

    .prologue
    const/4 v0, 0x0

    .line 131
    const-string v1, "QRcodeRecog"

    const-string v2, "setJPEGDataQRcode "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/barcode/QRcodeRecog;->mJpegDataQRcode:[I

    .line 135
    if-nez p0, :cond_0

    .line 136
    const-string v1, "QRcodeRecog"

    const-string v2, "ImgData is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :goto_0
    return v0

    .line 138
    :cond_0
    array-length v1, p0

    sget v2, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_SIZE:I

    if-le v1, v2, :cond_1

    .line 139
    const-string v1, "QRcodeRecog"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Image is too big: length = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MAX_RECOG_SIZE = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_SIZE:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_1
    sput-object p0, Lcom/sec/barcode/QRcodeRecog;->mJpegDataQRcode:[I

    .line 146
    sput p1, Lcom/sec/barcode/QRcodeRecog;->mjpegWidth:I

    .line 147
    sput p2, Lcom/sec/barcode/QRcodeRecog;->mjpegHeight:I

    .line 149
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    .line 150
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    .line 151
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginRight:I

    .line 152
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginBottom:I

    .line 154
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    .line 155
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    .line 156
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginRight:I

    .line 157
    sput v0, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginBottom:I

    .line 159
    sput-boolean v0, Lcom/sec/barcode/QRcodeRecog;->mPreviewCropMode:Z

    .line 161
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setPreviewCropMode(Z)V
    .locals 0
    .param p0, "isCropMode"    # Z

    .prologue
    .line 165
    sput-boolean p0, Lcom/sec/barcode/QRcodeRecog;->mPreviewCropMode:Z

    .line 166
    return-void
.end method

.method public static setPreviewDataQRcode([BIIIIIIIIIIIIIIZ)V
    .locals 2
    .param p0, "PrevData"    # [B
    .param p1, "nPrevDetectWidth"    # I
    .param p2, "nPrevDetectHeight"    # I
    .param p3, "nPrevSensorWidth"    # I
    .param p4, "nPrevSensorHeight"    # I
    .param p5, "nRecogPrevWidth"    # I
    .param p6, "nRecogPrevHeight"    # I
    .param p7, "nMarginLeft"    # I
    .param p8, "nMarginTop"    # I
    .param p9, "nMarginRight"    # I
    .param p10, "nMarginBottom"    # I
    .param p11, "nExtraMarginLeft"    # I
    .param p12, "nExtraMarginTop"    # I
    .param p13, "nExtraMarginRight"    # I
    .param p14, "nExtraMarginBottom"    # I
    .param p15, "isPrevCropMode"    # Z

    .prologue
    .line 100
    if-nez p0, :cond_0

    .line 101
    const-string v0, "QRcodeRecog"

    const-string v1, "PrevData is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_0
    return-void

    .line 105
    :cond_0
    sput-object p0, Lcom/sec/barcode/QRcodeRecog;->mPreviewDataQRcode:[B

    .line 107
    sput p3, Lcom/sec/barcode/QRcodeRecog;->mpreviewSensorWidth:I

    .line 108
    sput p4, Lcom/sec/barcode/QRcodeRecog;->mpreviewSensorHeight:I

    .line 109
    sput p5, Lcom/sec/barcode/QRcodeRecog;->mRecogWidth:I

    .line 110
    sput p6, Lcom/sec/barcode/QRcodeRecog;->mRecogHeight:I

    .line 112
    sput p1, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectWidth:I

    .line 113
    sput p2, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectHeight:I

    .line 115
    sput p7, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    .line 116
    sput p8, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    .line 117
    sput p9, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginRight:I

    .line 118
    sput p10, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginBottom:I

    .line 120
    sput p11, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    .line 121
    sput p12, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    .line 122
    sput p13, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginRight:I

    .line 123
    sput p14, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginBottom:I

    .line 125
    sput-boolean p15, Lcom/sec/barcode/QRcodeRecog;->mPreviewCropMode:Z

    goto :goto_0
.end method

.method public static startQRcodeRecognition()Z
    .locals 15

    .prologue
    .line 179
    const-string v0, "QRcodeRecog"

    const-string v1, "[startQRcodeRecognition()] START"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v11, 0x0

    .line 184
    .local v11, "nRet":I
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mPreviewDataQRcode:[B

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mJpegDataQRcode:[I

    if-nez v0, :cond_0

    .line 185
    const-string v0, "QRcodeRecog"

    const-string v1, "[startQRcodeRecognition()] : Imagedata is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const/4 v0, 0x0

    .line 281
    :goto_0
    return v0

    .line 190
    :cond_0
    :try_start_0
    new-instance v0, Lcom/sec/barcode/BarcodeLib;

    invoke-direct {v0}, Lcom/sec/barcode/BarcodeLib;-><init>()V

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getRecognitionMode()I

    move-result v0

    if-nez v0, :cond_3

    .line 199
    sget-boolean v0, Lcom/sec/barcode/QRcodeRecog;->mPreviewCropMode:Z

    if-eqz v0, :cond_2

    .line 200
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    sget-object v1, Lcom/sec/barcode/QRcodeRecog;->mPreviewDataQRcode:[B

    .line 201
    sget v2, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectWidth:I

    sget v3, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectHeight:I

    .line 202
    sget v4, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectWidth:I

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    sub-int/2addr v4, v5

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginRight:I

    sub-int/2addr v4, v5

    .line 203
    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewDetectHeight:I

    sget v14, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    sub-int/2addr v5, v14

    sget v14, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginBottom:I

    sub-int/2addr v5, v14

    .line 200
    invoke-virtual/range {v0 .. v5}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_PreviewMode([BIIII)I

    move-result v11

    .line 214
    :goto_2
    const/16 v0, 0x101

    if-ne v11, v0, :cond_6

    .line 215
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 218
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    invoke-virtual {v0}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_GetQR_OutputItems()[I

    move-result-object v0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->QR_Type:[I

    .line 220
    sget v0, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_NUM:I

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 222
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    sget v0, Lcom/sec/barcode/QRcodeRecog;->MAX_RECOG_NUM:I

    if-lt v8, v0, :cond_4

    .line 227
    sget-boolean v0, Lcom/sec/android/app/bcocr/Feature;->OCR_DEBUG_LEVEL_HIGH:Z

    if-eqz v0, :cond_1

    .line 228
    const-string v0, "QRcodeRecog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VI_BARCODE_DECODE BarStr: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    invoke-virtual {v0}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_GetPoints()[I

    move-result-object v10

    .line 232
    .local v10, "nQRcodeRect":[I
    if-eqz v10, :cond_5

    .line 234
    sget v0, Lcom/sec/barcode/QRcodeRecog;->x1:I

    aget v0, v10, v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->x2:I

    aget v1, v10, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->x3:I

    aget v1, v10, v1

    sget v2, Lcom/sec/barcode/QRcodeRecog;->x4:I

    aget v2, v10, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 235
    .local v9, "left":I
    sget v0, Lcom/sec/barcode/QRcodeRecog;->x1:I

    aget v0, v10, v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->x2:I

    aget v1, v10, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->x3:I

    aget v1, v10, v1

    sget v2, Lcom/sec/barcode/QRcodeRecog;->x4:I

    aget v2, v10, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 236
    .local v12, "right":I
    sget v0, Lcom/sec/barcode/QRcodeRecog;->y1:I

    aget v0, v10, v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->y2:I

    aget v1, v10, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->y3:I

    aget v1, v10, v1

    sget v2, Lcom/sec/barcode/QRcodeRecog;->y4:I

    aget v2, v10, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 237
    .local v13, "top":I
    sget v0, Lcom/sec/barcode/QRcodeRecog;->y1:I

    aget v0, v10, v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->y2:I

    aget v1, v10, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lcom/sec/barcode/QRcodeRecog;->y3:I

    aget v1, v10, v1

    sget v2, Lcom/sec/barcode/QRcodeRecog;->y4:I

    aget v2, v10, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 239
    .local v6, "bottom":I
    new-instance v0, Landroid/graphics/Rect;

    sget v1, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    add-int/2addr v1, v9

    sget v2, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    add-int/2addr v1, v2

    .line 240
    sget v2, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    add-int/2addr v2, v13

    sget v3, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    add-int/2addr v2, v3

    .line 241
    sget v3, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    add-int/2addr v3, v12

    sget v4, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginLeft:I

    add-int/2addr v3, v4

    .line 242
    sget v4, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    add-int/2addr v4, v6

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewExtraMarginTop:I

    add-int/2addr v4, v5

    .line 239
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    .line 252
    invoke-static {}, Lcom/sec/barcode/QRcodeRecog;->GetQRResultTypeString()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 273
    .end local v6    # "bottom":I
    .end local v8    # "i":I
    .end local v9    # "left":I
    .end local v10    # "nQRcodeRect":[I
    .end local v12    # "right":I
    .end local v13    # "top":I
    :goto_4
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mPreviewDataQRcode:[B

    .line 274
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mJpegDataQRcode:[I

    .line 275
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    .line 277
    const-string v0, "QRcodeRecog"

    const-string v1, "[startQRcodeRecognition()] END"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/16 v0, 0x101

    if-ne v11, v0, :cond_7

    .line 279
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 191
    :catch_0
    move-exception v7

    .line 193
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 207
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    sget-object v1, Lcom/sec/barcode/QRcodeRecog;->mPreviewDataQRcode:[B

    sget v2, Lcom/sec/barcode/QRcodeRecog;->mpreviewSensorWidth:I

    sget v3, Lcom/sec/barcode/QRcodeRecog;->mpreviewSensorHeight:I

    sget v4, Lcom/sec/barcode/QRcodeRecog;->mRecogWidth:I

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginLeft:I

    sub-int/2addr v4, v5

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginRight:I

    sub-int/2addr v4, v5

    sget v5, Lcom/sec/barcode/QRcodeRecog;->mRecogHeight:I

    sget v14, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginTop:I

    sub-int/2addr v5, v14

    sget v14, Lcom/sec/barcode/QRcodeRecog;->mpreviewMarginBottom:I

    sub-int/2addr v5, v14

    invoke-virtual/range {v0 .. v5}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_PreviewMode([BIIII)I

    move-result v11

    .line 209
    goto/16 :goto_2

    .line 210
    :cond_3
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->barcodelib:Lcom/sec/barcode/BarcodeLib;

    sget-object v1, Lcom/sec/barcode/QRcodeRecog;->mJpegDataQRcode:[I

    sget v2, Lcom/sec/barcode/QRcodeRecog;->mjpegWidth:I

    sget v3, Lcom/sec/barcode/QRcodeRecog;->mjpegHeight:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_SnapshotMode([III)I

    move-result v11

    goto/16 :goto_2

    .line 223
    .restart local v8    # "i":I
    :cond_4
    sget-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    add-int/lit8 v1, v8, 0x1

    invoke-static {v1}, Lcom/sec/barcode/BarcodeLib;->MVI_Barcode_GetResult(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 224
    const-string v0, "QRcodeRecog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VI_BARCODE_DECODE BarStr: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    aget-object v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    .line 255
    .restart local v10    # "nQRcodeRect":[I
    :cond_5
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    .line 256
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 257
    const-string v0, "QRcodeRecog"

    const-string v1, "QR code nQRcodeRect Error: "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    .line 268
    .end local v8    # "i":I
    .end local v10    # "nQRcodeRect":[I
    :catch_1
    move-exception v7

    .line 269
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 270
    const-string v0, "QRcodeRecog"

    const-string v1, "startQRcodeRecognition() Exception error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 261
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v0, 0x0

    :try_start_3
    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeRect:Landroid/graphics/Rect;

    .line 262
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/barcode/QRcodeRecog;->mQRcodeStringFull:[Ljava/lang/String;

    .line 263
    const-string v0, "QRcodeRecog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QR code Recog Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_4

    .line 281
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
