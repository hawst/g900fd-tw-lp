.class public Lcom/sec/barcode/BarcodeLib;
.super Ljava/lang/Object;
.source "BarcodeLib.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BarcodeLib"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    :try_start_0
    const-string v1, "BCOcrBarcodeDecoder_Lib"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 14
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 15
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native MVI_Barcode_GetResult(I)Ljava/lang/String;
.end method


# virtual methods
.method public native MVI_Barcode_GetPoints()[I
.end method

.method public native MVI_Barcode_GetQR_OutputItems()[I
.end method

.method public native MVI_Barcode_PreviewMode([BIIII)I
.end method

.method public native MVI_Barcode_SnapshotMode([III)I
.end method
