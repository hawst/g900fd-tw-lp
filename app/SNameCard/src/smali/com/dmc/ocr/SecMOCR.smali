.class public Lcom/dmc/ocr/SecMOCR;
.super Ljava/lang/Object;
.source "SecMOCR.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;
    }
.end annotation


# static fields
.field public static final MAX_RECOG_SIZE:I

.field public static final MOCR_BCRTYPE_ADDRESS:I = 0x6

.field public static final MOCR_BCRTYPE_CITY:I = 0x9

.field public static final MOCR_BCRTYPE_COMPANY:I = 0x5

.field public static final MOCR_BCRTYPE_COUNTRY:I = 0x8

.field public static final MOCR_BCRTYPE_DEPARTMENT:I = 0x4

.field public static final MOCR_BCRTYPE_EMAIL:I = 0xf

.field public static final MOCR_BCRTYPE_FAX:I = 0xc

.field public static final MOCR_BCRTYPE_FIRSTNAME:I = 0x1

.field public static final MOCR_BCRTYPE_LASTNAME:I = 0x2

.field public static final MOCR_BCRTYPE_MOBILE:I = 0xd

.field public static final MOCR_BCRTYPE_NAME:I = 0x0

.field public static final MOCR_BCRTYPE_OTHER:I = 0x10

.field public static final MOCR_BCRTYPE_PHONENUM:I = 0xb

.field public static final MOCR_BCRTYPE_POSTCODE:I = 0x7

.field public static final MOCR_BCRTYPE_STREET:I = 0xa

.field public static final MOCR_BCRTYPE_TITLE:I = 0x3

.field public static final MOCR_BCRTYPE_URL:I = 0xe

.field public static final MOCR_LANG_AFRIKAANS:I = 0x2

.field public static final MOCR_LANG_ALBANIAN:I = 0x3

.field public static final MOCR_LANG_BASQUE:I = 0x4

.field public static final MOCR_LANG_BRETON:I = 0x5

.field public static final MOCR_LANG_BULGARIAN:I = 0x6

.field public static final MOCR_LANG_BYELORUSSIAN:I = 0x7

.field public static final MOCR_LANG_CATALAN:I = 0x8

.field public static final MOCR_LANG_CHECHEN:I = 0x9

.field public static final MOCR_LANG_CHINESE_SIMPLIFIED:I = 0x41

.field public static final MOCR_LANG_CHINESE_TRADITIONAL:I = 0x42

.field public static final MOCR_LANG_CRIMEANTATAR:I = 0xa

.field public static final MOCR_LANG_CROATIAN:I = 0xb

.field public static final MOCR_LANG_CZECH:I = 0xc

.field public static final MOCR_LANG_DANISH:I = 0xd

.field public static final MOCR_LANG_DIGITS:I = 0x3f

.field public static final MOCR_LANG_DUTCH:I = 0xe

.field public static final MOCR_LANG_DUTCHBELGIAN:I = 0xf

.field public static final MOCR_LANG_ENGLISH:I = 0x10

.field public static final MOCR_LANG_ESTONIAN:I = 0x11

.field public static final MOCR_LANG_FIJIAN:I = 0x12

.field public static final MOCR_LANG_FINNISH:I = 0x13

.field public static final MOCR_LANG_FIRST_CJK_LANGUAGE_ID:I = 0x41

.field public static final MOCR_LANG_FIRST_LID:I = 0x1

.field public static final MOCR_LANG_FRENCH:I = 0x14

.field public static final MOCR_LANG_GERMAN:I = 0x15

.field public static final MOCR_LANG_GERMANNEWSPELLING:I = 0x16

.field public static final MOCR_LANG_GREEK:I = 0x17

.field public static final MOCR_LANG_HAWAIIAN:I = 0x18

.field public static final MOCR_LANG_HUNGARIAN:I = 0x19

.field public static final MOCR_LANG_ICELANDIC:I = 0x1a

.field public static final MOCR_LANG_INDONESIAN:I = 0x1b

.field public static final MOCR_LANG_IRISH:I = 0x1c

.field public static final MOCR_LANG_ITALIAN:I = 0x1d

.field public static final MOCR_LANG_JAPANESE:I = 0x43

.field public static final MOCR_LANG_KABARDIAN:I = 0x1e

.field public static final MOCR_LANG_KOREAN:I = 0x44

.field public static final MOCR_LANG_KOREAN_HANGUL:I = 0x45

.field public static final MOCR_LANG_KOREAN_HANJA:I = 0x46

.field public static final MOCR_LANG_LAST_CJK_LANGUAGE_ID:I = 0x46

.field public static final MOCR_LANG_LAST_LID:I = 0x46

.field public static final MOCR_LANG_LATIN:I = 0x1f

.field public static final MOCR_LANG_LATVIAN:I = 0x20

.field public static final MOCR_LANG_LITHUANIAN:I = 0x21

.field public static final MOCR_LANG_MACEDONIAN:I = 0x22

.field public static final MOCR_LANG_MALAY:I = 0x23

.field public static final MOCR_LANG_MAORI:I = 0x24

.field public static final MOCR_LANG_MAX:I = 0x4c

.field public static final MOCR_LANG_MIXED:I = 0x25

.field public static final MOCR_LANG_MOLDAVIAN:I = 0x26

.field public static final MOCR_LANG_MONGOL:I = 0x27

.field public static final MOCR_LANG_NORWEGIAN:I = 0x28

.field public static final MOCR_LANG_NORWEGIANBOKMAL:I = 0x29

.field public static final MOCR_LANG_NORWEGIANNYNORSK:I = 0x2a

.field public static final MOCR_LANG_NOT_SELECTED:I = 0x0

.field public static final MOCR_LANG_OSSETIC:I = 0x2b

.field public static final MOCR_LANG_POLISH:I = 0x2c

.field public static final MOCR_LANG_PORTUGUESE:I = 0x2d

.field public static final MOCR_LANG_PORTUGUESEBRAZILIAN:I = 0x2e

.field public static final MOCR_LANG_PROVENCAL:I = 0x2f

.field public static final MOCR_LANG_RHAETOROMANIC:I = 0x30

.field public static final MOCR_LANG_ROMANIAN:I = 0x31

.field public static final MOCR_LANG_RUSSIAN:I = 0x32

.field public static final MOCR_LANG_SAMOAN:I = 0x33

.field public static final MOCR_LANG_SERBIAN:I = 0x34

.field public static final MOCR_LANG_SLOVAK:I = 0x35

.field public static final MOCR_LANG_SLOVENIAN:I = 0x36

.field public static final MOCR_LANG_SPANISH:I = 0x37

.field public static final MOCR_LANG_SWAHILI:I = 0x38

.field public static final MOCR_LANG_SWEDISH:I = 0x39

.field public static final MOCR_LANG_TAGALOG:I = 0x3a

.field public static final MOCR_LANG_TATAR:I = 0x3b

.field public static final MOCR_LANG_TURKISH:I = 0x3c

.field public static final MOCR_LANG_UKRAINIAN:I = 0x3d

.field public static final MOCR_LANG_UNDEFINED:I = 0x1

.field public static final MOCR_LANG_WELSH:I = 0x3e

.field public static final MOCR_LANG_WESTEUROPEAN:I = 0x40

.field public static final MOCR_RECOG_MODE_CAPTURED:I = 0x1

.field public static final MOCR_RECOG_MODE_NONE:I = 0x2

.field public static final MOCR_RECOG_MODE_PREV:I = 0x0

.field public static final MOCR_RECOG_TYPE_BARCODE:I = 0x3

.field public static final MOCR_RECOG_TYPE_BCR:I = 0x0

.field public static final MOCR_RECOG_TYPE_NORM:I = 0x2

.field public static final MOCR_RECOG_TYPE_TRANS:I = 0x1

.field public static final RECOG_COMPLETE:I = 0x0

.field public static final RECOG_FAIL:I = 0x3

.field public static final RECOG_INIT_FAIL:I = 0x2

.field private static RECOG_LANGUAGE:[I = null

.field private static RECOG_MODE:I = 0x0

.field public static final RECOG_OK:I = 0x0

.field public static final RECOG_PROCESSING:I = 0x1

.field private static RECOG_TYPE:I

.field private static final TAG:Ljava/lang/String;

.field static isRecogCanceled:Z

.field private static mImageData:[I

.field public static mLinePerWordNum:[I

.field protected static mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

.field public static mLookUpWord:Ljava/lang/String;

.field public static mLookUpWordNum:I

.field public static mLookUpWordRect:[Landroid/graphics/Rect;

.field protected static mOCRState:I

.field public static mPreProcessImageHeight:I

.field public static mPreProcessImageWidth:I

.field private static mPreviewCropMode:Z

.field private static mPreviewData:[B

.field private static mRegionBottom:I

.field private static mRegionLeft:I

.field private static mRegionRight:I

.field private static mRegionTop:I

.field public static mSpecialWordBlockIndex:[I

.field public static mSpecialWordLineIndex:[I

.field public static mSpecialWordNum:I

.field public static mSpecialWordRect:[Landroid/graphics/Rect;

.field public static mSpecialWordText:[Ljava/lang/String;

.field public static mSpecialWordType:[I

.field private static mStrDBPath:Ljava/lang/String;

.field private static mStrLicensePath:Ljava/lang/String;

.field public static mWholeOrinWordBlockIndex:[I

.field public static mWholeOrinWordLineIndex:[I

.field public static mWholeOrinWordNum:I

.field public static mWholeOrinWordRect:[Landroid/graphics/Rect;

.field public static mWholeOrinWordText:[Ljava/lang/String;

.field public static mWholeRootWordNum:I

.field public static mWholeRootWordRect:[Landroid/graphics/Rect;

.field public static mWholeRootWordText:[Ljava/lang/String;

.field public static mWholeRootWordType:[I

.field public static mWholeWordBlockIndex:[I

.field public static mWholeWordInSpecial:[Z

.field public static mWholeWordLineIndex:[I

.field public static mWholeWordNum:I

.field public static mWholeWordRect:[Landroid/graphics/Rect;

.field public static mWholeWordText:[Ljava/lang/String;

.field public static mWholeWordType:[I

.field public static mjpegHeight:I

.field public static mjpegWidth:I

.field public static mpreviewDetectHeight:I

.field public static mpreviewDetectWidth:I

.field private static mpreviewExtraMarginBottom:I

.field private static mpreviewExtraMarginLeft:I

.field private static mpreviewExtraMarginRight:I

.field private static mpreviewExtraMarginTop:I

.field public static mpreviewHeight:I

.field public static mpreviewMarginBottom:I

.field public static mpreviewMarginLeft:I

.field public static mpreviewMarginRight:I

.field public static mpreviewMarginTop:I

.field public static mpreviewSensorHeight:I

.field public static mpreviewSensorWidth:I

.field public static mpreviewWidth:I

.field private static volatile secMOCR:Lcom/dmc/ocr/SecMOCR;


# instance fields
.field private final lock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 33
    const-class v1, Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    .line 74
    sget-object v1, Lcom/sec/android/app/bcocr/Feature;->BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/bcocr/CameraResolution;->getIntResolution(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    .line 83
    const/4 v1, 0x3

    sput v1, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 84
    sput-object v3, Lcom/dmc/ocr/SecMOCR;->mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .line 169
    sput v2, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    .line 170
    const/4 v1, 0x2

    sput v1, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    .line 172
    const/16 v1, 0x1b

    new-array v1, v1, [I

    sput-object v1, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    .line 174
    sput-object v3, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    .line 175
    sput-object v3, Lcom/dmc/ocr/SecMOCR;->mStrLicensePath:Ljava/lang/String;

    .line 180
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 181
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    .line 182
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    .line 183
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    .line 193
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    .line 194
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    .line 195
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginRight:I

    .line 196
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginBottom:I

    .line 198
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginLeft:I

    .line 199
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginTop:I

    .line 200
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginRight:I

    .line 201
    sput v2, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginBottom:I

    .line 203
    sput-boolean v2, Lcom/dmc/ocr/SecMOCR;->mPreviewCropMode:Z

    .line 204
    sput-boolean v2, Lcom/dmc/ocr/SecMOCR;->isRecogCanceled:Z

    .line 239
    sput v2, Lcom/dmc/ocr/SecMOCR;->mPreProcessImageWidth:I

    .line 240
    sput v2, Lcom/dmc/ocr/SecMOCR;->mPreProcessImageHeight:I

    .line 923
    :try_start_0
    const-string v1, "INTSIGBCREngine"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 928
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 924
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 926
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    .line 32
    return-void
.end method

.method public static native MOCR_DetectCardYUV([BII)[I
.end method

.method public static native MOCR_EnhanceCardImage([III)I
.end method

.method public static native MOCR_GetBCardRect([III)[I
.end method

.method public static native MOCR_GetVersion()Ljava/lang/String;
.end method

.method public static synchronized native declared-synchronized MOCR_Init([IILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)I
.end method

.method public static synchronized native declared-synchronized MOCR_Init([IILjava/lang/String;Ljava/lang/String;ZLandroid/content/Context;)I
.end method

.method public static native MOCR_Recognize_Image([IIIIIIIZZ)I
.end method

.method public static synchronized native declared-synchronized MOCR_Recognize_Preview([BIIIIII)I
.end method

.method public static native MOCR_TrimCardImage([III[I)[I
.end method

.method public static getInstance()Lcom/dmc/ocr/SecMOCR;
    .locals 2

    .prologue
    .line 248
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v0, :cond_1

    .line 249
    const-class v1, Lcom/dmc/ocr/SecMOCR;

    monitor-enter v1

    .line 250
    :try_start_0
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v0, :cond_0

    .line 251
    new-instance v0, Lcom/dmc/ocr/SecMOCR;

    invoke-direct {v0}, Lcom/dmc/ocr/SecMOCR;-><init>()V

    sput-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    .line 249
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    :cond_1
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    return-object v0

    .line 249
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getLookUpWord(Ljava/lang/String;)I
    .locals 5
    .param p0, "nWord"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 769
    sput v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 771
    sget v2, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v2, :cond_1

    .line 772
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "[getLookUpWord()] first, do recognize!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 845
    :cond_0
    :goto_0
    return v1

    .line 776
    :cond_1
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    if-nez v2, :cond_2

    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-nez v2, :cond_2

    .line 777
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "[getLookUpWord()] mWholeWordNum and mWholeOrinWordNum is 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    goto :goto_0

    .line 781
    :cond_2
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    .line 782
    sput v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 784
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-lez v2, :cond_6

    .line 785
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    new-array v2, v2, [Landroid/graphics/Rect;

    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    .line 787
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getLookUpWord] => "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    const-string v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 792
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget v1, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-lt v0, v1, :cond_4

    .line 812
    :cond_3
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "[getLookUpWord()] END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    goto :goto_0

    .line 793
    :cond_4
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    aget-object v1, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(?i).*"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 794
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    sget v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    .line 802
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 808
    :cond_5
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    if-ge v1, v2, :cond_3

    .line 792
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 816
    .end local v0    # "i":I
    :cond_6
    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    new-array v2, v2, [Landroid/graphics/Rect;

    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    .line 818
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getLookUpWord] => "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    const-string v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 823
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    sget v1, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    if-lt v0, v1, :cond_8

    .line 843
    :cond_7
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "[getLookUpWord()] END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    goto/16 :goto_0

    .line 824
    :cond_8
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    aget-object v1, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(?i).*"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 825
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    sget v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    .line 833
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 839
    :cond_9
    sget v1, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    sget v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    if-ge v1, v2, :cond_7

    .line 823
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static getRecognitionMode()I
    .locals 1

    .prologue
    .line 341
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    return v0
.end method

.method public static getRecognitionState()I
    .locals 1

    .prologue
    .line 349
    sget v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    return v0
.end method

.method public static getRecognitionType()I
    .locals 1

    .prologue
    .line 345
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    return v0
.end method

.method public static inCancel()I
    .locals 1

    .prologue
    .line 333
    sget-boolean v0, Lcom/dmc/ocr/SecMOCR;->isRecogCanceled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static inProgress(I)V
    .locals 1
    .param p0, "progress"    # I

    .prologue
    .line 327
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    if-nez v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 329
    :cond_0
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    invoke-interface {v0, p0}, Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;->onProgressChanged(I)V

    goto :goto_0
.end method

.method public static isSupported(II)Z
    .locals 3
    .param p0, "nWidth"    # I
    .param p1, "nHeight"    # I

    .prologue
    .line 363
    mul-int v0, p0, p1

    sget v1, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    if-le v0, v1, :cond_0

    .line 364
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "Not supported size max"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Width * Height = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " * "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , Max recog size is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 366
    sget v2, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 365
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v0, 0x0

    .line 370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static registerProgressChangedListener(Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .prologue
    .line 317
    if-eqz p0, :cond_0

    .line 318
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .line 320
    :cond_0
    return-void
.end method

.method public static resetRecogResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 449
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[resetRecogResult()]!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    sput v3, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 451
    sput v3, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 452
    sput v3, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 453
    sput v3, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    .line 455
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    .line 456
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    .line 457
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    .line 458
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordLineIndex:[I

    .line 459
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordBlockIndex:[I

    .line 461
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    .line 462
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    .line 463
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    .line 464
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordInSpecial:[Z

    .line 465
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordLineIndex:[I

    .line 466
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordBlockIndex:[I

    .line 468
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    .line 470
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    .line 471
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    .line 473
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    .line 474
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    .line 475
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    .line 476
    sput-object v2, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    .line 477
    return-void
.end method

.method public static setCancelFlag(Z)V
    .locals 0
    .param p0, "cancelFlag"    # Z

    .prologue
    .line 337
    sput-boolean p0, Lcom/dmc/ocr/SecMOCR;->isRecogCanceled:Z

    .line 338
    return-void
.end method

.method public static setDataBasePath(Ljava/lang/String;)V
    .locals 0
    .param p0, "strPath"    # Ljava/lang/String;

    .prologue
    .line 354
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    .line 355
    return-void
.end method

.method public static setJPEGData([IIIIIII)Z
    .locals 3
    .param p0, "ImgData"    # [I
    .param p1, "nImgWidth"    # I
    .param p2, "nImgHeight"    # I
    .param p3, "nRegionLeft"    # I
    .param p4, "nRegionTop"    # I
    .param p5, "nRegionRight"    # I
    .param p6, "nRegionBottom"    # I

    .prologue
    const/4 v0, 0x0

    .line 375
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "setJPEGData "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    if-nez p0, :cond_0

    .line 378
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "ImgData is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :goto_0
    return v0

    .line 380
    :cond_0
    array-length v1, p0

    sget v2, Lcom/dmc/ocr/SecMOCR;->MAX_RECOG_SIZE:I

    if-le v1, v2, :cond_1

    .line 381
    const/4 v1, 0x0

    sput-object v1, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    .line 382
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "Image is too big"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 386
    :cond_1
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    .line 387
    sput p1, Lcom/dmc/ocr/SecMOCR;->mjpegWidth:I

    .line 388
    sput p2, Lcom/dmc/ocr/SecMOCR;->mjpegHeight:I

    .line 390
    sput p3, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 391
    sput p4, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    .line 392
    sput p5, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    .line 393
    sput p6, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    .line 395
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    .line 396
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    .line 397
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginRight:I

    .line 398
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginBottom:I

    .line 400
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginLeft:I

    .line 401
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginTop:I

    .line 402
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginRight:I

    .line 403
    sput v0, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginBottom:I

    .line 405
    sput-boolean v0, Lcom/dmc/ocr/SecMOCR;->mPreviewCropMode:Z

    .line 407
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setLicenseBasePath(Ljava/lang/String;)V
    .locals 0
    .param p0, "strPath"    # Ljava/lang/String;

    .prologue
    .line 359
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mStrLicensePath:Ljava/lang/String;

    .line 360
    return-void
.end method

.method public static setPreviewCropMode(Z)V
    .locals 0
    .param p0, "isCropMode"    # Z

    .prologue
    .line 445
    sput-boolean p0, Lcom/dmc/ocr/SecMOCR;->mPreviewCropMode:Z

    .line 446
    return-void
.end method

.method public static setPreviewData([BIIIIIIIIIIIIIIZ)V
    .locals 2
    .param p0, "PrevData"    # [B
    .param p1, "nPrevDetectWidth"    # I
    .param p2, "nPrevDetectHeight"    # I
    .param p3, "nPrevSensorWidth"    # I
    .param p4, "nPrevSensorHeight"    # I
    .param p5, "nPrevWidth"    # I
    .param p6, "nPrevHeight"    # I
    .param p7, "nMarginLeft"    # I
    .param p8, "nMarginTop"    # I
    .param p9, "nMarginRight"    # I
    .param p10, "nMarginBottom"    # I
    .param p11, "nExtraMarginLeft"    # I
    .param p12, "nExtraMarginTop"    # I
    .param p13, "nExtraMarginRight"    # I
    .param p14, "nExtraMarginBottom"    # I
    .param p15, "isPrevCropMode"    # Z

    .prologue
    .line 416
    if-nez p0, :cond_0

    .line 417
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "PrevData is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    :goto_0
    return-void

    .line 421
    :cond_0
    sput-object p0, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    .line 423
    sput p3, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    .line 424
    sput p4, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorHeight:I

    .line 425
    sput p5, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    .line 426
    sput p6, Lcom/dmc/ocr/SecMOCR;->mpreviewHeight:I

    .line 428
    sput p1, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectWidth:I

    .line 429
    sput p2, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectHeight:I

    .line 431
    sput p7, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    .line 432
    sput p8, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    .line 433
    sput p9, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginRight:I

    .line 434
    sput p10, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginBottom:I

    .line 436
    sput p11, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginLeft:I

    .line 437
    sput p12, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginTop:I

    .line 438
    sput p13, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginRight:I

    .line 439
    sput p14, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginBottom:I

    .line 441
    sput-boolean p15, Lcom/dmc/ocr/SecMOCR;->mPreviewCropMode:Z

    goto :goto_0
.end method

.method public static setRecognitionLanguage([I)V
    .locals 8
    .param p0, "lang"    # [I

    .prologue
    const/16 v7, 0x10

    .line 268
    if-nez p0, :cond_0

    .line 269
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[setRecognitionLanguage()] lang[] is null!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :goto_0
    return-void

    .line 272
    :cond_0
    const/4 v1, 0x0

    .line 273
    .local v1, "Englang_set":Z
    const/4 v0, 0x0

    .line 276
    .local v0, "CJK_lang":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    array-length v5, v5

    if-lt v2, v5, :cond_4

    .line 281
    const/4 v2, 0x0

    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    array-length v5, p0

    if-lt v2, v5, :cond_5

    .line 301
    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 302
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    aput v7, v5, v3

    .line 305
    :cond_2
    const-string v4, "setRecognitionLanguage : "

    .line 306
    .local v4, "reg_lang":Ljava/lang/String;
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    if-eqz v5, :cond_3

    .line 307
    const/4 v2, 0x0

    :goto_3
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    array-length v5, v5

    if-lt v2, v5, :cond_9

    .line 311
    :cond_3
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    .end local v3    # "j":I
    .end local v4    # "reg_lang":Ljava/lang/String;
    :cond_4
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    const/4 v6, 0x0

    aput v6, v5, v2

    .line 276
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 282
    .restart local v3    # "j":I
    :cond_5
    aget v5, p0, v2

    if-eqz v5, :cond_1

    .line 285
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    aget v6, p0, v2

    aput v6, v5, v3

    .line 286
    add-int/lit8 v3, v3, 0x1

    .line 288
    aget v5, p0, v2

    if-ne v5, v7, :cond_6

    .line 289
    const/4 v1, 0x1

    .line 290
    const/4 v0, 0x0

    .line 293
    :cond_6
    if-nez v1, :cond_8

    .line 294
    aget v5, p0, v2

    const/16 v6, 0x41

    if-eq v5, v6, :cond_7

    .line 295
    aget v5, p0, v2

    const/16 v6, 0x42

    if-eq v5, v6, :cond_7

    .line 296
    aget v5, p0, v2

    const/16 v6, 0x44

    if-eq v5, v6, :cond_7

    aget v5, p0, v2

    const/16 v6, 0x43

    if-ne v5, v6, :cond_8

    .line 297
    :cond_7
    const/4 v0, 0x1

    .line 281
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 308
    .restart local v4    # "reg_lang":Ljava/lang/String;
    :cond_9
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    aget v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 307
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public static setRecognitionMode(I)V
    .locals 0
    .param p0, "mode"    # I

    .prologue
    .line 260
    sput p0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    .line 261
    return-void
.end method

.method public static setRecognitionType(I)V
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 264
    sput p0, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    .line 265
    return-void
.end method

.method public static unregisterProgressChangedListener()V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    sput-object v0, Lcom/dmc/ocr/SecMOCR;->mListener:Lcom/dmc/ocr/SecMOCR$OnProgressChangedListener;

    .line 324
    return-void
.end method


# virtual methods
.method public native MOCR_Close()V
.end method

.method public native MOCR_GetBlockIndexArray()[I
.end method

.method public native MOCR_GetIsBusinessCard()Z
.end method

.method public native MOCR_GetLineIndexArray()[I
.end method

.method public native MOCR_GetLinePerWordCount()[I
.end method

.method public native MOCR_GetOriginalBlockIndexArray()[I
.end method

.method public native MOCR_GetOriginalLineIndexArray()[I
.end method

.method public native MOCR_GetOriginalWholeWord()[Ljava/lang/String;
.end method

.method public native MOCR_GetOriginalWholeWordRect()[I
.end method

.method public native MOCR_GetRotatedValue()I
.end method

.method public native MOCR_GetSpecialWord()[Ljava/lang/String;
.end method

.method public native MOCR_GetSpecialWordRect()[I
.end method

.method public native MOCR_GetSpecialWordType()[I
.end method

.method public native MOCR_GetWholeWord()[Ljava/lang/String;
.end method

.method public native MOCR_GetWholeWordRect()[I
.end method

.method public native MOCR_GetWholeWordType()[I
.end method

.method public native MOCR_LookupSpecialWord()I
.end method

.method public native MOCR_LookupWholeWord()I
.end method

.method public native MOCR_LookupWord(Ljava/lang/String;)[I
.end method

.method public getRecogResult()I
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 491
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v7, "[getRecogResult()] START"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    sput v8, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 494
    sget v6, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v6, :cond_0

    .line 495
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v7, "[getRecogResult()] first, do recognize!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    sget v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 585
    :goto_0
    return v6

    .line 499
    :cond_0
    sget v6, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v6, :cond_1

    sget v6, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    if-nez v6, :cond_1

    .line 500
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v7, "getRecogResult(), mpreviewSensorWidth is not set (== 0)"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    sget v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    goto :goto_0

    .line 504
    :cond_1
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_LookupSpecialWord()I

    move-result v6

    if-nez v6, :cond_10

    .line 505
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWordType()[I

    move-result-object v5

    .line 506
    .local v5, "type":[I
    if-eqz v5, :cond_2

    .line 507
    array-length v6, v5

    new-array v6, v6, [I

    sput-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    .line 508
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v5

    if-lt v2, v6, :cond_7

    .line 514
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWord()[Ljava/lang/String;

    move-result-object v4

    .line 516
    .local v4, "specialWord":[Ljava/lang/String;
    if-eqz v4, :cond_3

    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    if-eqz v6, :cond_3

    .line 517
    array-length v6, v4

    sput v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 518
    array-length v6, v4

    new-array v6, v6, [Ljava/lang/String;

    sput-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    .line 519
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v6, v4

    if-lt v2, v6, :cond_8

    .line 530
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWordRect()[I

    move-result-object v3

    .line 531
    .local v3, "nSpecialWordRect":[I
    if-eqz v3, :cond_4

    .line 532
    array-length v6, v3

    div-int/lit8 v6, v6, 0x4

    new-array v6, v6, [Landroid/graphics/Rect;

    sput-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    .line 533
    sget v6, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v6, :cond_d

    .line 534
    sget-boolean v6, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v6, :cond_c

    .line 535
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v6, v3

    if-lt v2, v6, :cond_b

    .line 561
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetLineIndexArray()[I

    move-result-object v1

    .line 562
    .local v1, "Line":[I
    if-eqz v1, :cond_5

    .line 563
    array-length v6, v1

    new-array v6, v6, [I

    sput-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordLineIndex:[I

    .line 564
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    array-length v6, v1

    if-lt v2, v6, :cond_e

    .line 570
    .end local v2    # "i":I
    :cond_5
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetBlockIndexArray()[I

    move-result-object v0

    .line 571
    .local v0, "Block":[I
    if-eqz v0, :cond_6

    .line 572
    array-length v6, v0

    new-array v6, v6, [I

    sput-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordBlockIndex:[I

    .line 573
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    array-length v6, v0

    if-lt v2, v6, :cond_f

    .line 583
    .end local v0    # "Block":[I
    .end local v1    # "Line":[I
    .end local v2    # "i":I
    .end local v3    # "nSpecialWordRect":[I
    .end local v4    # "specialWord":[Ljava/lang/String;
    .end local v5    # "type":[I
    :cond_6
    :goto_6
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v7, "[getRecogResult()] END"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    sget v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    goto :goto_0

    .line 509
    .restart local v2    # "i":I
    .restart local v5    # "type":[I
    :cond_7
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    aget v7, v5, v2

    aput v7, v6, v2

    .line 510
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[mSpecialWordType("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    aget v8, v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 520
    .restart local v4    # "specialWord":[Ljava/lang/String;
    :cond_8
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    aget v6, v6, v2

    const/16 v7, 0xb

    if-eq v6, v7, :cond_9

    .line 521
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    aget v6, v6, v2

    const/16 v7, 0xc

    if-eq v6, v7, :cond_9

    .line 522
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    aget v6, v6, v2

    const/16 v7, 0xd

    if-ne v6, v7, :cond_a

    .line 523
    :cond_9
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    aget-object v7, v4, v2

    invoke-virtual {p0, v7}, Lcom/dmc/ocr/SecMOCR;->getTextRemovedSpecialCharInPhoneNum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 526
    :goto_7
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[mSpecialWordText("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 525
    :cond_a
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    aget-object v7, v4, v2

    aput-object v7, v6, v2

    goto :goto_7

    .line 536
    .restart local v3    # "nSpecialWordRect":[I
    :cond_b
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v7, v2, 0x4

    new-instance v8, Landroid/graphics/Rect;

    .line 537
    aget v9, v3, v2

    mul-int/lit8 v9, v9, 0x4

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    .line 538
    add-int/lit8 v10, v2, 0x1

    aget v10, v3, v10

    mul-int/lit8 v10, v10, 0x2

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 539
    add-int/lit8 v11, v2, 0x2

    aget v11, v3, v11

    mul-int/lit8 v11, v11, 0x4

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    .line 540
    add-int/lit8 v12, v2, 0x3

    aget v12, v3, v12

    mul-int/lit8 v12, v12, 0x2

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v12, v13

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 536
    aput-object v8, v6, v7

    .line 535
    add-int/lit8 v2, v2, 0x4

    goto/16 :goto_3

    .line 543
    .end local v2    # "i":I
    :cond_c
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    array-length v6, v3

    if-ge v2, v6, :cond_4

    .line 544
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v7, v2, 0x4

    new-instance v8, Landroid/graphics/Rect;

    .line 545
    aget v9, v3, v2

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    .line 546
    add-int/lit8 v10, v2, 0x1

    aget v10, v3, v10

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 547
    add-int/lit8 v11, v2, 0x2

    aget v11, v3, v11

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    .line 548
    add-int/lit8 v12, v2, 0x3

    aget v12, v3, v12

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v12, v13

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 544
    aput-object v8, v6, v7

    .line 543
    add-int/lit8 v2, v2, 0x4

    goto :goto_8

    .line 552
    .end local v2    # "i":I
    :cond_d
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_9
    array-length v6, v3

    if-ge v2, v6, :cond_4

    .line 553
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v7, v2, 0x4

    new-instance v8, Landroid/graphics/Rect;

    aget v9, v3, v2

    .line 554
    add-int/lit8 v10, v2, 0x1

    aget v10, v3, v10

    .line 555
    add-int/lit8 v11, v2, 0x2

    aget v11, v3, v11

    .line 556
    add-int/lit8 v12, v2, 0x3

    aget v12, v3, v12

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 553
    aput-object v8, v6, v7

    .line 552
    add-int/lit8 v2, v2, 0x4

    goto :goto_9

    .line 565
    .restart local v1    # "Line":[I
    :cond_e
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordLineIndex:[I

    aget v7, v1, v2

    aput v7, v6, v2

    .line 564
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    .line 574
    .restart local v0    # "Block":[I
    :cond_f
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->mSpecialWordBlockIndex:[I

    aget v7, v0, v2

    aput v7, v6, v2

    .line 573
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    .line 579
    .end local v0    # "Block":[I
    .end local v1    # "Line":[I
    .end local v2    # "i":I
    .end local v3    # "nSpecialWordRect":[I
    .end local v4    # "specialWord":[Ljava/lang/String;
    .end local v5    # "type":[I
    :cond_10
    sput v8, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 580
    sget-object v6, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v7, "mSpecialWordNum = 0"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6
.end method

.method public getTextRemovedSpecialCharInPhoneNum(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "orinStr"    # Ljava/lang/String;

    .prologue
    .line 480
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 481
    :cond_0
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "getTextRemovedSpecialCharInPhoneNum:orinStr is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const-string v0, ""

    .line 487
    :goto_0
    return-object v0

    .line 484
    :cond_1
    const-string v0, ""

    .line 486
    .local v0, "outputString":Ljava/lang/String;
    const-string v1, "[^0-9+\\-_\\.]"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 487
    goto :goto_0
.end method

.method public getWholeRecogOriginalResultTranslator()V
    .locals 13

    .prologue
    .line 702
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[getWholeRecogOriginalResultTranslator()] START"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    const/4 v5, 0x0

    sput v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    .line 705
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetOriginalWholeWord()[Ljava/lang/String;

    move-result-object v4

    .line 706
    .local v4, "wholeWord":[Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 707
    array-length v5, v4

    sput v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordNum:I

    .line 708
    array-length v5, v4

    new-array v5, v5, [Ljava/lang/String;

    sput-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    .line 710
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v4

    if-lt v2, v5, :cond_4

    .line 715
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetOriginalWholeWordRect()[I

    move-result-object v3

    .line 716
    .local v3, "nwholeWordRect":[I
    if-eqz v3, :cond_1

    .line 717
    array-length v5, v3

    div-int/lit8 v5, v5, 0x4

    new-array v5, v5, [Landroid/graphics/Rect;

    sput-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    .line 719
    sget v5, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v5, :cond_7

    .line 720
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    array-length v5, v3

    if-lt v2, v5, :cond_5

    .line 743
    .end local v2    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetOriginalLineIndexArray()[I

    move-result-object v1

    .line 744
    .local v1, "Line":[I
    if-eqz v1, :cond_2

    .line 745
    array-length v5, v1

    new-array v5, v5, [I

    sput-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    .line 746
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v5, v1

    if-lt v2, v5, :cond_8

    .line 754
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetOriginalBlockIndexArray()[I

    move-result-object v0

    .line 755
    .local v0, "Block":[I
    if-eqz v0, :cond_3

    .line 756
    array-length v5, v0

    new-array v5, v5, [I

    sput-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    .line 757
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_9

    .line 765
    .end local v2    # "i":I
    :cond_3
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[getWholeRecogOriginalResultTranslator()] END"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    return-void

    .line 711
    .end local v0    # "Block":[I
    .end local v1    # "Line":[I
    .end local v3    # "nwholeWordRect":[I
    .restart local v2    # "i":I
    :cond_4
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordText:[Ljava/lang/String;

    aget-object v6, v4, v2

    aput-object v6, v5, v2

    .line 710
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 721
    .restart local v3    # "nwholeWordRect":[I
    :cond_5
    sget-boolean v5, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v5, :cond_6

    .line 722
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v6, v2, 0x4

    new-instance v7, Landroid/graphics/Rect;

    aget v8, v3, v2

    mul-int/lit8 v8, v8, 0x2

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v8, v9

    .line 723
    add-int/lit8 v9, v2, 0x1

    aget v9, v3, v9

    mul-int/lit8 v9, v9, 0x2

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    .line 724
    add-int/lit8 v10, v2, 0x2

    aget v10, v3, v10

    mul-int/lit8 v10, v10, 0x2

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 725
    add-int/lit8 v11, v2, 0x3

    aget v11, v3, v11

    mul-int/lit8 v11, v11, 0x2

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 722
    aput-object v7, v5, v6

    .line 720
    :goto_4
    add-int/lit8 v2, v2, 0x4

    goto :goto_1

    .line 727
    :cond_6
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v6, v2, 0x4

    new-instance v7, Landroid/graphics/Rect;

    aget v8, v3, v2

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v8, v9

    .line 728
    add-int/lit8 v9, v2, 0x1

    aget v9, v3, v9

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    .line 729
    add-int/lit8 v10, v2, 0x2

    aget v10, v3, v10

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 730
    add-int/lit8 v11, v2, 0x3

    aget v11, v3, v11

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 727
    aput-object v7, v5, v6

    goto :goto_4

    .line 734
    .end local v2    # "i":I
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    array-length v5, v3

    if-ge v2, v5, :cond_1

    .line 735
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v6, v2, 0x4

    new-instance v7, Landroid/graphics/Rect;

    aget v8, v3, v2

    .line 736
    add-int/lit8 v9, v2, 0x1

    aget v9, v3, v9

    .line 737
    add-int/lit8 v10, v2, 0x2

    aget v10, v3, v10

    .line 738
    add-int/lit8 v11, v2, 0x3

    aget v11, v3, v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 735
    aput-object v7, v5, v6

    .line 734
    add-int/lit8 v2, v2, 0x4

    goto :goto_5

    .line 747
    .restart local v1    # "Line":[I
    :cond_8
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordLineIndex:[I

    aget v6, v1, v2

    aput v6, v5, v2

    .line 746
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 758
    .restart local v0    # "Block":[I
    :cond_9
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->mWholeOrinWordBlockIndex:[I

    aget v6, v0, v2

    aput v6, v5, v2

    .line 757
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3
.end method

.method public getWholeRecogResult()I
    .locals 15

    .prologue
    .line 589
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v8, "[getWholeRecogResult()] START"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    const/4 v7, 0x0

    sput v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 592
    sget v7, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v7, :cond_0

    .line 593
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v8, "[getWholeRecogResult()] first, do recognize!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    sget v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 697
    :goto_0
    return v7

    .line 597
    :cond_0
    sget v7, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v7, :cond_1

    sget v7, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    if-nez v7, :cond_1

    .line 598
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v8, "getWholeRecogResult(), mpreviewSensorWidth is not set (== 0)"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    sget v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    goto :goto_0

    .line 602
    :cond_1
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_LookupWholeWord()I

    move-result v7

    if-nez v7, :cond_13

    .line 603
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWord()[Ljava/lang/String;

    move-result-object v6

    .line 604
    .local v6, "wholeWord":[Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 605
    array-length v7, v6

    sput v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 606
    array-length v7, v6

    new-array v7, v7, [Ljava/lang/String;

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    .line 608
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, v6

    if-lt v2, v7, :cond_9

    .line 614
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWordRect()[I

    move-result-object v4

    .line 615
    .local v4, "nwholeWordRect":[I
    if-eqz v4, :cond_3

    array-length v7, v4

    const/4 v8, 0x4

    if-lt v7, v8, :cond_3

    .line 616
    array-length v7, v4

    div-int/lit8 v7, v7, 0x4

    new-array v7, v7, [Landroid/graphics/Rect;

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    .line 618
    sget v7, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v7, :cond_c

    .line 619
    sget-boolean v7, Lcom/sec/android/app/bcocr/Feature;->USE_PREVIEW_RECOG_FAST_MODE:Z

    if-eqz v7, :cond_b

    .line 620
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v7, v4

    if-lt v2, v7, :cond_a

    .line 646
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWordType()[I

    move-result-object v5

    .line 647
    .local v5, "type":[I
    if-eqz v5, :cond_4

    .line 648
    array-length v7, v5

    new-array v7, v7, [I

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    .line 649
    array-length v7, v5

    new-array v7, v7, [Z

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordInSpecial:[Z

    .line 650
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v7, v5

    if-lt v2, v7, :cond_d

    .line 662
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetLineIndexArray()[I

    move-result-object v1

    .line 663
    .local v1, "Line":[I
    if-eqz v1, :cond_5

    .line 664
    array-length v7, v1

    new-array v7, v7, [I

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordLineIndex:[I

    .line 665
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    array-length v7, v1

    if-lt v2, v7, :cond_10

    .line 671
    .end local v2    # "i":I
    :cond_5
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetBlockIndexArray()[I

    move-result-object v0

    .line 672
    .local v0, "Block":[I
    if-eqz v0, :cond_6

    .line 673
    array-length v7, v0

    new-array v7, v7, [I

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordBlockIndex:[I

    .line 674
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    array-length v7, v0

    if-lt v2, v7, :cond_11

    .line 680
    .end local v2    # "i":I
    :cond_6
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetLinePerWordCount()[I

    move-result-object v3

    .line 681
    .local v3, "nlinePerWord":[I
    if-eqz v3, :cond_7

    .line 682
    array-length v7, v3

    new-array v7, v7, [I

    sput-object v7, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    .line 683
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    array-length v7, v3

    if-lt v2, v7, :cond_12

    .line 688
    .end local v2    # "i":I
    :cond_7
    sget v7, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    .line 689
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->getWholeRecogOriginalResultTranslator()V

    .line 695
    .end local v0    # "Block":[I
    .end local v1    # "Line":[I
    .end local v3    # "nlinePerWord":[I
    .end local v4    # "nwholeWordRect":[I
    .end local v5    # "type":[I
    .end local v6    # "wholeWord":[Ljava/lang/String;
    :cond_8
    :goto_7
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v8, "[getWholeRecogResult()] END"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    sget v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    goto/16 :goto_0

    .line 609
    .restart local v2    # "i":I
    .restart local v6    # "wholeWord":[Ljava/lang/String;
    :cond_9
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    aget-object v8, v6, v2

    aput-object v8, v7, v2

    .line 608
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 621
    .restart local v4    # "nwholeWordRect":[I
    :cond_a
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v8, v2, 0x4

    new-instance v9, Landroid/graphics/Rect;

    .line 622
    aget v10, v4, v2

    mul-int/lit8 v10, v10, 0x4

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 623
    add-int/lit8 v11, v2, 0x1

    aget v11, v4, v11

    mul-int/lit8 v11, v11, 0x2

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    .line 624
    add-int/lit8 v12, v2, 0x2

    aget v12, v4, v12

    mul-int/lit8 v12, v12, 0x4

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v12, v13

    .line 625
    add-int/lit8 v13, v2, 0x3

    aget v13, v4, v13

    mul-int/lit8 v13, v13, 0x2

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v13, v14

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v13, v14

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v13, v14

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 621
    aput-object v9, v7, v8

    .line 620
    add-int/lit8 v2, v2, 0x4

    goto/16 :goto_2

    .line 628
    .end local v2    # "i":I
    :cond_b
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    array-length v7, v4

    if-ge v2, v7, :cond_3

    .line 629
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v8, v2, 0x4

    new-instance v9, Landroid/graphics/Rect;

    .line 630
    aget v10, v4, v2

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    .line 631
    add-int/lit8 v11, v2, 0x1

    aget v11, v4, v11

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    .line 632
    add-int/lit8 v12, v2, 0x2

    aget v12, v4, v12

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    add-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v12, v13

    sget v13, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v12, v13

    .line 633
    add-int/lit8 v13, v2, 0x3

    aget v13, v4, v13

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    add-int/2addr v13, v14

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v13, v14

    sget v14, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v13, v14

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 629
    aput-object v9, v7, v8

    .line 628
    add-int/lit8 v2, v2, 0x4

    goto :goto_8

    .line 637
    .end local v2    # "i":I
    :cond_c
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_9
    array-length v7, v4

    if-ge v2, v7, :cond_3

    .line 638
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v8, v2, 0x4

    new-instance v9, Landroid/graphics/Rect;

    aget v10, v4, v2

    .line 639
    add-int/lit8 v11, v2, 0x1

    aget v11, v4, v11

    .line 640
    add-int/lit8 v12, v2, 0x2

    aget v12, v4, v12

    .line 641
    add-int/lit8 v13, v2, 0x3

    aget v13, v4, v13

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 638
    aput-object v9, v7, v8

    .line 637
    add-int/lit8 v2, v2, 0x4

    goto :goto_9

    .line 651
    .restart local v5    # "type":[I
    :cond_d
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    aget v8, v5, v2

    aput v8, v7, v2

    .line 652
    aget v7, v5, v2

    const/16 v8, 0xb

    if-lt v7, v8, :cond_e

    aget v7, v5, v2

    const/16 v8, 0xf

    if-le v7, v8, :cond_f

    .line 653
    :cond_e
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordInSpecial:[Z

    const/4 v8, 0x0

    aput-boolean v8, v7, v2

    .line 650
    :goto_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 656
    :cond_f
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordInSpecial:[Z

    const/4 v8, 0x1

    aput-boolean v8, v7, v2

    goto :goto_a

    .line 666
    .restart local v1    # "Line":[I
    :cond_10
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordLineIndex:[I

    aget v8, v1, v2

    aput v8, v7, v2

    .line 665
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    .line 675
    .restart local v0    # "Block":[I
    :cond_11
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordBlockIndex:[I

    aget v8, v0, v2

    aput v8, v7, v2

    .line 674
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    .line 684
    .restart local v3    # "nlinePerWord":[I
    :cond_12
    sget-object v7, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    aget v8, v3, v2

    aput v8, v7, v2

    .line 683
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    .line 692
    .end local v0    # "Block":[I
    .end local v1    # "Line":[I
    .end local v2    # "i":I
    .end local v3    # "nlinePerWord":[I
    .end local v4    # "nwholeWordRect":[I
    .end local v5    # "type":[I
    .end local v6    # "wholeWord":[Ljava/lang/String;
    :cond_13
    const/4 v7, 0x0

    sput v7, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    goto/16 :goto_7
.end method

.method public startRecognition(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 850
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[startRecognize()] START : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    const/4 v10, 0x0

    .line 854
    .local v10, "nRet":I
    sget v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-ne v0, v6, :cond_0

    .line 855
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[startRecognize()] error!! now processing...."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    :goto_0
    return-void

    .line 859
    :cond_0
    invoke-static {v6}, Lcom/dmc/ocr/SecMOCR;->setCancelFlag(Z)V

    .line 860
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 861
    invoke-static {v3}, Lcom/dmc/ocr/SecMOCR;->setCancelFlag(Z)V

    .line 864
    const/4 v0, 0x1

    :try_start_0
    sput v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 866
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    sget v1, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    sget-object v2, Lcom/dmc/ocr/SecMOCR;->mStrLicensePath:Ljava/lang/String;

    sget-object v3, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/dmc/ocr/SecMOCR;->MOCR_Init([IILjava/lang/String;Ljava/lang/String;ZLandroid/content/Context;)I

    move-result v10

    .line 868
    if-eqz v10, :cond_1

    .line 869
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Initial Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    const/4 v0, 0x2

    sput v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 871
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 874
    :cond_1
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v0, :cond_4

    .line 876
    sget-boolean v0, Lcom/dmc/ocr/SecMOCR;->mPreviewCropMode:Z

    if-eqz v0, :cond_3

    .line 877
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    sget v1, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectWidth:I

    .line 878
    sget v2, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectHeight:I

    sget v3, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginLeft:I

    sget v4, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginTop:I

    .line 879
    sget v5, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectWidth:I

    sget v6, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginRight:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    .line 880
    sget v6, Lcom/dmc/ocr/SecMOCR;->mpreviewDetectHeight:I

    sget v7, Lcom/dmc/ocr/SecMOCR;->mpreviewExtraMarginBottom:I

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    .line 877
    invoke-static/range {v0 .. v6}, Lcom/dmc/ocr/SecMOCR;->MOCR_Recognize_Preview([BIIIIII)I

    move-result v10

    .line 896
    :goto_1
    if-nez v10, :cond_7

    .line 897
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Recognization success : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    const/4 v0, 0x0

    sput v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 911
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 914
    :goto_3
    sput-object v11, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    .line 915
    sput-object v11, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    .line 917
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[startRecognize()] END"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 882
    :cond_3
    :try_start_1
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    sget v1, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    .line 883
    sget v2, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorHeight:I

    sget v3, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginLeft:I

    sget v4, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginTop:I

    .line 884
    sget v5, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    sget v6, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginRight:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    sget v6, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorHeight:I

    .line 885
    sget v7, Lcom/dmc/ocr/SecMOCR;->mpreviewMarginBottom:I

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    .line 882
    invoke-static/range {v0 .. v6}, Lcom/dmc/ocr/SecMOCR;->MOCR_Recognize_Preview([BIIIIII)I

    move-result v10

    .line 887
    goto :goto_1

    :cond_4
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-ne v0, v6, :cond_5

    .line 888
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    sget v1, Lcom/dmc/ocr/SecMOCR;->mjpegWidth:I

    sget v2, Lcom/dmc/ocr/SecMOCR;->mjpegHeight:I

    sget v3, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 889
    sget v4, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    sget v5, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    sget v6, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 888
    invoke-static/range {v0 .. v8}, Lcom/dmc/ocr/SecMOCR;->MOCR_Recognize_Image([IIIIIIIZZ)I

    move-result v10

    .line 890
    goto :goto_1

    :cond_5
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-ne v0, v7, :cond_6

    .line 891
    const/4 v10, 0x0

    .line 892
    goto :goto_1

    .line 893
    :cond_6
    const/4 v10, 0x1

    goto :goto_1

    .line 899
    :cond_7
    if-eqz v10, :cond_2

    .line 900
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Recognization fail : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->resetRecogResult()V

    .line 902
    const/4 v0, 0x3

    sput v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 903
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 905
    :catch_0
    move-exception v9

    .line 906
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 907
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "startRecognize() Exception error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    const/4 v0, 0x3

    sput v0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 909
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 911
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_3

    .line 910
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 911
    iget-object v1, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 912
    throw v0
.end method
