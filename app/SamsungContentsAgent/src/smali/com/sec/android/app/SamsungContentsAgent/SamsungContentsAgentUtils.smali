.class public Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;
.super Ljava/lang/Object;
.source "SamsungContentsAgentUtils.java"


# static fields
.field static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "SViewerAgentUtils"

    sput-object v0, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private static byteToInt([BLjava/nio/ByteOrder;)I
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "order"    # Ljava/nio/ByteOrder;

    .prologue
    .line 61
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 62
    .local v0, "buff":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 63
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 64
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 65
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    return v1
.end method

.method public static getMimetypeFromSCCFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x4

    .line 19
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 20
    .local v8, "sccFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 21
    .local v2, "fis":Ljava/io/FileInputStream;
    new-array v6, v9, [B

    .line 24
    .local v6, "pklenBuf":[B
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    const-wide/16 v9, 0x16

    :try_start_1
    invoke-virtual {v3, v9, v10}, Ljava/io/FileInputStream;->skip(J)J

    .line 26
    const/4 v9, 0x0

    const/4 v10, 0x4

    invoke-virtual {v3, v6, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    .line 27
    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-static {v6, v9}, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->byteToInt([BLjava/nio/ByteOrder;)I

    move-result v5

    .line 29
    .local v5, "pklen":I
    const/16 v9, 0x64

    if-le v5, v9, :cond_0

    .line 30
    sget-object v9, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " is not SCC file"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 32
    const-string v7, "ERR_NOT_SCC_FILE"

    move-object v2, v3

    .line 56
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "pklen":I
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_0
    return-object v7

    .line 35
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "pklen":I
    :cond_0
    :try_start_2
    new-array v4, v5, [B

    .line 36
    .local v4, "mimetypeBuf":[B
    const-wide/16 v9, 0xc

    invoke-virtual {v3, v9, v10}, Ljava/io/FileInputStream;->skip(J)J

    .line 37
    const/4 v9, 0x0

    invoke-virtual {v3, v4, v9, v5}, Ljava/io/FileInputStream;->read([BII)I

    .line 38
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 39
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([B)V

    .line 40
    .local v7, "result":Ljava/lang/String;
    const-string v9, "application/vnd.samsung.scc"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 41
    sget-object v9, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " is not SCC file"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 42
    const-string v7, "ERR_NOT_SCC_FILE"

    .end local v7    # "result":Ljava/lang/String;
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "result":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 44
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 46
    .end local v4    # "mimetypeBuf":[B
    .end local v5    # "pklen":I
    .end local v7    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    if-eqz v2, :cond_2

    .line 50
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 54
    :cond_2
    :goto_2
    sget-object v9, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " is not correct file"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget-object v9, Lcom/sec/android/app/SamsungContentsAgent/SamsungContentsAgentUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v7, "ERR_NOT_CORRECT_FILE"

    goto :goto_0

    .line 51
    :catch_1
    move-exception v1

    .line 52
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 46
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "e1":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method
