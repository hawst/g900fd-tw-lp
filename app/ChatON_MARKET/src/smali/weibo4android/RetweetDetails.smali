.class public Lweibo4android/RetweetDetails;
.super Lweibo4android/WeiboResponse;
.source "RetweetDetails.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x1b2c267fe22c03d6L


# instance fields
.field private retweetId:J

.field private retweetedAt:Ljava/util/Date;

.field private retweetingUser:Lweibo4android/User;


# direct methods
.method constructor <init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 77
    invoke-direct {p0, p1, p2, p3}, Lweibo4android/RetweetDetails;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 78
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 55
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 56
    invoke-direct {p0, p1, v0, p2}, Lweibo4android/RetweetDetails;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 57
    return-void
.end method

.method constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 61
    invoke-direct {p0, p1}, Lweibo4android/RetweetDetails;->init(Lweibo4android/org/json/JSONObject;)V

    .line 62
    return-void
.end method

.method static createRetweetDetails(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/RetweetDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 106
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 107
    new-instance v4, Lweibo4android/RetweetDetails;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_1

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 112
    :catch_1
    move-exception v0

    .line 113
    throw v0

    .line 109
    :cond_0
    return-object v3
.end method

.method static createRetweetDetails(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            "Lweibo4android/Weibo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/RetweetDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v4

    .line 120
    invoke-static {v4}, Lweibo4android/RetweetDetails;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 135
    :goto_0
    return-object v0

    .line 124
    :cond_0
    :try_start_0
    const-string v0, "retweets"

    invoke-static {v0, v4}, Lweibo4android/RetweetDetails;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 125
    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v1, "retweet_details"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 126
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 127
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 128
    :goto_1
    if-ge v2, v6, :cond_1

    .line 129
    invoke-interface {v5, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 130
    new-instance v7, Lweibo4android/RetweetDetails;

    invoke-direct {v7, p0, v0, p1}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 132
    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    const-string v0, "nil-classes"

    invoke-static {v0, v4}, Lweibo4android/RetweetDetails;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 3

    .prologue
    .line 81
    const-string v0, "retweet_details"

    invoke-static {v0, p2}, Lweibo4android/RetweetDetails;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 82
    const-string v0, "retweet_id"

    invoke-static {v0, p2}, Lweibo4android/RetweetDetails;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/RetweetDetails;->retweetId:J

    .line 83
    const-string v0, "retweeted_at"

    invoke-static {v0, p2}, Lweibo4android/RetweetDetails;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/RetweetDetails;->retweetedAt:Ljava/util/Date;

    .line 84
    new-instance v1, Lweibo4android/User;

    const-string v0, "retweeting_user"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v1, p1, v0, p3}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    iput-object v1, p0, Lweibo4android/RetweetDetails;->retweetingUser:Lweibo4android/User;

    .line 85
    return-void
.end method

.method private init(Lweibo4android/org/json/JSONObject;)V
    .locals 4

    .prologue
    .line 66
    :try_start_0
    const-string v0, "retweetId"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lweibo4android/RetweetDetails;->retweetId:J

    .line 67
    const-string v0, "retweetedAt"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/RetweetDetails;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/RetweetDetails;->retweetedAt:Ljava/util/Date;

    .line 68
    new-instance v0, Lweibo4android/User;

    const-string v1, "retweetingUser"

    invoke-virtual {p1, v1}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/RetweetDetails;->retweetingUser:Lweibo4android/User;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 142
    if-ne p0, p1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    instance-of v2, p1, Lweibo4android/RetweetDetails;

    if-nez v2, :cond_2

    move v0, v1

    .line 146
    goto :goto_0

    .line 149
    :cond_2
    check-cast p1, Lweibo4android/RetweetDetails;

    .line 151
    iget-wide v2, p0, Lweibo4android/RetweetDetails;->retweetId:J

    iget-wide v4, p1, Lweibo4android/RetweetDetails;->retweetId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getRetweetId()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lweibo4android/RetweetDetails;->retweetId:J

    return-wide v0
.end method

.method public getRetweetedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lweibo4android/RetweetDetails;->retweetedAt:Ljava/util/Date;

    return-object v0
.end method

.method public getRetweetingUser()Lweibo4android/User;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lweibo4android/RetweetDetails;->retweetingUser:Lweibo4android/User;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 156
    iget-wide v0, p0, Lweibo4android/RetweetDetails;->retweetId:J

    iget-wide v2, p0, Lweibo4android/RetweetDetails;->retweetId:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/RetweetDetails;->retweetedAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/RetweetDetails;->retweetingUser:Lweibo4android/User;

    invoke-virtual {v1}, Lweibo4android/User;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RetweetDetails{retweetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/RetweetDetails;->retweetId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/RetweetDetails;->retweetedAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetingUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/RetweetDetails;->retweetingUser:Lweibo4android/User;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
