.class public Lweibo4android/WeiboStream;
.super Lweibo4android/WeiboSupport;
.source "WeiboStream.java"


# static fields
.field private static final DEBUG:Z


# instance fields
.field private handler:Lweibo4android/WeiboStream$StreamHandlingThread;

.field private retryPerMinutes:I

.field private statusListener:Lweibo4android/StatusListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lweibo4android/Configuration;->getDebug()Z

    move-result v0

    sput-boolean v0, Lweibo4android/WeiboStream;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lweibo4android/WeiboSupport;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lweibo4android/WeiboStream;->retryPerMinutes:I

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lweibo4android/WeiboSupport;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lweibo4android/WeiboStream;->retryPerMinutes:I

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lweibo4android/StatusListener;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lweibo4android/WeiboSupport;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lweibo4android/WeiboStream;->retryPerMinutes:I

    .line 64
    iput-object p3, p0, Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lweibo4android/WeiboStream;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lweibo4android/WeiboStream;->retryPerMinutes:I

    return v0
.end method

.method static synthetic access$100(Lweibo4android/WeiboStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lweibo4android/WeiboStream;->log(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lweibo4android/WeiboStream;)Lweibo4android/StatusListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;

    return-object v0
.end method

.method static synthetic access$300(Lweibo4android/WeiboStream;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lweibo4android/WeiboStream;->log(Ljava/lang/String;)V

    return-void
.end method

.method private getStreamBaseURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 655
    iget-boolean v0, p0, Lweibo4android/WeiboStream;->USE_SSL:Z

    if-eqz v0, :cond_0

    const-string v0, "https://stream.t.sina.com.cn/"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http://stream.t.sina.com.cn/"

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 659
    sget-boolean v0, Lweibo4android/WeiboStream;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 660
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 662
    :cond_0
    return-void
.end method

.method private log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 665
    sget-boolean v0, Lweibo4android/WeiboStream;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->log(Ljava/lang/String;)V

    .line 668
    :cond_0
    return-void
.end method

.method private declared-synchronized startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V
    .locals 2

    .prologue
    .line 548
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lweibo4android/WeiboStream;->cleanup()V

    .line 549
    iget-object v0, p0, Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "StatusListener is not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 552
    :cond_0
    :try_start_1
    iput-object p1, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    .line 553
    iget-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    invoke-virtual {v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    monitor-exit p0

    return-void
.end method

.method private toFollowString([I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 485
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0xb

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 486
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, p1, v0

    .line 487
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 488
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 490
    :cond_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toTrackString([Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 537
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x14

    mul-int/lit8 v0, v0, 0x4

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 538
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 539
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 540
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 542
    :cond_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 538
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 544
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public birddog(I[I)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 366
    new-instance v0, Lweibo4android/WeiboStream$7;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$7;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 372
    return-void
.end method

.method public declared-synchronized cleanup()V
    .locals 1

    .prologue
    .line 557
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 559
    :try_start_1
    iget-object v0, p0, Lweibo4android/WeiboStream;->handler:Lweibo4android/WeiboStream$StreamHandlingThread;

    invoke-virtual {v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 557
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 560
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public filter(I[I[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 225
    new-instance v0, Lweibo4android/WeiboStream$4;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$4;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 231
    return-void
.end method

.method public firehose(I)V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lweibo4android/WeiboStream$1;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$1;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 90
    return-void
.end method

.method public follow([I)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 458
    new-instance v0, Lweibo4android/WeiboStream$9;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$9;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 464
    return-void
.end method

.method public bridge synthetic forceUsePost(Z)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->forceUsePost(Z)V

    return-void
.end method

.method public gardenhose()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 279
    new-instance v0, Lweibo4android/WeiboStream$5;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$5;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 285
    return-void
.end method

.method public getBirddogStream(I[I)Lweibo4android/StatusStream;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 395
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/WeiboStream;->getFilterStream(I[I[Ljava/lang/String;)Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getClientURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getClientURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getClientVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getClientVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilterStream(I[I[Ljava/lang/String;)Lweibo4android/StatusStream;
    .locals 5

    .prologue
    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 250
    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "count"

    invoke-direct {v1, v2, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    if-eqz p2, :cond_0

    array-length v1, p2

    if-lez v1, :cond_0

    .line 252
    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "follow"

    invoke-direct {p0, p2}, Lweibo4android/WeiboStream;->toFollowString([I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_0
    if-eqz p3, :cond_1

    array-length v1, p3

    if-lez v1, :cond_1

    .line 255
    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "track"

    invoke-direct {p0, p3}, Lweibo4android/WeiboStream;->toTrackString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    :cond_1
    :try_start_0
    new-instance v1, Lweibo4android/StatusStream;

    iget-object v2, p0, Lweibo4android/WeiboStream;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lweibo4android/WeiboStream;->getStreamBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "1/statuses/filter.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Lweibo4android/http/PostParameter;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/StatusStream;-><init>(Lweibo4android/http/Response;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 259
    :catch_0
    move-exception v0

    .line 260
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public getFirehoseStream(I)Lweibo4android/StatusStream;
    .locals 8

    .prologue
    .line 111
    :try_start_0
    new-instance v0, Lweibo4android/StatusStream;

    iget-object v1, p0, Lweibo4android/WeiboStream;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lweibo4android/WeiboStream;->getStreamBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1/statuses/firehose.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "count"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/StatusStream;-><init>(Lweibo4android/http/Response;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public getFollowStream([I)Lweibo4android/StatusStream;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 481
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lweibo4android/WeiboStream;->getFilterStream(I[I[Ljava/lang/String;)Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public getGardenhoseStream()Lweibo4android/StatusStream;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0}, Lweibo4android/WeiboStream;->getSampleStream()Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetStream()Lweibo4android/StatusStream;
    .locals 5

    .prologue
    .line 158
    :try_start_0
    new-instance v0, Lweibo4android/StatusStream;

    iget-object v1, p0, Lweibo4android/WeiboStream;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lweibo4android/WeiboStream;->getStreamBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1/statuses/retweet.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/StatusStream;-><init>(Lweibo4android/http/Response;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public getSampleStream()Lweibo4android/StatusStream;
    .locals 4

    .prologue
    .line 202
    :try_start_0
    new-instance v0, Lweibo4android/StatusStream;

    iget-object v1, p0, Lweibo4android/WeiboStream;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lweibo4android/WeiboStream;->getStreamBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1/statuses/sample.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/StatusStream;-><init>(Lweibo4android/http/Response;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 203
    :catch_0
    move-exception v0

    .line 204
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public getShadowStream(I[I)Lweibo4android/StatusStream;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 441
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/WeiboStream;->getFilterStream(I[I[Ljava/lang/String;)Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getSource()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpritzerStream()Lweibo4android/StatusStream;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 343
    invoke-virtual {p0}, Lweibo4android/WeiboStream;->getSampleStream()Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public getStatusListener()Lweibo4android/StatusListener;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;

    return-object v0
.end method

.method public getTrackStream([Ljava/lang/String;)Lweibo4android/StatusStream;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 533
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lweibo4android/WeiboStream;->getFilterStream(I[I[Ljava/lang/String;)Lweibo4android/StatusStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isUsePostForced()Z
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lweibo4android/WeiboSupport;->isUsePostForced()Z

    move-result v0

    return v0
.end method

.method public retweet()V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lweibo4android/WeiboStream$2;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$2;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 138
    return-void
.end method

.method public sample()V
    .locals 2

    .prologue
    .line 178
    new-instance v0, Lweibo4android/WeiboStream$3;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$3;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 184
    return-void
.end method

.method public bridge synthetic setClientURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setClientURL(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setClientVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setClientVersion(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setHttpConnectionTimeout(I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setHttpConnectionTimeout(I)V

    return-void
.end method

.method public bridge synthetic setHttpProxy(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setHttpProxy(Ljava/lang/String;I)V

    return-void
.end method

.method public bridge synthetic setHttpProxyAuth(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setHttpProxyAuth(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setHttpReadTimeout(I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setHttpReadTimeout(I)V

    return-void
.end method

.method public bridge synthetic setPassword(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setPassword(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setRetryCount(I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setRetryCount(I)V

    return-void
.end method

.method public bridge synthetic setRetryIntervalSecs(I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setRetryIntervalSecs(I)V

    return-void
.end method

.method public bridge synthetic setSource(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setSource(Ljava/lang/String;)V

    return-void
.end method

.method public setStatusListener(Lweibo4android/StatusListener;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;

    .line 571
    return-void
.end method

.method public bridge synthetic setUserAgent(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setUserAgent(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setUserId(Ljava/lang/String;)V

    return-void
.end method

.method public shadow(I[I)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 415
    new-instance v0, Lweibo4android/WeiboStream$8;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$8;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 421
    return-void
.end method

.method public spritzer()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 320
    new-instance v0, Lweibo4android/WeiboStream$6;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lweibo4android/WeiboStream$6;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 326
    return-void
.end method

.method public track([Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 519
    new-instance v0, Lweibo4android/WeiboStream$10;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lweibo4android/WeiboStream$10;-><init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;[Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream;->startHandler(Lweibo4android/WeiboStream$StreamHandlingThread;)V

    .line 525
    return-void
.end method
