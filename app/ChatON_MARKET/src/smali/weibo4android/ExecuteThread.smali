.class Lweibo4android/ExecuteThread;
.super Ljava/lang/Thread;
.source "Dispatcher.java"


# instance fields
.field private alive:Z

.field q:Lweibo4android/Dispatcher;


# direct methods
.method constructor <init>(Ljava/lang/String;Lweibo4android/Dispatcher;I)V
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/ExecuteThread;->alive:Z

    .line 112
    iput-object p2, p0, Lweibo4android/ExecuteThread;->q:Lweibo4android/Dispatcher;

    .line 113
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 123
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lweibo4android/ExecuteThread;->alive:Z

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lweibo4android/ExecuteThread;->q:Lweibo4android/Dispatcher;

    invoke-virtual {v0}, Lweibo4android/Dispatcher;->poll()Ljava/lang/Runnable;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_0

    .line 127
    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 133
    :cond_1
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/ExecuteThread;->alive:Z

    .line 117
    return-void
.end method
