.class public Lweibo4android/WeiboAdapter;
.super Ljava/lang/Object;
.source "WeiboAdapter.java"

# interfaces
.implements Lweibo4android/WeiboListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public blocked(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 305
    return-void
.end method

.method public created(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 191
    return-void
.end method

.method public createdBlock(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public createdFavorite(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public createdFriendship(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public deletedDirectMessage(Lweibo4android/DirectMessage;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 168
    return-void
.end method

.method public destroyed(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206
    return-void
.end method

.method public destroyedBlock(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public destroyedDirectMessage(Lweibo4android/DirectMessage;)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public destroyedFavorite(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public destroyedFriendship(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public destroyedStatus(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public disabledNotification(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public enabledNotification(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 282
    return-void
.end method

.method public followed(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 275
    return-void
.end method

.method public gotBlockingUsers(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 341
    return-void
.end method

.method public gotBlockingUsersIDs(Lweibo4android/IDs;)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public gotCurrentTrends(Lweibo4android/Trends;)V
    .locals 0

    .prologue
    .line 378
    return-void
.end method

.method public gotDailyTrends(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    return-void
.end method

.method public gotDirectMessages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    return-void
.end method

.method public gotDowntimeSchedule(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 360
    return-void
.end method

.method public gotExists(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 221
    return-void
.end method

.method public gotExistsBlock(Z)V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method public gotExistsFriendship(Z)V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public gotFavorites(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    return-void
.end method

.method public gotFeatured(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    return-void
.end method

.method public gotFollowers(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    return-void
.end method

.method public gotFollowersIDs(Lweibo4android/IDs;)V
    .locals 0

    .prologue
    .line 183
    return-void
.end method

.method public gotFriends(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    return-void
.end method

.method public gotFriendsIDs(Lweibo4android/IDs;)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public gotFriendsTimeline(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    return-void
.end method

.method public gotHomeTimeline(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    return-void
.end method

.method public gotMentions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    return-void
.end method

.method public gotPublicTimeline(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    return-void
.end method

.method public gotRateLimitStatus(Lweibo4android/RateLimitStatus;)V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method public gotReplies(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 93
    return-void
.end method

.method public gotRetweetedByMe(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    return-void
.end method

.method public gotRetweetedToMe(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    return-void
.end method

.method public gotRetweetsOfMe(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    return-void
.end method

.method public gotSentDirectMessages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    return-void
.end method

.method public gotShow(Lweibo4android/Status;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public gotShowStatus(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public gotTrends(Lweibo4android/Trends;)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public gotUserDetail(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public gotUserTimeline(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    return-void
.end method

.method public gotWeeklyTrends(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    return-void
.end method

.method public left(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 290
    return-void
.end method

.method public onException(Lweibo4android/WeiboException;I)V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method public retweetedStatus(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public searched(Lweibo4android/QueryResult;)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public sentDirectMessage(Lweibo4android/DirectMessage;)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public tested(Z)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public unblocked(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 320
    return-void
.end method

.method public updated(Lweibo4android/Status;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 81
    return-void
.end method

.method public updatedDeliverlyDevice(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public updatedLocation(Lweibo4android/User;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 236
    return-void
.end method

.method public updatedProfile(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public updatedProfileColors(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public updatedStatus(Lweibo4android/Status;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method
