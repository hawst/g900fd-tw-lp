.class public Lweibo4android/Comment;
.super Lweibo4android/WeiboResponse;
.source "Comment.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1650c3edbf9bb2a0L


# instance fields
.field private createdAt:Ljava/util/Date;

.field private id:J

.field private inReplyToScreenName:Ljava/lang/String;

.field private inReplyToStatusId:J

.field private inReplyToUserId:I

.field private isFavorited:Z

.field private isTruncated:Z

.field private latitude:D

.field private longitude:D

.field private retweetDetails:Lweibo4android/RetweetDetails;

.field private source:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private user:Lweibo4android/User;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 80
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 32
    iput-wide v0, p0, Lweibo4android/Comment;->latitude:D

    .line 33
    iput-wide v0, p0, Lweibo4android/Comment;->longitude:D

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 81
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 82
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/Comment;->id:J

    .line 83
    const-string v1, "text"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    .line 84
    const-string v1, "source"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    .line 85
    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v1, v2}, Lweibo4android/Comment;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    .line 87
    new-instance v1, Lweibo4android/User;

    const-string v2, "user"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v1, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 88
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 5

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 46
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 32
    iput-wide v0, p0, Lweibo4android/Comment;->latitude:D

    .line 33
    iput-wide v0, p0, Lweibo4android/Comment;->longitude:D

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 47
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 49
    :try_start_0
    const-string v0, "id"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/Comment;->id:J

    .line 50
    const-string v0, "text"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    .line 51
    const-string v0, "source"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    .line 52
    const-string v0, "created_at"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v2}, Lweibo4android/Comment;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    .line 54
    const-string v0, "user"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lweibo4android/User;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, v2}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :cond_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method constructor <init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 74
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 32
    iput-wide v0, p0, Lweibo4android/Comment;->latitude:D

    .line 33
    iput-wide v0, p0, Lweibo4android/Comment;->longitude:D

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 75
    invoke-direct {p0, p1, p2, p3}, Lweibo4android/Comment;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 76
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 39
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 32
    iput-wide v0, p0, Lweibo4android/Comment;->latitude:D

    .line 33
    iput-wide v0, p0, Lweibo4android/Comment;->longitude:D

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 40
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 41
    invoke-direct {p0, p1, v0, p2}, Lweibo4android/Comment;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 63
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 32
    iput-wide v0, p0, Lweibo4android/Comment;->latitude:D

    .line 33
    iput-wide v0, p0, Lweibo4android/Comment;->longitude:D

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 64
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Comment;->id:J

    .line 65
    const-string v0, "text"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    .line 66
    const-string v0, "source"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    .line 67
    const-string v0, "created_at"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/Comment;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    .line 68
    const-string v0, "user"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lweibo4android/User;

    const-string v1, "user"

    invoke-virtual {p1, v1}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 71
    :cond_0
    return-void
.end method

.method static constructComments(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 258
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 259
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 260
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 261
    new-instance v4, Lweibo4android/Comment;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/Comment;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_1

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 266
    :catch_1
    move-exception v0

    .line 267
    throw v0

    .line 263
    :cond_0
    return-object v3
.end method

.method static constructComments(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            "Lweibo4android/Weibo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 233
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v4

    .line 234
    invoke-static {v4}, Lweibo4android/Comment;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 249
    :goto_0
    return-object v0

    .line 238
    :cond_0
    :try_start_0
    const-string v0, "comments"

    invoke-static {v0, v4}, Lweibo4android/Comment;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 239
    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v1, "comment"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 240
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 242
    :goto_1
    if-ge v2, v6, :cond_1

    .line 243
    invoke-interface {v5, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 244
    new-instance v7, Lweibo4android/Comment;

    invoke-direct {v7, p0, v0, p1}, Lweibo4android/Comment;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 246
    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    const-string v0, "nil-classes"

    invoke-static {v0, v4}, Lweibo4android/Comment;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 3

    .prologue
    .line 91
    const-string v0, "comment"

    invoke-static {v0, p2}, Lweibo4android/Comment;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 92
    new-instance v1, Lweibo4android/User;

    const-string v0, "user"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v1, p1, v0, p3}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    iput-object v1, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    .line 93
    const-string v0, "id"

    invoke-static {v0, p2}, Lweibo4android/Comment;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Comment;->id:J

    .line 94
    const-string v0, "text"

    invoke-static {v0, p2}, Lweibo4android/Comment;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    .line 95
    const-string v0, "source"

    invoke-static {v0, p2}, Lweibo4android/Comment;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    .line 96
    const-string v0, "created_at"

    invoke-static {v0, p2}, Lweibo4android/Comment;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    .line 97
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 279
    if-nez p1, :cond_1

    move v0, v1

    .line 285
    :cond_0
    :goto_0
    return v0

    .line 282
    :cond_1
    if-eq p0, p1, :cond_0

    .line 285
    instance-of v2, p1, Lweibo4android/Comment;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/Comment;

    iget-wide v2, p1, Lweibo4android/Comment;->id:J

    iget-wide v4, p0, Lweibo4android/Comment;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lweibo4android/Comment;->id:J

    return-wide v0
.end method

.method public getInReplyToScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lweibo4android/Comment;->inReplyToScreenName:Ljava/lang/String;

    return-object v0
.end method

.method public getInReplyToStatusId()J
    .locals 2

    .prologue
    .line 155
    iget-wide v0, p0, Lweibo4android/Comment;->inReplyToStatusId:J

    return-wide v0
.end method

.method public getInReplyToUserId()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lweibo4android/Comment;->inReplyToUserId:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 184
    iget-wide v0, p0, Lweibo4android/Comment;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lweibo4android/Comment;->longitude:D

    return-wide v0
.end method

.method public getRetweetDetails()Lweibo4android/RetweetDetails;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lweibo4android/Comment;->retweetDetails:Lweibo4android/RetweetDetails;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Lweibo4android/User;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 274
    iget-wide v0, p0, Lweibo4android/Comment;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public isFavorited()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lweibo4android/Comment;->isFavorited:Z

    return v0
.end method

.method public isRetweet()Z
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lweibo4android/Comment;->retweetDetails:Lweibo4android/RetweetDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTruncated()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lweibo4android/Comment;->isTruncated:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Comment{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Comment;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", text=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isTruncated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/Comment;->isTruncated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToStatusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Comment;->inReplyToStatusId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Comment;->inReplyToUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFavorited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/Comment;->isFavorited:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToScreenName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->inReplyToScreenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Comment;->latitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Comment;->longitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->retweetDetails:Lweibo4android/RetweetDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Comment;->user:Lweibo4android/User;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
