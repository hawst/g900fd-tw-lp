.class public Lweibo4android/UserWapper;
.super Ljava/lang/Object;
.source "UserWapper.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x2b494bedb867c69cL


# instance fields
.field private nextCursor:J

.field private previousCursor:J

.field private users:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;JJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lweibo4android/UserWapper;->users:Ljava/util/List;

    .line 36
    iput-wide p2, p0, Lweibo4android/UserWapper;->previousCursor:J

    .line 37
    iput-wide p4, p0, Lweibo4android/UserWapper;->nextCursor:J

    .line 38
    return-void
.end method


# virtual methods
.method public getNextCursor()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lweibo4android/UserWapper;->nextCursor:J

    return-wide v0
.end method

.method public getPreviousCursor()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lweibo4android/UserWapper;->previousCursor:J

    return-wide v0
.end method

.method public getUsers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lweibo4android/UserWapper;->users:Ljava/util/List;

    return-object v0
.end method

.method public setNextCursor(J)V
    .locals 0

    .prologue
    .line 61
    iput-wide p1, p0, Lweibo4android/UserWapper;->nextCursor:J

    .line 62
    return-void
.end method

.method public setPreviousCursor(J)V
    .locals 0

    .prologue
    .line 53
    iput-wide p1, p0, Lweibo4android/UserWapper;->previousCursor:J

    .line 54
    return-void
.end method

.method public setUsers(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    iput-object p1, p0, Lweibo4android/UserWapper;->users:Ljava/util/List;

    .line 46
    return-void
.end method
