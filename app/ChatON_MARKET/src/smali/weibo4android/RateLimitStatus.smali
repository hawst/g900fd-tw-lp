.class public Lweibo4android/RateLimitStatus;
.super Lweibo4android/WeiboResponse;
.source "RateLimitStatus.java"


# static fields
.field private static final serialVersionUID:J = 0xcf6392515d72b83L


# instance fields
.field private hourlyLimit:I

.field private remainingHits:I

.field private resetTime:Ljava/util/Date;

.field private resetTimeInSeconds:I


# direct methods
.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 50
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 51
    const-string v1, "remaining-hits"

    invoke-static {v1, v0}, Lweibo4android/RateLimitStatus;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/RateLimitStatus;->remainingHits:I

    .line 52
    const-string v1, "hourly-limit"

    invoke-static {v1, v0}, Lweibo4android/RateLimitStatus;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/RateLimitStatus;->hourlyLimit:I

    .line 53
    const-string v1, "reset-time-in-seconds"

    invoke-static {v1, v0}, Lweibo4android/RateLimitStatus;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/RateLimitStatus;->resetTimeInSeconds:I

    .line 54
    const-string v1, "reset-time"

    const-string v2, "EEE MMM d HH:mm:ss z yyyy"

    invoke-static {v1, v0, v2}, Lweibo4android/RateLimitStatus;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/RateLimitStatus;->resetTime:Ljava/util/Date;

    .line 55
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 5

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 60
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 62
    :try_start_0
    const-string v0, "remaining_hits"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/RateLimitStatus;->remainingHits:I

    .line 63
    const-string v0, "hourly_limit"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/RateLimitStatus;->hourlyLimit:I

    .line 64
    const-string v0, "reset_time_in_seconds"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/RateLimitStatus;->resetTimeInSeconds:I

    .line 65
    const-string v0, "reset_time"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v2}, Lweibo4android/RateLimitStatus;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/RateLimitStatus;->resetTime:Ljava/util/Date;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method


# virtual methods
.method public getDateTime()Ljava/util/Date;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lweibo4android/RateLimitStatus;->resetTime:Ljava/util/Date;

    return-object v0
.end method

.method public getHourlyLimit()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lweibo4android/RateLimitStatus;->hourlyLimit:I

    return v0
.end method

.method public getRemainingHits()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lweibo4android/RateLimitStatus;->remainingHits:I

    return v0
.end method

.method public getResetTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lweibo4android/RateLimitStatus;->resetTime:Ljava/util/Date;

    return-object v0
.end method

.method public getResetTimeInSeconds()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lweibo4android/RateLimitStatus;->resetTimeInSeconds:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    const-string v1, "RateLimitStatus{remainingHits:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget v1, p0, Lweibo4android/RateLimitStatus;->remainingHits:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    const-string v1, ";hourlyLimit:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    iget v1, p0, Lweibo4android/RateLimitStatus;->hourlyLimit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    const-string v1, ";resetTimeInSeconds:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget v1, p0, Lweibo4android/RateLimitStatus;->resetTimeInSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, ";resetTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v1, p0, Lweibo4android/RateLimitStatus;->resetTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
