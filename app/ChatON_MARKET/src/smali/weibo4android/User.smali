.class public Lweibo4android/User;
.super Lweibo4android/WeiboResponse;
.source "User.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final POSSIBLE_ROOT_NAMES:[Ljava/lang/String;

.field private static final serialVersionUID:J = -0x581124472812f726L


# instance fields
.field private createdAt:Ljava/util/Date;

.field private description:Ljava/lang/String;

.field private favouritesCount:I

.field private followersCount:I

.field private following:Z

.field private friendsCount:I

.field private geoEnabled:Z

.field private id:I

.field private isProtected:Z

.field private location:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private notificationEnabled:Z

.field private profileBackgroundColor:Ljava/lang/String;

.field private profileBackgroundImageUrl:Ljava/lang/String;

.field private profileBackgroundTile:Ljava/lang/String;

.field private profileImageUrl:Ljava/lang/String;

.field private profileLinkColor:Ljava/lang/String;

.field private profileSidebarBorderColor:Ljava/lang/String;

.field private profileSidebarFillColor:Ljava/lang/String;

.field private profileTextColor:Ljava/lang/String;

.field private screenName:Ljava/lang/String;

.field private statusCreatedAt:Ljava/util/Date;

.field private statusFavorited:Z

.field private statusId:J

.field private statusInReplyToScreenName:Ljava/lang/String;

.field private statusInReplyToStatusId:J

.field private statusInReplyToUserId:I

.field private statusSource:Ljava/lang/String;

.field private statusText:Ljava/lang/String;

.field private statusTruncated:Z

.field private statusesCount:I

.field private timeZone:Ljava/lang/String;

.field private url:Ljava/lang/String;

.field private utcOffset:I

.field private verified:Z

.field private weibo:Lweibo4android/Weibo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sender"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "recipient"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "retweeting_user"

    aput-object v2, v0, v1

    sput-object v0, Lweibo4android/User;->POSSIBLE_ROOT_NAMES:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 62
    iput-wide v3, p0, Lweibo4android/User;->statusId:J

    .line 63
    iput-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    .line 65
    iput-boolean v2, p0, Lweibo4android/User;->statusTruncated:Z

    .line 66
    iput-wide v3, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/User;->statusInReplyToUserId:I

    .line 68
    iput-boolean v2, p0, Lweibo4android/User;->statusFavorited:Z

    .line 69
    iput-object v1, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    .line 98
    invoke-direct {p0, p2, p3}, Lweibo4android/User;->init(Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 99
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 91
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 62
    iput-wide v3, p0, Lweibo4android/User;->statusId:J

    .line 63
    iput-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    .line 65
    iput-boolean v2, p0, Lweibo4android/User;->statusTruncated:Z

    .line 66
    iput-wide v3, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/User;->statusInReplyToUserId:I

    .line 68
    iput-boolean v2, p0, Lweibo4android/User;->statusFavorited:Z

    .line 69
    iput-object v1, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 93
    invoke-direct {p0, v0, p2}, Lweibo4android/User;->init(Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 94
    return-void
.end method

.method constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 62
    iput-wide v3, p0, Lweibo4android/User;->statusId:J

    .line 63
    iput-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    .line 65
    iput-boolean v2, p0, Lweibo4android/User;->statusTruncated:Z

    .line 66
    iput-wide v3, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/User;->statusInReplyToUserId:I

    .line 68
    iput-boolean v2, p0, Lweibo4android/User;->statusFavorited:Z

    .line 69
    iput-object v1, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    .line 103
    invoke-direct {p0, p1}, Lweibo4android/User;->init(Lweibo4android/org/json/JSONObject;)V

    .line 104
    return-void
.end method

.method static constructResult(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 417
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v2

    .line 419
    :try_start_0
    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    .line 420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 421
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 422
    new-instance v4, Lweibo4android/User;

    invoke-virtual {v2, v1}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 425
    :catch_0
    move-exception v0

    .line 427
    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static constructUsers(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 373
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 374
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 375
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 376
    new-instance v4, Lweibo4android/User;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_1

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 381
    :catch_1
    move-exception v0

    .line 382
    throw v0

    .line 378
    :cond_0
    return-object v3
.end method

.method public static constructUsers(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            "Lweibo4android/Weibo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 293
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v4

    .line 294
    invoke-static {v4}, Lweibo4android/User;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 321
    :goto_0
    return-object v0

    .line 298
    :cond_0
    :try_start_0
    const-string v0, "users"

    invoke-static {v0, v4}, Lweibo4android/User;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 308
    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 309
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 311
    :goto_1
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 312
    invoke-interface {v5, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 313
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "user"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 314
    new-instance v6, Lweibo4android/User;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v6, p0, v0, p1}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 318
    goto :goto_0

    .line 319
    :catch_0
    move-exception v0

    .line 320
    invoke-static {v4}, Lweibo4android/User;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    .line 323
    :cond_3
    throw v0
.end method

.method public static constructWapperUsers(Lweibo4android/http/Response;)Lweibo4android/UserWapper;
    .locals 9

    .prologue
    .line 392
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v6

    .line 394
    :try_start_0
    const-string v0, "users"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v2

    .line 395
    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    .line 396
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 397
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 398
    new-instance v4, Lweibo4android/User;

    invoke-virtual {v2, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 400
    :cond_0
    const-string v0, "previous_curosr"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 401
    const-string v0, "next_cursor"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 402
    const-wide/16 v7, -0x1

    cmp-long v0, v4, v7

    if-nez v0, :cond_1

    .line 403
    const-string v0, "nextCursor"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 405
    :cond_1
    new-instance v0, Lweibo4android/UserWapper;

    invoke-direct/range {v0 .. v5}, Lweibo4android/UserWapper;-><init>(Ljava/util/List;JJ)V
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public static constructWapperUsers(Lweibo4android/http/Response;Lweibo4android/Weibo;)Lweibo4android/UserWapper;
    .locals 12

    .prologue
    const/4 v10, 0x0

    const-wide/16 v2, 0x0

    .line 330
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v11

    .line 331
    invoke-static {v11}, Lweibo4android/User;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    new-instance v0, Lweibo4android/UserWapper;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lweibo4android/UserWapper;-><init>(Ljava/util/List;JJ)V

    .line 362
    :goto_0
    return-object v0

    .line 335
    :cond_0
    :try_start_0
    const-string v0, "users_list"

    invoke-static {v0, v11}, Lweibo4android/User;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 336
    invoke-interface {v11}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 337
    const-string v0, "users"

    invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 338
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 339
    if-nez v1, :cond_1

    .line 340
    new-instance v4, Lweibo4android/UserWapper;

    new-instance v5, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v4 .. v9}, Lweibo4android/UserWapper;-><init>(Ljava/util/List;JJ)V

    move-object v0, v4

    goto :goto_0

    .line 343
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 344
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 345
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v10

    .line 347
    :goto_1
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 348
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 349
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "user"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 350
    new-instance v7, Lweibo4android/User;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v7, p0, v0, p1}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 354
    :cond_3
    const-string v0, "previous_curosr"

    invoke-static {v0, v4}, Lweibo4android/User;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v6

    .line 355
    const-string v0, "next_curosr"

    invoke-static {v0, v4}, Lweibo4android/User;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v8

    .line 356
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_4

    .line 357
    const-string v0, "nextCurosr"

    invoke-static {v0, v4}, Lweibo4android/User;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v8

    .line 359
    :cond_4
    new-instance v4, Lweibo4android/UserWapper;

    invoke-direct/range {v4 .. v9}, Lweibo4android/UserWapper;-><init>(Ljava/util/List;JJ)V
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    goto :goto_0

    .line 360
    :catch_0
    move-exception v0

    .line 361
    invoke-static {v11}, Lweibo4android/User;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 362
    new-instance v0, Lweibo4android/UserWapper;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lweibo4android/UserWapper;-><init>(Ljava/util/List;JJ)V

    goto/16 :goto_0

    .line 364
    :cond_5
    throw v0
.end method

.method private init(Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 3

    .prologue
    .line 153
    iput-object p2, p0, Lweibo4android/User;->weibo:Lweibo4android/Weibo;

    .line 154
    sget-object v0, Lweibo4android/User;->POSSIBLE_ROOT_NAMES:[Ljava/lang/String;

    invoke-static {v0, p1}, Lweibo4android/User;->ensureRootNodeNameIs([Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 155
    const-string v0, "id"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->id:I

    .line 156
    const-string v0, "name"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->name:Ljava/lang/String;

    .line 157
    const-string v0, "screen_name"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->screenName:Ljava/lang/String;

    .line 158
    const-string v0, "location"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->location:Ljava/lang/String;

    .line 159
    const-string v0, "description"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->description:Ljava/lang/String;

    .line 160
    const-string v0, "profile_image_url"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileImageUrl:Ljava/lang/String;

    .line 161
    const-string v0, "url"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->url:Ljava/lang/String;

    .line 162
    const-string v0, "protected"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->isProtected:Z

    .line 163
    const-string v0, "followers_count"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->followersCount:I

    .line 165
    const-string v0, "profile_background_color"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundColor:Ljava/lang/String;

    .line 166
    const-string v0, "profile_text_color"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileTextColor:Ljava/lang/String;

    .line 167
    const-string v0, "profile_link_color"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileLinkColor:Ljava/lang/String;

    .line 168
    const-string v0, "profile_sidebar_fill_color"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileSidebarFillColor:Ljava/lang/String;

    .line 169
    const-string v0, "profile_sidebar_border_color"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileSidebarBorderColor:Ljava/lang/String;

    .line 170
    const-string v0, "friends_count"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->friendsCount:I

    .line 171
    const-string v0, "created_at"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->createdAt:Ljava/util/Date;

    .line 172
    const-string v0, "favourites_count"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->favouritesCount:I

    .line 173
    const-string v0, "utc_offset"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->utcOffset:I

    .line 174
    const-string v0, "time_zone"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->timeZone:Ljava/lang/String;

    .line 175
    const-string v0, "profile_background_image_url"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundImageUrl:Ljava/lang/String;

    .line 176
    const-string v0, "profile_background_tile"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundTile:Ljava/lang/String;

    .line 177
    const-string v0, "following"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->following:Z

    .line 178
    const-string v0, "notifications"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->notificationEnabled:Z

    .line 179
    const-string v0, "statuses_count"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->statusesCount:I

    .line 180
    const-string v0, "geo_enabled"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->geoEnabled:Z

    .line 181
    const-string v0, "verified"

    invoke-static {v0, p1}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->verified:Z

    .line 183
    const-string v0, "status"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 184
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 186
    const-string v1, "created_at"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusCreatedAt:Ljava/util/Date;

    .line 187
    const-string v1, "id"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/User;->statusId:J

    .line 188
    const-string v1, "text"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    .line 189
    const-string v1, "source"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    .line 190
    const-string v1, "truncated"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v1

    iput-boolean v1, p0, Lweibo4android/User;->statusTruncated:Z

    .line 191
    const-string v1, "in_reply_to_status_id"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    .line 192
    const-string v1, "in_reply_to_user_id"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/User;->statusInReplyToUserId:I

    .line 193
    const-string v1, "favorited"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v1

    iput-boolean v1, p0, Lweibo4android/User;->statusFavorited:Z

    .line 194
    const-string v1, "in_reply_to_screen_name"

    invoke-static {v1, v0}, Lweibo4android/User;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    .line 196
    :cond_0
    return-void
.end method

.method private init(Lweibo4android/org/json/JSONObject;)V
    .locals 4

    .prologue
    .line 107
    if-eqz p1, :cond_0

    .line 109
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->id:I

    .line 110
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->name:Ljava/lang/String;

    .line 111
    const-string v0, "screen_name"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->screenName:Ljava/lang/String;

    .line 112
    const-string v0, "location"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->location:Ljava/lang/String;

    .line 113
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->description:Ljava/lang/String;

    .line 114
    const-string v0, "profile_image_url"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileImageUrl:Ljava/lang/String;

    .line 115
    const-string v0, "url"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->url:Ljava/lang/String;

    .line 116
    const-string v0, "protected"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->isProtected:Z

    .line 117
    const-string v0, "followers_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->followersCount:I

    .line 119
    const-string v0, "profile_background_color"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundColor:Ljava/lang/String;

    .line 120
    const-string v0, "profile_text_color"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileTextColor:Ljava/lang/String;

    .line 121
    const-string v0, "profile_link_color"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileLinkColor:Ljava/lang/String;

    .line 122
    const-string v0, "profile_sidebar_fill_color"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileSidebarFillColor:Ljava/lang/String;

    .line 123
    const-string v0, "profile_sidebar_border_color"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileSidebarBorderColor:Ljava/lang/String;

    .line 124
    const-string v0, "friends_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->friendsCount:I

    .line 125
    const-string v0, "created_at"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/User;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->createdAt:Ljava/util/Date;

    .line 126
    const-string v0, "favourites_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->favouritesCount:I

    .line 127
    const-string v0, "utc_offset"

    invoke-static {v0, p1}, Lweibo4android/User;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->utcOffset:I

    .line 128
    const-string v0, "time_zone"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->timeZone:Ljava/lang/String;

    .line 129
    const-string v0, "profile_background_image_url"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundImageUrl:Ljava/lang/String;

    .line 130
    const-string v0, "profile_background_tile"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->profileBackgroundTile:Ljava/lang/String;

    .line 131
    const-string v0, "following"

    invoke-static {v0, p1}, Lweibo4android/User;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->following:Z

    .line 132
    const-string v0, "notifications"

    invoke-static {v0, p1}, Lweibo4android/User;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/User;->notificationEnabled:Z

    .line 133
    const-string v0, "statuses_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/User;->statusesCount:I

    .line 134
    const-string v0, "status"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    const-string v0, "status"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 136
    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v1, v2}, Lweibo4android/User;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusCreatedAt:Ljava/util/Date;

    .line 137
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/User;->statusId:J

    .line 138
    const-string v1, "text"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    .line 139
    const-string v1, "source"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    .line 140
    const-string v1, "truncated"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lweibo4android/User;->statusTruncated:Z

    .line 141
    const-string v1, "in_reply_to_status_id"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    .line 142
    const-string v1, "in_reply_to_user_id"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lweibo4android/User;->statusInReplyToUserId:I

    .line 143
    const-string v1, "favorited"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lweibo4android/User;->statusFavorited:Z

    .line 144
    const-string v1, "in_reply_to_screen_name"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 602
    if-nez p1, :cond_1

    move v0, v1

    .line 608
    :cond_0
    :goto_0
    return v0

    .line 605
    :cond_1
    if-eq p0, p1, :cond_0

    .line 608
    instance-of v2, p1, Lweibo4android/User;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/User;

    iget v2, p1, Lweibo4android/User;->id:I

    iget v3, p0, Lweibo4android/User;->id:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lweibo4android/User;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lweibo4android/User;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getFavouritesCount()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lweibo4android/User;->favouritesCount:I

    return v0
.end method

.method public getFollowersCount()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lweibo4android/User;->followersCount:I

    return v0
.end method

.method public getFriendsCount()I
    .locals 1

    .prologue
    .line 522
    iget v0, p0, Lweibo4android/User;->friendsCount:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lweibo4android/User;->id:I

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lweibo4android/User;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lweibo4android/User;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lweibo4android/User;->profileBackgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lweibo4android/User;->profileBackgroundImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileBackgroundTile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lweibo4android/User;->profileBackgroundTile:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileImageURL()Ljava/net/URL;
    .locals 2

    .prologue
    .line 250
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lweibo4android/User;->profileImageUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :goto_0
    return-object v0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProfileLinkColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lweibo4android/User;->profileLinkColor:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileSidebarBorderColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lweibo4android/User;->profileSidebarBorderColor:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileSidebarFillColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lweibo4android/User;->profileSidebarFillColor:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileTextColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lweibo4android/User;->profileTextColor:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lweibo4android/User;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lweibo4android/User;->statusCreatedAt:Ljava/util/Date;

    return-object v0
.end method

.method public getStatusId()J
    .locals 2

    .prologue
    .line 442
    iget-wide v0, p0, Lweibo4android/User;->statusId:J

    return-wide v0
.end method

.method public getStatusInReplyToScreenName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 498
    const/4 v0, -0x1

    iget v1, p0, Lweibo4android/User;->statusInReplyToUserId:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStatusInReplyToStatusId()J
    .locals 2

    .prologue
    .line 473
    iget-wide v0, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    return-wide v0
.end method

.method public getStatusInReplyToUserId()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lweibo4android/User;->statusInReplyToUserId:I

    return v0
.end method

.method public getStatusSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusesCount()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lweibo4android/User;->statusesCount:I

    return v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lweibo4android/User;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/net/URL;
    .locals 2

    .prologue
    .line 263
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lweibo4android/User;->url:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-object v0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUtcOffset()I
    .locals 1

    .prologue
    .line 534
    iget v0, p0, Lweibo4android/User;->utcOffset:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 597
    iget v0, p0, Lweibo4android/User;->id:I

    return v0
.end method

.method public isFollowing()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 554
    iget-boolean v0, p0, Lweibo4android/User;->following:Z

    return v0
.end method

.method public isGeoEnabled()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lweibo4android/User;->geoEnabled:Z

    return v0
.end method

.method public isNotificationEnabled()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 572
    iget-boolean v0, p0, Lweibo4android/User;->notificationEnabled:Z

    return v0
.end method

.method public isNotifications()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 563
    iget-boolean v0, p0, Lweibo4android/User;->notificationEnabled:Z

    return v0
.end method

.method public isProtected()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lweibo4android/User;->isProtected:Z

    return v0
.end method

.method public isStatusFavorited()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lweibo4android/User;->statusFavorited:Z

    return v0
.end method

.method public isStatusTruncated()Z
    .locals 1

    .prologue
    .line 465
    iget-boolean v0, p0, Lweibo4android/User;->statusTruncated:Z

    return v0
.end method

.method public isVerified()Z
    .locals 1

    .prologue
    .line 592
    iget-boolean v0, p0, Lweibo4android/User;->verified:Z

    return v0
.end method

.method public sendDirectMessage(Ljava/lang/String;)Lweibo4android/DirectMessage;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lweibo4android/User;->weibo:Lweibo4android/Weibo;

    invoke-virtual {p0}, Lweibo4android/User;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lweibo4android/Weibo;->sendDirectMessage(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/DirectMessage;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "User{weibo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->weibo:Lweibo4android/Weibo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->screenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", location=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->location:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileImageUrl=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isProtected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->isProtected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", followersCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->followersCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusCreatedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->statusCreatedAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/User;->statusId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusText=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->statusText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusSource=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->statusSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusTruncated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->statusTruncated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusInReplyToStatusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/User;->statusInReplyToStatusId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusInReplyToUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->statusInReplyToUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusFavorited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->statusFavorited:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusInReplyToScreenName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->statusInReplyToScreenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileBackgroundColor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileBackgroundColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileTextColor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileTextColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileLinkColor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileLinkColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileSidebarFillColor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileSidebarFillColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileSidebarBorderColor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileSidebarBorderColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", friendsCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->friendsCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", favouritesCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->favouritesCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", utcOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->utcOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeZone=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->timeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileBackgroundImageUrl=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileBackgroundImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileBackgroundTile=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/User;->profileBackgroundTile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", following="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->following:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notificationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->notificationEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusesCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/User;->statusesCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", geoEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->geoEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", verified="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/User;->verified:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
