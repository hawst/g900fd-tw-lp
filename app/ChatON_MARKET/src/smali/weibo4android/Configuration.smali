.class public Lweibo4android/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"


# static fields
.field private static DALVIK:Z

.field private static defaultProperty:Ljava/util/Properties;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 41
    invoke-static {}, Lweibo4android/Configuration;->init()V

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 245
    invoke-static {p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static getCilentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const-string v0, "weibo4j.clientVersion"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCilentVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string v0, "weibo4j.clientVersion"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getClientURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const-string v0, "weibo4j.clientURL"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getClientURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    const-string v0, "weibo4j.clientURL"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getConnectionTimeout()I
    .locals 1

    .prologue
    .line 173
    const-string v0, "weibo4j.http.connectionTimeout"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getConnectionTimeout(I)I
    .locals 1

    .prologue
    .line 177
    const-string v0, "weibo4j.http.connectionTimeout"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getDebug()Z
    .locals 1

    .prologue
    .line 328
    const-string v0, "weibo4j.debug"

    invoke-static {v0}, Lweibo4android/Configuration;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getIntProperty(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 250
    invoke-static {p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 254
    :goto_0
    return v0

    .line 253
    :catch_0
    move-exception v0

    .line 254
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getIntProperty(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 259
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 263
    :goto_0
    return v0

    .line 262
    :catch_0
    move-exception v0

    .line 263
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getLongProperty(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 268
    invoke-static {p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 272
    :goto_0
    return-wide v0

    .line 271
    :catch_0
    move-exception v0

    .line 272
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static getNumberOfAsyncThreads()I
    .locals 1

    .prologue
    .line 324
    const-string v0, "weibo4j.async.numThreads"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getOAuthConsumerKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    const-string v0, "weibo4j.oauth.consumerKey"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOAuthConsumerKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    const-string v0, "weibo4j.oauth.consumerKey"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOAuthConsumerSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    const-string v0, "weibo4j.oauth.consumerSecret"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOAuthConsumerSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    const-string v0, "weibo4j.oauth.consumerSecret"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    const-string v0, "weibo4j.password"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    const-string v0, "weibo4j.password"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 283
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    if-nez v0, :cond_0

    .line 285
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    invoke-virtual {v0, p0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 287
    :cond_0
    if-nez v0, :cond_1

    .line 288
    sget-object v1, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".fallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 289
    if-eqz v1, :cond_1

    .line 290
    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/AccessControlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 297
    :cond_1
    :goto_0
    invoke-static {v0}, Lweibo4android/Configuration;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 293
    :catch_0
    move-exception v0

    move-object v0, p1

    .line 295
    goto :goto_0
.end method

.method public static getProxyHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const-string v0, "weibo4j.http.proxyHost"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxyHost(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string v0, "weibo4j.http.proxyHost"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxyPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string v0, "weibo4j.http.proxyPassword"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxyPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const-string v0, "weibo4j.http.proxyPassword"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxyPort()I
    .locals 1

    .prologue
    .line 165
    const-string v0, "weibo4j.http.proxyPort"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getProxyPort(I)I
    .locals 1

    .prologue
    .line 169
    const-string v0, "weibo4j.http.proxyPort"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getProxyUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string v0, "weibo4j.http.proxyUser"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxyUser(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    const-string v0, "weibo4j.http.proxyUser"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getReadTimeout()I
    .locals 1

    .prologue
    .line 181
    const-string v0, "weibo4j.http.readTimeout"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getReadTimeout(I)I
    .locals 1

    .prologue
    .line 185
    const-string v0, "weibo4j.http.readTimeout"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRetryCount()I
    .locals 1

    .prologue
    .line 189
    const-string v0, "weibo4j.http.retryCount"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getRetryCount(I)I
    .locals 1

    .prologue
    .line 193
    const-string v0, "weibo4j.http.retryCount"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRetryIntervalSecs()I
    .locals 1

    .prologue
    .line 197
    const-string v0, "weibo4j.http.retryIntervalSecs"

    invoke-static {v0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getRetryIntervalSecs(I)I
    .locals 1

    .prologue
    .line 201
    const-string v0, "weibo4j.http.retryIntervalSecs"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getScheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lweibo4android/Configuration;->useSSL()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http://"

    goto :goto_0
.end method

.method public static getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const-string v0, "weibo4j.source"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    const-string v0, "weibo4j.source"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string v0, "weibo4j.user"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUser(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    const-string v0, "weibo4j.user"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    const-string v0, "weibo4j.http.userAgent"

    invoke-static {v0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserAgent(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    const-string v0, "weibo4j.http.userAgent"

    invoke-static {v0, p0}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static init()V
    .locals 5

    .prologue
    .line 45
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    sput-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    .line 46
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.debug"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.source"

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.clientURL"

    const-string v2, "http://open.t.sina.com.cn/-{weibo4j.clientVersion}.xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.userAgent"

    const-string v2, "weibo4j http://open.t.sina.com.cn/ /{weibo4j.clientVersion}"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.useSSL"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.proxyHost.fallback"

    const-string v2, "http.proxyHost"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.proxyPort.fallback"

    const-string v2, "http.proxyPort"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.connectionTimeout"

    const-string v2, "20000"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.readTimeout"

    const-string v2, "120000"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.retryCount"

    const-string v2, "3"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.http.retryIntervalSecs"

    const-string v2, "10"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.async.numThreads"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.clientVersion"

    invoke-static {}, Lweibo4android/Version;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 71
    :try_start_0
    const-string v0, "dalvik.system.VMRuntime"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 72
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.dalvik"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    const-string v0, "weibo4j.dalvik"

    invoke-static {v0}, Lweibo4android/Configuration;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lweibo4android/Configuration;->DALVIK:Z

    .line 77
    const-string v0, "weibo4j.properties"

    .line 78
    sget-object v1, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lweibo4android/Configuration;->loadProperties(Ljava/util/Properties;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-class v2, Lweibo4android/Configuration;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/WEB-INF/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lweibo4android/Configuration;->loadProperties(Ljava/util/Properties;Ljava/io/InputStream;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-class v2, Lweibo4android/Configuration;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v1, v0}, Lweibo4android/Configuration;->loadProperties(Ljava/util/Properties;Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    :cond_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    sget-object v0, Lweibo4android/Configuration;->defaultProperty:Ljava/util/Properties;

    const-string v1, "weibo4j.dalvik"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static isDalvik()Z
    .locals 1

    .prologue
    .line 105
    sget-boolean v0, Lweibo4android/Configuration;->DALVIK:Z

    return v0
.end method

.method private static loadProperties(Ljava/util/Properties;Ljava/io/InputStream;)Z
    .locals 1

    .prologue
    .line 95
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    .line 97
    :catch_0
    move-exception v0

    .line 99
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static loadProperties(Ljava/util/Properties;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 83
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    .line 88
    :catch_0
    move-exception v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static replace(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 301
    if-nez p0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-object p0

    .line 306
    :cond_1
    const/4 v0, -0x1

    const-string v1, "{"

    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 307
    const-string v0, "}"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 308
    add-int/lit8 v2, v1, 0x1

    if-le v0, v2, :cond_2

    .line 309
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 310
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 311
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Lweibo4android/Configuration;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    :goto_1
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 319
    invoke-static {v0}, Lweibo4android/Configuration;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    move-object v0, p0

    goto :goto_1
.end method

.method public static useSSL()Z
    .locals 1

    .prologue
    .line 109
    const-string v0, "weibo4j.http.useSSL"

    invoke-static {v0}, Lweibo4android/Configuration;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
