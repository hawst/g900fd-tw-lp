.class public Lweibo4android/IDs;
.super Lweibo4android/WeiboResponse;
.source "IDs.java"


# static fields
.field private static ROOT_NODE_NAMES:[Ljava/lang/String; = null

.field private static final serialVersionUID:J = -0x5b62b6c47b2132b9L


# instance fields
.field private ids:[J

.field private nextCursor:J

.field private previousCursor:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id_list"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ids"

    aput-object v2, v0, v1

    sput-object v0, Lweibo4android/IDs;->ROOT_NODE_NAMES:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 6

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 22
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 23
    sget-object v0, Lweibo4android/IDs;->ROOT_NODE_NAMES:[Ljava/lang/String;

    invoke-static {v0, v1}, Lweibo4android/IDs;->ensureRootNodeNameIs([Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 24
    const-string v0, "id"

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 25
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lweibo4android/IDs;->ids:[J

    .line 26
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 28
    :try_start_0
    iget-object v3, p0, Lweibo4android/IDs;->ids:[J

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v3, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Weibo API returned malformed response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    .line 33
    :cond_0
    const-string v0, "previous_cursor"

    invoke-static {v0, v1}, Lweibo4android/IDs;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/IDs;->previousCursor:J

    .line 34
    const-string v0, "next_cursor"

    invoke-static {v0, v1}, Lweibo4android/IDs;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/IDs;->nextCursor:J

    .line 35
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 6

    .prologue
    const-wide/16 v3, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 39
    const-string v1, "[]\n"

    invoke-virtual {p1}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    iput-wide v3, p0, Lweibo4android/IDs;->previousCursor:J

    .line 41
    iput-wide v3, p0, Lweibo4android/IDs;->nextCursor:J

    .line 42
    new-array v0, v0, [J

    iput-object v0, p0, Lweibo4android/IDs;->ids:[J

    .line 61
    :cond_0
    return-void

    .line 45
    :cond_1
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 47
    :try_start_0
    const-string v2, "previous_cursor"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/IDs;->previousCursor:J

    .line 48
    const-string v2, "next_cursor"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/IDs;->nextCursor:J

    .line 50
    const-string v2, "ids"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    const-string v2, "ids"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 53
    new-array v3, v2, [J

    iput-object v3, p0, Lweibo4android/IDs;->ids:[J

    .line 54
    :goto_0
    if-ge v0, v2, :cond_0

    .line 55
    iget-object v3, p0, Lweibo4android/IDs;->ids:[J

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v3, v0
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 85
    if-ne p0, p1, :cond_0

    .line 86
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    .line 88
    :cond_0
    instance-of v0, p1, Lweibo4android/IDs;

    if-nez v0, :cond_1

    .line 89
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_1
    check-cast p1, Lweibo4android/IDs;

    .line 94
    iget-object v0, p0, Lweibo4android/IDs;->ids:[J

    iget-object v1, p1, Lweibo4android/IDs;->ids:[J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    goto :goto_0
.end method

.method public getIDs()[J
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lweibo4android/IDs;->ids:[J

    return-object v0
.end method

.method public getNextCursor()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lweibo4android/IDs;->nextCursor:J

    return-wide v0
.end method

.method public getPreviousCursor()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lweibo4android/IDs;->previousCursor:J

    return-wide v0
.end method

.method public hasNext()Z
    .locals 4

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lweibo4android/IDs;->nextCursor:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 4

    .prologue
    .line 68
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lweibo4android/IDs;->previousCursor:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lweibo4android/IDs;->ids:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lweibo4android/IDs;->ids:[J

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([J)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IDs{ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/IDs;->ids:[J

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", previousCursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/IDs;->previousCursor:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextCursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/IDs;->nextCursor:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
