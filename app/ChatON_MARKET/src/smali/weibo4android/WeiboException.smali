.class public Lweibo4android/WeiboException;
.super Ljava/lang/Exception;
.source "WeiboException.java"


# static fields
.field private static final serialVersionUID:J = -0x2467dddfd16c8207L


# instance fields
.field private statusCode:I


# direct methods
.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 49
    iput p2, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 59
    iput p3, p0, Lweibo4android/WeiboException;->statusCode:I

    .line 61
    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lweibo4android/WeiboException;->statusCode:I

    return v0
.end method
