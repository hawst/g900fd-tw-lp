.class public Lweibo4android/QueryResult;
.super Lweibo4android/WeiboResponse;
.source "QueryResult.java"


# static fields
.field private static final serialVersionUID:J = -0x7db884b56e8fbc26L


# instance fields
.field private completedIn:D

.field private maxId:J

.field private page:I

.field private query:Ljava/lang/String;

.field private refreshUrl:Ljava/lang/String;

.field private resultsPerPage:I

.field private sinceId:J

.field private total:I

.field private tweets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private warning:Ljava/lang/String;


# direct methods
.method constructor <init>(Lweibo4android/Query;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/QueryResult;->total:I

    .line 82
    invoke-virtual {p1}, Lweibo4android/Query;->getSinceId()J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/QueryResult;->sinceId:J

    .line 83
    invoke-virtual {p1}, Lweibo4android/Query;->getRpp()I

    move-result v0

    iput v0, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    .line 84
    invoke-virtual {p1}, Lweibo4android/Query;->getPage()I

    move-result v0

    iput v0, p0, Lweibo4android/QueryResult;->page:I

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    .line 86
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/WeiboSupport;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 48
    const/4 v1, -0x1

    iput v1, p0, Lweibo4android/QueryResult;->total:I

    .line 58
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 60
    :try_start_0
    const-string v2, "since_id"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/QueryResult;->sinceId:J

    .line 61
    const-string v2, "max_id"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/QueryResult;->maxId:J

    .line 62
    const-string v2, "refresh_url"

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lweibo4android/QueryResult;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    .line 64
    const-string v2, "results_per_page"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    .line 65
    const-string v2, "warning"

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lweibo4android/QueryResult;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    .line 66
    const-string v2, "completed_in"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/QueryResult;->completedIn:D

    .line 67
    const-string v2, "page"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lweibo4android/QueryResult;->page:I

    .line 68
    const-string v2, "query"

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lweibo4android/QueryResult;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    .line 69
    const-string v2, "results"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v2

    .line 70
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    .line 71
    :goto_0
    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 72
    invoke-virtual {v2, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v3

    .line 73
    iget-object v4, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    new-instance v5, Lweibo4android/Tweet;

    invoke-direct {v5, v3, p2}, Lweibo4android/Tweet;-><init>(Lweibo4android/org/json/JSONObject;Lweibo4android/WeiboSupport;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 138
    if-ne p0, p1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 141
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 142
    goto :goto_0

    .line 145
    :cond_3
    check-cast p1, Lweibo4android/QueryResult;

    .line 147
    iget-wide v2, p1, Lweibo4android/QueryResult;->completedIn:D

    iget-wide v4, p0, Lweibo4android/QueryResult;->completedIn:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 148
    goto :goto_0

    .line 150
    :cond_4
    iget-wide v2, p0, Lweibo4android/QueryResult;->maxId:J

    iget-wide v4, p1, Lweibo4android/QueryResult;->maxId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 151
    goto :goto_0

    .line 153
    :cond_5
    iget v2, p0, Lweibo4android/QueryResult;->page:I

    iget v3, p1, Lweibo4android/QueryResult;->page:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 154
    goto :goto_0

    .line 156
    :cond_6
    iget v2, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    iget v3, p1, Lweibo4android/QueryResult;->resultsPerPage:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 157
    goto :goto_0

    .line 159
    :cond_7
    iget-wide v2, p0, Lweibo4android/QueryResult;->sinceId:J

    iget-wide v4, p1, Lweibo4android/QueryResult;->sinceId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 160
    goto :goto_0

    .line 162
    :cond_8
    iget v2, p0, Lweibo4android/QueryResult;->total:I

    iget v3, p1, Lweibo4android/QueryResult;->total:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :cond_9
    iget-object v2, p0, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 166
    goto :goto_0

    .line 168
    :cond_a
    iget-object v2, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 169
    goto :goto_0

    .line 168
    :cond_c
    iget-object v2, p1, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 171
    :cond_d
    iget-object v2, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    iget-object v3, p1, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 172
    goto :goto_0

    .line 171
    :cond_f
    iget-object v2, p1, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    if-nez v2, :cond_e

    .line 174
    :cond_10
    iget-object v2, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 175
    goto/16 :goto_0

    .line 174
    :cond_11
    iget-object v2, p1, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getCompletedIn()D
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lweibo4android/QueryResult;->completedIn:D

    return-wide v0
.end method

.method public getMaxId()J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lweibo4android/QueryResult;->maxId:J

    return-wide v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lweibo4android/QueryResult;->page:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getRefreshUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getResultsPerPage()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    return v0
.end method

.method public getSinceId()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lweibo4android/QueryResult;->sinceId:J

    return-wide v0
.end method

.method public getTotal()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 113
    iget v0, p0, Lweibo4android/QueryResult;->total:I

    return v0
.end method

.method public getTweets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    return-object v0
.end method

.method public getWarning()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 185
    iget-wide v2, p0, Lweibo4android/QueryResult;->sinceId:J

    iget-wide v4, p0, Lweibo4android/QueryResult;->sinceId:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 186
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lweibo4android/QueryResult;->maxId:J

    iget-wide v4, p0, Lweibo4android/QueryResult;->maxId:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 187
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 188
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    add-int/2addr v0, v2

    .line 189
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/QueryResult;->total:I

    add-int/2addr v0, v2

    .line 190
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 191
    iget-wide v2, p0, Lweibo4android/QueryResult;->completedIn:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lweibo4android/QueryResult;->completedIn:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 192
    :goto_2
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 193
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/QueryResult;->page:I

    add-int/2addr v0, v2

    .line 194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 195
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 196
    return v0

    :cond_1
    move v0, v1

    .line 187
    goto :goto_0

    :cond_2
    move v0, v1

    .line 190
    goto :goto_1

    .line 191
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QueryResult{sinceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/QueryResult;->sinceId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/QueryResult;->maxId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", refreshUrl=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/QueryResult;->refreshUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultsPerPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/QueryResult;->resultsPerPage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/QueryResult;->total:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", warning=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/QueryResult;->warning:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", completedIn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/QueryResult;->completedIn:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/QueryResult;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", query=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/QueryResult;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tweets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/QueryResult;->tweets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
