.class public Lweibo4android/SavedSearch;
.super Lweibo4android/WeiboResponse;
.source "SavedSearch.java"


# static fields
.field private static final serialVersionUID:J = 0x2acbedd301ef9884L


# instance fields
.field private createdAt:Ljava/util/Date;

.field private id:I

.field private name:Ljava/lang/String;

.field private position:I

.field private query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 53
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/SavedSearch;->init(Lweibo4android/org/json/JSONObject;)V

    .line 54
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/org/json/JSONObject;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 58
    invoke-direct {p0, p2}, Lweibo4android/SavedSearch;->init(Lweibo4android/org/json/JSONObject;)V

    .line 59
    return-void
.end method

.method constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 62
    invoke-direct {p0, p1}, Lweibo4android/SavedSearch;->init(Lweibo4android/org/json/JSONObject;)V

    .line 63
    return-void
.end method

.method static constructSavedSearches(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/SavedSearch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 69
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 70
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 71
    new-instance v3, Lweibo4android/SavedSearch;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lweibo4android/SavedSearch;-><init>(Lweibo4android/http/Response;Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 73
    :cond_0
    return-object v2
.end method

.method private init(Lweibo4android/org/json/JSONObject;)V
    .locals 4

    .prologue
    .line 81
    :try_start_0
    const-string v0, "created_at"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/SavedSearch;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    .line 82
    const-string v0, "query"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lweibo4android/SavedSearch;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    .line 83
    const-string v0, "position"

    invoke-static {v0, p1}, Lweibo4android/SavedSearch;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lweibo4android/SavedSearch;->position:I

    .line 84
    const-string v0, "name"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lweibo4android/SavedSearch;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    .line 85
    const-string v0, "id"

    invoke-static {v0, p1}, Lweibo4android/SavedSearch;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lweibo4android/SavedSearch;->id:I
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    instance-of v2, p1, Lweibo4android/SavedSearch;

    if-nez v2, :cond_2

    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_2
    check-cast p1, Lweibo4android/SavedSearch;

    .line 122
    iget v2, p0, Lweibo4android/SavedSearch;->id:I

    iget v3, p1, Lweibo4android/SavedSearch;->id:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 123
    goto :goto_0

    .line 125
    :cond_3
    iget v2, p0, Lweibo4android/SavedSearch;->position:I

    iget v3, p1, Lweibo4android/SavedSearch;->position:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 126
    goto :goto_0

    .line 128
    :cond_4
    iget-object v2, p0, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    iget-object v3, p1, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 129
    goto :goto_0

    .line 131
    :cond_5
    iget-object v2, p0, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :cond_6
    iget-object v2, p0, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 135
    goto :goto_0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lweibo4android/SavedSearch;->id:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lweibo4android/SavedSearch;->position:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    .line 144
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lweibo4android/SavedSearch;->position:I

    add-int/2addr v0, v1

    .line 146
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lweibo4android/SavedSearch;->id:I

    add-int/2addr v0, v1

    .line 148
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SavedSearch{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/SavedSearch;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", query=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/SavedSearch;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/SavedSearch;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/SavedSearch;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/SavedSearch;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
