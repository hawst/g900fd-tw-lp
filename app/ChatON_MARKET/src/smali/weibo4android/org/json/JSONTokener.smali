.class public Lweibo4android/org/json/JSONTokener;
.super Ljava/lang/Object;
.source "JSONTokener.java"


# instance fields
.field private index:I

.field private lastChar:C

.field private reader:Ljava/io/Reader;

.field private useLastChar:Z


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Ljava/io/Reader;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p1, p0, Lweibo4android/org/json/JSONTokener;->reader:Ljava/io/Reader;

    .line 52
    iput-boolean v1, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    .line 53
    iput v1, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 54
    return-void

    .line 51
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/io/Reader;)V

    .line 64
    return-void
.end method

.method public static dehexchar(C)I
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 89
    add-int/lit8 v0, p0, -0x30

    .line 97
    :goto_0
    return v0

    .line 91
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 92
    add-int/lit8 v0, p0, -0x37

    goto :goto_0

    .line 94
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 95
    add-int/lit8 v0, p0, -0x57

    goto :goto_0

    .line 97
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public back()V
    .locals 2

    .prologue
    .line 72
    iget-boolean v0, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    if-nez v0, :cond_0

    iget v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    if-gtz v0, :cond_1

    .line 73
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Stepping back two steps is not supported"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_1
    iget v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    .line 77
    return-void
.end method

.method public more()Z
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 112
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()C
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-boolean v1, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    if-eqz v1, :cond_1

    .line 122
    iput-boolean v0, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    .line 123
    iget-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    if-eqz v0, :cond_0

    .line 124
    iget v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 126
    :cond_0
    iget-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    .line 141
    :goto_0
    return v0

    .line 130
    :cond_1
    :try_start_0
    iget-object v1, p0, Lweibo4android/org/json/JSONTokener;->reader:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 135
    if-gtz v1, :cond_2

    .line 136
    iput-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 139
    :cond_2
    iget v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 140
    int-to-char v0, v1

    iput-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    .line 141
    iget-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    goto :goto_0
.end method

.method public next(C)C
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    .line 156
    if-eq v0, p1, :cond_0

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' and instead saw \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 159
    :cond_0
    return v0
.end method

.method public next(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 173
    if-nez p1, :cond_0

    .line 174
    const-string v0, ""

    .line 201
    :goto_0
    return-object v0

    .line 177
    :cond_0
    new-array v1, p1, [C

    .line 180
    iget-boolean v2, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    if-eqz v2, :cond_1

    .line 181
    iput-boolean v0, p0, Lweibo4android/org/json/JSONTokener;->useLastChar:Z

    .line 182
    iget-char v2, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    aput-char v2, v1, v0

    .line 183
    const/4 v0, 0x1

    .line 188
    :cond_1
    :goto_1
    if-ge v0, p1, :cond_2

    :try_start_0
    iget-object v2, p0, Lweibo4android/org/json/JSONTokener;->reader:Ljava/io/Reader;

    sub-int v3, p1, v0

    invoke-virtual {v2, v1, v0, v3}, Ljava/io/Reader;->read([CII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 189
    add-int/2addr v0, v2

    goto :goto_1

    .line 191
    :catch_0
    move-exception v0

    .line 192
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 194
    :cond_2
    iget v2, p0, Lweibo4android/org/json/JSONTokener;->index:I

    add-int/2addr v2, v0

    iput v2, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 196
    if-ge v0, p1, :cond_3

    .line 197
    const-string v0, "Substring bounds error"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 200
    :cond_3
    add-int/lit8 v0, p1, -0x1

    aget-char v0, v1, v0

    iput-char v0, p0, Lweibo4android/org/json/JSONTokener;->lastChar:C

    .line 201
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public nextClean()C
    .locals 2

    .prologue
    .line 212
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    .line 213
    if-eqz v0, :cond_1

    const/16 v1, 0x20

    if-le v0, v1, :cond_0

    .line 214
    :cond_1
    return v0
.end method

.method public nextString(C)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 233
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 235
    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    .line 236
    sparse-switch v1, :sswitch_data_0

    .line 270
    if-ne v1, p1, :cond_0

    .line 271
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 240
    :sswitch_0
    const-string v0, "Unterminated string"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 242
    :sswitch_1
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    .line 243
    sparse-switch v1, :sswitch_data_1

    .line 266
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 245
    :sswitch_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 248
    :sswitch_3
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 251
    :sswitch_4
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 254
    :sswitch_5
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 257
    :sswitch_6
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 260
    :sswitch_7
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONTokener;->next(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 263
    :sswitch_8
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONTokener;->next(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 273
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 236
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x5c -> :sswitch_1
    .end sparse-switch

    .line 243
    :sswitch_data_1
    .sparse-switch
        0x62 -> :sswitch_2
        0x66 -> :sswitch_5
        0x6e -> :sswitch_4
        0x72 -> :sswitch_6
        0x74 -> :sswitch_3
        0x75 -> :sswitch_7
        0x78 -> :sswitch_8
    .end sparse-switch
.end method

.method public nextTo(C)Ljava/lang/String;
    .locals 3

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 289
    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    .line 290
    if-eq v1, p1, :cond_0

    if-eqz v1, :cond_0

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    const/16 v2, 0xd

    if-ne v1, v2, :cond_2

    .line 291
    :cond_0
    if-eqz v1, :cond_1

    .line 292
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 294
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 296
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public nextTo(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 310
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 312
    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    .line 313
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_0

    if-eqz v1, :cond_0

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    const/16 v2, 0xd

    if-ne v1, v2, :cond_2

    .line 314
    :cond_0
    if-eqz v1, :cond_1

    .line 315
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 317
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 319
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public nextValue()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 332
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    .line 335
    sparse-switch v0, :sswitch_data_0

    .line 356
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 357
    :goto_0
    const/16 v2, 0x20

    if-lt v0, v2, :cond_0

    const-string v2, ",:]}/\\\"[{;=#"

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_0

    .line 358
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 359
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    goto :goto_0

    .line 338
    :sswitch_0
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->nextString(C)Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_1
    return-object v0

    .line 340
    :sswitch_1
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 341
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONObject;-><init>(Lweibo4android/org/json/JSONTokener;)V

    goto :goto_1

    .line 344
    :sswitch_2
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 345
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONArray;-><init>(Lweibo4android/org/json/JSONTokener;)V

    goto :goto_1

    .line 361
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 363
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 364
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 365
    const-string v0, "Missing value"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 367
    :cond_1
    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->stringToValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 335
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x27 -> :sswitch_0
        0x28 -> :sswitch_2
        0x5b -> :sswitch_2
        0x7b -> :sswitch_1
    .end sparse-switch
.end method

.method public skipTo(C)C
    .locals 3

    .prologue
    .line 382
    :try_start_0
    iget v1, p0, Lweibo4android/org/json/JSONTokener;->index:I

    .line 383
    iget-object v0, p0, Lweibo4android/org/json/JSONTokener;->reader:Ljava/io/Reader;

    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Ljava/io/Reader;->mark(I)V

    .line 385
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    .line 386
    if-nez v0, :cond_1

    .line 387
    iget-object v2, p0, Lweibo4android/org/json/JSONTokener;->reader:Ljava/io/Reader;

    invoke-virtual {v2}, Ljava/io/Reader;->reset()V

    .line 388
    iput v1, p0, Lweibo4android/org/json/JSONTokener;->index:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :goto_0
    return v0

    .line 391
    :cond_1
    if-ne v0, p1, :cond_0

    .line 396
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    goto :goto_0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;
    .locals 3

    .prologue
    .line 408
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " at character "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/org/json/JSONTokener;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
