.class public Lweibo4android/org/json/XML;
.super Ljava/lang/Object;
.source "XML.java"


# static fields
.field public static final AMP:Ljava/lang/Character;

.field public static final APOS:Ljava/lang/Character;

.field public static final BANG:Ljava/lang/Character;

.field public static final EQ:Ljava/lang/Character;

.field public static final GT:Ljava/lang/Character;

.field public static final LT:Ljava/lang/Character;

.field public static final QUEST:Ljava/lang/Character;

.field public static final QUOT:Ljava/lang/Character;

.field public static final SLASH:Ljava/lang/Character;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x26

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->AMP:Ljava/lang/Character;

    .line 39
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x27

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->APOS:Ljava/lang/Character;

    .line 42
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x21

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->BANG:Ljava/lang/Character;

    .line 45
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x3d

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->EQ:Ljava/lang/Character;

    .line 48
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x3e

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    .line 51
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x3c

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->LT:Ljava/lang/Character;

    .line 54
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x3f

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->QUEST:Ljava/lang/Character;

    .line 57
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x22

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->QUOT:Ljava/lang/Character;

    .line 60
    new-instance v0, Ljava/lang/Character;

    const/16 v1, 0x2f

    invoke-direct {v0, v1}, Ljava/lang/Character;-><init>(C)V

    sput-object v0, Lweibo4android/org/json/XML;->SLASH:Ljava/lang/Character;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 77
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 79
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 80
    sparse-switch v3, :sswitch_data_0

    .line 94
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 78
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :sswitch_0
    const-string v3, "&amp;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 85
    :sswitch_1
    const-string v3, "&lt;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 88
    :sswitch_2
    const-string v3, "&gt;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 91
    :sswitch_3
    const-string v3, "&quot;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 97
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x26 -> :sswitch_0
        0x3c -> :sswitch_1
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method public static noSpace(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 108
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 109
    if-nez v1, :cond_0

    .line 110
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Empty string."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 113
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' contains a space character."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_2
    return-void
.end method

.method private static parse(Lweibo4android/org/json/XMLTokener;Lweibo4android/org/json/JSONObject;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/16 v6, 0x5b

    const/16 v2, 0x2d

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 135
    .line 149
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 153
    sget-object v1, Lweibo4android/org/json/XML;->BANG:Ljava/lang/Character;

    if-ne v0, v1, :cond_a

    .line 154
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    .line 155
    if-ne v0, v2, :cond_4

    .line 156
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    if-ne v0, v2, :cond_1

    .line 157
    const-string v0, "-->"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->skipPast(Ljava/lang/String;)Z

    move v3, v4

    .line 274
    :cond_0
    :goto_0
    return v3

    .line 160
    :cond_1
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->back()V

    :cond_2
    move v0, v3

    .line 176
    :cond_3
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextMeta()Ljava/lang/Object;

    move-result-object v1

    .line 177
    if-nez v1, :cond_7

    .line 178
    const-string v0, "Missing \'>\' after \'<!\'."

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 161
    :cond_4
    if-ne v0, v6, :cond_2

    .line 162
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 163
    const-string v1, "CDATA"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 164
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    if-ne v0, v6, :cond_6

    .line 165
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextCDATA()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 167
    const-string v1, "content"

    invoke-virtual {p1, v1, v0}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    :cond_5
    move v3, v4

    .line 169
    goto :goto_0

    .line 172
    :cond_6
    const-string v0, "Expected \'CDATA[\'"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 179
    :cond_7
    sget-object v2, Lweibo4android/org/json/XML;->LT:Ljava/lang/Character;

    if-ne v1, v2, :cond_9

    .line 180
    add-int/lit8 v0, v0, 0x1

    .line 184
    :cond_8
    :goto_1
    if-gtz v0, :cond_3

    move v3, v4

    .line 185
    goto :goto_0

    .line 181
    :cond_9
    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-ne v1, v2, :cond_8

    .line 182
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 186
    :cond_a
    sget-object v1, Lweibo4android/org/json/XML;->QUEST:Ljava/lang/Character;

    if-ne v0, v1, :cond_b

    .line 190
    const-string v0, "?>"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->skipPast(Ljava/lang/String;)Z

    move v3, v4

    .line 191
    goto :goto_0

    .line 192
    :cond_b
    sget-object v1, Lweibo4android/org/json/XML;->SLASH:Ljava/lang/Character;

    if-ne v0, v1, :cond_e

    .line 196
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 197
    if-nez p2, :cond_c

    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mismatched close tag"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 200
    :cond_c
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mismatched "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 203
    :cond_d
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-eq v0, v1, :cond_0

    .line 204
    const-string v0, "Misshaped close tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 208
    :cond_e
    instance-of v1, v0, Ljava/lang/Character;

    if-eqz v1, :cond_f

    .line 209
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 214
    :cond_f
    check-cast v0, Ljava/lang/String;

    .line 216
    new-instance v6, Lweibo4android/org/json/JSONObject;

    invoke-direct {v6}, Lweibo4android/org/json/JSONObject;-><init>()V

    move-object v1, v5

    .line 218
    :goto_2
    if-nez v1, :cond_10

    .line 219
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v1

    .line 224
    :cond_10
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 225
    check-cast v1, Ljava/lang/String;

    .line 226
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v2

    .line 227
    sget-object v7, Lweibo4android/org/json/XML;->EQ:Ljava/lang/Character;

    if-ne v2, v7, :cond_12

    .line 228
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v2

    .line 229
    instance-of v7, v2, Ljava/lang/String;

    if-nez v7, :cond_11

    .line 230
    const-string v0, "Missing value"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 232
    :cond_11
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lweibo4android/org/json/JSONObject;->stringToValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move-object v1, v5

    .line 233
    goto :goto_2

    .line 235
    :cond_12
    const-string v7, ""

    invoke-virtual {v6, v1, v7}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move-object v1, v2

    goto :goto_2

    .line 240
    :cond_13
    sget-object v2, Lweibo4android/org/json/XML;->SLASH:Ljava/lang/Character;

    if-ne v1, v2, :cond_15

    .line 241
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-eq v1, v2, :cond_14

    .line 242
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 244
    :cond_14
    invoke-virtual {p1, v0, v6}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move v3, v4

    .line 245
    goto/16 :goto_0

    .line 249
    :cond_15
    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-ne v1, v2, :cond_1c

    .line 251
    :cond_16
    :goto_3
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextContent()Ljava/lang/Object;

    move-result-object v1

    .line 252
    if-nez v1, :cond_18

    .line 253
    if-eqz v0, :cond_17

    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unclosed tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    :cond_17
    move v3, v4

    .line 256
    goto/16 :goto_0

    .line 257
    :cond_18
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 258
    check-cast v1, Ljava/lang/String;

    .line 259
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_16

    .line 260
    const-string v2, "content"

    invoke-static {v1}, Lweibo4android/org/json/JSONObject;->stringToValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v6, v2, v1}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_3

    .line 265
    :cond_19
    sget-object v2, Lweibo4android/org/json/XML;->LT:Ljava/lang/Character;

    if-ne v1, v2, :cond_16

    .line 266
    invoke-static {p0, v6, v0}, Lweibo4android/org/json/XML;->parse(Lweibo4android/org/json/XMLTokener;Lweibo4android/org/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 267
    invoke-virtual {v6}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_1a

    .line 268
    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    :goto_4
    move v3, v4

    .line 274
    goto/16 :goto_0

    .line 269
    :cond_1a
    invoke-virtual {v6}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v1

    if-ne v1, v3, :cond_1b

    const-string v1, "content"

    invoke-virtual {v6, v1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1b

    .line 270
    const-string v1, "content"

    invoke-virtual {v6, v1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_4

    .line 272
    :cond_1b
    invoke-virtual {p1, v0, v6}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_4

    .line 279
    :cond_1c
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0
.end method

.method public static toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 3

    .prologue
    .line 301
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 302
    new-instance v1, Lweibo4android/org/json/XMLTokener;

    invoke-direct {v1, p0}, Lweibo4android/org/json/XMLTokener;-><init>(Ljava/lang/String;)V

    .line 303
    :goto_0
    invoke-virtual {v1}, Lweibo4android/org/json/XMLTokener;->more()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "<"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/XMLTokener;->skipPast(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lweibo4android/org/json/XML;->parse(Lweibo4android/org/json/XMLTokener;Lweibo4android/org/json/JSONObject;Ljava/lang/String;)Z

    goto :goto_0

    .line 306
    :cond_0
    return-object v0
.end method

.method public static toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/16 v10, 0x3c

    const/4 v2, 0x0

    const/16 v9, 0x3e

    .line 332
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 341
    instance-of v0, p0, Lweibo4android/org/json/JSONObject;

    if-eqz v0, :cond_c

    .line 345
    if-eqz p1, :cond_0

    .line 346
    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 347
    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 348
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 353
    :cond_0
    check-cast p0, Lweibo4android/org/json/JSONObject;

    .line 354
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 355
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 356
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 357
    invoke-virtual {p0, v5}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 358
    if-nez v1, :cond_2

    .line 359
    const-string v1, ""

    .line 361
    :cond_2
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 362
    check-cast v0, Ljava/lang/String;

    .line 369
    :cond_3
    const-string v0, "content"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 370
    instance-of v0, v1, Lweibo4android/org/json/JSONArray;

    if-eqz v0, :cond_5

    .line 371
    check-cast v1, Lweibo4android/org/json/JSONArray;

    .line 372
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v5

    move v0, v2

    .line 373
    :goto_1
    if-ge v0, v5, :cond_1

    .line 374
    if-lez v0, :cond_4

    .line 375
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 377
    :cond_4
    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 380
    :cond_5
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 385
    :cond_6
    instance-of v0, v1, Lweibo4android/org/json/JSONArray;

    if-eqz v0, :cond_8

    .line 386
    check-cast v1, Lweibo4android/org/json/JSONArray;

    .line 387
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v6

    move v0, v2

    .line 388
    :goto_2
    if-ge v0, v6, :cond_1

    .line 389
    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 390
    instance-of v8, v7, Lweibo4android/org/json/JSONArray;

    if-eqz v8, :cond_7

    .line 391
    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 392
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 394
    invoke-static {v7}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 395
    const-string v7, "</"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 396
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 397
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 388
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 399
    :cond_7
    invoke-static {v7, v5}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 402
    :cond_8
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 403
    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 404
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 405
    const-string v0, "/>"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 410
    :cond_9
    invoke-static {v1, v5}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 413
    :cond_a
    if-eqz p1, :cond_b

    .line 417
    const-string v0, "</"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 418
    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 419
    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 421
    :cond_b
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 436
    :goto_4
    return-object v0

    .line 426
    :cond_c
    instance-of v0, p0, Lweibo4android/org/json/JSONArray;

    if-eqz v0, :cond_f

    .line 427
    check-cast p0, Lweibo4android/org/json/JSONArray;

    .line 428
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v4

    move v1, v2

    .line 429
    :goto_5
    if-ge v1, v4, :cond_e

    .line 430
    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v2

    .line 431
    if-nez p1, :cond_d

    const-string v0, "array"

    :goto_6
    invoke-static {v2, v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 429
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_d
    move-object v0, p1

    .line 431
    goto :goto_6

    .line 433
    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 435
    :cond_f
    if-nez p0, :cond_10

    const-string v0, "null"

    .line 436
    :goto_7
    if-nez p1, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 435
    :cond_10
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 436
    :cond_11
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_12
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4
.end method
