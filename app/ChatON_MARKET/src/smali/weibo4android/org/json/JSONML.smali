.class public Lweibo4android/org/json/JSONML;
.super Ljava/lang/Object;
.source "JSONML.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parse(Lweibo4android/org/json/XMLTokener;ZLweibo4android/org/json/JSONArray;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/16 v8, 0x5b

    const/16 v7, 0x2d

    .line 52
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextContent()Ljava/lang/Object;

    move-result-object v0

    .line 67
    sget-object v1, Lweibo4android/org/json/XML;->LT:Ljava/lang/Character;

    if-ne v0, v1, :cond_21

    .line 68
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 69
    instance-of v1, v0, Ljava/lang/Character;

    if-eqz v1, :cond_d

    .line 70
    sget-object v1, Lweibo4android/org/json/XML;->SLASH:Ljava/lang/Character;

    if-ne v0, v1, :cond_2

    .line 74
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 75
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_1

    .line 76
    new-instance v1, Lweibo4android/org/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a closing name instead of \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_1
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-eq v1, v2, :cond_1a

    .line 79
    const-string v0, "Misshaped close tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 82
    :cond_2
    sget-object v1, Lweibo4android/org/json/XML;->BANG:Ljava/lang/Character;

    if-ne v0, v1, :cond_b

    .line 86
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    .line 87
    if-ne v0, v7, :cond_4

    .line 88
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    if-ne v0, v7, :cond_3

    .line 89
    const-string v0, "-->"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->skipPast(Ljava/lang/String;)Z

    .line 91
    :cond_3
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->back()V

    goto :goto_0

    .line 92
    :cond_4
    if-ne v0, v8, :cond_6

    .line 93
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    .line 94
    const-string v1, "CDATA"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->next()C

    move-result v0

    if-ne v0, v8, :cond_5

    .line 95
    if-eqz p2, :cond_0

    .line 96
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextCDATA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_0

    .line 99
    :cond_5
    const-string v0, "Expected \'CDATA[\'"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 102
    :cond_6
    const/4 v0, 0x1

    .line 104
    :cond_7
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextMeta()Ljava/lang/Object;

    move-result-object v1

    .line 105
    if-nez v1, :cond_8

    .line 106
    const-string v0, "Missing \'>\' after \'<!\'."

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 107
    :cond_8
    sget-object v2, Lweibo4android/org/json/XML;->LT:Ljava/lang/Character;

    if-ne v1, v2, :cond_a

    .line 108
    add-int/lit8 v0, v0, 0x1

    .line 112
    :cond_9
    :goto_1
    if-gtz v0, :cond_7

    goto/16 :goto_0

    .line 109
    :cond_a
    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-ne v1, v2, :cond_9

    .line 110
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 114
    :cond_b
    sget-object v1, Lweibo4android/org/json/XML;->QUEST:Ljava/lang/Character;

    if-ne v0, v1, :cond_c

    .line 118
    const-string v0, "?>"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->skipPast(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 120
    :cond_c
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 126
    :cond_d
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_e

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad tagName \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 129
    :cond_e
    check-cast v0, Ljava/lang/String;

    .line 130
    new-instance v3, Lweibo4android/org/json/JSONArray;

    invoke-direct {v3}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 131
    new-instance v4, Lweibo4android/org/json/JSONObject;

    invoke-direct {v4}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 132
    if-eqz p1, :cond_11

    .line 133
    invoke-virtual {v3, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 134
    if-eqz p2, :cond_f

    .line 135
    invoke-virtual {p2, v3}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    :cond_f
    :goto_2
    move-object v1, v5

    .line 145
    :goto_3
    if-nez v1, :cond_10

    .line 146
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v1

    .line 148
    :cond_10
    if-nez v1, :cond_12

    .line 149
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 138
    :cond_11
    const-string v1, "tagName"

    invoke-virtual {v4, v1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 139
    if-eqz p2, :cond_f

    .line 140
    invoke-virtual {p2, v4}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_2

    .line 151
    :cond_12
    instance-of v2, v1, Ljava/lang/String;

    if-nez v2, :cond_14

    .line 173
    if-eqz p1, :cond_13

    invoke-virtual {v4}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v2

    if-lez v2, :cond_13

    .line 174
    invoke-virtual {v3, v4}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 179
    :cond_13
    sget-object v2, Lweibo4android/org/json/XML;->SLASH:Ljava/lang/Character;

    if-ne v1, v2, :cond_1c

    .line 180
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-eq v0, v1, :cond_19

    .line 181
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 157
    :cond_14
    check-cast v1, Ljava/lang/String;

    .line 158
    if-nez p1, :cond_16

    const-string v2, "tagName"

    if-eq v1, v2, :cond_15

    const-string v2, "childNode"

    if-ne v1, v2, :cond_16

    .line 159
    :cond_15
    const-string v0, "Reserved attribute."

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 161
    :cond_16
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v2

    .line 162
    sget-object v6, Lweibo4android/org/json/XML;->EQ:Ljava/lang/Character;

    if-ne v2, v6, :cond_18

    .line 163
    invoke-virtual {p0}, Lweibo4android/org/json/XMLTokener;->nextToken()Ljava/lang/Object;

    move-result-object v2

    .line 164
    instance-of v6, v2, Ljava/lang/String;

    if-nez v6, :cond_17

    .line 165
    const-string v0, "Missing value"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 167
    :cond_17
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lweibo4android/org/json/JSONObject;->stringToValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move-object v1, v5

    .line 168
    goto :goto_3

    .line 170
    :cond_18
    const-string v6, ""

    invoke-virtual {v4, v1, v6}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move-object v1, v2

    goto :goto_3

    .line 183
    :cond_19
    if-nez p2, :cond_0

    .line 184
    if-eqz p1, :cond_1b

    move-object v0, v3

    .line 210
    :cond_1a
    :goto_4
    return-object v0

    :cond_1b
    move-object v0, v4

    .line 187
    goto :goto_4

    .line 194
    :cond_1c
    sget-object v2, Lweibo4android/org/json/XML;->GT:Ljava/lang/Character;

    if-eq v1, v2, :cond_1d

    .line 195
    const-string v0, "Misshaped tag"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 197
    :cond_1d
    invoke-static {p0, p1, v3}, Lweibo4android/org/json/JSONML;->parse(Lweibo4android/org/json/XMLTokener;ZLweibo4android/org/json/JSONArray;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 198
    if-eqz v1, :cond_0

    .line 199
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 200
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mismatched \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' and \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/XMLTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 203
    :cond_1e
    if-nez p1, :cond_1f

    invoke-virtual {v3}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1f

    .line 204
    const-string v0, "childNodes"

    invoke-virtual {v4, v0, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 206
    :cond_1f
    if-nez p2, :cond_0

    .line 207
    if-eqz p1, :cond_20

    move-object v0, v3

    .line 208
    goto :goto_4

    :cond_20
    move-object v0, v4

    .line 210
    goto :goto_4

    .line 217
    :cond_21
    if-eqz p2, :cond_0

    .line 218
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->stringToValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :cond_22
    invoke-virtual {p2, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto/16 :goto_0
.end method

.method public static toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lweibo4android/org/json/XMLTokener;

    invoke-direct {v0, p0}, Lweibo4android/org/json/XMLTokener;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Lweibo4android/org/json/XMLTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONArray(Lweibo4android/org/json/XMLTokener;)Lweibo4android/org/json/JSONArray;
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lweibo4android/org/json/JSONML;->parse(Lweibo4android/org/json/XMLTokener;ZLweibo4android/org/json/JSONArray;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/org/json/JSONArray;

    return-object v0
.end method

.method public static toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 293
    new-instance v0, Lweibo4android/org/json/XMLTokener;

    invoke-direct {v0, p0}, Lweibo4android/org/json/XMLTokener;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONObject(Lweibo4android/org/json/XMLTokener;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONObject(Lweibo4android/org/json/XMLTokener;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 275
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lweibo4android/org/json/JSONML;->parse(Lweibo4android/org/json/XMLTokener;ZLweibo4android/org/json/JSONArray;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/org/json/JSONObject;

    return-object v0
.end method

.method public static toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;
    .locals 12

    .prologue
    const/16 v11, 0x3c

    const/16 v10, 0x2f

    const/16 v9, 0x22

    const/4 v1, 0x1

    const/16 v8, 0x3e

    .line 311
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 317
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-static {v0}, Lweibo4android/org/json/XML;->noSpace(Ljava/lang/String;)V

    .line 319
    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 320
    invoke-virtual {v2, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 321
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 323
    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    .line 324
    instance-of v4, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v4, :cond_1

    .line 325
    const/4 v1, 0x2

    .line 326
    check-cast v0, Lweibo4android/org/json/JSONObject;

    .line 330
    invoke-virtual {v0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 331
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 332
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 333
    invoke-static {v5}, Lweibo4android/org/json/XML;->noSpace(Ljava/lang/String;)V

    .line 334
    invoke-virtual {v0, v5}, Lweibo4android/org/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 335
    if-eqz v6, :cond_0

    .line 336
    const/16 v7, 0x20

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 337
    invoke-static {v5}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 338
    const/16 v5, 0x3d

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 339
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 340
    invoke-static {v6}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 350
    :goto_1
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v4

    .line 351
    if-lt v0, v4, :cond_2

    .line 352
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 353
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 374
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 355
    :cond_2
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v1, v0

    .line 357
    :cond_3
    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 358
    add-int/lit8 v1, v1, 0x1

    .line 359
    if-eqz v0, :cond_4

    .line 360
    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 361
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 368
    :cond_4
    :goto_3
    if-lt v1, v4, :cond_3

    .line 369
    invoke-virtual {v2, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 370
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 371
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 372
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 362
    :cond_5
    instance-of v5, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v5, :cond_6

    .line 363
    check-cast v0, Lweibo4android/org/json/JSONObject;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 364
    :cond_6
    instance-of v5, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v5, :cond_4

    .line 365
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public static toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v9, 0x3c

    const/16 v8, 0x2f

    const/16 v6, 0x22

    const/16 v7, 0x3e

    .line 389
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 401
    const-string v0, "tagName"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 402
    if-nez v0, :cond_0

    .line 403
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 455
    :goto_0
    return-object v0

    .line 405
    :cond_0
    invoke-static {v0}, Lweibo4android/org/json/XML;->noSpace(Ljava/lang/String;)V

    .line 406
    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 407
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 408
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 412
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 413
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 414
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 415
    const-string v4, "tagName"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "childNodes"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 416
    invoke-static {v1}, Lweibo4android/org/json/XML;->noSpace(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 418
    if-eqz v4, :cond_1

    .line 419
    const/16 v5, 0x20

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 420
    invoke-static {v1}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 421
    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 422
    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 423
    invoke-static {v4}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 424
    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 431
    :cond_2
    const-string v0, "childNodes"

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v4

    .line 432
    if-nez v4, :cond_3

    .line 433
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 434
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 455
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 436
    :cond_3
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 437
    invoke-virtual {v4}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v5

    .line 438
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_7

    .line 439
    invoke-virtual {v4, v1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 440
    if-eqz v0, :cond_4

    .line 441
    instance-of v6, v0, Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 442
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/XML;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 438
    :cond_4
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 443
    :cond_5
    instance-of v6, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v6, :cond_6

    .line 444
    check-cast v0, Lweibo4android/org/json/JSONObject;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 445
    :cond_6
    instance-of v6, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v6, :cond_4

    .line 446
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 450
    :cond_7
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 451
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 452
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 453
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2
.end method
