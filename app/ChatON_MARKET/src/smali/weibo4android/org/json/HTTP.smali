.class public Lweibo4android/org/json/HTTP;
.super Ljava/lang/Object;
.source "HTTP.java"


# static fields
.field public static final CRLF:Ljava/lang/String; = "\r\n"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 7

    .prologue
    const/16 v6, 0x3a

    const/4 v5, 0x0

    .line 88
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 89
    new-instance v1, Lweibo4android/org/json/HTTPTokener;

    invoke-direct {v1, p0}, Lweibo4android/org/json/HTTPTokener;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "HTTP"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    const-string v3, "HTTP-Version"

    invoke-virtual {v0, v3, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 98
    const-string v2, "Status-Code"

    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 99
    const-string v2, "Reason-Phrase"

    invoke-virtual {v1, v5}, Lweibo4android/org/json/HTTPTokener;->nextTo(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 100
    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->next()C

    .line 113
    :goto_0
    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->more()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    invoke-virtual {v1, v6}, Lweibo4android/org/json/HTTPTokener;->nextTo(C)Ljava/lang/String;

    move-result-object v2

    .line 115
    invoke-virtual {v1, v6}, Lweibo4android/org/json/HTTPTokener;->next(C)C

    .line 116
    invoke-virtual {v1, v5}, Lweibo4android/org/json/HTTPTokener;->nextTo(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 117
    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->next()C

    goto :goto_0

    .line 106
    :cond_0
    const-string v3, "Method"

    invoke-virtual {v0, v3, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 107
    const-string v2, "Request-URI"

    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 108
    const-string v2, "HTTP-Version"

    invoke-virtual {v1}, Lweibo4android/org/json/HTTPTokener;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_0

    .line 119
    :cond_1
    return-object v0
.end method

.method public static toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x22

    const/16 v3, 0x20

    .line 154
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 156
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 157
    const-string v2, "Status-Code"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Reason-Phrase"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    const-string v2, "HTTP-Version"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 160
    const-string v2, "Status-Code"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 162
    const-string v2, "Reason-Phrase"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 174
    :goto_0
    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 176
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    const-string v3, "HTTP-Version"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Status-Code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Reason-Phrase"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Method"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Request-URI"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 178
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 180
    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 163
    :cond_1
    const-string v2, "Method"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Request-URI"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 164
    const-string v2, "Method"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 166
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 167
    const-string v2, "Request-URI"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 169
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 170
    const-string v2, "HTTP-Version"

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 172
    :cond_2
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Not enough material for an HTTP header."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_3
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
