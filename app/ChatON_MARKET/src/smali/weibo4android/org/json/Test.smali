.class public Lweibo4android/org/json/Test;
.super Ljava/lang/Object;
.source "Test.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 74
    new-instance v0, Lweibo4android/org/json/Test$1Obj;

    const-string v1, "A beany object"

    const-wide/high16 v2, 0x4045000000000000L    # 42.0

    invoke-direct {v0, v1, v2, v3, v4}, Lweibo4android/org/json/Test$1Obj;-><init>(Ljava/lang/String;DZ)V

    .line 77
    :try_start_0
    const-string v1, "<![CDATA[This is a collection of test patterns and examples for org.json.]]>  Ignore the stuff past the end.  "

    invoke-static {v1}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 78
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 80
    const-string v1, "{     \"list of lists\" : [         [1, 2, 3],         [4, 5, 6],     ] }"

    .line 81
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 82
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 83
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v2}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 85
    const-string v1, "<recipe name=\"bread\" prep_time=\"5 mins\" cook_time=\"3 hours\"> <title>Basic bread</title> <ingredient amount=\"8\" unit=\"dL\">Flour</ingredient> <ingredient amount=\"10\" unit=\"grams\">Yeast</ingredient> <ingredient amount=\"4\" unit=\"dL\" state=\"warm\">Water</ingredient> <ingredient amount=\"1\" unit=\"teaspoon\">Salt</ingredient> <instructions> <step>Mix all ingredients together.</step> <step>Knead thoroughly.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Knead again.</step> <step>Place in a bread baking tin.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Bake in the oven at 180(degrees)C for 30 minutes.</step> </instructions> </recipe> "

    .line 86
    invoke-static {v1}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 87
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 88
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->println()V

    .line 90
    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 91
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 92
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v2}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 93
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->println()V

    .line 95
    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 96
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 97
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 98
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/io/PrintStream;->println()V

    .line 100
    const-string v1, "<div id=\"demo\" class=\"JSONML\"><p>JSONML is a transformation between <b>JSON</b> and <b>XML</b> that preserves ordering of document features.</p><p>JSONML can work with JSON arrays or JSON objects.</p><p>Three<br/>little<br/>words</p></div>"

    .line 101
    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 102
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 103
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v2}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 104
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->println()V

    .line 106
    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 107
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 108
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v1}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 109
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/io/PrintStream;->println()V

    .line 111
    const-string v1, "<person created=\"2006-11-11T19:23\" modified=\"2006-12-31T23:59\">\n <firstName>Robert</firstName>\n <lastName>Smith</lastName>\n <address type=\"home\">\n <street>12345 Sixth Ave</street>\n <city>Anytown</city>\n <state>CA</state>\n <postalCode>98765-4321</postalCode>\n </address>\n </person>"

    .line 112
    invoke-static {v1}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 113
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 115
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;)V

    .line 116
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 118
    const-string v1, "{ \"entity\": { \"imageURL\": \"\", \"name\": \"IXXXXXXXXXXXXX\", \"id\": 12336, \"ratingCount\": null, \"averageRating\": null } }"

    .line 119
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 120
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 122
    new-instance v1, Lweibo4android/org/json/JSONStringer;

    invoke-direct {v1}, Lweibo4android/org/json/JSONStringer;-><init>()V

    .line 123
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "single"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "MARIE HAA\'S"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "Johnny"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "MARIE HAA\\\'S"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "foo"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "bar"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "baz"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/org/json/JSONWriter;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/org/json/JSONWriter;->object()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "quux"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "Thanks, Josh!"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/org/json/JSONWriter;->endObject()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    const-string v2, "obj keys"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->getNames(Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/org/json/JSONWriter;->endObject()Lweibo4android/org/json/JSONWriter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 126
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Lweibo4android/org/json/JSONStringer;

    invoke-direct {v2}, Lweibo4android/org/json/JSONStringer;-><init>()V

    invoke-virtual {v2}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const-string v3, "a"

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const-string v3, "b"

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endObject()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 128
    new-instance v1, Lweibo4android/org/json/JSONStringer;

    invoke-direct {v1}, Lweibo4android/org/json/JSONStringer;-><init>()V

    .line 129
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->array()Lweibo4android/org/json/JSONWriter;

    .line 130
    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONStringer;->value(J)Lweibo4android/org/json/JSONWriter;

    .line 131
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->array()Lweibo4android/org/json/JSONWriter;

    .line 132
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    .line 133
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->array()Lweibo4android/org/json/JSONWriter;

    .line 134
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    .line 135
    const-string v2, "empty-array"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    .line 136
    const-string v2, "answer"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const-wide/16 v3, 0x2a

    invoke-virtual {v2, v3, v4}, Lweibo4android/org/json/JSONWriter;->value(J)Lweibo4android/org/json/JSONWriter;

    .line 137
    const-string v2, "null"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    .line 138
    const-string v2, "false"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONWriter;->value(Z)Lweibo4android/org/json/JSONWriter;

    .line 139
    const-string v2, "true"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONWriter;->value(Z)Lweibo4android/org/json/JSONWriter;

    .line 140
    const-string v2, "big"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const-wide v3, 0x53e27ed9d50e89b3L    # 1.23456789E96

    invoke-virtual {v2, v3, v4}, Lweibo4android/org/json/JSONWriter;->value(D)Lweibo4android/org/json/JSONWriter;

    .line 141
    const-string v2, "small"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    const-wide v3, 0x2f576be43f1e4b54L    # 1.23456789E-80

    invoke-virtual {v2, v3, v4}, Lweibo4android/org/json/JSONWriter;->value(D)Lweibo4android/org/json/JSONWriter;

    .line 142
    const-string v2, "empty-object"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->object()Lweibo4android/org/json/JSONWriter;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/org/json/JSONWriter;->endObject()Lweibo4android/org/json/JSONWriter;

    .line 143
    const-string v2, "long"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    .line 144
    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONStringer;->value(J)Lweibo4android/org/json/JSONWriter;

    .line 145
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endObject()Lweibo4android/org/json/JSONWriter;

    .line 146
    const-string v2, "two"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    .line 147
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endArray()Lweibo4android/org/json/JSONWriter;

    .line 148
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->value(Z)Lweibo4android/org/json/JSONWriter;

    .line 149
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endArray()Lweibo4android/org/json/JSONWriter;

    .line 150
    const-wide v2, 0x4058a66666666666L    # 98.6

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONStringer;->value(D)Lweibo4android/org/json/JSONWriter;

    .line 151
    const-wide/high16 v2, -0x3fa7000000000000L    # -100.0

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONStringer;->value(D)Lweibo4android/org/json/JSONWriter;

    .line 152
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    .line 153
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endObject()Lweibo4android/org/json/JSONWriter;

    .line 154
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    .line 155
    const-string v2, "one"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONStringer;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    .line 156
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONStringer;->value(D)Lweibo4android/org/json/JSONWriter;

    .line 157
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endObject()Lweibo4android/org/json/JSONWriter;

    .line 158
    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONStringer;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    .line 159
    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->endArray()Lweibo4android/org/json/JSONWriter;

    .line 160
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 162
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Lweibo4android/org/json/JSONArray;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v3, v1}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 164
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 165
    new-instance v2, Lweibo4android/org/json/JSONArray;

    invoke-direct {v2, v1}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/Object;)V

    .line 166
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 168
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "aString"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "aNumber"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "aBoolean"

    aput-object v3, v1, v2

    .line 169
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2, v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;[Ljava/lang/String;)V

    .line 170
    const-string v1, "Testing JSONString interface"

    invoke-virtual {v2, v1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 171
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 173
    new-instance v0, Lweibo4android/org/json/JSONObject;

    const-string v1, "{slashes: \'///\', closetag: \'</script>\', backslash:\'\\\\\', ei: {quotes: \'\"\\\'\'},eo: {a: \'\"quoted\"\', b:\"don\'t\"}, quotes: [\"\'\", \'\"\']}"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 175
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 176
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 178
    new-instance v0, Lweibo4android/org/json/JSONObject;

    const-string v1, "{foo: [true, false,9876543210,    0.0, 1.00000001,  1.000000000001, 1.00000000000000001, .00000000000000001, 2.00, 0.1, 2e100, -32,[],{}, \"string\"],   to   : null, op : \'Good\',ten:10} postfix comment"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 179
    const-string v1, "String"

    const-string v2, "98.6"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 180
    const-string v1, "JSONObject"

    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2}, Lweibo4android/org/json/JSONObject;-><init>()V

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 181
    const-string v1, "JSONArray"

    new-instance v2, Lweibo4android/org/json/JSONArray;

    invoke-direct {v2}, Lweibo4android/org/json/JSONArray;-><init>()V

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 182
    const-string v1, "int"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;I)Lweibo4android/org/json/JSONObject;

    .line 183
    const-string v1, "double"

    const-wide v2, 0x45f8ee90ff6c373eL    # 1.2345678901234568E29

    invoke-virtual {v0, v1, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;D)Lweibo4android/org/json/JSONObject;

    .line 184
    const-string v1, "true"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Z)Lweibo4android/org/json/JSONObject;

    .line 185
    const-string v1, "false"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Z)Lweibo4android/org/json/JSONObject;

    .line 186
    const-string v1, "null"

    sget-object v2, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 187
    const-string v1, "bool"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 188
    const-string v1, "zero"

    const-wide/high16 v2, -0x8000000000000000L

    invoke-virtual {v0, v1, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;D)Lweibo4android/org/json/JSONObject;

    .line 189
    const-string v1, "\\u2028"

    const-string v2, "\u2028"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 190
    const-string v1, "\\u2029"

    const-string v2, "\u2029"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 191
    const-string v1, "foo"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 192
    const/16 v2, 0x29a

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(I)Lweibo4android/org/json/JSONArray;

    .line 193
    const-wide v2, 0x409f47f5c28f5c29L    # 2001.99

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONArray;->put(D)Lweibo4android/org/json/JSONArray;

    .line 194
    const-string v2, "so \"fine\"."

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 195
    const-string v2, "so <fine>."

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 196
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Z)Lweibo4android/org/json/JSONArray;

    .line 197
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Z)Lweibo4android/org/json/JSONArray;

    .line 198
    new-instance v2, Lweibo4android/org/json/JSONArray;

    invoke-direct {v2}, Lweibo4android/org/json/JSONArray;-><init>()V

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 199
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2}, Lweibo4android/org/json/JSONObject;-><init>()V

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 200
    const-string v1, "keys"

    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->getNames(Lweibo4android/org/json/JSONObject;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 201
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 202
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 204
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "String"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 205
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  bool: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "bool"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 206
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "to"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 207
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  true: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 208
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   foo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "foo"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 209
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    op: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "op"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 210
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   ten: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ten"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 211
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  oops: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "oops"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 213
    const-string v0, "<xml one = 1 two=\' \"2\" \'><five></five>First \t&lt;content&gt;<five></five> This is \"content\". <three>  3  </three>JSON does not preserve the sequencing of elements and contents.<three>  III  </three>  <three>  T H R E E</three><four/>Content text is an implied structure in XML. <six content=\"6\"/>JSON does not have implied structure:<seven>7</seven>everything is explicit.<![CDATA[CDATA blocks<are><supported>!]]></xml>"

    .line 214
    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 215
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 216
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v1}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 217
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 219
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 220
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 221
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 222
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 224
    const-string v0, "<xml do=\'0\'>uno<a re=\'1\' mi=\'2\'>dos<b fa=\'3\'/>tres<c>true</c>quatro</a>cinqo<d>seis<e/></d></xml>"

    .line 225
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 226
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 227
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 228
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 230
    const-string v0, "<mapping><empty/>   <class name = \"Customer\">      <field name = \"ID\" type = \"string\">         <bind-xml name=\"ID\" node=\"attribute\"/>      </field>      <field name = \"FirstName\" type = \"FirstName\"/>      <field name = \"MI\" type = \"MI\"/>      <field name = \"LastName\" type = \"LastName\"/>   </class>   <class name = \"FirstName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"MI\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"LastName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class></mapping>"

    .line 231
    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 233
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 234
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v1}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 235
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 236
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 237
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 238
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 239
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 241
    const-string v0, "<?xml version=\"1.0\" ?><Book Author=\"Anonymous\"><Title>Sample Book</Title><Chapter id=\"1\">This is chapter 1. It is not very long or interesting.</Chapter><Chapter id=\"2\">This is chapter 2. Although it is longer than chapter 1, it is not any more interesting.</Chapter></Book>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 243
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 244
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 245
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 247
    const-string v0, "<!DOCTYPE bCard \'http://www.cs.caltech.edu/~adam/schemas/bCard\'><bCard><?xml default bCard        firstname = \'\'        lastname  = \'\' company   = \'\' email = \'\' homepage  = \'\'?><bCard        firstname = \'Rohit\'        lastname  = \'Khare\'        company   = \'MCI\'        email     = \'khare@mci.net\'        homepage  = \'http://pest.w3.org/\'/><bCard        firstname = \'Adam\'        lastname  = \'Rifkin\'        company   = \'Caltech Infospheres Project\'        email     = \'adam@cs.caltech.edu\'        homepage  = \'http://www.cs.caltech.edu/~adam/\'/></bCard>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 249
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 250
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 251
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 253
    const-string v0, "<?xml version=\"1.0\"?><customer>    <firstName>        <text>Fred</text>    </firstName>    <ID>fbs0001</ID>    <lastName> <text>Scerbo</text>    </lastName>    <MI>        <text>B</text>    </MI></customer>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 254
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 255
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 256
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 258
    const-string v0, "<!ENTITY tp-address PUBLIC \'-//ABC University::Special Collections Library//TEXT (titlepage: name and address)//EN\' \'tpspcoll.sgm\'><list type=\'simple\'><head>Repository Address </head><item>Special Collections Library</item><item>ABC University</item><item>Main Library, 40 Circle Drive</item><item>Ourtown, Pennsylvania</item><item>17654 USA</item></list>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 260
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 261
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 262
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 264
    const-string v0, "<test intertag status=ok><empty/>deluxe<blip sweet=true>&amp;&quot;toot&quot;&toot;&#x41;</blip><x>eks</x><w>bonus</w><w>bonus2</w></test>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 265
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 266
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 267
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 269
    const-string v0, "GET / HTTP/1.0\nAccept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, */*\nAccept-Language: en-us\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90; T312461; Q312461)\nHost: www.nokko.com\nConnection: keep-alive\nAccept-encoding: gzip, deflate\n"

    invoke-static {v0}, Lweibo4android/org/json/HTTP;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 271
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 272
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/HTTP;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 273
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 275
    const-string v0, "HTTP/1.1 200 Oki Doki\nDate: Sun, 26 May 2002 17:38:52 GMT\nServer: Apache/1.3.23 (Unix) mod_perl/1.26\nKeep-Alive: timeout=15, max=100\nConnection: Keep-Alive\nTransfer-Encoding: chunked\nContent-Type: text/html\n"

    invoke-static {v0}, Lweibo4android/org/json/HTTP;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 276
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 277
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/HTTP;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 278
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 280
    new-instance v0, Lweibo4android/org/json/JSONObject;

    const-string v1, "{nix: null, nux: false, null: \'null\', \'Request-URI\': \'/\', Method: \'GET\', \'HTTP-Version\': \'HTTP/1.0\'}"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 281
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 282
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNull: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "nix"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 283
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   has: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "nix"

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 284
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 285
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/HTTP;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 286
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 288
    const-string v0, "<?xml version=\'1.0\' encoding=\'UTF-8\'?>\n\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><ns1:doGoogleSearch xmlns:ns1=\"urn:GoogleSearch\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><key xsi:type=\"xsd:string\">GOOGLEKEY</key> <q xsi:type=\"xsd:string\">\'+search+\'</q> <start xsi:type=\"xsd:int\">0</start> <maxResults xsi:type=\"xsd:int\">10</maxResults> <filter xsi:type=\"xsd:boolean\">true</filter> <restrict xsi:type=\"xsd:string\"></restrict> <safeSearch xsi:type=\"xsd:boolean\">false</safeSearch> <lr xsi:type=\"xsd:string\"></lr> <ie xsi:type=\"xsd:string\">latin1</ie> <oe xsi:type=\"xsd:string\">latin1</oe></ns1:doGoogleSearch></SOAP-ENV:Body></SOAP-ENV:Envelope>"

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 292
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 293
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 294
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 296
    new-instance v0, Lweibo4android/org/json/JSONObject;

    const-string v1, "{Envelope: {Body: {\"ns1:doGoogleSearch\": {oe: \"latin1\", filter: true, q: \"\'+search+\'\", key: \"GOOGLEKEY\", maxResults: 10, \"SOAP-ENV:encodingStyle\": \"http://schemas.xmlsoap.org/soap/encoding/\", start: 0, ie: \"latin1\", safeSearch:false, \"xmlns:ns1\": \"urn:GoogleSearch\"}}}}"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 298
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 299
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 300
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 302
    const-string v0, "  f%oo = b+l=ah  ; o;n%40e = t.wo "

    invoke-static {v0}, Lweibo4android/org/json/CookieList;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 303
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 304
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/CookieList;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 305
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 307
    const-string v0, "f%oo=blah; secure ;expires = April 24, 2002"

    invoke-static {v0}, Lweibo4android/org/json/Cookie;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 308
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 309
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/Cookie;->toString(Lweibo4android/org/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 310
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 312
    new-instance v0, Lweibo4android/org/json/JSONObject;

    const-string v1, "{script: \'It is not allowed in HTML to send a close script tag in a string<script>because it confuses browsers</script>so we insert a backslash before the /\'}"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 313
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 314
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 316
    new-instance v0, Lweibo4android/org/json/JSONTokener;

    const-string v1, "{op:\'test\', to:\'session\', pre:1}{op:\'test\', to:\'session\', pre:2}"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 317
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Lweibo4android/org/json/JSONTokener;)V

    .line 318
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 319
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pre: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "pre"

    invoke-virtual {v1, v4}, Lweibo4android/org/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 320
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONTokener;->skipTo(C)C

    move-result v1

    .line 321
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(I)V

    .line 322
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Lweibo4android/org/json/JSONTokener;)V

    .line 323
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 324
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 326
    const-string v0, "No quotes, \'Single Quotes\', \"Double Quotes\"\n1,\'2\',\"3\"\n,\'It is \"good,\"\', \"It works.\"\n\n"

    invoke-static {v0}, Lweibo4android/org/json/CDL;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 328
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/CDL;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 329
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 330
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 331
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 333
    new-instance v0, Lweibo4android/org/json/JSONArray;

    const-string v1, " [\"<escape>\", next is an implied null , , ok,] "

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 334
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 335
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 336
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 337
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 339
    new-instance v1, Lweibo4android/org/json/JSONObject;

    const-string v2, "{ fun => with non-standard forms ; forgiving => This package can be used to parse formats that are similar to but not stricting conforming to JSON; why=To make it easier to migrate existing data to JSON,one = [[1.00]]; uno=[[{1=>1}]];\'+\':+6e66 ;pluses=+++;empty = \'\' , \'double\':0.666,true: TRUE, false: FALSE, null=NULL;[true] = [[!,@;*]]; string=>  o. k. ; \r oct=0666; hex=0x666; dec=666; o=0999; noh=0x0x}"

    invoke-direct {v1, v2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 341
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 342
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 343
    const-string v2, "true"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "false"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "It\'s all good"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 347
    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 348
    new-instance v2, Lweibo4android/org/json/JSONObject;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "dec"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "oct"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "hex"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "missing"

    aput-object v5, v3, v4

    invoke-direct {v2, v1, v3}, Lweibo4android/org/json/JSONObject;-><init>(Lweibo4android/org/json/JSONObject;[Ljava/lang/String;)V

    .line 349
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 351
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 352
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Lweibo4android/org/json/JSONStringer;

    invoke-direct {v3}, Lweibo4android/org/json/JSONStringer;-><init>()V

    invoke-virtual {v3}, Lweibo4android/org/json/JSONStringer;->array()Lweibo4android/org/json/JSONWriter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/org/json/JSONWriter;->endArray()Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 354
    new-instance v1, Lweibo4android/org/json/JSONObject;

    const-string v0, "{string: \"98.6\", long: 2147483648, int: 2147483647, longer: 9223372036854775807, double: 9223372036854775808}"

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 355
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 357
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\ngetInt"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 358
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "int    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "int"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 359
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "long   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "long"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 360
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "longer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "longer"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 361
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "double "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "double"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 362
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "string "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "string"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 364
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\ngetLong"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 365
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "int    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "int"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 366
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "long   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "long"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 367
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "longer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "longer"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 368
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "double "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "double"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 369
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "string "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "string"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 371
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\ngetDouble"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 372
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "int    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "int"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 373
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "long   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "long"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 374
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "longer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "longer"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 375
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "double "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "double"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 376
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "string "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "string"

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 378
    const-string v0, "good sized"

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v0, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;J)Lweibo4android/org/json/JSONObject;

    .line 379
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 381
    new-instance v0, Lweibo4android/org/json/JSONArray;

    const-string v2, "[2147483647, 2147483648, 9223372036854775807, 9223372036854775808]"

    invoke-direct {v0, v2}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 382
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 384
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\nKeys: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 385
    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 386
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 571
    :catch_0
    move-exception v0

    .line 572
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 574
    :goto_1
    return-void

    .line 391
    :cond_1
    :try_start_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\naccumulate: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 392
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 393
    const-string v1, "stooge"

    const-string v2, "Curly"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 394
    const-string v1, "stooge"

    const-string v2, "Larry"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 395
    const-string v1, "stooge"

    const-string v2, "Moe"

    invoke-virtual {v0, v1, v2}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 396
    const-string v1, "stooge"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 397
    const/4 v2, 0x5

    const-string v3, "Shemp"

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 398
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 400
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\nwrite:"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 401
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->write(Ljava/io/Writer;)Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 403
    const-string v0, "<xml empty><a></a><a>1</a><a>22</a><a>333</a></xml>"

    .line 404
    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 405
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 406
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 408
    const-string v0, "<book><chapter>Content of the first chapter</chapter><chapter>Content of the second chapter      <chapter>Content of the first subchapter</chapter>      <chapter>Content of the second subchapter</chapter></chapter><chapter>Third Chapter</chapter></book>"

    .line 409
    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 410
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 411
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v1}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 413
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 414
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 415
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 417
    const/4 v0, 0x0

    .line 418
    const/4 v1, 0x0

    .line 420
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-direct {v2, v1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 421
    new-instance v3, Lweibo4android/org/json/JSONArray;

    invoke-direct {v3, v0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 422
    const-string v4, "stooge"

    const-string v5, "Joe DeRita"

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->append(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 423
    const-string v4, "stooge"

    const-string v5, "Shemp"

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->append(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 424
    const-string v4, "stooges"

    const-string v5, "Curly"

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 425
    const-string v4, "stooges"

    const-string v5, "Larry"

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 426
    const-string v4, "stooges"

    const-string v5, "Moe"

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 427
    const-string v4, "stoogearray"

    const-string v5, "stooges"

    invoke-virtual {v2, v5}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lweibo4android/org/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 428
    const-string v4, "map"

    invoke-virtual {v2, v4, v1}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/util/Map;)Lweibo4android/org/json/JSONObject;

    .line 429
    const-string v4, "collection"

    invoke-virtual {v2, v4, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/util/Collection;)Lweibo4android/org/json/JSONObject;

    .line 430
    const-string v4, "array"

    invoke-virtual {v2, v4, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 431
    invoke-virtual {v3, v1}, Lweibo4android/org/json/JSONArray;->put(Ljava/util/Map;)Lweibo4android/org/json/JSONArray;

    .line 432
    invoke-virtual {v3, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/util/Collection;)Lweibo4android/org/json/JSONArray;

    .line 433
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 435
    const-string v0, "{plist=Apple; AnimalSmells = { pig = piggish; lamb = lambish; worm = wormy; }; AnimalSounds = { pig = oink; lamb = baa; worm = baa;  Lisa = \"Why is the worm talking like a lamb?\" } ; AnimalColors = { pig = pink; lamb = black; worm = pink; } } "

    .line 436
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 437
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 439
    const-string v0, " (\"San Francisco\", \"New York\", \"Seoul\", \"London\", \"Seattle\", \"Shanghai\")"

    .line 440
    new-instance v2, Lweibo4android/org/json/JSONArray;

    invoke-direct {v2, v0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 441
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 443
    const-string v0, "<a ichi=\'1\' ni=\'2\'><b>The content of b</b> and <c san=\'3\'>The content of c</c><d>do</d><e></e><d>re</d><f/><d>mi</d></a>"

    .line 444
    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v3

    .line 446
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 447
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v3}, Lweibo4android/org/json/XML;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 448
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 449
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 450
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 451
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 452
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 454
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\nTesting Exceptions: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 456
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 458
    :try_start_2
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1}, Lweibo4android/org/json/JSONArray;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 459
    const-wide/high16 v4, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    :try_start_3
    invoke-virtual {v1, v4, v5}, Lweibo4android/org/json/JSONArray;->put(D)Lweibo4android/org/json/JSONArray;

    .line 460
    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    invoke-virtual {v1, v4, v5}, Lweibo4android/org/json/JSONArray;->put(D)Lweibo4android/org/json/JSONArray;

    .line 461
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_12

    .line 465
    :goto_2
    :try_start_4
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 467
    :try_start_5
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "stooge"

    invoke-virtual {v3, v2}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/PrintStream;->println(D)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 471
    :goto_3
    :try_start_6
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 473
    :try_start_7
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "howard"

    invoke-virtual {v3, v2}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/PrintStream;->println(D)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 477
    :goto_4
    :try_start_8
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 479
    :try_start_9
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x0

    const-string v4, "howard"

    invoke-virtual {v3, v2, v4}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    .line 483
    :goto_5
    :try_start_a
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    .line 485
    :try_start_b
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->getDouble(I)D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/PrintStream;->println(D)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    .line 489
    :goto_6
    :try_start_c
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    .line 491
    :try_start_d
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7

    .line 495
    :goto_7
    :try_start_e
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    .line 497
    :try_start_f
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    invoke-virtual {v1, v4, v5}, Lweibo4android/org/json/JSONArray;->put(D)Lweibo4android/org/json/JSONArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8

    .line 501
    :goto_8
    :try_start_10
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    .line 503
    :try_start_11
    const-string v0, "<a><b>    "

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_9

    move-result-object v2

    .line 507
    :goto_9
    :try_start_12
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    .line 509
    :try_start_13
    const-string v0, "<a></b>    "

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_a

    move-result-object v2

    .line 513
    :goto_a
    :try_start_14
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_0

    .line 515
    :try_start_15
    const-string v0, "<a></a    "

    invoke-static {v0}, Lweibo4android/org/json/XML;->toJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_b

    move-result-object v2

    .line 519
    :goto_b
    :try_start_16
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_0

    .line 521
    :try_start_17
    new-instance v0, Lweibo4android/org/json/JSONArray;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/Object;)V

    .line 522
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_c

    .line 527
    :goto_c
    :try_start_18
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_0

    .line 529
    :try_start_19
    const-string v0, "[)"

    .line 530
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 531
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d

    .line 536
    :goto_d
    :try_start_1a
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_0

    .line 538
    :try_start_1b
    const-string v0, "<xml"

    .line 539
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 540
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_e

    .line 545
    :goto_e
    :try_start_1c
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_0

    .line 547
    :try_start_1d
    const-string v0, "<right></wrong>"

    .line 548
    invoke-static {v0}, Lweibo4android/org/json/JSONML;->toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 549
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lweibo4android/org/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_f

    .line 554
    :goto_f
    :try_start_1e
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Exception: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_0

    .line 556
    :try_start_1f
    const-string v0, "{\"koda\": true, \"koda\": true}"

    .line 557
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_10

    .line 558
    :try_start_20
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_11

    .line 563
    :goto_10
    :try_start_21
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Exception: "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_0

    .line 565
    :try_start_22
    new-instance v0, Lweibo4android/org/json/JSONStringer;

    invoke-direct {v0}, Lweibo4android/org/json/JSONStringer;-><init>()V

    .line 566
    invoke-virtual {v0}, Lweibo4android/org/json/JSONStringer;->object()Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    const-string v2, "bosanda"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    const-string v2, "MARIE HAA\'S"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    const-string v2, "bosanda"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONWriter;->key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    const-string v2, "MARIE HAA\\\'S"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/org/json/JSONWriter;->endObject()Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 567
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_1

    goto/16 :goto_1

    .line 568
    :catch_1
    move-exception v0

    .line 569
    :try_start_23
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 462
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 463
    :goto_11
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 468
    :catch_3
    move-exception v0

    .line 469
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 474
    :catch_4
    move-exception v0

    .line 475
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 480
    :catch_5
    move-exception v0

    .line 481
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 486
    :catch_6
    move-exception v0

    .line 487
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 492
    :catch_7
    move-exception v0

    .line 493
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 498
    :catch_8
    move-exception v0

    .line 499
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_8

    .line 504
    :catch_9
    move-exception v0

    .line 505
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    move-object v2, v3

    goto/16 :goto_9

    .line 510
    :catch_a
    move-exception v0

    .line 511
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_a

    .line 516
    :catch_b
    move-exception v0

    .line 517
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 523
    :catch_c
    move-exception v0

    .line 524
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_c

    .line 532
    :catch_d
    move-exception v0

    .line 533
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_d

    .line 541
    :catch_e
    move-exception v0

    .line 542
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_e

    .line 550
    :catch_f
    move-exception v0

    .line 551
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_f

    .line 559
    :catch_10
    move-exception v0

    move-object v1, v2

    .line 560
    :goto_12
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_0

    goto/16 :goto_10

    .line 559
    :catch_11
    move-exception v0

    goto :goto_12

    .line 462
    :catch_12
    move-exception v0

    goto :goto_11

    .line 164
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method
