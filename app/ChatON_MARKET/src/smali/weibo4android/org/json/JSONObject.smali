.class public Lweibo4android/org/json/JSONObject;
.super Ljava/lang/Object;
.source "JSONObject.java"


# static fields
.field public static final NULL:Ljava/lang/Object;


# instance fields
.field private map:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lweibo4android/org/json/JSONObject$Null;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONObject$Null;-><init>(Lweibo4android/org/json/JSONObject$1;)V

    sput-object v0, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    .line 134
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 270
    invoke-direct {p0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->populateInternalMap(Ljava/lang/Object;Z)V

    .line 272
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 288
    invoke-direct {p0, p1, p2}, Lweibo4android/org/json/JSONObject;->populateInternalMap(Ljava/lang/Object;Z)V

    .line 289
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 363
    invoke-direct {p0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 364
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 365
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 366
    aget-object v2, p2, v0

    .line 368
    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lweibo4android/org/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 373
    :cond_0
    return-void

    .line 369
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 389
    new-instance v0, Lweibo4android/org/json/JSONTokener;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONObject;-><init>(Lweibo4android/org/json/JSONTokener;)V

    .line 390
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    :cond_0
    iput-object p1, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    .line 228
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Z)V
    .locals 5

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    .line 241
    if-eqz p1, :cond_0

    .line 242
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 244
    iget-object v2, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Lweibo4android/org/json/JSONObject;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v4, v0, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;Z)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 247
    :cond_0
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONObject;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 150
    invoke-direct {p0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 151
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 152
    aget-object v1, p2, v0

    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lweibo4android/org/json/JSONObject;->putOnce(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONTokener;)V
    .locals 3

    .prologue
    .line 166
    invoke-direct {p0}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 170
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_1

    .line 171
    const-string v0, "A JSONObject text must begin with \'{\'"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 209
    :cond_0
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 174
    :cond_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    .line 175
    sparse-switch v0, :sswitch_data_0

    .line 181
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 182
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v1

    .line 190
    const/16 v2, 0x3d

    if-ne v1, v2, :cond_3

    .line 191
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    const/16 v2, 0x3e

    if-eq v1, v2, :cond_2

    .line 192
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 197
    :cond_2
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lweibo4android/org/json/JSONObject;->putOnce(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 203
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 214
    const-string v0, "Expected a \',\' or \'}\'"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 177
    :sswitch_0
    const-string v0, "A JSONObject text must end with \'}\'"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 194
    :cond_3
    const/16 v2, 0x3a

    if-eq v1, v2, :cond_2

    .line 195
    const-string v0, "Expected a \':\' after a key"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 206
    :sswitch_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    const/16 v1, 0x7d

    if-ne v0, v1, :cond_0

    .line 212
    :sswitch_2
    return-void

    .line 175
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7d -> :sswitch_2
    .end sparse-switch

    .line 203
    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_1
        0x3b -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method public static doubleToString(D)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 458
    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 459
    :cond_0
    const-string v0, "null"

    .line 473
    :cond_1
    :goto_0
    return-object v0

    .line 464
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 465
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_1

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    .line 466
    :goto_1
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 467
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 469
    :cond_3
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 470
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getNames(Ljava/lang/Object;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 663
    if-nez p0, :cond_1

    .line 676
    :cond_0
    return-object v0

    .line 666
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 667
    invoke-virtual {v1}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 668
    array-length v3, v2

    .line 669
    if-eqz v3, :cond_0

    .line 672
    new-array v0, v3, [Ljava/lang/String;

    .line 673
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 674
    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 673
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getNames(Lweibo4android/org/json/JSONObject;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 643
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v0

    .line 644
    if-nez v0, :cond_0

    .line 645
    const/4 v0, 0x0

    .line 654
    :goto_0
    return-object v0

    .line 647
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 648
    new-array v2, v0, [Ljava/lang/String;

    .line 649
    const/4 v0, 0x0

    move v1, v0

    .line 650
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1

    .line 652
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 654
    goto :goto_0
.end method

.method private isStandardProperty(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p1}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Byte;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Short;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Float;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Double;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Character;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static numberToString(Ljava/lang/Number;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 764
    if-nez p0, :cond_0

    .line 765
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Null pointer"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 767
    :cond_0
    invoke-static {p0}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 771
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 772
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_2

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_2

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_2

    .line 773
    :goto_0
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 774
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 776
    :cond_1
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 777
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 780
    :cond_2
    return-object v0
.end method

.method private populateInternalMap(Ljava/lang/Object;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 292
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    if-nez v1, :cond_0

    move p2, v2

    .line 300
    :cond_0
    if-eqz p2, :cond_3

    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    move-object v1, v0

    .line 301
    :goto_0
    array-length v0, v1

    if-ge v2, v0, :cond_d

    .line 303
    :try_start_0
    aget-object v4, v1, v2

    .line 304
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    .line 305
    const-string v0, ""

    .line 306
    const-string v5, "get"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 307
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 311
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_2

    .line 312
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v7, :cond_5

    .line 313
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 318
    :goto_2
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v4, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 319
    if-nez v0, :cond_6

    .line 320
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    sget-object v4, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_2
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 300
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 308
    :cond_4
    :try_start_1
    const-string v5, "is"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 309
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 314
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-nez v3, :cond_e

    .line 315
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    .line 321
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 322
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    new-instance v5, Lweibo4android/org/json/JSONArray;

    invoke-direct {v5, v0, p2}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/Object;Z)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 337
    :catch_0
    move-exception v0

    .line 338
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 323
    :cond_7
    :try_start_2
    instance-of v4, v0, Ljava/util/Collection;

    if-eqz v4, :cond_8

    .line 324
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    new-instance v5, Lweibo4android/org/json/JSONArray;

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v5, v0, p2}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;Z)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 325
    :cond_8
    instance-of v4, v0, Ljava/util/Map;

    if-eqz v4, :cond_9

    .line 326
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    new-instance v5, Lweibo4android/org/json/JSONObject;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v5, v0, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;Z)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 327
    :cond_9
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {p0, v4}, Lweibo4android/org/json/JSONObject;->isStandardProperty(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 328
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 330
    :cond_a
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "java"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    if-nez v4, :cond_c

    .line 331
    :cond_b
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 333
    :cond_c
    iget-object v4, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    new-instance v5, Lweibo4android/org/json/JSONObject;

    invoke-direct {v5, v0, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;Z)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    .line 341
    :cond_d
    return-void

    :cond_e
    move-object v3, v0

    goto/16 :goto_2
.end method

.method public static quote(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x5c

    const/16 v7, 0x22

    const/4 v0, 0x0

    .line 1164
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1165
    :cond_0
    const-string v0, "\"\""

    .line 1216
    :goto_0
    return-object v0

    .line 1171
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 1172
    new-instance v4, Ljava/lang/StringBuffer;

    add-int/lit8 v1, v3, 0x4

    invoke-direct {v4, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1175
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v1, v0

    .line 1176
    :goto_1
    if-ge v0, v3, :cond_6

    .line 1178
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1179
    sparse-switch v2, :sswitch_data_0

    .line 1207
    const/16 v1, 0x20

    if-lt v2, v1, :cond_3

    const/16 v1, 0x80

    if-lt v2, v1, :cond_2

    const/16 v1, 0xa0

    if-lt v2, v1, :cond_3

    :cond_2
    const/16 v1, 0x2000

    if-lt v2, v1, :cond_5

    const/16 v1, 0x2100

    if-ge v2, v1, :cond_5

    .line 1208
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "000"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1209
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\\u"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1176
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    .line 1182
    :sswitch_0
    invoke-virtual {v4, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1183
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1186
    :sswitch_1
    const/16 v5, 0x3c

    if-ne v1, v5, :cond_4

    .line 1187
    invoke-virtual {v4, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1189
    :cond_4
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1192
    :sswitch_2
    const-string v1, "\\b"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1195
    :sswitch_3
    const-string v1, "\\t"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1198
    :sswitch_4
    const-string v1, "\\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1201
    :sswitch_5
    const-string v1, "\\f"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1204
    :sswitch_6
    const-string v1, "\\r"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1211
    :cond_5
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1215
    :cond_6
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1216
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1179
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xa -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0x22 -> :sswitch_0
        0x2f -> :sswitch_1
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method public static stringToValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/16 v4, 0x30

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1250
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1302
    :cond_0
    :goto_0
    return-object p0

    .line 1253
    :cond_1
    const-string v0, "true"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1254
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1256
    :cond_2
    const-string v0, "false"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1257
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1259
    :cond_3
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1260
    sget-object p0, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    goto :goto_0

    .line 1271
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1272
    if-lt v0, v4, :cond_5

    const/16 v1, 0x39

    if-le v0, v1, :cond_6

    :cond_5
    const/16 v1, 0x2e

    if-eq v0, v1, :cond_6

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_6

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_0

    .line 1273
    :cond_6
    if-ne v0, v4, :cond_9

    .line 1274
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_8

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x78

    if-eq v0, v1, :cond_7

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x58

    if-ne v0, v1, :cond_8

    .line 1276
    :cond_7
    :try_start_0
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-object p0, v0

    goto :goto_0

    .line 1282
    :cond_8
    :try_start_1
    new-instance v0, Ljava/lang/Integer;

    const/16 v1, 0x8

    invoke-static {p0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object p0, v0

    goto :goto_0

    .line 1283
    :catch_0
    move-exception v0

    .line 1289
    :cond_9
    :goto_1
    :try_start_2
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p0}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object p0, v0

    goto :goto_0

    .line 1290
    :catch_1
    move-exception v0

    .line 1292
    :try_start_3
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p0}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-object p0, v0

    goto :goto_0

    .line 1293
    :catch_2
    move-exception v0

    .line 1295
    :try_start_4
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p0}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-object p0, v0

    goto/16 :goto_0

    .line 1296
    :catch_3
    move-exception v0

    goto/16 :goto_0

    .line 1277
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method static testValidity(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1314
    if-eqz p0, :cond_3

    .line 1315
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 1316
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->isInfinite()Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p0, Ljava/lang/Double;

    invoke-virtual {p0}, Ljava/lang/Double;->isNaN()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1317
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "JSON does not allow non-finite numbers."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1319
    :cond_1
    instance-of v0, p0, Ljava/lang/Float;

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 1320
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->isInfinite()Z

    move-result v0

    if-nez v0, :cond_2

    check-cast p0, Ljava/lang/Float;

    invoke-virtual {p0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1321
    :cond_2
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "JSON does not allow non-finite numbers."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1325
    :cond_3
    return-void
.end method

.method static valueToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1484
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1485
    :cond_0
    const-string v0, "null"

    .line 1514
    :goto_0
    return-object v0

    .line 1487
    :cond_1
    instance-of v0, p0, Lweibo4android/org/json/JSONString;

    if-eqz v0, :cond_3

    .line 1490
    :try_start_0
    check-cast p0, Lweibo4android/org/json/JSONString;

    invoke-interface {p0}, Lweibo4android/org/json/JSONString;->toJSONString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1494
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1495
    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1491
    :catch_0
    move-exception v0

    .line 1492
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1497
    :cond_2
    new-instance v1, Lweibo4android/org/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad value from toJSONString: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1499
    :cond_3
    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_4

    .line 1500
    check-cast p0, Ljava/lang/Number;

    invoke-static {p0}, Lweibo4android/org/json/JSONObject;->numberToString(Ljava/lang/Number;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1502
    :cond_4
    instance-of v0, p0, Ljava/lang/Boolean;

    if-nez v0, :cond_5

    instance-of v0, p0, Lweibo4android/org/json/JSONObject;

    if-nez v0, :cond_5

    instance-of v0, p0, Lweibo4android/org/json/JSONArray;

    if-eqz v0, :cond_6

    .line 1503
    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1505
    :cond_6
    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 1506
    new-instance v0, Lweibo4android/org/json/JSONObject;

    check-cast p0, Ljava/util/Map;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1508
    :cond_7
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_8

    .line 1509
    new-instance v0, Lweibo4android/org/json/JSONArray;

    check-cast p0, Ljava/util/Collection;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1511
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1512
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1514
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method static valueToString(Ljava/lang/Object;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1537
    if-eqz p0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1538
    :cond_0
    const-string v1, "null"

    .line 1571
    :goto_0
    return-object v1

    .line 1541
    :cond_1
    :try_start_0
    instance-of v1, p0, Lweibo4android/org/json/JSONString;

    if-eqz v1, :cond_2

    .line 1542
    move-object v0, p0

    check-cast v0, Lweibo4android/org/json/JSONString;

    move-object v1, v0

    invoke-interface {v1}, Lweibo4android/org/json/JSONString;->toJSONString()Ljava/lang/String;

    move-result-object v1

    .line 1543
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1544
    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1547
    :catch_0
    move-exception v1

    .line 1550
    :cond_2
    instance-of v1, p0, Ljava/lang/Number;

    if-eqz v1, :cond_3

    .line 1551
    check-cast p0, Ljava/lang/Number;

    invoke-static {p0}, Lweibo4android/org/json/JSONObject;->numberToString(Ljava/lang/Number;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1553
    :cond_3
    instance-of v1, p0, Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1554
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1556
    :cond_4
    instance-of v1, p0, Lweibo4android/org/json/JSONObject;

    if-eqz v1, :cond_5

    .line 1557
    check-cast p0, Lweibo4android/org/json/JSONObject;

    invoke-virtual {p0, p1, p2}, Lweibo4android/org/json/JSONObject;->toString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1559
    :cond_5
    instance-of v1, p0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_6

    .line 1560
    check-cast p0, Lweibo4android/org/json/JSONArray;

    invoke-virtual {p0, p1, p2}, Lweibo4android/org/json/JSONArray;->toString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1562
    :cond_6
    instance-of v1, p0, Ljava/util/Map;

    if-eqz v1, :cond_7

    .line 1563
    new-instance v1, Lweibo4android/org/json/JSONObject;

    check-cast p0, Ljava/util/Map;

    invoke-direct {v1, p0}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1, p1, p2}, Lweibo4android/org/json/JSONObject;->toString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1565
    :cond_7
    instance-of v1, p0, Ljava/util/Collection;

    if-eqz v1, :cond_8

    .line 1566
    new-instance v1, Lweibo4android/org/json/JSONArray;

    check-cast p0, Ljava/util/Collection;

    invoke-direct {v1, p0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, p1, p2}, Lweibo4android/org/json/JSONArray;->toString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1568
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1569
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1, p0}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, p2}, Lweibo4android/org/json/JSONArray;->toString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1571
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public accumulate(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 409
    invoke-static {p2}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 410
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 411
    if-nez v0, :cond_1

    .line 412
    instance-of v0, p2, Lweibo4android/org/json/JSONArray;

    if-eqz v0, :cond_0

    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0}, Lweibo4android/org/json/JSONArray;-><init>()V

    invoke-virtual {v0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1, p2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 418
    :goto_0
    return-object p0

    .line 413
    :cond_1
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_2

    .line 414
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-virtual {v0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_0

    .line 416
    :cond_2
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1}, Lweibo4android/org/json/JSONArray;-><init>()V

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_0
.end method

.method public append(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    .locals 3

    .prologue
    .line 437
    invoke-static {p2}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 438
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 439
    if-nez v0, :cond_0

    .line 440
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0}, Lweibo4android/org/json/JSONArray;-><init>()V

    invoke-virtual {v0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 446
    :goto_0
    return-object p0

    .line 441
    :cond_0
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_1

    .line 442
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-virtual {v0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    goto :goto_0

    .line 444
    :cond_1
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONObject["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a JSONArray."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 508
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 509
    if-nez v1, :cond_0

    move v0, v2

    .line 515
    :goto_0
    return v0

    .line 512
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    const-string v3, "false"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    .line 513
    goto :goto_0

    .line 514
    :cond_2
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_4

    check-cast v1, Ljava/lang/String;

    const-string v0, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 517
    :cond_4
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONObject["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a Boolean."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDouble(Ljava/lang/String;)D
    .locals 4

    .prologue
    const-wide/16 v1, 0x0

    .line 532
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 533
    if-nez v0, :cond_0

    move-wide v0, v1

    .line 542
    :goto_0
    return-wide v0

    .line 537
    :cond_0
    :try_start_0
    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_1

    .line 538
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    goto :goto_0

    .line 539
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 540
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v1

    .line 542
    goto :goto_0

    .line 544
    :catch_0
    move-exception v0

    .line 545
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONObject["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a number."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 562
    if-nez v0, :cond_0

    .line 563
    const/4 v0, 0x0

    .line 565
    :goto_0
    return v0

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 579
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 580
    if-nez v0, :cond_0

    .line 581
    const/4 v0, 0x0

    .line 584
    :goto_0
    return-object v0

    .line 583
    :cond_0
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_1

    .line 584
    check-cast v0, Lweibo4android/org/json/JSONArray;

    goto :goto_0

    .line 586
    :cond_1
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONObject["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a JSONArray."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 3

    .prologue
    .line 600
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 601
    if-nez v0, :cond_0

    .line 602
    const/4 v0, 0x0

    .line 605
    :goto_0
    return-object v0

    .line 604
    :cond_0
    instance-of v1, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v1, :cond_1

    .line 605
    check-cast v0, Lweibo4android/org/json/JSONObject;

    goto :goto_0

    .line 607
    :cond_1
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONObject["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a JSONObject."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 4

    .prologue
    const-wide/16 v1, 0x0

    .line 622
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 624
    if-nez v0, :cond_0

    move-wide v0, v1

    .line 634
    :goto_0
    return-wide v0

    .line 627
    :cond_0
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 628
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 629
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    move-wide v0, v1

    .line 631
    goto :goto_0

    .line 634
    :cond_2
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v0, v0

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 689
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 690
    if-nez v0, :cond_0

    .line 691
    const-string v0, ""

    .line 693
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public has(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isNull(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 717
    sget-object v0, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public keys()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public names()Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 746
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 747
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 748
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 749
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_0

    .line 751
    :cond_0
    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public opt(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 791
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public optBoolean(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public optBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 820
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 822
    :goto_0
    return p2

    .line 821
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optDouble(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 853
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/org/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public optDouble(Ljava/lang/String;D)D
    .locals 2

    .prologue
    .line 870
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 871
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 873
    :goto_0
    return-wide v0

    .line 871
    :cond_0
    new-instance v1, Ljava/lang/Double;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 872
    :catch_0
    move-exception v0

    move-wide v0, p2

    .line 873
    goto :goto_0
.end method

.method public optInt(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public optInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 905
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 907
    :goto_0
    return p2

    .line 906
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;
    .locals 2

    .prologue
    .line 921
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 922
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_0

    check-cast v0, Lweibo4android/org/json/JSONArray;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 935
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 936
    instance-of v1, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v1, :cond_0

    check-cast v0, Lweibo4android/org/json/JSONObject;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optLong(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 950
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/org/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public optLong(Ljava/lang/String;J)J
    .locals 1

    .prologue
    .line 967
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 969
    :goto_0
    return-wide p2

    .line 968
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 983
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 997
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 998
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method public put(Ljava/lang/String;D)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 1029
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p2, p3}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1030
    return-object p0
.end method

.method public put(Ljava/lang/String;I)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 1045
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1046
    return-object p0
.end method

.method public put(Ljava/lang/String;J)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 1061
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p2, p3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1062
    return-object p0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 1098
    if-nez p1, :cond_0

    .line 1099
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Null key."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1101
    :cond_0
    if-eqz p2, :cond_1

    .line 1102
    invoke-static {p2}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 1103
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107
    :goto_0
    return-object p0

    .line 1105
    :cond_1
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/util/Collection;)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 838
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0, p2}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 839
    return-object p0
.end method

.method public put(Ljava/lang/String;Ljava/util/Map;)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 1077
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1078
    return-object p0
.end method

.method public put(Ljava/lang/String;Z)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 1013
    if-eqz p2, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1014
    return-object p0

    .line 1013
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public putOnce(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    .locals 3

    .prologue
    .line 1122
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1123
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1124
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate key \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1126
    :cond_0
    invoke-virtual {p0, p1, p2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1128
    :cond_1
    return-object p0
.end method

.method public putOpt(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;
    .locals 0

    .prologue
    .line 1147
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1148
    invoke-virtual {p0, p1, p2}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 1150
    :cond_0
    return-object p0
.end method

.method public remove(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1228
    iget-object v0, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public sortedKeys()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 1238
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toJSONArray(Lweibo4android/org/json/JSONArray;)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 1339
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1340
    :cond_0
    const/4 v0, 0x0

    .line 1346
    :goto_0
    return-object v0

    .line 1342
    :cond_1
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 1343
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1344
    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 1343
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1346
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1364
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 1365
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1367
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1368
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 1369
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1371
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1372
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1373
    const/16 v3, 0x3a

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1374
    iget-object v3, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1378
    :catch_0
    move-exception v0

    .line 1379
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1376
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1377
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1
.end method

.method public toString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1399
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONObject;->toString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toString(II)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/16 v7, 0xa

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 1421
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v1

    .line 1422
    if-nez v1, :cond_0

    .line 1423
    const-string v0, "{}"

    .line 1457
    :goto_0
    return-object v0

    .line 1425
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->sortedKeys()Ljava/util/Iterator;

    move-result-object v2

    .line 1426
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1427
    add-int v4, p2, p1

    .line 1429
    if-ne v1, v6, :cond_3

    .line 1430
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1431
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1432
    const-string v1, ": "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1433
    iget-object v1, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1456
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1457
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1445
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1446
    const-string v1, ": "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1447
    iget-object v1, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p1, v4}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1435
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1436
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1437
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le v1, v6, :cond_4

    .line 1438
    const-string v1, ",\n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    move v1, v0

    .line 1442
    :goto_2
    if-ge v1, v4, :cond_2

    .line 1443
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1442
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1440
    :cond_4
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1449
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le v1, v6, :cond_1

    .line 1450
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1451
    :goto_3
    if-ge v0, p2, :cond_1

    .line 1452
    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1451
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public write(Ljava/io/Writer;)Ljava/io/Writer;
    .locals 3

    .prologue
    .line 1585
    const/4 v0, 0x0

    .line 1586
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 1587
    const/16 v2, 0x7b

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(I)V

    .line 1589
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1590
    if-eqz v0, :cond_0

    .line 1591
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 1593
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1594
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1595
    const/16 v2, 0x3a

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(I)V

    .line 1596
    iget-object v2, p0, Lweibo4android/org/json/JSONObject;->map:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1597
    instance-of v2, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v2, :cond_1

    .line 1598
    check-cast v0, Lweibo4android/org/json/JSONObject;

    invoke-virtual {v0, p1}, Lweibo4android/org/json/JSONObject;->write(Ljava/io/Writer;)Ljava/io/Writer;

    .line 1604
    :goto_1
    const/4 v0, 0x1

    .line 1605
    goto :goto_0

    .line 1599
    :cond_1
    instance-of v2, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v2, :cond_2

    .line 1600
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-virtual {v0, p1}, Lweibo4android/org/json/JSONArray;->write(Ljava/io/Writer;)Ljava/io/Writer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1608
    :catch_0
    move-exception v0

    .line 1609
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1602
    :cond_2
    :try_start_1
    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1606
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1607
    return-object p1
.end method
