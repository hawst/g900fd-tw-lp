.class public Lweibo4android/org/json/CDL;
.super Ljava/lang/Object;
.source "CDL.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getValue(Lweibo4android/org/json/JSONTokener;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v0

    .line 56
    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 57
    sparse-switch v0, :sswitch_data_0

    .line 67
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 68
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->nextTo(C)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 59
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :sswitch_1
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->nextString(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 64
    :sswitch_2
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 65
    const-string v0, ""

    goto :goto_0

    .line 57
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x27 -> :sswitch_1
        0x2c -> :sswitch_2
    .end sparse-switch
.end method

.method public static rowToJSONArray(Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 83
    :cond_0
    invoke-static {p0}, Lweibo4android/org/json/CDL;->getValue(Lweibo4android/org/json/JSONTokener;)Ljava/lang/String;

    move-result-object v1

    .line 84
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 85
    :cond_1
    const/4 v0, 0x0

    .line 95
    :cond_2
    return-object v0

    .line 87
    :cond_3
    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 89
    :cond_4
    invoke-virtual {p0}, Lweibo4android/org/json/JSONTokener;->next()C

    move-result v1

    .line 90
    const/16 v2, 0x2c

    if-eq v1, v2, :cond_0

    .line 93
    const/16 v2, 0x20

    if-eq v1, v2, :cond_4

    .line 94
    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_2

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad character \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0
.end method

.method public static rowToJSONObject(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONObject;
    .locals 1

    .prologue
    .line 118
    invoke-static {p1}, Lweibo4android/org/json/CDL;->rowToJSONArray(Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lweibo4android/org/json/JSONArray;->toJSONObject(Lweibo4android/org/json/JSONArray;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static rowToString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x2c

    const/16 v5, 0x27

    const/16 v4, 0x22

    .line 201
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 202
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 203
    if-lez v0, :cond_0

    .line 204
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 206
    :cond_0
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_1

    .line 208
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 209
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_3

    .line 210
    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_2

    .line 211
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 212
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 213
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 202
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 216
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 220
    :cond_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 224
    :cond_4
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 225
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lweibo4android/org/json/JSONTokener;

    invoke-direct {v0, p0}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lweibo4android/org/json/CDL;->toJSONArray(Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONArray(Lweibo4android/org/json/JSONArray;Ljava/lang/String;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lweibo4android/org/json/JSONTokener;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, Lweibo4android/org/json/CDL;->toJSONArray(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONArray(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 175
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-object v0

    .line 178
    :cond_1
    new-instance v1, Lweibo4android/org/json/JSONArray;

    invoke-direct {v1}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 180
    :goto_1
    invoke-static {p0, p1}, Lweibo4android/org/json/CDL;->rowToJSONObject(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 181
    if-nez v2, :cond_2

    .line 186
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 189
    goto :goto_0

    .line 184
    :cond_2
    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_1
.end method

.method public static toJSONArray(Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lweibo4android/org/json/CDL;->rowToJSONArray(Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/org/json/CDL;->toJSONArray(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONTokener;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static toString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->optJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {v0}, Lweibo4android/org/json/JSONObject;->names()Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    .line 244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lweibo4android/org/json/CDL;->rowToString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0, p0}, Lweibo4android/org/json/CDL;->toString(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toString(Lweibo4android/org/json/JSONArray;Lweibo4android/org/json/JSONArray;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    const/4 v0, 0x0

    .line 273
    :goto_0
    return-object v0

    .line 266
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 267
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 268
    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONArray;->optJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 269
    if-eqz v2, :cond_2

    .line 270
    invoke-virtual {v2, p0}, Lweibo4android/org/json/JSONObject;->toJSONArray(Lweibo4android/org/json/JSONArray;)Lweibo4android/org/json/JSONArray;

    move-result-object v2

    invoke-static {v2}, Lweibo4android/org/json/CDL;->rowToString(Lweibo4android/org/json/JSONArray;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 273
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
