.class public Lweibo4android/org/json/JSONArray;
.super Ljava/lang/Object;
.source "JSONArray.java"


# instance fields
.field private myArrayList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 176
    invoke-direct {p0}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 177
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 179
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 180
    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "JSONArray initial value should be a string or collection or array."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 195
    invoke-direct {p0}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 196
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 198
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 199
    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {p0, v2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "JSONArray initial value should be a string or collection or array."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lweibo4android/org/json/JSONTokener;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONArray;-><init>(Lweibo4android/org/json/JSONTokener;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    if-nez p1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    .line 150
    return-void

    .line 149
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Collection;Z)V
    .locals 4

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    .line 162
    if-eqz p1, :cond_0

    .line 163
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    new-instance v2, Lweibo4android/org/json/JSONObject;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 167
    :cond_0
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONTokener;)V
    .locals 4

    .prologue
    const/16 v1, 0x5d

    .line 84
    invoke-direct {p0}, Lweibo4android/org/json/JSONArray;-><init>()V

    .line 85
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v0

    .line 87
    const/16 v2, 0x5b

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 94
    :goto_0
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v2

    if-ne v2, v1, :cond_3

    .line 120
    :cond_0
    return-void

    .line 89
    :cond_1
    const/16 v2, 0x28

    if-ne v0, v2, :cond_2

    .line 90
    const/16 v0, 0x29

    goto :goto_0

    .line 92
    :cond_2
    const-string v0, "A JSONArray text must start with \'[\'"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 97
    :cond_3
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 99
    :goto_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v2

    const/16 v3, 0x2c

    if-ne v2, v3, :cond_4

    .line 100
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 101
    iget-object v2, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    :goto_2
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v2

    .line 107
    sparse-switch v2, :sswitch_data_0

    .line 122
    const-string v0, "Expected a \',\' or \']\'"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 103
    :cond_4
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    .line 104
    iget-object v2, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 110
    :sswitch_0
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->nextClean()C

    move-result v2

    if-eq v2, v1, :cond_0

    .line 113
    invoke-virtual {p1}, Lweibo4android/org/json/JSONTokener;->back()V

    goto :goto_1

    .line 117
    :sswitch_1
    if-eq v0, v2, :cond_0

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Character;

    invoke-direct {v2, v0}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lweibo4android/org/json/JSONException;

    move-result-object v0

    throw v0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x29 -> :sswitch_1
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    .line 217
    if-nez v0, :cond_0

    .line 218
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    return-object v0
.end method

.method public getBoolean(I)Z
    .locals 3

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 236
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    :cond_0
    const/4 v0, 0x0

    .line 239
    :goto_0
    return v0

    .line 238
    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_3

    check-cast v1, Ljava/lang/String;

    const-string v0, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 241
    :cond_3
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a Boolean."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDouble(I)D
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 257
    :try_start_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a number."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getInt(I)I
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 276
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public getJSONArray(I)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 290
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 291
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_0

    .line 292
    check-cast v0, Lweibo4android/org/json/JSONArray;

    return-object v0

    .line 294
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a JSONArray."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getJSONObject(I)Lweibo4android/org/json/JSONObject;
    .locals 3

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 309
    instance-of v1, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v1, :cond_0

    .line 310
    check-cast v0, Lweibo4android/org/json/JSONObject;

    return-object v0

    .line 312
    :cond_0
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not a JSONObject."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 326
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 327
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    double-to-long v0, v0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNull(I)Z
    .locals 2

    .prologue
    .line 351
    sget-object v0, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public join(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 365
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v1

    .line 366
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 368
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 369
    if-lez v0, :cond_0

    .line 370
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 372
    :cond_0
    iget-object v3, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 374
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public opt(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 395
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public optBoolean(I)Z
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->optBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public optBoolean(IZ)Z
    .locals 1

    .prologue
    .line 424
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getBoolean(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 426
    :goto_0
    return p2

    .line 425
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optDouble(I)D
    .locals 2

    .prologue
    .line 440
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/org/json/JSONArray;->optDouble(ID)D

    move-result-wide v0

    return-wide v0
.end method

.method public optDouble(ID)D
    .locals 1

    .prologue
    .line 456
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getDouble(I)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 458
    :goto_0
    return-wide p2

    .line 457
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optInt(I)I
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->optInt(II)I

    move-result v0

    return v0
.end method

.method public optInt(II)I
    .locals 1

    .prologue
    .line 488
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 490
    :goto_0
    return p2

    .line 489
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optJSONArray(I)Lweibo4android/org/json/JSONArray;
    .locals 2

    .prologue
    .line 503
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    .line 504
    instance-of v1, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v1, :cond_0

    check-cast v0, Lweibo4android/org/json/JSONArray;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optJSONObject(I)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    .line 518
    instance-of v1, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v1, :cond_0

    check-cast v0, Lweibo4android/org/json/JSONObject;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optLong(I)J
    .locals 2

    .prologue
    .line 531
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/org/json/JSONArray;->optLong(IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public optLong(IJ)J
    .locals 1

    .prologue
    .line 547
    :try_start_0
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 549
    :goto_0
    return-wide p2

    .line 548
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public optString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public optString(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 577
    invoke-virtual {p0, p1}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    .line 578
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method public put(D)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 616
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p1, p2}, Ljava/lang/Double;-><init>(D)V

    .line 617
    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 618
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 619
    return-object p0
.end method

.method public put(I)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 630
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 631
    return-object p0
.end method

.method public put(ID)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 725
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p2, p3}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 726
    return-object p0
.end method

.method public put(II)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 743
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 744
    return-object p0
.end method

.method public put(IJ)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 761
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p2, p3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 762
    return-object p0
.end method

.method public put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;
    .locals 3

    .prologue
    .line 801
    invoke-static {p2}, Lweibo4android/org/json/JSONObject;->testValidity(Ljava/lang/Object;)V

    .line 802
    if-gez p1, :cond_0

    .line 803
    new-instance v0, Lweibo4android/org/json/JSONException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSONArray["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 805
    :cond_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 806
    iget-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 813
    :goto_0
    return-object p0

    .line 808
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-eq p1, v0, :cond_2

    .line 809
    sget-object v0, Lweibo4android/org/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_1

    .line 811
    :cond_2
    invoke-virtual {p0, p2}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    goto :goto_0
.end method

.method public put(ILjava/util/Collection;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 706
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0, p2}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 707
    return-object p0
.end method

.method public put(ILjava/util/Map;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 779
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p2}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 780
    return-object p0
.end method

.method public put(IZ)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 688
    if-eqz p2, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->put(ILjava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 689
    return-object p0

    .line 688
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public put(J)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 642
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p1, p2}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 643
    return-object p0
.end method

.method public put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    return-object p0
.end method

.method public put(Ljava/util/Collection;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 602
    new-instance v0, Lweibo4android/org/json/JSONArray;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 603
    return-object p0
.end method

.method public put(Ljava/util/Map;)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 655
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 656
    return-object p0
.end method

.method public put(Z)Lweibo4android/org/json/JSONArray;
    .locals 1

    .prologue
    .line 589
    if-eqz p1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->put(Ljava/lang/Object;)Lweibo4android/org/json/JSONArray;

    .line 590
    return-object p0

    .line 589
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public toJSONObject(Lweibo4android/org/json/JSONArray;)Lweibo4android/org/json/JSONObject;
    .locals 4

    .prologue
    .line 829
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 830
    :cond_0
    const/4 v0, 0x0

    .line 836
    :goto_0
    return-object v0

    .line 832
    :cond_1
    new-instance v1, Lweibo4android/org/json/JSONObject;

    invoke-direct {v1}, Lweibo4android/org/json/JSONObject;-><init>()V

    .line 833
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 834
    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lweibo4android/org/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 833
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 836
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 853
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {p0, v1}, Lweibo4android/org/json/JSONArray;->join(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 855
    :goto_0
    return-object v0

    .line 854
    :catch_0
    move-exception v0

    .line 855
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lweibo4android/org/json/JSONArray;->toString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toString(II)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x20

    const/16 v6, 0xa

    const/4 v0, 0x0

    .line 890
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    .line 891
    if-nez v3, :cond_0

    .line 892
    const-string v0, "[]"

    .line 916
    :goto_0
    return-object v0

    .line 895
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v1, "["

    invoke-direct {v4, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 896
    const/4 v1, 0x1

    if-ne v3, v1, :cond_2

    .line 897
    iget-object v1, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 915
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 916
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 899
    :cond_2
    add-int v5, p2, p1

    .line 900
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v0

    .line 901
    :goto_1
    if-ge v2, v3, :cond_5

    .line 902
    if-lez v2, :cond_3

    .line 903
    const-string v1, ",\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    move v1, v0

    .line 905
    :goto_2
    if-ge v1, v5, :cond_4

    .line 906
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 905
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 908
    :cond_4
    iget-object v1, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p1, v5}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 901
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 910
    :cond_5
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 911
    :goto_3
    if-ge v0, p2, :cond_1

    .line 912
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 911
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public write(Ljava/io/Writer;)Ljava/io/Writer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 930
    .line 931
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    .line 933
    const/16 v1, 0x5b

    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(I)V

    move v1, v0

    .line 935
    :goto_0
    if-ge v1, v3, :cond_3

    .line 936
    if-eqz v0, :cond_0

    .line 937
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 939
    :cond_0
    iget-object v0, p0, Lweibo4android/org/json/JSONArray;->myArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 940
    instance-of v2, v0, Lweibo4android/org/json/JSONObject;

    if-eqz v2, :cond_1

    .line 941
    check-cast v0, Lweibo4android/org/json/JSONObject;

    invoke-virtual {v0, p1}, Lweibo4android/org/json/JSONObject;->write(Ljava/io/Writer;)Ljava/io/Writer;

    .line 947
    :goto_1
    const/4 v2, 0x1

    .line 935
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 942
    :cond_1
    instance-of v2, v0, Lweibo4android/org/json/JSONArray;

    if-eqz v2, :cond_2

    .line 943
    check-cast v0, Lweibo4android/org/json/JSONArray;

    invoke-virtual {v0, p1}, Lweibo4android/org/json/JSONArray;->write(Ljava/io/Writer;)Ljava/io/Writer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 951
    :catch_0
    move-exception v0

    .line 952
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 945
    :cond_2
    :try_start_1
    invoke-static {v0}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 949
    :cond_3
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 950
    return-object p1
.end method
