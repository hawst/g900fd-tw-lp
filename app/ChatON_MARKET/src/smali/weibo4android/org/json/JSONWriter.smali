.class public Lweibo4android/org/json/JSONWriter;
.super Ljava/lang/Object;
.source "JSONWriter.java"


# static fields
.field private static final maxdepth:I = 0x14


# instance fields
.field private comma:Z

.field protected mode:C

.field private stack:[Lweibo4android/org/json/JSONObject;

.field private top:I

.field protected writer:Ljava/io/Writer;


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean v1, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 94
    const/16 v0, 0x69

    iput-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    .line 95
    const/16 v0, 0x14

    new-array v0, v0, [Lweibo4android/org/json/JSONObject;

    iput-object v0, p0, Lweibo4android/org/json/JSONWriter;->stack:[Lweibo4android/org/json/JSONObject;

    .line 96
    iput v1, p0, Lweibo4android/org/json/JSONWriter;->top:I

    .line 97
    iput-object p1, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    .line 98
    return-void
.end method

.method private append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;
    .locals 3

    .prologue
    const/16 v2, 0x6f

    const/16 v1, 0x61

    .line 110
    if-nez p1, :cond_0

    .line 111
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Null pointer"

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-eq v0, v2, :cond_1

    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-ne v0, v1, :cond_4

    .line 115
    :cond_1
    :try_start_0
    iget-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    if-eqz v0, :cond_2

    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-ne v0, v1, :cond_2

    .line 116
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 118
    :cond_2
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-ne v0, v2, :cond_3

    .line 123
    const/16 v0, 0x6b

    iput-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    .line 125
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 126
    return-object p0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 128
    :cond_4
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Value out of sequence."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private end(CC)Lweibo4android/org/json/JSONWriter;
    .locals 2

    .prologue
    .line 163
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-eq v0, p1, :cond_1

    .line 164
    new-instance v1, Lweibo4android/org/json/JSONException;

    const/16 v0, 0x6f

    if-ne p1, v0, :cond_0

    const-string v0, "Misplaced endObject."

    :goto_0
    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v0, "Misplaced endArray."

    goto :goto_0

    .line 166
    :cond_1
    invoke-direct {p0, p1}, Lweibo4android/org/json/JSONWriter;->pop(C)V

    .line 168
    :try_start_0
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p2}, Ljava/io/Writer;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 173
    return-object p0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private pop(C)V
    .locals 4

    .prologue
    const/16 v1, 0x6b

    const/16 v0, 0x61

    .line 265
    iget v2, p0, Lweibo4android/org/json/JSONWriter;->top:I

    if-gtz v2, :cond_0

    .line 266
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Nesting error."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    iget-object v2, p0, Lweibo4android/org/json/JSONWriter;->stack:[Lweibo4android/org/json/JSONObject;

    iget v3, p0, Lweibo4android/org/json/JSONWriter;->top:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    if-nez v2, :cond_1

    move v2, v0

    .line 269
    :goto_0
    if-eq v2, p1, :cond_2

    .line 270
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Nesting error."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v2, v1

    .line 268
    goto :goto_0

    .line 272
    :cond_2
    iget v2, p0, Lweibo4android/org/json/JSONWriter;->top:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lweibo4android/org/json/JSONWriter;->top:I

    .line 273
    iget v2, p0, Lweibo4android/org/json/JSONWriter;->top:I

    if-nez v2, :cond_4

    const/16 v0, 0x64

    :cond_3
    :goto_1
    iput-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    .line 274
    return-void

    .line 273
    :cond_4
    iget-object v2, p0, Lweibo4android/org/json/JSONWriter;->stack:[Lweibo4android/org/json/JSONObject;

    iget v3, p0, Lweibo4android/org/json/JSONWriter;->top:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_1
.end method

.method private push(Lweibo4android/org/json/JSONObject;)V
    .locals 2

    .prologue
    .line 285
    iget v0, p0, Lweibo4android/org/json/JSONWriter;->top:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 286
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Nesting too deep."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_0
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->stack:[Lweibo4android/org/json/JSONObject;

    iget v1, p0, Lweibo4android/org/json/JSONWriter;->top:I

    aput-object p1, v0, v1

    .line 289
    if-nez p1, :cond_1

    const/16 v0, 0x61

    :goto_0
    iput-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    .line 290
    iget v0, p0, Lweibo4android/org/json/JSONWriter;->top:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lweibo4android/org/json/JSONWriter;->top:I

    .line 291
    return-void

    .line 289
    :cond_1
    const/16 v0, 0x6b

    goto :goto_0
.end method


# virtual methods
.method public array()Lweibo4android/org/json/JSONWriter;
    .locals 2

    .prologue
    .line 142
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x69

    if-eq v0, v1, :cond_0

    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_0

    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x61

    if-ne v0, v1, :cond_1

    .line 143
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->push(Lweibo4android/org/json/JSONObject;)V

    .line 144
    const-string v0, "["

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 146
    return-object p0

    .line 148
    :cond_1
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Misplaced array."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public endArray()Lweibo4android/org/json/JSONWriter;
    .locals 2

    .prologue
    .line 184
    const/16 v0, 0x61

    const/16 v1, 0x5d

    invoke-direct {p0, v0, v1}, Lweibo4android/org/json/JSONWriter;->end(CC)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0
.end method

.method public endObject()Lweibo4android/org/json/JSONWriter;
    .locals 2

    .prologue
    .line 195
    const/16 v0, 0x6b

    const/16 v1, 0x7d

    invoke-direct {p0, v0, v1}, Lweibo4android/org/json/JSONWriter;->end(CC)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0
.end method

.method public key(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;
    .locals 2

    .prologue
    .line 210
    if-nez p1, :cond_0

    .line 211
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Null key."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x6b

    if-ne v0, v1, :cond_2

    .line 215
    :try_start_0
    iget-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 218
    :cond_1
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->stack:[Lweibo4android/org/json/JSONObject;

    iget v1, p0, Lweibo4android/org/json/JSONWriter;->top:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, Lweibo4android/org/json/JSONObject;->putOnce(Ljava/lang/String;Ljava/lang/Object;)Lweibo4android/org/json/JSONObject;

    .line 219
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lweibo4android/org/json/JSONWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 222
    const/16 v0, 0x6f

    iput-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    return-object p0

    .line 224
    :catch_0
    move-exception v0

    .line 225
    new-instance v1, Lweibo4android/org/json/JSONException;

    invoke-direct {v1, v0}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 228
    :cond_2
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Misplaced key."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public object()Lweibo4android/org/json/JSONWriter;
    .locals 3

    .prologue
    const/16 v2, 0x6f

    .line 243
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    .line 244
    iput-char v2, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    .line 246
    :cond_0
    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    if-eq v0, v2, :cond_1

    iget-char v0, p0, Lweibo4android/org/json/JSONWriter;->mode:C

    const/16 v1, 0x61

    if-ne v0, v1, :cond_2

    .line 247
    :cond_1
    const-string v0, "{"

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    .line 248
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0}, Lweibo4android/org/json/JSONObject;-><init>()V

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->push(Lweibo4android/org/json/JSONObject;)V

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/org/json/JSONWriter;->comma:Z

    .line 250
    return-object p0

    .line 252
    :cond_2
    new-instance v0, Lweibo4android/org/json/JSONException;

    const-string v1, "Misplaced object."

    invoke-direct {v0, v1}, Lweibo4android/org/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public value(D)Lweibo4android/org/json/JSONWriter;
    .locals 1

    .prologue
    .line 315
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p1, p2}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONWriter;->value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0
.end method

.method public value(J)Lweibo4android/org/json/JSONWriter;
    .locals 1

    .prologue
    .line 327
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0
.end method

.method public value(Ljava/lang/Object;)Lweibo4android/org/json/JSONWriter;
    .locals 1

    .prologue
    .line 343
    invoke-static {p1}, Lweibo4android/org/json/JSONObject;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0
.end method

.method public value(Z)Lweibo4android/org/json/JSONWriter;
    .locals 1

    .prologue
    .line 302
    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {p0, v0}, Lweibo4android/org/json/JSONWriter;->append(Ljava/lang/String;)Lweibo4android/org/json/JSONWriter;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method
