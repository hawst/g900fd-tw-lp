.class public Lweibo4android/Count;
.super Ljava/lang/Object;
.source "Count.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x7df5effd39a1e485L


# instance fields
.field private comments:J

.field private dm:J

.field private followers:J

.field private id:J

.field private mentions:J

.field private rt:J


# direct methods
.method public constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->id:J

    .line 39
    const-string v0, "comments"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->comments:J

    .line 40
    const-string v0, "rt"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->rt:J

    .line 41
    const-string v0, "dm"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->dm:J

    .line 42
    const-string v0, "mentions"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->mentions:J

    .line 43
    const-string v0, "followers"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Count;->followers:J

    .line 44
    return-void
.end method

.method static constructCounts(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Count;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 50
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 52
    new-instance v4, Lweibo4android/Count;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/Count;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_1

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 57
    :catch_1
    move-exception v0

    .line 58
    throw v0

    .line 54
    :cond_0
    return-object v3
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 69
    if-nez p1, :cond_1

    move v0, v1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    if-eq p0, p1, :cond_0

    .line 75
    instance-of v2, p1, Lweibo4android/Count;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/Count;

    iget-wide v2, p1, Lweibo4android/Count;->id:J

    iget-wide v4, p0, Lweibo4android/Count;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getComments()J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lweibo4android/Count;->comments:J

    return-wide v0
.end method

.method public getDm()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lweibo4android/Count;->dm:J

    return-wide v0
.end method

.method public getFollowers()J
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lweibo4android/Count;->followers:J

    return-wide v0
.end method

.method public getMentions()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lweibo4android/Count;->mentions:J

    return-wide v0
.end method

.method public getRt()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lweibo4android/Count;->rt:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lweibo4android/Count;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Count{ id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", comments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->comments:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->rt:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->dm:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mentions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->mentions:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", followers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Count;->followers:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
