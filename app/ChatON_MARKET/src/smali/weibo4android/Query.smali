.class public Lweibo4android/Query;
.super Ljava/lang/Object;
.source "Query.java"


# static fields
.field public static final KILOMETERS:Ljava/lang/String; = "km"

.field public static final MILES:Ljava/lang/String; = "mi"


# instance fields
.field private geocode:Ljava/lang/String;

.field private lang:Ljava/lang/String;

.field private page:I

.field private query:Ljava/lang/String;

.field private rpp:I

.field private sinceId:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v2, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    .line 38
    iput-object v2, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    .line 39
    iput v0, p0, Lweibo4android/Query;->rpp:I

    .line 40
    iput v0, p0, Lweibo4android/Query;->page:I

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lweibo4android/Query;->sinceId:J

    .line 42
    iput-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v2, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    .line 38
    iput-object v2, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    .line 39
    iput v0, p0, Lweibo4android/Query;->rpp:I

    .line 40
    iput v0, p0, Lweibo4android/Query;->page:I

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lweibo4android/Query;->sinceId:J

    .line 42
    iput-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private appendParameter(Ljava/lang/String;JLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Lweibo4android/http/PostParameter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p2

    if-gtz v0, :cond_0

    .line 169
    new-instance v0, Lweibo4android/http/PostParameter;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_0
    return-void
.end method

.method private appendParameter(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lweibo4android/http/PostParameter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    if-eqz p2, :cond_0

    .line 163
    new-instance v0, Lweibo4android/http/PostParameter;

    invoke-direct {v0, p1, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method public asPostParameters()[Lweibo4android/http/PostParameter;
    .locals 4

    .prologue
    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    const-string v1, "q"

    iget-object v2, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 152
    const-string v1, "lang"

    iget-object v2, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 153
    const-string v1, "rpp"

    iget v2, p0, Lweibo4android/Query;->rpp:I

    int-to-long v2, v2

    invoke-direct {p0, v1, v2, v3, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;JLjava/util/List;)V

    .line 154
    const-string v1, "page"

    iget v2, p0, Lweibo4android/Query;->page:I

    int-to-long v2, v2

    invoke-direct {p0, v1, v2, v3, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;JLjava/util/List;)V

    .line 155
    const-string v1, "since_id"

    iget-wide v2, p0, Lweibo4android/Query;->sinceId:J

    invoke-direct {p0, v1, v2, v3, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;JLjava/util/List;)V

    .line 156
    const-string v1, "geocode"

    iget-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lweibo4android/Query;->appendParameter(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 157
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    .line 158
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lweibo4android/http/PostParameter;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    if-ne p0, p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 179
    goto :goto_0

    .line 182
    :cond_3
    check-cast p1, Lweibo4android/Query;

    .line 184
    iget v2, p0, Lweibo4android/Query;->page:I

    iget v3, p1, Lweibo4android/Query;->page:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 185
    goto :goto_0

    .line 187
    :cond_4
    iget v2, p0, Lweibo4android/Query;->rpp:I

    iget v3, p1, Lweibo4android/Query;->rpp:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 188
    goto :goto_0

    .line 190
    :cond_5
    iget-wide v2, p0, Lweibo4android/Query;->sinceId:J

    iget-wide v4, p1, Lweibo4android/Query;->sinceId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 191
    goto :goto_0

    .line 193
    :cond_6
    iget-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Query;->geocode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 194
    goto :goto_0

    .line 193
    :cond_8
    iget-object v2, p1, Lweibo4android/Query;->geocode:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 196
    :cond_9
    iget-object v2, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Query;->lang:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 197
    goto :goto_0

    .line 196
    :cond_b
    iget-object v2, p1, Lweibo4android/Query;->lang:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 199
    :cond_c
    iget-object v2, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Query;->query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 200
    goto :goto_0

    .line 199
    :cond_d
    iget-object v2, p1, Lweibo4android/Query;->query:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getGeocode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    return-object v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lweibo4android/Query;->page:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getRpp()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lweibo4android/Query;->rpp:I

    return v0
.end method

.method public getSinceId()J
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lweibo4android/Query;->sinceId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 208
    iget-object v0, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 209
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 210
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/Query;->rpp:I

    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/Query;->page:I

    add-int/2addr v0, v2

    .line 212
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lweibo4android/Query;->sinceId:J

    iget-wide v4, p0, Lweibo4android/Query;->sinceId:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 213
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 214
    return v0

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0

    :cond_2
    move v0, v1

    .line 209
    goto :goto_1
.end method

.method public setGeoCode(DDDLjava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setLang(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setPage(I)V
    .locals 0

    .prologue
    .line 107
    iput p1, p0, Lweibo4android/Query;->page:I

    .line 108
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setRpp(I)V
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lweibo4android/Query;->rpp:I

    .line 93
    return-void
.end method

.method public setSinceId(J)V
    .locals 0

    .prologue
    .line 121
    iput-wide p1, p0, Lweibo4android/Query;->sinceId:J

    .line 122
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Query{query=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Query;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lang=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Query;->lang:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rpp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Query;->rpp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Query;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sinceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Query;->sinceId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", geocode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Query;->geocode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
