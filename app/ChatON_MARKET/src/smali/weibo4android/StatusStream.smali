.class public Lweibo4android/StatusStream;
.super Ljava/lang/Object;
.source "StatusStream.java"


# instance fields
.field private br:Ljava/io/BufferedReader;

.field private is:Ljava/io/InputStream;

.field private response:Lweibo4android/http/Response;

.field private streamAlive:Z


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/StatusStream;->streamAlive:Z

    .line 47
    iput-object p1, p0, Lweibo4android/StatusStream;->is:Ljava/io/InputStream;

    .line 48
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lweibo4android/StatusStream;->br:Ljava/io/BufferedReader;

    .line 49
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p1}, Lweibo4android/http/Response;->asStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/StatusStream;-><init>(Ljava/io/InputStream;)V

    .line 53
    iput-object p1, p0, Lweibo4android/StatusStream;->response:Lweibo4android/http/Response;

    .line 54
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lweibo4android/StatusStream;->is:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 88
    iget-object v0, p0, Lweibo4android/StatusStream;->br:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 89
    iget-object v0, p0, Lweibo4android/StatusStream;->response:Lweibo4android/http/Response;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lweibo4android/StatusStream;->response:Lweibo4android/http/Response;

    invoke-virtual {v0}, Lweibo4android/http/Response;->disconnect()V

    .line 92
    :cond_0
    return-void
.end method

.method public next()Lweibo4android/Status;
    .locals 3

    .prologue
    .line 57
    iget-boolean v0, p0, Lweibo4android/StatusStream;->streamAlive:Z

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Stream already closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :catch_0
    move-exception v0

    .line 62
    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lweibo4android/StatusStream;->streamAlive:Z

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lweibo4android/StatusStream;->br:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-lez v1, :cond_0

    .line 66
    :try_start_1
    new-instance v1, Lweibo4android/Status;

    invoke-direct {v1, v0}, Lweibo4android/Status;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lweibo4android/org/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    .line 74
    :cond_1
    :try_start_2
    new-instance v0, Lweibo4android/WeiboException;

    const-string v1, "Stream closed."

    invoke-direct {v0, v1}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 75
    :catch_1
    move-exception v0

    .line 77
    :try_start_3
    iget-object v1, p0, Lweibo4android/StatusStream;->is:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 80
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lweibo4android/StatusStream;->streamAlive:Z

    .line 81
    new-instance v1, Lweibo4android/WeiboException;

    const-string v2, "Stream closed."

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 78
    :catch_2
    move-exception v1

    goto :goto_0
.end method
