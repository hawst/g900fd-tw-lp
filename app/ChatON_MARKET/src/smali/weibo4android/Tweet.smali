.class public Lweibo4android/Tweet;
.super Lweibo4android/WeiboResponse;
.source "Tweet.java"


# static fields
.field private static final serialVersionUID:J = 0x3babbbc7515d16c3L


# instance fields
.field private createdAt:Ljava/util/Date;

.field private fromUser:Ljava/lang/String;

.field private fromUserId:I

.field private id:J

.field private isoLanguageCode:Ljava/lang/String;

.field private profileImageUrl:Ljava/lang/String;

.field private source:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private toUser:Ljava/lang/String;

.field private toUserId:I


# direct methods
.method constructor <init>(Lweibo4android/org/json/JSONObject;Lweibo4android/WeiboSupport;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lweibo4android/Tweet;->toUserId:I

    .line 41
    iput-object v1, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    .line 54
    :try_start_0
    const-string v0, "text"

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lweibo4android/Tweet;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :try_start_1
    const-string v0, "to_user_id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/Tweet;->toUserId:I

    .line 57
    const-string v0, "to_user"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;
    :try_end_1
    .catch Lweibo4android/org/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 62
    :goto_0
    :try_start_2
    const-string v0, "from_user"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    .line 63
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Tweet;->id:J

    .line 64
    const-string v0, "from_user_id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/Tweet;->fromUserId:I
    :try_end_2
    .catch Lweibo4android/org/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 66
    :try_start_3
    const-string v0, "iso_language_code"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;
    :try_end_3
    .catch Lweibo4android/org/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 70
    :goto_1
    :try_start_4
    const-string v0, "source"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lweibo4android/Tweet;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    .line 71
    const-string v0, "profile_image_url"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lweibo4android/Tweet;->getString(Ljava/lang/String;Lweibo4android/org/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    .line 72
    const-string v0, "created_at"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "E MMM dd hh:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/Tweet;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;
    :try_end_4
    .catch Lweibo4android/org/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 78
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 67
    :catch_1
    move-exception v0

    goto :goto_1

    .line 58
    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    if-ne p0, p1, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 186
    goto :goto_0

    .line 189
    :cond_3
    check-cast p1, Lweibo4android/Tweet;

    .line 191
    iget v2, p0, Lweibo4android/Tweet;->fromUserId:I

    iget v3, p1, Lweibo4android/Tweet;->fromUserId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 192
    goto :goto_0

    .line 194
    :cond_4
    iget-wide v2, p0, Lweibo4android/Tweet;->id:J

    iget-wide v4, p1, Lweibo4android/Tweet;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 195
    goto :goto_0

    .line 197
    :cond_5
    iget v2, p0, Lweibo4android/Tweet;->toUserId:I

    iget v3, p1, Lweibo4android/Tweet;->toUserId:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 198
    goto :goto_0

    .line 200
    :cond_6
    iget-object v2, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    iget-object v3, p1, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 201
    goto :goto_0

    .line 200
    :cond_8
    iget-object v2, p1, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    if-nez v2, :cond_7

    .line 203
    :cond_9
    iget-object v2, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 204
    goto :goto_0

    .line 203
    :cond_b
    iget-object v2, p1, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 206
    :cond_c
    iget-object v2, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 207
    goto :goto_0

    .line 206
    :cond_e
    iget-object v2, p1, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 209
    :cond_f
    iget-object v2, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 210
    goto :goto_0

    .line 209
    :cond_11
    iget-object v2, p1, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 212
    :cond_12
    iget-object v2, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->source:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 213
    goto/16 :goto_0

    .line 212
    :cond_14
    iget-object v2, p1, Lweibo4android/Tweet;->source:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 215
    :cond_15
    iget-object v2, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->text:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 216
    goto/16 :goto_0

    .line 215
    :cond_17
    iget-object v2, p1, Lweibo4android/Tweet;->text:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 218
    :cond_18
    iget-object v2, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 219
    goto/16 :goto_0

    .line 218
    :cond_19
    iget-object v2, p1, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getFromUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    return-object v0
.end method

.method public getFromUserId()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lweibo4android/Tweet;->fromUserId:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lweibo4android/Tweet;->id:J

    return-wide v0
.end method

.method public getIsoLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getToUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    return-object v0
.end method

.method public getToUserId()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lweibo4android/Tweet;->toUserId:I

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 228
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/Tweet;->toUserId:I

    iget v3, p0, Lweibo4android/Tweet;->toUserId:I

    ushr-int/lit8 v3, v3, 0x20

    xor-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 229
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 230
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 231
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lweibo4android/Tweet;->id:J

    iget-wide v4, p0, Lweibo4android/Tweet;->id:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 232
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/Tweet;->fromUserId:I

    iget v3, p0, Lweibo4android/Tweet;->fromUserId:I

    ushr-int/lit8 v3, v3, 0x20

    xor-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 233
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 234
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 235
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 236
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 237
    return v0

    :cond_1
    move v0, v1

    .line 227
    goto :goto_0

    :cond_2
    move v0, v1

    .line 229
    goto :goto_1

    :cond_3
    move v0, v1

    .line 230
    goto :goto_2

    :cond_4
    move v0, v1

    .line 233
    goto :goto_3

    :cond_5
    move v0, v1

    .line 234
    goto :goto_4

    :cond_6
    move v0, v1

    .line 235
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tweet{text=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", toUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Tweet;->toUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", toUser=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->toUser:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fromUser=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->fromUser:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Tweet;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fromUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Tweet;->fromUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isoLanguageCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->isoLanguageCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileImageUrl=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Tweet;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
