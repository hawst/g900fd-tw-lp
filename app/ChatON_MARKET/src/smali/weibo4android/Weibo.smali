.class public Lweibo4android/Weibo;
.super Lweibo4android/WeiboSupport;
.source "Weibo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static CONSUMER_KEY:Ljava/lang/String; = null

.field public static CONSUMER_SECRET:Ljava/lang/String; = null

.field public static final IM:Lweibo4android/Weibo$Device;

.field public static final NONE:Lweibo4android/Weibo$Device;

.field public static final SMS:Lweibo4android/Weibo$Device;

.field private static final serialVersionUID:J = -0x14a09c9e28673704L


# instance fields
.field private baseURL:Ljava/lang/String;

.field private format:Ljava/text/SimpleDateFormat;

.field private searchBaseURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    const-string v0, "1552112022"

    sput-object v0, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    .line 55
    const-string v0, "bd9e3b147ece106f9cee159758a28ec1"

    sput-object v0, Lweibo4android/Weibo;->CONSUMER_SECRET:Ljava/lang/String;

    .line 61
    new-instance v0, Lweibo4android/Weibo$Device;

    const-string v1, "im"

    invoke-direct {v0, v1}, Lweibo4android/Weibo$Device;-><init>(Ljava/lang/String;)V

    sput-object v0, Lweibo4android/Weibo;->IM:Lweibo4android/Weibo$Device;

    .line 62
    new-instance v0, Lweibo4android/Weibo$Device;

    const-string v1, "sms"

    invoke-direct {v0, v1}, Lweibo4android/Weibo$Device;-><init>(Ljava/lang/String;)V

    sput-object v0, Lweibo4android/Weibo;->SMS:Lweibo4android/Weibo$Device;

    .line 63
    new-instance v0, Lweibo4android/Weibo$Device;

    const-string v1, "none"

    invoke-direct {v0, v1}, Lweibo4android/Weibo$Device;-><init>(Ljava/lang/String;)V

    sput-object v0, Lweibo4android/Weibo;->NONE:Lweibo4android/Weibo$Device;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1057
    invoke-direct {p0}, Lweibo4android/WeiboSupport;-><init>()V

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, d MMM yyyy HH:mm:ss z"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    .line 1058
    iget-object v0, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1059
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "api.t.sina.com.cn/oauth/request_token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->setRequestTokenURL(Ljava/lang/String;)V

    .line 1060
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "api.t.sina.com.cn/oauth/authorize"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->setAuthorizationURL(Ljava/lang/String;)V

    .line 1061
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "api.t.sina.com.cn/oauth/access_token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->setAccessTokenURL(Ljava/lang/String;)V

    .line 1062
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1069
    invoke-direct {p0}, Lweibo4android/Weibo;-><init>()V

    .line 1070
    iput-object p1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    .line 1071
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1074
    invoke-direct {p0}, Lweibo4android/Weibo;-><init>()V

    .line 1075
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->setUserId(Ljava/lang/String;)V

    .line 1076
    invoke-virtual {p0, p2}, Lweibo4android/Weibo;->setPassword(Ljava/lang/String;)V

    .line 1077
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1080
    invoke-direct {p0}, Lweibo4android/Weibo;-><init>()V

    .line 1081
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->setUserId(Ljava/lang/String;)V

    .line 1082
    invoke-virtual {p0, p2}, Lweibo4android/Weibo;->setPassword(Ljava/lang/String;)V

    .line 1083
    iput-object p3, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    .line 1084
    return-void
.end method

.method private addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/http/PostParameter;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 920
    if-eqz p3, :cond_0

    .line 921
    new-instance v0, Lweibo4android/http/PostParameter;

    invoke-direct {v0, p2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 923
    :cond_0
    return-void
.end method

.method private generateParameterArray(Ljava/util/Map;)[Lweibo4android/http/PostParameter;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Lweibo4android/http/PostParameter;"
        }
    .end annotation

    .prologue
    .line 1167
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Lweibo4android/http/PostParameter;

    .line 1168
    const/4 v0, 0x0

    .line 1169
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1170
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1171
    new-instance v5, Lweibo4android/http/PostParameter;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v2

    .line 1172
    add-int/lit8 v0, v2, 0x1

    :goto_1
    move v2, v0

    goto :goto_0

    .line 1175
    :cond_0
    return-object v3

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private get(Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 1

    .prologue
    .line 1103
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public static getAppKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    return-object v0
.end method

.method public static getAppSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lweibo4android/Weibo;->CONSUMER_SECRET:Ljava/lang/String;

    return-object v0
.end method

.method private toDateStr(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 313
    if-nez p1, :cond_0

    .line 314
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 316
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 317
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public block(Ljava/lang/String;)Lweibo4android/User;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1599
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "blocks/create/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public create(Ljava/lang/String;)Lweibo4android/User;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1502
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->createFriendship(Ljava/lang/String;)Lweibo4android/User;

    move-result-object v0

    return-object v0
.end method

.method public createBlock(Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 198
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "blocks/create.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "user_id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public createBlockByScreenName(Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 202
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "blocks/create.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "screen_name"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public createFavorite(J)Lweibo4android/Status;
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "favorites/create/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public createFriendship(Ljava/lang/String;)Lweibo4android/User;
    .locals 4

    .prologue
    .line 321
    new-instance v0, Lweibo4android/User;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friendships/create.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p1, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public createFriendshipByScreenName(Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 325
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "friendships/create.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "screen_name"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public createFriendshipByUserid(Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 329
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "friendships/create.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "user_id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public createSavedSearch(Ljava/lang/String;)Lweibo4android/SavedSearch;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 942
    new-instance v0, Lweibo4android/SavedSearch;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "saved_searches/create.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "query"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/SavedSearch;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public createTags(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 177
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "tags/create.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "tags"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2, v6}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public createTags([Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 169
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 170
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 173
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->createTags(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public deleteDirectMessage(I)Lweibo4android/DirectMessage;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1496
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/Weibo;->destroyDirectMessage(J)Lweibo4android/DirectMessage;

    move-result-object v0

    return-object v0
.end method

.method public destoryTag(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 186
    :try_start_0
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "tags/destroy.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lweibo4android/http/PostParameter;

    const/4 v5, 0x0

    new-instance v6, Lweibo4android/http/PostParameter;

    const-string v7, "tag_id"

    invoke-direct {v6, v7, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v2

    const-string v3, "result"

    invoke-virtual {v2, v3}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public destory_batchTags(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 194
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "tags/destroy_batch.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "ids"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2, v6}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyBlock(Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 206
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "blocks/destroy.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public destroyComment(J)Lweibo4android/Comment;
    .locals 4

    .prologue
    .line 729
    new-instance v0, Lweibo4android/Comment;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/comment_destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json?source="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->delete(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Comment;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public destroyComments(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 733
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "statuses/comment/destroy_batch.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "ids"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2, v6}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyComments([Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 738
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 739
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 738
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 741
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 742
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "statuses/comment/destroy_batch.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Lweibo4android/http/PostParameter;

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "ids"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyDirectMessage(J)Lweibo4android/DirectMessage;
    .locals 5

    .prologue
    .line 376
    new-instance v0, Lweibo4android/DirectMessage;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "direct_messages/destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/DirectMessage;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public destroyDirectMessages(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 380
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "direct_messages/destroy_batch.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "ids"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2, v6}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyDirectMessages([Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 385
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 386
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 388
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 389
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "direct_messages/destroy_batch.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Lweibo4android/http/PostParameter;

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "ids"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyFavorite(J)Lweibo4android/Status;
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "favorites/destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public destroyFavorites(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 118
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "favorites/destroy_batch.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "ids"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2, v6}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyFavorites([Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 124
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 127
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "favorites/destroy_batch.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Lweibo4android/http/PostParameter;

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "ids"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroySavedSearch(I)Lweibo4android/SavedSearch;
    .locals 4

    .prologue
    .line 946
    new-instance v0, Lweibo4android/SavedSearch;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "saved_searches/destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/SavedSearch;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public destroyStatus(J)Lweibo4android/Status;
    .locals 1

    .prologue
    .line 703
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->destroyStatus(Ljava/lang/String;)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public destroyStatus(Ljava/lang/String;)Lweibo4android/Status;
    .locals 5

    .prologue
    .line 707
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public disableNotification(Ljava/lang/String;)Lweibo4android/User;
    .locals 4

    .prologue
    .line 930
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "notifications/leave/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public enableNotification(Ljava/lang/String;)Lweibo4android/User;
    .locals 4

    .prologue
    .line 926
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "notifications/follow/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public endSession()Lweibo4android/User;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lweibo4android/User;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account/end_session.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1006
    if-ne p0, p1, :cond_1

    .line 1007
    const/4 v0, 0x1

    .line 1027
    :cond_0
    :goto_0
    return v0

    .line 1009
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 1013
    check-cast p1, Lweibo4android/Weibo;

    .line 1015
    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    iget-object v2, p1, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1018
    iget-object v1, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    iget-object v2, p1, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1021
    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    iget-object v2, p1, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v1, v2}, Lweibo4android/http/HttpClient;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1024
    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    iget-object v2, p1, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    iget-object v0, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    iget-object v1, p1, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public exists(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1515
    invoke-virtual {p0, p1, p2}, Lweibo4android/Weibo;->existsFriendship(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public existsBlock(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 211
    const/4 v2, -0x1

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "blocks/exists.json?user_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v3

    invoke-virtual {v3}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "<error>You are not blocking this user.</error>"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v2, v3, :cond_0

    .line 214
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 211
    goto :goto_0

    .line 212
    :catch_0
    move-exception v0

    .line 213
    invoke-virtual {v0}, Lweibo4android/WeiboException;->getStatusCode()I

    move-result v2

    const/16 v3, 0x194

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 214
    goto :goto_0

    .line 216
    :cond_1
    throw v0
.end method

.method public existsFriendship(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 844
    const/4 v7, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "friendships/exists.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "user_a"

    const-string v4, "user_b"

    move-object v0, p0

    move-object v3, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v7, v0, :cond_0

    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public favorites()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1563
    invoke-virtual {p0}, Lweibo4android/Weibo;->getFavorites()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public favorites(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1569
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFavorites(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public favorites(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1575
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFavorites(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public favorites(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1581
    invoke-virtual {p0, p1, p2}, Lweibo4android/Weibo;->getFavorites(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public follow(Ljava/lang/String;)Lweibo4android/User;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1587
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->enableNotification(Ljava/lang/String;)Lweibo4android/User;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic forceUsePost(Z)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->forceUsePost(Z)V

    return-void
.end method

.method protected get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 3

    .prologue
    .line 1111
    const/4 v0, 0x2

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    const/4 v1, 0x0

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p4, p5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0, p6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method protected get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 3

    .prologue
    .line 1107
    const/4 v0, 0x1

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    const/4 v1, 0x0

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0, p4}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method protected get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;
    .locals 9

    .prologue
    const-wide/16 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1127
    if-eqz p3, :cond_7

    .line 1128
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1129
    invoke-virtual {p3}, Lweibo4android/Paging;->getMaxId()J

    move-result-wide v0

    cmp-long v0, v7, v0

    if-eqz v0, :cond_0

    .line 1130
    new-instance v0, Lweibo4android/http/PostParameter;

    const-string v1, "max_id"

    invoke-virtual {p3}, Lweibo4android/Paging;->getMaxId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1132
    :cond_0
    invoke-virtual {p3}, Lweibo4android/Paging;->getSinceId()J

    move-result-wide v0

    cmp-long v0, v7, v0

    if-eqz v0, :cond_1

    .line 1133
    new-instance v0, Lweibo4android/http/PostParameter;

    const-string v1, "since_id"

    invoke-virtual {p3}, Lweibo4android/Paging;->getSinceId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1135
    :cond_1
    invoke-virtual {p3}, Lweibo4android/Paging;->getPage()I

    move-result v0

    if-eq v5, v0, :cond_2

    .line 1136
    new-instance v0, Lweibo4android/http/PostParameter;

    const-string v1, "page"

    invoke-virtual {p3}, Lweibo4android/Paging;->getPage()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1138
    :cond_2
    invoke-virtual {p3}, Lweibo4android/Paging;->getCount()I

    move-result v0

    if-eq v5, v0, :cond_3

    .line 1139
    const-string v0, "search"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v5, v0, :cond_4

    .line 1140
    new-instance v0, Lweibo4android/http/PostParameter;

    const-string v1, "rpp"

    invoke-virtual {p3}, Lweibo4android/Paging;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1146
    :cond_3
    :goto_0
    const/4 v1, 0x0

    .line 1147
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lweibo4android/http/PostParameter;

    check-cast v0, [Lweibo4android/http/PostParameter;

    .line 1148
    if-eqz p2, :cond_5

    .line 1149
    array-length v1, p2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    .line 1150
    array-length v3, p2

    invoke-static {p2, v6, v1, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1151
    array-length v3, p2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v6, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 1161
    :goto_1
    invoke-virtual {p0, p1, v0, p4}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    .line 1163
    :goto_2
    return-object v0

    .line 1142
    :cond_4
    new-instance v0, Lweibo4android/http/PostParameter;

    const-string v1, "count"

    invoke-virtual {p3}, Lweibo4android/Paging;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1152
    :cond_5
    array-length v2, v0

    if-eqz v2, :cond_8

    .line 1153
    invoke-static {v0}, Lweibo4android/http/HttpClient;->encodeParameters([Lweibo4android/http/PostParameter;)Ljava/lang/String;

    move-result-object v0

    .line 1154
    const-string v2, "?"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v5, v2, :cond_6

    .line 1155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&source="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v0, v1

    goto :goto_1

    .line 1157
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?source="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v0, v1

    goto :goto_1

    .line 1163
    :cond_7
    invoke-virtual {p0, p1, p2, p4}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    goto :goto_2

    :cond_8
    move-object v0, v1

    goto :goto_1
.end method

.method protected get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1115
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1120
    :goto_0
    if-eqz p2, :cond_0

    array-length v1, p2

    if-lez v1, :cond_0

    .line 1121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lweibo4android/http/HttpClient;->encodeParameters([Lweibo4android/http/PostParameter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1123
    :cond_0
    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v1, v0, p3}, Lweibo4android/http/HttpClient;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0

    .line 1117
    :cond_1
    const-string v0, "source"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public getAuthenticatedUser()Lweibo4android/User;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1611
    new-instance v0, Lweibo4android/User;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account/verify_credentials.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getBaseURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091
    iget-object v0, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    return-object v0
.end method

.method public getBlockingUsers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "blocks/blocking.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBlockingUsers(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "blocks/blocking.json?page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBlockingUsersIDs()Lweibo4android/IDs;
    .locals 3

    .prologue
    .line 229
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "blocks/blocking/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public bridge synthetic getClientURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getClientURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getClientVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getClientVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getComments(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getComments(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsByMe()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsByMe(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsTimeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsTimeline(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsToMe()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_to_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsToMe(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/comments_to_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Comment;->constructComments(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCounts(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Count;",
            ">;"
        }
    .end annotation

    .prologue
    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/counts.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ids"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Count;->constructCounts(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDailyTrends()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1623
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/daily.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDailyTrends(Ljava/util/Date;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/daily.json?date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lweibo4android/Weibo;->toDateStr(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, "&exclude=hashtags"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getDirectMessages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMessages(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1466
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getDirectMessages(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMessages(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1460
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {v0, p2}, Lweibo4android/Paging;->sinceId(I)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getDirectMessages(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMessages(Ljava/util/Date;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since"

    iget-object v2, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMessages(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMessagesByPage(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1454
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getDirectMessages(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDowntimeSchedule()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1617
    new-instance v0, Lweibo4android/WeiboException;

    const-string v1, "this method is not supported by the Weibo API anymore"

    new-instance v2, Ljava/lang/NoSuchMethodException;

    const-string v3, "this method is not supported by the Weibo API anymore"

    invoke-direct {v2, v3}, Ljava/lang/NoSuchMethodException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public getFavorites()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "favorites.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFavorites(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "favorites.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "page"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFavorites(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "favorites/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFavorites(Ljava/lang/String;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "favorites/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "page"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFeatured()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/featured.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1229
    invoke-virtual {p0}, Lweibo4android/Weibo;->getFollowersStatuses()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1241
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getFollowersStatuses(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1247
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFollowersStatuses(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1223
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/Weibo;->getFollowersStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1217
    invoke-virtual {p0, p1, p2}, Lweibo4android/Weibo;->getFollowersStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers(Lweibo4android/Paging;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1235
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFollowersStatuses(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersIDSByScreenName(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 257
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "screen_name"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, p2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFollowersIDSByUserId(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 253
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "user_id"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, p2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFollowersIDs()Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 886
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lweibo4android/Weibo;->getFollowersIDs(J)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersIDs(I)Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 898
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/Weibo;->getFollowersIDs(IJ)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersIDs(IJ)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 904
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.xml?user_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFollowersIDs(ILweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1545
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.xml?user_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFollowersIDs(J)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 892
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.json?cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFollowersIDs(Ljava/lang/String;)Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 910
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/Weibo;->getFollowersIDs(Ljava/lang/String;J)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersIDs(Ljava/lang/String;J)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 916
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.json?screen_name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFollowersIDs(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1551
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.xml?screen_name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFollowersIDs(Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1539
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "followers/ids.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p1, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFollowersStatuses()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/followers.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructResult(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersStatuses(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/followers.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/followers.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersStatuses(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 433
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/followers.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1199
    invoke-virtual {p0}, Lweibo4android/Weibo;->getFriendsStatuses()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1193
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getFriendsStatuses(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1211
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFriendsStatuses(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1187
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/Weibo;->getFriendsStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1181
    invoke-virtual {p0, p1, p2}, Lweibo4android/Weibo;->getFriendsStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends(Lweibo4android/Paging;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1205
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getFriendsStatuses(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsIDSByScreenName(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 249
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "screen_name"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, p2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsIDSByUserId(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 245
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "user_id"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, p2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsIDs()Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 850
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lweibo4android/Weibo;->getFriendsIDs(J)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsIDs(I)Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 862
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/Weibo;->getFriendsIDs(IJ)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsIDs(IJ)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 868
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json?user_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsIDs(ILweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1527
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.xml?user_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFriendsIDs(J)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 856
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.xml?cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFriendsIDs(Ljava/lang/String;)Lweibo4android/IDs;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 874
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/Weibo;->getFriendsIDs(Ljava/lang/String;J)Lweibo4android/IDs;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsIDs(Ljava/lang/String;J)Lweibo4android/IDs;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 880
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json?screen_name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsIDs(Ljava/lang/String;Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1533
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.xml?screen_name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFriendsIDs(Lweibo4android/Paging;)Lweibo4android/IDs;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1521
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, p1, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public getFriendsListIDS_test(Ljava/lang/String;I)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1679
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "uid"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "cursor"

    invoke-direct {v3, v4, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsListIDS_test2(Ljava/lang/String;II)Lweibo4android/IDs;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1683
    new-instance v0, Lweibo4android/IDs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "friends/ids.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "uid"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "cursor"

    invoke-direct {v3, v4, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v3, v2, v6

    const/4 v3, 0x2

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "count"

    invoke-direct {v4, v5, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/IDs;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getFriendsStatuses()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructResult(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsStatuses(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 421
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    new-instance v2, Lweibo4android/http/PostParameter;

    const-string v3, "id"

    invoke-direct {v2, v3, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1, v4}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsStatuses(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsStatuses(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsStatuses2(Ljava/lang/String;II)Lweibo4android/UserWapper;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "uid"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    new-instance v2, Lweibo4android/http/PostParameter;

    const-string v3, "cursor"

    invoke-direct {v2, v3, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v2, v1, v5

    const/4 v2, 0x2

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "count"

    invoke-direct {v3, v4, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructWapperUsers(Lweibo4android/http/Response;)Lweibo4android/UserWapper;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1271
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getFriendsTimeline(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline(J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends_timeline.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline(JI)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1277
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p3}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {v0, p1, p2}, Lweibo4android/Paging;->sinceId(J)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getFriendsTimeline(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline(JLjava/lang/String;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1301
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/lang/String;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1295
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/lang/String;J)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1331
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/lang/String;Ljava/util/Date;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1325
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1307
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimeline(Ljava/util/Date;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends_timeline.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since"

    iget-object v2, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimeline(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/friends_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimelineByPage(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1265
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getFriendsTimeline(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriendsTimelineByPage(Ljava/lang/String;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1289
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getHomeTimeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/home_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHomeTimeline(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/home_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHotUsers(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "users/hot.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "category"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getListMembers(Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/UserWapper;
    .locals 4

    .prologue
    .line 959
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 960
    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/members.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    const-string v1, "GET"

    .line 962
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 964
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p3, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/User;->constructWapperUsers(Lweibo4android/http/Response;Lweibo4android/Weibo;)Lweibo4android/UserWapper;

    move-result-object v0

    return-object v0
.end method

.method public getListStatuses(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 950
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 951
    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/lists/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/statuses.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 952
    const-string v1, "GET"

    .line 953
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 955
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p3, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getListSubscribers(Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/UserWapper;
    .locals 4

    .prologue
    .line 968
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 969
    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/subscribers.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 970
    const-string v1, "GET"

    .line 971
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 973
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p3, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/User;->constructWapperUsers(Lweibo4android/http/Response;Lweibo4android/Weibo;)Lweibo4android/UserWapper;

    move-result-object v0

    return-object v0
.end method

.method public getMentions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/mentions.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMentions(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/mentions.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 2

    .prologue
    .line 772
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2}, Lweibo4android/http/HttpClient;->getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;

    move-result-object v0

    .line 773
    invoke-virtual {v0}, Lweibo4android/http/AccessToken;->getScreenName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lweibo4android/Weibo;->setUserId(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 774
    monitor-exit p0

    return-object v0

    .line 772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 1

    .prologue
    .line 778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2, p3}, Lweibo4android/http/HttpClient;->getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOAuthAccessToken(Lweibo4android/http/RequestToken;)Lweibo4android/http/AccessToken;
    .locals 1

    .prologue
    .line 762
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->getOAuthAccessToken(Lweibo4android/http/RequestToken;)Lweibo4android/http/AccessToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOAuthAccessToken(Lweibo4android/http/RequestToken;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 2

    .prologue
    .line 766
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2}, Lweibo4android/http/HttpClient;->getOAuthAccessToken(Lweibo4android/http/RequestToken;Ljava/lang/String;)Lweibo4android/http/AccessToken;

    move-result-object v0

    .line 767
    invoke-virtual {v0}, Lweibo4android/http/AccessToken;->getScreenName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lweibo4android/Weibo;->setUserId(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    monitor-exit p0

    return-object v0

    .line 766
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getOAuthRequestToken()Lweibo4android/http/RequestToken;
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->getOAuthRequestToken()Lweibo4android/http/RequestToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthRequestToken(Ljava/lang/String;)Lweibo4android/http/RequestToken;
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->getOauthRequestToken(Ljava/lang/String;)Lweibo4android/http/RequestToken;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPublicTimeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/public_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPublicTimeline(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1253
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->getPublicTimeline(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPublicTimeline(II)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/public_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    new-instance v2, Lweibo4android/http/PostParameter;

    const-string v3, "count"

    invoke-direct {v2, v3, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "base_app"

    invoke-direct {v3, v4, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPublicTimeline(J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/public_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lweibo4android/Paging;

    invoke-direct {v2, p1, p2}, Lweibo4android/Paging;-><init>(J)V

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRateLimitStatus()Lweibo4android/RateLimitStatus;
    .locals 3

    .prologue
    .line 798
    new-instance v0, Lweibo4android/RateLimitStatus;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account/rate_limit_status.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/RateLimitStatus;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public getReplies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1397
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/replies.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getReplies(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1418
    if-ge p1, v3, :cond_0

    .line 1419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page should be positive integer. passed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/replies.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "page"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getReplies(J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/replies.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getReplies(JI)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 1427
    if-ge p3, v6, :cond_0

    .line 1428
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page should be positive integer. passed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1430
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/replies.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "since_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "page"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRepliesByPage(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1409
    if-ge p1, v3, :cond_0

    .line 1410
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page should be positive integer. passed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1412
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/replies.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "page"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetedByMe()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 802
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweeted_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetedByMe(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 806
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweeted_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetedToMe()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 810
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweeted_to_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetedToMe(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweeted_to_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweets(J)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/RetweetDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 830
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweets/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/RetweetDetails;->createRetweetDetails(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetsOfMe()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 818
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweets_of_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRetweetsOfMe(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/retweets_of_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSavedSearches()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/SavedSearch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 934
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "saved_searches.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/SavedSearch;->constructSavedSearches(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSearchBaseURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1099
    iget-object v0, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSentDirectMessages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages/sent.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSentDirectMessages(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1484
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getSentDirectMessages(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSentDirectMessages(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1490
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1, p2}, Lweibo4android/Paging;-><init>(II)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getSentDirectMessages(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSentDirectMessages(Ljava/util/Date;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1478
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages/sent.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since"

    iget-object v2, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSentDirectMessages(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "direct_messages/sent.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/DirectMessage;->constructDirectMessages(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getSource()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionUsers()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "users/suggestions.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "with_reason"

    const-string v2, "0"

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/User;->constructUsers(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1657
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tags/suggestions.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionsTags()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tags/suggestions.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTags(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tags.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "user_id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrendStatus(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/statuses.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "trend_name"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrendsDaily(Ljava/lang/Integer;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    if-nez p1, :cond_0

    .line 300
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 302
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/daily.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "base_app"

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrendsHourly(Ljava/lang/Integer;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    if-nez p1, :cond_0

    .line 293
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 295
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/hourly.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "base_app"

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrendsWeekly(Ljava/lang/Integer;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    if-nez p1, :cond_0

    .line 307
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 309
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/weekly.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "base_app"

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUnread(Ljava/lang/Integer;Ljava/lang/Long;)Lweibo4android/Count;
    .locals 4

    .prologue
    .line 593
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 594
    if-eqz p1, :cond_0

    .line 595
    const-string v1, "with_new_status"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    :cond_0
    if-eqz p2, :cond_1

    .line 598
    const-string v1, "since_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    :cond_1
    new-instance v1, Lweibo4android/Count;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/unread.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0}, Lweibo4android/Weibo;->generateParameterArray(Ljava/util/Map;)[Lweibo4android/http/PostParameter;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v0, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/Count;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v1
.end method

.method public bridge synthetic getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserDetail(Ljava/lang/String;)Lweibo4android/User;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->showUser(Ljava/lang/String;)Lweibo4android/User;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(IJ)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1367
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2, p3}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {v0, p1}, Lweibo4android/Paging;->count(I)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getUserTimeline(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(ILjava/util/Date;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "since"

    iget-object v0, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "count"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1379
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1, p2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->getUserTimeline(Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/Integer;Ljava/lang/Integer;Lweibo4android/Paging;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lweibo4android/Weibo;->getUserTimeline(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v2}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v3}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;IJ)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJ)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1343
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p3, p4}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {v0, p2}, Lweibo4android/Paging;->count(I)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lweibo4android/Weibo;->getUserTimeline(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;ILjava/util/Date;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "since"

    iget-object v0, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1373
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2, p3}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, p1, v0}, Lweibo4android/Weibo;->getUserTimeline(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 497
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 498
    if-eqz p1, :cond_0

    .line 499
    const-string v1, "id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    :cond_0
    if-eqz p2, :cond_1

    .line 502
    const-string v1, "base_app"

    invoke-virtual {p2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    :cond_1
    if-eqz p3, :cond_2

    .line 505
    const-string v1, "feature"

    invoke-virtual {p3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "statuses/user_timeline.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lweibo4android/Weibo;->generateParameterArray(Ljava/util/Map;)[Lweibo4android/http/PostParameter;

    move-result-object v0

    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v2}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v2

    invoke-virtual {p0, v1, v0, p4, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;Ljava/util/Date;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "since"

    iget-object v2, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v3}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0, p0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v2}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v2

    invoke-virtual {p0, v0, v1, p2, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserTimeline(Lweibo4android/Paging;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/user_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWeeklyTrends()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/weekly.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWeeklyTrends(Ljava/util/Date;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1641
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trends/weekly.json?date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lweibo4android/Weibo;->toDateStr(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, "&exclude=hashtags"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Trends;->constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public declared-synchronized getXAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 1

    .prologue
    .line 786
    monitor-enter p0

    :try_start_0
    const-string v0, "client_auth"

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/Weibo;->getXAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getXAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 1

    .prologue
    .line 782
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2, p3}, Lweibo4android/http/HttpClient;->getXAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getrepostbyme(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/repost_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getrepostbyme(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/repost_by_me.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getreposttimeline(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/repost_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getreposttimeline(Ljava/lang/String;Lweibo4android/Paging;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lweibo4android/Paging;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "statuses/repost_timeline.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Lweibo4android/http/PostParameter;

    const/4 v2, 0x0

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "id"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1, p2, v5}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/Paging;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Status;->constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public gettags(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Tag;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1651
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "tags.json?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "user_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lweibo4android/http/HttpClient;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/Tag;->constructTags(Lweibo4android/http/Response;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->hashCode()I

    move-result v0

    .line 1033
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1034
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1035
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1036
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1037
    return v0
.end method

.method public isListMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 978
    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/members/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 979
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 981
    const-string v1, "GET"

    .line 983
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p4, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    .line 984
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 985
    const-string v1, "true"

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isListSubscriber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 989
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 990
    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/subscribers/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 993
    const-string v1, "GET"

    .line 995
    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p4, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    .line 996
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 997
    const-string v1, "true"

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isUsePostForced()Z
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lweibo4android/WeiboSupport;->isUsePostForced()Z

    move-result v0

    return v0
.end method

.method public leave(Ljava/lang/String;)Lweibo4android/User;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1593
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->disableNotification(Ljava/lang/String;)Lweibo4android/User;

    move-result-object v0

    return-object v0
.end method

.method public rateLimitStatus()Lweibo4android/RateLimitStatus;
    .locals 4

    .prologue
    .line 156
    new-instance v0, Lweibo4android/RateLimitStatus;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account/rate_limit_status.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/RateLimitStatus;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public register(Ljava/lang/String;[Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 1054
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account/register.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v11, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "nick"

    aget-object v6, p2, v8

    invoke-direct {v4, v5, v6}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "gender"

    aget-object v5, p2, v9

    invoke-direct {v3, v4, v5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v7

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "password"

    aget-object v5, p2, v10

    invoke-direct {v3, v4, v5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v8

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "email"

    aget-object v5, p2, v11

    invoke-direct {v3, v4, v5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v9

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "ip"

    invoke-direct {v3, v4, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v10

    invoke-virtual {v0, v1, v2, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public reply(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/Comment;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 746
    new-instance v0, Lweibo4android/Comment;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/reply.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "cid"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v7

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "comment"

    invoke-direct {v5, v6, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Comment;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public repost(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/Status;
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/Weibo;->repost(Ljava/lang/String;Ljava/lang/String;I)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public repost(Ljava/lang/String;Ljava/lang/String;I)Lweibo4android/Status;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 715
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/repost.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "status"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v7

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "is_comment"

    invoke-direct {v5, v6, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public resetCount(I)Ljava/lang/Boolean;
    .locals 7

    .prologue
    .line 604
    .line 605
    const/4 v1, 0x0

    .line 607
    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/reset_count.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "type"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;I)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 608
    const-string v0, "result"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 612
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 609
    :catch_0
    move-exception v0

    .line 610
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method public retweetStatus(J)Lweibo4android/Status;
    .locals 5

    .prologue
    .line 826
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/retweet/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public sendDirectMessage(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/DirectMessage;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 368
    new-instance v0, Lweibo4android/DirectMessage;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "direct_messages/new.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "user_id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "text"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v8

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "source"

    iget-object v7, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v8}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/DirectMessage;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public sendDirectMessageByScreenName(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/DirectMessage;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 372
    new-instance v0, Lweibo4android/DirectMessage;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "direct_messages/new.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "screen_name"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "text"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v8

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "source"

    iget-object v7, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v8}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/DirectMessage;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public setBaseURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1087
    iput-object p1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    .line 1088
    return-void
.end method

.method public bridge synthetic setClientURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setClientURL(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setClientVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setClientVersion(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setHttpConnectionTimeout(I)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setHttpConnectionTimeout(I)V

    return-void
.end method

.method public bridge synthetic setHttpProxy(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setHttpProxy(Ljava/lang/String;I)V

    return-void
.end method

.method public bridge synthetic setHttpProxyAuth(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setHttpProxyAuth(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setHttpReadTimeout(I)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setHttpReadTimeout(I)V

    return-void
.end method

.method public setOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 794
    new-instance v0, Lweibo4android/http/AccessToken;

    invoke-direct {v0, p1, p2}, Lweibo4android/http/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->setOAuthAccessToken(Lweibo4android/http/AccessToken;)V

    .line 795
    return-void
.end method

.method public setOAuthAccessToken(Lweibo4android/http/AccessToken;)V
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setOAuthAccessToken(Lweibo4android/http/AccessToken;)V

    .line 791
    return-void
.end method

.method public declared-synchronized setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 750
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2}, Lweibo4android/http/HttpClient;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    monitor-exit p0

    return-void

    .line 750
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setPassword(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setPassword(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lweibo4android/WeiboSupport;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setRetryCount(I)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setRetryCount(I)V

    return-void
.end method

.method public bridge synthetic setRetryIntervalSecs(I)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setRetryIntervalSecs(I)V

    return-void
.end method

.method public setSearchBaseURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1095
    iput-object p1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    .line 1096
    return-void
.end method

.method public bridge synthetic setSource(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setSource(Ljava/lang/String;)V

    return-void
.end method

.method public setToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2}, Lweibo4android/http/HttpClient;->setToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/RequestToken;

    .line 1066
    return-void
.end method

.method public setToken(Lweibo4android/http/AccessToken;)V
    .locals 2

    .prologue
    .line 1645
    invoke-virtual {p1}, Lweibo4android/http/AccessToken;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lweibo4android/http/AccessToken;->getTokenSecret()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lweibo4android/Weibo;->setToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    return-void
.end method

.method public bridge synthetic setUserAgent(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setUserAgent(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lweibo4android/WeiboSupport;->setUserId(Ljava/lang/String;)V

    return-void
.end method

.method public setupConsumerConfig(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    sput-object p1, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    .line 75
    sput-object p2, Lweibo4android/Weibo;->CONSUMER_SECRET:Ljava/lang/String;

    .line 77
    const-string v0, "weibo4j.oauth.consumerKey"

    invoke-static {v0, p1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 78
    const-string v0, "weibo4j.oauth.consumerSecret"

    invoke-static {v0, p2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 79
    return-void
.end method

.method public show(I)Lweibo4android/Status;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1436
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/Weibo;->showStatus(J)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public show(J)Lweibo4android/Status;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1442
    new-instance v0, Lweibo4android/Status;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "statuses/show/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public showFriendships(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 1046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "friendships/show.json?target_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public showFriendships(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/org/json/JSONObject;
    .locals 2

    .prologue
    .line 1050
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "friendships/show.json?target_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&source_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public showSavedSearch(I)Lweibo4android/SavedSearch;
    .locals 3

    .prologue
    .line 938
    new-instance v0, Lweibo4android/SavedSearch;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "saved_searches/show/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/SavedSearch;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public showStatus(J)Lweibo4android/Status;
    .locals 1

    .prologue
    .line 638
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lweibo4android/Weibo;->showStatus(Ljava/lang/String;)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public showStatus(Ljava/lang/String;)Lweibo4android/Status;
    .locals 3

    .prologue
    .line 634
    new-instance v0, Lweibo4android/Status;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "statuses/show/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public showUser(Ljava/lang/String;)Lweibo4android/User;
    .locals 6

    .prologue
    .line 393
    new-instance v0, Lweibo4android/User;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "users/show.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "user_id"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    iget-object v3, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v3}, Lweibo4android/http/HttpClient;->isAuthenticationEnabled()Z

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/Weibo;->get(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public test()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1001
    const/4 v1, -0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "help/test.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v2

    invoke-virtual {v2}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ok"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 1042
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Weibo{http="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", baseURL=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchBaseURL=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Weibo;->searchBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Weibo;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trendsDestroy(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lweibo4android/Weibo;->baseURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "trends/destroy.json?trend_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lweibo4android/http/HttpClient;->delete(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 285
    :try_start_0
    const-string v1, "result"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 286
    :catch_0
    move-exception v0

    .line 288
    new-instance v0, Lweibo4android/WeiboException;

    const-string v1, "e"

    invoke-direct {v0, v1}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unblock(Ljava/lang/String;)Lweibo4android/User;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1605
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "blocks/destroy/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public update(Ljava/lang/String;)Lweibo4android/Status;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1385
    invoke-virtual {p0, p1}, Lweibo4android/Weibo;->updateStatus(Ljava/lang/String;)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public update(Ljava/lang/String;J)Lweibo4android/Status;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1391
    invoke-virtual {p0, p1, p2, p3}, Lweibo4android/Weibo;->updateStatus(Ljava/lang/String;J)Lweibo4android/Status;

    move-result-object v0

    return-object v0
.end method

.method public updateComment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/Comment;
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 719
    .line 720
    if-nez p3, :cond_0

    .line 721
    new-array v0, v4, [Lweibo4android/http/PostParameter;

    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "comment"

    invoke-direct {v1, v2, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "id"

    invoke-direct {v1, v2, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    .line 725
    :goto_0
    new-instance v1, Lweibo4android/Comment;

    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "statuses/comment.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v5}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/Comment;-><init>(Lweibo4android/http/Response;)V

    return-object v1

    .line 723
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "comment"

    invoke-direct {v1, v2, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "cid"

    invoke-direct {v1, v2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lweibo4android/http/PostParameter;

    const-string v2, "id"

    invoke-direct {v1, v2, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public updateLocation(Ljava/lang/String;)Lweibo4android/User;
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 1557
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account/update_location.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "location"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V

    return-object v0
.end method

.method public updatePrivacy(Ljava/lang/String;)Lweibo4android/User;
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 836
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account/update_privacy.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "comment"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public updateProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/User;
    .locals 5

    .prologue
    .line 135
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 136
    const-string v1, "name"

    invoke-direct {p0, v0, v1, p1}, Lweibo4android/Weibo;->addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v1, "email"

    invoke-direct {p0, v0, v1, p2}, Lweibo4android/Weibo;->addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v1, "url"

    invoke-direct {p0, v0, v1, p3}, Lweibo4android/Weibo;->addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v1, "location"

    invoke-direct {p0, v0, v1, p4}, Lweibo4android/Weibo;->addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v1, "description"

    invoke-direct {p0, v0, v1, p5}, Lweibo4android/Weibo;->addParameterToList(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v1, Lweibo4android/User;

    iget-object v2, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "account/update_profile.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lweibo4android/http/PostParameter;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lweibo4android/http/PostParameter;

    check-cast v0, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v1
.end method

.method public updateRemark(Ljava/lang/Long;Ljava/lang/String;)Lweibo4android/User;
    .locals 2

    .prologue
    .line 461
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lweibo4android/Weibo;->updateRemark(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/User;

    move-result-object v0

    return-object v0
.end method

.method public updateRemark(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/User;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 454
    invoke-static {p2}, Lweibo4android/util/URLEncodeUtils;->isURLEncoded(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    invoke-static {p2}, Lweibo4android/util/URLEncodeUtils;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 457
    :cond_0
    new-instance v0, Lweibo4android/User;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "user/friends/update_remark.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "user_id"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "remark"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method

.method public updateStatus(Ljava/lang/String;)Lweibo4android/Status;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 642
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/update.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "status"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public updateStatus(Ljava/lang/String;DD)Lweibo4android/Status;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 646
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/update.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "status"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "lat"

    invoke-direct {v4, v5, p2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;D)V

    aput-object v4, v3, v7

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "long"

    invoke-direct {v5, v6, p4, p5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;D)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v7}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public updateStatus(Ljava/lang/String;J)Lweibo4android/Status;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 650
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/update.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "status"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "in_reply_to_status_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v8

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "source"

    iget-object v7, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v8}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public updateStatus(Ljava/lang/String;JDD)Lweibo4android/Status;
    .locals 8

    .prologue
    .line 654
    new-instance v0, Lweibo4android/Status;

    iget-object v1, p0, Lweibo4android/Weibo;->http:Lweibo4android/http/HttpClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "statuses/update.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    new-array v3, v3, [Lweibo4android/http/PostParameter;

    const/4 v4, 0x0

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "status"

    invoke-direct {v5, v6, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "lat"

    invoke-direct {v5, v6, p4, p5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;D)V

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "long"

    invoke-direct {v5, v6, p6, p7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;D)V

    aput-object v5, v3, v4

    const/4 v4, 0x3

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "in_reply_to_status_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x4

    new-instance v5, Lweibo4android/http/PostParameter;

    const-string v6, "source"

    iget-object v7, p0, Lweibo4android/Weibo;->source:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lweibo4android/http/HttpClient;->post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;)V

    return-object v0
.end method

.method public verifyCredentials()Lweibo4android/User;
    .locals 3

    .prologue
    .line 131
    new-instance v0, Lweibo4android/User;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lweibo4android/Weibo;->getBaseURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account/verify_credentials.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lweibo4android/Weibo;->get(Ljava/lang/String;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    return-object v0
.end method
