.class public Lweibo4android/Status;
.super Lweibo4android/WeiboResponse;
.source "Status.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1650c3edbf9bb2a0L


# instance fields
.field private bmiddle_pic:Ljava/lang/String;

.field private createdAt:Ljava/util/Date;

.field private id:J

.field private inReplyToScreenName:Ljava/lang/String;

.field private inReplyToStatusId:J

.field private inReplyToUserId:I

.field private isFavorited:Z

.field private isTruncated:Z

.field private latitude:D

.field private longitude:D

.field private original_pic:Ljava/lang/String;

.field private retweetDetails:Lweibo4android/RetweetDetails;

.field private source:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private thumbnail_pic:Ljava/lang/String;

.field private user:Lweibo4android/User;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 128
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 57
    iput-wide v0, p0, Lweibo4android/Status;->latitude:D

    .line 58
    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 129
    new-instance v0, Lweibo4android/org/json/JSONObject;

    invoke-direct {v0, p1}, Lweibo4android/org/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 130
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/Status;->id:J

    .line 131
    const-string v1, "text"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    .line 132
    const-string v1, "source"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    .line 133
    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v1, v2}, Lweibo4android/Status;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    .line 135
    const-string v1, "in_reply_to_status_id"

    invoke-static {v1, v0}, Lweibo4android/Status;->getLong(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)J

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/Status;->inReplyToStatusId:J

    .line 136
    const-string v1, "in_reply_to_user_id"

    invoke-static {v1, v0}, Lweibo4android/Status;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v1

    iput v1, p0, Lweibo4android/Status;->inReplyToUserId:I

    .line 137
    const-string v1, "favorited"

    invoke-static {v1, v0}, Lweibo4android/Status;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v1

    iput-boolean v1, p0, Lweibo4android/Status;->isFavorited:Z

    .line 138
    const-string v1, "thumbnail_pic"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->thumbnail_pic:Ljava/lang/String;

    .line 139
    const-string v1, "bmiddle_pic"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->bmiddle_pic:Ljava/lang/String;

    .line 140
    const-string v1, "original_pic"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lweibo4android/Status;->original_pic:Ljava/lang/String;

    .line 141
    new-instance v1, Lweibo4android/User;

    const-string v2, "user"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v1, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 142
    const-string v1, "retweeted_status"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    new-instance v1, Lweibo4android/RetweetDetails;

    const-string v2, "retweeted_status"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v0

    invoke-direct {v1, v0}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v1, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    .line 145
    :cond_0
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 5

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 77
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 57
    iput-wide v0, p0, Lweibo4android/Status;->latitude:D

    .line 58
    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 78
    invoke-virtual {p1}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v1

    .line 80
    :try_start_0
    const-string v0, "id"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/Status;->id:J

    .line 81
    const-string v0, "text"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    .line 82
    const-string v0, "source"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    .line 83
    const-string v0, "created_at"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v2}, Lweibo4android/Status;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    .line 85
    const-string v0, "in_reply_to_status_id"

    invoke-static {v0, v1}, Lweibo4android/Status;->getLong(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)J

    move-result-wide v2

    iput-wide v2, p0, Lweibo4android/Status;->inReplyToStatusId:J

    .line 86
    const-string v0, "in_reply_to_user_id"

    invoke-static {v0, v1}, Lweibo4android/Status;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lweibo4android/Status;->inReplyToUserId:I

    .line 87
    const-string v0, "favorited"

    invoke-static {v0, v1}, Lweibo4android/Status;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/Status;->isFavorited:Z

    .line 88
    const-string v0, "thumbnail_pic"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->thumbnail_pic:Ljava/lang/String;

    .line 89
    const-string v0, "bmiddle_pic"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->bmiddle_pic:Ljava/lang/String;

    .line 90
    const-string v0, "original_pic"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->original_pic:Ljava/lang/String;

    .line 91
    const-string v0, "user"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lweibo4android/User;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, v2}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 94
    :cond_0
    const-string v0, "inReplyToScreenName"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->inReplyToScreenName:Ljava/lang/String;

    .line 95
    const-string v0, "retweeted_status"

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    new-instance v0, Lweibo4android/RetweetDetails;

    const-string v2, "retweeted_status"

    invoke-virtual {v1, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, v2}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :cond_1
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    new-instance v2, Lweibo4android/WeiboException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method constructor <init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 72
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 57
    iput-wide v0, p0, Lweibo4android/Status;->latitude:D

    .line 58
    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 73
    invoke-direct {p0, p1, p2, p3}, Lweibo4android/Status;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 74
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 66
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 57
    iput-wide v0, p0, Lweibo4android/Status;->latitude:D

    .line 58
    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 67
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 68
    invoke-direct {p0, p1, v0, p2}, Lweibo4android/Status;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 105
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 57
    iput-wide v0, p0, Lweibo4android/Status;->latitude:D

    .line 58
    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 106
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Status;->id:J

    .line 107
    const-string v0, "text"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    .line 108
    const-string v0, "source"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    .line 109
    const-string v0, "created_at"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EEE MMM dd HH:mm:ss z yyyy"

    invoke-static {v0, v1}, Lweibo4android/Status;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    .line 111
    const-string v0, "favorited"

    invoke-static {v0, p1}, Lweibo4android/Status;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/Status;->isFavorited:Z

    .line 112
    const-string v0, "truncated"

    invoke-static {v0, p1}, Lweibo4android/Status;->getBoolean(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/Status;->isTruncated:Z

    .line 114
    const-string v0, "in_reply_to_status_id"

    invoke-static {v0, p1}, Lweibo4android/Status;->getLong(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Status;->inReplyToStatusId:J

    .line 115
    const-string v0, "in_reply_to_user_id"

    invoke-static {v0, p1}, Lweibo4android/Status;->getInt(Ljava/lang/String;Lweibo4android/org/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lweibo4android/Status;->inReplyToUserId:I

    .line 116
    const-string v0, "in_reply_to_screen_name"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->inReplyToScreenName:Ljava/lang/String;

    .line 117
    const-string v0, "thumbnail_pic"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->thumbnail_pic:Ljava/lang/String;

    .line 118
    const-string v0, "bmiddle_pic"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->bmiddle_pic:Ljava/lang/String;

    .line 119
    const-string v0, "original_pic"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->original_pic:Ljava/lang/String;

    .line 120
    new-instance v0, Lweibo4android/User;

    const-string v1, "user"

    invoke-virtual {p1, v1}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 121
    const-string v0, "retweeted_status"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lweibo4android/RetweetDetails;

    const-string v1, "retweeted_status"

    invoke-virtual {p1, v1}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    .line 124
    :cond_0
    return-void
.end method

.method static constructStatuses(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    :try_start_0
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONArray()Lweibo4android/org/json/JSONArray;

    move-result-object v1

    .line 351
    invoke-virtual {v1}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    .line 352
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 354
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 355
    new-instance v4, Lweibo4android/Status;

    invoke-virtual {v1, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/Status;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_1

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :catch_0
    move-exception v0

    .line 359
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 360
    :catch_1
    move-exception v0

    .line 361
    throw v0

    .line 357
    :cond_0
    return-object v3
.end method

.method static constructStatuses(Lweibo4android/http/Response;Lweibo4android/Weibo;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            "Lweibo4android/Weibo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 323
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v4

    .line 324
    invoke-static {v4}, Lweibo4android/Status;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 339
    :goto_0
    return-object v0

    .line 328
    :cond_0
    :try_start_0
    const-string v0, "statuses"

    invoke-static {v0, v4}, Lweibo4android/Status;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 329
    invoke-interface {v4}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 330
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 331
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 332
    :goto_1
    if-ge v2, v6, :cond_1

    .line 333
    invoke-interface {v5, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 334
    new-instance v7, Lweibo4android/Status;

    invoke-direct {v7, p0, v0, p1}, Lweibo4android/Status;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 336
    goto :goto_0

    .line 337
    :catch_0
    move-exception v0

    .line 338
    const-string v0, "nil-classes"

    invoke-static {v0, v4}, Lweibo4android/Status;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 339
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    const-string v0, "status"

    invoke-static {v0, p2}, Lweibo4android/Status;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 149
    new-instance v1, Lweibo4android/User;

    const-string v0, "user"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v1, p1, v0, p3}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    iput-object v1, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    .line 150
    const-string v0, "id"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Status;->id:J

    .line 151
    const-string v0, "text"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    .line 152
    const-string v0, "source"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    .line 153
    const-string v0, "created_at"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildDate(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    .line 154
    const-string v0, "truncated"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/Status;->isTruncated:Z

    .line 155
    const-string v0, "in_reply_to_status_id"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Status;->inReplyToStatusId:J

    .line 156
    const-string v0, "in_reply_to_user_id"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/Status;->inReplyToUserId:I

    .line 157
    const-string v0, "favorited"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildBoolean(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/Status;->isFavorited:Z

    .line 158
    const-string v0, "in_reply_to_screen_name"

    invoke-static {v0, p2}, Lweibo4android/Status;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/Status;->inReplyToScreenName:Ljava/lang/String;

    .line 159
    const-string v0, "georss:point"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-ne v4, v1, :cond_1

    .line 162
    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v1, "null"

    aget-object v2, v0, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lweibo4android/Status;->latitude:D

    .line 166
    :cond_0
    const-string v1, "null"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 167
    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/Status;->longitude:D

    .line 170
    :cond_1
    const-string v0, "retweet_details"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 171
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-ne v4, v1, :cond_2

    .line 172
    new-instance v1, Lweibo4android/RetweetDetails;

    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {v1, p1, v0, p3}, Lweibo4android/RetweetDetails;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    iput-object v1, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    .line 174
    :cond_2
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 373
    if-nez p1, :cond_1

    move v0, v1

    .line 379
    :cond_0
    :goto_0
    return v0

    .line 376
    :cond_1
    if-eq p0, p1, :cond_0

    .line 379
    instance-of v2, p1, Lweibo4android/Status;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/Status;

    iget-wide v2, p1, Lweibo4android/Status;->id:J

    iget-wide v4, p0, Lweibo4android/Status;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getBmiddle_pic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lweibo4android/Status;->bmiddle_pic:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lweibo4android/Status;->id:J

    return-wide v0
.end method

.method public getInReplyToScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lweibo4android/Status;->inReplyToScreenName:Ljava/lang/String;

    return-object v0
.end method

.method public getInReplyToStatusId()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lweibo4android/Status;->inReplyToStatusId:J

    return-wide v0
.end method

.method public getInReplyToUserId()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lweibo4android/Status;->inReplyToUserId:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 261
    iget-wide v0, p0, Lweibo4android/Status;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 270
    iget-wide v0, p0, Lweibo4android/Status;->longitude:D

    return-wide v0
.end method

.method public getOriginal_pic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lweibo4android/Status;->original_pic:Ljava/lang/String;

    return-object v0
.end method

.method public getRetweetDetails()Lweibo4android/RetweetDetails;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnail_pic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lweibo4android/Status;->thumbnail_pic:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Lweibo4android/User;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 368
    iget-wide v0, p0, Lweibo4android/Status;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public isFavorited()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lweibo4android/Status;->isFavorited:Z

    return v0
.end method

.method public isRetweet()Z
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTruncated()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lweibo4android/Status;->isTruncated:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 384
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Status{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Status;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", text=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isTruncated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/Status;->isTruncated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToStatusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Status;->inReplyToStatusId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/Status;->inReplyToUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFavorited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lweibo4android/Status;->isFavorited:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", thumbnail_pic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->thumbnail_pic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bmiddle_pic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->bmiddle_pic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", original_pic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->original_pic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToScreenName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->inReplyToScreenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Status;->latitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/Status;->longitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->retweetDetails:Lweibo4android/RetweetDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Status;->user:Lweibo4android/User;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
