.class Lweibo4android/WeiboSupport;
.super Ljava/lang/Object;
.source "WeiboSupport.java"


# instance fields
.field protected final USE_SSL:Z

.field protected http:Lweibo4android/http/HttpClient;

.field protected source:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, v0, v0}, Lweibo4android/WeiboSupport;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lweibo4android/http/HttpClient;

    invoke-direct {v0}, Lweibo4android/http/HttpClient;-><init>()V

    iput-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    .line 35
    invoke-static {}, Lweibo4android/Configuration;->getSource()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/WeiboSupport;->source:Ljava/lang/String;

    .line 43
    invoke-static {}, Lweibo4android/Configuration;->useSSL()Z

    move-result v0

    iput-boolean v0, p0, Lweibo4android/WeiboSupport;->USE_SSL:Z

    .line 44
    invoke-virtual {p0, v1}, Lweibo4android/WeiboSupport;->setClientVersion(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0, v1}, Lweibo4android/WeiboSupport;->setClientURL(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0, p1}, Lweibo4android/WeiboSupport;->setUserId(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p2}, Lweibo4android/WeiboSupport;->setPassword(Ljava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public forceUsePost(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 254
    return-void
.end method

.method public getClientURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    const-string v1, "X-Weibo-Client-URL"

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->getRequestHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClientVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    const-string v1, "X-Weibo-Client-Version"

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->getRequestHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lweibo4android/WeiboSupport;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isUsePostForced()Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method public setClientURL(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    const-string v0, "X-Weibo-Client-URL"

    invoke-static {p1}, Lweibo4android/Configuration;->getClientURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lweibo4android/WeiboSupport;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public setClientVersion(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "X-Weibo-Client-Version"

    invoke-static {p1}, Lweibo4android/Configuration;->getCilentVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lweibo4android/WeiboSupport;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public setHttpConnectionTimeout(I)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setConnectionTimeout(I)V

    .line 193
    return-void
.end method

.method public setHttpProxy(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setProxyHost(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p2}, Lweibo4android/http/HttpClient;->setProxyPort(I)V

    .line 162
    return-void
.end method

.method public setHttpProxyAuth(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setProxyAuthUser(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p2}, Lweibo4android/http/HttpClient;->setProxyAuthPassword(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public setHttpReadTimeout(I)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setReadTimeout(I)V

    .line 205
    return-void
.end method

.method public declared-synchronized setPassword(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-static {p1}, Lweibo4android/Configuration;->getPassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->setPassword(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    monitor-exit p0

    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1, p2}, Lweibo4android/http/HttpClient;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public setRetryCount(I)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setRetryCount(I)V

    .line 265
    return-void
.end method

.method public setRetryIntervalSecs(I)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setRetryIntervalSecs(I)V

    .line 269
    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 217
    invoke-static {p1}, Lweibo4android/Configuration;->getSource(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/WeiboSupport;->source:Ljava/lang/String;

    .line 218
    const-string v0, "X-Weibo-Client"

    iget-object v1, p0, Lweibo4android/WeiboSupport;->source:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lweibo4android/WeiboSupport;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p1}, Lweibo4android/http/HttpClient;->setUserAgent(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public declared-synchronized setUserId(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lweibo4android/WeiboSupport;->http:Lweibo4android/http/HttpClient;

    invoke-static {p1}, Lweibo4android/Configuration;->getUser(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lweibo4android/http/HttpClient;->setUserId(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
