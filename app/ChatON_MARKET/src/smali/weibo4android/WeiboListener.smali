.class public interface abstract Lweibo4android/WeiboListener;
.super Ljava/lang/Object;
.source "WeiboListener.java"


# virtual methods
.method public abstract blocked(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract created(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract createdBlock(Lweibo4android/User;)V
.end method

.method public abstract createdFavorite(Lweibo4android/Status;)V
.end method

.method public abstract createdFriendship(Lweibo4android/User;)V
.end method

.method public abstract deletedDirectMessage(Lweibo4android/DirectMessage;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract destroyed(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract destroyedBlock(Lweibo4android/User;)V
.end method

.method public abstract destroyedDirectMessage(Lweibo4android/DirectMessage;)V
.end method

.method public abstract destroyedFavorite(Lweibo4android/Status;)V
.end method

.method public abstract destroyedFriendship(Lweibo4android/User;)V
.end method

.method public abstract destroyedStatus(Lweibo4android/Status;)V
.end method

.method public abstract disabledNotification(Lweibo4android/User;)V
.end method

.method public abstract enabledNotification(Lweibo4android/User;)V
.end method

.method public abstract followed(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract gotBlockingUsers(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotBlockingUsersIDs(Lweibo4android/IDs;)V
.end method

.method public abstract gotCurrentTrends(Lweibo4android/Trends;)V
.end method

.method public abstract gotDailyTrends(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotDirectMessages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotDowntimeSchedule(Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract gotExists(Z)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract gotExistsBlock(Z)V
.end method

.method public abstract gotExistsFriendship(Z)V
.end method

.method public abstract gotFavorites(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotFeatured(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotFollowers(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotFollowersIDs(Lweibo4android/IDs;)V
.end method

.method public abstract gotFriends(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/User;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotFriendsIDs(Lweibo4android/IDs;)V
.end method

.method public abstract gotFriendsTimeline(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotHomeTimeline(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotMentions(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotPublicTimeline(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotRateLimitStatus(Lweibo4android/RateLimitStatus;)V
.end method

.method public abstract gotReplies(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract gotRetweetedByMe(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotRetweetedToMe(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotRetweetsOfMe(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotSentDirectMessages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/DirectMessage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotShow(Lweibo4android/Status;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract gotShowStatus(Lweibo4android/Status;)V
.end method

.method public abstract gotTrends(Lweibo4android/Trends;)V
.end method

.method public abstract gotUserDetail(Lweibo4android/User;)V
.end method

.method public abstract gotUserTimeline(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Status;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract gotWeeklyTrends(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract left(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onException(Lweibo4android/WeiboException;I)V
.end method

.method public abstract retweetedStatus(Lweibo4android/Status;)V
.end method

.method public abstract searched(Lweibo4android/QueryResult;)V
.end method

.method public abstract sentDirectMessage(Lweibo4android/DirectMessage;)V
.end method

.method public abstract tested(Z)V
.end method

.method public abstract unblocked(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract updated(Lweibo4android/Status;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract updatedDeliverlyDevice(Lweibo4android/User;)V
.end method

.method public abstract updatedLocation(Lweibo4android/User;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract updatedProfile(Lweibo4android/User;)V
.end method

.method public abstract updatedProfileColors(Lweibo4android/User;)V
.end method

.method public abstract updatedStatus(Lweibo4android/Status;)V
.end method
