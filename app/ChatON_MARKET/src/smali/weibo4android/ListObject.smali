.class public Lweibo4android/ListObject;
.super Lweibo4android/WeiboResponse;
.source "ListObject.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3a66a4e50e177790L


# instance fields
.field private description:Ljava/lang/String;

.field private fullName:Ljava/lang/String;

.field private id:J

.field private memberCount:I

.field private mode:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private slug:Ljava/lang/String;

.field private subscriberCount:I

.field private uri:Ljava/lang/String;

.field private user:Lweibo4android/User;


# direct methods
.method constructor <init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 84
    invoke-direct {p0, p1, p2, p3}, Lweibo4android/ListObject;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 85
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/Weibo;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 79
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lweibo4android/ListObject;->init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    .line 80
    return-void
.end method

.method constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 4

    .prologue
    .line 87
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 89
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/ListObject;->id:J

    .line 90
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->name:Ljava/lang/String;

    .line 91
    const-string v0, "full_name"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->fullName:Ljava/lang/String;

    .line 92
    const-string v0, "slug"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->slug:Ljava/lang/String;

    .line 93
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->description:Ljava/lang/String;

    .line 95
    const-string v0, "subscriber_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListObject;->subscriberCount:I

    .line 96
    const-string v0, "member_count"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListObject;->memberCount:I

    .line 97
    const-string v0, "uri"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->uri:Ljava/lang/String;

    .line 98
    const-string v0, "mode"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->mode:Ljava/lang/String;

    .line 100
    const-string v0, "user"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lweibo4android/User;

    const-string v1, "user"

    invoke-virtual {p1, v1}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/User;-><init>(Lweibo4android/org/json/JSONObject;)V

    iput-object v0, p0, Lweibo4android/ListObject;->user:Lweibo4android/User;
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :cond_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lweibo4android/org/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static constructListObjects(Lweibo4android/http/Response;)Lweibo4android/ListObjectWapper;
    .locals 9

    .prologue
    .line 271
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v6

    .line 273
    :try_start_0
    const-string v0, "lists"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v2

    .line 274
    invoke-virtual {v2}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v3

    .line 275
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 276
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 277
    new-instance v4, Lweibo4android/ListObject;

    invoke-virtual {v2, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lweibo4android/ListObject;-><init>(Lweibo4android/org/json/JSONObject;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_0
    const-string v0, "previous_curosr"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 280
    const-string v0, "next_cursor"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 281
    const-wide/16 v7, -0x1

    cmp-long v0, v4, v7

    if-nez v0, :cond_1

    .line 282
    const-string v0, "nextCursor"

    invoke-virtual {v6, v0}, Lweibo4android/org/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 284
    :cond_1
    new-instance v0, Lweibo4android/ListObjectWapper;

    invoke-direct/range {v0 .. v5}, Lweibo4android/ListObjectWapper;-><init>(Ljava/util/List;JJ)V
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    new-instance v1, Lweibo4android/WeiboException;

    invoke-direct {v1, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method static constructListObjects(Lweibo4android/http/Response;Lweibo4android/Weibo;)Lweibo4android/ListObjectWapper;
    .locals 12

    .prologue
    const/4 v10, 0x0

    const-wide/16 v2, 0x0

    .line 224
    invoke-virtual {p0}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v11

    .line 225
    invoke-static {v11}, Lweibo4android/ListObject;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, Lweibo4android/ListObjectWapper;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lweibo4android/ListObjectWapper;-><init>(Ljava/util/List;JJ)V

    .line 254
    :goto_0
    return-object v0

    .line 229
    :cond_0
    :try_start_0
    const-string v0, "lists_list"

    invoke-static {v0, v11}, Lweibo4android/ListObject;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Document;)V

    .line 230
    invoke-interface {v11}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 231
    const-string v0, "lists"

    invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 232
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 233
    if-nez v1, :cond_1

    .line 234
    new-instance v4, Lweibo4android/ListObjectWapper;

    new-instance v5, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v4 .. v9}, Lweibo4android/ListObjectWapper;-><init>(Ljava/util/List;JJ)V

    move-object v0, v4

    goto :goto_0

    .line 237
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 238
    const-string v1, "list"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 239
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    .line 240
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v10

    .line 241
    :goto_1
    if-ge v1, v7, :cond_2

    .line 242
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 243
    new-instance v8, Lweibo4android/ListObject;

    invoke-direct {v8, p0, v0, p1}, Lweibo4android/ListObject;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 246
    :cond_2
    const-string v0, "previous_curosr"

    invoke-static {v0, v4}, Lweibo4android/ListObject;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v6

    .line 247
    const-string v0, "next_curosr"

    invoke-static {v0, v4}, Lweibo4android/ListObject;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v8

    .line 248
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_3

    .line 249
    const-string v0, "nextCurosr"

    invoke-static {v0, v4}, Lweibo4android/ListObject;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v8

    .line 251
    :cond_3
    new-instance v4, Lweibo4android/ListObjectWapper;

    invoke-direct/range {v4 .. v9}, Lweibo4android/ListObjectWapper;-><init>(Ljava/util/List;JJ)V
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    invoke-static {v11}, Lweibo4android/ListObject;->isRootNodeNilClasses(Lorg/w3c/dom/Document;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 254
    new-instance v0, Lweibo4android/ListObjectWapper;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lweibo4android/ListObjectWapper;-><init>(Ljava/util/List;JJ)V

    goto/16 :goto_0

    .line 256
    :cond_4
    throw v0
.end method

.method private init(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V
    .locals 2

    .prologue
    .line 110
    const-string v0, "list"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 111
    const-string v0, "id"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildLong(Ljava/lang/String;Lorg/w3c/dom/Element;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/ListObject;->id:J

    .line 112
    const-string v0, "name"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->name:Ljava/lang/String;

    .line 113
    const-string v0, "full_name"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->fullName:Ljava/lang/String;

    .line 114
    const-string v0, "slug"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->slug:Ljava/lang/String;

    .line 115
    const-string v0, "description"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->description:Ljava/lang/String;

    .line 117
    const-string v0, "subscriber_count"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListObject;->subscriberCount:I

    .line 118
    const-string v0, "member_count"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListObject;->memberCount:I

    .line 119
    const-string v0, "uri"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->uri:Ljava/lang/String;

    .line 120
    const-string v0, "mode"

    invoke-static {v0, p2}, Lweibo4android/ListObject;->getChildText(Ljava/lang/String;Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/ListObject;->mode:Ljava/lang/String;

    .line 122
    const-string v0, "user"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 125
    new-instance v1, Lweibo4android/User;

    invoke-direct {v1, p1, v0, p3}, Lweibo4android/User;-><init>(Lweibo4android/http/Response;Lorg/w3c/dom/Element;Lweibo4android/Weibo;)V

    iput-object v1, p0, Lweibo4android/ListObject;->user:Lweibo4android/User;

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 297
    if-nez p1, :cond_1

    move v0, v1

    .line 303
    :cond_0
    :goto_0
    return v0

    .line 300
    :cond_1
    if-eq p0, p1, :cond_0

    .line 303
    instance-of v2, p1, Lweibo4android/ListObject;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/ListObject;

    iget-wide v2, p1, Lweibo4android/ListObject;->id:J

    iget-wide v4, p0, Lweibo4android/ListObject;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lweibo4android/ListObject;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lweibo4android/ListObject;->fullName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 192
    iget-wide v0, p0, Lweibo4android/ListObject;->id:J

    return-wide v0
.end method

.method public getMemberCount()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lweibo4android/ListObject;->memberCount:I

    return v0
.end method

.method public getMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lweibo4android/ListObject;->mode:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lweibo4android/ListObject;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSlug()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lweibo4android/ListObject;->slug:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriberCount()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lweibo4android/ListObject;->subscriberCount:I

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lweibo4android/ListObject;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Lweibo4android/User;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lweibo4android/ListObject;->user:Lweibo4android/User;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 292
    iget-wide v0, p0, Lweibo4android/ListObject;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lweibo4android/ListObject;->description:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public setFullName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lweibo4android/ListObject;->fullName:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setId(J)V
    .locals 0

    .prologue
    .line 188
    iput-wide p1, p0, Lweibo4android/ListObject;->id:J

    .line 189
    return-void
.end method

.method public setMemberCount(I)V
    .locals 0

    .prologue
    .line 184
    iput p1, p0, Lweibo4android/ListObject;->memberCount:I

    .line 185
    return-void
.end method

.method public setMode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lweibo4android/ListObject;->mode:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lweibo4android/ListObject;->name:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setSlug(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lweibo4android/ListObject;->slug:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public setSubscriberCount(I)V
    .locals 0

    .prologue
    .line 176
    iput p1, p0, Lweibo4android/ListObject;->subscriberCount:I

    .line 177
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lweibo4android/ListObject;->uri:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setUser(Lweibo4android/User;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lweibo4android/ListObject;->user:Lweibo4android/User;

    .line 209
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ListObject{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lweibo4android/ListObject;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fullName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", slug=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->slug:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscriberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/ListObject;->subscriberCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/ListObject;->memberCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->mode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', uri=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->uri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", user=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/ListObject;->user:Lweibo4android/User;

    invoke-virtual {v1}, Lweibo4android/User;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
