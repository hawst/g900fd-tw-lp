.class public Lweibo4android/util/BareBonesBrowserLaunch;
.super Ljava/lang/Object;
.source "BareBonesBrowserLaunch.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static browse(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 33
    const-string v1, "os.name"

    const-string v3, ""

    invoke-static {v1, v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 34
    const-string v3, "Mac OS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    const-string v1, "com.apple.eio.FileManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 36
    const-string v3, "openURL"

    new-array v4, v7, [Ljava/lang/Class;

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 37
    new-array v3, v7, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :goto_0
    return-void

    .line 38
    :cond_0
    const-string v3, "Windows"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rundll32 url.dll,FileProtocolHandler "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    goto :goto_0

    .line 41
    :cond_1
    const/4 v1, 0x6

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "firefox"

    aput-object v1, v3, v2

    const-string v1, "opera"

    aput-object v1, v3, v7

    const-string v1, "konqueror"

    aput-object v1, v3, v8

    const/4 v1, 0x3

    const-string v4, "epiphany"

    aput-object v4, v3, v1

    const/4 v1, 0x4

    const-string v4, "mozilla"

    aput-object v4, v3, v1

    const/4 v1, 0x5

    const-string v4, "netscape"

    aput-object v4, v3, v1

    move v1, v2

    .line 43
    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_3

    if-nez v0, :cond_3

    .line 44
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "which"

    aput-object v6, v5, v2

    aget-object v6, v3, v1

    aput-object v6, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Process;->waitFor()I

    move-result v4

    if-nez v4, :cond_2

    .line 45
    aget-object v0, v3, v1

    .line 43
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 48
    :cond_3
    if-nez v0, :cond_4

    .line 49
    new-instance v0, Ljava/lang/NoSuchMethodException;

    const-string v1, "Could not find web browser"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchMethodException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v2

    aput-object p0, v3, v7

    invoke-virtual {v1, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    goto :goto_0
.end method

.method public static openURL(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    :try_start_0
    invoke-static {p0}, Lweibo4android/util/BareBonesBrowserLaunch;->browse(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-void

    .line 27
    :catch_0
    move-exception v0

    goto :goto_0
.end method
