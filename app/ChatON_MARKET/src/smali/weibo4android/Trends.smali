.class public Lweibo4android/Trends;
.super Lweibo4android/WeiboResponse;
.source "Trends.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lweibo4android/WeiboResponse;",
        "Ljava/lang/Comparable",
        "<",
        "Lweibo4android/Trends;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x633f2864236532b5L


# instance fields
.field private asOf:Ljava/util/Date;

.field private trendAt:Ljava/util/Date;

.field private trends:[Lweibo4android/Trend;


# direct methods
.method constructor <init>(Lweibo4android/http/Response;Ljava/util/Date;Ljava/util/Date;[Lweibo4android/Trend;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lweibo4android/WeiboResponse;-><init>(Lweibo4android/http/Response;)V

    .line 59
    iput-object p2, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    .line 60
    iput-object p3, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    .line 61
    iput-object p4, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    .line 62
    return-void
.end method

.method static constructTrends(Lweibo4android/http/Response;)Lweibo4android/Trends;
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 99
    :try_start_0
    const-string v1, "as_of"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/Trends;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 100
    const-string v2, "trends"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lweibo4android/Trends;->jsonArrayToTrendArray(Lweibo4android/org/json/JSONArray;)[Lweibo4android/Trend;

    move-result-object v0

    .line 102
    new-instance v2, Lweibo4android/Trends;

    invoke-direct {v2, p0, v1, v1, v0}, Lweibo4android/Trends;-><init>(Lweibo4android/http/Response;Ljava/util/Date;Ljava/util/Date;[Lweibo4android/Trend;)V
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 103
    :catch_0
    move-exception v0

    .line 104
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static constructTrendsList(Lweibo4android/http/Response;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lweibo4android/http/Response;",
            ")",
            "Ljava/util/List",
            "<",
            "Lweibo4android/Trends;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0}, Lweibo4android/http/Response;->asJSONObject()Lweibo4android/org/json/JSONObject;

    move-result-object v0

    .line 69
    :try_start_0
    const-string v1, "as_of"

    invoke-virtual {v0, v1}, Lweibo4android/org/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/Trends;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 70
    const-string v2, "trends"

    invoke-virtual {v0, v2}, Lweibo4android/org/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 71
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lweibo4android/org/json/JSONObject;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 72
    invoke-virtual {v2}, Lweibo4android/org/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 73
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    invoke-virtual {v2, v0}, Lweibo4android/org/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lweibo4android/org/json/JSONArray;

    move-result-object v5

    .line 76
    invoke-static {v5}, Lweibo4android/Trends;->jsonArrayToTrendArray(Lweibo4android/org/json/JSONArray;)[Lweibo4android/Trend;

    move-result-object v5

    .line 77
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x13

    if-ne v6, v7, :cond_1

    .line 79
    new-instance v6, Lweibo4android/Trends;

    const-string v7, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v0, v7}, Lweibo4android/Trends;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {v6, p0, v1, v0, v5}, Lweibo4android/Trends;-><init>(Lweibo4android/http/Response;Ljava/util/Date;Ljava/util/Date;[Lweibo4android/Trend;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/org/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lweibo4android/org/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 80
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x10

    if-ne v6, v7, :cond_2

    .line 82
    new-instance v6, Lweibo4android/Trends;

    const-string v7, "yyyy-MM-dd HH:mm"

    invoke-static {v0, v7}, Lweibo4android/Trends;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {v6, p0, v1, v0, v5}, Lweibo4android/Trends;-><init>(Lweibo4android/http/Response;Ljava/util/Date;Ljava/util/Date;[Lweibo4android/Trend;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xa

    if-ne v6, v7, :cond_0

    .line 85
    new-instance v6, Lweibo4android/Trends;

    const-string v7, "yyyy-MM-dd"

    invoke-static {v0, v7}, Lweibo4android/Trends;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {v6, p0, v1, v0, v5}, Lweibo4android/Trends;-><init>(Lweibo4android/http/Response;Ljava/util/Date;Ljava/util/Date;[Lweibo4android/Trend;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    :cond_3
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_1
    .catch Lweibo4android/org/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 89
    return-object v3
.end method

.method private static jsonArrayToTrendArray(Lweibo4android/org/json/JSONArray;)[Lweibo4android/Trend;
    .locals 4

    .prologue
    .line 119
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v0

    new-array v1, v0, [Lweibo4android/Trend;

    .line 120
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lweibo4android/org/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 121
    invoke-virtual {p0, v0}, Lweibo4android/org/json/JSONArray;->getJSONObject(I)Lweibo4android/org/json/JSONObject;

    move-result-object v2

    .line 122
    new-instance v3, Lweibo4android/Trend;

    invoke-direct {v3, v2}, Lweibo4android/Trend;-><init>(Lweibo4android/org/json/JSONObject;)V

    aput-object v3, v1, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    return-object v1
.end method

.method private static parseDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 5

    .prologue
    .line 110
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 111
    new-instance v0, Ljava/util/Date;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 115
    :goto_0
    return-object v0

    .line 113
    :cond_0
    const-string v0, "EEE, d MMM yyyy HH:mm:ss z"

    invoke-static {p0, v0}, Lweibo4android/WeiboResponse;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lweibo4android/Trends;

    invoke-virtual {p0, p1}, Lweibo4android/Trends;->compareTo(Lweibo4android/Trends;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lweibo4android/Trends;)I
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    iget-object v1, p1, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 141
    if-ne p0, p1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v0

    .line 144
    :cond_1
    instance-of v2, p1, Lweibo4android/Trends;

    if-nez v2, :cond_2

    move v0, v1

    .line 145
    goto :goto_0

    .line 148
    :cond_2
    check-cast p1, Lweibo4android/Trends;

    .line 150
    iget-object v2, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    iget-object v3, p1, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v0, v1

    .line 151
    goto :goto_0

    .line 150
    :cond_4
    iget-object v2, p1, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    if-nez v2, :cond_3

    .line 153
    :cond_5
    iget-object v2, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    iget-object v3, p1, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    .line 154
    goto :goto_0

    .line 153
    :cond_7
    iget-object v2, p1, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    if-nez v2, :cond_6

    .line 156
    :cond_8
    iget-object v2, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    iget-object v3, p1, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 157
    goto :goto_0
.end method

.method public getAsOf()Ljava/util/Date;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    return-object v0
.end method

.method public getTrendAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    return-object v0
.end method

.method public getTrends()[Lweibo4android/Trend;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    .line 166
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 168
    return v0

    :cond_1
    move v0, v1

    .line 165
    goto :goto_0

    :cond_2
    move v0, v1

    .line 166
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trends{asOf="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Trends;->asOf:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", trendAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/Trends;->trendAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", trends="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lweibo4android/Trends;->trends:[Lweibo4android/Trend;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
