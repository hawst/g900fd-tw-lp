.class public Lweibo4android/http/PostParameter;
.super Ljava/lang/Object;
.source "PostParameter.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# static fields
.field private static final GIF:Ljava/lang/String; = "image/gif"

.field private static final JPEG:Ljava/lang/String; = "image/jpeg"

.field private static final OCTET:Ljava/lang/String; = "application/octet-stream"

.field private static final PNG:Ljava/lang/String; = "image/png"

.field private static final serialVersionUID:J = -0x78d96ac49142488cL


# instance fields
.field private file:Ljava/io/File;

.field name:Ljava/lang/String;

.field value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;D)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    .line 50
    iput-object p1, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    .line 51
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    .line 55
    iput-object p1, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    .line 56
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    .line 60
    iput-object p1, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    .line 45
    iput-object p1, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    .line 47
    return-void
.end method

.method static containsFile(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lweibo4android/http/PostParameter;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 138
    const/4 v1, 0x0

    .line 139
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/http/PostParameter;

    .line 140
    invoke-virtual {v0}, Lweibo4android/http/PostParameter;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static containsFile([Lweibo4android/http/PostParameter;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 124
    .line 125
    if-nez p0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 128
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    .line 129
    invoke-virtual {v3}, Lweibo4android/http/PostParameter;->isFile()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 130
    const/4 v0, 0x1

    .line 131
    goto :goto_0

    .line 128
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static encodeParameters([Lweibo4android/http/PostParameter;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 209
    if-nez p0, :cond_0

    .line 210
    const-string v0, ""

    .line 225
    :goto_0
    return-object v0

    .line 212
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 213
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 214
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lweibo4android/http/PostParameter;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parameter ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, p0, v0

    iget-object v0, v0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]should be text"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :cond_1
    if-eqz v0, :cond_2

    .line 218
    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    :cond_2
    :try_start_0
    aget-object v2, p0, v0

    iget-object v2, v2, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, p0, v0

    iget-object v3, v3, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 225
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method public static getParameterArray(Ljava/lang/String;I)[Lweibo4android/http/PostParameter;
    .locals 1

    .prologue
    .line 153
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lweibo4android/http/PostParameter;->getParameterArray(Ljava/lang/String;Ljava/lang/String;)[Lweibo4android/http/PostParameter;

    move-result-object v0

    return-object v0
.end method

.method public static getParameterArray(Ljava/lang/String;ILjava/lang/String;I)[Lweibo4android/http/PostParameter;
    .locals 2

    .prologue
    .line 161
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, p2, v1}, Lweibo4android/http/PostParameter;->getParameterArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lweibo4android/http/PostParameter;

    move-result-object v0

    return-object v0
.end method

.method public static getParameterArray(Ljava/lang/String;Ljava/lang/String;)[Lweibo4android/http/PostParameter;
    .locals 3

    .prologue
    .line 149
    const/4 v0, 0x1

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    const/4 v1, 0x0

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p0, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static getParameterArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Lweibo4android/http/PostParameter;
    .locals 3

    .prologue
    .line 157
    const/4 v0, 0x2

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    const/4 v1, 0x0

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p0, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lweibo4android/http/PostParameter;

    invoke-direct {v2, p2, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 200
    check-cast p1, Lweibo4android/http/PostParameter;

    .line 201
    iget-object v0, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    iget-object v1, p1, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 202
    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    iget-object v1, p1, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 205
    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 174
    if-nez p1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v1

    .line 177
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v0

    .line 178
    goto :goto_0

    .line 180
    :cond_2
    instance-of v2, p1, Lweibo4android/http/PostParameter;

    if-eqz v2, :cond_0

    .line 181
    check-cast p1, Lweibo4android/http/PostParameter;

    .line 183
    iget-object v2, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    iget-object v3, p1, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    :cond_3
    iget-object v2, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    move v1, v0

    goto :goto_0

    .line 183
    :cond_4
    iget-object v2, p1, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_5
    move v0, v1

    .line 187
    goto :goto_1
.end method

.method public getContentType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 89
    invoke-virtual {p0}, Lweibo4android/http/PostParameter;->isFile()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not a file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    iget-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 95
    const/4 v2, -0x1

    if-ne v2, v1, :cond_1

    .line 97
    const-string v0, "application/octet-stream"

    .line 120
    :goto_0
    return-object v0

    .line 99
    :cond_1
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 101
    const-string v1, "gif"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    const-string v0, "image/gif"

    goto :goto_0

    .line 103
    :cond_2
    const-string v1, "png"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 104
    const-string v0, "image/png"

    goto :goto_0

    .line 105
    :cond_3
    const-string v1, "jpg"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 106
    const-string v0, "image/jpeg"

    goto :goto_0

    .line 108
    :cond_4
    const-string v0, "application/octet-stream"

    goto :goto_0

    .line 110
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_7

    .line 111
    const-string v1, "jpeg"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    const-string v0, "image/jpeg"

    goto :goto_0

    .line 114
    :cond_6
    const-string v0, "application/octet-stream"

    goto :goto_0

    .line 117
    :cond_7
    const-string v0, "application/octet-stream"

    goto :goto_0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 169
    return v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFile()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostParameter{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/http/PostParameter;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
