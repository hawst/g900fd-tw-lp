.class public Lweibo4android/http/ImageItem;
.super Ljava/lang/Object;
.source "ImageItem.java"


# instance fields
.field private ImageType:Ljava/lang/String;

.field private content:[B

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p2}, Lweibo4android/http/ImageItem;->getImageType([B)Ljava/lang/String;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    const-string v1, "image/gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "image/png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 32
    :cond_0
    iput-object p2, p0, Lweibo4android/http/ImageItem;->content:[B

    .line 33
    iput-object p1, p0, Lweibo4android/http/ImageItem;->name:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lweibo4android/http/ImageItem;->ImageType:Ljava/lang/String;

    .line 38
    return-void

    .line 36
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported image type, Only Suport JPG ,GIF,PNG!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getImageType(Ljava/io/File;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 53
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 58
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :try_start_1
    invoke-static {v1}, Lweibo4android/http/ImageItem;->getImageType(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 65
    if-eqz v1, :cond_0

    .line 66
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v1

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    .line 65
    :goto_1
    if-eqz v1, :cond_2

    .line 66
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 64
    :cond_2
    :goto_2
    throw v0

    .line 61
    :catch_1
    move-exception v1

    move-object v1, v0

    .line 65
    :goto_3
    if-eqz v1, :cond_0

    .line 66
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 68
    :catch_2
    move-exception v1

    goto :goto_2

    .line 64
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 61
    :catch_3
    move-exception v2

    goto :goto_3
.end method

.method public static getImageType(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 81
    if-nez p0, :cond_0

    .line 89
    :goto_0
    return-object v0

    .line 85
    :cond_0
    const/16 v1, 0x8

    :try_start_0
    new-array v1, v1, [B

    .line 86
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    .line 87
    invoke-static {v1}, Lweibo4android/http/ImageItem;->getImageType([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static getImageType([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    invoke-static {p0}, Lweibo4android/http/ImageItem;->isJPEG([B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "image/jpeg"

    .line 113
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-static {p0}, Lweibo4android/http/ImageItem;->isGIF([B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const-string v0, "image/gif"

    goto :goto_0

    .line 107
    :cond_1
    invoke-static {p0}, Lweibo4android/http/ImageItem;->isPNG([B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    const-string v0, "image/png"

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {p0}, Lweibo4android/http/ImageItem;->isBMP([B)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    const-string v0, "application/x-bmp"

    goto :goto_0

    .line 113
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isBMP([B)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 138
    array-length v2, p0

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 141
    :goto_0
    return v1

    :cond_0
    aget-byte v2, p0, v1

    const/16 v3, 0x42

    if-ne v2, v3, :cond_1

    aget-byte v2, p0, v0

    const/16 v3, 0x4d

    if-ne v2, v3, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static isGIF([B)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 124
    array-length v2, p0

    const/4 v3, 0x6

    if-ge v2, v3, :cond_0

    .line 127
    :goto_0
    return v1

    :cond_0
    aget-byte v2, p0, v1

    const/16 v3, 0x47

    if-ne v2, v3, :cond_2

    aget-byte v2, p0, v0

    const/16 v3, 0x49

    if-ne v2, v3, :cond_2

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/16 v3, 0x46

    if-ne v2, v3, :cond_2

    const/4 v2, 0x3

    aget-byte v2, p0, v2

    const/16 v3, 0x38

    if-ne v2, v3, :cond_2

    aget-byte v2, p0, v4

    const/16 v3, 0x37

    if-eq v2, v3, :cond_1

    aget-byte v2, p0, v4

    const/16 v3, 0x39

    if-ne v2, v3, :cond_2

    :cond_1
    const/4 v2, 0x5

    aget-byte v2, p0, v2

    const/16 v3, 0x61

    if-ne v2, v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private static isJPEG([B)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    array-length v2, p0

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 120
    :goto_0
    return v1

    :cond_0
    aget-byte v2, p0, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    aget-byte v2, p0, v0

    const/16 v3, -0x28

    if-ne v2, v3, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static isPNG([B)Z
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    array-length v2, p0

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 134
    :goto_0
    return v1

    :cond_0
    aget-byte v2, p0, v1

    const/16 v3, -0x77

    if-ne v2, v3, :cond_1

    aget-byte v2, p0, v0

    const/16 v3, 0x50

    if-ne v2, v3, :cond_1

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/16 v3, 0x4e

    if-ne v2, v3, :cond_1

    const/4 v2, 0x3

    aget-byte v2, p0, v2

    const/16 v3, 0x47

    if-ne v2, v3, :cond_1

    const/4 v2, 0x4

    aget-byte v2, p0, v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_1

    const/4 v2, 0x5

    aget-byte v2, p0, v2

    if-ne v2, v4, :cond_1

    const/4 v2, 0x6

    aget-byte v2, p0, v2

    const/16 v3, 0x1a

    if-ne v2, v3, :cond_1

    const/4 v2, 0x7

    aget-byte v2, p0, v2

    if-ne v2, v4, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public getContent()[B
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lweibo4android/http/ImageItem;->content:[B

    return-object v0
.end method

.method public getImageType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lweibo4android/http/ImageItem;->ImageType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lweibo4android/http/ImageItem;->name:Ljava/lang/String;

    return-object v0
.end method
