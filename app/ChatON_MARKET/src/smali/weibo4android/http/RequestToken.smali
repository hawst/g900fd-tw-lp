.class public Lweibo4android/http/RequestToken;
.super Lweibo4android/http/OAuthToken;
.source "RequestToken.java"


# static fields
.field private static final serialVersionUID:J = -0x71ff4a3702538200L


# instance fields
.field private httpClient:Lweibo4android/http/HttpClient;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lweibo4android/http/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;Lweibo4android/http/HttpClient;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lweibo4android/http/OAuthToken;-><init>(Lweibo4android/http/Response;)V

    .line 41
    iput-object p2, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    .line 42
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    if-ne p0, p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :cond_3
    invoke-super {p0, p1}, Lweibo4android/http/OAuthToken;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 72
    goto :goto_0

    .line 75
    :cond_4
    check-cast p1, Lweibo4android/http/RequestToken;

    .line 77
    iget-object v2, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    iget-object v3, p1, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    invoke-virtual {v2, v3}, Lweibo4android/http/HttpClient;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 78
    goto :goto_0

    .line 77
    :cond_5
    iget-object v2, p1, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getAccessToken(Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    invoke-virtual {v0, p0, p1}, Lweibo4android/http/HttpClient;->getOAuthAccessToken(Lweibo4android/http/RequestToken;Ljava/lang/String;)Lweibo4android/http/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getAuthenticationURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    invoke-virtual {v1}, Lweibo4android/http/HttpClient;->getAuthenticationRL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?oauth_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lweibo4android/http/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorizationURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    invoke-virtual {v1}, Lweibo4android/http/HttpClient;->getAuthorizationURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?oauth_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lweibo4android/http/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Lweibo4android/http/OAuthToken;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->getToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->getTokenSecret()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->hashCode()I

    move-result v0

    .line 87
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/RequestToken;->httpClient:Lweibo4android/http/HttpClient;

    invoke-virtual {v0}, Lweibo4android/http/HttpClient;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 88
    return v0

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
