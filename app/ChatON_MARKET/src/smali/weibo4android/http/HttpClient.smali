.class public Lweibo4android/http/HttpClient;
.super Ljava/lang/Object;
.source "HttpClient.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final BAD_GATEWAY:I = 0x1f6

.field private static final BAD_REQUEST:I = 0x190

.field private static final DEBUG:Z

.field private static final FORBIDDEN:I = 0x193

.field private static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field private static final NOT_ACCEPTABLE:I = 0x196

.field private static final NOT_AUTHORIZED:I = 0x191

.field private static final NOT_FOUND:I = 0x194

.field private static final NOT_MODIFIED:I = 0x130

.field private static final OK:I = 0xc8

.field private static final SERVICE_UNAVAILABLE:I = 0x1f7

.field private static isJDK14orEarlier:Z = false

.field private static final serialVersionUID:J = 0xb36a81e0d31617cL


# instance fields
.field private accessTokenURL:Ljava/lang/String;

.field private authenticationURL:Ljava/lang/String;

.field private authorizationURL:Ljava/lang/String;

.field private basic:Ljava/lang/String;

.field private connectionTimeout:I

.field private oauth:Lweibo4android/http/OAuth;

.field private oauthToken:Lweibo4android/http/OAuthToken;

.field private password:Ljava/lang/String;

.field private proxyAuthPassword:Ljava/lang/String;

.field private proxyAuthUser:Ljava/lang/String;

.field private proxyHost:Ljava/lang/String;

.field private proxyPort:I

.field private readTimeout:I

.field private requestHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private requestTokenURL:Ljava/lang/String;

.field private retryCount:I

.field private retryIntervalMillis:I

.field private token:Ljava/lang/String;

.field private userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    invoke-static {}, Lweibo4android/Configuration;->getDebug()Z

    move-result v2

    sput-boolean v2, Lweibo4android/http/HttpClient;->DEBUG:Z

    .line 79
    sput-boolean v0, Lweibo4android/http/HttpClient;->isJDK14orEarlier:Z

    .line 91
    :try_start_0
    const-string v2, "java.specification.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    if-eqz v2, :cond_1

    .line 93
    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    cmpl-double v2, v3, v5

    if-lez v2, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lweibo4android/http/HttpClient;->isJDK14orEarlier:Z
    :try_end_0
    .catch Ljava/security/AccessControlException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_1
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    sput-boolean v1, Lweibo4android/http/HttpClient;->isJDK14orEarlier:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {}, Lweibo4android/Configuration;->getRetryCount()I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->retryCount:I

    .line 69
    invoke-static {}, Lweibo4android/Configuration;->getRetryIntervalSecs()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    .line 70
    invoke-static {}, Lweibo4android/Configuration;->getUser()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    .line 71
    invoke-static {}, Lweibo4android/Configuration;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    .line 72
    invoke-static {}, Lweibo4android/Configuration;->getProxyHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    .line 73
    invoke-static {}, Lweibo4android/Configuration;->getProxyPort()I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    .line 74
    invoke-static {}, Lweibo4android/Configuration;->getProxyUser()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    .line 75
    invoke-static {}, Lweibo4android/Configuration;->getProxyPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    .line 76
    invoke-static {}, Lweibo4android/Configuration;->getConnectionTimeout()I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    .line 77
    invoke-static {}, Lweibo4android/Configuration;->getReadTimeout()I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    .line 81
    iput-object v2, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/oauth/request_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/oauth/authorize"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/oauth/authenticate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->authenticationURL:Ljava/lang/String;

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lweibo4android/Configuration;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api.t.sina.com.cn/oauth/access_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    .line 86
    iput-object v2, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 87
    iput-object v2, p0, Lweibo4android/http/HttpClient;->token:Ljava/lang/String;

    .line 107
    iput-object v2, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    .line 108
    invoke-virtual {p0, v2}, Lweibo4android/http/HttpClient;->setUserAgent(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, v2, v2}, Lweibo4android/http/HttpClient;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "Accept-Encoding"

    const-string v1, "gzip"

    invoke-virtual {p0, v0, v1}, Lweibo4android/http/HttpClient;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lweibo4android/http/HttpClient;-><init>()V

    .line 102
    invoke-virtual {p0, p1}, Lweibo4android/http/HttpClient;->setUserId(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, p2}, Lweibo4android/http/HttpClient;->setPassword(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lweibo4android/http/HttpClient;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lweibo4android/http/HttpClient;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    return-object v0
.end method

.method private encodeBasicAuthenticationString()V
    .locals 5

    .prologue
    .line 403
    iget-object v0, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 404
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Basic "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    new-instance v2, Lweibo4android/http/BASE64Encoder;

    invoke-direct {v2}, Lweibo4android/http/BASE64Encoder;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lweibo4android/http/BASE64Encoder;->encode([B)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    .line 407
    :cond_0
    return-void
.end method

.method public static encodeParameters([Lweibo4android/http/PostParameter;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 712
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 713
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 714
    if-eqz v0, :cond_0

    .line 715
    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 718
    :cond_0
    :try_start_0
    aget-object v2, p0, v0

    iget-object v2, v2, Lweibo4android/http/PostParameter;->name:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, p0, v0

    iget-object v3, v3, Lweibo4android/http/PostParameter;->value:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 722
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 719
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private static getCause(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 908
    const/4 v0, 0x0

    .line 909
    sparse-switch p0, :sswitch_data_0

    .line 937
    const-string v0, ""

    .line 939
    :goto_0
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 913
    :sswitch_1
    const-string v0, "The request was invalid.  An accompanying error message will explain why. This is the status code will be returned during rate limiting."

    goto :goto_0

    .line 916
    :sswitch_2
    const-string v0, "Authentication credentials were missing or incorrect."

    goto :goto_0

    .line 919
    :sswitch_3
    const-string v0, "The request is understood, but it has been refused.  An accompanying error message will explain why."

    goto :goto_0

    .line 922
    :sswitch_4
    const-string v0, "The URI requested is invalid or the resource requested, such as a user, does not exists."

    goto :goto_0

    .line 925
    :sswitch_5
    const-string v0, "Returned by the Search API when an invalid format is specified in the request."

    goto :goto_0

    .line 928
    :sswitch_6
    const-string v0, "Something is broken.  Please post to the group so the Weibo team can investigate."

    goto :goto_0

    .line 931
    :sswitch_7
    const-string v0, "Weibo is down or being upgraded."

    goto :goto_0

    .line 934
    :sswitch_8
    const-string v0, "Service Unavailable: The Weibo servers are up, but overloaded with requests. Try again later. The search and trend methods use this to indicate when you are being rate limited."

    goto :goto_0

    .line 909
    :sswitch_data_0
    .sparse-switch
        0x130 -> :sswitch_0
        0x190 -> :sswitch_1
        0x191 -> :sswitch_2
        0x193 -> :sswitch_3
        0x194 -> :sswitch_4
        0x196 -> :sswitch_5
        0x1f4 -> :sswitch_6
        0x1f6 -> :sswitch_7
        0x1f7 -> :sswitch_8
    .end sparse-switch
.end method

.method private getConnection(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 769
    .line 770
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 771
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 772
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Proxy AuthUser: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 773
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Proxy AuthPassword: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 774
    new-instance v0, Lweibo4android/http/HttpClient$3;

    invoke-direct {v0, p0}, Lweibo4android/http/HttpClient$3;-><init>(Lweibo4android/http/HttpClient;)V

    invoke-static {v0}, Ljava/net/Authenticator;->setDefault(Ljava/net/Authenticator;)V

    .line 786
    :cond_0
    new-instance v0, Ljava/net/Proxy;

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    iget v3, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    invoke-static {v2, v3}, Ljava/net/InetSocketAddress;->createUnresolved(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    .line 787
    sget-boolean v1, Lweibo4android/http/HttpClient;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 788
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Opening proxied connection("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 790
    :cond_1
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 794
    :goto_0
    iget v1, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    if-lez v1, :cond_2

    sget-boolean v1, Lweibo4android/http/HttpClient;->isJDK14orEarlier:Z

    if-nez v1, :cond_2

    .line 795
    iget v1, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 797
    :cond_2
    iget v1, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    if-lez v1, :cond_3

    sget-boolean v1, Lweibo4android/http/HttpClient;->isJDK14orEarlier:Z

    if-nez v1, :cond_3

    .line 798
    iget v1, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 800
    :cond_3
    return-object v0

    .line 792
    :cond_4
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    goto :goto_0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 896
    sget-boolean v0, Lweibo4android/http/HttpClient;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 897
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 899
    :cond_0
    return-void
.end method

.method private static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 902
    sget-boolean v0, Lweibo4android/http/HttpClient;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 903
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 905
    :cond_0
    return-void
.end method

.method private setHeaders(Ljava/lang/String;[Lweibo4android/http/PostParameter;Ljava/net/HttpURLConnection;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 735
    const-string v0, "Request: "

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 736
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    if-eqz p4, :cond_1

    .line 739
    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-nez v0, :cond_0

    .line 742
    :cond_0
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-eqz v0, :cond_2

    .line 744
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    invoke-virtual {v0, p5, p1, p2, v1}, Lweibo4android/http/OAuth;->generateAuthorizationHeader(Ljava/lang/String;Ljava/lang/String;[Lweibo4android/http/PostParameter;Lweibo4android/http/OAuthToken;)Ljava/lang/String;

    move-result-object v0

    .line 751
    :goto_0
    const-string v1, "Authorization"

    invoke-virtual {p3, v1, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Authorization: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 754
    :cond_1
    iget-object v0, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 755
    iget-object v1, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 745
    :cond_2
    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 747
    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    goto :goto_0

    .line 749
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Neither user ID/password combination nor OAuth consumer key/secret combination supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 758
    :cond_4
    return-void
.end method


# virtual methods
.method public delete(Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 2

    .prologue
    .line 443
    const/4 v0, 0x0

    const-string v1, "DELETE"

    invoke-virtual {p0, p1, v0, p2, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 805
    if-ne p0, p1, :cond_1

    .line 869
    :cond_0
    :goto_0
    return v0

    .line 808
    :cond_1
    instance-of v2, p1, Lweibo4android/http/HttpClient;

    if-nez v2, :cond_2

    move v0, v1

    .line 809
    goto :goto_0

    .line 812
    :cond_2
    check-cast p1, Lweibo4android/http/HttpClient;

    .line 814
    iget v2, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    iget v3, p1, Lweibo4android/http/HttpClient;->connectionTimeout:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 815
    goto :goto_0

    .line 817
    :cond_3
    iget v2, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    iget v3, p1, Lweibo4android/http/HttpClient;->proxyPort:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 818
    goto :goto_0

    .line 820
    :cond_4
    iget v2, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    iget v3, p1, Lweibo4android/http/HttpClient;->readTimeout:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 821
    goto :goto_0

    .line 823
    :cond_5
    iget v2, p0, Lweibo4android/http/HttpClient;->retryCount:I

    iget v3, p1, Lweibo4android/http/HttpClient;->retryCount:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 824
    goto :goto_0

    .line 826
    :cond_6
    iget v2, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    iget v3, p1, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 827
    goto :goto_0

    .line 829
    :cond_7
    iget-object v2, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 830
    goto :goto_0

    .line 829
    :cond_9
    iget-object v2, p1, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 832
    :cond_a
    iget-object v2, p0, Lweibo4android/http/HttpClient;->authenticationURL:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->authenticationURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 833
    goto :goto_0

    .line 835
    :cond_b
    iget-object v2, p0, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 836
    goto :goto_0

    .line 838
    :cond_c
    iget-object v2, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 839
    goto :goto_0

    .line 838
    :cond_e
    iget-object v2, p1, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 841
    :cond_f
    iget-object v2, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    invoke-virtual {v2, v3}, Lweibo4android/http/OAuth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 842
    goto :goto_0

    .line 841
    :cond_11
    iget-object v2, p1, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-nez v2, :cond_10

    .line 844
    :cond_12
    iget-object v2, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    invoke-virtual {v2, v3}, Lweibo4android/http/OAuthToken;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 845
    goto/16 :goto_0

    .line 844
    :cond_14
    iget-object v2, p1, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    if-nez v2, :cond_13

    .line 847
    :cond_15
    iget-object v2, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 848
    goto/16 :goto_0

    .line 847
    :cond_17
    iget-object v2, p1, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 850
    :cond_18
    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 851
    goto/16 :goto_0

    .line 850
    :cond_1a
    iget-object v2, p1, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    if-nez v2, :cond_19

    .line 853
    :cond_1b
    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 854
    goto/16 :goto_0

    .line 853
    :cond_1d
    iget-object v2, p1, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 856
    :cond_1e
    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    .line 857
    goto/16 :goto_0

    .line 856
    :cond_20
    iget-object v2, p1, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    if-nez v2, :cond_1f

    .line 859
    :cond_21
    iget-object v2, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v0, v1

    .line 860
    goto/16 :goto_0

    .line 862
    :cond_22
    iget-object v2, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move v0, v1

    .line 863
    goto/16 :goto_0

    .line 865
    :cond_23
    iget-object v2, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    iget-object v3, p1, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 866
    goto/16 :goto_0

    .line 865
    :cond_24
    iget-object v2, p1, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public get(Ljava/lang/String;)Lweibo4android/http/Response;
    .locals 2

    .prologue
    .line 608
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 1

    .prologue
    .line 604
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public getAccessTokenURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthenticationRL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lweibo4android/http/HttpClient;->authenticationURL:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthorizationURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    return-object v0
.end method

.method public getConnectionTimeout()I
    .locals 1

    .prologue
    .line 368
    iget v0, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    return v0
.end method

.method public getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 4

    .prologue
    .line 230
    :try_start_0
    new-instance v0, Lweibo4android/http/HttpClient$1;

    invoke-direct {v0, p0, p1, p2}, Lweibo4android/http/HttpClient$1;-><init>(Lweibo4android/http/HttpClient;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 232
    new-instance v0, Lweibo4android/http/AccessToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/http/AccessToken;-><init>(Lweibo4android/http/Response;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/AccessToken;

    return-object v0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    new-instance v1, Lweibo4android/WeiboException;

    const-string v2, "The user has not given access to the account."

    invoke-virtual {v0}, Lweibo4android/WeiboException;->getStatusCode()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;I)V

    throw v1
.end method

.method public getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 6

    .prologue
    .line 252
    :try_start_0
    new-instance v0, Lweibo4android/http/HttpClient$2;

    invoke-direct {v0, p0, p1, p2}, Lweibo4android/http/HttpClient$2;-><init>(Lweibo4android/http/HttpClient;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 254
    new-instance v0, Lweibo4android/http/AccessToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "oauth_verifier"

    invoke-direct {v4, v5, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/http/AccessToken;-><init>(Lweibo4android/http/Response;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/AccessToken;

    return-object v0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    new-instance v1, Lweibo4android/WeiboException;

    const-string v2, "The user has not given access to the account."

    invoke-virtual {v0}, Lweibo4android/WeiboException;->getStatusCode()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;I)V

    throw v1
.end method

.method public getOAuthAccessToken(Lweibo4android/http/RequestToken;)Lweibo4android/http/AccessToken;
    .locals 4

    .prologue
    .line 194
    :try_start_0
    iput-object p1, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 195
    new-instance v0, Lweibo4android/http/AccessToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/http/AccessToken;-><init>(Lweibo4android/http/Response;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/AccessToken;

    return-object v0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    new-instance v1, Lweibo4android/WeiboException;

    const-string v2, "The user has not given access to the account."

    invoke-virtual {v0}, Lweibo4android/WeiboException;->getStatusCode()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;I)V

    throw v1
.end method

.method public getOAuthAccessToken(Lweibo4android/http/RequestToken;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 6

    .prologue
    .line 211
    :try_start_0
    iput-object p1, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 212
    new-instance v0, Lweibo4android/http/AccessToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "oauth_verifier"

    invoke-direct {v4, v5, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/http/AccessToken;-><init>(Lweibo4android/http/Response;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/AccessToken;

    return-object v0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    new-instance v1, Lweibo4android/WeiboException;

    const-string v2, "The user has not given access to the account."

    invoke-virtual {v0}, Lweibo4android/WeiboException;->getStatusCode()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;I)V

    throw v1
.end method

.method public getOAuthRequestToken()Lweibo4android/http/RequestToken;
    .locals 4

    .prologue
    .line 168
    new-instance v0, Lweibo4android/http/RequestToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/http/RequestToken;-><init>(Lweibo4android/http/Response;Lweibo4android/http/HttpClient;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 169
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/RequestToken;

    return-object v0
.end method

.method public getOauthRequestToken(Ljava/lang/String;)Lweibo4android/http/RequestToken;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 181
    new-instance v0, Lweibo4android/http/RequestToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    new-array v2, v6, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "oauth_callback"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, v6}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lweibo4android/http/RequestToken;-><init>(Lweibo4android/http/Response;Lweibo4android/http/HttpClient;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 182
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/RequestToken;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyAuthPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyAuthUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPort()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    return v0
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    return v0
.end method

.method public getRequestHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRequestTokenURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    const-string v0, "User-Agent"

    invoke-virtual {p0, v0}, Lweibo4android/http/HttpClient;->getRequestHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public getXAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 262
    new-instance v0, Lweibo4android/http/AccessToken;

    iget-object v1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Lweibo4android/http/PostParameter;

    const/4 v3, 0x0

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "x_auth_username"

    invoke-direct {v4, v5, p1}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "x_auth_password"

    invoke-direct {v3, v4, p2}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v6

    const/4 v3, 0x2

    new-instance v4, Lweibo4android/http/PostParameter;

    const-string v5, "x_auth_mode"

    invoke-direct {v4, v5, p3}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2, v6}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lweibo4android/http/AccessToken;-><init>(Lweibo4android/http/Response;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 263
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/AccessToken;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 874
    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 875
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/http/HttpClient;->retryCount:I

    add-int/2addr v0, v2

    .line 876
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    add-int/2addr v0, v2

    .line 877
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 878
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 879
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 880
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    add-int/2addr v0, v2

    .line 881
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 882
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 883
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    add-int/2addr v0, v2

    .line 884
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    add-int/2addr v0, v2

    .line 885
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 886
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    invoke-virtual {v0}, Lweibo4android/http/OAuth;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 887
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 888
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 889
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/http/HttpClient;->authenticationURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 890
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 891
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    invoke-virtual {v1}, Lweibo4android/http/OAuthToken;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 892
    return v0

    :cond_1
    move v0, v1

    .line 874
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 877
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 878
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 879
    goto :goto_3

    :cond_5
    move v0, v1

    .line 881
    goto :goto_4

    :cond_6
    move v0, v1

    .line 882
    goto :goto_5

    :cond_7
    move v0, v1

    .line 886
    goto :goto_6

    :cond_8
    move v0, v1

    .line 890
    goto :goto_7
.end method

.method protected httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;
    .locals 6

    .prologue
    .line 613
    .line 615
    const-string v0, "GET"

    .line 616
    if-eqz p2, :cond_1

    .line 617
    const-string v1, "POST"

    .line 618
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    .line 619
    new-array v2, v0, [Lweibo4android/http/PostParameter;

    .line 620
    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_0

    .line 621
    aget-object v3, p2, v0

    aput-object v3, v2, v0

    .line 620
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 623
    :cond_0
    array-length v0, p2

    new-instance v3, Lweibo4android/http/PostParameter;

    const-string v4, "source"

    sget-object v5, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    move-object v0, v1

    move-object p2, v2

    .line 625
    :cond_1
    invoke-virtual {p0, p1, p2, p3, v0}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;ZLjava/lang/String;)Lweibo4android/http/Response;
    .locals 13

    .prologue
    .line 630
    iget v1, p0, Lweibo4android/http/HttpClient;->retryCount:I

    add-int/lit8 v11, v1, 0x1

    .line 631
    const/4 v7, 0x0

    .line 632
    const/4 v1, 0x0

    move v10, v1

    :goto_0
    if-ge v10, v11, :cond_b

    .line 633
    const/4 v9, -0x1

    .line 636
    const/4 v8, 0x0

    .line 638
    :try_start_0
    invoke-direct {p0, p1}, Lweibo4android/http/HttpClient;->getConnection(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 639
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    .line 640
    invoke-direct/range {v1 .. v6}, Lweibo4android/http/HttpClient;->setHeaders(Ljava/lang/String;[Lweibo4android/http/PostParameter;Ljava/net/HttpURLConnection;ZLjava/lang/String;)V

    .line 641
    if-nez p2, :cond_0

    const-string v1, "POST"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 642
    :cond_0
    const-string v1, "POST"

    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 643
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-virtual {v4, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 645
    const-string v1, ""

    .line 646
    if-eqz p2, :cond_1

    .line 647
    invoke-static {p2}, Lweibo4android/http/HttpClient;->encodeParameters([Lweibo4android/http/PostParameter;)Ljava/lang/String;

    move-result-object v1

    .line 649
    :cond_1
    const-string v2, "Post Params: "

    invoke-static {v2, v1}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 652
    const-string v2, "Content-Length"

    array-length v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 654
    :try_start_1
    invoke-virtual {v2, v1}, Ljava/io/OutputStream;->write([B)V

    .line 655
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 656
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-object v5, v2

    .line 662
    :goto_1
    :try_start_2
    new-instance v3, Lweibo4android/http/Response;

    invoke-direct {v3, v4}, Lweibo4android/http/Response;-><init>(Ljava/net/HttpURLConnection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 663
    :try_start_3
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v6

    .line 664
    :try_start_4
    sget-boolean v1, Lweibo4android/http/HttpClient;->DEBUG:Z

    if-eqz v1, :cond_6

    .line 665
    const-string v1, "Response: "

    invoke-static {v1}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 666
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v4

    .line 667
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 668
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 669
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 670
    if-eqz v1, :cond_5

    .line 671
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ": "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 687
    :catchall_0
    move-exception v1

    move-object v2, v5

    move-object v7, v3

    move v3, v6

    .line 688
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 687
    :goto_4
    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 692
    :catch_0
    move-exception v1

    move v6, v3

    move-object v3, v7

    .line 694
    :goto_5
    iget v2, p0, Lweibo4android/http/HttpClient;->retryCount:I

    if-ne v10, v2, :cond_c

    .line 695
    new-instance v2, Lweibo4android/WeiboException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, v6}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;Ljava/lang/Exception;I)V

    throw v2

    .line 657
    :cond_3
    :try_start_7
    const-string v1, "DELETE"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 658
    const-string v1, "DELETE"

    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    move-object v5, v8

    goto/16 :goto_1

    .line 660
    :cond_4
    const-string v1, "GET"

    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v5, v8

    goto/16 :goto_1

    .line 673
    :cond_5
    :try_start_8
    invoke-static {v2}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 678
    :cond_6
    const/16 v1, 0xc8

    if-eq v6, v1, :cond_8

    .line 679
    const/16 v1, 0x1f4

    if-lt v6, v1, :cond_7

    iget v1, p0, Lweibo4android/http/HttpClient;->retryCount:I

    if-ne v10, v1, :cond_9

    .line 680
    :cond_7
    new-instance v1, Lweibo4android/WeiboException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6}, Lweibo4android/http/HttpClient;->getCause(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lweibo4android/WeiboException;-><init>(Ljava/lang/String;I)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 688
    :cond_8
    :try_start_9
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :goto_6
    move-object v1, v3

    .line 708
    :goto_7
    return-object v1

    .line 688
    :cond_9
    :try_start_a
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :goto_8
    move-object v7, v3

    .line 699
    :goto_9
    :try_start_b
    sget-boolean v1, Lweibo4android/http/HttpClient;->DEBUG:Z

    if-eqz v1, :cond_a

    if-eqz v7, :cond_a

    .line 700
    invoke-virtual {v7}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    .line 702
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sleeping "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millisecs for next retry."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lweibo4android/http/HttpClient;->log(Ljava/lang/String;)V

    .line 703
    iget v1, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_4

    .line 632
    :goto_a
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto/16 :goto_0

    .line 689
    :catch_1
    move-exception v2

    goto/16 :goto_4

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_6

    .line 704
    :catch_4
    move-exception v1

    goto :goto_a

    .line 692
    :catch_5
    move-exception v1

    goto/16 :goto_5

    .line 687
    :catchall_1
    move-exception v1

    move-object v2, v8

    move v3, v9

    goto/16 :goto_3

    :catchall_2
    move-exception v1

    move v3, v9

    goto/16 :goto_3

    :catchall_3
    move-exception v1

    move-object v2, v5

    move v3, v9

    goto/16 :goto_3

    :catchall_4
    move-exception v1

    move-object v2, v5

    move-object v7, v3

    move v3, v9

    goto/16 :goto_3

    :cond_b
    move-object v1, v7

    goto :goto_7

    :cond_c
    move-object v7, v3

    goto :goto_9
.end method

.method public isAuthenticationEnabled()Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lweibo4android/http/HttpClient;->basic:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public post(Ljava/lang/String;)Lweibo4android/http/Response;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 600
    new-array v0, v1, [Lweibo4android/http/PostParameter;

    invoke-virtual {p0, p1, v0, v1}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;Z)Lweibo4android/http/Response;
    .locals 1

    .prologue
    .line 592
    const/4 v0, 0x0

    new-array v0, v0, [Lweibo4android/http/PostParameter;

    invoke-virtual {p0, p1, v0, p2}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;[Lweibo4android/http/PostParameter;)Lweibo4android/http/Response;
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;
    .locals 5

    .prologue
    .line 434
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Lweibo4android/http/PostParameter;

    .line 435
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 436
    aget-object v2, p2, v0

    aput-object v2, v1, v0

    .line 435
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 438
    :cond_0
    array-length v0, p2

    new-instance v2, Lweibo4android/http/PostParameter;

    const-string v3, "source"

    sget-object v4, Lweibo4android/Weibo;->CONSUMER_KEY:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lweibo4android/http/PostParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v0

    .line 439
    invoke-virtual {p0, p1, v1, p3}, Lweibo4android/http/HttpClient;->httpRequest(Ljava/lang/String;[Lweibo4android/http/PostParameter;Z)Lweibo4android/http/Response;

    move-result-object v0

    return-object v0
.end method

.method public setAccessTokenURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lweibo4android/http/HttpClient;->accessTokenURL:Ljava/lang/String;

    .line 303
    return-void
.end method

.method public setAuthorizationURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lweibo4android/http/HttpClient;->authorizationURL:Ljava/lang/String;

    .line 288
    return-void
.end method

.method public setConnectionTimeout(I)V
    .locals 1

    .prologue
    .line 382
    invoke-static {p1}, Lweibo4android/Configuration;->getConnectionTimeout(I)I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->connectionTimeout:I

    .line 384
    return-void
.end method

.method public setOAuthAccessToken(Lweibo4android/http/AccessToken;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 276
    return-void
.end method

.method public setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 148
    invoke-static {p1}, Lweibo4android/Configuration;->getOAuthConsumerKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {p2}, Lweibo4android/Configuration;->getOAuthConsumerSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    new-instance v2, Lweibo4android/http/OAuth;

    invoke-direct {v2, v0, v1}, Lweibo4android/http/OAuth;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lweibo4android/http/HttpClient;->oauth:Lweibo4android/http/OAuth;

    .line 153
    :cond_0
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lweibo4android/http/HttpClient;->password:Ljava/lang/String;

    .line 120
    invoke-direct {p0}, Lweibo4android/http/HttpClient;->encodeBasicAuthenticationString()V

    .line 121
    return-void
.end method

.method public setProxyAuthPassword(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 364
    invoke-static {p1}, Lweibo4android/Configuration;->getProxyPassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthPassword:Ljava/lang/String;

    .line 365
    return-void
.end method

.method public setProxyAuthUser(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 350
    invoke-static {p1}, Lweibo4android/Configuration;->getProxyUser(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyAuthUser:Ljava/lang/String;

    .line 351
    return-void
.end method

.method public setProxyHost(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 321
    invoke-static {p1}, Lweibo4android/Configuration;->getProxyHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/HttpClient;->proxyHost:Ljava/lang/String;

    .line 322
    return-void
.end method

.method public setProxyPort(I)V
    .locals 1

    .prologue
    .line 336
    invoke-static {p1}, Lweibo4android/Configuration;->getProxyPort(I)I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->proxyPort:I

    .line 337
    return-void
.end method

.method public setReadTimeout(I)V
    .locals 1

    .prologue
    .line 399
    invoke-static {p1}, Lweibo4android/Configuration;->getReadTimeout(I)I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->readTimeout:I

    .line 400
    return-void
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lweibo4android/http/HttpClient;->requestHeaders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 762
    return-void
.end method

.method public setRequestTokenURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lweibo4android/http/HttpClient;->requestTokenURL:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public setRetryCount(I)V
    .locals 2

    .prologue
    .line 410
    if-ltz p1, :cond_0

    .line 411
    invoke-static {p1}, Lweibo4android/Configuration;->getRetryCount(I)I

    move-result v0

    iput v0, p0, Lweibo4android/http/HttpClient;->retryCount:I

    .line 415
    return-void

    .line 413
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RetryCount cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRetryIntervalSecs(I)V
    .locals 2

    .prologue
    .line 426
    if-ltz p1, :cond_0

    .line 427
    invoke-static {p1}, Lweibo4android/Configuration;->getRetryIntervalSecs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lweibo4android/http/HttpClient;->retryIntervalMillis:I

    .line 431
    return-void

    .line 429
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RetryInterval cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setToken(Ljava/lang/String;Ljava/lang/String;)Lweibo4android/http/RequestToken;
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Lweibo4android/http/HttpClient;->token:Ljava/lang/String;

    .line 157
    new-instance v0, Lweibo4android/http/RequestToken;

    invoke-direct {v0, p1, p2}, Lweibo4android/http/RequestToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    .line 158
    iget-object v0, p0, Lweibo4android/http/HttpClient;->oauthToken:Lweibo4android/http/OAuthToken;

    check-cast v0, Lweibo4android/http/RequestToken;

    return-object v0
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 418
    const-string v0, "User-Agent"

    invoke-static {p1}, Lweibo4android/Configuration;->getUserAgent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lweibo4android/http/HttpClient;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lweibo4android/http/HttpClient;->userId:Ljava/lang/String;

    .line 115
    invoke-direct {p0}, Lweibo4android/http/HttpClient;->encodeBasicAuthenticationString()V

    .line 116
    return-void
.end method
