.class public Lweibo4android/http/AccessToken;
.super Lweibo4android/http/OAuthToken;
.source "AccessToken.java"


# static fields
.field private static final serialVersionUID:J = -0x73cdb85869c93e33L


# instance fields
.field private screenName:Ljava/lang/String;

.field private userId:J


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lweibo4android/http/OAuthToken;-><init>(Ljava/lang/String;)V

    .line 50
    const-string v0, "screen_name"

    invoke-virtual {p0, v0}, Lweibo4android/http/AccessToken;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/http/AccessToken;->screenName:Ljava/lang/String;

    .line 51
    const-string v0, "user_id"

    invoke-virtual {p0, v0}, Lweibo4android/http/AccessToken;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lweibo4android/http/AccessToken;->userId:J

    .line 56
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lweibo4android/http/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method constructor <init>(Lweibo4android/http/Response;)V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p1}, Lweibo4android/http/Response;->asString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lweibo4android/http/AccessToken;-><init>(Ljava/lang/String;)V

    .line 45
    return-void
.end method


# virtual methods
.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lweibo4android/http/OAuthToken;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lweibo4android/http/OAuthToken;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lweibo4android/http/AccessToken;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->getToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->getTokenSecret()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lweibo4android/http/AccessToken;->userId:J

    return-wide v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lweibo4android/http/OAuthToken;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
