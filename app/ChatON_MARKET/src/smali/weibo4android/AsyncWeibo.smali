.class public Lweibo4android/AsyncWeibo;
.super Lweibo4android/Weibo;
.source "AsyncWeibo.java"


# static fields
.field public static final BLOCK:I = 0x16
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CREATE:I = 0xc

.field public static final CREATED_BLOCK:I = 0x2b

.field public static final CREATE_FAVORITE:I = 0x12

.field public static final CREATE_FRIENDSHIP:I = 0x20

.field public static final CURRENT_TRENDS:I = 0x2d

.field public static final DAILY_TRENDS:I = 0x2e

.field public static final DESTORY:I = 0xd
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DESTROY:I = 0xd
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DESTROYED_BLOCK:I = 0x2a

.field public static final DESTROY_DIRECT_MESSAGES:I = 0x28

.field public static final DESTROY_FAVORITE:I = 0x13

.field public static final DESTROY_FRIENDSHIP:I = 0x21

.field public static final DESTROY_STATUS:I = 0x1a

.field public static final DIRECT_MESSAGES:I = 0xa

.field public static final DISABLE_NOTIFICATION:I = 0x24

.field public static final ENABLE_NOTIFICATION:I = 0x23

.field public static final EXISTS:I = 0x1c
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final EXISTS_BLOCK:I = 0x30

.field public static final EXISTS_FRIENDSHIP:I = 0x22

.field public static final FAVORITES:I = 0x11

.field public static final FEATURED:I = 0x8

.field public static final FOLLOW:I = 0xe
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FOLLOWERS:I = 0x7

.field public static final FOLLOWERS_IDS:I = 0x1e

.field public static final FRIENDS:I = 0x6

.field public static final FRIENDS_IDS:I = 0x1d

.field public static final FRIENDS_TIMELINE:I = 0x1

.field private static final GET_BLOCKING_USERS:I = 0x31

.field private static final GET_BLOCKING_USERS_IDS:I = 0x32

.field public static final GET_DOWNTIME_SCHEDULE:I = 0x19
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HOME_TIMELINE:I = 0x33

.field public static final LEAVE:I = 0xf
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MENTIONS:I = 0x25

.field public static final PUBLIC_TIMELINE:I = 0x0

.field public static final RATE_LIMIT_STATUS:I = 0x1c

.field public static final REPLIES:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RETWEETED_BY_ME:I = 0x35

.field public static final RETWEETED_TO_ME:I = 0x36

.field public static final RETWEETS_OF_ME:I = 0x37

.field public static final RETWEET_STATUS:I = 0x34

.field public static final SEARCH:I = 0x1b

.field public static final SEND_DIRECT_MESSAGE:I = 0xb

.field public static final SHOW:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SHOW_STATUS:I = 0x26

.field public static final TEST:I = 0x18

.field public static final TRENDS:I = 0x2c

.field public static final UNBLOCK:I = 0x17
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final UPDATE:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final UPDATE_DELIVERLY_DEVICE:I = 0x15

.field public static final UPDATE_LOCATION:I = 0x14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final UPDATE_PROFILE:I = 0x29

.field public static final UPDATE_PROFILE_COLORS:I = 0x1f

.field public static final UPDATE_STATUS:I = 0x27

.field public static final USER_DETAIL:I = 0x9

.field public static final USER_TIMELINE:I = 0x2

.field public static final WEEKLY_TRENDS:I = 0x2f

.field private static transient dispatcher:Lweibo4android/Dispatcher; = null

.field private static final serialVersionUID:J = -0x1be038d7c798e703L


# instance fields
.field private shutdown:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lweibo4android/Weibo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3848
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/AsyncWeibo;->shutdown:Z

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lweibo4android/Weibo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3848
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/AsyncWeibo;->shutdown:Z

    .line 46
    return-void
.end method

.method private getDispatcher()Lweibo4android/Dispatcher;
    .locals 3

    .prologue
    .line 3867
    const/4 v0, 0x1

    iget-boolean v1, p0, Lweibo4android/AsyncWeibo;->shutdown:Z

    if-ne v0, v1, :cond_0

    .line 3868
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already shut down"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3870
    :cond_0
    sget-object v0, Lweibo4android/AsyncWeibo;->dispatcher:Lweibo4android/Dispatcher;

    if-nez v0, :cond_1

    .line 3871
    new-instance v0, Lweibo4android/Dispatcher;

    const-string v1, "Weibo4J Async Dispatcher"

    invoke-static {}, Lweibo4android/Configuration;->getNumberOfAsyncThreads()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lweibo4android/Dispatcher;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lweibo4android/AsyncWeibo;->dispatcher:Lweibo4android/Dispatcher;

    .line 3873
    :cond_1
    sget-object v0, Lweibo4android/AsyncWeibo;->dispatcher:Lweibo4android/Dispatcher;

    return-object v0
.end method


# virtual methods
.method public blockAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3653
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$104;

    const/16 v2, 0x16

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$104;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3659
    return-void
.end method

.method public createAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2374
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$64;

    const/16 v2, 0xc

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$64;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2380
    return-void
.end method

.method public createAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2301
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$62;

    const/16 v2, 0xc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$62;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2307
    return-void
.end method

.method public createBlockAsync(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3693
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lweibo4android/AsyncWeibo;->createBlockAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3694
    return-void
.end method

.method public createBlockAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3674
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$105;

    const/16 v2, 0x2b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$105;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3680
    return-void
.end method

.method public createFavoriteAsync(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3382
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/AsyncWeibo;->createFavoriteAsync(J)V

    .line 3383
    return-void
.end method

.method public createFavoriteAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3343
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->createFavoriteAsync(JLweibo4android/WeiboListener;)V

    .line 3344
    return-void
.end method

.method public createFavoriteAsync(J)V
    .locals 7

    .prologue
    .line 3397
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$95;

    const/16 v2, 0x11

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$95;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3403
    return-void
.end method

.method public createFavoriteAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 3360
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$94;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$94;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3366
    return-void
.end method

.method public createFriendshipAsync(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2394
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lweibo4android/AsyncWeibo;->createFriendshipAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 2395
    return-void
.end method

.method public createFriendshipAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2323
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$63;

    const/16 v2, 0x20

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$63;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2329
    return-void
.end method

.method public deleteDirectMessageAsync(ILweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2241
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$59;

    const/16 v2, 0x28

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$59;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2247
    return-void
.end method

.method public destoryStatusAsync(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1393
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/AsyncWeibo;->destroyStatusAsync(J)V

    .line 1394
    return-void
.end method

.method public destoryStatusAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1446
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->destroyStatusAsync(JLweibo4android/WeiboListener;)V

    .line 1447
    return-void
.end method

.method public destroyBlockAsync(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3751
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lweibo4android/AsyncWeibo;->destroyBlockAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3752
    return-void
.end method

.method public destroyBlockAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3731
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$107;

    const/16 v2, 0x2a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$107;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3737
    return-void
.end method

.method public destroyDirectMessageAsync(I)V
    .locals 7

    .prologue
    .line 2277
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$61;

    const/16 v2, 0x28

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$61;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2283
    return-void
.end method

.method public destroyDirectMessageAsync(ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2261
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$60;

    const/16 v2, 0x28

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$60;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2267
    return-void
.end method

.method public destroyFavoriteAsync(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3459
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/AsyncWeibo;->destroyFavoriteAsync(J)V

    .line 3460
    return-void
.end method

.method public destroyFavoriteAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3421
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->destroyFavoriteAsync(JLweibo4android/WeiboListener;)V

    .line 3422
    return-void
.end method

.method public destroyFavoriteAsync(J)V
    .locals 7

    .prologue
    .line 3474
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$97;

    const/16 v2, 0x11

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$97;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3480
    return-void
.end method

.method public destroyFavoriteAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 3438
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$96;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$96;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3444
    return-void
.end method

.method public destroyStatusAsync(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1409
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lweibo4android/AsyncWeibo;->destroyStatusAsync(J)V

    .line 1410
    return-void
.end method

.method public destroyStatusAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1464
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->destroyStatusAsync(JLweibo4android/WeiboListener;)V

    .line 1465
    return-void
.end method

.method public destroyStatusAsync(J)V
    .locals 7

    .prologue
    .line 1423
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$38;

    const/16 v2, 0x1a

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$38;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1429
    return-void
.end method

.method public destroyStatusAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 1480
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$39;

    const/16 v2, 0x1a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$39;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1486
    return-void
.end method

.method public disableNotificationAsync(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3635
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lweibo4android/AsyncWeibo;->disableNotificationAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3636
    return-void
.end method

.method public disableNotificationAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3596
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$102;

    const/16 v2, 0x24

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$102;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3602
    return-void
.end method

.method public enableNotificationAsync(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3557
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lweibo4android/AsyncWeibo;->enableNotificationAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3558
    return-void
.end method

.method public enableNotificationAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3518
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$99;

    const/16 v2, 0x23

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$99;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3524
    return-void
.end method

.method public existsAsync(Ljava/lang/String;Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2502
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$65;

    const/16 v2, 0x1c

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$65;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2508
    return-void
.end method

.method public existsBlockAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3764
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$108;

    const/16 v2, 0x30

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$108;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3770
    return-void
.end method

.method public existsFriendshipAsync(Ljava/lang/String;Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2524
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$66;

    const/16 v2, 0x22

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$66;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2530
    return-void
.end method

.method public favoritesAsync(ILweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3207
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->getFavoritesAsync(ILweibo4android/WeiboListener;)V

    .line 3208
    return-void
.end method

.method public favoritesAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3295
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$92;

    const/16 v2, 0x11

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$92;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3301
    return-void
.end method

.method public favoritesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3247
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$90;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$90;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3253
    return-void
.end method

.method public favoritesAsync(Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3170
    invoke-virtual {p0, p1}, Lweibo4android/AsyncWeibo;->getFavoritesAsync(Lweibo4android/WeiboListener;)V

    .line 3171
    return-void
.end method

.method public followAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3538
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$100;

    const/16 v2, 0xe

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$100;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3544
    return-void
.end method

.method public followAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3497
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$98;

    const/16 v2, 0xe

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$98;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3503
    return-void
.end method

.method public getAuthenticatedUserAsync(Lweibo4android/WeiboListener;)V
    .locals 2

    .prologue
    .line 3896
    invoke-virtual {p0}, Lweibo4android/AsyncWeibo;->getUserId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3897
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "User Id not specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3899
    :cond_0
    invoke-virtual {p0}, Lweibo4android/AsyncWeibo;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lweibo4android/AsyncWeibo;->getUserDetailAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3900
    return-void
.end method

.method public getBlockingUsersAsync(ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 3802
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$110;

    const/16 v2, 0x31

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$110;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3808
    return-void
.end method

.method public getBlockingUsersAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 3782
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$109;

    const/16 v2, 0x31

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$109;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3788
    return-void
.end method

.method public getBlockingUsersIDsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 3820
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$111;

    const/16 v2, 0x32

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$111;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3826
    return-void
.end method

.method public getDailyTrendsAsync(Ljava/util/Date;ZLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 146
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$2;

    const/16 v2, 0x2e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$2;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 152
    return-void
.end method

.method public getDailyTrendsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 124
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$1;

    const/16 v2, 0x2e

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$1;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 130
    return-void
.end method

.method public getDirectMessagesAsync(ILweibo4android/WeiboListener;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2060
    new-instance v0, Lweibo4android/Paging;

    int-to-long v1, p1

    invoke-direct {v0, v1, v2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 2061
    return-void
.end method

.method public getDirectMessagesAsync(Ljava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2076
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$53;

    const/16 v2, 0xa

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$53;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2082
    return-void
.end method

.method public getDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2000
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$52;

    const/16 v2, 0xa

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$52;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2006
    return-void
.end method

.method public getDirectMessagesAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 1980
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$51;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$51;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1986
    return-void
.end method

.method public getDirectMessagesByPageAsync(IILweibo4android/WeiboListener;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2043
    new-instance v0, Lweibo4android/Paging;

    int-to-long v1, p2

    invoke-direct {v0, p1, v1, v2}, Lweibo4android/Paging;-><init>(IJ)V

    invoke-virtual {p0, v0, p3}, Lweibo4android/AsyncWeibo;->getDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 2044
    return-void
.end method

.method public getDirectMessagesByPageAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2022
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 2023
    return-void
.end method

.method public getDowntimeScheduleAsync()V
    .locals 4

    .prologue
    .line 3883
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "this method is not supported by the Weibo API anymore"

    new-instance v2, Ljava/lang/NoSuchMethodException;

    const-string v3, "this method is not supported by the Weibo API anymore"

    invoke-direct {v2, v3}, Ljava/lang/NoSuchMethodException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public getFavoritesAsync(ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 3223
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$89;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$89;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3229
    return-void
.end method

.method public getFavoritesAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 3319
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$93;

    const/16 v2, 0x11

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$93;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3325
    return-void
.end method

.method public getFavoritesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3269
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$91;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$91;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3275
    return-void
.end method

.method public getFavoritesAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 3184
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$88;

    const/16 v2, 0x11

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$88;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3190
    return-void
.end method

.method public getFeaturedAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 1963
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$50;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$50;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1969
    return-void
.end method

.method public getFollowersAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1839
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1840
    return-void
.end method

.method public getFollowersAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1952
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, p1, v0, p3}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1953
    return-void
.end method

.method public getFollowersAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1904
    invoke-virtual {p0, p1, p2, p3}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1905
    return-void
.end method

.method public getFollowersAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1859
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 1860
    return-void
.end method

.method public getFollowersAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 0

    .prologue
    .line 1797
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1798
    return-void
.end method

.method public getFollowersAsync(Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1759
    invoke-virtual {p0, p1}, Lweibo4android/AsyncWeibo;->getFollowersStatusesAsync(Lweibo4android/WeiboListener;)V

    .line 1760
    return-void
.end method

.method public getFollowersIDsAsync(IJLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2891
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$81;

    const/16 v2, 0x1e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$81;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2897
    return-void
.end method

.method public getFollowersIDsAsync(ILweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2865
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$80;

    const/16 v2, 0x1e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$80;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2871
    return-void
.end method

.method public getFollowersIDsAsync(ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2836
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$79;

    const/16 v2, 0x1e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$79;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2842
    return-void
.end method

.method public getFollowersIDsAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2814
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$78;

    const/16 v2, 0x1e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$78;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2820
    return-void
.end method

.method public getFollowersIDsAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2968
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$84;

    const/16 v2, 0x1e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$84;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2974
    return-void
.end method

.method public getFollowersIDsAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2942
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$83;

    const/16 v2, 0x1e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$83;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2948
    return-void
.end method

.method public getFollowersIDsAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2913
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$82;

    const/16 v2, 0x1e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$82;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2919
    return-void
.end method

.method public getFollowersIDsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2791
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$77;

    const/16 v2, 0x1e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$77;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2797
    return-void
.end method

.method public getFollowersIDsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 2765
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$76;

    const/16 v2, 0x1e

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$76;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2771
    return-void
.end method

.method public getFollowersStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1924
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$49;

    const/4 v2, 0x7

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$49;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1930
    return-void
.end method

.method public getFollowersStatusesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1877
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$48;

    const/4 v2, 0x7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$48;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1883
    return-void
.end method

.method public getFollowersStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1814
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$47;

    const/4 v2, 0x7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$47;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1820
    return-void
.end method

.method public getFollowersStatusesAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 1774
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$46;

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$46;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1780
    return-void
.end method

.method public getFriendsAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1648
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1649
    return-void
.end method

.method public getFriendsAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1743
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, p1, v0, p3}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1744
    return-void
.end method

.method public getFriendsAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702
    invoke-virtual {p0, p1, p2, p3}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1703
    return-void
.end method

.method public getFriendsAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1664
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 1665
    return-void
.end method

.method public getFriendsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1611
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1612
    return-void
.end method

.method public getFriendsAsync(Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1576
    invoke-virtual {p0, p1}, Lweibo4android/AsyncWeibo;->getFriendsStatusesAsync(Lweibo4android/WeiboListener;)V

    .line 1577
    return-void
.end method

.method public getFriendsIDsAsync(IJLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2669
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$72;

    const/16 v2, 0x1d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$72;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2675
    return-void
.end method

.method public getFriendsIDsAsync(ILweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2643
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$71;

    const/16 v2, 0x1d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$71;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2649
    return-void
.end method

.method public getFriendsIDsAsync(ILweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2614
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$70;

    const/16 v2, 0x1d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$70;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2620
    return-void
.end method

.method public getFriendsIDsAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2592
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$69;

    const/16 v2, 0x1d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$69;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2598
    return-void
.end method

.method public getFriendsIDsAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 2746
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$75;

    const/16 v2, 0x1d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$75;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2752
    return-void
.end method

.method public getFriendsIDsAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2720
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$74;

    const/16 v2, 0x1d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$74;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2726
    return-void
.end method

.method public getFriendsIDsAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2691
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$73;

    const/16 v2, 0x1d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$73;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2697
    return-void
.end method

.method public getFriendsIDsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2569
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$68;

    const/16 v2, 0x1d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$68;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2575
    return-void
.end method

.method public getFriendsIDsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 2543
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$67;

    const/16 v2, 0x1d

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$67;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2549
    return-void
.end method

.method public getFriendsStatusesAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1719
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$45;

    const/4 v2, 0x6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$45;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1725
    return-void
.end method

.method public getFriendsStatusesAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1679
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$44;

    const/4 v2, 0x6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$44;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1685
    return-void
.end method

.method public getFriendsStatusesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1626
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$43;

    const/4 v2, 0x6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$43;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1632
    return-void
.end method

.method public getFriendsStatusesAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 1589
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$42;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$42;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1595
    return-void
.end method

.method public getFriendsTimelineAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 373
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getFriendsTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 374
    return-void
.end method

.method public getFriendsTimelineAsync(JILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 396
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p3, p1, p2}, Lweibo4android/Paging;-><init>(IJ)V

    invoke-virtual {p0, v0, p4}, Lweibo4android/AsyncWeibo;->getFriendsTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 397
    return-void
.end method

.method public getFriendsTimelineAsync(JLjava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 500
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(JLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 544
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1, p2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0, p3}, Lweibo4android/AsyncWeibo;->getFriendsTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 545
    return-void
.end method

.method public getFriendsTimelineAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 476
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 585
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(Ljava/lang/String;Ljava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 564
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 435
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 414
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFriendsTimelineAsync(Ljava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 519
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$11;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v4, p2, v2}, Lweibo4android/AsyncWeibo$11;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 525
    return-void
.end method

.method public getFriendsTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 329
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$10;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v4, p2, v2}, Lweibo4android/AsyncWeibo$10;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 335
    return-void
.end method

.method public getFriendsTimelineAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 304
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$9;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$9;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 310
    return-void
.end method

.method public getFriendsTimelineByPageAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 353
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getFriendsTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 354
    return-void
.end method

.method public getFriendsTimelineByPageAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 455
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Weibo API is not supporting this method anymore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getHomeTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 285
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$8;

    const/16 v2, 0x33

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$8;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 291
    return-void
.end method

.method public getHomeTimelineAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 263
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$7;

    const/16 v2, 0x33

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$7;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 269
    return-void
.end method

.method public getMentionsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 917
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$21;

    const/16 v2, 0x25

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$21;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 923
    return-void
.end method

.method public getMentionsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 892
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$20;

    const/16 v2, 0x25

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$20;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 898
    return-void
.end method

.method public getPublicTimelineAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->getPublicTimelineAsync(JLweibo4android/WeiboListener;)V

    .line 229
    return-void
.end method

.method public getPublicTimelineAsync(JLweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 243
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$6;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {v1, p0, v4, p3, v2}, Lweibo4android/AsyncWeibo$6;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 249
    return-void
.end method

.method public getPublicTimelineAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 205
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$5;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$5;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 211
    return-void
.end method

.method public getRepliesAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1105
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getMentionsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1106
    return-void
.end method

.method public getRepliesAsync(JILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1131
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p3, p1, p2}, Lweibo4android/Paging;-><init>(IJ)V

    invoke-virtual {p0, v0, p4}, Lweibo4android/AsyncWeibo;->getMentionsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1132
    return-void
.end method

.method public getRepliesAsync(JLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1062
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1, p2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0, p3}, Lweibo4android/AsyncWeibo;->getMentionsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1063
    return-void
.end method

.method public getRepliesAsync(Lweibo4android/WeiboListener;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 869
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$19;

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$19;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 875
    return-void
.end method

.method public getRepliesByPageAsync(ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1082
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1}, Lweibo4android/Paging;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getMentionsAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 1083
    return-void
.end method

.method public getRetweetedByMeAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 953
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$23;

    const/16 v2, 0x35

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$23;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 959
    return-void
.end method

.method public getRetweetedByMeAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 933
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$22;

    const/16 v2, 0x35

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$22;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 939
    return-void
.end method

.method public getRetweetedToMeAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 993
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$25;

    const/16 v2, 0x36

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$25;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 999
    return-void
.end method

.method public getRetweetedToMeAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 972
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$24;

    const/16 v2, 0x36

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$24;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 978
    return-void
.end method

.method public getRetweetsOfMeAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1033
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$27;

    const/16 v2, 0x37

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$27;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1039
    return-void
.end method

.method public getRetweetsOfMeAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 1012
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$26;

    const/16 v2, 0x37

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$26;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1018
    return-void
.end method

.method public getSentDirectMessagesAsync(IILweibo4android/WeiboListener;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2178
    new-instance v0, Lweibo4android/Paging;

    int-to-long v1, p2

    invoke-direct {v0, p1, v1, v2}, Lweibo4android/Paging;-><init>(IJ)V

    invoke-virtual {p0, v0, p3}, Lweibo4android/AsyncWeibo;->getSentDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 2179
    return-void
.end method

.method public getSentDirectMessagesAsync(ILweibo4android/WeiboListener;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2157
    new-instance v0, Lweibo4android/Paging;

    int-to-long v1, p1

    invoke-direct {v0, v1, v2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0, p2}, Lweibo4android/AsyncWeibo;->getSentDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 2158
    return-void
.end method

.method public getSentDirectMessagesAsync(Ljava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2134
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$56;

    const/16 v2, 0xa

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$56;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2140
    return-void
.end method

.method public getSentDirectMessagesAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2113
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$55;

    const/16 v2, 0xa

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$55;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2119
    return-void
.end method

.method public getSentDirectMessagesAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 2093
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$54;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$54;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2099
    return-void
.end method

.method public getUserDetailAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1539
    invoke-virtual {p0, p1, p2}, Lweibo4android/AsyncWeibo;->showUserAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 1540
    return-void
.end method

.method public getUserTimelineAsync(IJLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 770
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2, p3}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {v0, p1}, Lweibo4android/Paging;->count(I)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lweibo4android/AsyncWeibo;->getUserTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 771
    return-void
.end method

.method public getUserTimelineAsync(ILjava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 722
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$15;

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-direct {v1, p0, v5, p3, v2}, Lweibo4android/AsyncWeibo$15;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 728
    return-void
.end method

.method public getUserTimelineAsync(JLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 850
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p1, p2}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, v0, p3}, Lweibo4android/AsyncWeibo;->getUserTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 851
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;IJLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 659
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2, p3, p4}, Lweibo4android/Paging;-><init>(IJ)V

    invoke-virtual {p0, p1, v0, p5}, Lweibo4android/AsyncWeibo;->getUserTimelineAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 660
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;ILjava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 606
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$12;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    aput-object p3, v2, v5

    invoke-direct {v1, p0, v5, p4, v2}, Lweibo4android/AsyncWeibo$12;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 612
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;ILweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 703
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0}, Lweibo4android/Paging;-><init>()V

    invoke-virtual {v0, p2}, Lweibo4android/Paging;->count(I)Lweibo4android/Paging;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lweibo4android/AsyncWeibo;->getUserTimelineAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 704
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 812
    new-instance v0, Lweibo4android/Paging;

    invoke-direct {v0, p2, p3}, Lweibo4android/Paging;-><init>(J)V

    invoke-virtual {p0, p1, v0, p4}, Lweibo4android/AsyncWeibo;->getUserTimelineAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V

    .line 813
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;Ljava/util/Date;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 678
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$14;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-direct {v1, p0, v4, p3, v2}, Lweibo4android/AsyncWeibo$14;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 684
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 630
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$13;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-direct {v1, p0, v4, p3, v2}, Lweibo4android/AsyncWeibo$13;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 636
    return-void
.end method

.method public getUserTimelineAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 785
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$17;

    const/4 v2, 0x2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$17;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 791
    return-void
.end method

.method public getUserTimelineAsync(Lweibo4android/Paging;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 743
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$16;

    const/4 v2, 0x2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$16;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 749
    return-void
.end method

.method public getUserTimelineAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 825
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$18;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$18;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 831
    return-void
.end method

.method public getWeeklyTrendsAsync(Ljava/util/Date;ZLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 185
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$4;

    const/16 v2, 0x2f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$4;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 191
    return-void
.end method

.method public getWeeklyTrendsAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 163
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$3;

    const/16 v2, 0x2f

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$3;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 169
    return-void
.end method

.method public leaveAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3616
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$103;

    const/16 v2, 0xf

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$103;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3622
    return-void
.end method

.method public leaveAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3575
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$101;

    const/16 v2, 0xf

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$101;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3581
    return-void
.end method

.method public rateLimitStatusAsync(Lweibo4android/WeiboListener;)V
    .locals 4

    .prologue
    .line 3064
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$87;

    const/16 v2, 0x1c

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v1, p0, v2, p1, v3}, Lweibo4android/AsyncWeibo$87;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3070
    return-void
.end method

.method public retweetStatusAsync(J)V
    .locals 1

    .prologue
    .line 1520
    new-instance v0, Lweibo4android/WeiboAdapter;

    invoke-direct {v0}, Lweibo4android/WeiboAdapter;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lweibo4android/AsyncWeibo;->retweetStatusAsync(JLweibo4android/WeiboListener;)V

    .line 1521
    return-void
.end method

.method public retweetStatusAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 1501
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$40;

    const/16 v2, 0x34

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$40;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1507
    return-void
.end method

.method public sendDirectMessageAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2219
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$58;

    const/16 v2, 0xb

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$58;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2225
    return-void
.end method

.method public sendDirectMessageAsync(Ljava/lang/String;Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 2197
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$57;

    const/16 v2, 0xb

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$57;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2203
    return-void
.end method

.method public showAsync(ILweibo4android/WeiboListener;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1148
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1, p2}, Lweibo4android/AsyncWeibo;->showAsync(JLweibo4android/WeiboListener;)V

    .line 1149
    return-void
.end method

.method public showAsync(JLweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1166
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$28;

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$28;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1172
    return-void
.end method

.method public showStatusAsync(JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 1187
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$29;

    const/16 v2, 0x26

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p3, v3}, Lweibo4android/AsyncWeibo$29;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1193
    return-void
.end method

.method public showUserAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1557
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$41;

    const/16 v2, 0x9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$41;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1563
    return-void
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 3856
    const-class v1, Lweibo4android/AsyncWeibo;

    monitor-enter v1

    .line 3857
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lweibo4android/AsyncWeibo;->shutdown:Z

    .line 3858
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Already shut down"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3863
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public testAsync()V
    .locals 5

    .prologue
    .line 3839
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$112;

    const/16 v2, 0x18

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$112;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3845
    return-void
.end method

.method public unblockAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3711
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$106;

    const/16 v2, 0x17

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$106;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3717
    return-void
.end method

.method public updateAsync(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1228
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$31;

    const/4 v2, 0x4

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$31;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1234
    return-void
.end method

.method public updateAsync(Ljava/lang/String;J)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1322
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$35;

    const/4 v2, 0x4

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$35;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1328
    return-void
.end method

.method public updateAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1295
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$34;

    const/4 v2, 0x4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$34;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1301
    return-void
.end method

.method public updateAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1209
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$30;

    const/4 v2, 0x4

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$30;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1215
    return-void
.end method

.method public updateLocationAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2990
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$85;

    const/16 v2, 0x14

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$85;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2996
    return-void
.end method

.method public updateProfileAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 3048
    new-instance v6, Lweibo4android/WeiboAdapter;

    invoke-direct {v6}, Lweibo4android/WeiboAdapter;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lweibo4android/AsyncWeibo;->updateProfileAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lweibo4android/WeiboListener;)V

    .line 3049
    return-void
.end method

.method public updateProfileAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 3020
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$86;

    const/16 v2, 0x29

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    const/4 v4, 0x3

    aput-object p4, v3, v4

    const/4 v4, 0x4

    aput-object p5, v3, v4

    invoke-direct {v1, p0, v2, p6, v3}, Lweibo4android/AsyncWeibo$86;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 3026
    return-void
.end method

.method public updateStatusAsync(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1266
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$33;

    const/16 v2, 0x27

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$33;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1272
    return-void
.end method

.method public updateStatusAsync(Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 1372
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$37;

    const/16 v2, 0x27

    new-instance v3, Lweibo4android/WeiboAdapter;

    invoke-direct {v3}, Lweibo4android/WeiboAdapter;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, p0, v2, v3, v4}, Lweibo4android/AsyncWeibo$37;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1378
    return-void
.end method

.method public updateStatusAsync(Ljava/lang/String;JLweibo4android/WeiboListener;)V
    .locals 6

    .prologue
    .line 1348
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$36;

    const/16 v2, 0x27

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, p4, v3}, Lweibo4android/AsyncWeibo$36;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1354
    return-void
.end method

.method public updateStatusAsync(Ljava/lang/String;Lweibo4android/WeiboListener;)V
    .locals 5

    .prologue
    .line 1248
    invoke-direct {p0}, Lweibo4android/AsyncWeibo;->getDispatcher()Lweibo4android/Dispatcher;

    move-result-object v0

    new-instance v1, Lweibo4android/AsyncWeibo$32;

    const/16 v2, 0x27

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v2, p2, v3}, Lweibo4android/AsyncWeibo$32;-><init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lweibo4android/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1254
    return-void
.end method
