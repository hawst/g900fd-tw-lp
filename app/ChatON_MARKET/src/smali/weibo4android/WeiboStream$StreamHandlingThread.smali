.class abstract Lweibo4android/WeiboStream$StreamHandlingThread;
.super Ljava/lang/Thread;
.source "WeiboStream.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "Weibo Stream Handling Thread"


# instance fields
.field args:[Ljava/lang/Object;

.field private closed:Z

.field private retryHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field stream:Lweibo4android/StatusStream;

.field final synthetic this$0:Lweibo4android/WeiboStream;


# direct methods
.method constructor <init>(Lweibo4android/WeiboStream;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 580
    iput-object p1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    .line 581
    const-string v0, "Weibo Stream Handling Thread[initializing]"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 574
    const/4 v0, 0x0

    iput-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    .line 578
    const/4 v0, 0x0

    iput-boolean v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->closed:Z

    .line 582
    iput-object p2, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->args:[Ljava/lang/Object;

    .line 583
    new-instance v0, Ljava/util/ArrayList;

    # getter for: Lweibo4android/WeiboStream;->retryPerMinutes:I
    invoke-static {p1}, Lweibo4android/WeiboStream;->access$000(Lweibo4android/WeiboStream;)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    .line 584
    return-void
.end method

.method private setStatus(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 645
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Weibo Stream Handling Thread"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 646
    invoke-virtual {p0, v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->setName(Ljava/lang/String;)V

    .line 647
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # invokes: Lweibo4android/WeiboStream;->log(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lweibo4android/WeiboStream;->access$300(Lweibo4android/WeiboStream;Ljava/lang/String;)V

    .line 648
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 637
    monitor-enter p0

    :try_start_0
    const-string v0, "[disposing thread]"

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->setStatus(Ljava/lang/String;)V

    .line 638
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    invoke-virtual {v0}, Lweibo4android/StatusStream;->close()V

    .line 640
    const/4 v0, 0x1

    iput-boolean v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 642
    :cond_0
    monitor-exit p0

    return-void

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method abstract getStream()Lweibo4android/StatusStream;
.end method

.method public run()V
    .locals 7

    .prologue
    const-wide/32 v5, 0xea60

    .line 589
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->closed:Z

    if-nez v0, :cond_6

    .line 592
    :try_start_0
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 593
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v0, v1, v3

    cmp-long v0, v0, v5

    if-lez v0, :cond_1

    .line 594
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 597
    :cond_1
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # getter for: Lweibo4android/WeiboStream;->retryPerMinutes:I
    invoke-static {v1}, Lweibo4android/WeiboStream;->access$000(Lweibo4android/WeiboStream;)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 599
    const-string v0, "[establishing connection]"

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->setStatus(Ljava/lang/String;)V

    .line 601
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->closed:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    if-nez v0, :cond_4

    .line 602
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # getter for: Lweibo4android/WeiboStream;->retryPerMinutes:I
    invoke-static {v1}, Lweibo4android/WeiboStream;->access$000(Lweibo4android/WeiboStream;)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 603
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 604
    invoke-virtual {p0}, Lweibo4android/WeiboStream$StreamHandlingThread;->getStream()Lweibo4android/StatusStream;

    move-result-object v0

    iput-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 627
    :catch_0
    move-exception v0

    .line 628
    const/4 v1, 0x0

    iput-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    .line 629
    invoke-virtual {v0}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 630
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    invoke-virtual {v0}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;

    move-result-object v2

    # invokes: Lweibo4android/WeiboStream;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lweibo4android/WeiboStream;->access$300(Lweibo4android/WeiboStream;Ljava/lang/String;)V

    .line 631
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # getter for: Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;
    invoke-static {v1}, Lweibo4android/WeiboStream;->access$200(Lweibo4android/WeiboStream;)Lweibo4android/StatusListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lweibo4android/StatusListener;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 609
    :cond_3
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    iget-object v3, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->retryHistory:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v0, v1, v3

    sub-long v0, v5, v0

    .line 610
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[retry limit reached. sleeping for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v3, 0x3e8

    div-long v3, v0, v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " secs]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lweibo4android/WeiboStream$StreamHandlingThread;->setStatus(Ljava/lang/String;)V
    :try_end_1
    .catch Lweibo4android/WeiboException; {:try_start_1 .. :try_end_1} :catch_0

    .line 612
    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lweibo4android/WeiboException; {:try_start_2 .. :try_end_2} :catch_0

    .line 617
    :cond_4
    :goto_2
    :try_start_3
    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "[receiving stream]"

    invoke-direct {p0, v0}, Lweibo4android/WeiboStream$StreamHandlingThread;->setStatus(Ljava/lang/String;)V

    .line 620
    :cond_5
    :goto_3
    iget-boolean v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->closed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->stream:Lweibo4android/StatusStream;

    invoke-virtual {v0}, Lweibo4android/StatusStream;->next()Lweibo4android/Status;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 621
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    const-string v2, "received:"

    invoke-virtual {v0}, Lweibo4android/Status;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lweibo4android/WeiboStream;->log(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lweibo4android/WeiboStream;->access$100(Lweibo4android/WeiboStream;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # getter for: Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;
    invoke-static {v1}, Lweibo4android/WeiboStream;->access$200(Lweibo4android/WeiboStream;)Lweibo4android/StatusListener;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 623
    iget-object v1, p0, Lweibo4android/WeiboStream$StreamHandlingThread;->this$0:Lweibo4android/WeiboStream;

    # getter for: Lweibo4android/WeiboStream;->statusListener:Lweibo4android/StatusListener;
    invoke-static {v1}, Lweibo4android/WeiboStream;->access$200(Lweibo4android/WeiboStream;)Lweibo4android/StatusListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lweibo4android/StatusListener;->onStatus(Lweibo4android/Status;)V
    :try_end_3
    .catch Lweibo4android/WeiboException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 613
    :catch_1
    move-exception v0

    goto :goto_2

    .line 634
    :cond_6
    return-void
.end method
