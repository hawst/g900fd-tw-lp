.class public Lweibo4android/ListUserCount;
.super Lweibo4android/WeiboResponse;
.source "ListUserCount.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x249e88f8d62e1119L


# instance fields
.field private listCount:I

.field private listedCount:I

.field private subscriberCount:I


# direct methods
.method public constructor <init>(Lweibo4android/http/Response;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 68
    invoke-virtual {p1}, Lweibo4android/http/Response;->asDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 69
    const-string v1, "count"

    invoke-static {v1, v0}, Lweibo4android/ListUserCount;->ensureRootNodeNameIs(Ljava/lang/String;Lorg/w3c/dom/Element;)V

    .line 70
    const-string v1, "lists"

    invoke-static {v1, v0}, Lweibo4android/ListUserCount;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/ListUserCount;->listCount:I

    .line 71
    const-string v1, "subscriptions"

    invoke-static {v1, v0}, Lweibo4android/ListUserCount;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v1

    iput v1, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    .line 72
    const-string v1, "listed"

    invoke-static {v1, v0}, Lweibo4android/ListUserCount;->getChildInt(Ljava/lang/String;Lorg/w3c/dom/Element;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListUserCount;->listedCount:I

    .line 73
    return-void
.end method

.method public constructor <init>(Lweibo4android/org/json/JSONObject;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lweibo4android/WeiboResponse;-><init>()V

    .line 53
    const-string v0, "lists"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListUserCount;->listCount:I

    .line 54
    const-string v0, "subscriptions"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    .line 55
    const-string v0, "listed"

    invoke-virtual {p1, v0}, Lweibo4android/org/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lweibo4android/ListUserCount;->listedCount:I

    .line 56
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    if-nez p1, :cond_1

    move v0, v1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    if-eq p0, p1, :cond_0

    .line 88
    instance-of v2, p1, Lweibo4android/ListUserCount;

    if-eqz v2, :cond_2

    check-cast p1, Lweibo4android/ListUserCount;

    invoke-virtual {p1}, Lweibo4android/ListUserCount;->hashCode()I

    move-result v2

    invoke-virtual {p0}, Lweibo4android/ListUserCount;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getListCount()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lweibo4android/ListUserCount;->listCount:I

    return v0
.end method

.method public getListedCount()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lweibo4android/ListUserCount;->listedCount:I

    return v0
.end method

.method public getSubscriberCount()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lweibo4android/ListUserCount;->listCount:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iget v1, p0, Lweibo4android/ListUserCount;->listedCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public setListCount(I)V
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lweibo4android/ListUserCount;->listCount:I

    .line 97
    return-void
.end method

.method public setListedCount(I)V
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lweibo4android/ListUserCount;->listedCount:I

    .line 113
    return-void
.end method

.method public setSubscriberCount(I)V
    .locals 0

    .prologue
    .line 104
    iput p1, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    .line 105
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ListUserCount{listCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/ListUserCount;->listCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscriberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/ListUserCount;->subscriberCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", listedCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lweibo4android/ListUserCount;->listedCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
