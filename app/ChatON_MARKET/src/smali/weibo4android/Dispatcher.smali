.class Lweibo4android/Dispatcher;
.super Ljava/lang/Object;
.source "Dispatcher.java"


# instance fields
.field private active:Z

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private threads:[Lweibo4android/ExecuteThread;

.field ticket:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lweibo4android/Dispatcher;-><init>(Ljava/lang/String;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    .line 90
    iput-boolean v3, p0, Lweibo4android/Dispatcher;->active:Z

    .line 43
    new-array v0, p2, [Lweibo4android/ExecuteThread;

    iput-object v0, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    .line 44
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 45
    iget-object v1, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    new-instance v2, Lweibo4android/ExecuteThread;

    invoke-direct {v2, p1, p0, v0}, Lweibo4android/ExecuteThread;-><init>(Ljava/lang/String;Lweibo4android/Dispatcher;I)V

    aput-object v2, v1, v0

    .line 46
    iget-object v1, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lweibo4android/ExecuteThread;->setDaemon(Z)V

    .line 47
    iget-object v1, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lweibo4android/ExecuteThread;->start()V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    new-instance v1, Lweibo4android/Dispatcher$1;

    invoke-direct {v1, p0}, Lweibo4android/Dispatcher$1;-><init>(Lweibo4android/Dispatcher;)V

    invoke-virtual {v0, v1}, Ljava/lang/Runtime;->addShutdownHook(Ljava/lang/Thread;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lweibo4android/Dispatcher;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lweibo4android/Dispatcher;->active:Z

    return v0
.end method


# virtual methods
.method public declared-synchronized invokeLater(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 61
    :try_start_1
    iget-object v0, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :try_start_2
    iget-object v1, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 64
    :try_start_3
    iget-object v0, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 65
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 66
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 60
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 65
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1
.end method

.method public poll()Ljava/lang/Runnable;
    .locals 3

    .prologue
    .line 71
    :goto_0
    iget-boolean v0, p0, Lweibo4android/Dispatcher;->active:Z

    if-eqz v0, :cond_1

    .line 72
    iget-object v1, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 74
    iget-object v0, p0, Lweibo4android/Dispatcher;->q:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    monitor-exit v1

    .line 87
    :goto_1
    return-object v0

    .line 79
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 80
    iget-object v1, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    :try_start_1
    iget-object v0, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :goto_2
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 79
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 83
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public declared-synchronized shutdown()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 93
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lweibo4android/Dispatcher;->active:Z

    if-eqz v1, :cond_1

    .line 94
    const/4 v1, 0x0

    iput-boolean v1, p0, Lweibo4android/Dispatcher;->active:Z

    .line 95
    iget-object v1, p0, Lweibo4android/Dispatcher;->threads:[Lweibo4android/ExecuteThread;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 96
    invoke-virtual {v3}, Lweibo4android/ExecuteThread;->shutdown()V

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 98
    :cond_0
    iget-object v1, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 99
    :try_start_1
    iget-object v0, p0, Lweibo4android/Dispatcher;->ticket:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 100
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 100
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 93
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_1
    :try_start_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already shutdown"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
.end method
