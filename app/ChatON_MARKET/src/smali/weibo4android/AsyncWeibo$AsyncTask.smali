.class abstract Lweibo4android/AsyncWeibo$AsyncTask;
.super Ljava/lang/Object;
.source "AsyncWeibo.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field args:[Ljava/lang/Object;

.field listener:Lweibo4android/WeiboListener;

.field method:I

.field final synthetic this$0:Lweibo4android/AsyncWeibo;


# direct methods
.method constructor <init>(Lweibo4android/AsyncWeibo;ILweibo4android/WeiboListener;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3907
    iput-object p1, p0, Lweibo4android/AsyncWeibo$AsyncTask;->this$0:Lweibo4android/AsyncWeibo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3908
    iput p2, p0, Lweibo4android/AsyncWeibo$AsyncTask;->method:I

    .line 3909
    iput-object p3, p0, Lweibo4android/AsyncWeibo$AsyncTask;->listener:Lweibo4android/WeiboListener;

    .line 3910
    iput-object p4, p0, Lweibo4android/AsyncWeibo$AsyncTask;->args:[Ljava/lang/Object;

    .line 3911
    return-void
.end method


# virtual methods
.method abstract invoke(Lweibo4android/WeiboListener;[Ljava/lang/Object;)V
.end method

.method public run()V
    .locals 3

    .prologue
    .line 3918
    :try_start_0
    iget-object v0, p0, Lweibo4android/AsyncWeibo$AsyncTask;->listener:Lweibo4android/WeiboListener;

    iget-object v1, p0, Lweibo4android/AsyncWeibo$AsyncTask;->args:[Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lweibo4android/AsyncWeibo$AsyncTask;->invoke(Lweibo4android/WeiboListener;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3924
    :cond_0
    :goto_0
    return-void

    .line 3919
    :catch_0
    move-exception v0

    .line 3920
    iget-object v1, p0, Lweibo4android/AsyncWeibo$AsyncTask;->listener:Lweibo4android/WeiboListener;

    if-eqz v1, :cond_0

    .line 3921
    iget-object v1, p0, Lweibo4android/AsyncWeibo$AsyncTask;->listener:Lweibo4android/WeiboListener;

    iget v2, p0, Lweibo4android/AsyncWeibo$AsyncTask;->method:I

    invoke-interface {v1, v0, v2}, Lweibo4android/WeiboListener;->onException(Lweibo4android/WeiboException;I)V

    goto :goto_0
.end method
