.class public Lorg/a/a/a/a/j;
.super Lorg/a/a/a/a;
.source "GenericKeyedObjectPool.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/a/a/a/a",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private volatile b:I

.field private c:I

.field private d:I

.field private e:J

.field private f:B

.field private volatile g:Z

.field private volatile h:Z

.field private i:Z

.field private j:J

.field private k:I

.field private l:J

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Lorg/a/a/a/a/j",
            "<TK;TV;>.org/a/a/a/a/n;>;"
        }
    .end annotation
.end field

.field private n:I

.field private o:I

.field private p:I

.field private q:Lorg/a/a/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/e",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private r:Lorg/a/a/a/a/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/j",
            "<TK;TV;>.org/a/a/a/a/l;"
        }
    .end annotation
.end field

.field private s:Lorg/a/a/a/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/a",
            "<TK;>;"
        }
    .end annotation
.end field

.field private t:Lorg/a/a/a/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/a",
            "<",
            "Lorg/a/a/a/a/o",
            "<TV;>;>.org/a/a/a/a/b;"
        }
    .end annotation
.end field

.field private u:Lorg/a/a/a/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/a",
            "<TK;>.org/a/a/a/a/b;"
        }
    .end annotation
.end field

.field private v:Z

.field private w:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/a/a/a/a/j",
            "<TK;TV;>.org/a/a/a/a/m<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 15

    .prologue
    const-wide/16 v4, -0x1

    const/16 v2, 0x8

    const/4 v7, 0x0

    .line 364
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    move-object v0, p0

    move v6, v2

    move v8, v7

    move-wide v9, v4

    move v14, v7

    invoke-direct/range {v0 .. v14}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJIZZJIJZ)V

    .line 367
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/e;IBJ)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/e",
            "<TK;TV;>;IBJ)V"
        }
    .end annotation

    .prologue
    .line 415
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v14}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJIZZJIJZ)V

    .line 418
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/e;IBJIIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/e",
            "<TK;TV;>;IBJIIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 575
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move-wide/from16 v11, p11

    move/from16 v13, p13

    move-wide/from16 v14, p14

    move/from16 v16, p16

    invoke-direct/range {v0 .. v17}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJIIIZZJIJZZ)V

    .line 578
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/e;IBJIIIZZJIJZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/e",
            "<TK;TV;>;IBJIIIZZJIJZZ)V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-direct {p0}, Lorg/a/a/a/a;-><init>()V

    .line 2571
    const/16 v2, 0x8

    iput v2, p0, Lorg/a/a/a/a/j;->a:I

    .line 2578
    const/4 v2, 0x0

    iput v2, p0, Lorg/a/a/a/a/j;->b:I

    .line 2585
    const/16 v2, 0x8

    iput v2, p0, Lorg/a/a/a/a/j;->c:I

    .line 2592
    const/4 v2, -0x1

    iput v2, p0, Lorg/a/a/a/a/j;->d:I

    .line 2610
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/a/a/a/a/j;->e:J

    .line 2624
    const/4 v2, 0x1

    iput-byte v2, p0, Lorg/a/a/a/a/j;->f:B

    .line 2637
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/a/a/a/a/j;->g:Z

    .line 2648
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/a/a/a/a/j;->h:Z

    .line 2661
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/a/a/a/a/j;->i:Z

    .line 2672
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/a/a/a/a/j;->j:J

    .line 2687
    const/4 v2, 0x3

    iput v2, p0, Lorg/a/a/a/a/j;->k:I

    .line 2701
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/a/a/a/a/j;->l:J

    .line 2704
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    .line 2707
    const/4 v2, 0x0

    iput v2, p0, Lorg/a/a/a/a/j;->n:I

    .line 2710
    const/4 v2, 0x0

    iput v2, p0, Lorg/a/a/a/a/j;->o:I

    .line 2717
    const/4 v2, 0x0

    iput v2, p0, Lorg/a/a/a/a/j;->p:I

    .line 2720
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    .line 2725
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    .line 2731
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    .line 2734
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    .line 2737
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    .line 2740
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/a/a/a/a/j;->v:Z

    .line 2747
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    .line 611
    iput-object p1, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    .line 612
    iput p2, p0, Lorg/a/a/a/a/j;->c:I

    .line 613
    move/from16 v0, p17

    iput-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    .line 614
    packed-switch p3, :pswitch_data_0

    .line 621
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "whenExhaustedAction "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not recognized."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 618
    :pswitch_0
    iput-byte p3, p0, Lorg/a/a/a/a/j;->f:B

    .line 623
    iput-wide p4, p0, Lorg/a/a/a/a/j;->e:J

    .line 624
    iput p6, p0, Lorg/a/a/a/a/j;->a:I

    .line 625
    iput p7, p0, Lorg/a/a/a/a/j;->d:I

    .line 626
    iput p8, p0, Lorg/a/a/a/a/j;->b:I

    .line 627
    iput-boolean p9, p0, Lorg/a/a/a/a/j;->g:Z

    .line 628
    iput-boolean p10, p0, Lorg/a/a/a/a/j;->h:Z

    .line 629
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lorg/a/a/a/a/j;->j:J

    .line 630
    move/from16 v0, p13

    iput v0, p0, Lorg/a/a/a/a/j;->k:I

    .line 631
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lorg/a/a/a/a/j;->l:J

    .line 632
    move/from16 v0, p16

    iput-boolean v0, p0, Lorg/a/a/a/a/j;->i:Z

    .line 634
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    .line 635
    new-instance v2, Lorg/a/a/a/a/a;

    invoke-direct {v2}, Lorg/a/a/a/a/a;-><init>()V

    iput-object v2, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    .line 637
    iget-wide v2, p0, Lorg/a/a/a/a/j;->j:J

    invoke-virtual {p0, v2, v3}, Lorg/a/a/a/a/j;->a(J)V

    .line 638
    return-void

    .line 614
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lorg/a/a/a/e;IBJIIZZJIJZ)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/e",
            "<TK;TV;>;IBJIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 540
    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-wide/from16 v11, p10

    move/from16 v13, p12

    move-wide/from16 v14, p13

    move/from16 v16, p15

    invoke-direct/range {v0 .. v16}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJIIIZZJIJZ)V

    .line 543
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/e;IBJIZZJIJZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/e",
            "<TK;TV;>;IBJIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 507
    const/4 v7, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-wide/from16 v13, p12

    move/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJIIZZJIJZ)V

    .line 510
    return-void
.end method

.method static synthetic a(Lorg/a/a/a/a/j;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/a/a/a/a/j;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/a/a/a/a/j;->n:I

    return v0
.end method

.method private declared-synchronized a(Lorg/a/a/a/a/n;Z)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/j",
            "<TK;TV;>.org/a/a/a/a/n;Z)I"
        }
    .end annotation

    .prologue
    .line 2204
    monitor-enter p0

    .line 2208
    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->i()I

    move-result v0

    invoke-static {p1}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2209
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->g()I

    move-result v1

    if-lez v1, :cond_0

    .line 2210
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/a/a/a/a/j;->g()I

    move-result v2

    invoke-static {p1}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {p1}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v3

    invoke-virtual {v3}, Lorg/a/a/a/a/a;->size()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {p1}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2211
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2215
    :cond_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->h()I

    move-result v1

    if-lez v1, :cond_1

    .line 2216
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/a/a/a/a/j;->h()I

    move-result v2

    invoke-virtual {p0}, Lorg/a/a/a/a/j;->b()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lorg/a/a/a/a/j;->a()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/a/a/a/a/j;->p:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2217
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2220
    :cond_1
    if-eqz p2, :cond_2

    if-lez v0, :cond_2

    .line 2221
    invoke-virtual {p1}, Lorg/a/a/a/a/n;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2223
    :cond_2
    monitor-exit p0

    return v0

    .line 2204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1633
    .line 1634
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->h:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v0, p1, p2}, Lorg/a/a/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v4, v3

    .line 1640
    :goto_0
    if-nez v4, :cond_6

    move v1, v2

    .line 1646
    :goto_1
    monitor-enter p0

    .line 1648
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1650
    if-nez v0, :cond_0

    .line 1651
    new-instance v0, Lorg/a/a/a/a/n;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lorg/a/a/a/a/n;-><init>(Lorg/a/a/a/a/j;Lorg/a/a/a/a/k;)V

    .line 1652
    iget-object v5, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1653
    iget-object v5, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v5, p1}, Lorg/a/a/a/a/a;->add(Ljava/lang/Object;)Z

    .line 1655
    :cond_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->e()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1677
    :cond_1
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678
    if-eqz v3, :cond_2

    .line 1679
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1683
    :cond_2
    if-eqz v2, :cond_4

    .line 1685
    :try_start_1
    iget-object v1, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v1, p1, p2}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1690
    :goto_3
    if-eqz p3, :cond_4

    .line 1691
    monitor-enter p0

    .line 1692
    :try_start_2
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->b()V

    .line 1693
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v0

    if-nez v0, :cond_3

    .line 1696
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1697
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1699
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1700
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1703
    :cond_4
    return-void

    .line 1637
    :cond_5
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v0, p1, p2}, Lorg/a/a/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move v4, v2

    goto :goto_0

    :cond_6
    move v1, v3

    .line 1640
    goto :goto_1

    .line 1660
    :cond_7
    :try_start_3
    iget v5, p0, Lorg/a/a/a/a/j;->a:I

    if-ltz v5, :cond_8

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v5

    invoke-virtual {v5}, Lorg/a/a/a/a/a;->size()I

    move-result v5

    iget v6, p0, Lorg/a/a/a/a/j;->a:I

    if-ge v5, v6, :cond_1

    .line 1662
    :cond_8
    if-eqz v4, :cond_b

    .line 1665
    iget-boolean v3, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v3, :cond_a

    .line 1666
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v3

    new-instance v4, Lorg/a/a/a/a/o;

    invoke-direct {v4, p2}, Lorg/a/a/a/a/o;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Lorg/a/a/a/a/a;->a(Ljava/lang/Object;)Z

    .line 1670
    :goto_4
    iget v3, p0, Lorg/a/a/a/a/j;->o:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/a/a/a/a/j;->o:I

    .line 1671
    if-eqz p3, :cond_9

    .line 1672
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->b()V

    :cond_9
    move v3, v2

    move v2, v1

    .line 1674
    goto :goto_2

    .line 1668
    :cond_a
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v3

    new-instance v4, Lorg/a/a/a/a/o;

    invoke-direct {v4, p2}, Lorg/a/a/a/a/o;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Lorg/a/a/a/a/a;->b(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1677
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1699
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1686
    :catch_0
    move-exception v1

    goto :goto_3

    :cond_b
    move v2, v1

    goto/16 :goto_2
.end method

.method private a(Ljava/util/Map;Lorg/a/a/a/e;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/List",
            "<",
            "Lorg/a/a/a/a/o",
            "<TV;>;>;>;",
            "Lorg/a/a/a/e",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1488
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1489
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1490
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 1491
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1492
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/o;

    iget-object v0, v0, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {p2, v2, v0}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1499
    monitor-enter p0

    .line 1500
    :try_start_1
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1502
    if-eqz v0, :cond_7

    .line 1503
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 1504
    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1507
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1514
    :goto_2
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    goto :goto_0

    .line 1499
    :catchall_0
    move-exception v0

    move-object v1, v0

    monitor-enter p0

    .line 1500
    :try_start_2
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1502
    if-eqz v0, :cond_6

    .line 1503
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 1504
    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1507
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_2
    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1514
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1499
    throw v1

    .line 1519
    :cond_3
    return-void

    .line 1496
    :catch_0
    move-exception v0

    .line 1499
    monitor-enter p0

    .line 1500
    :try_start_3
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1502
    if-eqz v0, :cond_5

    .line 1503
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 1504
    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1507
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_4
    :goto_4
    monitor-exit p0

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1511
    :cond_5
    :try_start_4
    iget v0, p0, Lorg/a/a/a/a/j;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/j;->p:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    .line 1513
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 1511
    :cond_6
    :try_start_6
    iget v0, p0, Lorg/a/a/a/a/j;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/j;->p:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_3

    .line 1513
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    .line 1511
    :cond_7
    :try_start_8
    iget v0, p0, Lorg/a/a/a/a/j;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/j;->p:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto/16 :goto_1
.end method

.method static synthetic b(Lorg/a/a/a/a/j;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/a/a/a/a/j;->n:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/a/a/a/a/j;->n:I

    return v0
.end method

.method static synthetic c(Lorg/a/a/a/a/j;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/a/a/a/a/j;->p:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/a/a/a/a/j;->p:I

    return v0
.end method

.method private c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2057
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 2058
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->b()V

    .line 2060
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2068
    :cond_1
    :goto_0
    return-void

    .line 2063
    :cond_2
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 2064
    if-eqz v0, :cond_1

    .line 2065
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    .line 2066
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, Lorg/a/a/a/a/a;->a(I)Lorg/a/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic d(Lorg/a/a/a/a/j;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/a/a/a/a/j;->p:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/a/a/a/a/j;->p:I

    return v0
.end method

.method private d(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2111
    monitor-enter p0

    .line 2112
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 2113
    monitor-exit p0

    .line 2114
    if-nez v0, :cond_1

    .line 2135
    :cond_0
    return-void

    .line 2113
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2123
    :cond_1
    invoke-direct {p0, v0, v1}, Lorg/a/a/a/a/j;->a(Lorg/a/a/a/a/n;Z)I

    move-result v2

    .line 2125
    :goto_0
    if-ge v1, v2, :cond_0

    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lorg/a/a/a/a/j;->a(Lorg/a/a/a/a/n;Z)I

    move-result v3

    if-lez v3, :cond_0

    .line 2127
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/a/a/a/a/j;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2129
    monitor-enter p0

    .line 2130
    :try_start_2
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 2131
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 2132
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 2125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2129
    :catchall_1
    move-exception v1

    monitor-enter p0

    .line 2130
    :try_start_3
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 2131
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2132
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 2129
    throw v1

    .line 2131
    :catchall_2
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0
.end method

.method static synthetic e(Lorg/a/a/a/a/j;)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lorg/a/a/a/a/j;->n()V

    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 1282
    const/4 v3, 0x0

    .line 1284
    monitor-enter p0

    .line 1285
    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit p0

    .line 1354
    :cond_0
    :goto_0
    return-void

    .line 1287
    :cond_1
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1289
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1291
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/m;

    .line 1292
    iget-object v1, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-static {v0}, Lorg/a/a/a/a/m;->d(Lorg/a/a/a/a/m;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/a/a/a/a/n;

    .line 1293
    if-nez v1, :cond_8

    .line 1294
    new-instance v1, Lorg/a/a/a/a/n;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lorg/a/a/a/a/n;-><init>(Lorg/a/a/a/a/j;Lorg/a/a/a/a/k;)V

    .line 1295
    iget-object v4, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-static {v0}, Lorg/a/a/a/a/m;->d(Lorg/a/a/a/a/m;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1296
    iget-object v4, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-static {v0}, Lorg/a/a/a/a/m;->d(Lorg/a/a/a/a/m;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/a/a/a/a/a;->add(Ljava/lang/Object;)Z

    move-object v4, v1

    .line 1298
    :goto_2
    invoke-static {v0, v4}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;Lorg/a/a/a/a/n;)V

    .line 1299
    invoke-static {v4}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1300
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1301
    invoke-static {v4}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/a/a/a/a/o;

    invoke-static {v0, v1}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;Lorg/a/a/a/a/o;)V

    .line 1303
    invoke-virtual {v4}, Lorg/a/a/a/a/n;->c()V

    .line 1304
    iget v1, p0, Lorg/a/a/a/a/j;->o:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/a/a/a/a/j;->o:I

    .line 1305
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1306
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1307
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 1341
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1314
    :cond_3
    :try_start_3
    iget v1, p0, Lorg/a/a/a/a/j;->d:I

    if-lez v1, :cond_4

    iget v1, p0, Lorg/a/a/a/a/j;->n:I

    iget v6, p0, Lorg/a/a/a/a/j;->o:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/a/a/a/a/j;->p:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/a/a/a/a/j;->d:I

    if-lt v1, v6, :cond_4

    move v0, v2

    .line 1341
    :goto_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1343
    if-eqz v0, :cond_0

    .line 1352
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->j()V

    goto/16 :goto_0

    .line 1321
    :cond_4
    :try_start_4
    iget v1, p0, Lorg/a/a/a/a/j;->c:I

    if-ltz v1, :cond_5

    invoke-static {v4}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v1

    invoke-static {v4}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v6

    add-int/2addr v1, v6

    iget v6, p0, Lorg/a/a/a/a/j;->c:I

    if-ge v1, v6, :cond_7

    :cond_5
    iget v1, p0, Lorg/a/a/a/a/j;->d:I

    if-ltz v1, :cond_6

    iget v1, p0, Lorg/a/a/a/a/j;->n:I

    iget v6, p0, Lorg/a/a/a/a/j;->o:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/a/a/a/a/j;->p:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/a/a/a/a/j;->d:I

    if-ge v1, v6, :cond_7

    .line 1324
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1325
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;Z)V

    .line 1326
    invoke-virtual {v4}, Lorg/a/a/a/a/n;->c()V

    .line 1327
    monitor-enter v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1328
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1329
    monitor-exit v0

    goto/16 :goto_1

    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v1

    .line 1337
    :cond_7
    iget v0, p0, Lorg/a/a/a/a/j;->c:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-gez v0, :cond_2

    move v0, v3

    .line 1338
    goto :goto_3

    :cond_8
    move-object v4, v1

    goto/16 :goto_2

    :cond_9
    move v0, v3

    goto :goto_3
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2041
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->b()V

    .line 2044
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->a()Lorg/a/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    .line 2045
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_1

    .line 2046
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->b()V

    .line 2047
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    .line 2049
    :cond_1
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 2080
    iget v0, p0, Lorg/a/a/a/a/j;->b:I

    if-lez v0, :cond_0

    .line 2082
    monitor-enter p0

    .line 2084
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 2085
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2092
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Lorg/a/a/a/a/j;->d(Ljava/lang/Object;)V

    .line 2090
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2085
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2095
    :cond_0
    return-void
.end method

.method private declared-synchronized o()I
    .locals 4

    .prologue
    .line 2184
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/a/a/a/a/j;->k:I

    if-ltz v0, :cond_0

    .line 2185
    iget v0, p0, Lorg/a/a/a/a/j;->k:I

    iget v1, p0, Lorg/a/a/a/a/j;->o:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2187
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/a/a/a/a/j;->o:I

    int-to-double v0, v0

    iget v2, p0, Lorg/a/a/a/a/j;->k:I

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0

    .line 2184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()I
    .locals 1

    .prologue
    .line 1538
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/a/a/a/a/j;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(J)V
    .locals 2

    .prologue
    .line 2147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    if-eqz v0, :cond_0

    .line 2148
    iget-object v0, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    invoke-static {v0}, Lorg/a/a/a/a/f;->a(Ljava/util/TimerTask;)V

    .line 2149
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    .line 2151
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 2152
    new-instance v0, Lorg/a/a/a/a/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/a/a/a/a/l;-><init>(Lorg/a/a/a/a/j;Lorg/a/a/a/a/k;)V

    iput-object v0, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    .line 2153
    iget-object v0, p0, Lorg/a/a/a/a/j;->r:Lorg/a/a/a/a/l;

    invoke-static {v0, p1, p2, p1, p2}, Lorg/a/a/a/a/f;->a(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2155
    :cond_1
    monitor-exit p0

    return-void

    .line 2147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 1744
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->f()V

    .line 1745
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    if-nez v0, :cond_0

    .line 1746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1748
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v0, p1}, Lorg/a/a/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1750
    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->f()V

    .line 1751
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lorg/a/a/a/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1760
    return-void

    .line 1752
    :catch_0
    move-exception v0

    .line 1754
    :try_start_1
    iget-object v2, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v2, p1, v1}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1758
    :goto_0
    throw v0

    .line 1755
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 1587
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lorg/a/a/a/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1613
    :cond_0
    :goto_0
    return-void

    .line 1588
    :catch_0
    move-exception v0

    .line 1589
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    if-eqz v0, :cond_0

    .line 1591
    :try_start_1
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v0, p1, p2}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1598
    :goto_1
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1599
    if-eqz v0, :cond_0

    .line 1600
    monitor-enter p0

    .line 1601
    :try_start_2
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->b()V

    .line 1602
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1605
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1606
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1608
    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1609
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    goto :goto_0

    .line 1608
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1592
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public declared-synchronized b()I
    .locals 1

    .prologue
    .line 1528
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/a/a/a/a/j;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1093
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1094
    new-instance v7, Lorg/a/a/a/a/m;

    const/4 v0, 0x0

    invoke-direct {v7, p0, p1, v0}, Lorg/a/a/a/a/m;-><init>(Lorg/a/a/a/a/j;Ljava/lang/Object;Lorg/a/a/a/a/k;)V

    .line 1097
    monitor-enter p0

    .line 1101
    :try_start_0
    iget-byte v8, p0, Lorg/a/a/a/a/j;->f:B

    .line 1102
    iget-wide v9, p0, Lorg/a/a/a/a/j;->e:J

    .line 1105
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1106
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1109
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1112
    :cond_0
    monitor-enter p0

    .line 1113
    :try_start_1
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->f()V

    .line 1114
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1116
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1118
    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1218
    :cond_1
    :goto_0
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_10

    .line 1220
    :try_start_2
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-interface {v0, p1}, Lorg/a/a/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1221
    new-instance v1, Lorg/a/a/a/a/o;

    invoke-direct {v1, v0}, Lorg/a/a/a/a/o;-><init>(Ljava/lang/Object;)V

    invoke-static {v7, v1}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;Lorg/a/a/a/a/o;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    move v2, v3

    .line 1237
    :goto_1
    :try_start_3
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v1

    iget-object v1, v1, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lorg/a/a/a/e;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1238
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->g:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v1

    iget-object v1, v1, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lorg/a/a/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1239
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "ValidateObject failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 1246
    :catch_0
    move-exception v0

    .line 1247
    invoke-static {v0}, Lorg/a/a/a/g;->a(Ljava/lang/Throwable;)V

    .line 1250
    :try_start_4
    iget-object v1, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v11

    iget-object v11, v11, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v1, p1, v11}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    .line 1255
    :goto_2
    monitor-enter p0

    .line 1256
    :try_start_5
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/n;->d()V

    .line 1257
    if-nez v2, :cond_2

    .line 1258
    invoke-static {v7}, Lorg/a/a/a/a/m;->e(Lorg/a/a/a/a/m;)V

    .line 1259
    iget-object v1, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    const/4 v11, 0x0

    invoke-virtual {v1, v11, v7}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1261
    :cond_2
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_a

    .line 1262
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1263
    if-eqz v2, :cond_0

    .line 1264
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create a validated object, cause: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1106
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 1114
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v0

    .line 1122
    :cond_3
    packed-switch v8, :pswitch_data_0

    .line 1211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "whenExhaustedAction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not recognized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1125
    :pswitch_0
    monitor-enter p0

    .line 1128
    :try_start_8
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1129
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1130
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/n;->c()V

    .line 1132
    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 1135
    :pswitch_1
    monitor-enter p0

    .line 1138
    :try_start_9
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1139
    :cond_5
    monitor-exit p0

    goto/16 :goto_0

    .line 1142
    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 1141
    :cond_6
    :try_start_a
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1142
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1143
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Pool exhausted"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1146
    :pswitch_2
    :try_start_b
    monitor-enter v7
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_1

    .line 1149
    :try_start_c
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1150
    cmp-long v0, v9, v12

    if-gtz v0, :cond_9

    .line 1151
    invoke-virtual {v7}, Ljava/lang/Object;->wait()V

    .line 1165
    :cond_7
    :goto_3
    monitor-exit v7
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 1167
    :try_start_d
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->e()Z

    move-result v0

    if-ne v0, v3, :cond_d

    .line 1168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_1

    .line 1170
    :catch_1
    move-exception v0

    .line 1172
    monitor-enter p0

    .line 1174
    :try_start_e
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v1

    if-nez v1, :cond_b

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1177
    iget-object v1, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v1, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1189
    :goto_4
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 1190
    if-eqz v4, :cond_8

    .line 1191
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 1193
    :cond_8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1194
    throw v0

    .line 1155
    :cond_9
    :try_start_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v5

    .line 1156
    sub-long v0, v9, v0

    .line 1157
    cmp-long v2, v0, v12

    if-lez v2, :cond_7

    .line 1159
    invoke-virtual {v7, v0, v1}, Ljava/lang/Object;->wait(J)V

    goto :goto_3

    .line 1165
    :catchall_4
    move-exception v0

    monitor-exit v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :try_start_10
    throw v0
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_1

    .line 1163
    :cond_a
    :try_start_11
    monitor-exit v7
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    goto/16 :goto_0

    .line 1178
    :cond_b
    :try_start_12
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v1

    if-nez v1, :cond_c

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1181
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/n;->d()V

    move v4, v3

    .line 1182
    goto :goto_4

    .line 1185
    :cond_c
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/n;->d()V

    .line 1186
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/n;->a()V

    .line 1187
    invoke-static {v7}, Lorg/a/a/a/a/m;->d(Lorg/a/a/a/a/m;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/a/a/o;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/a/a/a/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 1189
    :catchall_5
    move-exception v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v0

    .line 1196
    :cond_d
    cmp-long v0, v9, v12

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v5

    cmp-long v0, v0, v9

    if-ltz v0, :cond_0

    .line 1197
    monitor-enter p0

    .line 1200
    :try_start_13
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    if-nez v0, :cond_e

    invoke-static {v7}, Lorg/a/a/a/a/m;->b(Lorg/a/a/a/a/m;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1201
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1205
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 1206
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Timeout waiting for idle object"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1203
    :cond_e
    :try_start_14
    monitor-exit p0

    goto/16 :goto_0

    .line 1205
    :catchall_6
    move-exception v0

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    throw v0

    .line 1224
    :catchall_7
    move-exception v0

    .line 1226
    monitor-enter p0

    .line 1227
    :try_start_15
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/a/a/a/a/n;->d()V

    .line 1229
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    .line 1230
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    throw v0

    .line 1229
    :catchall_8
    move-exception v0

    :try_start_16
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_8

    throw v0

    .line 1241
    :cond_f
    :try_start_17
    monitor-enter p0
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_0

    .line 1242
    :try_start_18
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 1243
    invoke-static {v7}, Lorg/a/a/a/a/m;->c(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/n;->a()V

    .line 1244
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_9

    .line 1245
    :try_start_19
    invoke-static {v7}, Lorg/a/a/a/a/m;->a(Lorg/a/a/a/a/m;)Lorg/a/a/a/a/o;

    move-result-object v0

    iget-object v0, v0, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_0

    return-object v0

    .line 1244
    :catchall_9
    move-exception v0

    :try_start_1a
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_9

    :try_start_1b
    throw v0
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_0

    .line 1251
    :catch_2
    move-exception v1

    .line 1252
    invoke-static {v1}, Lorg/a/a/a/g;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1261
    :catchall_a
    move-exception v0

    :try_start_1c
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_a

    throw v0

    :cond_10
    move v2, v4

    goto/16 :goto_1

    .line 1122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public c()V
    .locals 6

    .prologue
    .line 1373
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1374
    monitor-enter p0

    .line 1375
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1376
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1377
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1380
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1381
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1382
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1384
    iget-object v4, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v4, v3}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 1385
    iget v3, p0, Lorg/a/a/a/a/j;->o:I

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v4

    invoke-virtual {v4}, Lorg/a/a/a/a/a;->size()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p0, Lorg/a/a/a/a/j;->o:I

    .line 1386
    iget v3, p0, Lorg/a/a/a/a/j;->p:I

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v4

    invoke-virtual {v4}, Lorg/a/a/a/a/a;->size()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lorg/a/a/a/a/j;->p:I

    .line 1388
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->clear()V

    goto :goto_0

    .line 1390
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-direct {p0, v1, v0}, Lorg/a/a/a/a/j;->a(Ljava/util/Map;Lorg/a/a/a/e;)V

    .line 1392
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1805
    invoke-super {p0}, Lorg/a/a/a/a;->d()V

    .line 1806
    monitor-enter p0

    .line 1807
    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/j;->c()V

    .line 1808
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 1809
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->b()V

    .line 1810
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    .line 1812
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_1

    .line 1813
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->b()V

    .line 1814
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    .line 1816
    :cond_1
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/a/a/a/a/j;->a(J)V

    .line 1818
    :goto_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1819
    iget-object v0, p0, Lorg/a/a/a/a/j;->w:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/m;

    .line 1821
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1824
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 1826
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1827
    return-void
.end method

.method public declared-synchronized g()I
    .locals 1

    .prologue
    .line 653
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/a/a/a/a/j;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()I
    .locals 1

    .prologue
    .line 677
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/a/a/a/a/j;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 836
    iget v0, p0, Lorg/a/a/a/a/j;->b:I

    return v0
.end method

.method public j()V
    .locals 7

    .prologue
    .line 1402
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1405
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 1406
    monitor-enter p0

    .line 1407
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1408
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1409
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v0

    .line 1410
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1414
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1446
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1419
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 1420
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v0, v0

    const-wide v5, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v0, v5

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 1422
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    .line 1423
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    if-lez v3, :cond_3

    .line 1424
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1428
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 1429
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/o;

    .line 1430
    iget-object v1, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/a/a/a/a/n;

    .line 1431
    invoke-static {v1}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v6

    .line 1432
    invoke-interface {v6, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1434
    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1435
    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1441
    :goto_2
    invoke-virtual {v1}, Lorg/a/a/a/a/n;->c()V

    .line 1442
    iget v0, p0, Lorg/a/a/a/a/j;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/j;->o:I

    .line 1443
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    .line 1444
    goto :goto_1

    .line 1437
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1438
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1439
    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1446
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    invoke-direct {p0, v4, v0}, Lorg/a/a/a/a/j;->a(Ljava/util/Map;Lorg/a/a/a/e;)V

    .line 1448
    return-void
.end method

.method public k()V
    .locals 13

    .prologue
    .line 1888
    const/4 v1, 0x0

    .line 1892
    monitor-enter p0

    .line 1896
    :try_start_0
    iget-boolean v5, p0, Lorg/a/a/a/a/j;->i:Z

    .line 1897
    iget-wide v6, p0, Lorg/a/a/a/a/j;->l:J

    .line 1900
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    iget-object v0, v0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    if-eqz v0, :cond_0

    .line 1902
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    iget-object v0, v0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    .line 1904
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1906
    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/a/a/a/a/j;->o()I

    move-result v8

    move v4, v0

    move-object v0, v1

    :goto_0
    if-ge v4, v8, :cond_14

    .line 1908
    monitor-enter p0

    .line 1910
    :try_start_1
    iget-object v1, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1911
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1906
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 1904
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1915
    :cond_2
    :try_start_3
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-nez v1, :cond_3

    .line 1916
    invoke-direct {p0}, Lorg/a/a/a/a/j;->m()V

    .line 1917
    const/4 v0, 0x0

    .line 1921
    :cond_3
    iget-object v1, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    if-nez v1, :cond_4

    .line 1923
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1924
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1925
    invoke-direct {p0, v0}, Lorg/a/a/a/a/j;->c(Ljava/lang/Object;)V

    .line 1938
    :cond_4
    :goto_2
    iget-object v1, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    if-nez v1, :cond_6

    .line 1939
    monitor-exit p0

    goto :goto_1

    .line 1976
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1928
    :cond_5
    :try_start_4
    invoke-direct {p0}, Lorg/a/a/a/a/j;->m()V

    .line 1929
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v1, :cond_4

    .line 1930
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1931
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1932
    invoke-direct {p0, v0}, Lorg/a/a/a/a/j;->c(Ljava/lang/Object;)V

    goto :goto_2

    .line 1944
    :cond_6
    iget-boolean v1, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    iget-boolean v1, p0, Lorg/a/a/a/a/j;->v:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1946
    :cond_8
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v1, :cond_c

    .line 1947
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1948
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1949
    invoke-direct {p0, v0}, Lorg/a/a/a/a/j;->c(Ljava/lang/Object;)V

    move-object v1, v0

    .line 1962
    :goto_3
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1964
    :cond_a
    monitor-exit p0

    move-object v0, v1

    goto/16 :goto_1

    .line 1951
    :cond_b
    invoke-direct {p0}, Lorg/a/a/a/a/j;->m()V

    .line 1952
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    if-eqz v1, :cond_c

    .line 1953
    iget-object v1, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v1}, Lorg/a/a/a/a/b;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1954
    iget-object v0, p0, Lorg/a/a/a/a/j;->u:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1955
    invoke-direct {p0, v0}, Lorg/a/a/a/a/j;->c(Ljava/lang/Object;)V

    :cond_c
    move-object v1, v0

    goto :goto_3

    .line 1969
    :cond_d
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/o;

    move-object v3, v0

    .line 1972
    :goto_4
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->remove()V

    .line 1973
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 1974
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->c()V

    .line 1975
    iget v0, p0, Lorg/a/a/a/a/j;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/j;->o:I

    .line 1976
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1978
    const/4 v0, 0x0

    .line 1979
    const-wide/16 v9, 0x0

    cmp-long v2, v6, v9

    if-lez v2, :cond_e

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iget-wide v11, v3, Lorg/a/a/a/a/o;->b:J

    sub-long/2addr v9, v11

    cmp-long v2, v9, v6

    if-lez v2, :cond_e

    .line 1982
    const/4 v0, 0x1

    .line 1984
    :cond_e
    if-eqz v5, :cond_15

    if-nez v0, :cond_15

    .line 1985
    const/4 v2, 0x0

    .line 1987
    :try_start_5
    iget-object v9, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    iget-object v10, v3, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v9, v1, v10}, Lorg/a/a/a/e;->d(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 1988
    const/4 v2, 0x1

    .line 1992
    :goto_5
    if-eqz v2, :cond_15

    .line 1993
    iget-object v2, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    iget-object v9, v3, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v2, v1, v9}, Lorg/a/a/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1994
    const/4 v0, 0x1

    move v2, v0

    .line 2005
    :goto_6
    if-eqz v2, :cond_f

    .line 2007
    :try_start_6
    iget-object v0, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    iget-object v9, v3, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v9}, Lorg/a/a/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 2012
    :cond_f
    :goto_7
    monitor-enter p0

    .line 2013
    :try_start_7
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/n;

    .line 2015
    invoke-virtual {v0}, Lorg/a/a/a/a/n;->d()V

    .line 2016
    if-eqz v2, :cond_13

    .line 2017
    invoke-static {v0}, Lorg/a/a/a/a/n;->a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-static {v0}, Lorg/a/a/a/a/n;->b(Lorg/a/a/a/a/n;)I

    move-result v2

    if-nez v2, :cond_10

    invoke-static {v0}, Lorg/a/a/a/a/n;->c(Lorg/a/a/a/a/n;)I

    move-result v0

    if-nez v0, :cond_10

    .line 2020
    iget-object v0, p0, Lorg/a/a/a/a/j;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021
    iget-object v0, p0, Lorg/a/a/a/a/j;->s:Lorg/a/a/a/a/a;

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    .line 2031
    :cond_10
    :goto_8
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v0, v1

    goto/16 :goto_1

    .line 1969
    :cond_11
    :try_start_8
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/a/a/a/o;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object v3, v0

    goto/16 :goto_4

    .line 1989
    :catch_0
    move-exception v0

    .line 1990
    const/4 v0, 0x1

    goto :goto_5

    .line 1997
    :cond_12
    :try_start_9
    iget-object v2, p0, Lorg/a/a/a/a/j;->q:Lorg/a/a/a/e;

    iget-object v9, v3, Lorg/a/a/a/a/o;->a:Ljava/lang/Object;

    invoke-interface {v2, v1, v9}, Lorg/a/a/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move v2, v0

    .line 2000
    goto :goto_6

    .line 1998
    :catch_1
    move-exception v0

    .line 1999
    const/4 v0, 0x1

    move v2, v0

    goto :goto_6

    .line 2024
    :cond_13
    :try_start_a
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0, v3}, Lorg/a/a/a/a/b;->add(Ljava/lang/Object;)V

    .line 2025
    iget v0, p0, Lorg/a/a/a/a/j;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/j;->o:I

    .line 2026
    iget-boolean v0, p0, Lorg/a/a/a/a/j;->v:Z

    if-eqz v0, :cond_10

    .line 2028
    iget-object v0, p0, Lorg/a/a/a/a/j;->t:Lorg/a/a/a/a/b;

    invoke-virtual {v0}, Lorg/a/a/a/a/b;->previous()Ljava/lang/Object;

    goto :goto_8

    .line 2031
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v0

    .line 2033
    :cond_14
    invoke-direct {p0}, Lorg/a/a/a/a/j;->l()V

    .line 2034
    return-void

    .line 2008
    :catch_2
    move-exception v0

    goto :goto_7

    :cond_15
    move v2, v0

    goto :goto_6
.end method
