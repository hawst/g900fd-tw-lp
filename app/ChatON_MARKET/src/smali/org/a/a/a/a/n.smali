.class Lorg/a/a/a/a/n;
.super Ljava/lang/Object;
.source "GenericKeyedObjectPool.java"


# instance fields
.field final synthetic a:Lorg/a/a/a/a/j;

.field private b:I

.field private final c:Lorg/a/a/a/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/a",
            "<",
            "Lorg/a/a/a/a/o",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method private constructor <init>(Lorg/a/a/a/a/j;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2231
    iput-object p1, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2233
    iput v1, p0, Lorg/a/a/a/a/n;->b:I

    .line 2236
    new-instance v0, Lorg/a/a/a/a/a;

    invoke-direct {v0}, Lorg/a/a/a/a/a;-><init>()V

    iput-object v0, p0, Lorg/a/a/a/a/n;->c:Lorg/a/a/a/a/a;

    .line 2239
    iput v1, p0, Lorg/a/a/a/a/n;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/a/a/a/a/j;Lorg/a/a/a/a/k;)V
    .locals 0

    .prologue
    .line 2231
    invoke-direct {p0, p1}, Lorg/a/a/a/a/n;-><init>(Lorg/a/a/a/a/j;)V

    return-void
.end method

.method static synthetic a(Lorg/a/a/a/a/n;)Lorg/a/a/a/a/a;
    .locals 1

    .prologue
    .line 2231
    iget-object v0, p0, Lorg/a/a/a/a/n;->c:Lorg/a/a/a/a/a;

    return-object v0
.end method

.method static synthetic b(Lorg/a/a/a/a/n;)I
    .locals 1

    .prologue
    .line 2231
    iget v0, p0, Lorg/a/a/a/a/n;->b:I

    return v0
.end method

.method static synthetic c(Lorg/a/a/a/a/n;)I
    .locals 1

    .prologue
    .line 2231
    iget v0, p0, Lorg/a/a/a/a/n;->d:I

    return v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 2243
    iget-object v1, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    monitor-enter v1

    .line 2244
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    invoke-static {v0}, Lorg/a/a/a/a/j;->a(Lorg/a/a/a/a/j;)I

    .line 2245
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2246
    iget v0, p0, Lorg/a/a/a/a/n;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/n;->b:I

    .line 2247
    return-void

    .line 2245
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method b()V
    .locals 2

    .prologue
    .line 2251
    iget-object v1, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    monitor-enter v1

    .line 2252
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    invoke-static {v0}, Lorg/a/a/a/a/j;->b(Lorg/a/a/a/a/j;)I

    .line 2253
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2254
    iget v0, p0, Lorg/a/a/a/a/n;->b:I

    if-lez v0, :cond_0

    .line 2255
    iget v0, p0, Lorg/a/a/a/a/n;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/n;->b:I

    .line 2257
    :cond_0
    return-void

    .line 2253
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 2261
    iget-object v1, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    monitor-enter v1

    .line 2262
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    invoke-static {v0}, Lorg/a/a/a/a/j;->c(Lorg/a/a/a/a/j;)I

    .line 2263
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2264
    iget v0, p0, Lorg/a/a/a/a/n;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/n;->d:I

    .line 2265
    return-void

    .line 2263
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method d()V
    .locals 2

    .prologue
    .line 2269
    iget-object v1, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    monitor-enter v1

    .line 2270
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/n;->a:Lorg/a/a/a/a/j;

    invoke-static {v0}, Lorg/a/a/a/a/j;->d(Lorg/a/a/a/a/j;)I

    .line 2271
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272
    iget v0, p0, Lorg/a/a/a/a/n;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/n;->d:I

    .line 2273
    return-void

    .line 2271
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
