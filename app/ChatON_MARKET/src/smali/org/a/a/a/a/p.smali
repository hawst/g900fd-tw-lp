.class public Lorg/a/a/a/a/p;
.super Lorg/a/a/a/c;
.source "StackObjectPool.java"

# interfaces
.implements Lorg/a/a/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/a/a/a/c",
        "<TT;>;",
        "Lorg/a/a/a/f",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected b:Lorg/a/a/a/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/h",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected c:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected d:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lorg/a/a/a/a/p;-><init>(Lorg/a/a/a/h;II)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/h",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    const/16 v0, 0x8

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v1}, Lorg/a/a/a/a/p;-><init>(Lorg/a/a/a/h;II)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lorg/a/a/a/h;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/h",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0x8

    .line 134
    invoke-direct {p0}, Lorg/a/a/a/c;-><init>()V

    .line 426
    iput-object v1, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    .line 433
    iput-object v1, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    .line 440
    iput v0, p0, Lorg/a/a/a/a/p;->c:I

    .line 447
    const/4 v1, 0x0

    iput v1, p0, Lorg/a/a/a/a/p;->d:I

    .line 135
    iput-object p1, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    .line 136
    if-gez p2, :cond_0

    move p2, v0

    :cond_0
    iput p2, p0, Lorg/a/a/a/a/p;->c:I

    .line 137
    const/4 v0, 0x1

    if-ge p3, v0, :cond_1

    const/4 p3, 0x4

    .line 138
    :cond_1
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    .line 139
    iget-object v0, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    iget v1, p0, Lorg/a/a/a/a/p;->c:I

    if-le p3, v1, :cond_2

    iget p3, p0, Lorg/a/a/a/a/p;->c:I

    :cond_2
    invoke-virtual {v0, p3}, Ljava/util/Stack;->ensureCapacity(I)V

    .line 140
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 229
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/p;->a()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 230
    :goto_0
    iget-object v3, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    if-eqz v3, :cond_0

    .line 231
    iget-object v3, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v3, p1}, Lorg/a/a/a/h;->c(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v0

    .line 242
    :cond_0
    :goto_1
    if-nez v1, :cond_1

    move v0, v2

    .line 244
    :cond_1
    iget v3, p0, Lorg/a/a/a/a/p;->d:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/a/a/a/a/p;->d:I

    .line 245
    if-eqz v1, :cond_3

    .line 246
    const/4 v1, 0x0

    .line 247
    iget-object v3, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    iget v4, p0, Lorg/a/a/a/a/p;->c:I

    if-lt v3, v4, :cond_2

    .line 249
    iget-object v0, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v1

    move v0, v2

    .line 251
    :cond_2
    iget-object v2, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    invoke-virtual {v2, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, v1

    .line 254
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    if-eqz v0, :cond_4

    .line 258
    :try_start_1
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v0, p1}, Lorg/a/a/a/h;->b(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    :cond_4
    :goto_2
    monitor-exit p0

    return-void

    :cond_5
    move v1, v0

    .line 229
    goto :goto_0

    .line 235
    :cond_6
    :try_start_2
    iget-object v3, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v3, p1}, Lorg/a/a/a/h;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 236
    :catch_0
    move-exception v1

    move v1, v0

    .line 237
    goto :goto_1

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 259
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public declared-synchronized c()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 166
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/a/a/a/a/p;->b()V

    .line 168
    const/4 v0, 0x0

    move-object v1, v3

    .line 169
    :goto_0
    if-nez v1, :cond_5

    .line 170
    iget-object v1, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 171
    iget-object v1, p0, Lorg/a/a/a/a/p;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    move v2, v0

    .line 183
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 185
    :try_start_1
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v0, v1}, Lorg/a/a/a/h;->d(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v0, v1}, Lorg/a/a/a/h;->c(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    new-instance v0, Ljava/lang/Exception;

    const-string v4, "ValidateObject failed"

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_2
    invoke-static {v0}, Lorg/a/a/a/g;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :try_start_3
    iget-object v4, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v4, v1}, Lorg/a/a/a/h;->b(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 199
    :goto_1
    if-eqz v2, :cond_4

    .line 200
    :try_start_4
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create a validated object, cause: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173
    :cond_1
    :try_start_5
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    if-nez v0, :cond_2

    .line 174
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 176
    :cond_2
    iget-object v0, p0, Lorg/a/a/a/a/p;->b:Lorg/a/a/a/h;

    invoke-interface {v0}, Lorg/a/a/a/h;->b()Ljava/lang/Object;

    move-result-object v1

    .line 177
    const/4 v2, 0x1

    .line 178
    if-nez v1, :cond_0

    .line 179
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "PoolableObjectFactory.makeObject() returned null."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    move v0, v2

    .line 204
    goto :goto_0

    .line 193
    :catch_1
    move-exception v1

    .line 194
    :try_start_6
    invoke-static {v1}, Lorg/a/a/a/g;->a(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 197
    :catchall_1
    move-exception v0

    :try_start_7
    throw v0

    :cond_4
    move v0, v2

    move-object v1, v3

    .line 204
    goto :goto_0

    .line 207
    :cond_5
    iget v0, p0, Lorg/a/a/a/a/p;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/p;->d:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 208
    monitor-exit p0

    return-object v1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method
