.class public abstract Lorg/a/a/a/c;
.super Ljava/lang/Object;
.source "BaseObjectPool.java"

# interfaces
.implements Lorg/a/a/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/a/a/a/f",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private volatile a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/a/a/a/c;->a:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lorg/a/a/a/c;->a:Z

    return v0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/a/a/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool not open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    return-void
.end method
