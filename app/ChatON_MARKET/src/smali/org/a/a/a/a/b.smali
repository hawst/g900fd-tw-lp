.class public Lorg/a/a/a/a/b;
.super Lorg/a/a/a/a/c;
.source "CursorableLinkedList.java"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/a/a/a/a/a",
        "<TE;>.org/a/a/a/a/c;",
        "Ljava/util/ListIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:Z

.field final synthetic b:Lorg/a/a/a/a/a;


# direct methods
.method constructor <init>(Lorg/a/a/a/a/a;I)V
    .locals 1

    .prologue
    .line 1125
    iput-object p1, p0, Lorg/a/a/a/a/b;->b:Lorg/a/a/a/a/a;

    .line 1126
    invoke-direct {p0, p1, p2}, Lorg/a/a/a/a/c;-><init>(Lorg/a/a/a/a/a;I)V

    .line 1123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/a/a/a/a/b;->a:Z

    .line 1127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/a/a/a/a/b;->a:Z

    .line 1128
    invoke-virtual {p1, p0}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/b;)V

    .line 1129
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 1190
    iget-boolean v0, p0, Lorg/a/a/a/a/b;->a:Z

    if-nez v0, :cond_0

    .line 1191
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1193
    :cond_0
    return-void
.end method

.method protected a(Lorg/a/a/a/a/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/d",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1153
    iget-object v0, p0, Lorg/a/a/a/a/b;->b:Lorg/a/a/a/a/a;

    iget-object v0, v0, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1154
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1158
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/a/a/a/a/b;->b:Lorg/a/a/a/a/a;

    iget-object v0, v0, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1159
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1163
    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    if-ne v0, p1, :cond_2

    .line 1164
    iput-object v2, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    .line 1166
    :cond_2
    return-void

    .line 1155
    :cond_3
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1156
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    goto :goto_0

    .line 1160
    :cond_4
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1161
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    goto :goto_1
.end method

.method public add(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1143
    invoke-virtual {p0}, Lorg/a/a/a/a/b;->a()V

    .line 1144
    iget-object v0, p0, Lorg/a/a/a/a/b;->b:Lorg/a/a/a/a/a;

    iget-object v1, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    iget-object v2, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)Lorg/a/a/a/a/d;

    move-result-object v0

    .line 1145
    iget-object v1, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1146
    iget-object v1, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1147
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    .line 1148
    iget v0, p0, Lorg/a/a/a/a/b;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/b;->f:I

    .line 1149
    iget v0, p0, Lorg/a/a/a/a/b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/b;->e:I

    .line 1150
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1208
    iget-boolean v0, p0, Lorg/a/a/a/a/b;->a:Z

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/a/a/a/a/b;->a:Z

    .line 1210
    iget-object v0, p0, Lorg/a/a/a/a/b;->b:Lorg/a/a/a/a/a;

    invoke-virtual {v0, p0}, Lorg/a/a/a/a/a;->b(Lorg/a/a/a/a/b;)V

    .line 1212
    :cond_0
    return-void
.end method

.method protected b(Lorg/a/a/a/a/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/d",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1169
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1170
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1174
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1175
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1177
    :cond_1
    iget-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    if-ne v0, p1, :cond_2

    .line 1178
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    .line 1180
    :cond_2
    return-void

    .line 1171
    :cond_3
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1172
    iget-object v0, p0, Lorg/a/a/a/a/b;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    goto :goto_0
.end method

.method protected c(Lorg/a/a/a/a/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/d",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1183
    iget-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    if-ne v0, p1, :cond_0

    .line 1184
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/b;->d:Lorg/a/a/a/a/d;

    .line 1186
    :cond_0
    return-void
.end method

.method public bridge synthetic hasNext()Z
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/a/a/a/a/c;->hasNext()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hasPrevious()Z
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/a/a/a/a/c;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/a/a/a/a/c;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 1138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/a/a/a/a/c;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 1133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/a/a/a/a/c;->remove()V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1122
    invoke-super {p0, p1}, Lorg/a/a/a/a/c;->set(Ljava/lang/Object;)V

    return-void
.end method
