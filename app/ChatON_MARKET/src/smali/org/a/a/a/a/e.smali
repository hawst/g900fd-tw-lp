.class Lorg/a/a/a/a/e;
.super Lorg/a/a/a/a/a;
.source "CursorableLinkedList.java"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/a/a/a/a/a",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;"
    }
.end annotation


# instance fields
.field protected e:Lorg/a/a/a/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/a",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected f:Lorg/a/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/d",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected g:Lorg/a/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/d",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/a/a/a/a/a;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/a",
            "<TE;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1221
    invoke-direct {p0}, Lorg/a/a/a/a/a;-><init>()V

    .line 1508
    iput-object v1, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    .line 1511
    iput-object v1, p0, Lorg/a/a/a/a/e;->f:Lorg/a/a/a/a/d;

    .line 1514
    iput-object v1, p0, Lorg/a/a/a/a/e;->g:Lorg/a/a/a/a/d;

    .line 1222
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    if-ge v0, p3, :cond_1

    .line 1223
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1224
    :cond_1
    if-le p2, p3, :cond_2

    .line 1225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1227
    :cond_2
    iput-object p1, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    .line 1228
    invoke-virtual {p1}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    if-ge p2, v0, :cond_4

    .line 1229
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    iget-object v2, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    invoke-virtual {v2, p2}, Lorg/a/a/a/a/a;->b(I)Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1230
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lorg/a/a/a/a/e;->f:Lorg/a/a/a/a/d;

    .line 1234
    :goto_1
    if-ne p2, p3, :cond_6

    .line 1235
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1236
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1237
    invoke-virtual {p1}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    if-ge p3, v0, :cond_5

    .line 1238
    iget-object v0, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    invoke-virtual {v0, p3}, Lorg/a/a/a/a/a;->b(I)Lorg/a/a/a/a/d;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/a/e;->g:Lorg/a/a/a/a/d;

    .line 1246
    :goto_2
    sub-int v0, p3, p2

    iput v0, p0, Lorg/a/a/a/a/e;->a:I

    .line 1247
    iget-object v0, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    iget v0, v0, Lorg/a/a/a/a/a;->c:I

    iput v0, p0, Lorg/a/a/a/a/e;->c:I

    .line 1248
    return-void

    .line 1230
    :cond_3
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    goto :goto_0

    .line 1232
    :cond_4
    iget-object v0, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/a;->b(I)Lorg/a/a/a/a/d;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/a/e;->f:Lorg/a/a/a/a/d;

    goto :goto_1

    .line 1240
    :cond_5
    iput-object v1, p0, Lorg/a/a/a/a/e;->g:Lorg/a/a/a/a/d;

    goto :goto_2

    .line 1243
    :cond_6
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    iget-object v1, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Lorg/a/a/a/a/a;->b(I)Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1244
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/a/e;->g:Lorg/a/a/a/a/d;

    goto :goto_2
.end method


# virtual methods
.method protected a(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)Lorg/a/a/a/a/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/d",
            "<TE;>;",
            "Lorg/a/a/a/a/d",
            "<TE;>;TE;)",
            "Lorg/a/a/a/a/d",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1453
    iget v0, p0, Lorg/a/a/a/a/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/e;->c:I

    .line 1454
    iget v0, p0, Lorg/a/a/a/a/e;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/e;->a:I

    .line 1455
    iget-object v2, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    if-nez p1, :cond_3

    iget-object v0, p0, Lorg/a/a/a/a/e;->f:Lorg/a/a/a/a/d;

    move-object v1, v0

    :goto_0
    if-nez p2, :cond_4

    iget-object v0, p0, Lorg/a/a/a/a/e;->g:Lorg/a/a/a/a/d;

    :goto_1
    invoke-virtual {v2, v1, v0, p3}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)Lorg/a/a/a/a/d;

    move-result-object v0

    .line 1456
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1457
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1458
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1460
    :cond_0
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 1461
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1463
    :cond_1
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    if-ne p2, v1, :cond_2

    .line 1464
    iget-object v1, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1, v0}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1466
    :cond_2
    invoke-virtual {p0, v0}, Lorg/a/a/a/a/e;->d(Lorg/a/a/a/a/d;)V

    .line 1467
    return-object v0

    :cond_3
    move-object v1, p1

    .line 1455
    goto :goto_0

    :cond_4
    move-object v0, p2

    goto :goto_1
.end method

.method protected a(Lorg/a/a/a/a/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/a/a/a/d",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1475
    iget v0, p0, Lorg/a/a/a/a/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/e;->c:I

    .line 1476
    iget v0, p0, Lorg/a/a/a/a/e;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/e;->a:I

    .line 1477
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1478
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1479
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1481
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1482
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1484
    :cond_1
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 1485
    iget-object v0, p0, Lorg/a/a/a/a/e;->b:Lorg/a/a/a/a/d;

    invoke-virtual {p1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1487
    :cond_2
    iget-object v0, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/d;)V

    .line 1488
    invoke-virtual {p0, p1}, Lorg/a/a/a/a/e;->c(Lorg/a/a/a/a/d;)V

    .line 1489
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1330
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1331
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public add(ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 1402
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1403
    invoke-super {p0, p1, p2}, Lorg/a/a/a/a/a;->add(ILjava/lang/Object;)V

    .line 1404
    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1324
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1325
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 1354
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1355
    invoke-super {p0, p1, p2}, Lorg/a/a/a/a/a;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 1318
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1319
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1306
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1307
    invoke-super {p0}, Lorg/a/a/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1336
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1337
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 1500
    iget v0, p0, Lorg/a/a/a/a/e;->c:I

    iget-object v1, p0, Lorg/a/a/a/a/e;->e:Lorg/a/a/a/a/a;

    iget v1, v1, Lorg/a/a/a/a/a;->c:I

    if-eq v0, v1, :cond_0

    .line 1501
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1503
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 1254
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1255
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1256
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1257
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1258
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1260
    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1294
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1295
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1348
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1349
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1378
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1379
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 1384
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1385
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1360
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1361
    invoke-super {p0}, Lorg/a/a/a/a/a;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1420
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1421
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 1276
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1277
    invoke-super {p0}, Lorg/a/a/a/a/a;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1264
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1265
    invoke-super {p0}, Lorg/a/a/a/a/a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1426
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1427
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1432
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1433
    invoke-super {p0}, Lorg/a/a/a/a/a;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1408
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1409
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 1414
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1415
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1300
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1301
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1342
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1343
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1366
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1367
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 1372
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1373
    invoke-super {p0, p1, p2}, Lorg/a/a/a/a/a;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1270
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1271
    invoke-super {p0}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1438
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1439
    invoke-super {p0, p1, p2}, Lorg/a/a/a/a/a;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1282
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1283
    invoke-super {p0}, Lorg/a/a/a/a/a;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 1288
    invoke-virtual {p0}, Lorg/a/a/a/a/e;->c()V

    .line 1289
    invoke-super {p0, p1}, Lorg/a/a/a/a/a;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
