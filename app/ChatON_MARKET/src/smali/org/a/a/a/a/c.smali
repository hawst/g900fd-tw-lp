.class Lorg/a/a/a/a/c;
.super Ljava/lang/Object;
.source "CursorableLinkedList.java"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/ListIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field c:Lorg/a/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/d",
            "<TE;>;"
        }
    .end annotation
.end field

.field d:Lorg/a/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/d",
            "<TE;>;"
        }
    .end annotation
.end field

.field e:I

.field f:I

.field final synthetic g:Lorg/a/a/a/a/a;


# direct methods
.method constructor <init>(Lorg/a/a/a/a/a;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1016
    iput-object p1, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011
    iput-object v3, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    .line 1012
    iput-object v3, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    .line 1013
    iget-object v0, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget v0, v0, Lorg/a/a/a/a/a;->c:I

    iput v0, p0, Lorg/a/a/a/a/c;->e:I

    .line 1014
    iput v2, p0, Lorg/a/a/a/a/c;->f:I

    .line 1017
    if-nez p2, :cond_0

    .line 1018
    new-instance v0, Lorg/a/a/a/a/d;

    iget-object v1, p1, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-direct {v0, v3, v1, v3}, Lorg/a/a/a/a/d;-><init>(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    .line 1019
    iput v2, p0, Lorg/a/a/a/a/c;->f:I

    .line 1028
    :goto_0
    return-void

    .line 1020
    :cond_0
    iget v0, p1, Lorg/a/a/a/a/a;->a:I

    if-ne p2, v0, :cond_1

    .line 1021
    new-instance v0, Lorg/a/a/a/a/d;

    iget-object v1, p1, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-direct {v0, v1, v3, v3}, Lorg/a/a/a/a/d;-><init>(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    .line 1022
    iget v0, p1, Lorg/a/a/a/a/a;->a:I

    iput v0, p0, Lorg/a/a/a/a/c;->f:I

    goto :goto_0

    .line 1024
    :cond_1
    invoke-virtual {p1, p2}, Lorg/a/a/a/a/a;->b(I)Lorg/a/a/a/a/d;

    move-result-object v0

    .line 1025
    new-instance v1, Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-direct {v1, v2, v0, v3}, Lorg/a/a/a/a/d;-><init>(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    .line 1026
    iput p2, p0, Lorg/a/a/a/a/c;->f:I

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 1116
    iget v0, p0, Lorg/a/a/a/a/c;->e:I

    iget-object v1, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget v1, v1, Lorg/a/a/a/a/a;->c:I

    if-eq v0, v1, :cond_0

    .line 1117
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1119
    :cond_0
    return-void
.end method

.method public add(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1108
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1109
    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v1, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v2

    iget-object v3, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v3}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/d;Lorg/a/a/a/a/d;Ljava/lang/Object;)Lorg/a/a/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1110
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    .line 1111
    iget v0, p0, Lorg/a/a/a/a/c;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/c;->f:I

    .line 1112
    iget v0, p0, Lorg/a/a/a/a/c;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/c;->e:I

    .line 1113
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 1045
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1046
    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    iget-object v1, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v1, v1, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 2

    .prologue
    .line 1072
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1073
    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    iget-object v1, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v1, v1, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1050
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1051
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1052
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1054
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    .line 1055
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v1

    iput-object v1, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    .line 1056
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1057
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1058
    iget v1, p0, Lorg/a/a/a/a/c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/a/a/a/a/c;->f:I

    .line 1059
    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 1086
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1087
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1088
    iget-object v0, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    invoke-virtual {v0}, Lorg/a/a/a/a/a;->size()I

    move-result v0

    .line 1090
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/a/a/a/a/c;->f:I

    goto :goto_0
.end method

.method public previous()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1031
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1032
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1033
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1035
    :cond_0
    iget-object v0, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    .line 1036
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v1}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v1

    iput-object v1, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    .line 1037
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1038
    iget-object v1, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1039
    iget v1, p0, Lorg/a/a/a/a/c;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/a/a/a/a/c;->f:I

    .line 1040
    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 1064
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1065
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    const/4 v0, -0x1

    .line 1068
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/a/a/a/a/c;->f:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public remove()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1094
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1095
    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    if-nez v0, :cond_0

    .line 1096
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1098
    :cond_0
    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    iget-object v3, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v3, v3, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v3}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lorg/a/a/a/a/d;->a(Lorg/a/a/a/a/d;)V

    .line 1099
    iget-object v2, p0, Lorg/a/a/a/a/c;->c:Lorg/a/a/a/a/d;

    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    iget-object v3, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v3, v3, Lorg/a/a/a/a/a;->b:Lorg/a/a/a/a/d;

    invoke-virtual {v3}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v3

    if-ne v0, v3, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lorg/a/a/a/a/d;->b(Lorg/a/a/a/a/d;)V

    .line 1100
    iget-object v0, p0, Lorg/a/a/a/a/c;->g:Lorg/a/a/a/a/a;

    iget-object v2, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    invoke-virtual {v0, v2}, Lorg/a/a/a/a/a;->a(Lorg/a/a/a/a/d;)V

    .line 1101
    iput-object v1, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    .line 1102
    iget v0, p0, Lorg/a/a/a/a/c;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/a/a/a/a/c;->f:I

    .line 1103
    iget v0, p0, Lorg/a/a/a/a/c;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/a/a/a/a/c;->e:I

    .line 1105
    return-void

    .line 1098
    :cond_1
    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->a()Lorg/a/a/a/a/d;

    move-result-object v0

    goto :goto_0

    .line 1099
    :cond_2
    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    invoke-virtual {v0}, Lorg/a/a/a/a/d;->b()Lorg/a/a/a/a/d;

    move-result-object v0

    goto :goto_1
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1077
    invoke-virtual {p0}, Lorg/a/a/a/a/c;->a()V

    .line 1079
    :try_start_0
    iget-object v0, p0, Lorg/a/a/a/a/c;->d:Lorg/a/a/a/a/d;

    invoke-virtual {v0, p1}, Lorg/a/a/a/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1083
    return-void

    .line 1080
    :catch_0
    move-exception v0

    .line 1081
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
