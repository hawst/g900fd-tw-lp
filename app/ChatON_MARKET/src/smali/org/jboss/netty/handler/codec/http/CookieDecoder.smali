.class public Lorg/jboss/netty/handler/codec/http/CookieDecoder;
.super Ljava/lang/Object;
.source "CookieDecoder.java"


# static fields
.field private static final COMMA:Ljava/lang/String; = ","

.field private static final PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "(?:\\s|[;,])*\\$*([^;=]+)(?:=(?:[\"\']((?:\\\\.|[^\"])*)[\"\']|([^;,]*)))?(\\s*(?:[;,]+\\s*|$))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/jboss/netty/handler/codec/http/CookieDecoder;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method private decodeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 235
    if-nez p1, :cond_0

    .line 238
    :goto_0
    return-object p1

    :cond_0
    const-string v0, "\\\""

    const-string v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\\\"

    const-string v2, "\\"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private extractKeyValuePairs(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 187
    sget-object v1, Lorg/jboss/netty/handler/codec/http/CookieDecoder;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 188
    const/4 v1, 0x0

    move-object v2, v0

    move-object v4, v0

    .line 192
    :goto_0
    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 193
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->end()I

    move-result v6

    .line 196
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 197
    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 198
    if-nez v3, :cond_0

    .line 199
    const/4 v1, 0x2

    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/jboss/netty/handler/codec/http/CookieDecoder;->decodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 203
    if-nez v4, :cond_2

    .line 205
    if-nez v3, :cond_1

    const-string v0, ""

    :goto_1
    move-object v2, v0

    move-object v4, v5

    move-object v0, v1

    move v1, v6

    .line 207
    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 205
    goto :goto_1

    .line 210
    :cond_2
    if-nez v3, :cond_3

    const-string v8, "Discard"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "Secure"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "HTTPOnly"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    move v1, v6

    .line 216
    goto :goto_0

    .line 219
    :cond_3
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v2, v3

    move-object v4, v5

    move v1, v6

    .line 225
    goto :goto_0

    .line 228
    :cond_4
    if-eqz v4, :cond_5

    .line 229
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_5
    return-void
.end method


# virtual methods
.method public decode(Ljava/lang/String;)Ljava/util/Set;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lorg/jboss/netty/handler/codec/http/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v18, Ljava/util/ArrayList;

    const/16 v4, 0x8

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 67
    new-instance v19, Ljava/util/ArrayList;

    const/16 v4, 0x8

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lorg/jboss/netty/handler/codec/http/CookieDecoder;->extractKeyValuePairs(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 70
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    .line 182
    :goto_0
    return-object v4

    .line 75
    :cond_0
    const/4 v5, 0x0

    .line 79
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v6, "Version"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 81
    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 85
    :goto_1
    const/4 v5, 0x1

    .line 90
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, v5, :cond_2

    .line 92
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    goto :goto_0

    .line 82
    :catch_0
    move-exception v4

    move v4, v5

    goto :goto_1

    .line 87
    :cond_1
    const/4 v4, 0x0

    move/from16 v25, v5

    move v5, v4

    move/from16 v4, v25

    goto :goto_2

    .line 95
    :cond_2
    new-instance v17, Ljava/util/TreeSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/TreeSet;-><init>()V

    move v6, v4

    move v7, v5

    .line 96
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    if-ge v7, v4, :cond_13

    .line 97
    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 98
    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 99
    if-nez v5, :cond_3

    .line 100
    const-string v5, ""

    .line 103
    :cond_3
    new-instance v20, Lorg/jboss/netty/handler/codec/http/DefaultCookie;

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5}, Lorg/jboss/netty/handler/codec/http/DefaultCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    const/4 v14, 0x0

    .line 107
    const/4 v13, 0x0

    .line 108
    const/4 v12, 0x0

    .line 109
    const/4 v11, 0x0

    .line 110
    const/4 v10, 0x0

    .line 111
    const/4 v9, 0x0

    .line 112
    const/4 v8, 0x0

    .line 113
    const/4 v5, -0x1

    .line 114
    new-instance v21, Ljava/util/ArrayList;

    const/4 v4, 0x2

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    add-int/lit8 v4, v7, 0x1

    move v15, v4

    move/from16 v16, v7

    move v7, v5

    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    if-ge v15, v4, :cond_10

    .line 117
    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 118
    move-object/from16 v0, v19

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 120
    const-string v22, "Discard"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 121
    const/4 v4, 0x1

    move-object v5, v8

    move-object v8, v11

    move v11, v4

    move v4, v7

    move-object v7, v10

    move v10, v13

    move/from16 v25, v12

    move v12, v6

    move-object v6, v9

    move/from16 v9, v25

    .line 116
    :goto_5
    add-int/lit8 v13, v15, 0x1

    add-int/lit8 v14, v16, 0x1

    move v15, v13

    move/from16 v16, v14

    move v13, v10

    move v14, v11

    move-object v10, v7

    move-object v11, v8

    move-object v8, v5

    move v7, v4

    move-object/from16 v25, v6

    move v6, v12

    move v12, v9

    move-object/from16 v9, v25

    goto :goto_4

    .line 122
    :cond_4
    const-string v22, "Secure"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 123
    const/4 v4, 0x1

    move-object v5, v8

    move-object v8, v11

    move v11, v14

    move-object/from16 v25, v10

    move v10, v4

    move v4, v7

    move-object/from16 v7, v25

    move/from16 v26, v12

    move v12, v6

    move-object v6, v9

    move/from16 v9, v26

    goto :goto_5

    .line 124
    :cond_5
    const-string v22, "HTTPOnly"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 125
    const/4 v4, 0x1

    move-object v5, v8

    move v12, v6

    move-object v6, v9

    move-object v8, v11

    move v9, v4

    move v11, v14

    move v4, v7

    move-object v7, v10

    move v10, v13

    goto :goto_5

    .line 126
    :cond_6
    const-string v22, "Comment"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_7

    move v4, v7

    move v11, v14

    move-object v7, v10

    move v10, v13

    move-object/from16 v25, v5

    move-object v5, v8

    move-object/from16 v8, v25

    move-object/from16 v26, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v26

    .line 127
    goto :goto_5

    .line 128
    :cond_7
    const-string v22, "CommentURL"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_8

    move v4, v7

    move v10, v13

    move-object v7, v5

    move-object v5, v8

    move-object v8, v11

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    .line 129
    goto :goto_5

    .line 130
    :cond_8
    const-string v22, "Domain"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_9

    move v4, v7

    move v9, v12

    move-object v7, v10

    move v12, v6

    move v10, v13

    move-object v6, v5

    move-object v5, v8

    move-object v8, v11

    move v11, v14

    .line 131
    goto/16 :goto_5

    .line 132
    :cond_9
    const-string v22, "Path"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    move v4, v7

    move-object v8, v11

    move-object v7, v10

    move v11, v14

    move v10, v13

    move/from16 v25, v12

    move v12, v6

    move-object v6, v9

    move/from16 v9, v25

    .line 133
    goto/16 :goto_5

    .line 134
    :cond_a
    const-string v22, "Expires"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 136
    :try_start_1
    new-instance v4, Lorg/jboss/netty/handler/codec/http/CookieDateFormat;

    invoke-direct {v4}, Lorg/jboss/netty/handler/codec/http/CookieDateFormat;-><init>()V

    invoke-virtual {v4, v5}, Lorg/jboss/netty/handler/codec/http/CookieDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v4, v4, v22

    .line 139
    const-wide/16 v22, 0x0

    cmp-long v22, v4, v22

    if-gtz v22, :cond_b

    .line 140
    const/4 v4, 0x0

    :goto_6
    move-object v5, v8

    move-object v7, v10

    move-object v8, v11

    move v10, v13

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    .line 147
    goto/16 :goto_5

    .line 142
    :cond_b
    const-wide/16 v22, 0x3e8

    div-long v22, v4, v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    const-wide/16 v23, 0x3e8

    rem-long v4, v4, v23
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    const-wide/16 v23, 0x0

    cmp-long v4, v4, v23

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_7
    add-int v4, v4, v22

    goto :goto_6

    :cond_c
    const/4 v4, 0x0

    goto :goto_7

    .line 145
    :catch_1
    move-exception v4

    move v4, v7

    move-object v5, v8

    move-object v7, v10

    move-object v8, v11

    move v10, v13

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    .line 147
    goto/16 :goto_5

    .line 148
    :cond_d
    const-string v22, "Max-Age"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 149
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move-object v5, v8

    move-object v7, v10

    move-object v8, v11

    move v10, v13

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    goto/16 :goto_5

    .line 150
    :cond_e
    const-string v22, "Version"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 151
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move v4, v7

    move-object v5, v8

    move-object v7, v10

    move-object v8, v11

    move v10, v13

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    goto/16 :goto_5

    .line 152
    :cond_f
    const-string v22, "Port"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 153
    const-string v4, ","

    invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 154
    array-length v0, v5

    move/from16 v22, v0

    const/4 v4, 0x0

    :goto_8
    move/from16 v0, v22

    if-ge v4, v0, :cond_14

    aget-object v23, v5, v4

    .line 156
    :try_start_2
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 154
    :goto_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 166
    :cond_10
    move-object/from16 v0, v20

    invoke-interface {v0, v6}, Lorg/jboss/netty/handler/codec/http/Cookie;->setVersion(I)V

    .line 167
    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Lorg/jboss/netty/handler/codec/http/Cookie;->setMaxAge(I)V

    .line 168
    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Lorg/jboss/netty/handler/codec/http/Cookie;->setPath(Ljava/lang/String;)V

    .line 169
    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Lorg/jboss/netty/handler/codec/http/Cookie;->setDomain(Ljava/lang/String;)V

    .line 170
    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Lorg/jboss/netty/handler/codec/http/Cookie;->setSecure(Z)V

    .line 171
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Lorg/jboss/netty/handler/codec/http/Cookie;->setHttpOnly(Z)V

    .line 172
    if-lez v6, :cond_11

    .line 173
    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Lorg/jboss/netty/handler/codec/http/Cookie;->setComment(Ljava/lang/String;)V

    .line 175
    :cond_11
    const/4 v4, 0x1

    if-le v6, v4, :cond_12

    .line 176
    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Lorg/jboss/netty/handler/codec/http/Cookie;->setCommentUrl(Ljava/lang/String;)V

    .line 177
    invoke-interface/range {v20 .. v21}, Lorg/jboss/netty/handler/codec/http/Cookie;->setPorts(Ljava/lang/Iterable;)V

    .line 178
    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Lorg/jboss/netty/handler/codec/http/Cookie;->setDiscard(Z)V

    .line 96
    :cond_12
    add-int/lit8 v5, v16, 0x1

    move v7, v5

    goto/16 :goto_3

    :cond_13
    move-object/from16 v4, v17

    .line 182
    goto/16 :goto_0

    .line 157
    :catch_2
    move-exception v23

    goto :goto_9

    :cond_14
    move v4, v7

    move-object v5, v8

    move-object v7, v10

    move-object v8, v11

    move v10, v13

    move v11, v14

    move-object/from16 v25, v9

    move v9, v12

    move v12, v6

    move-object/from16 v6, v25

    goto/16 :goto_5
.end method
