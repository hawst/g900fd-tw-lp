.class public Lorg/jboss/netty/util/internal/LinkedTransferQueue;
.super Ljava/util/AbstractQueue;
.source "LinkedTransferQueue.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/BlockingQueue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<TE;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/BlockingQueue",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final ASYNC:I = 0x1

.field private static final CHAINED_SPINS:I = 0x40

.field private static final FRONT_SPINS:I = 0x80

.field private static final MP:Z

.field private static final NOW:I = 0x0

.field static final SWEEP_THRESHOLD:I = 0x20

.field private static final SYNC:I = 0x2

.field private static final TIMED:I = 0x3

.field private static final headUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue;",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x2cbacc91e0a3c166L

.field private static final sweepVotesUpdater:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue;",
            ">;"
        }
    .end annotation
.end field

.field private static final tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue;",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field volatile transient head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

.field volatile transient sweepVotes:I

.field volatile transient tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 396
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    sput-boolean v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->MP:Z

    .line 1344
    const-class v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    const-class v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    const-string v2, "head"

    invoke-static {v0, v1, v2}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->newRefUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->headUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 1347
    const-class v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    const-class v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    const-string v2, "tail"

    invoke-static {v0, v1, v2}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->newRefUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 1350
    const-class v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    const-string v1, "sweepVotes"

    invoke-static {v0, v1}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->newIntUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweepVotesUpdater:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void

    .line 396
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1032
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 1033
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1045
    invoke-direct {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;-><init>()V

    .line 1046
    invoke-virtual {p0, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->addAll(Ljava/util/Collection;)Z

    .line 1047
    return-void
.end method

.method private awaitMatch(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Ljava/lang/Object;ZJ)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;",
            "Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;",
            "TE;ZJ)TE;"
        }
    .end annotation

    .prologue
    .line 731
    if-eqz p4, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 732
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    .line 733
    const/4 v3, -0x1

    .line 734
    const/4 v2, 0x0

    move-wide/from16 v6, p5

    move v9, v3

    move-wide v10, v0

    move v1, v9

    move-object v0, v2

    move-wide v2, v10

    .line 737
    :goto_1
    iget-object v4, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->item:Ljava/lang/Object;

    .line 738
    if-eq v4, p3, :cond_1

    .line 740
    invoke-virtual {p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->forgetContents()V

    .line 741
    invoke-static {v4}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 746
    :goto_2
    return-object p3

    .line 731
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 743
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz p4, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-gtz v4, :cond_3

    :cond_2
    invoke-virtual {p1, p3, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->casItem(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 745
    invoke-virtual {p0, p2, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->unsplice(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V

    goto :goto_2

    .line 749
    :cond_3
    if-gez v1, :cond_4

    .line 750
    iget-boolean v1, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    invoke-static {p2, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->spinsFor(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Z)I

    move-result v1

    if-lez v1, :cond_9

    .line 751
    invoke-static {}, Lorg/jboss/netty/util/internal/ThreadLocalRandom;->current()Lorg/jboss/netty/util/internal/ThreadLocalRandom;

    move-result-object v0

    move-wide v4, v6

    :goto_3
    move-wide v6, v4

    .line 773
    goto :goto_1

    .line 754
    :cond_4
    if-lez v1, :cond_5

    .line 755
    add-int/lit8 v1, v1, -0x1

    .line 756
    const/16 v4, 0x40

    invoke-virtual {v0, v4}, Lorg/jboss/netty/util/internal/ThreadLocalRandom;->nextInt(I)I

    move-result v4

    if-nez v4, :cond_9

    .line 757
    invoke-static {}, Ljava/lang/Thread;->yield()V

    move-wide v4, v6

    goto :goto_3

    .line 760
    :cond_5
    iget-object v4, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->waiter:Ljava/lang/Thread;

    if-nez v4, :cond_6

    .line 761
    iput-object v8, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->waiter:Ljava/lang/Thread;

    move-wide v4, v6

    goto :goto_3

    .line 763
    :cond_6
    if-eqz p4, :cond_8

    .line 764
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 765
    sub-long v2, v4, v2

    sub-long v2, v6, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_7

    .line 766
    invoke-static {v2, v3}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(J)V

    :cond_7
    move-wide v9, v4

    move-wide v4, v2

    move-wide v2, v9

    .line 769
    goto :goto_3

    :cond_8
    move-wide v4, v6

    .line 771
    invoke-static {}, Ljava/util/concurrent/locks/LockSupport;->park()V

    goto :goto_3

    :cond_9
    move-wide v4, v6

    goto :goto_3
.end method

.method private casHead(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z
    .locals 1

    .prologue
    .line 576
    invoke-static {}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    sget-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->headUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 584
    :goto_0
    return v0

    .line 579
    :cond_0
    monitor-enter p0

    .line 580
    :try_start_0
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v0, p1, :cond_1

    .line 581
    iput-object p2, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 582
    const/4 v0, 0x1

    monitor-exit p0

    goto :goto_0

    .line 586
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 584
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private casSweepVotes(II)Z
    .locals 1

    .prologue
    .line 591
    invoke-static {}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    sget-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweepVotesUpdater:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    .line 599
    :goto_0
    return v0

    .line 594
    :cond_0
    monitor-enter p0

    .line 595
    :try_start_0
    iget v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweepVotes:I

    if-ne v0, p1, :cond_1

    .line 596
    iput p2, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweepVotes:I

    .line 597
    const/4 v0, 0x1

    monitor-exit p0

    goto :goto_0

    .line 601
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 599
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private casTail(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z
    .locals 1

    .prologue
    .line 561
    invoke-static {}, Lorg/jboss/netty/util/internal/AtomicFieldUpdaterUtil;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    sget-object v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 569
    :goto_0
    return v0

    .line 564
    :cond_0
    monitor-enter p0

    .line 565
    :try_start_0
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v0, p1, :cond_1

    .line 566
    iput-object p2, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 567
    const/4 v0, 0x1

    monitor-exit p0

    goto :goto_0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 569
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static cast(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 616
    return-object p0
.end method

.method private countOfMode(Z)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 844
    .line 845
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v3, v0

    move v0, v2

    :goto_0
    if-eqz v3, :cond_1

    .line 846
    invoke-virtual {v3}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v1

    if-nez v1, :cond_2

    .line 847
    iget-boolean v1, v3, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-eq v1, p1, :cond_0

    .line 862
    :goto_1
    return v2

    .line 850
    :cond_0
    add-int/lit8 v0, v0, 0x1

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    :cond_1
    move v2, v0

    .line 862
    goto :goto_1

    .line 854
    :cond_2
    iget-object v1, v3, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 855
    if-eq v1, v3, :cond_3

    move-object v4, v1

    move v1, v0

    move-object v0, v4

    :goto_2
    move-object v3, v0

    move v0, v1

    .line 861
    goto :goto_0

    .line 859
    :cond_3
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move v1, v2

    goto :goto_2
.end method

.method private findAndRemove(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1004
    if-eqz p1, :cond_1

    .line 1005
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v0, v2

    :goto_0
    if-eqz v1, :cond_1

    .line 1006
    iget-object v3, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->item:Ljava/lang/Object;

    .line 1007
    iget-boolean v4, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-eqz v4, :cond_0

    .line 1008
    if-eqz v3, :cond_2

    if-eq v3, v1, :cond_2

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->tryMatchData()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1010
    invoke-virtual {p0, v0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->unsplice(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V

    .line 1011
    const/4 v0, 0x1

    .line 1024
    :goto_1
    return v0

    .line 1014
    :cond_0
    if-nez v3, :cond_2

    .line 1024
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1018
    :cond_2
    iget-object v0, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v0, v1, :cond_3

    .line 1020
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v1, v2

    :cond_3
    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 1022
    goto :goto_0
.end method

.method private firstDataItem()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 825
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    :goto_0
    if-eqz v1, :cond_0

    .line 826
    iget-object v2, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->item:Ljava/lang/Object;

    .line 827
    iget-boolean v3, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-eqz v3, :cond_1

    .line 828
    if-eqz v2, :cond_2

    if-eq v2, v1, :cond_2

    .line 829
    invoke-static {v2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 836
    :cond_0
    return-object v0

    .line 832
    :cond_1
    if-eqz v2, :cond_0

    .line 825
    :cond_2
    invoke-virtual {p0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v1

    goto :goto_0
.end method

.method private firstOfMode(Z)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 812
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    :goto_0
    if-eqz v0, :cond_2

    .line 813
    invoke-virtual {v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v2

    if-nez v2, :cond_1

    .line 814
    iget-boolean v2, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-ne v2, p1, :cond_0

    .line 817
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 814
    goto :goto_1

    .line 812
    :cond_1
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 817
    goto :goto_1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 1332
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1334
    :goto_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 1335
    if-nez v0, :cond_0

    .line 1341
    return-void

    .line 1338
    :cond_0
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static spinsFor(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Z)I
    .locals 1

    .prologue
    .line 781
    sget-boolean v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->MP:Z

    if-eqz v0, :cond_2

    if-eqz p0, :cond_2

    .line 782
    iget-boolean v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-eq v0, p1, :cond_0

    .line 783
    const/16 v0, 0xc0

    .line 792
    :goto_0
    return v0

    .line 785
    :cond_0
    invoke-virtual {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 786
    const/16 v0, 0x80

    goto :goto_0

    .line 788
    :cond_1
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->waiter:Ljava/lang/Thread;

    if-nez v0, :cond_2

    .line 789
    const/16 v0, 0x40

    goto :goto_0

    .line 792
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sweep()V
    .locals 3

    .prologue
    .line 985
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v1, :cond_1

    .line 986
    invoke-virtual {v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 988
    goto :goto_0

    .line 989
    :cond_0
    iget-object v2, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-nez v2, :cond_2

    .line 998
    :cond_1
    return-void

    .line 991
    :cond_2
    if-ne v1, v2, :cond_3

    .line 993
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    goto :goto_0

    .line 995
    :cond_3
    invoke-virtual {v0, v1, v2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->casNext(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    goto :goto_0
.end method

.method private tryAppend(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Z)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 690
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v4, v0

    .line 692
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-nez v0, :cond_1

    .line 693
    invoke-direct {p0, v3, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casHead(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 713
    :goto_1
    return-object p1

    .line 697
    :cond_1
    invoke-virtual {v0, p2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->cannotPrecede(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p1, v3

    .line 698
    goto :goto_1

    .line 699
    :cond_2
    iget-object v2, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v2, :cond_5

    .line 700
    if-eq v0, v4, :cond_3

    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eq v4, v1, :cond_3

    move-object v0, v1

    :goto_2
    move-object v4, v1

    goto :goto_0

    :cond_3
    if-eq v0, v2, :cond_4

    move-object v0, v2

    move-object v1, v4

    goto :goto_2

    :cond_4
    move-object v0, v3

    move-object v1, v4

    goto :goto_2

    .line 702
    :cond_5
    invoke-virtual {v0, v3, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->casNext(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 703
    iget-object v0, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    goto :goto_0

    .line 705
    :cond_6
    if-eq v0, v4, :cond_9

    .line 709
    :cond_7
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v1, v4, :cond_8

    invoke-direct {p0, v4, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casTail(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v1

    if-nez v1, :cond_9

    :cond_8
    iget-object v4, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tail:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v4, :cond_9

    iget-object v1, v4, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v1, :cond_9

    iget-object p1, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz p1, :cond_9

    if-ne p1, v4, :cond_7

    :cond_9
    move-object p1, v0

    .line 713
    goto :goto_1
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 1316
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1317
    invoke-virtual {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1318
    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 1321
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1322
    return-void
.end method

.method private xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZIJ)TE;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 630
    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    .line 631
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 633
    :cond_0
    const/4 v1, 0x0

    .line 637
    :cond_1
    iget-object v5, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v2, v5

    :goto_0
    if-eqz v5, :cond_2

    .line 638
    iget-boolean v6, v5, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    .line 639
    iget-object v7, v5, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->item:Ljava/lang/Object;

    .line 640
    if-eq v7, v5, :cond_b

    if-eqz v7, :cond_5

    move v0, v4

    :goto_1
    if-ne v0, v6, :cond_b

    .line 641
    if-ne v6, p2, :cond_6

    .line 664
    :cond_2
    if-eqz p3, :cond_4

    .line 665
    if-nez v1, :cond_3

    .line 666
    new-instance v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    invoke-direct {v1, p1, p2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;-><init>(Ljava/lang/Object;Z)V

    .line 668
    :cond_3
    invoke-direct {p0, v1, p2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->tryAppend(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Z)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v2

    .line 669
    if-eqz v2, :cond_1

    .line 672
    if-eq p3, v4, :cond_4

    .line 673
    const/4 v0, 0x3

    if-ne p3, v0, :cond_d

    :goto_2
    move-object v0, p0

    move-object v3, p1

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->awaitMatch(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Ljava/lang/Object;ZJ)Ljava/lang/Object;

    move-result-object p1

    .line 676
    :cond_4
    :goto_3
    return-object p1

    :cond_5
    move v0, v3

    .line 640
    goto :goto_1

    .line 644
    :cond_6
    invoke-virtual {v5, v7, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->casItem(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v5

    .line 645
    :cond_7
    if-eq v0, v2, :cond_8

    .line 646
    iget-object v1, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 647
    iget-object v3, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v3, v2, :cond_a

    if-nez v1, :cond_9

    :goto_4
    invoke-direct {p0, v2, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casHead(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 648
    invoke-virtual {v2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->forgetNext()V

    .line 656
    :cond_8
    :goto_5
    iget-object v0, v5, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->waiter:Ljava/lang/Thread;

    invoke-static {v0}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    .line 657
    invoke-static {v7}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_3

    :cond_9
    move-object v0, v1

    .line 647
    goto :goto_4

    .line 651
    :cond_a
    iget-object v2, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v2, :cond_8

    iget-object v0, v2, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_5

    .line 660
    :cond_b
    iget-object v0, v5, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 661
    if-eq v5, v0, :cond_c

    :goto_6
    move-object v5, v0

    .line 662
    goto :goto_0

    .line 661
    :cond_c
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-object v2, v0

    goto :goto_6

    :cond_d
    move v4, v3

    .line 673
    goto :goto_2
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1096
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    .line 1097
    return v2
.end method

.method public drainTo(Ljava/util/Collection;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-TE;>;)I"
        }
    .end annotation

    .prologue
    .line 1183
    if-nez p1, :cond_0

    .line 1184
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1186
    :cond_0
    if-ne p1, p0, :cond_1

    .line 1187
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1189
    :cond_1
    const/4 v0, 0x0

    .line 1191
    :goto_0
    invoke-virtual {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1192
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1193
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1195
    :cond_2
    return v0
.end method

.method public drainTo(Ljava/util/Collection;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-TE;>;I)I"
        }
    .end annotation

    .prologue
    .line 1203
    if-nez p1, :cond_0

    .line 1204
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1206
    :cond_0
    if-ne p1, p0, :cond_1

    .line 1207
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1209
    :cond_1
    const/4 v0, 0x0

    .line 1211
    :goto_0
    if-ge v0, p2, :cond_2

    invoke-virtual {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1212
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1215
    :cond_2
    return v0
.end method

.method public getWaitingConsumerCount()I
    .locals 1

    .prologue
    .line 1277
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->countOfMode(Z)I

    move-result v0

    return v0
.end method

.method public hasWaitingConsumer()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1256
    invoke-direct {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->firstOfMode(Z)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isEmpty()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1247
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    :goto_0
    if-eqz v1, :cond_0

    .line 1248
    invoke-virtual {v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1249
    iget-boolean v1, v1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-nez v1, :cond_1

    .line 1252
    :cond_0
    :goto_1
    return v0

    .line 1249
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1247
    :cond_2
    invoke-virtual {p0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v1

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1233
    new-instance v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;-><init>(Lorg/jboss/netty/util/internal/LinkedTransferQueue;)V

    return-object v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1082
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    .line 1083
    return v2
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1069
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    .line 1070
    return v2
.end method

.method public peek()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1237
    invoke-direct {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->firstDataItem()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public poll()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1175
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 1167
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    .line 1168
    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1169
    :cond_0
    return-object v0

    .line 1171
    :cond_1
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
.end method

.method public put(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1056
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    .line 1057
    return-void
.end method

.method public remainingCapacity()I
    .locals 1

    .prologue
    .line 1304
    const v0, 0x7fffffff

    return v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1293
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->findAndRemove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1273
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->countOfMode(Z)I

    move-result v0

    return v0
.end method

.method final succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;
    .locals 1

    .prologue
    .line 803
    iget-object v0, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 804
    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    :cond_0
    return-object v0
.end method

.method public take()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1158
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    .line 1159
    if-eqz v0, :cond_0

    .line 1160
    return-object v0

    .line 1162
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 1163
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
.end method

.method public transfer(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1126
    const/4 v2, 0x1

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1127
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 1128
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 1130
    :cond_0
    return-void
.end method

.method public tryTransfer(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1111
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public tryTransfer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1148
    const/4 v3, 0x3

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->xfer(Ljava/lang/Object;ZIJ)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1152
    :goto_0
    return v2

    .line 1151
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1152
    const/4 v2, 0x0

    goto :goto_0

    .line 1154
    :cond_1
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
.end method

.method final unsplice(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V
    .locals 2

    .prologue
    .line 934
    invoke-virtual {p2}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->forgetContents()V

    .line 942
    if-eqz p1, :cond_1

    if-eq p1, p2, :cond_1

    iget-object v0, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-ne v0, p2, :cond_1

    .line 943
    iget-object v0, p2, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 944
    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_1

    invoke-virtual {p1, p2, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->casNext(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 947
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 948
    if-eq v0, p1, :cond_1

    if-eq v0, p2, :cond_1

    if-nez v0, :cond_2

    .line 978
    :cond_1
    :goto_1
    return-void

    .line 951
    :cond_2
    invoke-virtual {v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isMatched()Z

    move-result v1

    if-nez v1, :cond_4

    .line 962
    iget-object v0, p1, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eq v0, p1, :cond_1

    iget-object v0, p2, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eq v0, p2, :cond_1

    .line 964
    :cond_3
    iget v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweepVotes:I

    .line 965
    const/16 v1, 0x20

    if-ge v0, v1, :cond_5

    .line 966
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casSweepVotes(II)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 954
    :cond_4
    iget-object v1, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->next:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 955
    if-eqz v1, :cond_1

    .line 958
    if-eq v1, v0, :cond_0

    invoke-direct {p0, v0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casHead(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 959
    invoke-virtual {v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->forgetNext()V

    goto :goto_0

    .line 970
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->casSweepVotes(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 971
    invoke-direct {p0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->sweep()V

    goto :goto_1
.end method
