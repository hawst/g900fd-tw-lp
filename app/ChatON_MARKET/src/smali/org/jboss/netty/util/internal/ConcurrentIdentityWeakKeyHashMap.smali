.class public final Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;
.super Ljava/util/AbstractMap;
.source "ConcurrentIdentityWeakKeyHashMap.java"

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final DEFAULT_CONCURRENCY_LEVEL:I = 0x10

.field static final DEFAULT_INITIAL_CAPACITY:I = 0x10

.field static final DEFAULT_LOAD_FACTOR:F = 0.75f

.field static final MAXIMUM_CAPACITY:I = 0x40000000

.field static final MAX_SEGMENTS:I = 0x10000

.field static final RETRIES_BEFORE_LOCK:I = 0x2


# instance fields
.field entrySet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field keySet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field final segmentMask:I

.field final segmentShift:I

.field final segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field values:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 742
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-direct {p0, v1, v0, v1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;-><init>(IFI)V

    .line 743
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 733
    const/high16 v0, 0x3f400000    # 0.75f

    const/16 v1, 0x10

    invoke-direct {p0, p1, v0, v1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;-><init>(IFI)V

    .line 734
    return-void
.end method

.method public constructor <init>(IF)V
    .locals 1

    .prologue
    .line 719
    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;-><init>(IFI)V

    .line 720
    return-void
.end method

.method public constructor <init>(IFI)V
    .locals 6

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v0, 0x10000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 667
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 668
    const/4 v4, 0x0

    cmpl-float v4, p2, v4

    if-lez v4, :cond_0

    if-ltz p1, :cond_0

    if-gtz p3, :cond_1

    .line 669
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 672
    :cond_1
    if-le p3, v0, :cond_2

    move p3, v0

    :cond_2
    move v4, v3

    move v0, v2

    .line 679
    :goto_0
    if-ge v4, p3, :cond_3

    .line 680
    add-int/lit8 v5, v0, 0x1

    .line 681
    shl-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v5

    goto :goto_0

    .line 683
    :cond_3
    rsub-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentShift:I

    .line 684
    add-int/lit8 v0, v4, -0x1

    iput v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentMask:I

    .line 685
    invoke-static {v4}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->newArray(I)[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v0

    iput-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    .line 687
    if-le p1, v1, :cond_4

    move p1, v1

    .line 690
    :cond_4
    div-int v0, p1, v4

    .line 691
    mul-int v1, v0, v4

    if-ge v1, p1, :cond_5

    .line 692
    add-int/lit8 v0, v0, 0x1

    :cond_5
    move v1, v3

    .line 695
    :goto_1
    if-ge v1, v0, :cond_6

    .line 696
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    move v0, v2

    .line 699
    :goto_2
    iget-object v2, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 700
    iget-object v2, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    new-instance v3, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    invoke-direct {v3, v1, p2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;-><init>(IF)V

    aput-object v3, v2, v0

    .line 699
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 702
    :cond_7
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    .line 754
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0, v1, v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;-><init>(IFI)V

    .line 757
    invoke-virtual {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->putAll(Ljava/util/Map;)V

    .line 758
    return-void
.end method

.method private static hash(I)I
    .locals 3

    .prologue
    .line 134
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 135
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 136
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 137
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 138
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 139
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method private hashOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 153
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hash(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 1078
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1079
    iget-object v1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->clear()V

    .line 1078
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1081
    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 963
    invoke-virtual {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 881
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 882
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->containsKey(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 898
    if-nez p1, :cond_0

    .line 899
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 904
    :cond_0
    iget-object v5, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    .line 905
    array-length v0, v5

    new-array v6, v0, [I

    move v4, v1

    .line 908
    :goto_0
    const/4 v0, 0x2

    if-ge v4, v0, :cond_5

    move v0, v1

    move v2, v1

    .line 910
    :goto_1
    array-length v7, v5

    if-ge v0, v7, :cond_2

    .line 911
    aget-object v7, v5, v0

    iget v7, v7, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    aput v7, v6, v0

    add-int/2addr v2, v7

    .line 912
    aget-object v7, v5, v0

    invoke-virtual {v7, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->containsValue(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 946
    :goto_2
    return v3

    .line 910
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 917
    :cond_2
    if-eqz v2, :cond_b

    move v0, v1

    .line 918
    :goto_3
    array-length v2, v5

    if-ge v0, v2, :cond_b

    .line 919
    aget v2, v6, v0

    aget-object v7, v5, v0

    iget v7, v7, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    if-eq v2, v7, :cond_3

    move v0, v1

    .line 925
    :goto_4
    if-eqz v0, :cond_4

    move v3, v1

    .line 926
    goto :goto_2

    .line 918
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 908
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 930
    :goto_5
    array-length v2, v5

    if-ge v0, v2, :cond_6

    .line 931
    aget-object v2, v5, v0

    invoke-virtual {v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->lock()V

    .line 930
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    move v0, v1

    .line 935
    :goto_6
    :try_start_0
    array-length v2, v5

    if-ge v0, v2, :cond_a

    .line 936
    aget-object v2, v5, v0

    invoke-virtual {v2, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->containsValue(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    move v0, v3

    .line 942
    :goto_7
    array-length v2, v5

    if-ge v1, v2, :cond_9

    .line 943
    aget-object v2, v5, v1

    invoke-virtual {v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->unlock()V

    .line 942
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 935
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 942
    :goto_8
    array-length v2, v5

    if-ge v1, v2, :cond_8

    .line 943
    aget-object v2, v5, v1

    invoke-virtual {v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->unlock()V

    .line 942
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_8
    throw v0

    :cond_9
    move v3, v0

    .line 946
    goto :goto_2

    .line 942
    :catchall_0
    move-exception v0

    goto :goto_8

    :cond_a
    move v0, v1

    goto :goto_7

    :cond_b
    move v0, v3

    goto :goto_4
.end method

.method public elements()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1180
    new-instance v0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$ValueIterator;-><init>(Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;)V

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1159
    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->entrySet:Ljava/util/Set;

    .line 1160
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$EntrySet;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$EntrySet;-><init>(Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;)V

    iput-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->entrySet:Ljava/util/Set;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 866
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 867
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 767
    iget-object v3, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    .line 776
    array-length v0, v3

    new-array v4, v0, [I

    move v0, v1

    move v2, v1

    .line 778
    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_2

    .line 779
    aget-object v5, v3, v0

    iget v5, v5, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->count:I

    if-eqz v5, :cond_1

    .line 795
    :cond_0
    :goto_1
    return v1

    .line 782
    :cond_1
    aget-object v5, v3, v0

    iget v5, v5, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    aput v5, v4, v0

    add-int/2addr v2, v5

    .line 778
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 788
    :cond_2
    if-eqz v2, :cond_3

    move v0, v1

    .line 789
    :goto_2
    array-length v2, v3

    if-ge v0, v2, :cond_3

    .line 790
    aget-object v2, v3, v0

    iget v2, v2, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->count:I

    if-nez v2, :cond_0

    aget v2, v4, v0

    aget-object v5, v3, v0

    iget v5, v5, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    if-ne v2, v5, :cond_0

    .line 789
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 795
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 1117
    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->keySet:Ljava/util/Set;

    .line 1118
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$KeySet;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$KeySet;-><init>(Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;)V

    iput-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->keySet:Ljava/util/Set;

    goto :goto_0
.end method

.method public keys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 1170
    new-instance v0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$KeyIterator;-><init>(Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;)V

    return-object v0
.end method

.method public purgeStaleEntries()V
    .locals 2

    .prologue
    .line 1095
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1096
    iget-object v1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->removeStale()V

    .line 1095
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1098
    :cond_0
    return-void
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 981
    if-nez p2, :cond_0

    .line 982
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 984
    :cond_0
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 985
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->put(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1012
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1013
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1015
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 996
    if-nez p2, :cond_0

    .line 997
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 999
    :cond_0
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 1000
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->put(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 1028
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 1029
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v0, v2, v3}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->remove(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1038
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v1

    .line 1039
    if-nez p2, :cond_1

    .line 1042
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v2

    invoke-virtual {v2, p1, v1, p2, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->remove(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 1066
    if-nez p2, :cond_0

    .line 1067
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1069
    :cond_0
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 1070
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->replace(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 1051
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1052
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1054
    :cond_1
    invoke-direct {p0, p1}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->hashOf(Ljava/lang/Object;)I

    move-result v0

    .line 1055
    invoke-virtual {p0, v0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->replace(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final segmentFor(I)Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    iget v1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentShift:I

    ushr-int v1, p1, v1

    iget v2, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segmentMask:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public size()I
    .locals 15

    .prologue
    .line 807
    iget-object v7, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->segments:[Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;

    .line 808
    const-wide/16 v3, 0x0

    .line 809
    const-wide/16 v1, 0x0

    .line 810
    array-length v0, v7

    new-array v8, v0, [I

    .line 813
    const/4 v0, 0x0

    move v6, v0

    move-wide v11, v1

    move-wide v0, v11

    move-wide v13, v3

    move-wide v2, v13

    :goto_0
    const/4 v4, 0x2

    if-ge v6, v4, :cond_8

    .line 814
    const-wide/16 v4, 0x0

    .line 815
    const-wide/16 v2, 0x0

    .line 816
    const/4 v1, 0x0

    .line 817
    const/4 v0, 0x0

    :goto_1
    array-length v9, v7

    if-ge v0, v9, :cond_0

    .line 818
    aget-object v9, v7, v0

    iget v9, v9, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->count:I

    int-to-long v9, v9

    add-long/2addr v2, v9

    .line 819
    aget-object v9, v7, v0

    iget v9, v9, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    aput v9, v8, v0

    add-int/2addr v1, v9

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 821
    :cond_0
    if-eqz v1, :cond_7

    .line 822
    const/4 v0, 0x0

    :goto_2
    array-length v1, v7

    if-ge v0, v1, :cond_7

    .line 823
    aget-object v1, v7, v0

    iget v1, v1, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->count:I

    int-to-long v9, v1

    add-long/2addr v4, v9

    .line 824
    aget v1, v8, v0

    aget-object v9, v7, v0

    iget v9, v9, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->modCount:I

    if-eq v1, v9, :cond_1

    .line 825
    const-wide/16 v4, -0x1

    move-wide v0, v4

    .line 830
    :goto_3
    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    move-wide v11, v0

    move-wide v13, v2

    move-wide v1, v13

    move-wide v3, v11

    .line 834
    :goto_4
    cmp-long v0, v3, v1

    if-eqz v0, :cond_5

    .line 835
    const-wide/16 v1, 0x0

    .line 836
    const/4 v0, 0x0

    :goto_5
    array-length v3, v7

    if-ge v0, v3, :cond_3

    .line 837
    aget-object v3, v7, v0

    invoke-virtual {v3}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->lock()V

    .line 836
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 822
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 813
    :cond_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 839
    :cond_3
    const/4 v0, 0x0

    :goto_6
    array-length v3, v7

    if-ge v0, v3, :cond_4

    .line 840
    aget-object v3, v7, v0

    iget v3, v3, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->count:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 842
    :cond_4
    const/4 v0, 0x0

    :goto_7
    array-length v3, v7

    if-ge v0, v3, :cond_5

    .line 843
    aget-object v3, v7, v0

    invoke-virtual {v3}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Segment;->unlock()V

    .line 842
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 846
    :cond_5
    const-wide/32 v3, 0x7fffffff

    cmp-long v0, v1, v3

    if-lez v0, :cond_6

    .line 847
    const v0, 0x7fffffff

    .line 849
    :goto_8
    return v0

    :cond_6
    long-to-int v0, v1

    goto :goto_8

    :cond_7
    move-wide v0, v4

    goto :goto_3

    :cond_8
    move-wide v11, v0

    move-wide v13, v2

    move-wide v1, v13

    move-wide v3, v11

    goto :goto_4
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1138
    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->values:Ljava/util/Collection;

    .line 1139
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Values;

    invoke-direct {v0, p0}, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap$Values;-><init>(Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;)V

    iput-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityWeakKeyHashMap;->values:Ljava/util/Collection;

    goto :goto_0
.end method
