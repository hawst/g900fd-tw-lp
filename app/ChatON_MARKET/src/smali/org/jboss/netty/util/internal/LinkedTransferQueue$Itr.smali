.class final Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;
.super Ljava/lang/Object;
.source "LinkedTransferQueue.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private lastPred:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

.field private lastRet:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

.field private nextItem:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private nextNode:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

.field final synthetic this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;


# direct methods
.method constructor <init>(Lorg/jboss/netty/util/internal/LinkedTransferQueue;)V
    .locals 1

    .prologue
    .line 894
    iput-object p1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 895
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->advance(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V

    .line 896
    return-void
.end method

.method private advance(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V
    .locals 3

    .prologue
    .line 875
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->lastRet:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    iput-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->lastPred:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 876
    iput-object p1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->lastRet:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 877
    if-nez p1, :cond_0

    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    iget-object v0, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->head:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 878
    :goto_0
    if-eqz v0, :cond_2

    .line 879
    iget-object v1, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->item:Ljava/lang/Object;

    .line 880
    iget-boolean v2, v0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->isData:Z

    if-eqz v2, :cond_1

    .line 881
    if-eqz v1, :cond_3

    if-eq v1, v0, :cond_3

    .line 882
    invoke-static {v1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextItem:Ljava/lang/Object;

    .line 883
    iput-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextNode:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 892
    :goto_1
    return-void

    .line 877
    :cond_0
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    invoke-virtual {v0, p1}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v0

    goto :goto_0

    .line 887
    :cond_1
    if-nez v1, :cond_3

    .line 891
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextNode:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    goto :goto_1

    .line 878
    :cond_3
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    invoke-virtual {v1, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->succ(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 899
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextNode:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 903
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextNode:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 904
    if-nez v0, :cond_0

    .line 905
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 907
    :cond_0
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->nextItem:Ljava/lang/Object;

    .line 908
    invoke-direct {p0, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->advance(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V

    .line 909
    return-object v1
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 913
    iget-object v0, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->lastRet:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    .line 914
    if-nez v0, :cond_0

    .line 915
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 917
    :cond_0
    invoke-virtual {v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;->tryMatchData()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 918
    iget-object v1, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->this$0:Lorg/jboss/netty/util/internal/LinkedTransferQueue;

    iget-object v2, p0, Lorg/jboss/netty/util/internal/LinkedTransferQueue$Itr;->lastPred:Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;

    invoke-virtual {v1, v2, v0}, Lorg/jboss/netty/util/internal/LinkedTransferQueue;->unsplice(Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;Lorg/jboss/netty/util/internal/LinkedTransferQueue$Node;)V

    .line 920
    :cond_1
    return-void
.end method
