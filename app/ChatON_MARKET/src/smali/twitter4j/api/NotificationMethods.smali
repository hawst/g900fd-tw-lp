.class public interface abstract Ltwitter4j/api/NotificationMethods;
.super Ljava/lang/Object;
.source "NotificationMethods.java"


# virtual methods
.method public abstract disableNotification(J)Ltwitter4j/User;
.end method

.method public abstract disableNotification(Ljava/lang/String;)Ltwitter4j/User;
.end method

.method public abstract enableNotification(J)Ltwitter4j/User;
.end method

.method public abstract enableNotification(Ljava/lang/String;)Ltwitter4j/User;
.end method
