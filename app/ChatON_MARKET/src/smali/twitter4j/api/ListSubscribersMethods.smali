.class public interface abstract Ltwitter4j/api/ListSubscribersMethods;
.super Ljava/lang/Object;
.source "ListSubscribersMethods.java"


# virtual methods
.method public abstract checkUserListSubscription(Ljava/lang/String;IJ)Ltwitter4j/User;
.end method

.method public abstract createUserListSubscription(I)Ltwitter4j/UserList;
.end method

.method public abstract destroyUserListSubscription(I)Ltwitter4j/UserList;
.end method

.method public abstract getUserListSubscribers(IJ)Ltwitter4j/PagableResponseList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserListSubscribers(Ljava/lang/String;IJ)Ltwitter4j/PagableResponseList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJ)",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showUserListSubscription(IJ)Ltwitter4j/User;
.end method

.method public abstract subscribeUserList(Ljava/lang/String;I)Ltwitter4j/UserList;
.end method

.method public abstract unsubscribeUserList(Ljava/lang/String;I)Ltwitter4j/UserList;
.end method
