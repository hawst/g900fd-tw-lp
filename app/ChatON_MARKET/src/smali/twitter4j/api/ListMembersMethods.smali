.class public interface abstract Ltwitter4j/api/ListMembersMethods;
.super Ljava/lang/Object;
.source "ListMembersMethods.java"


# virtual methods
.method public abstract addUserListMember(IJ)Ltwitter4j/UserList;
.end method

.method public abstract addUserListMembers(I[J)Ltwitter4j/UserList;
.end method

.method public abstract addUserListMembers(I[Ljava/lang/String;)Ltwitter4j/UserList;
.end method

.method public abstract checkUserListMembership(Ljava/lang/String;IJ)Ltwitter4j/User;
.end method

.method public abstract deleteUserListMember(IJ)Ltwitter4j/UserList;
.end method

.method public abstract getUserListMembers(IJ)Ltwitter4j/PagableResponseList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserListMembers(JIJ)Ltwitter4j/PagableResponseList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ)",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserListMembers(Ljava/lang/String;IJ)Ltwitter4j/PagableResponseList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJ)",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showUserListMembership(IJ)Ltwitter4j/User;
.end method
