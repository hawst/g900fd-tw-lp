.class Lcom/renren/android/c;
.super Ljava/lang/Object;
.source "Renren.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/common/b;

.field final synthetic b:Lcom/renren/android/Renren;


# direct methods
.method constructor <init>(Lcom/renren/android/Renren;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/renren/android/c;->b:Lcom/renren/android/Renren;

    iput-object p2, p0, Lcom/renren/android/c;->a:Lcom/sec/chaton/settings/tellfriends/common/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 214
    const-string v0, "Renren-SDK"

    const-string v1, "Login canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/renren/android/c;->a:Lcom/sec/chaton/settings/tellfriends/common/b;

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a()V

    .line 216
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/a;)V
    .locals 3

    .prologue
    .line 190
    const-string v0, "Renren-SDK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to login by error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/renren/android/c;->a:Lcom/sec/chaton/settings/tellfriends/common/b;

    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 192
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 196
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 197
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 198
    check-cast v0, Ljava/lang/String;

    .line 199
    if-eqz v0, :cond_0

    .line 200
    const-string v1, "Renren-SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Success obtain access_token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :try_start_0
    iget-object v1, p0, Lcom/renren/android/c;->b:Lcom/renren/android/Renren;

    invoke-virtual {v1, v0}, Lcom/renren/android/Renren;->a(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/renren/android/c;->a:Lcom/sec/chaton/settings/tellfriends/common/b;

    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v0

    .line 205
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 206
    iget-object v1, p0, Lcom/renren/android/c;->a:Lcom/sec/chaton/settings/tellfriends/common/b;

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    const/16 v4, -0x3ef

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v4, v0}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    goto :goto_0
.end method
