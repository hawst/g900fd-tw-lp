.class public Lcom/renren/android/c/f;
.super Lcom/renren/android/a/a;
.source "UsersGetInfoRequestParam.java"


# instance fields
.field private a:[Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/renren/android/a/a;-><init>()V

    .line 62
    const-string v0, "uid,name,tinyurl,headurl,zidou,star"

    iput-object v0, p0, Lcom/renren/android/c/f;->b:Ljava/lang/String;

    .line 69
    iput-object p1, p0, Lcom/renren/android/c/f;->a:[Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 7

    .prologue
    .line 117
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 118
    const-string v0, "method"

    const-string v2, "users.getInfo"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/renren/android/c/f;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "fields"

    iget-object v2, p0, Lcom/renren/android/c/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/renren/android/c/f;->a:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 123
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 124
    iget-object v3, p0, Lcom/renren/android/c/f;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 125
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 128
    const-string v0, "uids"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_2
    return-object v1
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/renren/android/c/f;->b:Ljava/lang/String;

    .line 112
    return-void
.end method
