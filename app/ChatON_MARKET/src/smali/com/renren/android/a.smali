.class Lcom/renren/android/a;
.super Ljava/lang/Object;
.source "AccessTokenManager.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lcom/renren/android/AccessTokenManager;


# direct methods
.method constructor <init>(Lcom/renren/android/AccessTokenManager;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    iput-object p2, p0, Lcom/renren/android/a;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 186
    iget-object v0, p0, Lcom/renren/android/a;->a:Landroid/content/SharedPreferences;

    const-string v1, "renren_sdk_config_prop_session_key"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/renren/android/a;->a:Landroid/content/SharedPreferences;

    const-string v2, "renren_sdk_config_prop_session_secret"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    iget-object v2, p0, Lcom/renren/android/a;->a:Landroid/content/SharedPreferences;

    const-string v3, "renren_sdk_config_prop_user_id"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 193
    iget-object v4, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v4}, Lcom/renren/android/AccessTokenManager;->a(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v4}, Lcom/renren/android/AccessTokenManager;->a(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_0
    iget-object v4, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v4}, Lcom/renren/android/AccessTokenManager;->b(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v4}, Lcom/renren/android/AccessTokenManager;->a(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    iget-object v4, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v4}, Lcom/renren/android/AccessTokenManager;->a(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    if-nez v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v0}, Lcom/renren/android/AccessTokenManager;->b(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    if-nez v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v0}, Lcom/renren/android/AccessTokenManager;->c(Lcom/renren/android/AccessTokenManager;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 198
    :cond_4
    iget-object v0, p0, Lcom/renren/android/a;->b:Lcom/renren/android/AccessTokenManager;

    invoke-static {v0}, Lcom/renren/android/AccessTokenManager;->d(Lcom/renren/android/AccessTokenManager;)V

    .line 200
    :cond_5
    return-void
.end method
