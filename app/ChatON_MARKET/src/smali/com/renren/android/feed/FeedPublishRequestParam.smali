.class public Lcom/renren/android/feed/FeedPublishRequestParam;
.super Lcom/renren/android/a/a;
.source "FeedPublishRequestParam.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final ACTION_NAME_MAX_LENGTH:I = 0xa

.field public static final ACTION_NAME_TOO_LONG:I = 0x10000

.field private static final CAPTION_MAX_LENGTH:I = 0x14

.field public static final CAPTION_TOO_LONG:I = 0x1000

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/renren/android/feed/FeedPublishRequestParam;",
            ">;"
        }
    .end annotation
.end field

.field private static final DESCRIPTION_MAX_LENGTH:I = 0xc8

.field public static final DESCRIPTION_TOO_LONG:I = 0x100

.field private static final MESSAGE_MAX_LENGTH:I = 0xc8

.field public static final MESSAGE_TOO_LONG:I = 0x100000

.field private static final METHOD:Ljava/lang/String; = "feed.publishFeed"

.field private static final NAME_MAX_LENGTH:I = 0x1e

.field public static final NAME_TOO_LONG:I = 0x10

.field public static final NORMAL_LENGTH:I = 0x0

.field private static final WIDGET_DESCRIPTION_MAX_LENGTH:I = 0x78

.field private static final WIDGET_MESSAGE_MAX_LENGTH:I = 0x8c


# instance fields
.field private actionLink:Ljava/lang/String;

.field private actionName:Ljava/lang/String;

.field private caption:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private imageUrl:Ljava/lang/String;

.field private message:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/renren/android/feed/a;

    invoke-direct {v0}, Lcom/renren/android/feed/a;-><init>()V

    sput-object v0, Lcom/renren/android/feed/FeedPublishRequestParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/renren/android/a/a;-><init>()V

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 132
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    .line 133
    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    .line 134
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    .line 135
    const-string v1, "image_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    .line 136
    const-string v1, "caption"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    .line 137
    const-string v1, "action_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    .line 138
    const-string v1, "action_link"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    .line 139
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/renren/android/a/a;-><init>()V

    .line 144
    iput-object p1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    .line 145
    iput-object p2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    .line 146
    iput-object p3, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    .line 147
    iput-object p4, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    .line 148
    iput-object p5, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    .line 149
    iput-object p6, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    .line 150
    iput-object p7, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    .line 151
    iput-object p8, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    .line 152
    return-void
.end method

.method private b()I
    .locals 4

    .prologue
    const/16 v3, 0xc8

    .line 269
    const/4 v0, 0x0

    .line 270
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_0

    .line 271
    const/16 v0, 0x10

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 276
    or-int/lit16 v0, v0, 0x100

    .line 279
    :cond_1
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_2

    .line 280
    or-int/lit16 v0, v0, 0x1000

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_3

    .line 285
    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 288
    :cond_3
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_4

    .line 289
    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    .line 292
    :cond_4
    return v0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 336
    :cond_0
    const-string v0, "Required parameter could not be null."

    .line 337
    new-instance v1, Lcom/renren/android/f;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v0, v0}, Lcom/renren/android/f;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 342
    :cond_1
    invoke-direct {p0}, Lcom/renren/android/feed/FeedPublishRequestParam;->b()I

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    const-string v0, "Some parameter is illegal for feed."

    .line 344
    new-instance v1, Lcom/renren/android/f;

    const/4 v2, -0x3

    invoke-direct {v1, v2, v0, v0}, Lcom/renren/android/f;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 349
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 350
    const-string v1, "method"

    const-string v2, "feed.publishFeed"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v1, "name"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v1, "description"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const-string v1, "url"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 355
    const-string v1, "image"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_3
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 358
    const-string v1, "caption"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_4
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 361
    const-string v1, "action_name"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_5
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 364
    const-string v1, "action_link"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_6
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 367
    const-string v1, "message"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_7
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 161
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 163
    const-string v1, "name"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 166
    const-string v1, "description"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->description:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 169
    const-string v1, "url"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 172
    const-string v1, "image_url"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 175
    const-string v1, "caption"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_4
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 178
    const-string v1, "action_name"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_5
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 181
    const-string v1, "action_link"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->actionLink:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_6
    iget-object v1, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 184
    const-string v1, "message"

    iget-object v2, p0, Lcom/renren/android/feed/FeedPublishRequestParam;->message:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 187
    return-void
.end method
