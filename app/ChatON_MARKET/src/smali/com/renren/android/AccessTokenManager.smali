.class Lcom/renren/android/AccessTokenManager;
.super Ljava/lang/Object;
.source "AccessTokenManager.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/renren/android/AccessTokenManager;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_ACCESS_TOKEN:Ljava/lang/String; = "renren_token_manager_access_token"

.field private static final KEY_SESSION_KEY:Ljava/lang/String; = "renren_token_manager_session_key"

.field private static final KEY_SESSION_KEY_EXPIRE_TIME:Ljava/lang/String; = "renren_token_manager_session_key_expire_time"

.field private static final KEY_SESSION_SECRET:Ljava/lang/String; = "renren_token_manager_session_secret"

.field private static final KEY_UID:Ljava/lang/String; = "renren_token_manager_user_id"

.field private static final ONE_HOUR:J = 0x36ee80L

.field private static final RENREN_SDK_CONFIG:Ljava/lang/String; = "renren_sdk_config"

.field private static final RENREN_SDK_CONFIG_PROP_ACCESS_TOKEN:Ljava/lang/String; = "renren_sdk_config_prop_access_token"

.field private static final RENREN_SDK_CONFIG_PROP_CREATE_TIME:Ljava/lang/String; = "renren_sdk_config_prop_create_time"

.field private static final RENREN_SDK_CONFIG_PROP_EXPIRE_SECONDS:Ljava/lang/String; = "renren_sdk_config_prop_expire_secends"

.field private static final RENREN_SDK_CONFIG_PROP_SESSION_CREATE_TIME:Ljava/lang/String; = "renren_sdk_config_prop_session_create_time"

.field private static final RENREN_SDK_CONFIG_PROP_SESSION_KEY:Ljava/lang/String; = "renren_sdk_config_prop_session_key"

.field private static final RENREN_SDK_CONFIG_PROP_SESSION_SECRET:Ljava/lang/String; = "renren_sdk_config_prop_session_secret"

.field private static final RENREN_SDK_CONFIG_PROP_USER_ID:Ljava/lang/String; = "renren_sdk_config_prop_user_id"


# instance fields
.field private accessToken:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private expireTime:J

.field private sessionKey:Ljava/lang/String;

.field private sessionSecret:Ljava/lang/String;

.field private uid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 427
    new-instance v0, Lcom/renren/android/b;

    invoke-direct {v0}, Lcom/renren/android/b;-><init>()V

    sput-object v0, Lcom/renren/android/AccessTokenManager;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 62
    iput-object p1, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    .line 63
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->h()V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 67
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 68
    if-eqz v0, :cond_0

    .line 69
    const-string v1, "renren_token_manager_access_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 70
    const-string v1, "renren_token_manager_session_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 71
    const-string v1, "renren_token_manager_session_secret"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 72
    const-string v1, "renren_token_manager_session_key_expire_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 73
    const-string v1, "renren_token_manager_user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    .line 75
    :cond_0
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->h()V

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 3

    .prologue
    .line 340
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 341
    :cond_0
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    .line 352
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 346
    const-string v1, "renren_sdk_config_prop_session_key"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 347
    const-string v1, "renren_sdk_config_prop_session_secret"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 348
    const-string v1, "renren_sdk_config_prop_expire_secends"

    invoke-interface {v0, v1, p5, p6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 349
    const-string v1, "renren_sdk_config_prop_session_create_time"

    invoke-interface {v0, v1, p3, p4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 350
    const-string v1, "renren_sdk_config_prop_user_id"

    invoke-interface {v0, v1, p7, p8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 351
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 247
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iput-object p1, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 252
    :try_start_0
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 254
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 255
    :cond_2
    invoke-direct {p0, p1}, Lcom/renren/android/AccessTokenManager;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_3
    :goto_1
    if-eqz p2, :cond_4

    .line 262
    invoke-direct {p0, p1}, Lcom/renren/android/AccessTokenManager;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    .line 258
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 259
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    goto :goto_1

    .line 264
    :cond_4
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/renren/android/AccessTokenManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 269
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 273
    const-string v1, "oauth_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    sget-object v1, Lcom/sec/chaton/c/b;->o:Ljava/lang/String;

    const-string v2, "POST"

    invoke-static {v1, v2, v0}, Lcom/renren/android/g;->b(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 276
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 277
    const-string v0, "error"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    if-eqz v0, :cond_2

    .line 279
    new-instance v0, Lcom/renren/android/e;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/renren/android/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :catch_0
    move-exception v0

    .line 298
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 281
    :cond_2
    :try_start_1
    const-string v0, "renren_token"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "session_key"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 283
    const-string v0, "renren_token"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "session_secret"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 286
    const-string v0, "user"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "id"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    .line 288
    const-string v0, "renren_token"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "expires_in"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v5, v0, v2

    .line 290
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 291
    add-long v0, v3, v5

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 293
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    iget-wide v7, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/renren/android/AccessTokenManager;->a(Ljava/lang/String;Ljava/lang/String;JJJ)V

    .line 294
    const-string v0, "Renren-SDK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---login success sessionKey:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expires:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionSecret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/renren/android/AccessTokenManager;)J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    return-wide v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 310
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 312
    if-eqz p1, :cond_0

    .line 313
    const-string v1, "renren_sdk_config_prop_access_token"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 314
    const-string v1, "renren_sdk_config_prop_create_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 319
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 320
    return-void

    .line 317
    :cond_0
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/renren/android/AccessTokenManager;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 182
    new-instance v1, Lcom/renren/android/a;

    invoke-direct {v1, p0, v0}, Lcom/renren/android/a;-><init>(Lcom/renren/android/AccessTokenManager;Landroid/content/SharedPreferences;)V

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0
.end method

.method private declared-synchronized i()V
    .locals 7

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 229
    const-string v1, "renren_sdk_config_prop_session_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 230
    const-string v1, "renren_sdk_config_prop_session_secret"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 231
    const-string v1, "renren_sdk_config_prop_user_id"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    .line 232
    const-string v1, "renren_sdk_config_prop_expire_secends"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 233
    const-string v3, "renren_sdk_config_prop_session_create_time"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 235
    add-long v0, v3, v1

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 237
    iget-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v0, v0, v5

    if-gez v0, :cond_0

    .line 238
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->j()V

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 241
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 242
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/renren/android/AccessTokenManager;->uid:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :cond_0
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private j()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 360
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 362
    const-string v1, "renren_sdk_config_prop_session_key"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 363
    const-string v1, "renren_sdk_config_prop_session_secret"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 364
    const-string v1, "renren_sdk_config_prop_expire_secends"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 365
    const-string v1, "renren_sdk_config_prop_session_create_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 366
    const-string v1, "renren_sdk_config_prop_user_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 367
    iput-object v3, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 368
    iput-object v3, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 369
    iput-wide v4, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 370
    iput-wide v4, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    .line 371
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 372
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 375
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v2, "renren_sdk_config"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 377
    const-string v1, "renren_sdk_config_prop_access_token"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    if-nez v1, :cond_0

    .line 391
    :goto_0
    return-object v0

    .line 383
    :cond_0
    const-string v3, "renren_sdk_config_prop_create_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 384
    const-string v4, "\\."

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 385
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 387
    add-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    sub-long v4, v6, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 388
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 391
    goto :goto_0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/renren/android/AccessTokenManager;->a(Ljava/lang/String;Z)V

    .line 148
    return-void
.end method

.method b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 87
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 88
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->j()V

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    return-object v0
.end method

.method c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 95
    :cond_0
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 97
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 98
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->j()V

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    return-object v0
.end method

.method d()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 123
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 129
    :cond_1
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 130
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 134
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()V
    .locals 4

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 157
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/renren/android/AccessTokenManager;->i()V

    .line 159
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/renren/android/AccessTokenManager;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_2
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 164
    invoke-virtual {p0}, Lcom/renren/android/AccessTokenManager;->g()V

    goto :goto_0
.end method

.method g()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lcom/renren/android/AccessTokenManager;->context:Landroid/content/Context;

    const-string v1, "renren_sdk_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 207
    const-string v1, "renren_sdk_config_prop_access_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 208
    const-string v1, "renren_sdk_config_prop_create_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 209
    const-string v1, "renren_sdk_config_prop_session_key"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 210
    const-string v1, "renren_sdk_config_prop_session_secret"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 211
    const-string v1, "renren_sdk_config_prop_expire_secends"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 212
    const-string v1, "renren_sdk_config_prop_user_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 213
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 214
    iput-object v3, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    .line 215
    iput-object v3, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    .line 216
    iput-object v3, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    .line 217
    iput-wide v4, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    .line 218
    iput-wide v4, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    .line 219
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 402
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 404
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 405
    const-string v1, "renren_token_manager_access_token"

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 409
    const-string v1, "renren_token_manager_session_key"

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->sessionKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_1
    iget-object v1, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 413
    const-string v1, "renren_token_manager_session_secret"

    iget-object v2, p0, Lcom/renren/android/AccessTokenManager;->sessionSecret:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_2
    iget-wide v1, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_3

    .line 417
    const-string v1, "renren_token_manager_session_key_expire_time"

    iget-wide v2, p0, Lcom/renren/android/AccessTokenManager;->expireTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 420
    :cond_3
    iget-wide v1, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_4

    .line 421
    const-string v1, "renren_token_manager_user_id"

    iget-wide v2, p0, Lcom/renren/android/AccessTokenManager;->uid:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 424
    :cond_4
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 425
    return-void
.end method
