.class public Lcom/renren/android/Renren;
.super Ljava/lang/Object;
.source "Renren.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CANCEL_URI:Ljava/lang/String; = "rrconnect://cancel"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/renren/android/Renren;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_PERMISSIONS:[Ljava/lang/String;

.field private static final KEY_API_KEY:Ljava/lang/String; = "api_key"

.field private static final KEY_APP_ID:Ljava/lang/String; = "appid"

.field private static final KEY_SECRET:Ljava/lang/String; = "secret"

.field private static final LOG_TAG:Ljava/lang/String; = "Renren"

.field private static final LOG_TAG_REQUEST:Ljava/lang/String; = "Renren-SDK-Request"

.field private static final LOG_TAG_RESPONSE:Ljava/lang/String; = "Renren-SDK-Response"

.field public static final RENREN_LABEL:Ljava/lang/String; = "Renren"

.field public static final RESPONSE_FORMAT_JSON:Ljava/lang/String; = "json"

.field public static final RESPONSE_FORMAT_XML:Ljava/lang/String; = "xml"

.field private static final SIGNATURE:Ljava/lang/String; = "4382fbee4f43aa1ed294ae9052e86a61"

.field public static final SUCCESS_URI:Ljava/lang/String; = "rrconnect://success"


# instance fields
.field private accessTokenManager:Lcom/renren/android/AccessTokenManager;

.field private apiKey:Ljava/lang/String;

.field private appId:Ljava/lang/String;

.field private secret:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "publish_feed"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "create_album"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_upload"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "read_user_album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "status_update"

    aput-object v2, v0, v1

    sput-object v0, Lcom/renren/android/Renren;->DEFAULT_PERMISSIONS:[Ljava/lang/String;

    .line 421
    new-instance v0, Lcom/renren/android/d;

    invoke-direct {v0}, Lcom/renren/android/d;-><init>()V

    sput-object v0, Lcom/renren/android/Renren;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 103
    const-string v1, "api_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    .line 104
    const-string v1, "secret"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/renren/android/Renren;->secret:Ljava/lang/String;

    .line 105
    const-string v1, "appid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/renren/android/Renren;->appId:Ljava/lang/String;

    .line 106
    sget-object v0, Lcom/renren/android/AccessTokenManager;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/renren/android/AccessTokenManager;

    iput-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    if-nez p1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "apiKey\u5fc5\u987b\u63d0\u4f9b"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_0
    if-nez p2, :cond_1

    .line 89
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "secret\u5fc5\u987b\u63d0\u4f9b"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    if-nez p3, :cond_2

    .line 92
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "appId\u5fc5\u987b\u63d0\u4f9b"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_2
    iput-object p1, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    .line 95
    iput-object p2, p0, Lcom/renren/android/Renren;->secret:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Lcom/renren/android/Renren;->appId:Ljava/lang/String;

    .line 98
    invoke-virtual {p0, p4}, Lcom/renren/android/Renren;->a(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 450
    const-string v0, "format"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {p0}, Lcom/renren/android/Renren;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    const-string v0, "session_key"

    iget-object v1, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v1}, Lcom/renren/android/AccessTokenManager;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_0
    invoke-direct {p0, p1}, Lcom/renren/android/Renren;->c(Landroid/os/Bundle;)V

    .line 455
    invoke-direct {p0, p1}, Lcom/renren/android/Renren;->b(Landroid/os/Bundle;)V

    .line 456
    sget-object v0, Lcom/sec/chaton/c/b;->t:Ljava/lang/String;

    const-string v1, "POST"

    invoke-static {v0, v1, p1}, Lcom/renren/android/g;->b(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 457
    const-string v1, "method"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/renren/android/Renren;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    return-object v0
.end method

.method private a(Landroid/app/Activity;[Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 233
    invoke-static {p1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 235
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 236
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v1, "redirect_uri"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v1, "response_type"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v1, "display"

    const-string v2, "touch"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    if-nez p2, :cond_0

    .line 243
    sget-object p2, Lcom/renren/android/Renren;->DEFAULT_PERMISSIONS:[Ljava/lang/String;

    .line 246
    :cond_0
    if-eqz p2, :cond_1

    array-length v1, p2

    if-lez v1, :cond_1

    .line 247
    const-string v1, " "

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 248
    const-string v2, "scope"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/chaton/c/b;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/renren/android/g;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    const-string v1, "android.permission.INTERNET"

    invoke-virtual {p1, v1}, Landroid/app/Activity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    const-string v0, "Error"

    const-string v1, "Application requires permission to access the Internet"

    invoke-static {p1, v0, v1}, Lcom/renren/android/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_2
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/e;

    invoke-direct {v1, p1, v0, p3}, Lcom/sec/chaton/settings/tellfriends/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/e;->show()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 482
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 483
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 484
    const-string v1, "method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 485
    const-string v1, "Renren-SDK-Response"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    :cond_0
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 465
    if-eqz p1, :cond_0

    .line 466
    const-string v0, "method"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_1

    .line 468
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 469
    const-string v2, "method="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 470
    const-string v0, "Renren-SDK-Request"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const-string v0, "Renren-SDK-Request"

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 490
    const-string v0, "api_key"

    iget-object v1, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string v0, "v"

    const-string v1, "1.0"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string v0, "call_id"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string v0, "xn_ss"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 496
    new-instance v0, Ljava/util/TreeSet;

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 497
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 498
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 499
    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 500
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 503
    const-string v0, "sig"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/renren/android/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    const-string v0, "json"

    invoke-direct {p0, p1, v0}, Lcom/renren/android/Renren;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;[Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 6

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/renren/android/Renren;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p3, v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V

    .line 226
    :goto_0
    return-void

    .line 186
    :cond_0
    new-instance v3, Lcom/renren/android/c;

    invoke-direct {v3, p0, p3}, Lcom/renren/android/c;-><init>(Lcom/renren/android/Renren;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    .line 224
    sget-object v4, Lcom/sec/chaton/c/b;->q:Ljava/lang/String;

    const-string v5, "token"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/renren/android/Renren;->a(Landroid/app/Activity;[Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 117
    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {p1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "Renren"

    const-string v1, "App miss permission android.permission.ACCESS_NETWORK_STATE! Some mobile\'s WebView don\'t display page!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    new-instance v0, Lcom/renren/android/AccessTokenManager;

    invoke-direct {v0, p1}, Lcom/renren/android/AccessTokenManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    .line 126
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->f()V

    .line 127
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0, p1}, Lcom/renren/android/AccessTokenManager;->a(Ljava/lang/String;)V

    .line 143
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->e()Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->a()Ljava/lang/String;

    move-result-object v0

    .line 270
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 271
    const/4 v0, 0x1

    .line 273
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/renren/android/Renren;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->a()Ljava/lang/String;

    move-result-object v0

    .line 299
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0}, Lcom/renren/android/AccessTokenManager;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 408
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 409
    iget-object v1, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 410
    const-string v1, "api_key"

    iget-object v2, p0, Lcom/renren/android/Renren;->apiKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    iget-object v1, p0, Lcom/renren/android/Renren;->secret:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 413
    const-string v1, "secret"

    iget-object v2, p0, Lcom/renren/android/Renren;->secret:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/renren/android/Renren;->appId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 416
    const-string v1, "appid"

    iget-object v2, p0, Lcom/renren/android/Renren;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_2
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 419
    iget-object v0, p0, Lcom/renren/android/Renren;->accessTokenManager:Lcom/renren/android/AccessTokenManager;

    invoke-virtual {v0, p1, p2}, Lcom/renren/android/AccessTokenManager;->writeToParcel(Landroid/os/Parcel;I)V

    .line 420
    return-void
.end method
