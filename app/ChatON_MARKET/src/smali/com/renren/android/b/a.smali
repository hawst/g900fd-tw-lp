.class public Lcom/renren/android/b/a;
.super Lcom/renren/android/a/a;
.source "FriendsGetFriendsRequestParam.java"


# instance fields
.field private a:I

.field private b:I

.field private c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/renren/android/a/a;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/renren/android/b/a;->a:I

    .line 32
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/renren/android/b/a;->b:I

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/renren/android/b/a;->c:[Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    const-string v1, "method"

    const-string v2, "friends.getFriends"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v1, "page"

    iget v2, p0, Lcom/renren/android/b/a;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v1, "count"

    iget v2, p0, Lcom/renren/android/b/a;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v1, p0, Lcom/renren/android/b/a;->c:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/renren/android/b/a;->c:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 102
    const-string v1, ", "

    iget-object v2, p0, Lcom/renren/android/b/a;->c:[Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 103
    const-string v2, "fields"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    return-object v0
.end method

.method public a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/renren/android/b/a;->c:[Ljava/lang/String;

    .line 93
    return-void
.end method
