.class Lcom/facebook/j;
.super Ljava/lang/Object;
.source "AuthorizationClient.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final transient a:Lcom/facebook/t;

.field private b:Lcom/facebook/br;

.field private c:I

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/bq;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/facebook/br;IZLjava/util/List;Lcom/facebook/bq;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/t;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/br;",
            "IZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/bq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 704
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/j;->d:Z

    .line 713
    iput-object p1, p0, Lcom/facebook/j;->b:Lcom/facebook/br;

    .line 714
    iput p2, p0, Lcom/facebook/j;->c:I

    .line 715
    iput-boolean p3, p0, Lcom/facebook/j;->d:Z

    .line 716
    iput-object p4, p0, Lcom/facebook/j;->e:Ljava/util/List;

    .line 717
    iput-object p5, p0, Lcom/facebook/j;->f:Lcom/facebook/bq;

    .line 718
    iput-object p6, p0, Lcom/facebook/j;->g:Ljava/lang/String;

    .line 719
    iput-object p7, p0, Lcom/facebook/j;->h:Ljava/lang/String;

    .line 720
    iput-object p8, p0, Lcom/facebook/j;->a:Lcom/facebook/t;

    .line 722
    return-void
.end method


# virtual methods
.method a()Lcom/facebook/t;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/facebook/j;->a:Lcom/facebook/t;

    return-object v0
.end method

.method a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 733
    iput-object p1, p0, Lcom/facebook/j;->e:Ljava/util/List;

    .line 734
    return-void
.end method

.method b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 729
    iget-object v0, p0, Lcom/facebook/j;->e:Ljava/util/List;

    return-object v0
.end method

.method c()Lcom/facebook/br;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/facebook/j;->b:Lcom/facebook/br;

    return-object v0
.end method

.method d()I
    .locals 1

    .prologue
    .line 741
    iget v0, p0, Lcom/facebook/j;->c:I

    return v0
.end method

.method e()Lcom/facebook/bq;
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/facebook/j;->f:Lcom/facebook/bq;

    return-object v0
.end method

.method f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/facebook/j;->g:Ljava/lang/String;

    return-object v0
.end method

.method g()Z
    .locals 1

    .prologue
    .line 753
    iget-boolean v0, p0, Lcom/facebook/j;->d:Z

    return v0
.end method

.method h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/facebook/j;->h:Ljava/lang/String;

    return-object v0
.end method

.method i()Z
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcom/facebook/j;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/j;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
