.class Lcom/facebook/u;
.super Lcom/facebook/i;
.source "AuthorizationClient.java"


# instance fields
.field final synthetic b:Lcom/facebook/c;

.field private transient c:Lcom/facebook/c/o;


# direct methods
.method constructor <init>(Lcom/facebook/c;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/facebook/u;->b:Lcom/facebook/c;

    invoke-direct {p0, p1}, Lcom/facebook/i;-><init>(Lcom/facebook/c;)V

    return-void
.end method


# virtual methods
.method a(Lcom/facebook/j;Landroid/os/Bundle;Lcom/facebook/y;)V
    .locals 2

    .prologue
    .line 468
    if-eqz p2, :cond_0

    .line 472
    iget-object v0, p0, Lcom/facebook/u;->b:Lcom/facebook/c;

    iget-object v0, v0, Lcom/facebook/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 475
    invoke-virtual {p1}, Lcom/facebook/j;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/facebook/b;->e:Lcom/facebook/b;

    invoke-static {v0, p2, v1}, Lcom/facebook/a;->a(Ljava/util/List;Landroid/os/Bundle;Lcom/facebook/b;)Lcom/facebook/a;

    move-result-object v0

    .line 477
    invoke-static {v0}, Lcom/facebook/r;->a(Lcom/facebook/a;)Lcom/facebook/r;

    move-result-object v0

    .line 486
    :goto_0
    iget-object v1, p0, Lcom/facebook/u;->b:Lcom/facebook/c;

    invoke-virtual {v1, v0}, Lcom/facebook/c;->a(Lcom/facebook/r;)V

    .line 487
    return-void

    .line 480
    :cond_0
    instance-of v0, p3, Lcom/facebook/aa;

    if-eqz v0, :cond_1

    .line 481
    const-string v0, "User canceled log in."

    invoke-static {v0}, Lcom/facebook/r;->a(Ljava/lang/String;)Lcom/facebook/r;

    move-result-object v0

    goto :goto_0

    .line 483
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/y;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/r;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/r;

    move-result-object v0

    goto :goto_0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x1

    return v0
.end method

.method a(Lcom/facebook/j;)Z
    .locals 5

    .prologue
    .line 440
    invoke-virtual {p1}, Lcom/facebook/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 441
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 442
    invoke-virtual {p1}, Lcom/facebook/j;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/b/s;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 443
    const-string v2, "scope"

    const-string v3, ","

    invoke-virtual {p1}, Lcom/facebook/j;->b()Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_0
    iget-object v2, p0, Lcom/facebook/u;->b:Lcom/facebook/c;

    iget-object v2, v2, Lcom/facebook/c;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/facebook/b/s;->a(Landroid/content/Context;)V

    .line 449
    new-instance v2, Lcom/facebook/v;

    invoke-direct {v2, p0, p1}, Lcom/facebook/v;-><init>(Lcom/facebook/u;Lcom/facebook/j;)V

    .line 456
    new-instance v3, Lcom/facebook/h;

    iget-object v4, p0, Lcom/facebook/u;->b:Lcom/facebook/c;

    invoke-virtual {v4}, Lcom/facebook/c;->f()Lcom/facebook/t;

    move-result-object v4

    invoke-interface {v4}, Lcom/facebook/t;->a()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4, v0, v1}, Lcom/facebook/h;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v3, v2}, Lcom/facebook/h;->a(Lcom/facebook/c/w;)Lcom/facebook/c/t;

    move-result-object v0

    check-cast v0, Lcom/facebook/c/s;

    .line 459
    invoke-virtual {v0}, Lcom/facebook/c/s;->a()Lcom/facebook/c/o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/u;->c:Lcom/facebook/c/o;

    .line 460
    iget-object v0, p0, Lcom/facebook/u;->c:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->show()V

    .line 462
    const/4 v0, 0x1

    return v0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x1

    return v0
.end method

.method c()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/facebook/u;->c:Lcom/facebook/c/o;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/facebook/u;->c:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->dismiss()V

    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/u;->c:Lcom/facebook/c/o;

    .line 436
    :cond_0
    return-void
.end method
