.class public Lcom/facebook/az;
.super Ljava/lang/Object;
.source "Response.java"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljava/net/HttpURLConnection;

.field private final c:Lcom/facebook/model/GraphObject;

.field private final d:Lcom/facebook/model/GraphObjectList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/model/GraphObjectList",
            "<",
            "Lcom/facebook/model/GraphObject;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private final f:Lcom/facebook/ab;

.field private final g:Lcom/facebook/am;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/facebook/az;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/az;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/facebook/az;->g:Lcom/facebook/am;

    .line 85
    iput-object p2, p0, Lcom/facebook/az;->b:Ljava/net/HttpURLConnection;

    .line 86
    iput-object v0, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    .line 87
    iput-object v0, p0, Lcom/facebook/az;->d:Lcom/facebook/model/GraphObjectList;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/az;->e:Z

    .line 89
    iput-object p3, p0, Lcom/facebook/az;->f:Lcom/facebook/ab;

    .line 90
    return-void
.end method

.method constructor <init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/model/GraphObject;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/facebook/az;->g:Lcom/facebook/am;

    .line 66
    iput-object p2, p0, Lcom/facebook/az;->b:Ljava/net/HttpURLConnection;

    .line 67
    iput-object p3, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    .line 68
    iput-object v0, p0, Lcom/facebook/az;->d:Lcom/facebook/model/GraphObjectList;

    .line 69
    iput-boolean p4, p0, Lcom/facebook/az;->e:Z

    .line 70
    iput-object v0, p0, Lcom/facebook/az;->f:Lcom/facebook/ab;

    .line 71
    return-void
.end method

.method constructor <init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/model/GraphObjectList;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/am;",
            "Ljava/net/HttpURLConnection;",
            "Lcom/facebook/model/GraphObjectList",
            "<",
            "Lcom/facebook/model/GraphObject;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/facebook/az;->g:Lcom/facebook/am;

    .line 76
    iput-object p2, p0, Lcom/facebook/az;->b:Ljava/net/HttpURLConnection;

    .line 77
    iput-object v0, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    .line 78
    iput-object p3, p0, Lcom/facebook/az;->d:Lcom/facebook/model/GraphObjectList;

    .line 79
    iput-boolean p4, p0, Lcom/facebook/az;->e:Z

    .line 80
    iput-object v0, p0, Lcom/facebook/az;->f:Lcom/facebook/ab;

    .line 81
    return-void
.end method

.method private static a(Lcom/facebook/am;Ljava/net/HttpURLConnection;Ljava/lang/Object;ZLjava/lang/Object;)Lcom/facebook/az;
    .locals 3

    .prologue
    .line 401
    instance-of v0, p2, Lorg/json/JSONObject;

    if-eqz v0, :cond_4

    .line 402
    check-cast p2, Lorg/json/JSONObject;

    .line 404
    invoke-static {p2, p4, p1}, Lcom/facebook/ab;->a(Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)Lcom/facebook/ab;

    move-result-object v1

    .line 406
    if-eqz v1, :cond_1

    .line 407
    invoke-virtual {v1}, Lcom/facebook/ab;->b()I

    move-result v0

    const/16 v2, 0xbe

    if-ne v0, v2, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/facebook/am;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 409
    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {v0}, Lcom/facebook/ba;->i()V

    .line 413
    :cond_0
    new-instance v0, Lcom/facebook/az;

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    .line 431
    :goto_0
    return-object v0

    .line 416
    :cond_1
    const-string v0, "body"

    const-string v1, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p2, v0, v1}, Lcom/facebook/b/s;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 418
    instance-of v1, v0, Lorg/json/JSONObject;

    if-eqz v1, :cond_2

    .line 419
    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/facebook/model/GraphObject$Factory;->create(Lorg/json/JSONObject;)Lcom/facebook/model/GraphObject;

    move-result-object v1

    .line 420
    new-instance v0, Lcom/facebook/az;

    invoke-direct {v0, p0, p1, v1, p3}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/model/GraphObject;Z)V

    goto :goto_0

    .line 421
    :cond_2
    instance-of v1, v0, Lorg/json/JSONArray;

    if-eqz v1, :cond_3

    .line 422
    check-cast v0, Lorg/json/JSONArray;

    const-class v1, Lcom/facebook/model/GraphObject;

    invoke-static {v0, v1}, Lcom/facebook/model/GraphObject$Factory;->createList(Lorg/json/JSONArray;Ljava/lang/Class;)Lcom/facebook/model/GraphObjectList;

    move-result-object v1

    .line 424
    new-instance v0, Lcom/facebook/az;

    invoke-direct {v0, p0, p1, v1, p3}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/model/GraphObjectList;Z)V

    goto :goto_0

    .line 427
    :cond_3
    sget-object p2, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    .line 430
    :cond_4
    sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne p2, v0, :cond_5

    .line 431
    new-instance v1, Lcom/facebook/az;

    const/4 v0, 0x0

    check-cast v0, Lcom/facebook/model/GraphObject;

    invoke-direct {v1, p0, p1, v0, p3}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/model/GraphObject;Z)V

    move-object v0, v1

    goto :goto_0

    .line 433
    :cond_5
    new-instance v0, Lcom/facebook/y;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got unexpected object type in response, class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Ljava/io/InputStream;Ljava/net/HttpURLConnection;Lcom/facebook/ax;Z)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/net/HttpURLConnection;",
            "Lcom/facebook/ax;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/az;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 331
    invoke-static {p0}, Lcom/facebook/b/s;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 332
    sget-object v1, Lcom/facebook/ak;->c:Lcom/facebook/ak;

    const-string v2, "Response"

    const-string v3, "Response (raw)\n  Size: %d\n  Response:\n%s\n"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v0, v4, v8

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 337
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    .line 339
    invoke-static {p1, p2, v1, p3}, Lcom/facebook/az;->a(Ljava/net/HttpURLConnection;Ljava/util/List;Ljava/lang/Object;Z)Ljava/util/List;

    move-result-object v1

    .line 340
    sget-object v2, Lcom/facebook/ak;->a:Lcom/facebook/ak;

    const-string v3, "Response"

    const-string v4, "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/ax;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    aput-object v1, v5, v9

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 343
    return-object v1
.end method

.method static a(Ljava/net/HttpURLConnection;Lcom/facebook/ax;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Lcom/facebook/ax;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/az;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 265
    .line 267
    const/4 v0, 0x0

    .line 268
    const/4 v3, 0x0

    .line 301
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v4, 0x190

    if-lt v2, v4, :cond_1

    .line 302
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    move-object v1, v2

    .line 313
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v1, p0, p1, v0}, Lcom/facebook/az;->a(Ljava/io/InputStream;Ljava/net/HttpURLConnection;Lcom/facebook/ax;Z)Ljava/util/List;
    :try_end_0
    .catch Lcom/facebook/y; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 324
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    :goto_1
    return-object v0

    .line 304
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Lcom/facebook/y; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 305
    if-eqz v1, :cond_2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 306
    :try_start_2
    invoke-virtual {v0, v3, v2}, Lcom/facebook/b/a;->a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;
    :try_end_2
    .catch Lcom/facebook/y; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 307
    if-nez v1, :cond_0

    :cond_2
    move-object v1, v2

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    :goto_2
    :try_start_3
    sget-object v2, Lcom/facebook/ak;->a:Lcom/facebook/ak;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    invoke-static {p1, p0, v0}, Lcom/facebook/az;->a(Ljava/util/List;Ljava/net/HttpURLConnection;Lcom/facebook/y;)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 324
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 317
    :catch_1
    move-exception v0

    .line 318
    :goto_3
    :try_start_4
    sget-object v2, Lcom/facebook/ak;->a:Lcom/facebook/ak;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    new-instance v2, Lcom/facebook/y;

    invoke-direct {v2, v0}, Lcom/facebook/y;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p1, p0, v2}, Lcom/facebook/az;->a(Ljava/util/List;Ljava/net/HttpURLConnection;Lcom/facebook/y;)Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 324
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 320
    :catch_2
    move-exception v0

    .line 321
    :goto_4
    :try_start_5
    sget-object v2, Lcom/facebook/ak;->a:Lcom/facebook/ak;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    new-instance v2, Lcom/facebook/y;

    invoke-direct {v2, v0}, Lcom/facebook/y;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p1, p0, v2}, Lcom/facebook/az;->a(Ljava/util/List;Ljava/net/HttpURLConnection;Lcom/facebook/y;)Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 324
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_5
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_5

    .line 320
    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 317
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 314
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method private static a(Ljava/net/HttpURLConnection;Ljava/util/List;Ljava/lang/Object;Z)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/am;",
            ">;",
            "Ljava/lang/Object;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/az;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 348
    sget-boolean v0, Lcom/facebook/az;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 350
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 351
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 354
    const/4 v0, 0x1

    if-ne v3, v0, :cond_3

    .line 355
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/am;

    .line 360
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 361
    const-string v1, "body"

    invoke-virtual {v5, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 363
    :goto_0
    const-string v6, "code"

    invoke-virtual {v5, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 365
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 366
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 377
    :goto_1
    instance-of v0, v1, Lorg/json/JSONArray;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 378
    :cond_1
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Unexpected number of results"

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    .line 379
    throw v0

    .line 362
    :cond_2
    const/16 v1, 0xc8

    goto :goto_0

    .line 370
    :catch_0
    move-exception v1

    .line 371
    new-instance v5, Lcom/facebook/az;

    new-instance v6, Lcom/facebook/ab;

    invoke-direct {v6, p0, v1}, Lcom/facebook/ab;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, p2

    .line 374
    goto :goto_1

    .line 372
    :catch_1
    move-exception v1

    .line 373
    new-instance v5, Lcom/facebook/az;

    new-instance v6, Lcom/facebook/ab;

    invoke-direct {v6, p0, v1}, Lcom/facebook/ab;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v1, p2

    goto :goto_1

    .line 382
    :cond_4
    check-cast v1, Lorg/json/JSONArray;

    .line 384
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 385
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/am;

    .line 387
    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 388
    invoke-static {v0, p0, v3, p3, p2}, Lcom/facebook/az;->a(Lcom/facebook/am;Ljava/net/HttpURLConnection;Ljava/lang/Object;ZLjava/lang/Object;)Lcom/facebook/az;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/facebook/y; {:try_start_1 .. :try_end_1} :catch_3

    .line 384
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 389
    :catch_2
    move-exception v3

    .line 390
    new-instance v5, Lcom/facebook/az;

    new-instance v6, Lcom/facebook/ab;

    invoke-direct {v6, p0, v3}, Lcom/facebook/ab;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 391
    :catch_3
    move-exception v3

    .line 392
    new-instance v5, Lcom/facebook/az;

    new-instance v6, Lcom/facebook/ab;

    invoke-direct {v6, p0, v3}, Lcom/facebook/ab;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 396
    :cond_5
    return-object v4
.end method

.method static a(Ljava/util/List;Ljava/net/HttpURLConnection;Lcom/facebook/y;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/am;",
            ">;",
            "Ljava/net/HttpURLConnection;",
            "Lcom/facebook/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/az;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 441
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 442
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 443
    new-instance v4, Lcom/facebook/az;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/am;

    new-instance v5, Lcom/facebook/ab;

    invoke-direct {v5, p1, p2}, Lcom/facebook/ab;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v4, v0, p1, v5}, Lcom/facebook/az;-><init>(Lcom/facebook/am;Ljava/net/HttpURLConnection;Lcom/facebook/ab;)V

    .line 444
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 446
    :cond_0
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/facebook/ab;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/facebook/az;->f:Lcom/facebook/ab;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/facebook/model/GraphObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/model/GraphObject;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 124
    :goto_0
    return-object v0

    .line 121
    :cond_0
    if-nez p1, :cond_1

    .line 122
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Must pass in a valid interface that extends GraphObject"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    invoke-interface {v0, p1}, Lcom/facebook/model/GraphObject;->cast(Ljava/lang/Class;)Lcom/facebook/model/GraphObject;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/model/GraphObject;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    return-object v0
.end method

.method public c()Lcom/facebook/am;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/facebook/az;->g:Lcom/facebook/am;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 232
    :try_start_0
    const-string v1, "%d"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/facebook/az;->b:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/az;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 237
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "{Response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " responseCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", graphObject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/az;->c:Lcom/facebook/model/GraphObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/az;->f:Lcom/facebook/ab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFromCache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/az;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 232
    :cond_0
    const/16 v0, 0xc8

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    const-string v0, "unknown"

    goto :goto_1
.end method
