.class Lcom/facebook/c;
.super Ljava/lang/Object;
.source "AuthorizationClient.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/i;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/facebook/i;

.field transient c:Landroid/content/Context;

.field transient d:Lcom/facebook/t;

.field transient e:Lcom/facebook/q;

.field transient f:Lcom/facebook/k;

.field transient g:Z

.field h:Lcom/facebook/j;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 770
    return-void
.end method

.method static synthetic a(Lcom/facebook/c;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/facebook/c;->i()V

    return-void
.end method

.method static synthetic b(Lcom/facebook/c;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/facebook/c;->h()V

    return-void
.end method

.method private c(Lcom/facebook/j;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/j;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    invoke-virtual {p1}, Lcom/facebook/j;->c()Lcom/facebook/br;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lcom/facebook/br;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    invoke-virtual {p1}, Lcom/facebook/j;->g()Z

    move-result v2

    if-nez v2, :cond_0

    .line 152
    new-instance v2, Lcom/facebook/l;

    invoke-direct {v2, p0}, Lcom/facebook/l;-><init>(Lcom/facebook/c;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v2, Lcom/facebook/o;

    invoke-direct {v2, p0}, Lcom/facebook/o;-><init>(Lcom/facebook/c;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_0
    new-instance v2, Lcom/facebook/p;

    invoke-direct {v2, p0}, Lcom/facebook/p;-><init>(Lcom/facebook/c;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/br;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    new-instance v1, Lcom/facebook/u;

    invoke-direct {v1, p0}, Lcom/facebook/u;-><init>(Lcom/facebook/c;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_2
    return-object v0
.end method

.method private e(Lcom/facebook/r;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/facebook/c;->e:Lcom/facebook/q;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/facebook/c;->e:Lcom/facebook/q;

    invoke-interface {v0, p1}, Lcom/facebook/q;->a(Lcom/facebook/r;)V

    .line 379
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 201
    const-string v0, "Login attempt failed."

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/r;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/c;->b(Lcom/facebook/r;)V

    .line 202
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/facebook/c;->f:Lcom/facebook/k;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/facebook/c;->f:Lcom/facebook/k;

    invoke-interface {v0}, Lcom/facebook/k;->a()V

    .line 385
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/facebook/c;->f:Lcom/facebook/k;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/facebook/c;->f:Lcom/facebook/k;

    invoke-interface {v0}, Lcom/facebook/k;->b()V

    .line 391
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/facebook/c;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method a()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Attempted to continue authorization without a pending request."

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    invoke-virtual {v0}, Lcom/facebook/i;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    invoke-virtual {v0}, Lcom/facebook/i;->c()V

    .line 125
    invoke-virtual {p0}, Lcom/facebook/c;->e()Z

    .line 127
    :cond_2
    return-void
.end method

.method a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/facebook/c;->c:Landroid/content/Context;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/c;->d:Lcom/facebook/t;

    .line 72
    return-void
.end method

.method a(Lcom/facebook/j;)V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/facebook/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/facebook/c;->a()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/c;->b(Lcom/facebook/j;)V

    goto :goto_0
.end method

.method a(Lcom/facebook/q;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/facebook/c;->e:Lcom/facebook/q;

    .line 235
    return-void
.end method

.method a(Lcom/facebook/r;)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p1, Lcom/facebook/r;->b:Lcom/facebook/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    invoke-virtual {v0}, Lcom/facebook/j;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0, p1}, Lcom/facebook/c;->c(Lcom/facebook/r;)V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/c;->b(Lcom/facebook/r;)V

    goto :goto_0
.end method

.method b(Ljava/lang/String;)Lcom/facebook/am;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 362
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 363
    const-string v0, "fields"

    const-string v2, "id"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v0, "access_token"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    new-instance v0, Lcom/facebook/am;

    const-string v2, "me/permissions"

    sget-object v4, Lcom/facebook/aj;->a:Lcom/facebook/aj;

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/am;-><init>(Lcom/facebook/ba;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/aj;Lcom/facebook/ar;)V

    return-object v0
.end method

.method b(Lcom/facebook/j;)V
    .locals 2

    .prologue
    .line 101
    if-nez p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    if-eqz v0, :cond_2

    .line 106
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Attempted to authorize while a request is pending."

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/j;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    :cond_3
    iput-object p1, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    .line 114
    invoke-direct {p0, p1}, Lcom/facebook/c;->c(Lcom/facebook/j;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/c;->a:Ljava/util/List;

    .line 115
    invoke-virtual {p0}, Lcom/facebook/c;->d()V

    goto :goto_0
.end method

.method b(Lcom/facebook/r;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 222
    iput-object v0, p0, Lcom/facebook/c;->a:Ljava/util/List;

    .line 223
    iput-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    .line 224
    iput-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    .line 226
    invoke-direct {p0, p1}, Lcom/facebook/c;->e(Lcom/facebook/r;)V

    .line 227
    return-void
.end method

.method b()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(Ljava/lang/String;)Lcom/facebook/am;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 369
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 370
    const-string v0, "fields"

    const-string v2, "id"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v0, "access_token"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    new-instance v0, Lcom/facebook/am;

    const-string v2, "me"

    sget-object v4, Lcom/facebook/aj;->a:Lcom/facebook/aj;

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/am;-><init>(Lcom/facebook/ba;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/aj;Lcom/facebook/ar;)V

    return-object v0
.end method

.method c(Lcom/facebook/r;)V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p1, Lcom/facebook/r;->b:Lcom/facebook/a;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Can\'t validate without a token"

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/c;->d(Lcom/facebook/r;)Lcom/facebook/ax;

    move-result-object v0

    .line 276
    invoke-direct {p0}, Lcom/facebook/c;->h()V

    .line 278
    invoke-virtual {v0}, Lcom/facebook/ax;->h()Lcom/facebook/aw;

    .line 279
    return-void
.end method

.method c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 166
    iget-boolean v1, p0, Lcom/facebook/c;->g:Z

    if-eqz v1, :cond_0

    .line 180
    :goto_0
    return v0

    .line 170
    :cond_0
    const-string v1, "android.permission.INTERNET"

    invoke-virtual {p0, v1}, Lcom/facebook/c;->a(Ljava/lang/String;)I

    move-result v1

    .line 171
    if-eqz v1, :cond_1

    .line 172
    const-string v0, "AndroidManifest Error"

    .line 173
    const-string v1, "WebView login requires INTERNET permission"

    .line 174
    invoke-static {v0, v1}, Lcom/facebook/r;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/c;->b(Lcom/facebook/r;)V

    .line 176
    const/4 v0, 0x0

    goto :goto_0

    .line 179
    :cond_1
    iput-boolean v0, p0, Lcom/facebook/c;->g:Z

    goto :goto_0
.end method

.method d(Lcom/facebook/r;)Lcom/facebook/ax;
    .locals 8

    .prologue
    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 288
    iget-object v2, p1, Lcom/facebook/r;->b:Lcom/facebook/a;

    invoke-virtual {v2}, Lcom/facebook/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 290
    new-instance v3, Lcom/facebook/e;

    invoke-direct {v3, p0, v0}, Lcom/facebook/e;-><init>(Lcom/facebook/c;Ljava/util/ArrayList;)V

    .line 303
    iget-object v4, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    invoke-virtual {v4}, Lcom/facebook/j;->h()Ljava/lang/String;

    move-result-object v4

    .line 304
    invoke-virtual {p0, v4}, Lcom/facebook/c;->c(Ljava/lang/String;)Lcom/facebook/am;

    move-result-object v5

    .line 305
    invoke-virtual {v5, v3}, Lcom/facebook/am;->a(Lcom/facebook/ar;)V

    .line 307
    invoke-virtual {p0, v2}, Lcom/facebook/c;->c(Ljava/lang/String;)Lcom/facebook/am;

    move-result-object v2

    .line 308
    invoke-virtual {v2, v3}, Lcom/facebook/am;->a(Lcom/facebook/ar;)V

    .line 310
    invoke-virtual {p0, v4}, Lcom/facebook/c;->b(Ljava/lang/String;)Lcom/facebook/am;

    move-result-object v3

    .line 311
    new-instance v4, Lcom/facebook/f;

    invoke-direct {v4, p0, v1}, Lcom/facebook/f;-><init>(Lcom/facebook/c;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Lcom/facebook/am;->a(Lcom/facebook/ar;)V

    .line 330
    new-instance v4, Lcom/facebook/ax;

    const/4 v6, 0x3

    new-array v6, v6, [Lcom/facebook/am;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    const/4 v5, 0x1

    aput-object v2, v6, v5

    const/4 v2, 0x2

    aput-object v3, v6, v2

    invoke-direct {v4, v6}, Lcom/facebook/ax;-><init>([Lcom/facebook/am;)V

    .line 332
    iget-object v2, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    invoke-virtual {v2}, Lcom/facebook/j;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/ax;->a(Ljava/lang/String;)V

    .line 333
    new-instance v2, Lcom/facebook/g;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/facebook/g;-><init>(Lcom/facebook/c;Ljava/util/ArrayList;Lcom/facebook/r;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v2}, Lcom/facebook/ax;->a(Lcom/facebook/ay;)V

    .line 358
    return-object v4
.end method

.method d()V
    .locals 2

    .prologue
    .line 184
    :cond_0
    iget-object v0, p0, Lcom/facebook/c;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/facebook/c;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/i;

    iput-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    .line 187
    invoke-virtual {p0}, Lcom/facebook/c;->e()Z

    move-result v0

    .line 189
    if-eqz v0, :cond_0

    .line 198
    :cond_1
    :goto_0
    return-void

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    if-eqz v0, :cond_1

    .line 196
    invoke-direct {p0}, Lcom/facebook/c;->g()V

    goto :goto_0
.end method

.method e()Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    invoke-virtual {v0}, Lcom/facebook/i;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/c;->b:Lcom/facebook/i;

    iget-object v1, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    invoke-virtual {v0, v1}, Lcom/facebook/i;->a(Lcom/facebook/j;)Z

    move-result v0

    goto :goto_0
.end method

.method f()Lcom/facebook/t;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/facebook/c;->d:Lcom/facebook/t;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/facebook/c;->d:Lcom/facebook/t;

    .line 262
    :goto_0
    return-object v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/facebook/c;->h:Lcom/facebook/j;

    if-eqz v0, :cond_1

    .line 250
    new-instance v0, Lcom/facebook/d;

    invoke-direct {v0, p0}, Lcom/facebook/d;-><init>(Lcom/facebook/c;)V

    goto :goto_0

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
