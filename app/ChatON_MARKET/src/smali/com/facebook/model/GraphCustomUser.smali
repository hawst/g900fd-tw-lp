.class public interface abstract Lcom/facebook/model/GraphCustomUser;
.super Ljava/lang/Object;
.source "GraphCustomUser.java"

# interfaces
.implements Lcom/facebook/model/GraphObject;


# virtual methods
.method public abstract getEmail()Ljava/lang/String;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getInstalled()Ljava/lang/Boolean;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setInstalled(Z)V
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method
