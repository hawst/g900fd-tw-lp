.class public final enum Lcom/facebook/b/n;
.super Ljava/lang/Enum;
.source "SessionAuthorizationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/b/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/b/n;

.field public static final enum b:Lcom/facebook/b/n;

.field private static final synthetic c:[Lcom/facebook/b/n;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/facebook/b/n;

    const-string v1, "READ"

    invoke-direct {v0, v1, v2}, Lcom/facebook/b/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/b/n;->a:Lcom/facebook/b/n;

    .line 26
    new-instance v0, Lcom/facebook/b/n;

    const-string v1, "PUBLISH"

    invoke-direct {v0, v1, v3}, Lcom/facebook/b/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/b/n;->b:Lcom/facebook/b/n;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/b/n;

    sget-object v1, Lcom/facebook/b/n;->a:Lcom/facebook/b/n;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/b/n;->b:Lcom/facebook/b/n;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/b/n;->c:[Lcom/facebook/b/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/b/n;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/facebook/b/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/b/n;

    return-object v0
.end method

.method public static values()[Lcom/facebook/b/n;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/facebook/b/n;->c:[Lcom/facebook/b/n;

    invoke-virtual {v0}, [Lcom/facebook/b/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/b/n;

    return-object v0
.end method
