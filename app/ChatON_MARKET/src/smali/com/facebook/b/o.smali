.class public Lcom/facebook/b/o;
.super Ljava/lang/Object;
.source "SessionTracker.java"


# instance fields
.field private a:Lcom/facebook/ba;

.field private final b:Lcom/facebook/bn;

.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/support/v4/content/LocalBroadcastManager;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/bn;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/b/o;-><init>(Landroid/content/Context;Lcom/facebook/bn;Lcom/facebook/ba;)V

    .line 49
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/facebook/bn;Lcom/facebook/ba;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/b/o;-><init>(Landroid/content/Context;Lcom/facebook/bn;Lcom/facebook/ba;Z)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/bn;Lcom/facebook/ba;Z)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/b/o;->e:Z

    .line 73
    new-instance v0, Lcom/facebook/b/r;

    invoke-direct {v0, p0, p2}, Lcom/facebook/b/r;-><init>(Lcom/facebook/b/o;Lcom/facebook/bn;)V

    iput-object v0, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    .line 74
    iput-object p3, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    .line 75
    new-instance v0, Lcom/facebook/b/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/b/q;-><init>(Lcom/facebook/b/o;Lcom/facebook/b/p;)V

    iput-object v0, p0, Lcom/facebook/b/o;->c:Landroid/content/BroadcastReceiver;

    .line 76
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/b/o;->d:Landroid/support/v4/content/LocalBroadcastManager;

    .line 78
    if-eqz p4, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/facebook/b/o;->c()V

    .line 81
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/facebook/b/o;)Lcom/facebook/bn;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    return-object v0
.end method

.method static synthetic b(Lcom/facebook/b/o;)Lcom/facebook/ba;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 196
    const-string v1, "com.facebook.sdk.ACTIVE_SESSION_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    const-string v1, "com.facebook.sdk.ACTIVE_SESSION_UNSET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/facebook/b/o;->d:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/facebook/b/o;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 202
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/ba;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/ba;->j()Lcom/facebook/ba;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    goto :goto_0
.end method

.method public a(Lcom/facebook/ba;)V
    .locals 2

    .prologue
    .line 112
    if-nez p1, :cond_1

    .line 113
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->b(Lcom/facebook/bn;)V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    .line 118
    invoke-direct {p0}, Lcom/facebook/b/o;->e()V

    .line 119
    invoke-virtual {p0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->a(Lcom/facebook/bn;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    if-nez v0, :cond_3

    .line 127
    invoke-static {}, Lcom/facebook/ba;->j()Lcom/facebook/ba;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_2

    .line 129
    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->b(Lcom/facebook/bn;)V

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/facebook/b/o;->d:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/facebook/b/o;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 138
    :goto_1
    iput-object p1, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    .line 139
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->a(Lcom/facebook/bn;)V

    goto :goto_0

    .line 136
    :cond_3
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->b(Lcom/facebook/bn;)V

    goto :goto_1
.end method

.method public b()Lcom/facebook/ba;
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/ba;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/facebook/b/o;->e:Z

    if-eqz v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/facebook/b/o;->a:Lcom/facebook/ba;

    if-nez v0, :cond_1

    .line 151
    invoke-direct {p0}, Lcom/facebook/b/o;->e()V

    .line 154
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 155
    invoke-virtual {p0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/b/o;->b:Lcom/facebook/bn;

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->a(Lcom/facebook/bn;)V

    .line 157
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/b/o;->e:Z

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/facebook/b/o;->e:Z

    return v0
.end method
