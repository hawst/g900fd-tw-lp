.class public final Lcom/facebook/ab;
.super Ljava/lang/Object;
.source "FacebookRequestError.java"


# static fields
.field private static final a:Lcom/facebook/ae;

.field private static final b:Lcom/facebook/ae;

.field private static final c:Lcom/facebook/ae;

.field private static final d:Lcom/facebook/ae;


# instance fields
.field private final e:I

.field private final f:Z

.field private final g:Lcom/facebook/ad;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Lorg/json/JSONObject;

.field private final n:Lorg/json/JSONObject;

.field private final o:Ljava/lang/Object;

.field private final p:Ljava/net/HttpURLConnection;

.field private final q:Lcom/facebook/y;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v2, 0x12b

    const/16 v1, 0xc8

    const/4 v3, 0x0

    .line 81
    new-instance v0, Lcom/facebook/ae;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ae;-><init>(IILcom/facebook/ac;)V

    sput-object v0, Lcom/facebook/ab;->a:Lcom/facebook/ae;

    .line 87
    new-instance v0, Lcom/facebook/ae;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ae;-><init>(IILcom/facebook/ac;)V

    sput-object v0, Lcom/facebook/ab;->b:Lcom/facebook/ae;

    .line 88
    new-instance v0, Lcom/facebook/ae;

    const/16 v1, 0x190

    const/16 v2, 0x1f3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ae;-><init>(IILcom/facebook/ac;)V

    sput-object v0, Lcom/facebook/ab;->c:Lcom/facebook/ae;

    .line 89
    new-instance v0, Lcom/facebook/ae;

    const/16 v1, 0x1f4

    const/16 v2, 0x257

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ae;-><init>(IILcom/facebook/ac;)V

    sput-object v0, Lcom/facebook/ab;->d:Lcom/facebook/ae;

    return-void
.end method

.method private constructor <init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)V
    .locals 11

    .prologue
    .line 180
    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/facebook/ab;-><init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;Lcom/facebook/y;)V

    .line 182
    return-void
.end method

.method private constructor <init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;Lcom/facebook/y;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput p1, p0, Lcom/facebook/ab;->h:I

    .line 110
    iput p2, p0, Lcom/facebook/ab;->i:I

    .line 111
    iput p3, p0, Lcom/facebook/ab;->j:I

    .line 112
    iput-object p4, p0, Lcom/facebook/ab;->k:Ljava/lang/String;

    .line 113
    iput-object p5, p0, Lcom/facebook/ab;->l:Ljava/lang/String;

    .line 114
    iput-object p6, p0, Lcom/facebook/ab;->n:Lorg/json/JSONObject;

    .line 115
    iput-object p7, p0, Lcom/facebook/ab;->m:Lorg/json/JSONObject;

    .line 116
    iput-object p8, p0, Lcom/facebook/ab;->o:Ljava/lang/Object;

    .line 117
    iput-object p9, p0, Lcom/facebook/ab;->p:Ljava/net/HttpURLConnection;

    .line 120
    if-eqz p10, :cond_1

    .line 121
    iput-object p10, p0, Lcom/facebook/ab;->q:Lcom/facebook/y;

    move v3, v0

    .line 129
    :goto_0
    const/4 v2, 0x0

    .line 132
    if-eqz v3, :cond_2

    .line 133
    sget-object v0, Lcom/facebook/ad;->h:Lcom/facebook/ad;

    move-object v2, v0

    move v0, v1

    .line 172
    :cond_0
    :goto_1
    iput-object v2, p0, Lcom/facebook/ab;->g:Lcom/facebook/ad;

    .line 173
    iput v1, p0, Lcom/facebook/ab;->e:I

    .line 174
    iput-boolean v0, p0, Lcom/facebook/ab;->f:Z

    .line 175
    return-void

    .line 124
    :cond_1
    new-instance v2, Lcom/facebook/af;

    invoke-direct {v2, p0, p5}, Lcom/facebook/af;-><init>(Lcom/facebook/ab;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/ab;->q:Lcom/facebook/y;

    move v3, v1

    goto :goto_0

    .line 136
    :cond_2
    if-eq p2, v0, :cond_3

    const/4 v3, 0x2

    if-ne p2, v3, :cond_4

    .line 137
    :cond_3
    sget-object v2, Lcom/facebook/ad;->d:Lcom/facebook/ad;

    move v0, v1

    .line 161
    :goto_2
    if-nez v2, :cond_0

    .line 162
    sget-object v2, Lcom/facebook/ab;->c:Lcom/facebook/ae;

    invoke-virtual {v2, p1}, Lcom/facebook/ae;->a(I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 163
    sget-object v2, Lcom/facebook/ad;->g:Lcom/facebook/ad;

    goto :goto_1

    .line 138
    :cond_4
    const/4 v3, 0x4

    if-eq p2, v3, :cond_5

    const/16 v3, 0x11

    if-ne p2, v3, :cond_6

    .line 139
    :cond_5
    sget-object v2, Lcom/facebook/ad;->e:Lcom/facebook/ad;

    move v0, v1

    goto :goto_2

    .line 140
    :cond_6
    const/16 v3, 0xa

    if-eq p2, v3, :cond_7

    sget-object v3, Lcom/facebook/ab;->a:Lcom/facebook/ae;

    invoke-virtual {v3, p2}, Lcom/facebook/ae;->a(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 141
    :cond_7
    sget-object v2, Lcom/facebook/ad;->c:Lcom/facebook/ad;

    move v0, v1

    goto :goto_2

    .line 143
    :cond_8
    const/16 v3, 0x66

    if-eq p2, v3, :cond_9

    const/16 v3, 0xbe

    if-ne p2, v3, :cond_d

    .line 144
    :cond_9
    const/16 v2, 0x1cb

    if-eq p3, v2, :cond_a

    const/16 v2, 0x1d0

    if-ne p3, v2, :cond_b

    .line 145
    :cond_a
    sget-object v2, Lcom/facebook/ad;->a:Lcom/facebook/ad;

    goto :goto_2

    .line 149
    :cond_b
    sget-object v2, Lcom/facebook/ad;->b:Lcom/facebook/ad;

    .line 151
    const/16 v0, 0x1ca

    if-ne p3, v0, :cond_c

    move v0, v1

    goto :goto_2

    .line 153
    :cond_c
    const/16 v0, 0x1cc

    if-ne p3, v0, :cond_d

    :cond_d
    move v0, v1

    goto :goto_2

    .line 164
    :cond_e
    sget-object v2, Lcom/facebook/ab;->d:Lcom/facebook/ae;

    invoke-virtual {v2, p1}, Lcom/facebook/ae;->a(I)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 165
    sget-object v2, Lcom/facebook/ad;->d:Lcom/facebook/ad;

    goto :goto_1

    .line 167
    :cond_f
    sget-object v2, Lcom/facebook/ad;->f:Lcom/facebook/ad;

    goto :goto_1
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 192
    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v4, p2

    move-object v5, p3

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v0 .. v10}, Lcom/facebook/ab;-><init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;Lcom/facebook/y;)V

    .line 194
    return-void
.end method

.method constructor <init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 185
    instance-of v0, p2, Lcom/facebook/y;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/facebook/y;

    move-object v10, p2

    :goto_0
    move-object v0, p0

    move v2, v1

    move v3, v1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, p1

    invoke-direct/range {v0 .. v10}, Lcom/facebook/ab;-><init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;Lcom/facebook/y;)V

    .line 189
    return-void

    .line 185
    :cond_0
    new-instance v10, Lcom/facebook/y;

    invoke-direct {v10, p2}, Lcom/facebook/y;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static a(Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)Lcom/facebook/ab;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v10, 0x0

    .line 348
    :try_start_0
    const-string v0, "code"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 349
    const-string v0, "code"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 350
    const-string v0, "body"

    const-string v4, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p0, v0, v4}, Lcom/facebook/b/s;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 353
    if-eqz v6, :cond_2

    instance-of v0, v6, Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 354
    check-cast v6, Lorg/json/JSONObject;

    .line 362
    const/4 v0, 0x0

    .line 363
    const-string v4, "error"

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 365
    const-string v0, "error"

    const/4 v3, 0x0

    invoke-static {v6, v0, v3}, Lcom/facebook/b/s;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 367
    const-string v3, "type"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 368
    const-string v3, "message"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 369
    const-string v3, "code"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 370
    const-string v3, "error_subcode"

    const/4 v8, -0x1

    invoke-virtual {v0, v3, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    move v0, v2

    move v2, v4

    move-object v4, v7

    .line 381
    :goto_0
    if-eqz v0, :cond_2

    .line 382
    new-instance v0, Lcom/facebook/ab;

    move-object v7, p0

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ab;-><init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)V

    .line 400
    :goto_1
    return-object v0

    .line 372
    :cond_0
    const-string v4, "error_code"

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "error_msg"

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "error_reason"

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 374
    :cond_1
    const-string v0, "error_reason"

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 375
    const-string v0, "error_msg"

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 376
    const-string v0, "error_code"

    const/4 v3, -0x1

    invoke-virtual {v6, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 377
    const-string v3, "error_subcode"

    const/4 v7, -0x1

    invoke-virtual {v6, v3, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    move v11, v2

    move v2, v0

    move v0, v11

    .line 378
    goto :goto_0

    .line 388
    :cond_2
    sget-object v0, Lcom/facebook/ab;->b:Lcom/facebook/ae;

    invoke-virtual {v0, v1}, Lcom/facebook/ae;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 389
    new-instance v0, Lcom/facebook/ab;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "body"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "body"

    const-string v7, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p0, v6, v7}, Lcom/facebook/b/s;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    :goto_2
    move-object v7, p0

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ab;-><init>(IIILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 397
    :catch_0
    move-exception v0

    :cond_3
    move-object v0, v10

    .line 400
    goto :goto_1

    :cond_4
    move-object v6, v10

    .line 389
    goto :goto_2

    :cond_5
    move v2, v3

    move-object v5, v10

    move-object v4, v10

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/facebook/ab;->h:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/facebook/ab;->i:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/facebook/ab;->k:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/facebook/ab;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/facebook/ab;->l:Ljava/lang/String;

    .line 275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ab;->q:Lcom/facebook/y;

    invoke-virtual {v0}, Lcom/facebook/y;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{HttpStatus: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/facebook/ab;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/ab;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ab;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMessage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ab;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
