.class public final Lcom/facebook/bu;
.super Ljava/lang/Object;
.source "Settings.java"


# static fields
.field private static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/ak;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:Ljava/util/concurrent/Executor;

.field private static volatile c:Z

.field private static final d:Ljava/lang/Object;

.field private static final e:Landroid/net/Uri;

.field private static final f:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/concurrent/ThreadFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/facebook/bu;->a:Ljava/util/HashSet;

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/bu;->d:Ljava/lang/Object;

    .line 53
    const-string v0, "content://com.facebook.katana.provider.AttributionIdProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/bu;->e:Landroid/net/Uri;

    .line 65
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lcom/facebook/bu;->f:Ljava/util/concurrent/BlockingQueue;

    .line 67
    new-instance v0, Lcom/facebook/bv;

    invoke-direct {v0}, Lcom/facebook/bv;-><init>()V

    sput-object v0, Lcom/facebook/bu;->g:Ljava/util/concurrent/ThreadFactory;

    return-void
.end method

.method public static a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 298
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "aid"

    aput-object v1, v2, v0

    .line 299
    sget-object v1, Lcom/facebook/bu;->e:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 300
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-object v3

    .line 303
    :cond_1
    const-string v1, "aid"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 304
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static a()Ljava/util/concurrent/Executor;
    .locals 9

    .prologue
    .line 157
    sget-object v8, Lcom/facebook/bu;->d:Ljava/lang/Object;

    monitor-enter v8

    .line 158
    :try_start_0
    sget-object v0, Lcom/facebook/bu;->b:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_1

    .line 159
    invoke-static {}, Lcom/facebook/bu;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    .line 160
    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x5

    const/16 v2, 0x80

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v6, Lcom/facebook/bu;->f:Ljava/util/concurrent/BlockingQueue;

    sget-object v7, Lcom/facebook/bu;->g:Ljava/util/concurrent/ThreadFactory;

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 164
    :cond_0
    sput-object v0, Lcom/facebook/bu;->b:Ljava/util/concurrent/Executor;

    .line 166
    :cond_1
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    sget-object v0, Lcom/facebook/bu;->b:Ljava/util/concurrent/Executor;

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 251
    if-nez p1, :cond_0

    .line 294
    :goto_0
    return v2

    .line 254
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/bu;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v5

    .line 255
    const-string v0, "com.facebook.sdk.attributionTracking"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "ping"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 257
    const-wide/16 v3, 0x0

    invoke-interface {v6, v7, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 258
    cmp-long v0, v3, v10

    if-nez v0, :cond_2

    if-eqz v5, :cond_2

    .line 259
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 260
    const-string v8, "fields"

    const-string v9, "supports_attribution"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, p1, v9}, Lcom/facebook/am;->a(Lcom/facebook/ba;Ljava/lang/String;Lcom/facebook/ar;)Lcom/facebook/am;

    move-result-object v8

    .line 262
    invoke-virtual {v8, v0}, Lcom/facebook/am;->a(Landroid/os/Bundle;)V

    .line 264
    invoke-virtual {v8}, Lcom/facebook/am;->b()Lcom/facebook/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/az;->b()Lcom/facebook/model/GraphObject;

    move-result-object v0

    .line 265
    const-string v8, "supports_attribution"

    invoke-interface {v0, v8}, Lcom/facebook/model/GraphObject;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 267
    instance-of v8, v0, Ljava/lang/Boolean;

    if-nez v8, :cond_1

    .line 268
    new-instance v1, Lorg/json/JSONException;

    const-string v3, "%s contains %s instead of a Boolean"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "supports_attribution"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :catch_0
    move-exception v0

    .line 292
    const-string v1, "Facebook-publish"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/b/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_1
    :try_start_1
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    invoke-static {}, Lcom/facebook/model/GraphObject$Factory;->create()Lcom/facebook/model/GraphObject;

    move-result-object v0

    .line 274
    const-string v3, "event"

    const-string v4, "MOBILE_APP_INSTALL"

    invoke-interface {v0, v3, v4}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 275
    const-string v3, "attribution"

    invoke-interface {v0, v3, v5}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    const-string v3, "%s/activities"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 279
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v3, v0, v5}, Lcom/facebook/am;->a(Lcom/facebook/ba;Ljava/lang/String;Lcom/facebook/model/GraphObject;Lcom/facebook/ar;)Lcom/facebook/am;

    move-result-object v0

    .line 280
    invoke-virtual {v0}, Lcom/facebook/am;->b()Lcom/facebook/az;

    .line 283
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 284
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 285
    invoke-interface {v0, v7, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 286
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    :cond_2
    cmp-long v0, v3, v10

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    move v2, v0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public static final a(Lcom/facebook/ak;)Z
    .locals 2

    .prologue
    .line 143
    sget-object v1, Lcom/facebook/bu;->a:Ljava/util/HashSet;

    monitor-enter v1

    .line 144
    const/4 v0, 0x0

    :try_start_0
    monitor-exit v1

    return v0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 239
    sget-boolean v0, Lcom/facebook/bu;->c:Z

    return v0
.end method

.method private static c()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 186
    :try_start_0
    const-class v0, Landroid/os/AsyncTask;

    const-string v2, "THREAD_POOL_EXECUTOR"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 191
    if-nez v0, :cond_0

    move-object v0, v1

    .line 210
    :goto_0
    return-object v0

    .line 187
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 188
    goto :goto_0

    .line 197
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 202
    if-nez v0, :cond_1

    move-object v0, v1

    .line 203
    goto :goto_0

    .line 198
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 199
    goto :goto_0

    .line 206
    :cond_1
    instance-of v2, v0, Ljava/util/concurrent/Executor;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 207
    goto :goto_0

    .line 210
    :cond_2
    check-cast v0, Ljava/util/concurrent/Executor;

    goto :goto_0
.end method
