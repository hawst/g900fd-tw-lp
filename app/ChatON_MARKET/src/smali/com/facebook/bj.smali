.class Lcom/facebook/bj;
.super Landroid/os/AsyncTask;
.source "Session.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/facebook/ba;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/ba;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1497
    iput-object p1, p0, Lcom/facebook/bj;->a:Lcom/facebook/ba;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1498
    iput-object p2, p0, Lcom/facebook/bj;->b:Ljava/lang/String;

    .line 1499
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bj;->c:Landroid/content/Context;

    .line 1500
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 1505
    :try_start_0
    iget-object v0, p0, Lcom/facebook/bj;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/bj;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/bu;->a(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1509
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1506
    :catch_0
    move-exception v0

    .line 1507
    const-string v1, "Facebook-publish"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/b/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 1515
    iget-object v1, p0, Lcom/facebook/bj;->a:Lcom/facebook/ba;

    monitor-enter v1

    .line 1516
    :try_start_0
    iget-object v0, p0, Lcom/facebook/bj;->a:Lcom/facebook/ba;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/ba;->a(Lcom/facebook/ba;Lcom/facebook/bj;)Lcom/facebook/bj;

    .line 1517
    monitor-exit v1

    .line 1518
    return-void

    .line 1517
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1493
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/facebook/bj;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1493
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/facebook/bj;->a(Ljava/lang/Void;)V

    return-void
.end method
