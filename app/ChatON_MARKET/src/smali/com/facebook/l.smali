.class Lcom/facebook/l;
.super Lcom/facebook/i;
.source "AuthorizationClient.java"


# instance fields
.field final synthetic b:Lcom/facebook/c;

.field private transient c:Lcom/facebook/ag;


# direct methods
.method constructor <init>(Lcom/facebook/c;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    invoke-direct {p0, p1}, Lcom/facebook/i;-><init>(Lcom/facebook/c;)V

    return-void
.end method


# virtual methods
.method a(Lcom/facebook/j;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    .line 524
    iget-object v0, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    invoke-static {v0}, Lcom/facebook/c;->a(Lcom/facebook/c;)V

    .line 526
    if-eqz p2, :cond_4

    .line 527
    const-string v0, "com.facebook.platform.extra.PERMISSIONS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 528
    invoke-virtual {p1}, Lcom/facebook/j;->b()Ljava/util/List;

    move-result-object v0

    .line 529
    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 532
    :cond_0
    sget-object v0, Lcom/facebook/b;->d:Lcom/facebook/b;

    invoke-static {p2, v0}, Lcom/facebook/a;->a(Landroid/os/Bundle;Lcom/facebook/b;)Lcom/facebook/a;

    move-result-object v0

    .line 534
    invoke-static {v0}, Lcom/facebook/r;->a(Lcom/facebook/a;)Lcom/facebook/r;

    move-result-object v0

    .line 535
    iget-object v1, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    invoke-virtual {v1, v0}, Lcom/facebook/c;->a(Lcom/facebook/r;)V

    .line 551
    :goto_0
    return-void

    .line 541
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 542
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 543
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 544
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 547
    :cond_3
    invoke-virtual {p1, v2}, Lcom/facebook/j;->a(Ljava/util/List;)V

    .line 550
    :cond_4
    iget-object v0, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    invoke-virtual {v0}, Lcom/facebook/c;->d()V

    goto :goto_0
.end method

.method a(Lcom/facebook/j;)Z
    .locals 3

    .prologue
    .line 503
    new-instance v0, Lcom/facebook/ag;

    iget-object v1, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    iget-object v1, v1, Lcom/facebook/c;->c:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/j;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/ag;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    .line 504
    iget-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    invoke-virtual {v0}, Lcom/facebook/ag;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    const/4 v0, 0x0

    .line 518
    :goto_0
    return v0

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/facebook/l;->b:Lcom/facebook/c;

    invoke-static {v0}, Lcom/facebook/c;->b(Lcom/facebook/c;)V

    .line 510
    new-instance v0, Lcom/facebook/m;

    invoke-direct {v0, p0, p1}, Lcom/facebook/m;-><init>(Lcom/facebook/l;Lcom/facebook/j;)V

    .line 517
    iget-object v1, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    invoke-virtual {v1, v0}, Lcom/facebook/ag;->a(Lcom/facebook/ai;)V

    .line 518
    const/4 v0, 0x1

    goto :goto_0
.end method

.method c()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    invoke-virtual {v0}, Lcom/facebook/ag;->b()V

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/l;->c:Lcom/facebook/ag;

    .line 500
    :cond_0
    return-void
.end method
