.class Lcom/facebook/bo;
.super Ljava/lang/Object;
.source "Session.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final a:Landroid/os/Messenger;

.field b:Landroid/os/Messenger;

.field final synthetic c:Lcom/facebook/ba;


# direct methods
.method constructor <init>(Lcom/facebook/ba;)V
    .locals 3

    .prologue
    .line 1291
    iput-object p1, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1293
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/facebook/bp;

    iget-object v2, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    invoke-direct {v1, v2, p0}, Lcom/facebook/bp;-><init>(Lcom/facebook/ba;Lcom/facebook/bo;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/bo;->a:Landroid/os/Messenger;

    .line 1296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/bo;->b:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic a(Lcom/facebook/bo;)V
    .locals 0

    .prologue
    .line 1291
    invoke-direct {p0}, Lcom/facebook/bo;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    invoke-static {v0}, Lcom/facebook/ba;->d(Lcom/facebook/ba;)Lcom/facebook/bo;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1325
    iget-object v0, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/ba;->a(Lcom/facebook/ba;Lcom/facebook/bo;)Lcom/facebook/bo;

    .line 1327
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1330
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1331
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    invoke-virtual {v2}, Lcom/facebook/ba;->o()Lcom/facebook/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1334
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1335
    iget-object v0, p0, Lcom/facebook/bo;->a:Landroid/os/Messenger;

    iput-object v0, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 1338
    :try_start_0
    iget-object v0, p0, Lcom/facebook/bo;->b:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1342
    :goto_0
    return-void

    .line 1339
    :catch_0
    move-exception v0

    .line 1340
    invoke-direct {p0}, Lcom/facebook/bo;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1299
    invoke-static {}, Lcom/facebook/ba;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/al;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1300
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/ba;->p()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/bo;

    iget-object v3, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    invoke-direct {v2, v3}, Lcom/facebook/bo;-><init>(Lcom/facebook/ba;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1302
    iget-object v0, p0, Lcom/facebook/bo;->c:Lcom/facebook/ba;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->a(Ljava/util/Date;)V

    .line 1306
    :goto_0
    return-void

    .line 1304
    :cond_0
    invoke-direct {p0}, Lcom/facebook/bo;->b()V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1310
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/facebook/bo;->b:Landroid/os/Messenger;

    .line 1311
    invoke-direct {p0}, Lcom/facebook/bo;->c()V

    .line 1312
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 1316
    invoke-direct {p0}, Lcom/facebook/bo;->b()V

    .line 1320
    invoke-static {}, Lcom/facebook/ba;->p()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1321
    return-void
.end method
