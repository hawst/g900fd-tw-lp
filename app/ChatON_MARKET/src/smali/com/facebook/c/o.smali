.class public Lcom/facebook/c/o;
.super Landroid/app/Dialog;
.source "WebDialog.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/facebook/c/w;

.field private c:Landroid/webkit/WebView;

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/FrameLayout;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILcom/facebook/c/w;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    invoke-direct {p0, p1, p4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 69
    iput-boolean v0, p0, Lcom/facebook/c/o;->g:Z

    .line 70
    iput-boolean v0, p0, Lcom/facebook/c/o;->h:Z

    .line 123
    if-nez p3, :cond_0

    .line 124
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 126
    :cond_0
    const-string v0, "display"

    const-string v1, "touch"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "type"

    const-string v1, "user_agent"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "m.facebook.com"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dialog/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/facebook/b/s;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/c/o;->a:Ljava/lang/String;

    .line 131
    iput-object p5, p0, Lcom/facebook/c/o;->b:Lcom/facebook/c/w;

    .line 132
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/facebook/aa;

    invoke-direct {v0}, Lcom/facebook/aa;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/c/o;->a(Ljava/lang/Throwable;)V

    .line 249
    return-void
.end method

.method private a(I)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 271
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 272
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    .line 273
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 274
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 275
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/facebook/c/u;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/facebook/c/u;-><init>(Lcom/facebook/c/o;Lcom/facebook/c/p;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 276
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 277
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/facebook/c/o;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 278
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 281
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 283
    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 284
    iget-object v1, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 285
    iget-object v1, p0, Lcom/facebook/c/o;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 286
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/facebook/c/o;->b:Lcom/facebook/c/w;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/c/o;->g:Z

    if-nez v0, :cond_0

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/c/o;->g:Z

    .line 226
    iget-object v0, p0, Lcom/facebook/c/o;->b:Lcom/facebook/c/w;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/facebook/c/w;->a(Landroid/os/Bundle;Lcom/facebook/y;)V

    .line 228
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/facebook/c/o;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/facebook/c/o;->a()V

    return-void
.end method

.method static synthetic a(Lcom/facebook/c/o;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/facebook/c/o;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/facebook/c/o;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/facebook/c/o;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/facebook/c/o;->b:Lcom/facebook/c/w;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/c/o;->g:Z

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/c/o;->g:Z

    .line 235
    instance-of v0, p1, Lcom/facebook/aa;

    if-eqz v0, :cond_1

    .line 236
    check-cast p1, Lcom/facebook/aa;

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/facebook/c/o;->b:Lcom/facebook/c/w;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/facebook/c/w;->a(Landroid/os/Bundle;Lcom/facebook/y;)V

    .line 245
    :cond_0
    return-void

    .line 238
    :cond_1
    instance-of v0, p1, Lcom/facebook/y;

    if-eqz v0, :cond_2

    .line 239
    check-cast p1, Lcom/facebook/y;

    goto :goto_0

    .line 241
    :cond_2
    new-instance v0, Lcom/facebook/y;

    invoke-direct {v0, p1}, Lcom/facebook/y;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 252
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    .line 254
    iget-object v0, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    new-instance v1, Lcom/facebook/c/r;

    invoke-direct {v1, p0}, Lcom/facebook/c/r;-><init>(Lcom/facebook/c/o;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v0, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    return-void
.end method

.method static synthetic b(Lcom/facebook/c/o;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/facebook/c/o;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/facebook/c/o;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic d(Lcom/facebook/c/o;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/facebook/c/o;->f:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic e(Lcom/facebook/c/o;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic f(Lcom/facebook/c/o;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/facebook/c/o;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 157
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/c/o;->h:Z

    if-nez v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 161
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 163
    :cond_2
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/c/o;->h:Z

    .line 174
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 175
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 179
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 181
    new-instance v0, Lcom/facebook/c/p;

    invoke-direct {v0, p0}, Lcom/facebook/c/p;-><init>(Lcom/facebook/c/o;)V

    invoke-virtual {p0, v0}, Lcom/facebook/c/o;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 188
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    .line 189
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 190
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/facebook/c/o;->d:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/facebook/c/q;

    invoke-direct {v1, p0}, Lcom/facebook/c/q;-><init>(Lcom/facebook/c/o;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 199
    invoke-virtual {p0, v5}, Lcom/facebook/c/o;->requestWindowFeature(I)Z

    .line 200
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/c/o;->f:Landroid/widget/FrameLayout;

    .line 206
    invoke-direct {p0}, Lcom/facebook/c/o;->b()V

    .line 211
    iget-object v0, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 212
    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/c/o;->a(I)V

    .line 217
    iget-object v0, p0, Lcom/facebook/c/o;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/c/o;->e:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    iget-object v0, p0, Lcom/facebook/c/o;->f:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/c/o;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/c/o;->h:Z

    .line 168
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 169
    return-void
.end method
