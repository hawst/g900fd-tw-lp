.class Lcom/facebook/c/t;
.super Ljava/lang/Object;
.source "WebDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONCRETE:",
        "Lcom/facebook/c/t",
        "<*>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/ba;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Lcom/facebook/c/w;

.field private g:Landroid/os/Bundle;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/facebook/ba;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    const v0, 0x1030010

    iput v0, p0, Lcom/facebook/c/t;->e:I

    .line 396
    const-string v0, "session"

    invoke-static {p2, v0}, Lcom/facebook/b/t;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 397
    invoke-virtual {p2}, Lcom/facebook/ba;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Attempted to use a Session that was not open."

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :cond_0
    iput-object p2, p0, Lcom/facebook/c/t;->b:Lcom/facebook/ba;

    .line 402
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/c/t;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 403
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    const v0, 0x1030010

    iput v0, p0, Lcom/facebook/c/t;->e:I

    .line 406
    const-string v0, "applicationId"

    invoke-static {p2, v0}, Lcom/facebook/b/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iput-object p2, p0, Lcom/facebook/c/t;->c:Ljava/lang/String;

    .line 409
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/c/t;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 410
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 480
    iput-object p1, p0, Lcom/facebook/c/t;->a:Landroid/content/Context;

    .line 481
    iput-object p2, p0, Lcom/facebook/c/t;->d:Ljava/lang/String;

    .line 482
    if-eqz p3, :cond_0

    .line 483
    iput-object p3, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    .line 487
    :goto_0
    return-void

    .line 485
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/facebook/c/o;
    .locals 6

    .prologue
    .line 445
    iget-object v0, p0, Lcom/facebook/c/t;->b:Lcom/facebook/ba;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/c/t;->b:Lcom/facebook/ba;

    invoke-virtual {v0}, Lcom/facebook/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    const-string v1, "app_id"

    iget-object v2, p0, Lcom/facebook/c/t;->b:Lcom/facebook/ba;

    invoke-virtual {v2}, Lcom/facebook/ba;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    const-string v1, "access_token"

    iget-object v2, p0, Lcom/facebook/c/t;->b:Lcom/facebook/ba;

    invoke-virtual {v2}, Lcom/facebook/ba;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :goto_0
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    const-string v1, "redirect_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    const-string v1, "redirect_uri"

    const-string v2, "fbconnect://success"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :cond_0
    new-instance v0, Lcom/facebook/c/o;

    iget-object v1, p0, Lcom/facebook/c/t;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/c/t;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    iget v4, p0, Lcom/facebook/c/t;->e:I

    iget-object v5, p0, Lcom/facebook/c/t;->f:Lcom/facebook/c/w;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/c/o;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILcom/facebook/c/w;)V

    return-object v0

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    const-string v1, "app_id"

    iget-object v2, p0, Lcom/facebook/c/t;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/facebook/c/w;)Lcom/facebook/c/t;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/c/w;",
            ")TCONCRETE;"
        }
    .end annotation

    .prologue
    .line 432
    iput-object p1, p0, Lcom/facebook/c/t;->f:Lcom/facebook/c/w;

    .line 435
    return-object p0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/facebook/c/t;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/facebook/c/t;->a:Landroid/content/Context;

    return-object v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/facebook/c/t;->e:I

    return v0
.end method

.method protected e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/facebook/c/t;->g:Landroid/os/Bundle;

    return-object v0
.end method

.method protected f()Lcom/facebook/c/w;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/facebook/c/t;->f:Lcom/facebook/c/w;

    return-object v0
.end method
