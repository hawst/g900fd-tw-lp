.class public Lcom/facebook/c/l;
.super Ljava/lang/Object;
.source "ImageResponseCache.java"


# static fields
.field static final a:Ljava/lang/String;

.field private static volatile b:Lcom/facebook/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/facebook/c/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/c/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/facebook/b/a;
    .locals 5

    .prologue
    .line 38
    const-class v1, Lcom/facebook/c/l;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/c/l;->b:Lcom/facebook/b/a;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/facebook/b/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/c/l;->a:Ljava/lang/String;

    new-instance v4, Lcom/facebook/b/i;

    invoke-direct {v4}, Lcom/facebook/b/i;-><init>()V

    invoke-direct {v0, v2, v3, v4}, Lcom/facebook/b/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/b/i;)V

    sput-object v0, Lcom/facebook/c/l;->b:Lcom/facebook/b/a;

    .line 41
    :cond_0
    sget-object v0, Lcom/facebook/c/l;->b:Lcom/facebook/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Landroid/content/Context;Ljava/net/HttpURLConnection;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    .line 65
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 67
    invoke-static {v1}, Lcom/facebook/c/l;->a(Ljava/net/URL;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    :try_start_0
    invoke-static {p0}, Lcom/facebook/c/l;->a(Landroid/content/Context;)Lcom/facebook/b/a;

    move-result-object v2

    .line 72
    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/facebook/c/m;

    invoke-direct {v3, v0, p1}, Lcom/facebook/c/m;-><init>(Ljava/io/InputStream;Ljava/net/HttpURLConnection;)V

    invoke-virtual {v2, v1, v3}, Lcom/facebook/b/a;->a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 80
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static a(Ljava/net/URL;Landroid/content/Context;)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 47
    const/4 v0, 0x0

    .line 48
    if-eqz p0, :cond_0

    .line 49
    invoke-static {p0}, Lcom/facebook/c/l;->a(Ljava/net/URL;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    :try_start_0
    invoke-static {p1}, Lcom/facebook/c/l;->a(Landroid/content/Context;)Lcom/facebook/b/a;

    move-result-object v1

    .line 52
    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/b/a;->a(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    :cond_0
    :goto_0
    return-object v0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    sget-object v2, Lcom/facebook/ak;->d:Lcom/facebook/ak;

    const/4 v3, 0x5

    sget-object v4, Lcom/facebook/c/l;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v4, v1}, Lcom/facebook/b/m;->a(Lcom/facebook/ak;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/net/URL;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 84
    if-eqz p0, :cond_2

    .line 85
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 87
    const-string v2, "fbcdn.net"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const-string v2, "fbcdn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "akamaihd.net"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
