.class public Lcom/facebook/c/g;
.super Ljava/lang/Object;
.source "ImageRequest.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/net/URL;

.field private d:Lcom/facebook/c/j;

.field private e:Z

.field private f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/c/b;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%s/picture"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/c/g;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/facebook/c/i;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p1}, Lcom/facebook/c/i;->a(Lcom/facebook/c/i;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/c/g;->b:Landroid/content/Context;

    .line 83
    invoke-static {p1}, Lcom/facebook/c/i;->b(Lcom/facebook/c/i;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/c/g;->c:Ljava/net/URL;

    .line 84
    invoke-static {p1}, Lcom/facebook/c/i;->c(Lcom/facebook/c/i;)Lcom/facebook/c/j;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/c/g;->d:Lcom/facebook/c/j;

    .line 85
    invoke-static {p1}, Lcom/facebook/c/i;->d(Lcom/facebook/c/i;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/c/g;->e:Z

    .line 86
    invoke-static {p1}, Lcom/facebook/c/i;->e(Lcom/facebook/c/i;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/facebook/c/g;->f:Ljava/lang/Object;

    .line 87
    return-void

    .line 86
    :cond_0
    invoke-static {p1}, Lcom/facebook/c/i;->e(Lcom/facebook/c/i;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/facebook/c/i;Lcom/facebook/c/h;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/facebook/c/g;-><init>(Lcom/facebook/c/i;)V

    return-void
.end method


# virtual methods
.method a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/facebook/c/g;->b:Landroid/content/Context;

    return-object v0
.end method

.method public b()Ljava/net/URL;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/facebook/c/g;->c:Ljava/net/URL;

    return-object v0
.end method

.method c()Lcom/facebook/c/j;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/facebook/c/g;->d:Lcom/facebook/c/j;

    return-object v0
.end method

.method d()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/facebook/c/g;->e:Z

    return v0
.end method

.method e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/facebook/c/g;->f:Ljava/lang/Object;

    return-object v0
.end method
