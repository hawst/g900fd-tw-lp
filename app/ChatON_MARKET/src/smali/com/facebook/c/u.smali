.class Lcom/facebook/c/u;
.super Landroid/webkit/WebViewClient;
.source "WebDialog.java"


# instance fields
.field final synthetic a:Lcom/facebook/c/o;


# direct methods
.method private constructor <init>(Lcom/facebook/c/o;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/c/o;Lcom/facebook/c/p;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/facebook/c/u;-><init>(Lcom/facebook/c/o;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->b(Lcom/facebook/c/o;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->c(Lcom/facebook/c/o;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->d(Lcom/facebook/c/o;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 378
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->e(Lcom/facebook/c/o;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->f(Lcom/facebook/c/o;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 380
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 360
    const-string v0, "FacebookSDK.WebDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Webview loading URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/b/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 362
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->b(Lcom/facebook/c/o;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->c(Lcom/facebook/c/o;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 365
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 344
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    new-instance v1, Lcom/facebook/x;

    invoke-direct {v1, p3, p2, p4}, Lcom/facebook/x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;Ljava/lang/Throwable;)V

    .line 346
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->dismiss()V

    .line 347
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 351
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 353
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    new-instance v1, Lcom/facebook/x;

    const/16 v2, -0xb

    invoke-direct {v1, v3, v2, v3}, Lcom/facebook/x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;Ljava/lang/Throwable;)V

    .line 354
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 355
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->dismiss()V

    .line 356
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 292
    const-string v0, "FacebookSDK.WebDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redirect URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/b/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v0, "fbconnect://success"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294
    invoke-static {p2}, Lcom/facebook/a/f;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 296
    const-string v0, "error"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    if-nez v0, :cond_0

    .line 298
    const-string v0, "error_type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    :cond_0
    const-string v1, "error_msg"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    if-nez v1, :cond_1

    .line 303
    const-string v1, "error_description"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 305
    :cond_1
    const-string v2, "error_code"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 307
    invoke-static {v2}, Lcom/facebook/b/s;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 309
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 315
    :goto_0
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-ne v2, v3, :cond_2

    .line 317
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0, v5}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;Landroid/os/Bundle;)V

    .line 326
    :goto_1
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->dismiss()V

    move v0, v4

    .line 338
    :goto_2
    return v0

    .line 310
    :catch_0
    move-exception v2

    move v2, v3

    .line 311
    goto :goto_0

    .line 318
    :cond_2
    if-eqz v0, :cond_4

    const-string v3, "access_denied"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "OAuthAccessDeniedException"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 320
    :cond_3
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;)V

    goto :goto_1

    .line 322
    :cond_4
    new-instance v3, Lcom/facebook/ab;

    invoke-direct {v3, v2, v0, v1}, Lcom/facebook/ab;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    new-instance v2, Lcom/facebook/af;

    invoke-direct {v2, v3, v1}, Lcom/facebook/af;-><init>(Lcom/facebook/ab;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 328
    :cond_5
    const-string v0, "fbconnect://cancel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 329
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-static {v0}, Lcom/facebook/c/o;->a(Lcom/facebook/c/o;)V

    .line 330
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->dismiss()V

    move v0, v4

    .line 331
    goto :goto_2

    .line 332
    :cond_6
    const-string v0, "touch"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 333
    const/4 v0, 0x0

    goto :goto_2

    .line 336
    :cond_7
    iget-object v0, p0, Lcom/facebook/c/u;->a:Lcom/facebook/c/o;

    invoke-virtual {v0}, Lcom/facebook/c/o;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v0, v4

    .line 338
    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_0
.end method
