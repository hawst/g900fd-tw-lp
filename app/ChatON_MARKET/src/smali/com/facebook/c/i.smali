.class public Lcom/facebook/c/i;
.super Ljava/lang/Object;
.source "ImageRequest.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/net/URL;

.field private c:Lcom/facebook/c/j;

.field private d:Z

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string v0, "imageUrl"

    invoke-static {p2, v0}, Lcom/facebook/b/t;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iput-object p1, p0, Lcom/facebook/c/i;->a:Landroid/content/Context;

    .line 122
    iput-object p2, p0, Lcom/facebook/c/i;->b:Ljava/net/URL;

    .line 123
    return-void
.end method

.method static synthetic a(Lcom/facebook/c/i;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/facebook/c/i;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/facebook/c/i;)Ljava/net/URL;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/facebook/c/i;->b:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic c(Lcom/facebook/c/i;)Lcom/facebook/c/j;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/facebook/c/i;->c:Lcom/facebook/c/j;

    return-object v0
.end method

.method static synthetic d(Lcom/facebook/c/i;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/facebook/c/i;->d:Z

    return v0
.end method

.method static synthetic e(Lcom/facebook/c/i;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/facebook/c/i;->e:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/facebook/c/g;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Lcom/facebook/c/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/c/g;-><init>(Lcom/facebook/c/i;Lcom/facebook/c/h;)V

    return-object v0
.end method

.method public a(Lcom/facebook/c/j;)Lcom/facebook/c/i;
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/facebook/c/i;->c:Lcom/facebook/c/j;

    .line 127
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/facebook/c/i;
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/facebook/c/i;->e:Ljava/lang/Object;

    .line 132
    return-object p0
.end method
