.class Lcom/facebook/c/n;
.super Ljava/lang/Object;
.source "UrlRedirectCache.java"


# static fields
.field static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile c:Lcom/facebook/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-class v0, Lcom/facebook/c/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/c/n;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/c/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Redirect"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/c/n;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/facebook/b/a;
    .locals 5

    .prologue
    .line 37
    const-class v1, Lcom/facebook/c/n;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/c/n;->c:Lcom/facebook/b/a;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/facebook/b/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/c/n;->a:Ljava/lang/String;

    new-instance v4, Lcom/facebook/b/i;

    invoke-direct {v4}, Lcom/facebook/b/i;-><init>()V

    invoke-direct {v0, v2, v3, v4}, Lcom/facebook/b/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/b/i;)V

    sput-object v0, Lcom/facebook/c/n;->c:Lcom/facebook/b/a;

    .line 40
    :cond_0
    sget-object v0, Lcom/facebook/c/n;->c:Lcom/facebook/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Landroid/content/Context;Ljava/net/URL;)Ljava/net/URL;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    if-nez p1, :cond_0

    .line 82
    :goto_0
    return-object v1

    .line 48
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    .line 53
    :try_start_0
    invoke-static {p0}, Lcom/facebook/c/n;->a(Landroid/content/Context;)Lcom/facebook/b/a;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    move-object v3, v2

    move-object v2, v1

    .line 55
    :goto_1
    :try_start_1
    sget-object v5, Lcom/facebook/c/n;->b:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 56
    const/4 v3, 0x1

    .line 59
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 60
    const/16 v2, 0x80

    :try_start_2
    new-array v2, v2, [C

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    :goto_2
    const/4 v6, 0x0

    array-length v7, v2

    invoke-virtual {v0, v2, v6, v7}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v6

    if-lez v6, :cond_1

    .line 64
    const/4 v7, 0x0

    invoke-virtual {v5, v2, v7, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 75
    :catch_0
    move-exception v2

    .line 79
    :goto_3
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 66
    :cond_1
    :try_start_3
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    .line 69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    move v8, v3

    move-object v3, v2

    move-object v2, v0

    move v0, v8

    .line 70
    goto :goto_1

    .line 72
    :cond_2
    if-eqz v0, :cond_3

    .line 73
    :try_start_4
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 79
    :goto_4
    invoke-static {v2}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    move-object v1, v0

    .line 80
    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 79
    :goto_5
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_6
    invoke-static {v2}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    .line 77
    :catch_2
    move-exception v2

    goto :goto_5

    :catch_3
    move-exception v0

    move-object v0, v2

    goto :goto_5

    .line 75
    :catch_4
    move-exception v0

    move-object v0, v1

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v0, v2

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_4
.end method

.method static a(Landroid/content/Context;Ljava/net/URL;Ljava/net/URL;)V
    .locals 5

    .prologue
    .line 86
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    .line 92
    :try_start_0
    invoke-static {p0}, Lcom/facebook/c/n;->a(Landroid/content/Context;)Lcom/facebook/b/a;

    move-result-object v1

    .line 93
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/c/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 94
    :try_start_1
    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 98
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 95
    :catch_0
    move-exception v1

    .line 98
    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    invoke-static {v1}, Lcom/facebook/b/s;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1
.end method
