.class public final enum Lcom/facebook/bs;
.super Ljava/lang/Enum;
.source "SessionState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/bs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/bs;

.field public static final enum b:Lcom/facebook/bs;

.field public static final enum c:Lcom/facebook/bs;

.field public static final enum d:Lcom/facebook/bs;

.field public static final enum e:Lcom/facebook/bs;

.field public static final enum f:Lcom/facebook/bs;

.field public static final enum g:Lcom/facebook/bs;

.field private static final synthetic i:[Lcom/facebook/bs;


# instance fields
.field private final h:Lcom/facebook/bt;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "CREATED"

    sget-object v2, Lcom/facebook/bt;->a:Lcom/facebook/bt;

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->a:Lcom/facebook/bs;

    .line 47
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "CREATED_TOKEN_LOADED"

    sget-object v2, Lcom/facebook/bt;->a:Lcom/facebook/bt;

    invoke-direct {v0, v1, v5, v2}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->b:Lcom/facebook/bs;

    .line 52
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "OPENING"

    sget-object v2, Lcom/facebook/bt;->a:Lcom/facebook/bt;

    invoke-direct {v0, v1, v6, v2}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->c:Lcom/facebook/bs;

    .line 58
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "OPENED"

    sget-object v2, Lcom/facebook/bt;->b:Lcom/facebook/bt;

    invoke-direct {v0, v1, v7, v2}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->d:Lcom/facebook/bs;

    .line 70
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "OPENED_TOKEN_UPDATED"

    sget-object v2, Lcom/facebook/bt;->b:Lcom/facebook/bt;

    invoke-direct {v0, v1, v8, v2}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->e:Lcom/facebook/bs;

    .line 78
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "CLOSED_LOGIN_FAILED"

    const/4 v2, 0x5

    sget-object v3, Lcom/facebook/bt;->c:Lcom/facebook/bt;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->f:Lcom/facebook/bs;

    .line 83
    new-instance v0, Lcom/facebook/bs;

    const-string v1, "CLOSED"

    const/4 v2, 0x6

    sget-object v3, Lcom/facebook/bt;->c:Lcom/facebook/bt;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/bs;-><init>(Ljava/lang/String;ILcom/facebook/bt;)V

    sput-object v0, Lcom/facebook/bs;->g:Lcom/facebook/bs;

    .line 28
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/bs;

    sget-object v1, Lcom/facebook/bs;->a:Lcom/facebook/bs;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/bs;->b:Lcom/facebook/bs;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/bs;->c:Lcom/facebook/bs;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/bs;->d:Lcom/facebook/bs;

    aput-object v1, v0, v7

    sget-object v1, Lcom/facebook/bs;->e:Lcom/facebook/bs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/bs;->f:Lcom/facebook/bs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/bs;->g:Lcom/facebook/bs;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/bs;->i:[Lcom/facebook/bs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/bt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/bt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 88
    iput-object p3, p0, Lcom/facebook/bs;->h:Lcom/facebook/bt;

    .line 89
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/bs;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/facebook/bs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/bs;

    return-object v0
.end method

.method public static values()[Lcom/facebook/bs;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/facebook/bs;->i:[Lcom/facebook/bs;

    invoke-virtual {v0}, [Lcom/facebook/bs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/bs;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/bs;->h:Lcom/facebook/bt;

    sget-object v1, Lcom/facebook/bt;->b:Lcom/facebook/bt;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/facebook/bs;->h:Lcom/facebook/bt;

    sget-object v1, Lcom/facebook/bt;->c:Lcom/facebook/bt;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
