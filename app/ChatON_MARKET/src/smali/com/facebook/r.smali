.class Lcom/facebook/r;
.super Ljava/lang/Object;
.source "AuthorizationClient.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field final a:Lcom/facebook/s;

.field final b:Lcom/facebook/a;

.field final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/facebook/s;Lcom/facebook/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 784
    iput-object p2, p0, Lcom/facebook/r;->b:Lcom/facebook/a;

    .line 785
    iput-object p3, p0, Lcom/facebook/r;->c:Ljava/lang/String;

    .line 786
    iput-object p1, p0, Lcom/facebook/r;->a:Lcom/facebook/s;

    .line 787
    return-void
.end method

.method static a(Lcom/facebook/a;)Lcom/facebook/r;
    .locals 3

    .prologue
    .line 790
    new-instance v0, Lcom/facebook/r;

    sget-object v1, Lcom/facebook/s;->a:Lcom/facebook/s;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/facebook/r;-><init>(Lcom/facebook/s;Lcom/facebook/a;Ljava/lang/String;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;)Lcom/facebook/r;
    .locals 3

    .prologue
    .line 794
    new-instance v0, Lcom/facebook/r;

    sget-object v1, Lcom/facebook/s;->b:Lcom/facebook/s;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/facebook/r;-><init>(Lcom/facebook/s;Lcom/facebook/a;Ljava/lang/String;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/r;
    .locals 3

    .prologue
    .line 798
    .line 799
    if-eqz p1, :cond_0

    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 802
    :cond_0
    new-instance v0, Lcom/facebook/r;

    sget-object v1, Lcom/facebook/s;->c:Lcom/facebook/s;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/facebook/r;-><init>(Lcom/facebook/s;Lcom/facebook/a;Ljava/lang/String;)V

    return-object v0
.end method
