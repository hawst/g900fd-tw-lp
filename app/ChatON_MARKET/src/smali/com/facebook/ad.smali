.class public final enum Lcom/facebook/ad;
.super Ljava/lang/Enum;
.source "FacebookRequestError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/ad;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/ad;

.field public static final enum b:Lcom/facebook/ad;

.field public static final enum c:Lcom/facebook/ad;

.field public static final enum d:Lcom/facebook/ad;

.field public static final enum e:Lcom/facebook/ad;

.field public static final enum f:Lcom/facebook/ad;

.field public static final enum g:Lcom/facebook/ad;

.field public static final enum h:Lcom/facebook/ad;

.field private static final synthetic i:[Lcom/facebook/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 411
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "AUTHENTICATION_RETRY"

    invoke-direct {v0, v1, v3}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->a:Lcom/facebook/ad;

    .line 417
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "AUTHENTICATION_REOPEN_SESSION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->b:Lcom/facebook/ad;

    .line 420
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "PERMISSION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->c:Lcom/facebook/ad;

    .line 426
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->d:Lcom/facebook/ad;

    .line 429
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "THROTTLING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->e:Lcom/facebook/ad;

    .line 435
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "OTHER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->f:Lcom/facebook/ad;

    .line 441
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "BAD_REQUEST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->g:Lcom/facebook/ad;

    .line 447
    new-instance v0, Lcom/facebook/ad;

    const-string v1, "CLIENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ad;->h:Lcom/facebook/ad;

    .line 406
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/ad;

    sget-object v1, Lcom/facebook/ad;->a:Lcom/facebook/ad;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/ad;->b:Lcom/facebook/ad;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/ad;->c:Lcom/facebook/ad;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/ad;->d:Lcom/facebook/ad;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/ad;->e:Lcom/facebook/ad;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/ad;->f:Lcom/facebook/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/ad;->g:Lcom/facebook/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/ad;->h:Lcom/facebook/ad;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/ad;->i:[Lcom/facebook/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 406
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/ad;
    .locals 1

    .prologue
    .line 406
    const-class v0, Lcom/facebook/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/ad;

    return-object v0
.end method

.method public static values()[Lcom/facebook/ad;
    .locals 1

    .prologue
    .line 406
    sget-object v0, Lcom/facebook/ad;->i:[Lcom/facebook/ad;

    invoke-virtual {v0}, [Lcom/facebook/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/ad;

    return-object v0
.end method
