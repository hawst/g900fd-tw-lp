.class Lcom/facebook/h;
.super Lcom/facebook/c/s;
.source "AuthorizationClient.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 685
    const-string v0, "oauth"

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/facebook/c/s;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 686
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/c/o;
    .locals 6

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/facebook/h;->e()Landroid/os/Bundle;

    move-result-object v3

    .line 691
    const-string v0, "redirect_uri"

    const-string v1, "fbconnect://success"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    const-string v0, "client_id"

    invoke-virtual {p0}, Lcom/facebook/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    new-instance v0, Lcom/facebook/c/o;

    invoke-virtual {p0}, Lcom/facebook/h;->c()Landroid/content/Context;

    move-result-object v1

    const-string v2, "oauth"

    invoke-virtual {p0}, Lcom/facebook/h;->d()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/h;->f()Lcom/facebook/c/w;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/c/o;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILcom/facebook/c/w;)V

    return-object v0
.end method
