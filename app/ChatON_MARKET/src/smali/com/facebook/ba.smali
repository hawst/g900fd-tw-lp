.class public Lcom/facebook/ba;
.super Ljava/lang/Object;
.source "Session.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/facebook/ba;

.field private static volatile d:Landroid/content/Context;

.field private static final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private f:Ljava/lang/String;

.field private g:Lcom/facebook/bs;

.field private h:Lcom/facebook/a;

.field private i:Ljava/util/Date;

.field private j:Lcom/facebook/bg;

.field private k:Lcom/facebook/c;

.field private volatile l:Landroid/os/Bundle;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bn;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/os/Handler;

.field private o:Lcom/facebook/bj;

.field private final p:Ljava/lang/Object;

.field private q:Lcom/facebook/bx;

.field private volatile r:Lcom/facebook/bo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/facebook/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/ba;->a:Ljava/lang/String;

    .line 115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/ba;->b:Ljava/lang/Object;

    .line 130
    new-instance v0, Lcom/facebook/bb;

    invoke-direct {v0}, Lcom/facebook/bb;-><init>()V

    sput-object v0, Lcom/facebook/ba;->e:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/bx;)V
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/ba;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/bx;Z)V

    .line 224
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/bx;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/facebook/ba;->i:Ljava/util/Date;

    .line 150
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    .line 230
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 231
    const-string p2, "254066384616989"

    .line 235
    :cond_0
    const-string v1, "applicationId"

    invoke-static {p2, v1}, Lcom/facebook/b/t;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    invoke-static {p1}, Lcom/facebook/ba;->b(Landroid/content/Context;)V

    .line 239
    if-nez p3, :cond_1

    .line 240
    new-instance p3, Lcom/facebook/bw;

    sget-object v1, Lcom/facebook/ba;->d:Landroid/content/Context;

    invoke-direct {p3, v1}, Lcom/facebook/bw;-><init>(Landroid/content/Context;)V

    .line 243
    :cond_1
    iput-object p2, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    .line 244
    iput-object p3, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    .line 245
    sget-object v1, Lcom/facebook/bs;->a:Lcom/facebook/bs;

    iput-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 246
    iput-object v0, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    .line 247
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    .line 248
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/facebook/ba;->n:Landroid/os/Handler;

    .line 250
    if-eqz p4, :cond_2

    invoke-virtual {p3}, Lcom/facebook/bx;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 251
    :cond_2
    invoke-static {v0}, Lcom/facebook/bx;->b(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 252
    const-string v1, "com.facebook.TokenCachingStrategy.ExpirationDate"

    invoke-static {v0, v1}, Lcom/facebook/bx;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 254
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 256
    if-eqz v1, :cond_3

    invoke-virtual {v1, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 259
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/bx;->b()V

    .line 260
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/a;->a(Ljava/util/List;)Lcom/facebook/a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 269
    :goto_0
    return-void

    .line 263
    :cond_4
    invoke-static {v0}, Lcom/facebook/a;->a(Landroid/os/Bundle;)Lcom/facebook/a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 264
    sget-object v0, Lcom/facebook/bs;->b:Lcom/facebook/bs;

    iput-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    goto :goto_0

    .line 267
    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/a;->a(Ljava/util/List;)Lcom/facebook/a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/ba;
    .locals 2

    .prologue
    .line 794
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/ba;->a(Landroid/content/Context;ZLcom/facebook/bl;)Lcom/facebook/ba;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ZLcom/facebook/bl;)Lcom/facebook/ba;
    .locals 3

    .prologue
    .line 874
    new-instance v0, Lcom/facebook/bk;

    invoke-direct {v0, p0}, Lcom/facebook/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/bk;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 875
    sget-object v1, Lcom/facebook/bs;->b:Lcom/facebook/bs;

    invoke-virtual {v0}, Lcom/facebook/ba;->c()Lcom/facebook/bs;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/bs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    .line 876
    :cond_0
    invoke-static {v0}, Lcom/facebook/ba;->a(Lcom/facebook/ba;)V

    .line 877
    invoke-virtual {v0, p2}, Lcom/facebook/ba;->a(Lcom/facebook/bl;)V

    .line 880
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/facebook/ba;Lcom/facebook/bj;)Lcom/facebook/bj;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/facebook/ba;->o:Lcom/facebook/bj;

    return-object p1
.end method

.method static synthetic a(Lcom/facebook/ba;Lcom/facebook/bo;)Lcom/facebook/bo;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/facebook/ba;->r:Lcom/facebook/bo;

    return-object p1
.end method

.method private a(ILcom/facebook/r;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1045
    .line 1047
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1048
    iget-object v0, p2, Lcom/facebook/r;->a:Lcom/facebook/s;

    sget-object v2, Lcom/facebook/s;->a:Lcom/facebook/s;

    if-ne v0, v2, :cond_0

    .line 1049
    iget-object v0, p2, Lcom/facebook/r;->b:Lcom/facebook/a;

    move-object v2, v0

    move-object v0, v1

    .line 1057
    :goto_0
    iput-object v1, p0, Lcom/facebook/ba;->k:Lcom/facebook/c;

    .line 1058
    invoke-virtual {p0, v2, v0}, Lcom/facebook/ba;->a(Lcom/facebook/a;Ljava/lang/Exception;)V

    .line 1059
    return-void

    .line 1051
    :cond_0
    new-instance v0, Lcom/facebook/w;

    iget-object v2, p2, Lcom/facebook/r;->c:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/w;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_0

    .line 1053
    :cond_1
    if-nez p1, :cond_2

    .line 1054
    new-instance v0, Lcom/facebook/aa;

    iget-object v2, p2, Lcom/facebook/r;->c:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/aa;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/facebook/ba;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/facebook/a;)V
    .locals 2

    .prologue
    .line 1173
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    invoke-virtual {p1}, Lcom/facebook/a;->f()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/bx;->a(Landroid/os/Bundle;)V

    .line 1176
    :cond_0
    return-void
.end method

.method public static final a(Lcom/facebook/ba;)V
    .locals 2

    .prologue
    .line 761
    sget-object v1, Lcom/facebook/ba;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 762
    :try_start_0
    sget-object v0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    if-eq p0, v0, :cond_2

    .line 763
    sget-object v0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    .line 765
    if-eqz v0, :cond_0

    .line 766
    invoke-virtual {v0}, Lcom/facebook/ba;->h()V

    .line 769
    :cond_0
    sput-object p0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    .line 771
    if-eqz v0, :cond_1

    .line 772
    const-string v0, "com.facebook.sdk.ACTIVE_SESSION_UNSET"

    invoke-static {v0}, Lcom/facebook/ba;->b(Ljava/lang/String;)V

    .line 775
    :cond_1
    if-eqz p0, :cond_2

    .line 776
    const-string v0, "com.facebook.sdk.ACTIVE_SESSION_SET"

    invoke-static {v0}, Lcom/facebook/ba;->b(Ljava/lang/String;)V

    .line 778
    invoke-virtual {p0}, Lcom/facebook/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 779
    const-string v0, "com.facebook.sdk.ACTIVE_SESSION_OPENED"

    invoke-static {v0}, Lcom/facebook/ba;->b(Ljava/lang/String;)V

    .line 783
    :cond_2
    monitor-exit v1

    .line 784
    return-void

    .line 783
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/facebook/ba;ILcom/facebook/r;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/facebook/ba;->a(ILcom/facebook/r;)V

    return-void
.end method

.method private a(Lcom/facebook/bg;Lcom/facebook/b/n;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1011
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/bg;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1012
    :cond_0
    sget-object v0, Lcom/facebook/b/n;->b:Lcom/facebook/b/n;

    invoke-virtual {v0, p2}, Lcom/facebook/b/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1013
    new-instance v0, Lcom/facebook/y;

    const-string v1, "Cannot request publish or manage authorization with no permissions."

    invoke-direct {v0, v1}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1017
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/bg;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1018
    invoke-static {v0}, Lcom/facebook/ba;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1019
    sget-object v2, Lcom/facebook/b/n;->a:Lcom/facebook/b/n;

    invoke-virtual {v2, p2}, Lcom/facebook/b/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1020
    new-instance v1, Lcom/facebook/y;

    const-string v2, "Cannot pass a publish or manage permission (%s) to a request for read authorization"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1026
    :cond_3
    sget-object v2, Lcom/facebook/b/n;->b:Lcom/facebook/b/n;

    invoke-virtual {v2, p2}, Lcom/facebook/b/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1027
    sget-object v2, Lcom/facebook/ba;->a:Ljava/lang/String;

    const-string v3, "Should not pass a read permission (%s) to a request for publish or manage authorization"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1034
    :cond_4
    return-void
.end method

.method private a(Lcom/facebook/bl;Lcom/facebook/b/n;)V
    .locals 5

    .prologue
    .line 925
    invoke-direct {p0, p1, p2}, Lcom/facebook/ba;->a(Lcom/facebook/bg;Lcom/facebook/b/n;)V

    .line 926
    invoke-direct {p0, p1}, Lcom/facebook/ba;->b(Lcom/facebook/bg;)V

    .line 929
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 930
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    if-eqz v0, :cond_1

    .line 931
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Session: an attempt was made to open a session that has a pending request."

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 933
    monitor-exit v1

    .line 970
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 937
    sget-object v0, Lcom/facebook/bf;->a:[I

    iget-object v3, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v3}, Lcom/facebook/bs;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 958
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Session: an attempt was made to open an already opened session."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 965
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 939
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/facebook/bs;->c:Lcom/facebook/bs;

    iput-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 940
    if-nez p1, :cond_2

    .line 941
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "openRequest cannot be null when opening a new Session"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 943
    :cond_2
    iput-object p1, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    .line 961
    :goto_1
    if-eqz p1, :cond_3

    .line 962
    invoke-virtual {p1}, Lcom/facebook/bl;->a()Lcom/facebook/bn;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bn;)V

    .line 964
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 965
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 967
    sget-object v1, Lcom/facebook/bs;->c:Lcom/facebook/bs;

    if-ne v0, v1, :cond_0

    .line 968
    invoke-virtual {p0, p1}, Lcom/facebook/ba;->a(Lcom/facebook/bg;)V

    goto :goto_0

    .line 946
    :pswitch_2
    if-eqz p1, :cond_4

    :try_start_2
    invoke-virtual {p1}, Lcom/facebook/bl;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/s;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 947
    invoke-virtual {p1}, Lcom/facebook/bl;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ba;->g()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/b/s;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 948
    iput-object p1, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    .line 951
    :cond_4
    iget-object v0, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    if-nez v0, :cond_5

    .line 952
    sget-object v0, Lcom/facebook/bs;->d:Lcom/facebook/bs;

    iput-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    goto :goto_1

    .line 954
    :cond_5
    sget-object v0, Lcom/facebook/bs;->c:Lcom/facebook/bs;

    iput-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 937
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1409
    if-nez p0, :cond_1

    .line 1410
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1412
    :goto_0
    return v0

    .line 1410
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1412
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1037
    if-eqz p0, :cond_1

    const-string v0, "publish"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "manage"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/ba;->e:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/facebook/ba;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    return-object v0
.end method

.method static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 888
    if-eqz p0, :cond_1

    sget-object v0, Lcom/facebook/ba;->d:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 889
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 890
    if-eqz v0, :cond_0

    move-object p0, v0

    :cond_0
    sput-object p0, Lcom/facebook/ba;->d:Landroid/content/Context;

    .line 892
    :cond_1
    return-void
.end method

.method private static b(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1226
    if-eqz p0, :cond_0

    .line 1227
    invoke-virtual {p0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1231
    :goto_0
    return-void

    .line 1229
    :cond_0
    invoke-static {}, Lcom/facebook/bu;->a()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/a;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 1146
    if-eqz p1, :cond_1

    .line 1147
    iput-object p1, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 1148
    invoke-direct {p0, p1}, Lcom/facebook/ba;->a(Lcom/facebook/a;)V

    .line 1150
    sget-object v1, Lcom/facebook/bs;->d:Lcom/facebook/bs;

    iput-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 1154
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    .line 1155
    iget-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {p0, v0, v1, p2}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 1156
    return-void

    .line 1151
    :cond_1
    if-eqz p2, :cond_0

    .line 1152
    sget-object v1, Lcom/facebook/bs;->f:Lcom/facebook/bs;

    iput-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    goto :goto_0
.end method

.method private b(Lcom/facebook/bg;)V
    .locals 0

    .prologue
    .line 1008
    return-void
.end method

.method static b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1220
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1222
    invoke-static {}, Lcom/facebook/ba;->k()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1223
    return-void
.end method

.method static synthetic c(Lcom/facebook/ba;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/facebook/ba;->n:Landroid/os/Handler;

    return-object v0
.end method

.method private c(Lcom/facebook/a;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 1161
    if-eqz p1, :cond_0

    .line 1162
    iput-object p1, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 1163
    invoke-direct {p0, p1}, Lcom/facebook/ba;->a(Lcom/facebook/a;)V

    .line 1165
    sget-object v1, Lcom/facebook/bs;->e:Lcom/facebook/bs;

    iput-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 1168
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/ba;->j:Lcom/facebook/bg;

    .line 1169
    iget-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {p0, v0, v1, p2}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 1170
    return-void
.end method

.method private c(Lcom/facebook/bg;)Z
    .locals 2

    .prologue
    .line 1099
    new-instance v0, Lcom/facebook/c;

    invoke-direct {v0}, Lcom/facebook/c;-><init>()V

    iput-object v0, p0, Lcom/facebook/ba;->k:Lcom/facebook/c;

    .line 1100
    iget-object v0, p0, Lcom/facebook/ba;->k:Lcom/facebook/c;

    new-instance v1, Lcom/facebook/bc;

    invoke-direct {v1, p0}, Lcom/facebook/bc;-><init>(Lcom/facebook/ba;)V

    invoke-virtual {v0, v1}, Lcom/facebook/c;->a(Lcom/facebook/q;)V

    .line 1113
    iget-object v0, p0, Lcom/facebook/ba;->k:Lcom/facebook/c;

    invoke-static {}, Lcom/facebook/ba;->k()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/c;->a(Landroid/content/Context;)V

    .line 1114
    iget-object v0, p0, Lcom/facebook/ba;->k:Lcom/facebook/c;

    invoke-virtual {p1}, Lcom/facebook/bg;->c()Lcom/facebook/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/c;->a(Lcom/facebook/j;)V

    .line 1116
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic d(Lcom/facebook/ba;)Lcom/facebook/bo;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/facebook/ba;->r:Lcom/facebook/bo;

    return-object v0
.end method

.method public static final j()Lcom/facebook/ba;
    .locals 2

    .prologue
    .line 740
    sget-object v1, Lcom/facebook/ba;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 741
    :try_start_0
    sget-object v0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    monitor-exit v1

    return-object v0

    .line 742
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static k()Landroid/content/Context;
    .locals 1

    .prologue
    .line 884
    sget-object v0, Lcom/facebook/ba;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p()Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/facebook/ba;->d:Landroid/content/Context;

    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 1472
    const/4 v0, 0x0

    .line 1473
    monitor-enter p0

    .line 1474
    :try_start_0
    iget-object v1, p0, Lcom/facebook/ba;->o:Lcom/facebook/bj;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/facebook/bu;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1476
    iget-object v1, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    .line 1479
    if-eqz v1, :cond_0

    .line 1480
    new-instance v0, Lcom/facebook/bj;

    sget-object v2, Lcom/facebook/ba;->d:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/bj;-><init>(Lcom/facebook/ba;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/ba;->o:Lcom/facebook/bj;

    .line 1483
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1485
    if-eqz v0, :cond_1

    .line 1486
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/facebook/bj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1488
    :cond_1
    return-void

    .line 1483
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 279
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->l:Landroid/os/Bundle;

    monitor-exit v1

    return-object v0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 640
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 641
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 643
    sget-object v2, Lcom/facebook/bf;->a:[I

    iget-object v3, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v3}, Lcom/facebook/bs;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 652
    sget-object v0, Lcom/facebook/ba;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshToken ignored in state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    monitor-exit v1

    .line 660
    :goto_0
    return-void

    .line 645
    :pswitch_0
    sget-object v2, Lcom/facebook/bs;->e:Lcom/facebook/bs;

    iput-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 646
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 655
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-static {v0, p1}, Lcom/facebook/a;->a(Lcom/facebook/a;Landroid/os/Bundle;)Lcom/facebook/a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 656
    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    iget-object v2, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v2}, Lcom/facebook/a;->f()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/bx;->a(Landroid/os/Bundle;)V

    .line 659
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 643
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Lcom/facebook/a;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1122
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1123
    const/4 p1, 0x0

    .line 1124
    new-instance p2, Lcom/facebook/y;

    const-string v0, "Invalid access token."

    invoke-direct {p2, v0}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    .line 1127
    :cond_0
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 1128
    :try_start_0
    sget-object v0, Lcom/facebook/bf;->a:[I

    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v2}, Lcom/facebook/bs;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1141
    :goto_0
    :pswitch_0
    monitor-exit v1

    .line 1142
    return-void

    .line 1131
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/ba;->b(Lcom/facebook/a;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1141
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1137
    :pswitch_2
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/ba;->c(Lcom/facebook/a;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1128
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/facebook/bg;)V
    .locals 5

    .prologue
    .line 895
    const/4 v0, 0x0

    .line 897
    iget-object v1, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/facebook/bg;->a(Ljava/lang/String;)V

    .line 899
    invoke-direct {p0}, Lcom/facebook/ba;->q()V

    .line 903
    invoke-static {p1}, Lcom/facebook/bg;->a(Lcom/facebook/bg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 904
    invoke-direct {p0, p1}, Lcom/facebook/ba;->c(Lcom/facebook/bg;)Z

    move-result v0

    .line 907
    :cond_0
    if-nez v0, :cond_1

    .line 908
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 909
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 911
    sget-object v2, Lcom/facebook/bf;->a:[I

    iget-object v3, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v3}, Lcom/facebook/bs;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 917
    sget-object v2, Lcom/facebook/bs;->f:Lcom/facebook/bs;

    iput-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 918
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    new-instance v3, Lcom/facebook/y;

    const-string v4, "Log in attempt failed."

    invoke-direct {v3, v4}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 920
    monitor-exit v1

    .line 922
    :cond_1
    :goto_0
    return-void

    .line 914
    :pswitch_0
    monitor-exit v1

    goto :goto_0

    .line 920
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 911
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/bl;)V
    .locals 1

    .prologue
    .line 396
    sget-object v0, Lcom/facebook/b/n;->a:Lcom/facebook/b/n;

    invoke-direct {p0, p1, v0}, Lcom/facebook/ba;->a(Lcom/facebook/bl;Lcom/facebook/b/n;)V

    .line 397
    return-void
.end method

.method public final a(Lcom/facebook/bn;)V
    .locals 2

    .prologue
    .line 614
    iget-object v1, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    monitor-enter v1

    .line 615
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 618
    :cond_0
    monitor-exit v1

    .line 619
    return-void

    .line 618
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1179
    if-ne p1, p2, :cond_1

    if-nez p3, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 1183
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/bs;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/a;->a(Ljava/util/List;)Lcom/facebook/a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    .line 1187
    :cond_2
    iget-object v1, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    monitor-enter v1

    .line 1191
    :try_start_0
    new-instance v0, Lcom/facebook/bd;

    invoke-direct {v0, p0, p2, p3}, Lcom/facebook/bd;-><init>(Lcom/facebook/ba;Lcom/facebook/bs;Ljava/lang/Exception;)V

    .line 1205
    iget-object v2, p0, Lcom/facebook/ba;->n:Landroid/os/Handler;

    invoke-static {v2, v0}, Lcom/facebook/ba;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1206
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208
    sget-object v0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    if-ne p0, v0, :cond_0

    .line 1209
    invoke-virtual {p1}, Lcom/facebook/bs;->a()Z

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/bs;->a()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1210
    invoke-virtual {p2}, Lcom/facebook/bs;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1211
    const-string v0, "com.facebook.sdk.ACTIVE_SESSION_OPENED"

    invoke-static {v0}, Lcom/facebook/ba;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1206
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1213
    :cond_3
    const-string v0, "com.facebook.sdk.ACTIVE_SESSION_CLOSED"

    invoke-static {v0}, Lcom/facebook/ba;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 1284
    iput-object p1, p0, Lcom/facebook/ba;->i:Ljava/util/Date;

    .line 1285
    return-void
.end method

.method public final b(Lcom/facebook/bl;)V
    .locals 1

    .prologue
    .line 426
    sget-object v0, Lcom/facebook/b/n;->b:Lcom/facebook/b/n;

    invoke-direct {p0, p1, v0}, Lcom/facebook/ba;->a(Lcom/facebook/bl;Lcom/facebook/b/n;)V

    .line 427
    return-void
.end method

.method public final b(Lcom/facebook/bn;)V
    .locals 2

    .prologue
    .line 627
    iget-object v1, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    monitor-enter v1

    .line 628
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 629
    monitor-exit v1

    .line 630
    return-void

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 290
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v0}, Lcom/facebook/bs;->a()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Lcom/facebook/bs;
    .locals 2

    .prologue
    .line 308
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    monitor-exit v1

    return-object v0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v0}, Lcom/facebook/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1397
    instance-of v1, p1, Lcom/facebook/ba;

    if-nez v1, :cond_1

    .line 1402
    :cond_0
    :goto_0
    return v0

    .line 1400
    :cond_1
    check-cast p1, Lcom/facebook/ba;

    .line 1402
    iget-object v1, p1, Lcom/facebook/ba;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ba;->l:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/facebook/ba;->l:Landroid/os/Bundle;

    invoke-static {v1, v2}, Lcom/facebook/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-static {v1, v2}, Lcom/facebook/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ba;->f()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/ba;->f()Ljava/util/Date;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Ljava/util/Date;
    .locals 2

    .prologue
    .line 345
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v0}, Lcom/facebook/a;->b()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 365
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v0}, Lcom/facebook/a;->c()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 575
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 578
    sget-object v2, Lcom/facebook/bf;->a:[I

    iget-object v3, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v3}, Lcom/facebook/bs;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 593
    :goto_0
    monitor-exit v1

    .line 594
    return-void

    .line 581
    :pswitch_0
    sget-object v2, Lcom/facebook/bs;->f:Lcom/facebook/bs;

    iput-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 582
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    new-instance v3, Lcom/facebook/y;

    const-string v4, "Log in attempt aborted."

    invoke-direct {v3, v4}, Lcom/facebook/y;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 593
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 589
    :pswitch_1
    :try_start_1
    sget-object v2, Lcom/facebook/bs;->g:Lcom/facebook/bs;

    iput-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    .line 590
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ba;->a(Lcom/facebook/bs;Lcom/facebook/bs;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1392
    const/4 v0, 0x0

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/facebook/ba;->q:Lcom/facebook/bx;

    invoke-virtual {v0}, Lcom/facebook/bx;->b()V

    .line 604
    :cond_0
    sget-object v0, Lcom/facebook/ba;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/b/s;->a(Landroid/content/Context;)V

    .line 605
    invoke-virtual {p0}, Lcom/facebook/ba;->h()V

    .line 606
    return-void
.end method

.method l()V
    .locals 1

    .prologue
    .line 1234
    invoke-virtual {p0}, Lcom/facebook/ba;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1235
    invoke-virtual {p0}, Lcom/facebook/ba;->m()V

    .line 1237
    :cond_0
    return-void
.end method

.method m()V
    .locals 3

    .prologue
    .line 1240
    const/4 v0, 0x0

    .line 1241
    iget-object v1, p0, Lcom/facebook/ba;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 1242
    :try_start_0
    iget-object v2, p0, Lcom/facebook/ba;->r:Lcom/facebook/bo;

    if-nez v2, :cond_0

    .line 1243
    new-instance v0, Lcom/facebook/bo;

    invoke-direct {v0, p0}, Lcom/facebook/bo;-><init>(Lcom/facebook/ba;)V

    .line 1244
    iput-object v0, p0, Lcom/facebook/ba;->r:Lcom/facebook/bo;

    .line 1246
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1248
    if-eqz v0, :cond_1

    .line 1249
    invoke-virtual {v0}, Lcom/facebook/bo;->a()V

    .line 1251
    :cond_1
    return-void

    .line 1246
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method n()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1254
    iget-object v1, p0, Lcom/facebook/ba;->r:Lcom/facebook/bo;

    if-eqz v1, :cond_1

    .line 1268
    :cond_0
    :goto_0
    return v0

    .line 1260
    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 1262
    iget-object v2, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v2}, Lcom/facebook/bs;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v2}, Lcom/facebook/a;->d()Lcom/facebook/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/ba;->i:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    invoke-virtual {v3}, Lcom/facebook/a;->e()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x5265c00

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1265
    const/4 v0, 0x1

    goto :goto_0
.end method

.method o()Lcom/facebook/a;
    .locals 1

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{Session"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ba;->g:Lcom/facebook/bs;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", token:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ba;->h:Lcom/facebook/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/ba;->f:Ljava/lang/String;

    goto :goto_1
.end method
