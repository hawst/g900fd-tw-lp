.class public Lcom/facebook/bg;
.super Ljava/lang/Object;
.source "Session.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/facebook/bm;

.field private b:Lcom/facebook/br;

.field private c:I

.field private d:Lcom/facebook/bn;

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/bq;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529
    sget-object v0, Lcom/facebook/br;->a:Lcom/facebook/br;

    iput-object v0, p0, Lcom/facebook/bg;->b:Lcom/facebook/br;

    .line 1530
    const v0, 0xface

    iput v0, p0, Lcom/facebook/bg;->c:I

    .line 1532
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/bg;->e:Z

    .line 1533
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bg;->f:Ljava/util/List;

    .line 1534
    sget-object v0, Lcom/facebook/bq;->c:Lcom/facebook/bq;

    iput-object v0, p0, Lcom/facebook/bg;->g:Lcom/facebook/bq;

    .line 1539
    new-instance v0, Lcom/facebook/bh;

    invoke-direct {v0, p0, p1}, Lcom/facebook/bh;-><init>(Lcom/facebook/bg;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/facebook/bg;->a:Lcom/facebook/bm;

    .line 1550
    return-void
.end method

.method static synthetic a(Lcom/facebook/bg;)Z
    .locals 1

    .prologue
    .line 1524
    iget-boolean v0, p0, Lcom/facebook/bg;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/facebook/bg;)Lcom/facebook/bm;
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/facebook/bg;->a:Lcom/facebook/bm;

    return-object v0
.end method


# virtual methods
.method a(I)Lcom/facebook/bg;
    .locals 0

    .prologue
    .line 1628
    if-ltz p1, :cond_0

    .line 1629
    iput p1, p0, Lcom/facebook/bg;->c:I

    .line 1631
    :cond_0
    return-object p0
.end method

.method a(Lcom/facebook/bn;)Lcom/facebook/bg;
    .locals 0

    .prologue
    .line 1608
    iput-object p1, p0, Lcom/facebook/bg;->d:Lcom/facebook/bn;

    .line 1609
    return-object p0
.end method

.method a(Lcom/facebook/br;)Lcom/facebook/bg;
    .locals 0

    .prologue
    .line 1617
    if-eqz p1, :cond_0

    .line 1618
    iput-object p1, p0, Lcom/facebook/bg;->b:Lcom/facebook/br;

    .line 1620
    :cond_0
    return-object p0
.end method

.method a(Ljava/util/List;)Lcom/facebook/bg;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/bg;"
        }
    .end annotation

    .prologue
    .line 1639
    if-eqz p1, :cond_0

    .line 1640
    iput-object p1, p0, Lcom/facebook/bg;->f:Ljava/util/List;

    .line 1642
    :cond_0
    return-object p0
.end method

.method a()Lcom/facebook/bn;
    .locals 1

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/facebook/bg;->d:Lcom/facebook/bn;

    return-object v0
.end method

.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1669
    iput-object p1, p0, Lcom/facebook/bg;->h:Ljava/lang/String;

    .line 1670
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1600
    iput-boolean p1, p0, Lcom/facebook/bg;->e:Z

    .line 1601
    return-void
.end method

.method b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/facebook/bg;->f:Ljava/util/List;

    return-object v0
.end method

.method c()Lcom/facebook/j;
    .locals 9

    .prologue
    .line 1681
    new-instance v8, Lcom/facebook/bi;

    invoke-direct {v8, p0}, Lcom/facebook/bi;-><init>(Lcom/facebook/bg;)V

    .line 1692
    new-instance v0, Lcom/facebook/j;

    iget-object v1, p0, Lcom/facebook/bg;->b:Lcom/facebook/br;

    iget v2, p0, Lcom/facebook/bg;->c:I

    iget-boolean v3, p0, Lcom/facebook/bg;->e:Z

    iget-object v4, p0, Lcom/facebook/bg;->f:Ljava/util/List;

    iget-object v5, p0, Lcom/facebook/bg;->g:Lcom/facebook/bq;

    iget-object v6, p0, Lcom/facebook/bg;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/bg;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/j;-><init>(Lcom/facebook/br;IZLjava/util/List;Lcom/facebook/bq;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/t;)V

    return-object v0
.end method
