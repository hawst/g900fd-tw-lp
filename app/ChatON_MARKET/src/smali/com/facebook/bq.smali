.class public final enum Lcom/facebook/bq;
.super Ljava/lang/Enum;
.source "SessionDefaultAudience.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/bq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/bq;

.field public static final enum b:Lcom/facebook/bq;

.field public static final enum c:Lcom/facebook/bq;

.field public static final enum d:Lcom/facebook/bq;

.field private static final synthetic f:[Lcom/facebook/bq;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/facebook/bq;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/bq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/bq;->a:Lcom/facebook/bq;

    .line 34
    new-instance v0, Lcom/facebook/bq;

    const-string v1, "ONLY_ME"

    const-string v2, "SELF"

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/bq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/bq;->b:Lcom/facebook/bq;

    .line 39
    new-instance v0, Lcom/facebook/bq;

    const-string v1, "FRIENDS"

    const-string v2, "ALL_FRIENDS"

    invoke-direct {v0, v1, v5, v2}, Lcom/facebook/bq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/bq;->c:Lcom/facebook/bq;

    .line 44
    new-instance v0, Lcom/facebook/bq;

    const-string v1, "EVERYONE"

    const-string v2, "EVERYONE"

    invoke-direct {v0, v1, v6, v2}, Lcom/facebook/bq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/bq;->d:Lcom/facebook/bq;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/bq;

    sget-object v1, Lcom/facebook/bq;->a:Lcom/facebook/bq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/bq;->b:Lcom/facebook/bq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/bq;->c:Lcom/facebook/bq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/bq;->d:Lcom/facebook/bq;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/bq;->f:[Lcom/facebook/bq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput-object p3, p0, Lcom/facebook/bq;->e:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/bq;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/facebook/bq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/bq;

    return-object v0
.end method

.method public static values()[Lcom/facebook/bq;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/facebook/bq;->f:[Lcom/facebook/bq;

    invoke-virtual {v0}, [Lcom/facebook/bq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/bq;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/facebook/bq;->e:Ljava/lang/String;

    return-object v0
.end method
