.class public Lcom/coolots/sso/a/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Z


# instance fields
.field private b:Lcom/coolots/sso/a/f;

.field private c:Lcom/coolots/sso/a/c;

.field private d:Lcom/coolots/sso/a/e;

.field private e:Lcom/coolots/sso/a/b;

.field private f:Lcom/coolots/sso/a/g;

.field private g:Lcom/coolots/sso/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/coolots/sso/a/a;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    iput-object v0, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    iput-object v0, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    iput-object v0, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    iput-object v0, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    iput-object v0, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChatONVAPII create() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "STATE_INIT"

    goto :goto_0

    :sswitch_1
    const-string v0, "STATE_CALLING"

    goto :goto_0

    :sswitch_2
    const-string v0, "STATE_CONNECTED"

    goto :goto_0

    :sswitch_3
    const-string v0, "STATE_DISCONNECTED"

    goto :goto_0

    :sswitch_4
    const-string v0, "STATE_CHANGE_TO_CONFERENCE_CONNECTED"

    goto :goto_0

    :sswitch_5
    const-string v0, "STATE_CHANGE_TO_P2P_CONNECTED"

    goto :goto_0

    :sswitch_6
    const-string v0, "STATE_CHANGE_TO_CONFERENCE_DISCONNECTED"

    goto :goto_0

    :sswitch_7
    const-string v0, "STATE_CHANGE_TO_P2P_DISCONNECTED"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x64 -> :sswitch_4
        0x65 -> :sswitch_5
        0x66 -> :sswitch_6
        0x67 -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 5

    sput-boolean p1, Lcom/coolots/sso/a/a;->a:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setDisplayLog: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChatON V is not installed"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "content://com.coolots.chaton/set_log_display"

    invoke-static {p0, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "content://com.coolots.chaton/set_log_display"

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-eqz p1, :cond_3

    const-string v0, "1"

    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v0, "0"

    goto :goto_1
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lcom/coolots/sso/a/a;->a:Z

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x80

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "isInstalled() true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "isInstalled() false"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4

    const-string v0, "checkKey()"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const-string v0, "content://com.coolots.chaton/key"

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "key"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "function"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://com.coolots.chaton/not_supported_function"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.coolots.chaton/no_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/f;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    const-string v0, ""

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "RET_CHATONV_SUCCESS"

    goto :goto_0

    :pswitch_1
    const-string v0, "RET_CHATONV_FAIL"

    goto :goto_0

    :pswitch_2
    const-string v0, "RET_CHATONV_NOT_INSTALLED"

    goto :goto_0

    :pswitch_3
    const-string v0, "RET_CHATONV_ID_EMPTY"

    goto :goto_0

    :pswitch_4
    const-string v0, "RET_CHATONV_UNEXPECTED_ERROR"

    goto :goto_0

    :pswitch_5
    const-string v0, "RET_CHATONV_NOT_ALLOWED_ACCESS"

    goto :goto_0

    :pswitch_6
    const-string v0, "RET_CHATONV_NATIONAL_CODE_EMPTY"

    goto :goto_0

    :pswitch_7
    const-string v0, "RET_CHATONV_GUID_EMPTY"

    goto :goto_0

    :pswitch_8
    const-string v0, "RET_CHATONV_CALLER_NAME_EMPTY"

    goto :goto_0

    :pswitch_9
    const-string v0, "RET_CHATONV_CHATON_NUMBER_EMPTY"

    goto :goto_0

    :pswitch_a
    const-string v0, "RET_CHATONV_CHATON_UID_EMPTY"

    goto :goto_0

    :pswitch_b
    const-string v0, "RET_CHATONV_IMEI_EMPTY"

    goto :goto_0

    :pswitch_c
    const-string v0, "RET_CHATONV_BIRTHDAY_EMPTY"

    goto :goto_0

    :pswitch_d
    const-string v0, "RET_CHATONV_NOT_SUPPORTED_FUNCTION"

    goto :goto_0

    :pswitch_e
    const-string v0, "RET_CHATONV_NO_ACCOUNT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private static b(Ljava/lang/String;)V
    .locals 1

    sget-boolean v0, Lcom/coolots/sso/a/a;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "ChatONVAPII(1.0.0.3)"

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x1

    const-string v1, "com.coolots.chaton"

    invoke-static {p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "com.coolots.chatonforcanada"

    invoke-static {p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 1

    sget-boolean v0, Lcom/coolots/sso/a/a;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "ChatONVAPII(1.0.0.3)"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/e;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    return-object v0
.end method

.method static synthetic e(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/d;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    return-object v0
.end method

.method static synthetic f(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/g;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    return-object v0
.end method

.method private o(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearAccountListener: mListener :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    :cond_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearAccountListener: mReceiver :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "IllegalArgumentException \n e"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    throw v0
.end method

.method private p(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearCallListener: mCallListener :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    :cond_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearCallListener: mCallReceiver :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "IllegalArgumentException \n e"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    throw v0
.end method

.method private q(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearUpdateListener: mUpdateListener :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    :cond_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearUpdateListener: mUpdateReceiver :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "IllegalArgumentException \n e"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    const-string v0, "createAccount() call start"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChatON V is not installed"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const-string v0, "content://com.coolots.chaton/create_account"

    invoke-static {p1, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xd

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    const-string v0, "no account"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const-string v0, "samsungAccount is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x3

    goto :goto_0

    :cond_5
    if-eqz p4, :cond_6

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    const-string v0, "nationalCode is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x6

    goto :goto_0

    :cond_7
    if-eqz p5, :cond_8

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const-string v0, "GUID is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x7

    goto :goto_0

    :cond_9
    if-eqz p6, :cond_a

    invoke-virtual {p6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    const-string v0, "callerName is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/16 v0, 0x8

    goto :goto_0

    :cond_b
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_c
    const-string v0, "chatonID is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/16 v0, 0x9

    goto :goto_0

    :cond_d
    if-eqz p7, :cond_e

    invoke-virtual {p7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_e
    const-string v0, "IMEI is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/16 v0, 0xb

    goto :goto_0

    :cond_f
    if-eqz p8, :cond_10

    invoke-virtual {p8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    const-string v0, "chatONUID is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/16 v0, 0xa

    goto/16 :goto_0

    :cond_11
    if-eqz p9, :cond_12

    invoke-virtual {p9}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    const-string v0, "birthday is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    :cond_13
    const-string v1, "content://com.coolots.chaton/create_account"

    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x1

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x2

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x3

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x4

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x5

    invoke-static {p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x6

    invoke-static {p8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x7

    invoke-static {p9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v1, 0x2

    const-string v0, "outgoingCall() call start"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChatON V is not installed"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "content://com.coolots.chaton/outgoingcall"

    invoke-static {p1, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v7, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xd

    goto :goto_0

    :cond_2
    if-ne v0, v3, :cond_3

    const-string v0, "no account"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xe

    goto :goto_0

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const-string v0, "chatonID is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    move v0, v3

    goto :goto_0

    :cond_5
    const-string v5, "content://com.coolots.chaton/outgoingcall"

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    if-eqz p2, :cond_7

    const-string v0, "1"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    move v0, v6

    goto :goto_0

    :cond_7
    const-string v0, "0"

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v1, 0x2

    const/4 v6, 0x0

    const-string v0, "outgoingConferenceCall() call start"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChatON V is not installed"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "content://com.coolots.chaton/outgoingconferencecall"

    invoke-static {p1, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v7, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xd

    goto :goto_0

    :cond_2
    if-ne v0, v3, :cond_3

    const-string v0, "no account"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xe

    goto :goto_0

    :cond_3
    if-eqz p3, :cond_4

    array-length v0, p3

    if-nez v0, :cond_5

    :cond_4
    const-string v0, "samsungAccount is empty"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    move v0, v3

    goto :goto_0

    :cond_5
    const-string v5, "content://com.coolots.chaton/outgoingconferencecall"

    array-length v0, p3

    add-int/lit8 v0, v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    if-eqz p2, :cond_7

    const-string v0, "1"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x4

    array-length v1, p3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    array-length v1, p3

    invoke-static {p3, v6, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    move v0, v6

    goto :goto_0

    :cond_7
    const-string v0, "0"

    goto :goto_1
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "dispose()"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->o(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->p(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->q(Landroid/content/Context;)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/coolots/sso/a/b;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCallListener: clear "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->p(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->p(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    new-instance v0, Lcom/coolots/sso/a/e;

    invoke-direct {v0, p0, v3}, Lcom/coolots/sso/a/e;-><init>(Lcom/coolots/sso/a/a;Lcom/coolots/sso/a/e;)V

    iput-object v0, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCallListener: set "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCallListener: mCallListener "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->e:Lcom/coolots/sso/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCallListener: mCallReceiver "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.coolots.chaton.version.CONFERENCE_MEMBER_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.coolots.chaton.account.CALL_STATE_CHANGE_INFO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->d:Lcom/coolots/sso/a/e;

    const-string v2, "com.coolots.permission.COOLOTS"

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setListener: clear "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->o(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->o(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    new-instance v0, Lcom/coolots/sso/a/f;

    invoke-direct {v0, p0, v3}, Lcom/coolots/sso/a/f;-><init>(Lcom/coolots/sso/a/a;Lcom/coolots/sso/a/f;)V

    iput-object v0, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setListener: set :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setListener: mListener :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->c:Lcom/coolots/sso/a/c;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setListener: mReceiver :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.coolots.chaton.account.ACCOUNT_RESULTINFO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.coolots.chaton.account.REMOVE_ACCOUNT_RESULTINFO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->b:Lcom/coolots/sso/a/f;

    const-string v2, "com.coolots.permission.COOLOTS"

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setUpdateListener: clear "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->q(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/coolots/sso/a/a;->q(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    new-instance v0, Lcom/coolots/sso/a/g;

    invoke-direct {v0, p0, v3}, Lcom/coolots/sso/a/g;-><init>(Lcom/coolots/sso/a/a;Lcom/coolots/sso/a/g;)V

    iput-object v0, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setUpdateListener: set "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setUpdateListener: mUpdateListener "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->g:Lcom/coolots/sso/a/d;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setUpdateListener: mUpdateReceiver "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.coolots.chaton.version.UPGRADE_NEW_APK_INFO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/a;->f:Lcom/coolots/sso/a/g;

    const-string v2, "com.coolots.permission.COOLOTS"

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Z)I
    .locals 5

    const/4 v0, 0x0

    const-string v1, "getMaxConferenceMemberCnt()"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "content://com.coolots.chaton/maxconferencecallnum"

    invoke-static {p1, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "content://com.coolots.chaton/maxconferencecallnum"

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-eqz p2, :cond_4

    const-string v0, "1"

    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_4
    const-string v0, "0"

    goto :goto_1
.end method

.method public c(Landroid/content/Context;)I
    .locals 6

    const/4 v0, 0x2

    const/4 v2, 0x0

    const-string v1, "removeAccountInDevice() call start"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "content://com.coolots.chaton/remove_account"

    invoke-static {p1, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    if-ne v1, v0, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xd

    goto :goto_0

    :cond_2
    const-string v1, "content://com.coolots.chaton/remove_account"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "isReadyToCall() call start"

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v2, "content://com.coolots.chaton/account"

    invoke-static {p1, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_1

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    const-string v0, "getIntentSettingActivity()"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.coolots.chaton.USER_SETTING_SUBPAGE_CALL_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "getIntentSettingActivity() return null"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    const-string v0, "getIntentDataUsageActivity()"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.coolots.chaton.USER_SETTING_SUBPAGE_DATA_USGAE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "getIntentDataUsageActivity() return null"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Landroid/content/Context;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "showCallActivity() call start"

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "content://com.coolots.chaton/remotecallactivity"

    invoke-static {p1, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_2

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "content://com.coolots.chaton/remotecallactivity"

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v5, "0"

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public h(Landroid/content/Context;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "endCallActivity() call start"

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "content://com.coolots.chaton/remotecallactivity"

    invoke-static {p1, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_2

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "content://com.coolots.chaton/remotecallactivity"

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v5, "1"

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public i(Landroid/content/Context;)I
    .locals 3

    const/4 v0, -0x1

    const-string v1, "getCallDuration() call start"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "content://com.coolots.chaton/callduration"

    invoke-static {p1, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v1, "content://com.coolots.chaton/callduration"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public j(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "isCalling() call start"

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "content://com.coolots.chaton/iscalling"

    invoke-static {p1, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_2

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "content://com.coolots.chaton/iscalling"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public k(Landroid/content/Context;)I
    .locals 4

    const/4 v1, 0x3

    const/4 v0, 0x0

    const-string v2, "getCallStateDuringCall() call start"

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v2, "content://com.coolots.chaton/callstate"

    invoke-static {p1, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-ne v2, v1, :cond_3

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "content://com.coolots.chaton/callstate"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public l(Landroid/content/Context;)I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    const-string v1, "getCallType() call start"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "content://com.coolots.chaton/get_call_type"

    invoke-static {p1, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v1, "not allowed access"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const-string v1, "not supported function"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    const-string v1, "no account"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "content://com.coolots.chaton/get_call_type"

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0, v1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public m(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 5

    const/4 v1, 0x0

    const-string v0, "getCallMember() call start"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChatON V is not installed"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "content://com.coolots.chaton/get_call_member"

    invoke-static {p1, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    const-string v0, "no account"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v0, "content://com.coolots.chaton/get_call_member"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-lt v0, v4, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "call member: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    move-object v0, v1

    goto :goto_0

    :cond_6
    aget-object v4, v3, v0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move-object v0, v2

    goto :goto_0
.end method

.method public n(Landroid/content/Context;)I
    .locals 3

    const/4 v0, 0x2

    const-string v1, "getLatestApkVersion() call start"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ChatON V is not installed"

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "content://com.coolots.chaton/get_latest_version"

    invoke-static {p1, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v0, "not allowed access"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    if-ne v1, v0, :cond_2

    const-string v0, "not supported function"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xd

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne v1, v0, :cond_3

    const-string v0, "no account"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Ljava/lang/String;)V

    const/16 v0, 0xe

    goto :goto_0

    :cond_3
    const-string v0, "content://com.coolots.chaton/get_latest_version"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    const/4 v0, 0x0

    goto :goto_0
.end method
