.class Lcom/coolots/sso/a/e;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/coolots/sso/a/a;


# direct methods
.method private constructor <init>(Lcom/coolots/sso/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/coolots/sso/a/a;Lcom/coolots/sso/a/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/e;-><init>(Lcom/coolots/sso/a/a;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIICallReceiver() action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIICallReceiver() this: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIICallReceiver() mCallListener: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v2}, Lcom/coolots/sso/a/a;->c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIICallReceiver() mCallReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v2}, Lcom/coolots/sso/a/a;->d(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/e;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    const-string v1, "com.coolots.chaton.account.CALL_STATE_CHANGE_INFO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "result"

    const/4 v1, 0x3

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BR_CALL_STATE_CHANGE_INFO state("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v1}, Lcom/coolots/sso/a/a;->c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/coolots/sso/a/b;->a(I)V

    goto/16 :goto_0

    :cond_2
    const-string v1, "com.coolots.chaton.version.CONFERENCE_MEMBER_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BR_CONFERENCE_MEMBER_CHANGED"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/coolots/sso/a/e;->a:Lcom/coolots/sso/a/a;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->c(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/coolots/sso/a/b;->a()V

    goto/16 :goto_0
.end method
