.class Lcom/coolots/sso/a/f;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/coolots/sso/a/a;


# direct methods
.method private constructor <init>(Lcom/coolots/sso/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/coolots/sso/a/a;Lcom/coolots/sso/a/f;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/coolots/sso/a/f;-><init>(Lcom/coolots/sso/a/a;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIIReceiver() action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIIReceiver() this: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIIReceiver() mListener: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v2}, Lcom/coolots/sso/a/a;->a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatONVAPIIReceiver() mReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v2}, Lcom/coolots/sso/a/a;->b(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/f;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    const-string v1, "com.coolots.chaton.account.ACCOUNT_RESULTINFO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BR_ACCOUNT_RESULT_INFO"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "result"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "account_fail_string"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", result string: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v2}, Lcom/coolots/sso/a/a;->a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/coolots/sso/a/c;->onReceiveCreateAccount(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string v1, "com.coolots.chaton.account.REMOVE_ACCOUNT_RESULTINFO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BR_REMOVE_ACCOUNT_RESULT_INFO"

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/coolots/sso/a/f;->a:Lcom/coolots/sso/a/a;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->a(Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/c;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/coolots/sso/a/c;->onReceiveRemoveAccount(Z)V

    goto/16 :goto_0
.end method
