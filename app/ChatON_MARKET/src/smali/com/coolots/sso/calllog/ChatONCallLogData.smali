.class public Lcom/coolots/sso/calllog/ChatONCallLogData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/coolots/sso/calllog/a;

    invoke-direct {v0}, Lcom/coolots/sso/calllog/a;-><init>()V

    sput-object v0, Lcom/coolots/sso/calllog/ChatONCallLogData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->b:I

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->c:I

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->b:I

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->c:I

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    iput v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->b:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->d:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    iput-object v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    iput-object v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    iput-object v1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    :goto_3
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :goto_2
    iget v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :goto_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/coolots/sso/calllog/ChatONCallLogData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
