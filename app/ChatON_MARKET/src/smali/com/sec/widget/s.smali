.class Lcom/sec/widget/s;
.super Ljava/lang/Object;
.source "DropPanelMenu.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/DropPanelMenu;


# direct methods
.method constructor <init>(Lcom/sec/widget/DropPanelMenu;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/widget/s;->a:Lcom/sec/widget/DropPanelMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 511
    move-object v0, p1

    check-cast v0, Landroid/widget/LinearLayout;

    .line 513
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 558
    :cond_0
    return v5

    .line 515
    :pswitch_0
    invoke-virtual {p1, v5}, Landroid/view/View;->setPressed(Z)V

    move v1, v2

    .line 516
    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 517
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setPressed(Z)V

    .line 516
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 521
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    move v3, v2

    .line 522
    :goto_1
    iget-object v1, p0, Lcom/sec/widget/s;->a:Lcom/sec/widget/DropPanelMenu;

    invoke-static {v1}, Lcom/sec/widget/DropPanelMenu;->e(Lcom/sec/widget/DropPanelMenu;)Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 525
    iget-object v1, p0, Lcom/sec/widget/s;->a:Lcom/sec/widget/DropPanelMenu;

    invoke-static {v1}, Lcom/sec/widget/DropPanelMenu;->e(Lcom/sec/widget/DropPanelMenu;)Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    check-cast v1, Lcom/sec/widget/v;

    .line 527
    invoke-virtual {v1}, Lcom/sec/widget/v;->b()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 528
    iget-object v4, p0, Lcom/sec/widget/s;->a:Lcom/sec/widget/DropPanelMenu;

    invoke-static {v4}, Lcom/sec/widget/DropPanelMenu;->b(Lcom/sec/widget/DropPanelMenu;)Lcom/sec/widget/t;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/sec/widget/t;->a(Landroid/view/MenuItem;)Z

    .line 522
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 532
    :cond_2
    iget-object v1, p0, Lcom/sec/widget/s;->a:Lcom/sec/widget/DropPanelMenu;

    invoke-static {v1}, Lcom/sec/widget/DropPanelMenu;->d(Lcom/sec/widget/DropPanelMenu;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 533
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    move v1, v2

    .line 534
    :goto_2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 535
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setPressed(Z)V

    .line 534
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 540
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 541
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 542
    if-ltz v1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    if-lt v4, v1, :cond_3

    if-ltz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ge v1, v3, :cond_0

    .line 543
    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    move v1, v2

    .line 544
    :goto_3
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 545
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setPressed(Z)V

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 550
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 551
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    move v1, v2

    .line 552
    :goto_4
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 553
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setPressed(Z)V

    .line 552
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
