.class Lcom/sec/widget/bh;
.super Ljava/lang/Object;
.source "TwIndexScrollView.java"


# instance fields
.field final synthetic a:Lcom/sec/widget/TwIndexScrollView;

.field private b:I

.field private c:F

.field private d:Landroid/graphics/Rect;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/az;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:F


# direct methods
.method public constructor <init>(Lcom/sec/widget/TwIndexScrollView;Ljava/lang/String;Ljava/util/ArrayList;FILandroid/graphics/Rect;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/az;",
            ">;FI",
            "Landroid/graphics/Rect;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1129
    iput-object p1, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    .line 1130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/widget/bh;->c:F

    .line 1087
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/widget/bh;->h:I

    .line 1092
    iput v2, p0, Lcom/sec/widget/bh;->i:I

    .line 1094
    iput v3, p0, Lcom/sec/widget/bh;->k:I

    .line 1131
    iput p7, p0, Lcom/sec/widget/bh;->b:I

    .line 1132
    iput v2, p0, Lcom/sec/widget/bh;->h:I

    .line 1136
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    if-ge p5, v3, :cond_1

    .line 1139
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NDepthScroll( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1141
    :cond_1
    if-nez p2, :cond_2

    const-string p2, ""

    :cond_2
    iput-object p2, p0, Lcom/sec/widget/bh;->e:Ljava/lang/String;

    .line 1142
    iput-object p3, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    .line 1143
    iput p5, p0, Lcom/sec/widget/bh;->g:I

    .line 1144
    cmpg-float v1, p4, v0

    if-gez v1, :cond_3

    move p4, v0

    .line 1147
    :cond_3
    iput p4, p0, Lcom/sec/widget/bh;->l:F

    .line 1148
    iput p4, p0, Lcom/sec/widget/bh;->m:F

    .line 1149
    invoke-virtual {p1}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1151
    iget v1, p0, Lcom/sec/widget/bh;->m:F

    const v2, 0x7f090285

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/widget/bh;->m:F

    .line 1152
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/widget/bh;->j:I

    .line 1154
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    .line 1155
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v1, p6, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1156
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v1, p6, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1157
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v1, p6, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1158
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v1, p6, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1161
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/bh;)F
    .locals 1

    .prologue
    .line 1060
    iget v0, p0, Lcom/sec/widget/bh;->l:F

    return v0
.end method

.method static synthetic a(Lcom/sec/widget/bh;F)Z
    .locals 1

    .prologue
    .line 1060
    invoke-direct {p0, p1}, Lcom/sec/widget/bh;->b(F)Z

    move-result v0

    return v0
.end method

.method private b(F)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1337
    iget v0, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float v4, p1, v0

    .line 1338
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 1429
    :cond_0
    :goto_0
    return v2

    .line 1342
    :cond_1
    iget v5, p0, Lcom/sec/widget/bh;->h:I

    .line 1358
    const/4 v0, 0x0

    .line 1359
    iget-object v1, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    if-eqz v1, :cond_2

    .line 1360
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 1362
    :cond_2
    iget v3, p0, Lcom/sec/widget/bh;->j:I

    move v1, v2

    .line 1364
    :goto_1
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v6, v6, 0x6

    int-to-float v6, v6

    add-float/2addr v6, v0

    cmpg-float v6, v6, v4

    if-gez v6, :cond_4

    .line 1365
    rem-int v6, v1, v3

    add-int/lit8 v7, v3, -0x2

    if-ne v6, v7, :cond_3

    .line 1367
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    add-float/2addr v0, v6

    .line 1371
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1369
    :cond_3
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    goto :goto_2

    .line 1375
    :cond_4
    div-int v6, v1, v3

    .line 1376
    rem-int v7, v1, v3

    .line 1377
    iget v8, p0, Lcom/sec/widget/bh;->k:I

    add-int/2addr v3, v8

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v6

    add-int/2addr v3, v7

    .line 1383
    iget-object v6, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v6}, Lcom/sec/widget/TwIndexScrollView;->h(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v6

    if-nez v6, :cond_5

    iget v6, p0, Lcom/sec/widget/bh;->j:I

    rem-int v6, v1, v6

    iget v7, p0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_5

    .line 1385
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v6, v6, 0x6

    int-to-float v6, v6

    add-float/2addr v6, v0

    sub-float v4, v6, v4

    .line 1386
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    sub-float v4, v6, v4

    .line 1387
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/widget/bh;->k:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    .line 1388
    div-float/2addr v4, v6

    float-to-int v4, v4

    .line 1390
    add-int/2addr v3, v4

    .line 1395
    :cond_5
    iget-object v4, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_7

    .line 1396
    iget-object v3, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1409
    :cond_6
    :goto_3
    iput v0, p0, Lcom/sec/widget/bh;->l:F

    .line 1417
    iget-object v0, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->h(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/sec/widget/bh;->j:I

    rem-int v0, v1, v0

    iget v1, p0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_9

    .line 1418
    iget v0, p0, Lcom/sec/widget/bh;->k:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/bh;->h:I

    .line 1420
    iget v0, p0, Lcom/sec/widget/bh;->h:I

    iget-object v1, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 1421
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/widget/bh;->h:I

    goto/16 :goto_0

    .line 1397
    :cond_7
    if-gez v3, :cond_6

    move v3, v2

    .line 1398
    goto :goto_3

    .line 1422
    :cond_8
    iget v0, p0, Lcom/sec/widget/bh;->h:I

    if-gez v0, :cond_0

    .line 1423
    iput v2, p0, Lcom/sec/widget/bh;->h:I

    goto/16 :goto_0

    .line 1427
    :cond_9
    iput v3, p0, Lcom/sec/widget/bh;->h:I

    .line 1429
    if-eq v3, v5, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private h()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    .line 1165
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1166
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1167
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1168
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 1169
    iget v0, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v0

    .line 1170
    iget-object v0, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->g(Lcom/sec/widget/TwIndexScrollView;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v7

    .line 1172
    iget v0, p0, Lcom/sec/widget/bh;->i:I

    if-nez v0, :cond_0

    .line 1228
    :goto_0
    return-void

    .line 1175
    :cond_0
    const/4 v0, 0x1

    move v3, v0

    :goto_1
    if-gt v3, v5, :cond_5

    .line 1176
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1177
    add-int/lit8 v1, v5, 0x1

    .line 1178
    :goto_2
    if-le v1, v11, :cond_4

    .line 1185
    add-int v0, v1, v3

    add-int/lit8 v0, v0, -0x1

    div-int v0, v8, v0

    .line 1186
    add-int/lit8 v2, v1, -0x1

    mul-int/2addr v2, v0

    .line 1188
    add-int v0, v1, v3

    add-int/lit8 v0, v0, -0x1

    rem-int v0, v8, v0

    .line 1189
    if-eqz v0, :cond_6

    .line 1190
    add-int/lit8 v9, v1, -0x1

    if-ge v9, v0, :cond_1

    move v0, v1

    :cond_1
    add-int/2addr v0, v2

    .line 1193
    :goto_3
    add-int/lit8 v2, v1, -0x1

    add-int/2addr v2, v3

    div-int v2, v8, v2

    int-to-float v2, v2

    .line 1207
    int-to-float v0, v0

    mul-float/2addr v0, v6

    iget v9, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v9, v2

    mul-float/2addr v9, v6

    add-float/2addr v0, v9

    float-to-int v0, v0

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1210
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v9, v1, -0x1

    add-int/2addr v9, v3

    rem-int/2addr v0, v9

    add-int/lit8 v9, v1, -0x1

    if-lt v0, v9, :cond_2

    .line 1211
    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    add-float/2addr v0, v6

    float-to-int v0, v0

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1215
    :cond_2
    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/sec/widget/bh;->i:I

    iget v10, v7, Landroid/graphics/Paint$FontMetrics;->ascent:F

    float-to-int v10, v10

    add-int/2addr v9, v10

    if-ge v0, v9, :cond_3

    .line 1216
    iput v3, p0, Lcom/sec/widget/bh;->k:I

    .line 1217
    iput v1, p0, Lcom/sec/widget/bh;->j:I

    .line 1218
    iget-object v0, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    float-to-int v1, v2

    invoke-static {v0, v1}, Lcom/sec/widget/TwIndexScrollView;->a(Lcom/sec/widget/TwIndexScrollView;I)I

    goto :goto_0

    .line 1221
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 1222
    goto :goto_2

    .line 1175
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1224
    :cond_5
    iput v5, p0, Lcom/sec/widget/bh;->k:I

    .line 1225
    iput v11, p0, Lcom/sec/widget/bh;->j:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1101
    iget v0, p0, Lcom/sec/widget/bh;->j:I

    return v0
.end method

.method public a(I)Lcom/sec/widget/az;
    .locals 1

    .prologue
    .line 1278
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/az;

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Paint;III)V
    .locals 20

    .prologue
    .line 1557
    .line 1558
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->i:I

    if-nez v2, :cond_c

    .line 1559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    iget v3, v2, Lcom/sec/widget/TwIndexScrollView;->a:I

    .line 1562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual {v2}, Lcom/sec/widget/TwIndexScrollView;->getHeight()I

    move-result v9

    .line 1563
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual {v2}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->i(Lcom/sec/widget/TwIndexScrollView;)I

    move-result v2

    .line 1569
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/widget/bh;->g:I

    .line 1573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->b:I

    invoke-static {v2, v4}, Lcom/sec/widget/TwIndexScrollView;->b(Lcom/sec/widget/TwIndexScrollView;I)Landroid/graphics/Rect;

    move-result-object v10

    .line 1575
    const/4 v4, 0x0

    .line 1576
    if-eqz v10, :cond_1f

    .line 1577
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->m:F

    float-to-int v2, v2

    if-le v2, v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->m:F

    float-to-int v2, v2

    :goto_0
    iput v2, v10, Landroid/graphics/Rect;->top:I

    .line 1583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    .line 1584
    iget v2, v10, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090281

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 1590
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1592
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v8, v4, 0x1

    .line 1596
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/widget/bh;->g:I

    .line 1598
    const/4 v5, 0x0

    const/4 v4, 0x0

    move/from16 v19, v4

    move v4, v2

    move/from16 v2, v19

    :goto_2
    if-ge v2, v7, :cond_3

    .line 1599
    rem-int v12, v5, v8

    add-int/lit8 v13, v8, -0x1

    if-ne v12, v13, :cond_2

    add-int/lit8 v12, v7, -0x1

    if-ge v2, v12, :cond_2

    .line 1600
    int-to-float v4, v4

    int-to-float v12, v11

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v12, v13

    add-float/2addr v4, v12

    float-to-int v4, v4

    .line 1598
    :goto_3
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v2, v3

    .line 1577
    goto :goto_0

    .line 1585
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v5, 0x1

    if-le v2, v5, :cond_1f

    .line 1586
    iget v2, v10, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090285

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    goto :goto_1

    .line 1605
    :cond_2
    add-int/2addr v4, v11

    goto :goto_3

    .line 1609
    :cond_3
    if-eqz v10, :cond_4

    .line 1610
    iput v4, v10, Landroid/graphics/Rect;->bottom:I

    .line 1617
    iget v2, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v2, v5

    if-le v2, v9, :cond_4

    .line 1618
    iget v2, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v2, v5

    sub-int/2addr v2, v9

    .line 1619
    iput v9, v10, Landroid/graphics/Rect;->bottom:I

    .line 1621
    iget v5, v10, Landroid/graphics/Rect;->top:I

    sub-int v2, v5, v2

    iput v2, v10, Landroid/graphics/Rect;->top:I

    .line 1622
    iget v2, v10, Landroid/graphics/Rect;->top:I

    if-ge v2, v3, :cond_4

    .line 1623
    iput v3, v10, Landroid/graphics/Rect;->top:I

    .line 1633
    :cond_4
    const/4 v2, 0x0

    .line 1634
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v5}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v5}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v7, 0x1

    if-ne v5, v7, :cond_6

    .line 1635
    const v2, 0x7f090281

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    move v8, v2

    .line 1640
    :goto_4
    if-eqz v10, :cond_1d

    .line 1641
    iget v2, v10, Landroid/graphics/Rect;->top:I

    .line 1644
    :goto_5
    sub-int v3, v9, v3

    mul-int/lit8 v4, v8, 0x2

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/widget/bh;->i:I

    .line 1646
    invoke-direct/range {p0 .. p0}, Lcom/sec/widget/bh;->h()V

    .line 1653
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1654
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/widget/bh;->j:I

    .line 1655
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/widget/bh;->k:I

    .line 1656
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/widget/bh;->g:I

    .line 1658
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->k(Lcom/sec/widget/TwIndexScrollView;)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_5

    .line 1660
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/widget/bh;->i:I

    div-int/2addr v3, v11

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/widget/bh;->g:I

    .line 1661
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/widget/bh;->g:I

    .line 1664
    :cond_5
    const/4 v5, 0x0

    const/4 v4, 0x0

    move v7, v5

    move/from16 v19, v4

    move v4, v2

    move/from16 v2, v19

    :goto_6
    if-ge v2, v11, :cond_9

    .line 1666
    rem-int v5, v7, v12

    add-int/lit8 v6, v12, -0x1

    if-ne v5, v6, :cond_7

    add-int/lit8 v5, v11, -0x1

    if-ge v2, v5, :cond_7

    .line 1667
    int-to-float v4, v4

    int-to-float v5, v3

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 1668
    const/4 v5, 0x0

    :goto_7
    add-int/lit8 v6, v13, -0x1

    if-ge v5, v6, :cond_8

    add-int/lit8 v6, v11, -0x2

    if-ge v2, v6, :cond_8

    .line 1669
    add-int/lit8 v6, v2, 0x1

    .line 1668
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v6

    goto :goto_7

    .line 1636
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v5}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v5}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v7, 0x1

    if-le v5, v7, :cond_1e

    .line 1637
    const v2, 0x7f090285

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    move v8, v2

    goto/16 :goto_4

    .line 1672
    :cond_7
    add-int/2addr v4, v3

    .line 1664
    :cond_8
    add-int/lit8 v5, v7, 0x1

    add-int/lit8 v2, v2, 0x1

    move v7, v5

    goto :goto_6

    .line 1677
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    .line 1678
    mul-int/lit8 v2, v8, 0x2

    add-int/2addr v2, v4

    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    .line 1685
    :cond_a
    :goto_8
    if-lt v4, v9, :cond_b

    .line 1686
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/widget/bh;->j:I

    .line 1687
    if-eqz v10, :cond_b

    .line 1688
    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/widget/bh;->g:I

    sub-int/2addr v2, v3

    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    .line 1694
    :cond_b
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    .line 1695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->l(Lcom/sec/widget/TwIndexScrollView;)F

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/widget/bh;->b(F)Z

    .line 1698
    sget-boolean v2, Lcom/sec/widget/TwIndexScrollView;->b:Z

    if-eqz v2, :cond_c

    .line 1699
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/widget/bh;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/widget/TwIndexScrollView;->a(Lcom/sec/widget/TwIndexScrollView;Ljava/lang/String;)V

    .line 1700
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/widget/TwIndexScrollView;->b:Z

    .line 1720
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1721
    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1722
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1724
    invoke-virtual/range {p3 .. p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1725
    const/16 v2, 0x66

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1732
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->l(Lcom/sec/widget/TwIndexScrollView;)F

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/widget/bh;->a(F)Z

    .line 1734
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    div-int/lit8 v11, v2, 0x2

    .line 1741
    const/4 v2, 0x0

    .line 1742
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual {v3}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1743
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    .line 1744
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090281

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    .line 1752
    :cond_d
    :goto_9
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 1753
    int-to-float v2, v2

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v2, v3

    float-to-int v9, v2

    .line 1755
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1756
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/widget/bh;->j:I

    .line 1757
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/widget/bh;->k:I

    .line 1758
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/widget/bh;->g:I

    .line 1761
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v0, v2, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    .line 1762
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v8, v2, Landroid/graphics/Rect;->top:I

    .line 1763
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v0, v2, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    .line 1764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    .line 1765
    move/from16 v0, v16

    int-to-float v3, v0

    int-to-float v4, v8

    move/from16 v0, v17

    int-to-float v5, v0

    int-to-float v6, v8

    move-object/from16 v2, p1

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1766
    move/from16 v0, v16

    int-to-float v3, v0

    int-to-float v4, v8

    move/from16 v0, v16

    int-to-float v5, v0

    int-to-float v6, v10

    move-object/from16 v2, p1

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1767
    move/from16 v0, v16

    int-to-float v3, v0

    int-to-float v4, v10

    move/from16 v0, v17

    int-to-float v5, v0

    int-to-float v6, v10

    move-object/from16 v2, p1

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1770
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->m(Lcom/sec/widget/TwIndexScrollView;)I

    move-result v2

    sub-int v2, v15, v2

    div-int/lit8 v18, v2, 0x2

    .line 1772
    const/4 v2, 0x0

    const/4 v8, 0x0

    move v10, v9

    move v9, v2

    :goto_a
    if-ge v8, v12, :cond_13

    .line 1774
    if-eqz v9, :cond_e

    .line 1775
    mul-int v2, v15, v9

    .line 1776
    move/from16 v0, v16

    int-to-float v3, v0

    int-to-float v4, v2

    move/from16 v0, v17

    int-to-float v5, v0

    int-to-float v6, v2

    move-object/from16 v2, p1

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1780
    :cond_e
    rem-int v2, v9, v13

    add-int/lit8 v3, v13, -0x1

    if-ne v2, v3, :cond_11

    add-int/lit8 v2, v12, -0x1

    if-ge v8, v2, :cond_11

    .line 1782
    const-string v2, "."

    int-to-float v3, v11

    int-to-float v4, v10

    int-to-float v5, v15

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/widget/bh;->c:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1788
    int-to-float v2, v10

    int-to-float v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v3, v2

    .line 1789
    const/4 v2, 0x0

    move v4, v2

    move v2, v8

    :goto_b
    add-int/lit8 v5, v14, -0x1

    if-ge v4, v5, :cond_12

    add-int/lit8 v5, v12, -0x2

    if-ge v2, v5, :cond_12

    .line 1790
    add-int/lit8 v8, v2, 0x1

    .line 1789
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v8

    goto :goto_b

    .line 1679
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_a

    .line 1680
    mul-int/lit8 v2, v8, 0x2

    add-int/2addr v2, v4

    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_8

    .line 1745
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_d

    .line 1746
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090285

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    goto/16 :goto_9

    .line 1794
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/widget/az;

    iget-object v2, v2, Lcom/sec/widget/az;->a:Ljava/lang/String;

    int-to-float v3, v11

    add-int v4, v10, v18

    int-to-float v4, v4

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1800
    add-int v2, v10, v15

    move v3, v2

    move v2, v8

    .line 1772
    :cond_12
    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v8, v2, 0x1

    move v9, v4

    move v10, v3

    goto/16 :goto_a

    .line 1807
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v2}, Lcom/sec/widget/TwIndexScrollView;->b(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0xff

    move/from16 v0, p6

    if-ne v0, v2, :cond_1a

    .line 1808
    move-object/from16 v0, p3

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1809
    move-object/from16 v0, p3

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1813
    const/4 v2, 0x0

    .line 1814
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual {v3}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1815
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_16

    .line 1817
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090281

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    .line 1826
    :cond_14
    :goto_c
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 1827
    int-to-float v2, v2

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v2, v3

    float-to-int v4, v2

    .line 1829
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/widget/bh;->h:I

    .line 1830
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/widget/bh;->j:I

    .line 1831
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/widget/bh;->k:I

    .line 1833
    const/4 v3, 0x0

    const/4 v2, 0x0

    move v5, v3

    move v3, v4

    :goto_d
    if-ge v2, v6, :cond_19

    .line 1834
    rem-int v4, v5, v7

    add-int/lit8 v9, v7, -0x1

    if-ne v4, v9, :cond_18

    .line 1835
    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->g:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v4, v9

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1836
    const/4 v4, 0x0

    :goto_e
    add-int/lit8 v9, v8, -0x1

    if-ge v4, v9, :cond_15

    .line 1838
    add-int/lit8 v2, v2, 0x1

    .line 1839
    add-int/lit8 v9, v6, -0x1

    if-lt v2, v9, :cond_17

    .line 1833
    :cond_15
    :goto_f
    add-int/lit8 v4, v5, 0x1

    add-int/lit8 v2, v2, 0x1

    move v5, v4

    goto :goto_d

    .line 1818
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_14

    .line 1820
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const v4, 0x7f090285

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    goto :goto_c

    .line 1836
    :cond_17
    add-int/lit8 v4, v4, 0x1

    goto :goto_e

    .line 1844
    :cond_18
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->g:I

    add-int/2addr v3, v4

    goto :goto_f

    .line 1848
    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->h:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->j:I

    rem-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->j:I

    if-ne v2, v4, :cond_1b

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->h:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v2, v4, :cond_1b

    .line 1850
    const-string v2, "."

    int-to-float v4, v11

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/widget/bh;->g:I

    int-to-float v5, v5

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/widget/bh;->c:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v3, v5

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1870
    :cond_1a
    :goto_10
    return-void

    .line 1861
    :cond_1b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->h:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->j:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/widget/bh;->k:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    rem-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v4, v4, -0x1

    if-lt v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/widget/bh;->h:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_1a

    .line 1862
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/widget/bh;->h:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/widget/az;

    iget-object v2, v2, Lcom/sec/widget/az;->a:Ljava/lang/String;

    int-to-float v4, v11

    add-int v3, v3, v18

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_10

    :cond_1d
    move v2, v4

    goto/16 :goto_5

    :cond_1e
    move v8, v2

    goto/16 :goto_4

    :cond_1f
    move v2, v4

    goto/16 :goto_1
.end method

.method public a(F)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1463
    iget v0, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float v4, p1, v0

    .line 1464
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 1539
    :cond_0
    :goto_0
    return v2

    .line 1468
    :cond_1
    iget v5, p0, Lcom/sec/widget/bh;->h:I

    .line 1475
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 1476
    iget v3, p0, Lcom/sec/widget/bh;->j:I

    move v1, v2

    .line 1478
    :goto_1
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v6, v6, 0x6

    int-to-float v6, v6

    add-float/2addr v6, v0

    cmpg-float v6, v6, v4

    if-gez v6, :cond_3

    .line 1479
    rem-int v6, v1, v3

    add-int/lit8 v7, v3, -0x2

    if-ne v6, v7, :cond_2

    .line 1481
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    add-float/2addr v0, v6

    .line 1485
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1483
    :cond_2
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    goto :goto_2

    .line 1489
    :cond_3
    div-int v6, v1, v3

    .line 1490
    rem-int v7, v1, v3

    .line 1491
    iget v8, p0, Lcom/sec/widget/bh;->k:I

    add-int/2addr v3, v8

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v6

    add-int/2addr v3, v7

    .line 1497
    iget-object v6, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v6}, Lcom/sec/widget/TwIndexScrollView;->h(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v6

    if-nez v6, :cond_4

    iget v6, p0, Lcom/sec/widget/bh;->j:I

    rem-int v6, v1, v6

    iget v7, p0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_4

    .line 1499
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    div-int/lit8 v6, v6, 0x6

    int-to-float v6, v6

    add-float/2addr v6, v0

    sub-float v4, v6, v4

    .line 1500
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    sub-float v4, v6, v4

    .line 1501
    iget v6, p0, Lcom/sec/widget/bh;->g:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/widget/bh;->c:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/widget/bh;->k:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    .line 1502
    div-float/2addr v4, v6

    float-to-int v4, v4

    .line 1504
    add-int/2addr v3, v4

    .line 1509
    :cond_4
    iget-object v4, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_7

    .line 1510
    iget-object v3, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1528
    :cond_5
    :goto_3
    iget-object v4, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v4}, Lcom/sec/widget/TwIndexScrollView;->h(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget v4, p0, Lcom/sec/widget/bh;->j:I

    rem-int/2addr v1, v4

    iget v4, p0, Lcom/sec/widget/bh;->j:I

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_0

    .line 1531
    :cond_6
    iput v3, p0, Lcom/sec/widget/bh;->h:I

    .line 1533
    if-eq v3, v5, :cond_0

    .line 1534
    iput v0, p0, Lcom/sec/widget/bh;->l:F

    .line 1535
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1511
    :cond_7
    if-gez v3, :cond_5

    move v3, v2

    .line 1512
    goto :goto_3
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1292
    invoke-virtual {p0, p1}, Lcom/sec/widget/bh;->a(I)Lcom/sec/widget/az;

    move-result-object v0

    .line 1293
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/sec/widget/az;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/az;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1259
    iget v0, p0, Lcom/sec/widget/bh;->h:I

    return v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1543
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/widget/bh;->i:I

    .line 1545
    const-string v0, "TwIndexScrollView"

    const-string v1, "NdepthScroll:: onSizeChanged() / awakenScrollBar() is now being called!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1547
    iget-object v0, p0, Lcom/sec/widget/bh;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-virtual {v0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    .line 1548
    return-void
.end method

.method public d()Lcom/sec/widget/az;
    .locals 2

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/widget/bh;->h:I

    iget-object v1, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/widget/bh;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/widget/bh;->h:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/az;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1300
    iget v0, p0, Lcom/sec/widget/bh;->h:I

    invoke-virtual {p0, v0}, Lcom/sec/widget/bh;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/widget/bh;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/widget/bh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/sec/widget/bh;->d:Landroid/graphics/Rect;

    return-object v0
.end method
