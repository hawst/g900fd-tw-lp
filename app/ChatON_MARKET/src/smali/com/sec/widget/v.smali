.class public final Lcom/sec/widget/v;
.super Ljava/lang/Object;
.source "DropPanelMenuItemImpl.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private a:Lcom/sec/widget/u;

.field private b:Landroid/view/View;

.field private c:Landroid/view/SubMenu;

.field private d:Landroid/view/ContextMenu$ContextMenuInfo;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Landroid/content/Intent;

.field private l:C

.field private m:C

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:I

.field private p:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private q:I


# direct methods
.method constructor <init>(Lcom/sec/widget/u;IIIILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/v;->b:Landroid/view/View;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/widget/v;->o:I

    .line 34
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 47
    iput-object p1, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    .line 48
    iput p3, p0, Lcom/sec/widget/v;->e:I

    .line 49
    iput p2, p0, Lcom/sec/widget/v;->f:I

    .line 50
    iput p4, p0, Lcom/sec/widget/v;->g:I

    .line 51
    iput p5, p0, Lcom/sec/widget/v;->h:I

    .line 52
    iput-object p6, p0, Lcom/sec/widget/v;->i:Ljava/lang/CharSequence;

    .line 53
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/widget/v;->h:I

    return v0
.end method

.method a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/widget/v;->d:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 322
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 420
    if-eqz p1, :cond_0

    .line 421
    iput-object p1, p0, Lcom/sec/widget/v;->b:Landroid/view/View;

    .line 423
    :cond_0
    return-void
.end method

.method a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 216
    iget v2, p0, Lcom/sec/widget/v;->q:I

    .line 217
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 218
    iget v0, p0, Lcom/sec/widget/v;->q:I

    if-eq v2, v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 221
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 217
    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/sec/widget/v;->b:Landroid/view/View;

    return-object v0
.end method

.method b(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    iget v2, p0, Lcom/sec/widget/v;->q:I

    .line 246
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 247
    iget v0, p0, Lcom/sec/widget/v;->q:I

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 246
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 325
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 326
    return-void

    .line 325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public collapseActionView()Z
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 196
    iget-char v0, p0, Lcom/sec/widget/v;->m:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/widget/v;->f:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/widget/v;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/widget/v;->n:Landroid/graphics/drawable/Drawable;

    .line 139
    :goto_0
    return-object v0

    .line 135
    :cond_0
    iget v0, p0, Lcom/sec/widget/v;->o:I

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0}, Lcom/sec/widget/u;->b()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/widget/v;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/widget/v;->k:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/widget/v;->e:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/widget/v;->d:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 178
    iget-char v0, p0, Lcom/sec/widget/v;->l:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/widget/v;->g:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/widget/v;->c:Landroid/view/SubMenu;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/widget/v;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/widget/v;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/widget/v;->j:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/widget/v;->i:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/widget/v;->c:Landroid/view/SubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 212
    iget v1, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 2

    .prologue
    .line 232
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    return-object v0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    return-object v0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 183
    iget-char v0, p0, Lcom/sec/widget/v;->m:C

    if-ne v0, p1, :cond_0

    .line 191
    :goto_0
    return-object p0

    .line 187
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Lcom/sec/widget/v;->m:C

    .line 189
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    goto :goto_0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    iget v2, p0, Lcom/sec/widget/v;->q:I

    .line 202
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 203
    iget v0, p0, Lcom/sec/widget/v;->q:I

    if-eq v2, v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 207
    :cond_0
    return-object p0

    :cond_1
    move v0, v1

    .line 202
    goto :goto_0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/sec/widget/v;->a(Z)V

    .line 227
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 269
    if-eqz p1, :cond_0

    .line 270
    iget v0, p0, Lcom/sec/widget/v;->q:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/widget/v;->q:I

    .line 276
    :goto_0
    iget-object v0, p0, Lcom/sec/widget/v;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/widget/v;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    move v1, v2

    .line 279
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 280
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 279
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 272
    :cond_0
    iget v0, p0, Lcom/sec/widget/v;->q:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/widget/v;->q:I

    goto :goto_0

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0, v2}, Lcom/sec/widget/u;->a(Z)V

    .line 287
    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/v;->n:Landroid/graphics/drawable/Drawable;

    .line 121
    iput p1, p0, Lcom/sec/widget/v;->o:I

    .line 124
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 126
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/widget/v;->o:I

    .line 111
    iput-object p1, p0, Lcom/sec/widget/v;->n:Landroid/graphics/drawable/Drawable;

    .line 113
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 115
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/widget/v;->k:Landroid/content/Intent;

    .line 145
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 165
    iget-char v0, p0, Lcom/sec/widget/v;->l:C

    if-ne v0, p1, :cond_0

    .line 173
    :goto_0
    return-object p0

    .line 169
    :cond_0
    iput-char p1, p0, Lcom/sec/widget/v;->l:C

    .line 171
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    goto :goto_0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    return-object v0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/widget/v;->p:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 308
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 155
    iput-char p1, p0, Lcom/sec/widget/v;->l:C

    .line 156
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Lcom/sec/widget/v;->m:C

    .line 158
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 160
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0}, Lcom/sec/widget/u;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/widget/v;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/widget/v;->i:Ljava/lang/CharSequence;

    .line 74
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 76
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/widget/v;->j:Ljava/lang/CharSequence;

    .line 94
    if-nez p1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/widget/v;->i:Ljava/lang/CharSequence;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 100
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/sec/widget/v;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/widget/v;->a:Lcom/sec/widget/u;

    invoke-virtual {v0, p0}, Lcom/sec/widget/u;->a(Lcom/sec/widget/v;)V

    .line 259
    :cond_0
    return-object p0
.end method
