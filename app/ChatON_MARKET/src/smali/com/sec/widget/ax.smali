.class public abstract Lcom/sec/widget/ax;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "SeparatorCursorAdapter.java"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 21
    new-array v4, v0, [Ljava/lang/String;

    new-array v5, v0, [I

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ax;->b:Ljava/util/ArrayList;

    .line 22
    iput-object p4, p0, Lcom/sec/widget/ax;->c:Ljava/lang/String;

    .line 23
    invoke-direct {p0}, Lcom/sec/widget/ax;->a()V

    .line 24
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 27
    iget-object v2, p0, Lcom/sec/widget/ax;->mCursor:Landroid/database/Cursor;

    .line 28
    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 29
    iget-object v0, p0, Lcom/sec/widget/ax;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 31
    iget-object v0, p0, Lcom/sec/widget/ax;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 55
    :cond_0
    return-void

    .line 35
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    .line 36
    iget-object v0, p0, Lcom/sec/widget/ax;->c:Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 37
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 39
    add-int/lit8 v0, v4, 0x1

    .line 41
    const/4 v1, -0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 42
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 44
    if-eq v0, v1, :cond_2

    .line 45
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget-object v5, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v0, v5

    .line 46
    iget-object v5, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v0, p0, Lcom/sec/widget/ax;->b:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    if-eq v1, v4, :cond_0

    :cond_2
    move v0, v1

    .line 53
    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 61
    if-gez v0, :cond_0

    .line 62
    add-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_0
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/sec/widget/ax;->b(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Landroid/support/v4/widget/SimpleCursorAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Landroid/support/v4/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/sec/widget/ax;->b(I)I

    move-result v0

    .line 96
    if-gez v0, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, v0}, Landroid/support/v4/widget/SimpleCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/widget/ax;->b(I)I

    move-result v0

    .line 106
    if-gez v0, :cond_0

    .line 107
    const-wide/16 v0, -0x1

    .line 109
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, v0}, Landroid/support/v4/widget/SimpleCursorAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/sec/widget/ax;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 139
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/widget/ax;->b(I)I

    move-result v0

    .line 74
    if-gez v0, :cond_1

    .line 75
    neg-int v0, v0

    add-int/lit8 v1, v0, -0x1

    .line 76
    iget-object v0, p0, Lcom/sec/widget/ax;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 78
    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/sec/widget/ax;->getCount()I

    move-result v3

    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, -0x1

    .line 83
    :goto_0
    invoke-virtual {p0, v2, v0, p2, p3}, Lcom/sec/widget/ax;->a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 85
    :goto_1
    return-object v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/sec/widget/ax;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 85
    :cond_1
    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/support/v4/widget/SimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 116
    invoke-direct {p0}, Lcom/sec/widget/ax;->a()V

    .line 117
    return-object v0
.end method
