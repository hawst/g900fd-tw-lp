.class Lcom/sec/widget/af;
.super Ljava/lang/Object;
.source "FastScrollableListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/FastScrollableListView;


# direct methods
.method constructor <init>(Lcom/sec/widget/FastScrollableListView;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableListView;->a(Lcom/sec/widget/FastScrollableListView;)Lcom/sec/widget/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableListView;->a(Lcom/sec/widget/FastScrollableListView;)Lcom/sec/widget/ag;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    iget-object v2, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableListView;->getChildCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCount()I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/widget/ag;->a(Landroid/widget/AbsListView;III)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableListView;->b(Lcom/sec/widget/FastScrollableListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/widget/af;->a:Lcom/sec/widget/FastScrollableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableListView;->b(Lcom/sec/widget/FastScrollableListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 194
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method
