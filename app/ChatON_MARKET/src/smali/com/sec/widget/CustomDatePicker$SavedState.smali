.class Lcom/sec/widget/CustomDatePicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "CustomDatePicker.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/widget/CustomDatePicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDay:I

.field private final mHasYear:Z

.field private final mMonth:I

.field private final mYear:I

.field private final mYearOptional:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551
    new-instance v0, Lcom/sec/widget/o;

    invoke-direct {v0}, Lcom/sec/widget/o;-><init>()V

    sput-object v0, Lcom/sec/widget/CustomDatePicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 512
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 513
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYear:I

    .line 514
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mMonth:I

    .line 515
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mDay:I

    .line 516
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mHasYear:Z

    .line 517
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYearOptional:Z

    .line 518
    return-void

    :cond_0
    move v0, v2

    .line 516
    goto :goto_0

    :cond_1
    move v1, v2

    .line 517
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/widget/c;)V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/sec/widget/CustomDatePicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;IIIZZ)V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 501
    iput p2, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYear:I

    .line 502
    iput p3, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mMonth:I

    .line 503
    iput p4, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mDay:I

    .line 504
    iput-boolean p5, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mHasYear:Z

    .line 505
    iput-boolean p6, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYearOptional:Z

    .line 506
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;IIIZZLcom/sec/widget/c;)V
    .locals 0

    .prologue
    .line 488
    invoke-direct/range {p0 .. p6}, Lcom/sec/widget/CustomDatePicker$SavedState;-><init>(Landroid/os/Parcelable;IIIZZ)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYear:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 525
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mMonth:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 529
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mDay:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mHasYear:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYearOptional:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 542
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 543
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 544
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 545
    iget v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 546
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mHasYear:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 547
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker$SavedState;->mYearOptional:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 548
    return-void

    :cond_0
    move v0, v2

    .line 546
    goto :goto_0

    :cond_1
    move v1, v2

    .line 547
    goto :goto_1
.end method
