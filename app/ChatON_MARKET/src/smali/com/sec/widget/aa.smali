.class Lcom/sec/widget/aa;
.super Ljava/lang/Object;
.source "EditTextWithClearButton.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/widget/EditTextWithClearButton;


# direct methods
.method constructor <init>(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v0}, Lcom/sec/widget/EditTextWithClearButton;->b(Lcom/sec/widget/EditTextWithClearButton;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v0}, Lcom/sec/widget/EditTextWithClearButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v0}, Lcom/sec/widget/EditTextWithClearButton;->c(Lcom/sec/widget/EditTextWithClearButton;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v1}, Lcom/sec/widget/EditTextWithClearButton;->c(Lcom/sec/widget/EditTextWithClearButton;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v0}, Lcom/sec/widget/EditTextWithClearButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v1}, Lcom/sec/widget/EditTextWithClearButton;->b(Lcom/sec/widget/EditTextWithClearButton;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/aa;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v0}, Lcom/sec/widget/EditTextWithClearButton;->d(Lcom/sec/widget/EditTextWithClearButton;)V

    .line 246
    return-void
.end method
