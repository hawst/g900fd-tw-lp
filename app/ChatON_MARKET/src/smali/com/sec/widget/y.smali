.class Lcom/sec/widget/y;
.super Ljava/lang/Object;
.source "EditTextWithClearButton.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/EditTextWithClearButton;


# direct methods
.method constructor <init>(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/widget/y;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/widget/y;->a:Lcom/sec/widget/EditTextWithClearButton;

    const v1, 0x7f070147

    invoke-virtual {v0, v1}, Lcom/sec/widget/EditTextWithClearButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/sec/widget/y;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v1}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/sec/widget/y;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-static {v1}, Lcom/sec/widget/EditTextWithClearButton;->a(Lcom/sec/widget/EditTextWithClearButton;)V

    .line 175
    iget-object v1, p0, Lcom/sec/widget/y;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v1}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
