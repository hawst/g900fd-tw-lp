.class Lcom/sec/widget/bb;
.super Ljava/lang/Object;
.source "TwIndexScrollView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/TwIndexScrollView;


# direct methods
.method constructor <init>(Lcom/sec/widget/TwIndexScrollView;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->a(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/widget/TwIndexScrollView;->a(Lcom/sec/widget/TwIndexScrollView;Z)Z

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->b(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->c(Lcom/sec/widget/TwIndexScrollView;)V

    .line 308
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->b(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->d(Lcom/sec/widget/TwIndexScrollView;)Lcom/sec/widget/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/be;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->d(Lcom/sec/widget/TwIndexScrollView;)Lcom/sec/widget/be;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->d(Lcom/sec/widget/TwIndexScrollView;)Lcom/sec/widget/be;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/widget/bb;->a:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v1}, Lcom/sec/widget/TwIndexScrollView;->e(Lcom/sec/widget/TwIndexScrollView;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/widget/be;->startAnimation(Landroid/view/animation/Animation;)V

    .line 324
    :cond_1
    return-void
.end method
