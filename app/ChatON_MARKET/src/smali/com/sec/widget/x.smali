.class Lcom/sec/widget/x;
.super Ljava/lang/Object;
.source "EditTextWithClearButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/EditTextWithClearButton;


# direct methods
.method constructor <init>(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/widget/x;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154
    iget-object v0, p0, Lcom/sec/widget/x;->a:Lcom/sec/widget/EditTextWithClearButton;

    iget-object v0, v0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/sec/widget/x;->a:Lcom/sec/widget/EditTextWithClearButton;

    iget-object v0, v0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSoundEffectsEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/sec/widget/x;->a:Lcom/sec/widget/EditTextWithClearButton;

    iget-object v0, v0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->playSoundEffect(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/widget/x;->a:Lcom/sec/widget/EditTextWithClearButton;

    iget-object v0, v0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 158
    sput-boolean v2, Lcom/sec/chaton/buddy/BuddyFragment;->i:Z

    .line 159
    return-void
.end method
