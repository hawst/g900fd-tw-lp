.class public Lcom/sec/widget/EditTextWithClearButton;
.super Landroid/widget/RelativeLayout;
.source "EditTextWithClearButton.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Landroid/widget/ImageButton;

.field public b:Landroid/widget/ImageButton;

.field public c:Landroid/widget/EditText;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:I

.field private j:Ljava/lang/CharSequence;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Landroid/view/View$OnFocusChangeListener;

.field private m:Landroid/view/View$OnTouchListener;

.field private n:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    iput-boolean v2, p0, Lcom/sec/widget/EditTextWithClearButton;->f:Z

    .line 33
    iput-boolean v3, p0, Lcom/sec/widget/EditTextWithClearButton;->g:Z

    .line 34
    iput-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/EditTextWithClearButton;->h:I

    .line 37
    iput-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->j:Ljava/lang/CharSequence;

    .line 150
    new-instance v0, Lcom/sec/widget/x;

    invoke-direct {v0, p0}, Lcom/sec/widget/x;-><init>(Lcom/sec/widget/EditTextWithClearButton;)V

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->k:Landroid/view/View$OnClickListener;

    .line 162
    new-instance v0, Lcom/sec/widget/y;

    invoke-direct {v0, p0}, Lcom/sec/widget/y;-><init>(Lcom/sec/widget/EditTextWithClearButton;)V

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->l:Landroid/view/View$OnFocusChangeListener;

    .line 212
    new-instance v0, Lcom/sec/widget/z;

    invoke-direct {v0, p0}, Lcom/sec/widget/z;-><init>(Lcom/sec/widget/EditTextWithClearButton;)V

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->m:Landroid/view/View$OnTouchListener;

    .line 230
    new-instance v0, Lcom/sec/widget/aa;

    invoke-direct {v0, p0}, Lcom/sec/widget/aa;-><init>(Lcom/sec/widget/EditTextWithClearButton;)V

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->n:Landroid/text/TextWatcher;

    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030037

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    const v0, 0x7f070149

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    .line 45
    const v0, 0x7f07014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    .line 46
    const v0, 0x7f070148

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    .line 47
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->setSingleLine()V

    .line 48
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->l:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 50
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->m:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 54
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    sget-object v0, Lcom/sec/chaton/am;->EditTextWithClearButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    iget v1, p0, Lcom/sec/widget/EditTextWithClearButton;->h:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/sec/widget/EditTextWithClearButton;->h:I

    .line 58
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/sec/widget/EditTextWithClearButton;->a(I)V

    .line 60
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->j:Ljava/lang/CharSequence;

    .line 61
    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->j:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/widget/EditTextWithClearButton;->j:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 66
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 73
    invoke-virtual {p0}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020134

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->d:Landroid/graphics/drawable/Drawable;

    .line 74
    invoke-virtual {p0}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->e:Landroid/graphics/drawable/Drawable;

    .line 77
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 78
    invoke-direct {p0}, Lcom/sec/widget/EditTextWithClearButton;->a()V

    .line 79
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 115
    iget-boolean v0, p0, Lcom/sec/widget/EditTextWithClearButton;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 148
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-boolean v0, p0, Lcom/sec/widget/EditTextWithClearButton;->g:Z

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 144
    :goto_1
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 146
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 141
    iget-object v0, p0, Lcom/sec/widget/EditTextWithClearButton;->b:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/widget/EditTextWithClearButton;->b()V

    return-void
.end method

.method static synthetic b(Lcom/sec/widget/EditTextWithClearButton;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/widget/EditTextWithClearButton;->i:I

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 184
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/widget/EditTextWithClearButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 190
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/sec/widget/EditTextWithClearButton;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/widget/EditTextWithClearButton;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/widget/EditTextWithClearButton;->h:I

    return v0
.end method

.method static synthetic d(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/widget/EditTextWithClearButton;->a()V

    return-void
.end method


# virtual methods
.method public setMaxLengthString(I)V
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/widget/EditTextWithClearButton;->i:I

    .line 88
    return-void
.end method

.method public setMaxLenth(I)V
    .locals 3

    .prologue
    .line 101
    iput p1, p0, Lcom/sec/widget/EditTextWithClearButton;->h:I

    .line 102
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    .line 103
    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 104
    iget-object v1, p0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    return-void
.end method

.method public setShowClear(Z)V
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/sec/widget/EditTextWithClearButton;->f:Z

    .line 92
    invoke-direct {p0}, Lcom/sec/widget/EditTextWithClearButton;->a()V

    .line 93
    return-void
.end method

.method public setShowSearch(Z)V
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/widget/EditTextWithClearButton;->g:Z

    .line 97
    invoke-direct {p0}, Lcom/sec/widget/EditTextWithClearButton;->a()V

    .line 98
    return-void
.end method
