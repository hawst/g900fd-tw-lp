.class public Lcom/sec/widget/ah;
.super Ljava/lang/Object;
.source "FastScrollableListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field b:J

.field final synthetic c:Lcom/sec/widget/ag;


# direct methods
.method public constructor <init>(Lcom/sec/widget/ag;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 1019
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/sec/widget/ah;->b:J

    .line 1021
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/widget/ah;->a:J

    .line 1023
    iget-object v0, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/widget/ag;->a(I)V

    .line 1025
    return-void
.end method

.method b()I
    .locals 8

    .prologue
    const-wide/16 v6, 0xd0

    .line 1029
    iget-object v0, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    invoke-virtual {v0}, Lcom/sec/widget/ag;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1031
    const/16 v0, 0xd0

    .line 1049
    :goto_0
    return v0

    .line 1037
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1039
    iget-wide v2, p0, Lcom/sec/widget/ah;->a:J

    iget-wide v4, p0, Lcom/sec/widget/ah;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1041
    const/4 v0, 0x0

    goto :goto_0

    .line 1045
    :cond_1
    iget-wide v2, p0, Lcom/sec/widget/ah;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, Lcom/sec/widget/ah;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    invoke-virtual {v0}, Lcom/sec/widget/ag;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1058
    invoke-virtual {p0}, Lcom/sec/widget/ah;->a()V

    .line 1074
    :goto_0
    return-void

    .line 1064
    :cond_0
    invoke-virtual {p0}, Lcom/sec/widget/ah;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 1066
    iget-object v0, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    invoke-static {v0}, Lcom/sec/widget/ag;->a(Lcom/sec/widget/ag;)Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 1070
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/ah;->c:Lcom/sec/widget/ag;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/ag;->a(I)V

    goto :goto_0
.end method
