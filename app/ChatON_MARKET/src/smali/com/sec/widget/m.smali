.class Lcom/sec/widget/m;
.super Ljava/lang/Object;
.source "CustomDatePicker.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/widget/CustomDatePicker;


# direct methods
.method constructor <init>(Lcom/sec/widget/CustomDatePicker;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/widget/m;->a:Lcom/sec/widget/CustomDatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 294
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/widget/m;->a:Lcom/sec/widget/CustomDatePicker;

    invoke-static {v0}, Lcom/sec/widget/CustomDatePicker;->d(Lcom/sec/widget/CustomDatePicker;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v0, p0, Lcom/sec/widget/m;->a:Lcom/sec/widget/CustomDatePicker;

    invoke-static {v0}, Lcom/sec/widget/CustomDatePicker;->d(Lcom/sec/widget/CustomDatePicker;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 298
    :cond_0
    return-void
.end method
