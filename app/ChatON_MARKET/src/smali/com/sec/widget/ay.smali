.class public abstract Lcom/sec/widget/ay;
.super Landroid/database/DataSetObserver;
.source "TwAbstractIndexer.java"


# instance fields
.field protected a:Ljava/lang/CharSequence;

.field protected b:Ljava/lang/CharSequence;

.field protected c:I

.field protected d:I

.field protected e:Landroid/util/SparseIntArray;

.field protected f:Ljava/text/Collator;

.field protected g:[Ljava/lang/String;

.field private final h:Landroid/database/DataSetObservable;


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/widget/ay;->f:Ljava/text/Collator;

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method a(Ljava/lang/String;)Lcom/sec/widget/ba;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/high16 v8, -0x80000000

    .line 277
    iget-object v5, p0, Lcom/sec/widget/ay;->e:Landroid/util/SparseIntArray;

    .line 279
    invoke-virtual {p0}, Lcom/sec/widget/ay;->d()I

    move-result v2

    .line 280
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 281
    :cond_0
    new-instance v0, Lcom/sec/widget/ba;

    invoke-direct {v0, v2}, Lcom/sec/widget/ba;-><init>(I)V

    .line 388
    :goto_0
    return-object v0

    .line 284
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 285
    :cond_2
    new-instance v0, Lcom/sec/widget/ba;

    invoke-direct {v0, v2}, Lcom/sec/widget/ba;-><init>(I)V

    goto :goto_0

    .line 292
    :cond_3
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 297
    invoke-virtual {v5, v6, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-eq v8, v0, :cond_8

    .line 298
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    move v3, v0

    move v0, v2

    .line 323
    :goto_1
    add-int v4, v0, v3

    div-int/lit8 v4, v4, 0x2

    move v9, v4

    move v4, v3

    move v3, v9

    .line 325
    :goto_2
    if-ge v3, v0, :cond_5

    .line 327
    invoke-virtual {p0, v3}, Lcom/sec/widget/ay;->a(I)Ljava/lang/String;

    move-result-object v7

    .line 328
    if-eqz v7, :cond_4

    const-string v8, ""

    if-ne v7, v8, :cond_a

    .line 329
    :cond_4
    if-nez v3, :cond_9

    .line 376
    :cond_5
    :goto_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_6

    .line 377
    invoke-virtual {v5, v6, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 380
    :cond_6
    if-ge v3, v2, :cond_7

    .line 381
    invoke-virtual {p0, v3}, Lcom/sec/widget/ay;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_7

    .line 384
    invoke-virtual {p0, v0, p1}, Lcom/sec/widget/ay;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 388
    :cond_7
    new-instance v0, Lcom/sec/widget/ba;

    invoke-direct {v0, v3, v1}, Lcom/sec/widget/ba;-><init>(IZ)V

    goto :goto_0

    .line 302
    :cond_8
    iget-object v0, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 304
    if-lez v3, :cond_e

    iget-object v0, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    add-int/lit8 v4, v3, -0x1

    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-le v6, v0, :cond_e

    .line 305
    iget-object v0, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    add-int/lit8 v4, v3, -0x1

    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 306
    invoke-virtual {v5, v0, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 307
    if-eq v0, v8, :cond_e

    .line 308
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 314
    :goto_4
    iget-object v4, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_d

    iget-object v4, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    add-int/lit8 v7, v3, 0x1

    invoke-interface {v4, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    if-ge v6, v4, :cond_d

    .line 315
    iget-object v4, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v4, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 316
    invoke-virtual {v5, v3, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 317
    if-eq v3, v8, :cond_d

    .line 318
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    move v9, v3

    move v3, v0

    move v0, v9

    goto/16 :goto_1

    .line 332
    :cond_9
    add-int/lit8 v3, v3, -0x1

    .line 333
    goto :goto_2

    .line 336
    :cond_a
    invoke-virtual {p0, v7, p1}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 337
    if-eqz v7, :cond_5

    .line 349
    if-gez v7, :cond_b

    .line 350
    add-int/lit8 v3, v3, 0x1

    .line 351
    if-lt v3, v2, :cond_c

    move v3, v2

    .line 353
    goto :goto_3

    :cond_b
    move v0, v3

    move v3, v4

    .line 373
    :cond_c
    add-int v4, v3, v0

    div-int/lit8 v4, v4, 0x2

    move v9, v4

    move v4, v3

    move v3, v9

    .line 374
    goto/16 :goto_2

    :cond_d
    move v3, v0

    move v0, v2

    goto/16 :goto_1

    :cond_e
    move v0, v1

    goto :goto_4
.end method

.method protected abstract a(I)Ljava/lang/String;
.end method

.method public a(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/az;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    .line 152
    invoke-virtual {p0}, Lcom/sec/widget/ay;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :goto_0
    return-object v5

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/sec/widget/ay;->d()I

    move-result v0

    .line 157
    if-nez v0, :cond_1

    .line 159
    const-string v1, "TwAbstractIndexer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIndexInfo() return null: mData.size() =="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    :cond_1
    if-nez p1, :cond_2

    const-string p1, ""

    .line 165
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/widget/ay;->b()V

    move v0, v1

    .line 171
    :goto_1
    iget v3, p0, Lcom/sec/widget/ay;->c:I

    if-ge v0, v3, :cond_11

    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    invoke-interface {v4, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    const-string v4, "A"

    invoke-virtual {p0, v3, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    const-string v4, "Z"

    invoke-virtual {p0, v3, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_7

    :cond_3
    move v0, v2

    :goto_2
    move v3, v2

    .line 180
    :goto_3
    iget v4, p0, Lcom/sec/widget/ay;->c:I

    if-ge v3, v4, :cond_e

    .line 184
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/widget/ay;->a:Ljava/lang/CharSequence;

    invoke-interface {v6, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 188
    if-eqz v0, :cond_8

    .line 189
    invoke-virtual {p0, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;)Lcom/sec/widget/ba;

    move-result-object v4

    move-object v6, v4

    .line 215
    :goto_4
    if-eqz v6, :cond_f

    .line 216
    iget v4, v6, Lcom/sec/widget/ba;->a:I

    .line 217
    iget-boolean v6, v6, Lcom/sec/widget/ba;->b:Z

    .line 220
    :goto_5
    if-gez v4, :cond_4

    .line 221
    neg-int v4, v4

    .line 224
    :cond_4
    if-eqz p2, :cond_5

    if-ne v6, v1, :cond_6

    .line 225
    :cond_5
    new-instance v9, Lcom/sec/widget/az;

    invoke-direct {v9}, Lcom/sec/widget/az;-><init>()V

    .line 226
    iput-boolean v6, v9, Lcom/sec/widget/az;->b:Z

    .line 228
    if-ne v6, v1, :cond_d

    .line 229
    iput v4, v9, Lcom/sec/widget/az;->c:I

    .line 234
    :goto_6
    iget-object v4, p0, Lcom/sec/widget/ay;->g:[Ljava/lang/String;

    aget-object v4, v4, v3

    iput-object v4, v9, Lcom/sec/widget/az;->a:Ljava/lang/String;

    .line 235
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 171
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 191
    :cond_8
    const-string v6, "A"

    invoke-virtual {p0, v4, v6}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_a

    move-object v4, v5

    move v6, v2

    .line 192
    :goto_7
    iget v9, p0, Lcom/sec/widget/ay;->d:I

    if-ge v6, v9, :cond_10

    .line 193
    iget-object v4, p0, Lcom/sec/widget/ay;->b:Ljava/lang/CharSequence;

    invoke-interface {v4, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    .line 194
    invoke-virtual {p0, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;)Lcom/sec/widget/ba;

    move-result-object v4

    .line 196
    iget-boolean v9, v4, Lcom/sec/widget/ba;->b:Z

    if-eqz v9, :cond_9

    move-object v6, v4

    .line 197
    goto :goto_4

    .line 192
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 200
    :cond_a
    const-string v6, "Z"

    invoke-virtual {p0, v4, v6}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_c

    .line 201
    iget v4, p0, Lcom/sec/widget/ay;->d:I

    add-int/lit8 v4, v4, -0x1

    move v6, v4

    move-object v4, v5

    :goto_8
    if-lez v6, :cond_10

    .line 202
    iget-object v4, p0, Lcom/sec/widget/ay;->b:Ljava/lang/CharSequence;

    invoke-interface {v4, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    .line 203
    invoke-virtual {p0, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;)Lcom/sec/widget/ba;

    move-result-object v4

    .line 205
    iget-boolean v9, v4, Lcom/sec/widget/ba;->b:Z

    if-eqz v9, :cond_b

    move-object v6, v4

    .line 206
    goto :goto_4

    .line 201
    :cond_b
    add-int/lit8 v6, v6, -0x1

    goto :goto_8

    .line 210
    :cond_c
    invoke-virtual {p0, v4}, Lcom/sec/widget/ay;->a(Ljava/lang/String;)Lcom/sec/widget/ba;

    move-result-object v4

    move-object v6, v4

    goto :goto_4

    .line 231
    :cond_d
    iput v8, v9, Lcom/sec/widget/az;->c:I

    goto :goto_6

    .line 240
    :cond_e
    invoke-virtual {p0}, Lcom/sec/widget/ay;->c()V

    move-object v5, v7

    .line 241
    goto/16 :goto_0

    :cond_f
    move v4, v8

    move v6, v2

    goto :goto_5

    :cond_10
    move-object v6, v4

    goto/16 :goto_4

    :cond_11
    move v0, v1

    goto/16 :goto_2
.end method

.method public a(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/widget/ay;->h:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 449
    return-void
.end method

.method a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/widget/ay;->g:[Ljava/lang/String;

    return-object v0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 463
    const-string v0, "TwAbstractIndexer"

    const-string v1, "TwAbstractIndexer.onBeginTransaction is now being called !! "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    return-void
.end method

.method public b(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/widget/ay;->h:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 459
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 401
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 402
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 405
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 406
    const/4 v0, 0x1

    .line 408
    :cond_1
    return v0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 469
    const-string v0, "TwAbstractIndexer"

    const-string v1, "TwAbstractIndexer.onEndTransaction  is now being called !! "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    return-void
.end method

.method protected abstract d()I
.end method

.method protected abstract e()Z
.end method

.method public onChanged()V
    .locals 1

    .prologue
    .line 418
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 419
    iget-object v0, p0, Lcom/sec/widget/ay;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 421
    iget-object v0, p0, Lcom/sec/widget/ay;->h:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 422
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 430
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 431
    iget-object v0, p0, Lcom/sec/widget/ay;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 432
    iget-object v0, p0, Lcom/sec/widget/ay;->h:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    .line 433
    return-void
.end method
