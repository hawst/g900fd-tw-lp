.class public Lcom/sec/widget/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z

.field protected b:Landroid/widget/ListAdapter;

.field protected c:I

.field protected d:I

.field protected e:Landroid/widget/Scroller;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/view/GestureDetector;

.field private k:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private m:Landroid/widget/AdapterView$OnItemClickListener;

.field private n:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private o:Z

.field private p:Z

.field private q:Landroid/database/DataSetObserver;

.field private r:Landroid/view/GestureDetector$OnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/HorizontalListView;->a:Z

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    .line 43
    iput v1, p0, Lcom/sec/widget/HorizontalListView;->g:I

    .line 46
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    .line 47
    iput v1, p0, Lcom/sec/widget/HorizontalListView;->i:I

    .line 50
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/HorizontalListView;->k:Ljava/util/Queue;

    .line 54
    iput-boolean v1, p0, Lcom/sec/widget/HorizontalListView;->o:Z

    .line 55
    iput-boolean v1, p0, Lcom/sec/widget/HorizontalListView;->p:Z

    .line 89
    new-instance v0, Lcom/sec/widget/al;

    invoke-direct {v0, p0}, Lcom/sec/widget/al;-><init>(Lcom/sec/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/sec/widget/HorizontalListView;->q:Landroid/database/DataSetObserver;

    .line 359
    new-instance v0, Lcom/sec/widget/ap;

    invoke-direct {v0, p0}, Lcom/sec/widget/ap;-><init>(Lcom/sec/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/sec/widget/HorizontalListView;->r:Landroid/view/GestureDetector$OnGestureListener;

    .line 59
    invoke-direct {p0}, Lcom/sec/widget/HorizontalListView;->b()V

    .line 60
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213
    .line 214
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_1

    .line 216
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 218
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/sec/widget/HorizontalListView;->a(II)V

    .line 221
    invoke-virtual {p0, v1}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 225
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/sec/widget/HorizontalListView;->b(II)V

    .line 227
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 230
    :goto_0
    add-int v0, p1, p2

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 232
    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v2, p0, Lcom/sec/widget/HorizontalListView;->g:I

    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v2, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 233
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 234
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr p1, v0

    .line 236
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 237
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->c:I

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    .line 240
    :cond_0
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    if-gez v0, :cond_1

    .line 241
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    .line 243
    :cond_1
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    goto :goto_0

    .line 246
    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/high16 v2, -0x80000000

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 155
    if-nez v0, :cond_0

    .line 156
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 159
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 160
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 161
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/widget/HorizontalListView;->c()V

    return-void
.end method

.method static synthetic a(Lcom/sec/widget/HorizontalListView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/widget/HorizontalListView;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->n:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    .line 64
    iput v3, p0, Lcom/sec/widget/HorizontalListView;->g:I

    .line 65
    iput v3, p0, Lcom/sec/widget/HorizontalListView;->i:I

    .line 66
    iput v3, p0, Lcom/sec/widget/HorizontalListView;->c:I

    .line 67
    iput v3, p0, Lcom/sec/widget/HorizontalListView;->d:I

    .line 68
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    .line 69
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    .line 70
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/widget/HorizontalListView;->r:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/widget/HorizontalListView;->j:Landroid/view/GestureDetector;

    .line 71
    iput-boolean v3, p0, Lcom/sec/widget/HorizontalListView;->p:Z

    .line 72
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 259
    invoke-virtual {p0, v3}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 260
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 261
    iget v1, p0, Lcom/sec/widget/HorizontalListView;->i:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/widget/HorizontalListView;->i:I

    .line 262
    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 263
    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 264
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    .line 265
    invoke-virtual {p0, v3}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 270
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 271
    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 272
    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 273
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->g:I

    .line 274
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 276
    :cond_1
    return-void
.end method

.method private b(II)V
    .locals 3

    .prologue
    .line 249
    :goto_0
    add-int v0, p1, p2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    if-ltz v0, :cond_0

    .line 250
    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v2, p0, Lcom/sec/widget/HorizontalListView;->f:I

    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v2, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 251
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 252
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 253
    iget v1, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/widget/HorizontalListView;->f:I

    .line 254
    iget v1, p0, Lcom/sec/widget/HorizontalListView;->i:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->i:I

    goto :goto_0

    .line 256
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/widget/HorizontalListView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/widget/HorizontalListView;->p:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/widget/HorizontalListView;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->f:I

    return v0
.end method

.method private c()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/sec/widget/HorizontalListView;->b()V

    .line 144
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 145
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->requestLayout()V

    .line 146
    return-void
.end method

.method private c(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 279
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 280
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->i:I

    .line 281
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->i:I

    move v2, v0

    move v0, v1

    .line 282
    :goto_0
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 283
    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 284
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 285
    add-int v5, v2, v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v3, v2, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 286
    add-int/2addr v2, v4

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method protected a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 355
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 356
    return v1
.end method

.method protected a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 347
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    iget v1, p0, Lcom/sec/widget/HorizontalListView;->d:I

    neg-float v3, p3

    float-to-int v3, v3

    iget v6, p0, Lcom/sec/widget/HorizontalListView;->h:I

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 349
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->requestLayout()V

    .line 351
    const/4 v0, 0x1

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 300
    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->j:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    .line 303
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v8, :cond_5

    .line 305
    iget-boolean v1, p0, Lcom/sec/widget/HorizontalListView;->p:Z

    if-eqz v1, :cond_1

    .line 306
    iput-boolean v0, p0, Lcom/sec/widget/HorizontalListView;->p:Z

    .line 307
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p0}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    :cond_0
    move v0, v7

    .line 342
    :goto_0
    return v0

    .line 313
    :cond_1
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move v6, v0

    .line 315
    :goto_1
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-ge v6, v1, :cond_4

    .line 316
    invoke-virtual {p0, v6}, Lcom/sec/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 317
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 318
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    .line 319
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    .line 320
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v9

    .line 321
    invoke-virtual {v3, v1, v5, v4, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 322
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 323
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    iget v1, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v1, v1, 0x1

    add-int v3, v1, v6

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v4, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v6

    invoke-interface {v1, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget v1, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v1, v1, 0x1

    add-int v3, v1, v6

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v4, p0, Lcom/sec/widget/HorizontalListView;->f:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v6

    invoke-interface {v1, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_3
    move v0, v8

    .line 334
    :cond_4
    if-nez v0, :cond_5

    .line 335
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_5

    .line 336
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p0}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    :cond_5
    move v0, v7

    .line 342
    goto :goto_0

    .line 315
    :cond_6
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1
.end method

.method public synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 165
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 167
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-boolean v0, p0, Lcom/sec/widget/HorizontalListView;->o:Z

    if-eqz v0, :cond_2

    .line 172
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->c:I

    .line 173
    invoke-direct {p0}, Lcom/sec/widget/HorizontalListView;->b()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 175
    iput v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    .line 176
    iput-boolean v1, p0, Lcom/sec/widget/HorizontalListView;->o:Z

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 181
    iput v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    .line 184
    :cond_3
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    if-gtz v0, :cond_4

    .line 185
    iput v1, p0, Lcom/sec/widget/HorizontalListView;->d:I

    .line 186
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 188
    :cond_4
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    iget v1, p0, Lcom/sec/widget/HorizontalListView;->h:I

    if-lt v0, v1, :cond_5

    .line 189
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->h:I

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    .line 190
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 193
    :cond_5
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->c:I

    iget v1, p0, Lcom/sec/widget/HorizontalListView;->d:I

    sub-int/2addr v0, v1

    .line 195
    invoke-direct {p0, v0}, Lcom/sec/widget/HorizontalListView;->b(I)V

    .line 196
    invoke-direct {p0, v0}, Lcom/sec/widget/HorizontalListView;->a(I)V

    .line 197
    invoke-direct {p0, v0}, Lcom/sec/widget/HorizontalListView;->c(I)V

    .line 199
    iget v0, p0, Lcom/sec/widget/HorizontalListView;->d:I

    iput v0, p0, Lcom/sec/widget/HorizontalListView;->c:I

    .line 201
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcom/sec/widget/ao;

    invoke-direct {v0, p0}, Lcom/sec/widget/ao;-><init>(Lcom/sec/widget/HorizontalListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/widget/HorizontalListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/sec/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->q:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 137
    :cond_0
    iput-object p1, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    .line 138
    iget-object v0, p0, Lcom/sec/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sec/widget/HorizontalListView;->q:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 139
    invoke-direct {p0}, Lcom/sec/widget/HorizontalListView;->c()V

    .line 140
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/widget/HorizontalListView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    .line 82
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/widget/HorizontalListView;->n:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 87
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 77
    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method
