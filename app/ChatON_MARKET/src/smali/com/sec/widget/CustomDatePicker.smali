.class public Lcom/sec/widget/CustomDatePicker;
.super Landroid/widget/FrameLayout;
.source "CustomDatePicker.java"


# static fields
.field public static final a:Landroid/widget/NumberPicker$Formatter;

.field private static b:I


# instance fields
.field private final c:Landroid/widget/EditText;

.field private final d:Landroid/widget/EditText;

.field private final e:Landroid/widget/EditText;

.field private final f:Landroid/widget/CheckBox;

.field private final g:Landroid/widget/NumberPicker;

.field private final h:Landroid/widget/NumberPicker;

.field private final i:Landroid/widget/NumberPicker;

.field private j:Lcom/sec/widget/n;

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    sput v0, Lcom/sec/widget/CustomDatePicker;->b:I

    .line 335
    new-instance v0, Lcom/sec/widget/e;

    invoke-direct {v0}, Lcom/sec/widget/e;-><init>()V

    sput-object v0, Lcom/sec/widget/CustomDatePicker;->a:Landroid/widget/NumberPicker$Formatter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/widget/CustomDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/widget/CustomDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/16 v4, 0xc

    const v7, 0x7f080013

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 111
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    .line 113
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 114
    const v2, 0x7f030099

    invoke-virtual {v0, v2, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 116
    const v0, 0x7f0702f4

    invoke-virtual {p0, v0}, Lcom/sec/widget/CustomDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    .line 117
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    sget-object v2, Lcom/sec/widget/CustomDatePicker;->a:Landroid/widget/NumberPicker$Formatter;

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    .line 118
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    .line 120
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    new-instance v2, Lcom/sec/widget/c;

    invoke-direct {v2, p0}, Lcom/sec/widget/c;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 130
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 131
    sput v1, Lcom/sec/widget/CustomDatePicker;->b:I

    .line 132
    iput-object v6, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    .line 138
    :goto_0
    const v0, 0x7f0702f3

    invoke-virtual {p0, v0}, Lcom/sec/widget/CustomDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    .line 139
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    sget-object v2, Lcom/sec/widget/CustomDatePicker;->a:Landroid/widget/NumberPicker$Formatter;

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    .line 140
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    .line 141
    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v2

    .line 148
    aget-object v0, v2, v1

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 149
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 150
    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    sget v2, Lcom/sec/widget/CustomDatePicker;->b:I

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    .line 135
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/widget/CustomDatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v4}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 160
    :goto_2
    sget v0, Lcom/sec/widget/CustomDatePicker;->b:I

    if-nez v0, :cond_7

    .line 161
    iput-object v6, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    .line 166
    :goto_3
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v3, v4}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    .line 167
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    new-instance v3, Lcom/sec/widget/f;

    invoke-direct {v3, p0}, Lcom/sec/widget/f;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 182
    const v0, 0x7f0702f5

    invoke-virtual {p0, v0}, Lcom/sec/widget/CustomDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    .line 184
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    .line 185
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    new-instance v3, Lcom/sec/widget/g;

    invoke-direct {v3, p0}, Lcom/sec/widget/g;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 196
    sget v0, Lcom/sec/widget/CustomDatePicker;->b:I

    if-nez v0, :cond_8

    .line 197
    iput-object v6, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    .line 202
    :goto_4
    const v0, 0x7f0702f1

    invoke-virtual {p0, v0}, Lcom/sec/widget/CustomDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->f:Landroid/widget/CheckBox;

    .line 203
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->f:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/widget/h;

    invoke-direct {v3, p0}, Lcom/sec/widget/h;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 226
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    const/16 v3, 0x76c

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    const/16 v3, 0x834

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/i;

    invoke-direct {v3, p0}, Lcom/sec/widget/i;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 252
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/j;

    invoke-direct {v3, p0}, Lcom/sec/widget/j;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/k;

    invoke-direct {v3, p0}, Lcom/sec/widget/k;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 281
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/l;

    invoke-direct {v3, p0}, Lcom/sec/widget/l;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/m;

    invoke-direct {v3, p0}, Lcom/sec/widget/m;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 310
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/widget/d;

    invoke-direct {v3, p0}, Lcom/sec/widget/d;-><init>(Lcom/sec/widget/CustomDatePicker;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 324
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 325
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p0, v3, v4, v0, v6}, Lcom/sec/widget/CustomDatePicker;->a(IIILcom/sec/widget/n;)V

    .line 328
    invoke-direct {p0, v2}, Lcom/sec/widget/CustomDatePicker;->a([Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/widget/CustomDatePicker;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    .line 331
    invoke-virtual {p0, v1}, Lcom/sec/widget/CustomDatePicker;->setEnabled(Z)V

    .line 333
    :cond_5
    return-void

    .line 155
    :cond_6
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v4}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    goto/16 :goto_2

    .line 163
    :cond_7
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    sget v3, Lcom/sec/widget/CustomDatePicker;->b:I

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    .line 164
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/widget/CustomDatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTextColor(I)V

    goto/16 :goto_3

    .line 199
    :cond_8
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    sget v3, Lcom/sec/widget/CustomDatePicker;->b:I

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    .line 200
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/widget/CustomDatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTextColor(I)V

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/sec/widget/CustomDatePicker;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    return p1
.end method

.method private a(Landroid/widget/EditText;I)V
    .locals 2

    .prologue
    .line 638
    if-nez p1, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 642
    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 643
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 644
    invoke-virtual {p1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 645
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/widget/CustomDatePicker;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->j()V

    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 429
    invoke-virtual {p0}, Lcom/sec/widget/CustomDatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v6

    .line 436
    const v0, 0x7f0702f2

    invoke-virtual {p0, v0}, Lcom/sec/widget/CustomDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 437
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 440
    array-length v7, v6

    const/4 v1, 0x0

    move v3, v1

    move v4, v5

    :goto_0
    if-ge v3, v7, :cond_5

    aget-char v1, v6, v3

    .line 441
    const/4 v2, 0x0

    .line 442
    const/16 v8, 0x64

    if-ne v1, v8, :cond_2

    .line 443
    iget-object v1, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 444
    iget-object v2, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    .line 452
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    move-object v1, v2

    .line 453
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v8, 0x2

    if-le v1, v8, :cond_1

    .line 454
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 455
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v8, "EditText"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 456
    check-cast v1, Landroid/widget/EditText;

    .line 457
    if-eqz v1, :cond_1

    .line 458
    array-length v2, v6

    if-ne v4, v2, :cond_4

    .line 459
    const v2, 0x10000006

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 467
    :cond_1
    :goto_2
    add-int/lit8 v2, v4, 0x1

    .line 440
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto :goto_0

    .line 445
    :cond_2
    const/16 v8, 0x4d

    if-ne v1, v8, :cond_3

    .line 446
    iget-object v1, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 447
    iget-object v2, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    goto :goto_1

    .line 448
    :cond_3
    const/16 v8, 0x79

    if-ne v1, v8, :cond_0

    .line 449
    iget-object v1, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 450
    iget-object v2, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    goto :goto_1

    .line 461
    :cond_4
    const v2, 0x10000005

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    goto :goto_2

    .line 471
    :cond_5
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/CustomDatePicker;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/widget/CustomDatePicker;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    return p1
.end method

.method static synthetic b(Lcom/sec/widget/CustomDatePicker;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/widget/CustomDatePicker;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    return p1
.end method

.method static synthetic c(Lcom/sec/widget/CustomDatePicker;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 485
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/widget/CustomDatePicker;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 650
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->f()V

    .line 651
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->f:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 652
    iget-object v3, p0, Lcom/sec/widget/CustomDatePicker;->f:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    iget v3, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 654
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    iget v3, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    invoke-direct {p0, v0, v3}, Lcom/sec/widget/CustomDatePicker;->a(Landroid/widget/EditText;I)V

    .line 655
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    iget-boolean v3, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    .line 660
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    iget v1, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/CustomDatePicker;->a(Landroid/widget/EditText;I)V

    .line 662
    return-void

    :cond_0
    move v0, v2

    .line 652
    goto :goto_0

    :cond_1
    move v1, v2

    .line 655
    goto :goto_1
.end method

.method private f()V
    .locals 4

    .prologue
    .line 665
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 667
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    :goto_0
    iget v2, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 668
    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    .line 669
    iget-object v1, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v1, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 670
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    iget v1, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 671
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/CustomDatePicker;->a(Landroid/widget/EditText;I)V

    .line 672
    return-void

    .line 667
    :cond_0
    const/16 v0, 0x7d0

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 722
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 702
    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    :goto_1
    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 703
    const/4 v0, 0x2

    iget v2, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 704
    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    .line 706
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 707
    const/4 v0, 0x0

    .line 708
    if-eqz v2, :cond_2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 709
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 711
    :cond_2
    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    .line 712
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    if-gt v0, v1, :cond_3

    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    if-nez v0, :cond_0

    .line 713
    :cond_3
    iput v1, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    goto :goto_0

    .line 702
    :cond_4
    const/16 v0, 0x7d0

    goto :goto_1
.end method

.method private h()V
    .locals 5

    .prologue
    const/16 v4, 0x834

    const/16 v3, 0x76c

    .line 725
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 730
    const/4 v0, 0x0

    .line 731
    if-eqz v1, :cond_2

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 732
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 734
    :cond_2
    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    .line 736
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    if-le v0, v4, :cond_3

    .line 737
    iput v4, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    goto :goto_0

    .line 738
    :cond_3
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    if-ge v0, v3, :cond_0

    .line 739
    iput v3, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 749
    const/4 v0, 0x0

    .line 750
    if-eqz v1, :cond_2

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 751
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 752
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    .line 754
    :cond_2
    if-nez v0, :cond_0

    .line 755
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 765
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->h()V

    .line 766
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->i()V

    .line 767
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->g()V

    .line 770
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->e()V

    .line 772
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->j:Lcom/sec/widget/n;

    if-eqz v0, :cond_0

    .line 773
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 774
    :goto_0
    iget-object v1, p0, Lcom/sec/widget/CustomDatePicker;->j:Lcom/sec/widget/n;

    iget v2, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    iget v3, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    invoke-interface {v1, p0, v0, v2, v3}, Lcom/sec/widget/n;->a(Lcom/sec/widget/CustomDatePicker;III)V

    .line 776
    :cond_0
    return-void

    .line 773
    :cond_1
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 677
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    goto :goto_0
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    if-eq v0, p3, :cond_2

    .line 475
    :cond_0
    iget-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->d()I

    move-result p1

    :cond_1
    iput p1, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    .line 476
    iput p2, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    .line 477
    iput p3, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    .line 478
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->e()V

    .line 479
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/widget/CustomDatePicker;->a([Ljava/lang/String;)V

    .line 480
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->j()V

    .line 482
    :cond_2
    return-void
.end method

.method public a(IIILcom/sec/widget/n;)V
    .locals 6

    .prologue
    .line 605
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/widget/CustomDatePicker;->a(IIIZLcom/sec/widget/n;)V

    .line 606
    return-void
.end method

.method public a(IIIZLcom/sec/widget/n;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 623
    if-eqz p4, :cond_0

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->d()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    .line 624
    iput p2, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    .line 625
    iput p3, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    .line 626
    iput-boolean p4, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    .line 627
    if-eqz p4, :cond_2

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    .line 628
    iput-object p5, p0, Lcom/sec/widget/CustomDatePicker;->j:Lcom/sec/widget/n;

    .line 629
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->e()V

    .line 630
    return-void

    :cond_0
    move v0, p1

    .line 623
    goto :goto_0

    .line 627
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 689
    iget v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 570
    invoke-virtual {p0, p1}, Lcom/sec/widget/CustomDatePicker;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 571
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 582
    check-cast p1, Lcom/sec/widget/CustomDatePicker$SavedState;

    .line 583
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 584
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->a()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    .line 585
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    .line 586
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    .line 587
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    .line 588
    invoke-virtual {p1}, Lcom/sec/widget/CustomDatePicker$SavedState;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    .line 589
    invoke-direct {p0}, Lcom/sec/widget/CustomDatePicker;->e()V

    .line 590
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 8

    .prologue
    .line 575
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 577
    new-instance v0, Lcom/sec/widget/CustomDatePicker$SavedState;

    iget v2, p0, Lcom/sec/widget/CustomDatePicker;->m:I

    iget v3, p0, Lcom/sec/widget/CustomDatePicker;->l:I

    iget v4, p0, Lcom/sec/widget/CustomDatePicker;->k:I

    iget-boolean v5, p0, Lcom/sec/widget/CustomDatePicker;->o:Z

    iget-boolean v6, p0, Lcom/sec/widget/CustomDatePicker;->n:Z

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/sec/widget/CustomDatePicker$SavedState;-><init>(Landroid/os/Parcelable;IIIZZLcom/sec/widget/c;)V

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 353
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 354
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    .line 355
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->h:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    .line 356
    iget-object v0, p0, Lcom/sec/widget/CustomDatePicker;->i:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    .line 357
    return-void
.end method
