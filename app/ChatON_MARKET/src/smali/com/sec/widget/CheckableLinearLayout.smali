.class public Lcom/sec/widget/CheckableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "CheckableLinearLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field d:I

.field e:I

.field f:Landroid/widget/Checkable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 10
    const-string v0, "http://schemas.android.com/apk/res/com.sec.chaton"

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->a:Ljava/lang/String;

    .line 11
    const-string v0, "checkable_single"

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->b:Ljava/lang/String;

    .line 12
    const-string v0, "checkable_multiple"

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->c:Ljava/lang/String;

    .line 20
    const-string v0, "http://schemas.android.com/apk/res/com.sec.chaton"

    const-string v1, "checkable_single"

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CheckableLinearLayout;->d:I

    .line 21
    const-string v0, "http://schemas.android.com/apk/res/com.sec.chaton"

    const-string v1, "checkable_multiple"

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/CheckableLinearLayout;->e:I

    .line 22
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/widget/CheckableLinearLayout;->d:I

    invoke-virtual {p0, v0}, Lcom/sec/widget/CheckableLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 32
    iget v1, p0, Lcom/sec/widget/CheckableLinearLayout;->e:I

    invoke-virtual {p0, v1}, Lcom/sec/widget/CheckableLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 34
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 35
    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    .line 41
    :goto_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 42
    return-void

    .line 36
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 37
    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/CheckableLinearLayout;->f:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    goto :goto_0
.end method
