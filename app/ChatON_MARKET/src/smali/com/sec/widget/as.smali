.class Lcom/sec/widget/as;
.super Ljava/lang/Object;
.source "PreferenceListFragment.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/widget/as;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/widget/aq;)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/sec/widget/as;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/widget/as;I)I
    .locals 0

    .prologue
    .line 158
    iput p1, p0, Lcom/sec/widget/as;->a:I

    return p1
.end method

.method static synthetic a(Lcom/sec/widget/as;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/widget/as;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/widget/as;I)I
    .locals 0

    .prologue
    .line 158
    iput p1, p0, Lcom/sec/widget/as;->b:I

    return p1
.end method


# virtual methods
.method public a(Lcom/sec/widget/as;)I
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/widget/as;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/widget/as;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 166
    if-nez v0, :cond_0

    .line 167
    iget v0, p0, Lcom/sec/widget/as;->a:I

    iget v1, p1, Lcom/sec/widget/as;->a:I

    if-ne v0, v1, :cond_2

    .line 168
    iget v0, p0, Lcom/sec/widget/as;->b:I

    iget v1, p1, Lcom/sec/widget/as;->b:I

    if-ne v0, v1, :cond_1

    .line 169
    const/4 v0, 0x0

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    iget v0, p0, Lcom/sec/widget/as;->b:I

    iget v1, p1, Lcom/sec/widget/as;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 174
    :cond_2
    iget v0, p0, Lcom/sec/widget/as;->a:I

    iget v1, p1, Lcom/sec/widget/as;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 158
    check-cast p1, Lcom/sec/widget/as;

    invoke-virtual {p0, p1}, Lcom/sec/widget/as;->a(Lcom/sec/widget/as;)I

    move-result v0

    return v0
.end method
