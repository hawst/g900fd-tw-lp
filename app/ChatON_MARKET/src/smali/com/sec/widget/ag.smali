.class Lcom/sec/widget/ag;
.super Ljava/lang/Object;
.source "FastScrollableListView.java"


# static fields
.field private static a:I


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/graphics/RectF;

.field private h:I

.field private i:Landroid/widget/AbsListView;

.field private j:Z

.field private k:I

.field private l:Landroid/graphics/Paint;

.field private m:I

.field private n:I

.field private o:Z

.field private p:[Ljava/lang/Object;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Lcom/sec/widget/ah;

.field private t:I

.field private u:Landroid/os/Handler;

.field private v:Landroid/widget/BaseAdapter;

.field private w:Landroid/widget/SectionIndexer;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x4

    sput v0, Lcom/sec/widget/ag;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;)V
    .locals 1

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/ag;->n:I

    .line 266
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ag;->u:Landroid/os/Handler;

    .line 276
    iput-object p2, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    .line 278
    invoke-direct {p0, p1}, Lcom/sec/widget/ag;->a(Landroid/content/Context;)V

    .line 280
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/ag;)Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private a(F)V
    .locals 13

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v7

    .line 659
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/widget/ag;->j:Z

    .line 661
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v7

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000    # 8.0f

    div-float v8, v0, v1

    .line 663
    iget-object v9, p0, Lcom/sec/widget/ag;->p:[Ljava/lang/Object;

    .line 667
    if-eqz v9, :cond_7

    array-length v0, v9

    const/4 v1, 0x1

    if-le v0, v1, :cond_7

    .line 669
    array-length v10, v9

    .line 671
    int-to-float v0, v10

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 673
    if-lt v0, v10, :cond_0

    .line 675
    add-int/lit8 v0, v10, -0x1

    .line 683
    :cond_0
    iget-object v1, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    invoke-interface {v1, v0}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v2

    .line 703
    add-int/lit8 v5, v0, 0x1

    .line 707
    add-int/lit8 v1, v10, -0x1

    if-ge v0, v1, :cond_10

    .line 709
    iget-object v1, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v3}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v1

    move v6, v1

    .line 715
    :goto_0
    if-ne v6, v2, :cond_f

    move v1, v2

    move v3, v0

    .line 719
    :goto_1
    if-lez v3, :cond_e

    .line 721
    add-int/lit8 v1, v3, -0x1

    .line 723
    iget-object v3, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    invoke-interface {v3, v1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v3

    .line 725
    if-eq v3, v2, :cond_1

    move v2, v3

    move v3, v1

    .line 759
    :goto_2
    add-int/lit8 v4, v5, 0x1

    .line 763
    :goto_3
    if-ge v4, v10, :cond_2

    iget-object v11, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    invoke-interface {v11, v4}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v11

    if-ne v11, v6, :cond_2

    .line 765
    add-int/lit8 v4, v4, 0x1

    .line 767
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 733
    :cond_1
    if-nez v1, :cond_d

    .line 739
    const/4 v1, 0x0

    move v2, v3

    move v3, v1

    move v1, v0

    .line 741
    goto :goto_2

    .line 777
    :cond_2
    int-to-float v4, v1

    int-to-float v11, v10

    div-float/2addr v4, v11

    .line 779
    int-to-float v5, v5

    int-to-float v10, v10

    div-float/2addr v5, v10

    .line 781
    if-ne v1, v0, :cond_4

    sub-float v0, p1, v4

    cmpg-float v0, v0, v8

    if-gez v0, :cond_4

    move v0, v2

    .line 795
    :goto_4
    add-int/lit8 v1, v7, -0x1

    if-le v0, v1, :cond_c

    .line 796
    add-int/lit8 v0, v7, -0x1

    move v1, v0

    .line 799
    :goto_5
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_5

    .line 801
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ExpandableListView;

    .line 803
    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 843
    :goto_6
    if-ltz v3, :cond_b

    .line 845
    aget-object v0, v9, v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ag;->q:Ljava/lang/String;

    .line 847
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_a

    :cond_3
    array-length v0, v9

    if-ge v3, v0, :cond_a

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/sec/widget/ag;->r:Z

    .line 857
    :goto_8
    return-void

    .line 787
    :cond_4
    sub-int v0, v6, v2

    int-to-float v0, v0

    sub-float v1, p1, v4

    mul-float/2addr v0, v1

    sub-float v1, v5, v4

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    goto :goto_4

    .line 807
    :cond_5
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_6

    .line 809
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_6

    .line 813
    :cond_6
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_6

    .line 819
    :cond_7
    int-to-float v0, v7

    mul-float/2addr v0, p1

    float-to-int v1, v0

    .line 821
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_8

    .line 823
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ExpandableListView;

    .line 825
    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 839
    :goto_9
    const/4 v3, -0x1

    goto :goto_6

    .line 829
    :cond_8
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_9

    .line 831
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_9

    .line 835
    :cond_9
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    iget v2, p0, Lcom/sec/widget/ag;->m:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_9

    .line 847
    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    .line 853
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/widget/ag;->r:Z

    goto :goto_8

    :cond_c
    move v1, v0

    goto/16 :goto_5

    :cond_d
    move v12, v3

    move v3, v1

    move v1, v12

    goto/16 :goto_1

    :cond_e
    move v2, v1

    move v3, v0

    move v1, v0

    goto/16 :goto_2

    :cond_f
    move v1, v0

    move v3, v0

    goto/16 :goto_2

    :cond_10
    move v6, v7

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const v4, 0x7f02028c

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 365
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 367
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/sec/widget/ag;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 371
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ag;->c:Landroid/graphics/drawable/Drawable;

    .line 375
    iput-boolean v2, p0, Lcom/sec/widget/ag;->j:Z

    .line 377
    invoke-direct {p0}, Lcom/sec/widget/ag;->e()V

    .line 379
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090303

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ag;->h:I

    .line 383
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ag;->g:Landroid/graphics/RectF;

    .line 385
    new-instance v0, Lcom/sec/widget/ah;

    invoke-direct {v0, p0}, Lcom/sec/widget/ah;-><init>(Lcom/sec/widget/ag;)V

    iput-object v0, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    .line 387
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    .line 389
    iget-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 391
    iget-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 393
    iget-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/widget/ag;->h:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 395
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v2, [I

    const v2, 0x1010036

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 399
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    .line 403
    iget-object v1, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 407
    iput v3, p0, Lcom/sec/widget/ag;->t:I

    .line 409
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 347
    iput-object p2, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    .line 349
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090304

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ag;->e:I

    .line 353
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090305

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ag;->d:I

    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/ag;->x:Z

    .line 359
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    .line 338
    iget-object v1, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/sec/widget/ag;->e:I

    sub-int v2, v0, v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/widget/ag;->d:I

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 343
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 607
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    .line 609
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    .line 611
    instance-of v0, v1, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 613
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ag;->m:I

    .line 615
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 635
    :cond_0
    instance-of v0, v1, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 637
    check-cast v0, Landroid/widget/BaseAdapter;

    iput-object v0, p0, Lcom/sec/widget/ag;->v:Landroid/widget/BaseAdapter;

    .line 639
    check-cast v1, Landroid/widget/SectionIndexer;

    iput-object v1, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    .line 641
    iget-object v0, p0, Lcom/sec/widget/ag;->w:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ag;->p:[Ljava/lang/Object;

    .line 653
    :goto_0
    return-void

    .line 645
    :cond_1
    check-cast v1, Landroid/widget/BaseAdapter;

    iput-object v1, p0, Lcom/sec/widget/ag;->v:Landroid/widget/BaseAdapter;

    .line 647
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/widget/ag;->p:[Ljava/lang/Object;

    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    .line 863
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 865
    iget-object v1, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 867
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 869
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    return v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 284
    packed-switch p1, :pswitch_data_0

    .line 320
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/widget/ag;->t:I

    .line 322
    return-void

    .line 288
    :pswitch_1
    iget-object v0, p0, Lcom/sec/widget/ag;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 290
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 296
    :pswitch_2
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 298
    invoke-direct {p0}, Lcom/sec/widget/ag;->d()V

    .line 306
    :cond_0
    :pswitch_3
    iget-object v0, p0, Lcom/sec/widget/ag;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 312
    :pswitch_4
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    .line 314
    iget-object v1, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    iget v2, p0, Lcom/sec/widget/ag;->e:I

    sub-int v2, v0, v2

    iget v3, p0, Lcom/sec/widget/ag;->f:I

    iget v4, p0, Lcom/sec/widget/ag;->f:I

    iget v5, p0, Lcom/sec/widget/ag;->d:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/widget/AbsListView;->invalidate(IIII)V

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method a(IIII)V
    .locals 5

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/sec/widget/ag;->e:I

    sub-int v1, p1, v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/widget/ag;->d:I

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/ag;->g:Landroid/graphics/RectF;

    .line 507
    iget v1, p0, Lcom/sec/widget/ag;->h:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 509
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/sec/widget/ag;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 511
    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 513
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/sec/widget/ag;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 515
    iget-object v1, p0, Lcom/sec/widget/ag;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 517
    iget-object v1, p0, Lcom/sec/widget/ag;->c:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 523
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 425
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-nez v0, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    iget v1, p0, Lcom/sec/widget/ag;->f:I

    .line 435
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v2

    .line 437
    iget-object v3, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    .line 439
    const/4 v0, -0x1

    .line 441
    iget v4, p0, Lcom/sec/widget/ag;->t:I

    if-ne v4, v8, :cond_3

    .line 443
    invoke-virtual {v3}, Lcom/sec/widget/ah;->b()I

    move-result v0

    .line 445
    const/16 v3, 0x68

    if-ge v0, v3, :cond_2

    .line 451
    :cond_2
    iget v3, p0, Lcom/sec/widget/ag;->e:I

    mul-int/2addr v3, v0

    div-int/lit16 v3, v3, 0xd0

    sub-int v3, v2, v3

    .line 453
    iget-object v4, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/sec/widget/ag;->d:I

    invoke-virtual {v4, v3, v7, v2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 455
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/widget/ag;->x:Z

    .line 459
    :cond_3
    int-to-float v3, v1

    invoke-virtual {p1, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 461
    iget-object v3, p0, Lcom/sec/widget/ag;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 463
    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {p1, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 467
    iget v3, p0, Lcom/sec/widget/ag;->t:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/sec/widget/ag;->r:Z

    if-eqz v3, :cond_4

    .line 469
    iget-object v0, p0, Lcom/sec/widget/ag;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 471
    iget-object v0, p0, Lcom/sec/widget/ag;->l:Landroid/graphics/Paint;

    .line 473
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v1

    .line 475
    iget-object v2, p0, Lcom/sec/widget/ag;->g:Landroid/graphics/RectF;

    .line 477
    iget-object v3, p0, Lcom/sec/widget/ag;->q:Ljava/lang/String;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget v5, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v5

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    iget v5, p0, Lcom/sec/widget/ag;->h:I

    div-int/lit8 v5, v5, 0x4

    add-int/2addr v2, v5

    int-to-float v2, v2

    sub-float v1, v2, v1

    invoke-virtual {p1, v3, v4, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 481
    :cond_4
    iget v3, p0, Lcom/sec/widget/ag;->t:I

    if-ne v3, v8, :cond_0

    .line 483
    if-nez v0, :cond_5

    .line 485
    invoke-virtual {p0, v7}, Lcom/sec/widget/ag;->a(I)V

    goto :goto_0

    .line 489
    :cond_5
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    iget v3, p0, Lcom/sec/widget/ag;->e:I

    sub-int v3, v2, v3

    iget v4, p0, Lcom/sec/widget/ag;->d:I

    add-int/2addr v4, v1

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/widget/AbsListView;->invalidate(IIII)V

    goto/16 :goto_0
.end method

.method a(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 531
    iget v0, p0, Lcom/sec/widget/ag;->n:I

    if-eq v0, p4, :cond_0

    if-lez p3, :cond_0

    .line 533
    iput p4, p0, Lcom/sec/widget/ag;->n:I

    .line 535
    iget v0, p0, Lcom/sec/widget/ag;->n:I

    div-int/2addr v0, p3

    sget v3, Lcom/sec/widget/ag;->a:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/widget/ag;->o:Z

    .line 539
    :cond_0
    iget-boolean v0, p0, Lcom/sec/widget/ag;->o:Z

    if-nez v0, :cond_3

    .line 541
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-eqz v0, :cond_1

    .line 543
    invoke-virtual {p0, v2}, Lcom/sec/widget/ag;->a(I)V

    .line 585
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 535
    goto :goto_0

    .line 551
    :cond_3
    sub-int v0, p4, p3

    if-lez v0, :cond_4

    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-eq v0, v4, :cond_4

    .line 553
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getHeight()I

    move-result v0

    iget v3, p0, Lcom/sec/widget/ag;->d:I

    sub-int/2addr v0, v3

    mul-int/2addr v0, p2

    sub-int v3, p4, p3

    div-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/ag;->f:I

    .line 557
    iget-boolean v0, p0, Lcom/sec/widget/ag;->x:Z

    if-eqz v0, :cond_4

    .line 559
    invoke-direct {p0}, Lcom/sec/widget/ag;->d()V

    .line 561
    iput-boolean v2, p0, Lcom/sec/widget/ag;->x:Z

    .line 567
    :cond_4
    iput-boolean v1, p0, Lcom/sec/widget/ag;->j:Z

    .line 569
    iget v0, p0, Lcom/sec/widget/ag;->k:I

    if-eq p2, v0, :cond_1

    .line 575
    iput p2, p0, Lcom/sec/widget/ag;->k:I

    .line 577
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-eq v0, v4, :cond_1

    .line 579
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/widget/ag;->a(I)V

    .line 581
    iget-object v0, p0, Lcom/sec/widget/ag;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method a(FF)Z
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/widget/ag;->e:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/widget/ag;->f:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/widget/ag;->f:I

    iget v1, p0, Lcom/sec/widget/ag;->d:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 873
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 875
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/widget/ag;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/widget/ag;->a(I)V

    .line 879
    const/4 v0, 0x1

    .line 885
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/widget/ag;->a(I)V

    .line 415
    return-void
.end method

.method b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 891
    iget v1, p0, Lcom/sec/widget/ag;->t:I

    if-nez v1, :cond_1

    .line 997
    :cond_0
    :goto_0
    return v0

    .line 897
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 899
    if-nez v1, :cond_4

    .line 901
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/sec/widget/ag;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 903
    invoke-virtual {p0, v4}, Lcom/sec/widget/ag;->a(I)V

    .line 905
    iget-object v0, p0, Lcom/sec/widget/ag;->v:Landroid/widget/BaseAdapter;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 907
    invoke-direct {p0}, Lcom/sec/widget/ag;->e()V

    .line 911
    :cond_2
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    if-eqz v0, :cond_3

    .line 913
    iget-object v0, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 919
    :cond_3
    invoke-direct {p0}, Lcom/sec/widget/ag;->f()V

    move v0, v2

    .line 921
    goto :goto_0

    .line 925
    :cond_4
    if-ne v1, v2, :cond_6

    .line 927
    iget v1, p0, Lcom/sec/widget/ag;->t:I

    if-ne v1, v4, :cond_0

    .line 929
    iget-object v1, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    if-eqz v1, :cond_5

    .line 937
    iget-object v1, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 943
    :cond_5
    invoke-virtual {p0, v5}, Lcom/sec/widget/ag;->a(I)V

    .line 945
    iget-object v0, p0, Lcom/sec/widget/ag;->u:Landroid/os/Handler;

    .line 947
    iget-object v1, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 949
    iget-object v1, p0, Lcom/sec/widget/ag;->s:Lcom/sec/widget/ah;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v2

    .line 951
    goto :goto_0

    .line 955
    :cond_6
    if-ne v1, v5, :cond_0

    .line 957
    iget v1, p0, Lcom/sec/widget/ag;->t:I

    if-ne v1, v4, :cond_0

    .line 959
    iget-object v1, p0, Lcom/sec/widget/ag;->i:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getHeight()I

    move-result v3

    .line 963
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget v4, p0, Lcom/sec/widget/ag;->d:I

    sub-int/2addr v1, v4

    add-int/lit8 v1, v1, 0xa

    .line 965
    if-gez v1, :cond_7

    .line 975
    :goto_1
    iget v1, p0, Lcom/sec/widget/ag;->f:I

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v5, :cond_8

    move v0, v2

    .line 977
    goto :goto_0

    .line 969
    :cond_7
    iget v0, p0, Lcom/sec/widget/ag;->d:I

    add-int/2addr v0, v1

    if-le v0, v3, :cond_a

    .line 971
    iget v0, p0, Lcom/sec/widget/ag;->d:I

    sub-int v0, v3, v0

    goto :goto_1

    .line 981
    :cond_8
    iput v0, p0, Lcom/sec/widget/ag;->f:I

    .line 985
    iget-boolean v0, p0, Lcom/sec/widget/ag;->j:Z

    if-eqz v0, :cond_9

    .line 987
    iget v0, p0, Lcom/sec/widget/ag;->f:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/widget/ag;->d:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sec/widget/ag;->a(F)V

    :cond_9
    move v0, v2

    .line 991
    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_1
.end method

.method c()Z
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lcom/sec/widget/ag;->t:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
