.class Lcom/sec/widget/ar;
.super Landroid/widget/ArrayAdapter;
.source "PreferenceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/preference/Preference;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/as;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/sec/widget/as;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Lcom/sec/widget/as;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/widget/as;-><init>(Lcom/sec/widget/aq;)V

    iput-object v0, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    .line 71
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 72
    invoke-direct {p0, v0}, Lcom/sec/widget/ar;->a(Landroid/preference/Preference;)V

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method private a(Landroid/preference/Preference;Lcom/sec/widget/as;)Lcom/sec/widget/as;
    .locals 1

    .prologue
    .line 139
    if-eqz p2, :cond_0

    .line 140
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/sec/widget/as;->a(Lcom/sec/widget/as;Ljava/lang/String;)Ljava/lang/String;

    .line 141
    invoke-virtual {p1}, Landroid/preference/Preference;->getLayoutResource()I

    move-result v0

    invoke-static {p2, v0}, Lcom/sec/widget/as;->a(Lcom/sec/widget/as;I)I

    .line 142
    invoke-virtual {p1}, Landroid/preference/Preference;->getWidgetLayoutResource()I

    move-result v0

    invoke-static {p2, v0}, Lcom/sec/widget/as;->b(Lcom/sec/widget/as;I)I

    .line 143
    return-object p2

    .line 139
    :cond_0
    new-instance p2, Lcom/sec/widget/as;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/sec/widget/as;-><init>(Lcom/sec/widget/aq;)V

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference;)V
    .locals 3

    .prologue
    .line 147
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/widget/ar;->a(Landroid/preference/Preference;Lcom/sec/widget/as;)Lcom/sec/widget/as;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v1

    .line 151
    if-gez v1, :cond_0

    .line 153
    mul-int/lit8 v1, v1, -0x1

    add-int/lit8 v1, v1, -0x1

    .line 154
    iget-object v2, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 156
    :cond_0
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/sec/widget/ar;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 110
    iget-object v1, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/ar;->a(Landroid/preference/Preference;Lcom/sec/widget/as;)Lcom/sec/widget/as;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    .line 112
    iget-object v0, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    invoke-static {v0, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 113
    if-gez v0, :cond_0

    .line 116
    const/4 v0, -0x1

    .line 118
    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/sec/widget/ar;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 80
    iget-object v1, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/ar;->a(Landroid/preference/Preference;Lcom/sec/widget/as;)Lcom/sec/widget/as;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    .line 84
    iget-object v1, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/widget/ar;->b:Lcom/sec/widget/as;

    invoke-static {v1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_0

    .line 85
    const/4 p2, 0x0

    .line 88
    :cond_0
    invoke-virtual {v0, p2, p3}, Landroid/preference/Preference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 2

    .prologue
    .line 128
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/widget/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 93
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/widget/ar;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 94
    :cond_0
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/widget/ar;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isSelectable()Z

    move-result v0

    goto :goto_0
.end method
