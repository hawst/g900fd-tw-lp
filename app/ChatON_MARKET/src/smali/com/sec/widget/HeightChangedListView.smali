.class public Lcom/sec/widget/HeightChangedListView;
.super Landroid/widget/ListView;
.source "HeightChangedListView.java"


# instance fields
.field private a:Lcom/sec/widget/ak;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/widget/HeightChangedListView;->getTranscriptMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 56
    :cond_0
    if-ne p1, p3, :cond_1

    if-eqz p2, :cond_1

    if-nez p4, :cond_2

    .line 72
    :cond_1
    :goto_0
    return-void

    .line 59
    :cond_2
    iget-object v1, p0, Lcom/sec/widget/HeightChangedListView;->a:Lcom/sec/widget/ak;

    if-eqz v1, :cond_1

    .line 60
    if-eq p2, p4, :cond_1

    .line 64
    iget-object v1, p0, Lcom/sec/widget/HeightChangedListView;->a:Lcom/sec/widget/ak;

    int-to-double v2, p2

    int-to-double v4, p4

    const-wide v6, 0x3ff6666666666666L    # 1.4

    mul-double/2addr v4, v6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-interface {v1, p0, v0}, Lcom/sec/widget/ak;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public setOnHeightChangedListener(Lcom/sec/widget/ak;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/widget/HeightChangedListView;->a:Lcom/sec/widget/ak;

    .line 42
    return-void
.end method
