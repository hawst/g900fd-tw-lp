.class Lcom/sec/widget/ab;
.super Ljava/lang/Object;
.source "FastScrollableExpandableListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/FastScrollableExpandableListView;


# direct methods
.method constructor <init>(Lcom/sec/widget/FastScrollableExpandableListView;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->b(Lcom/sec/widget/FastScrollableExpandableListView;)Lcom/sec/widget/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->b(Lcom/sec/widget/FastScrollableExpandableListView;)Lcom/sec/widget/ac;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    iget-object v2, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableExpandableListView;->getChildCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCount()I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/widget/ac;->a(Landroid/widget/AbsListView;III)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a(Lcom/sec/widget/FastScrollableExpandableListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a(Lcom/sec/widget/FastScrollableExpandableListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 140
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/widget/ab;->a:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-static {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a(Lcom/sec/widget/FastScrollableExpandableListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 130
    return-void
.end method
