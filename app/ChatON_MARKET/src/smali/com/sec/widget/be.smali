.class Lcom/sec/widget/be;
.super Landroid/view/View;
.source "TwIndexScrollView.java"


# instance fields
.field protected a:[Ljava/lang/String;

.field final synthetic b:Lcom/sec/widget/TwIndexScrollView;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/widget/TwIndexScrollView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1893
    iput-object p1, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    .line 1894
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1895
    invoke-direct {p0}, Lcom/sec/widget/be;->a()V

    .line 1896
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1910
    invoke-virtual {p0}, Lcom/sec/widget/be;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1911
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/widget/be;->setFocusable(Z)V

    .line 1912
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    .line 1913
    iget-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1914
    iget-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1915
    iget-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    const v2, 0x7f09027e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1916
    iget-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    const v2, 0x7f080043

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1917
    iget-object v1, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1918
    const v1, 0x7f02028b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/be;->d:Landroid/graphics/drawable/Drawable;

    .line 1920
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2014
    iput-object p1, p0, Lcom/sec/widget/be;->e:Ljava/lang/String;

    .line 2015
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/widget/be;->a:[Ljava/lang/String;

    .line 2016
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2017
    iget-object v1, p0, Lcom/sec/widget/be;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2016
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2019
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 1924
    iget-object v0, p0, Lcom/sec/widget/be;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1925
    iget-object v0, p0, Lcom/sec/widget/be;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1926
    invoke-virtual {p0}, Lcom/sec/widget/be;->getWidth()I

    move-result v0

    .line 1927
    invoke-virtual {p0}, Lcom/sec/widget/be;->getHeight()I

    move-result v2

    .line 1928
    div-int/lit8 v0, v0, 0x2

    .line 1930
    iget-object v3, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 1933
    iget v4, v3, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    .line 1936
    iget v5, v3, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-int v5, v5

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->bottom:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v3, v5

    .line 1938
    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v5, v3, 0x2

    sub-int/2addr v2, v5

    .line 1940
    iget-object v5, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v5}, Lcom/sec/widget/TwIndexScrollView;->n(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2011
    :cond_0
    :goto_0
    return-void

    .line 1945
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1955
    int-to-float v0, v0

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1965
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1967
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 1968
    iget-object v5, p0, Lcom/sec/widget/be;->a:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1967
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1971
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1973
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 1974
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 1975
    iget-object v0, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v1, v2, v5, v8}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 1977
    iget-object v0, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v1, v2, v5, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1978
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    const/4 v2, 0x0

    aget v2, v8, v2

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 1979
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const/4 v5, 0x0

    aget v5, v8, v5

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 1980
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 1981
    iget-object v0, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->o(Lcom/sec/widget/TwIndexScrollView;)I

    move-result v0

    neg-int v2, v0

    .line 1982
    iget-object v0, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->o(Lcom/sec/widget/TwIndexScrollView;)I

    move-result v0

    .line 1986
    :cond_3
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1987
    iget-object v6, p0, Lcom/sec/widget/be;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1988
    iget-object v6, p0, Lcom/sec/widget/be;->d:Landroid/graphics/drawable/Drawable;

    iget v9, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v9

    iget v9, v5, Landroid/graphics/Rect;->top:I

    neg-int v9, v9

    iget v10, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v10

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v5

    invoke-virtual {v6, v2, v9, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1990
    iget-object v0, p0, Lcom/sec/widget/be;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1995
    const/4 v0, 0x0

    int-to-float v2, v4

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1996
    iget-object v0, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->p(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/widget/be;->b:Lcom/sec/widget/TwIndexScrollView;

    invoke-static {v0}, Lcom/sec/widget/TwIndexScrollView;->q(Lcom/sec/widget/TwIndexScrollView;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1997
    iget-object v0, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    invoke-static {}, Lcom/sec/widget/TwIndexScrollView;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2001
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    const/4 v0, 0x0

    aget v0, v8, v0

    neg-float v0, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v0, v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 2002
    iget-object v0, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    invoke-static {}, Lcom/sec/widget/TwIndexScrollView;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2004
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 2005
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 2009
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 2007
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const/4 v4, 0x0

    aget v4, v8, v4

    sub-float/2addr v0, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v0, v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/widget/be;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    goto :goto_2
.end method
