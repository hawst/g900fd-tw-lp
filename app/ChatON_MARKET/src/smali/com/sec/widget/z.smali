.class Lcom/sec/widget/z;
.super Ljava/lang/Object;
.source "EditTextWithClearButton.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/sec/widget/EditTextWithClearButton;


# direct methods
.method constructor <init>(Lcom/sec/widget/EditTextWithClearButton;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/widget/z;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const v3, 0x7f0202bb

    .line 217
    iget-object v0, p0, Lcom/sec/widget/z;->a:Lcom/sec/widget/EditTextWithClearButton;

    const v1, 0x7f070147

    invoke-virtual {v0, v1}, Lcom/sec/widget/EditTextWithClearButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 218
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 219
    if-nez v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/sec/widget/z;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v1}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 225
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 221
    :cond_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/widget/z;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v1}, Lcom/sec/widget/EditTextWithClearButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 223
    iget-object v0, p0, Lcom/sec/widget/z;->a:Lcom/sec/widget/EditTextWithClearButton;

    invoke-virtual {v0}, Lcom/sec/widget/EditTextWithClearButton;->invalidate()V

    goto :goto_0
.end method
