.class public Lcom/sec/widget/TwIndexScrollView;
.super Landroid/widget/FrameLayout;
.source "TwIndexScrollView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field public static b:Z

.field private static l:I

.field private static m:I


# instance fields
.field private A:Lcom/sec/widget/bi;

.field private B:F

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Landroid/view/animation/Animation;

.field private G:Landroid/view/animation/Animation;

.field private H:Z

.field private I:Ljava/lang/Runnable;

.field private J:I

.field private final K:J

.field private final L:J

.field private M:Z

.field private N:I

.field a:I

.field private final c:Z

.field private d:F

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Landroid/graphics/Rect;

.field private i:Landroid/graphics/Paint;

.field private j:I

.field private k:Landroid/graphics/drawable/Drawable;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/bh;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:Lcom/sec/widget/ay;

.field private final x:Lcom/sec/widget/bf;

.field private y:[Ljava/lang/String;

.field private z:Lcom/sec/widget/be;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/widget/TwIndexScrollView;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 197
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 34
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->c:Z

    .line 36
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    .line 40
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->a:I

    .line 49
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->f:Z

    .line 55
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->g:Z

    .line 72
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    .line 76
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    .line 85
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->k:Landroid/graphics/drawable/Drawable;

    .line 119
    iput v3, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    .line 137
    new-instance v0, Lcom/sec/widget/bf;

    invoke-direct {v0, p0}, Lcom/sec/widget/bf;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    .line 141
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    .line 145
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    .line 150
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    .line 158
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 167
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    .line 172
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    .line 173
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    .line 174
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 175
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->J:I

    .line 177
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->K:J

    .line 181
    const-wide/16 v0, 0x15e

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->L:J

    .line 183
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    .line 186
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->N:I

    .line 198
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 199
    const v1, 0x7f090280

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    .line 200
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(Landroid/content/Context;)V

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 225
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->c:Z

    .line 36
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    .line 40
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->a:I

    .line 49
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->f:Z

    .line 55
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->g:Z

    .line 72
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    .line 76
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    .line 85
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->k:Landroid/graphics/drawable/Drawable;

    .line 119
    iput v3, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    .line 137
    new-instance v0, Lcom/sec/widget/bf;

    invoke-direct {v0, p0}, Lcom/sec/widget/bf;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    .line 141
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    .line 145
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    .line 150
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    .line 158
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 167
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    .line 172
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    .line 173
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    .line 174
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 175
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->J:I

    .line 177
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->K:J

    .line 181
    const-wide/16 v0, 0x15e

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->L:J

    .line 183
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    .line 186
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->N:I

    .line 226
    sget-object v0, Lcom/sec/chaton/am;->TwIndexView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 227
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 228
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 229
    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    .line 230
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 231
    const v2, 0x7f090280

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    .line 232
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(Landroid/content/Context;)V

    .line 233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 243
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->c:Z

    .line 36
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    .line 40
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->a:I

    .line 49
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->f:Z

    .line 55
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->g:Z

    .line 72
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    .line 76
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    .line 85
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->k:Landroid/graphics/drawable/Drawable;

    .line 119
    iput v3, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    .line 137
    new-instance v0, Lcom/sec/widget/bf;

    invoke-direct {v0, p0}, Lcom/sec/widget/bf;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    .line 141
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    .line 145
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    .line 150
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    .line 158
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 167
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    .line 172
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    .line 173
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    .line 174
    iput-boolean v3, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 175
    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->J:I

    .line 177
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->K:J

    .line 181
    const-wide/16 v0, 0x15e

    iput-wide v0, p0, Lcom/sec/widget/TwIndexScrollView;->L:J

    .line 183
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    .line 186
    iput v2, p0, Lcom/sec/widget/TwIndexScrollView;->N:I

    .line 244
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 245
    const v1, 0x7f090280

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    .line 246
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(Landroid/content/Context;)V

    .line 247
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/sec/widget/TwIndexScrollView;->m:I

    return v0
.end method

.method static synthetic a(Lcom/sec/widget/TwIndexScrollView;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/widget/TwIndexScrollView;->N:I

    return p1
.end method

.method private a(I)Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 620
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 621
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 622
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getHeight()I

    move-result v0

    .line 623
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getWidth()I

    move-result v2

    .line 624
    if-nez v0, :cond_0

    .line 625
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 627
    :cond_0
    if-nez v2, :cond_1

    .line 628
    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 631
    :cond_1
    if-nez p1, :cond_5

    .line 635
    iget-object v3, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    if-nez v3, :cond_2

    .line 636
    const/4 v0, 0x0

    .line 674
    :goto_0
    return-object v0

    .line 638
    :cond_2
    iget v3, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    iget-object v4, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    array-length v4, v4

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/sec/widget/TwIndexScrollView;->s:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 639
    iget-boolean v4, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    if-nez v4, :cond_3

    .line 640
    const v4, 0x7f090284

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/widget/TwIndexScrollView;->o:I

    .line 642
    :cond_3
    const v4, 0x7f090282

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v4, v1

    .line 645
    add-int v1, v4, v3

    .line 648
    if-eqz v0, :cond_7

    if-le v1, v0, :cond_7

    .line 651
    :goto_1
    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    if-ne v1, v5, :cond_4

    .line 653
    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->o:I

    sub-int v1, v2, v1

    move v6, v2

    move v2, v1

    move v1, v6

    .line 658
    :goto_2
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v2, v4, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    .line 659
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    goto :goto_0

    .line 655
    :cond_4
    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->o:I

    .line 656
    const/4 v2, 0x0

    goto :goto_2

    .line 661
    :cond_5
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    add-int/2addr v0, v1

    .line 664
    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    if-ne v1, v5, :cond_6

    .line 665
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    sub-int/2addr v1, v2

    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 666
    iget v1, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    sub-int v1, v0, v1

    .line 673
    :goto_3
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v1, v3, v0, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v2

    .line 674
    goto :goto_0

    .line 668
    :cond_6
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    add-int/2addr v1, v2

    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 669
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    add-int/2addr v0, v1

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const-wide/16 v5, 0x1f4

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 250
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 251
    invoke-virtual {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->setFocusable(Z)V

    .line 252
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->bringToFront()V

    .line 253
    invoke-virtual {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->setHapticFeedbackEnabled(Z)V

    .line 254
    invoke-virtual {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->setSoundEffectsEnabled(Z)V

    .line 255
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    if-nez v1, :cond_0

    .line 256
    new-instance v1, Lcom/sec/widget/be;

    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/widget/be;-><init>(Lcom/sec/widget/TwIndexScrollView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    .line 257
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 261
    :goto_0
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/widget/TwIndexScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    const v1, 0x7f090283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->o:I

    .line 263
    const v1, 0x7f090286

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    .line 264
    const v1, 0x7f090287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    .line 265
    const v1, 0x7f090288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->j:I

    .line 267
    const v1, 0x7f090281

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->s:I

    .line 269
    const v1, 0x7f080042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/sec/widget/TwIndexScrollView;->l:I

    .line 270
    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/sec/widget/TwIndexScrollView;->m:I

    .line 273
    const v1, 0x7f09027f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/widget/TwIndexScrollView;->n:I

    .line 275
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    .line 276
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    sget v2, Lcom/sec/widget/TwIndexScrollView;->l:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 279
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/widget/TwIndexScrollView;->n:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 280
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 281
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 282
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 283
    const v1, 0x7f02044a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->k:Landroid/graphics/drawable/Drawable;

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    .line 286
    const v0, 0x7f040002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    .line 287
    const v0, 0x7f040003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    .line 288
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 289
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 290
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 291
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 292
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 293
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 294
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/widget/bb;

    invoke-direct {v1, p0}, Lcom/sec/widget/bb;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 326
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/widget/bc;

    invoke-direct {v1, p0}, Lcom/sec/widget/bc;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 359
    new-instance v0, Lcom/sec/widget/bd;

    invoke-direct {v0, p0}, Lcom/sec/widget/bd;-><init>(Lcom/sec/widget/TwIndexScrollView;)V

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    .line 365
    return-void

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    invoke-virtual {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->removeView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/widget/TwIndexScrollView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 867
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    invoke-virtual {v0, p1}, Lcom/sec/widget/be;->a(Ljava/lang/String;)V

    .line 868
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    invoke-virtual {v0}, Lcom/sec/widget/be;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 873
    :goto_0
    return-void

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    invoke-virtual {v0}, Lcom/sec/widget/be;->invalidate()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 446
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 447
    if-nez p1, :cond_0

    if-ge v0, v1, :cond_2

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 449
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 450
    if-eqz v0, :cond_1

    .line 451
    const-string v1, ""

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v1, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;I)Z

    .line 459
    :cond_1
    return-void

    .line 455
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->c()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 456
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/widget/TwIndexScrollView;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    return p1
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 483
    move v0, v1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 484
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 485
    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    .line 486
    sget-object v3, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v3, v2}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/Character$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v3, v2}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/Character$UnicodeBlock;->HANGUL_JAMO:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v3, v2}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 537
    :cond_0
    :goto_1
    return v1

    .line 483
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 494
    :cond_2
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 495
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/bh;

    .line 496
    invoke-virtual {v0}, Lcom/sec/widget/bh;->c()I

    move-result v2

    .line 497
    invoke-virtual {v0}, Lcom/sec/widget/bh;->a()I

    move-result v3

    .line 504
    rem-int v4, v2, v3

    if-ne v4, v3, :cond_3

    invoke-virtual {v0}, Lcom/sec/widget/bh;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_0

    .line 508
    :cond_3
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 509
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->v:I

    if-lez v0, :cond_4

    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->v:I

    if-ge v7, v0, :cond_0

    .line 511
    :cond_4
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    if-ne v0, v8, :cond_5

    .line 515
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 516
    :goto_2
    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 517
    new-instance v2, Lcom/sec/widget/az;

    invoke-direct {v2}, Lcom/sec/widget/az;-><init>()V

    .line 518
    iput-boolean v8, v2, Lcom/sec/widget/az;->b:Z

    .line 519
    iput v0, v2, Lcom/sec/widget/az;->c:I

    .line 520
    iget-object v4, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    aget-object v4, v4, v0

    iput-object v4, v2, Lcom/sec/widget/az;->a:Ljava/lang/String;

    .line 521
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 524
    :cond_5
    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    if-nez v7, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v2, p1, v0}, Lcom/sec/widget/ay;->a(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 526
    :cond_6
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-direct {p0, v7}, Lcom/sec/widget/TwIndexScrollView;->a(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 528
    if-eqz v6, :cond_0

    .line 532
    new-instance v0, Lcom/sec/widget/bh;

    int-to-float v4, p2

    iget v5, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/widget/bh;-><init>(Lcom/sec/widget/TwIndexScrollView;Ljava/lang/String;Ljava/util/ArrayList;FILandroid/graphics/Rect;I)V

    .line 533
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v8

    .line 534
    goto/16 :goto_1

    :cond_7
    move v0, v8

    .line 524
    goto :goto_3
.end method

.method static synthetic b(Lcom/sec/widget/TwIndexScrollView;I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 546
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 547
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 549
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/widget/TwIndexScrollView;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    return v0
.end method

.method private c()I
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/widget/TwIndexScrollView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->f()V

    return-void
.end method

.method static synthetic d(Lcom/sec/widget/TwIndexScrollView;)Lcom/sec/widget/be;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    return-object v0
.end method

.method private d()Lcom/sec/widget/bh;
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 557
    if-lez v0, :cond_0

    .line 558
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/bh;

    .line 560
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/widget/TwIndexScrollView;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->h:Landroid/graphics/Rect;

    .line 613
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 879
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    invoke-virtual {v1}, Lcom/sec/widget/bf;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 881
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v1

    .line 882
    if-eqz v1, :cond_1

    .line 885
    invoke-virtual {v1}, Lcom/sec/widget/bh;->d()Lcom/sec/widget/az;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 886
    iget-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/widget/bh;->c()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    if-nez v0, :cond_2

    .line 889
    invoke-virtual {v1}, Lcom/sec/widget/bh;->d()Lcom/sec/widget/az;

    move-result-object v0

    iget v0, v0, Lcom/sec/widget/az;->c:I

    neg-int v0, v0

    .line 896
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    invoke-interface {v1, v0}, Lcom/sec/widget/bi;->a(I)V

    .line 899
    :cond_1
    return-void

    .line 891
    :cond_2
    invoke-virtual {v1}, Lcom/sec/widget/bh;->d()Lcom/sec/widget/az;

    move-result-object v0

    iget v0, v0, Lcom/sec/widget/az;->c:I

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/widget/TwIndexScrollView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->h()V

    return-void
.end method

.method static synthetic g(Lcom/sec/widget/TwIndexScrollView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 951
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 957
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    if-eqz v0, :cond_0

    .line 964
    :goto_0
    return-void

    .line 962
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 963
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->F:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 967
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    if-nez v0, :cond_0

    .line 974
    :goto_0
    return-void

    .line 972
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 973
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->f:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/widget/TwIndexScrollView;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->r:I

    return v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 977
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    .line 978
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 980
    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/animation/Transformation;->setAlpha(F)V

    .line 982
    return-void
.end method

.method static synthetic j(Lcom/sec/widget/TwIndexScrollView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/widget/TwIndexScrollView;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->N:I

    return v0
.end method

.method static synthetic l(Lcom/sec/widget/TwIndexScrollView;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->d:F

    return v0
.end method

.method static synthetic m(Lcom/sec/widget/TwIndexScrollView;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->n:I

    return v0
.end method

.method static synthetic n(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    return v0
.end method

.method static synthetic o(Lcom/sec/widget/TwIndexScrollView;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->j:I

    return v0
.end method

.method static synthetic p(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    return v0
.end method

.method static synthetic q(Lcom/sec/widget/TwIndexScrollView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    return v0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 906
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 925
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    if-eqz v0, :cond_1

    .line 947
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v7, v8

    .line 930
    :goto_1
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_3

    .line 932
    iget-object v2, p0, Lcom/sec/widget/TwIndexScrollView;->k:Landroid/graphics/drawable/Drawable;

    .line 933
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v7, v0, :cond_2

    const/16 v6, 0xff

    .line 936
    :goto_2
    sget v4, Lcom/sec/widget/TwIndexScrollView;->l:I

    .line 937
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/bh;

    iget-object v3, p0, Lcom/sec/widget/TwIndexScrollView;->i:Landroid/graphics/Paint;

    sget v5, Lcom/sec/widget/TwIndexScrollView;->m:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/widget/bh;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Paint;III)V

    .line 930
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_2
    move v6, v8

    .line 933
    goto :goto_2

    .line 940
    :cond_3
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    if-eqz v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 942
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->I:Ljava/lang/Runnable;

    const-wide/16 v1, 0x15e

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/widget/TwIndexScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 2081
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 2092
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    if-nez v0, :cond_0

    .line 2093
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    .line 2095
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2150
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2162
    iget-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-eqz v1, :cond_0

    .line 2163
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 2164
    invoke-direct {p0, v2}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    .line 2165
    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 2167
    :cond_0
    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 993
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 996
    sub-int v0, p4, p3

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/widget/TwIndexScrollView;->J:I

    if-eq v0, p2, :cond_0

    .line 997
    iput p2, p0, Lcom/sec/widget/TwIndexScrollView;->J:I

    .line 1003
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->g()V

    .line 1006
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1011
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 828
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 829
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-eqz v0, :cond_0

    .line 830
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 831
    :goto_0
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->c()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 832
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->b()V

    goto :goto_0

    .line 836
    :cond_0
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 837
    invoke-virtual {v0, p2}, Lcom/sec/widget/bh;->c(I)V

    .line 839
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 840
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 684
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 685
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 686
    iput v3, p0, Lcom/sec/widget/TwIndexScrollView;->d:F

    .line 687
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 688
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v5

    .line 689
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v2

    .line 822
    :goto_1
    return v0

    .line 703
    :pswitch_0
    if-eqz v5, :cond_3

    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/bh;

    invoke-virtual {v0}, Lcom/sec/widget/bh;->g()Landroid/graphics/Rect;

    move-result-object v0

    float-to-int v6, v4

    float-to-int v7, v3

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 719
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 720
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 721
    iput v4, p0, Lcom/sec/widget/TwIndexScrollView;->B:F

    .line 723
    invoke-static {v5, v3}, Lcom/sec/widget/bh;->a(Lcom/sec/widget/bh;F)Z

    .line 724
    invoke-virtual {v5}, Lcom/sec/widget/bh;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;)V

    .line 725
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 726
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->i()V

    .line 731
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->g()V

    goto :goto_0

    .line 727
    :cond_2
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->H:Z

    if-eqz v0, :cond_1

    .line 729
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->f()V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 733
    goto :goto_1

    .line 742
    :pswitch_1
    if-eqz v5, :cond_0

    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-eqz v0, :cond_0

    .line 750
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 751
    iget v6, p0, Lcom/sec/widget/TwIndexScrollView;->B:F

    sub-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 752
    iget v6, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    iget v7, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    int-to-float v7, v0

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    const/high16 v7, 0x3f000000    # 0.5f

    iget v8, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    .line 753
    iget v7, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    iget v8, p0, Lcom/sec/widget/TwIndexScrollView;->p:I

    add-int/2addr v7, v8

    mul-int/2addr v7, v0

    iget v8, p0, Lcom/sec/widget/TwIndexScrollView;->q:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    .line 755
    cmpl-float v6, v4, v6

    if-ltz v6, :cond_5

    iget v6, p0, Lcom/sec/widget/TwIndexScrollView;->v:I

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_5

    .line 756
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    if-nez v0, :cond_0

    .line 764
    invoke-virtual {v5}, Lcom/sec/widget/bh;->f()Ljava/lang/String;

    move-result-object v0

    .line 767
    invoke-static {v5}, Lcom/sec/widget/bh;->a(Lcom/sec/widget/bh;)F

    move-result v4

    .line 768
    float-to-int v4, v4

    invoke-direct {p0, v0, v4}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 769
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 770
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v0

    .line 771
    invoke-virtual {v0, v3}, Lcom/sec/widget/bh;->a(F)Z

    .line 775
    sput-boolean v2, Lcom/sec/widget/TwIndexScrollView;->b:Z

    .line 780
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    goto/16 :goto_0

    .line 782
    :cond_4
    iput-boolean v2, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    goto/16 :goto_0

    .line 787
    :cond_5
    cmpg-float v0, v4, v7

    if-gez v0, :cond_7

    .line 788
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 789
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->b()V

    .line 790
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->d()Lcom/sec/widget/bh;

    move-result-object v0

    .line 791
    if-eqz v0, :cond_6

    .line 792
    invoke-virtual {v0, v3}, Lcom/sec/widget/bh;->a(F)Z

    .line 793
    invoke-virtual {v0}, Lcom/sec/widget/bh;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;)V

    .line 795
    :cond_6
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->f()V

    goto/16 :goto_0

    .line 801
    :cond_7
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->C:Z

    .line 802
    invoke-virtual {v5, v3}, Lcom/sec/widget/bh;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803
    invoke-virtual {v5}, Lcom/sec/widget/bh;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Ljava/lang/String;)V

    .line 804
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->f()V

    .line 805
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    goto/16 :goto_0

    .line 813
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    if-eqz v0, :cond_0

    .line 814
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->f()V

    .line 815
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->D:Z

    .line 816
    invoke-direct {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    .line 817
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->z:Lcom/sec/widget/be;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/widget/be;->setVisibility(I)V

    .line 818
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    goto/16 :goto_0

    .line 689
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setHandlePosition(I)V
    .locals 1

    .prologue
    .line 586
    iput p1, p0, Lcom/sec/widget/TwIndexScrollView;->t:I

    .line 587
    invoke-direct {p0}, Lcom/sec/widget/TwIndexScrollView;->e()V

    .line 588
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    .line 589
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    .line 590
    return-void
.end method

.method public setIndexer(Lcom/sec/widget/ay;)V
    .locals 2

    .prologue
    .line 376
    if-nez p1, :cond_0

    .line 377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TwIndexView.setIndexer(indexer) : indexer=null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_0
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    if-eqz v0, :cond_1

    .line 380
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TwIndexView.setIndexer(indexer) :  you are not allowed to set the indexer if you already use indexScroll view under Simple scroll mode !!!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    invoke-virtual {v0, v1}, Lcom/sec/widget/ay;->b(Landroid/database/DataSetObserver;)V

    .line 385
    :cond_2
    iput-object p1, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    .line 386
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    invoke-virtual {v0}, Lcom/sec/widget/ay;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    .line 393
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    iget-object v1, p0, Lcom/sec/widget/TwIndexScrollView;->x:Lcom/sec/widget/bf;

    invoke-virtual {v0, v1}, Lcom/sec/widget/ay;->a(Landroid/database/DataSetObserver;)V

    .line 407
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    .line 408
    return-void
.end method

.method public setInvisibleIndexScroll(Z)V
    .locals 1

    .prologue
    .line 1039
    if-eqz p1, :cond_1

    .line 1040
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->setVisibility(I)V

    .line 1047
    :goto_0
    iget-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    if-eq v0, p1, :cond_0

    .line 1048
    iput-boolean p1, p0, Lcom/sec/widget/TwIndexScrollView;->e:Z

    .line 1049
    invoke-virtual {p0}, Lcom/sec/widget/TwIndexScrollView;->invalidate()V

    .line 1051
    :cond_0
    return-void

    .line 1043
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/widget/TwIndexScrollView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOnIndexSelectedListener(Lcom/sec/widget/bi;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/sec/widget/TwIndexScrollView;->A:Lcom/sec/widget/bi;

    .line 1020
    return-void
.end method

.method public setSimpleIndexHandleChar([Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 417
    if-nez p1, :cond_0

    .line 418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TwIndexView.setIndexHandleChar(handleChar) "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/TwIndexScrollView;->w:Lcom/sec/widget/ay;

    if-eqz v0, :cond_1

    .line 421
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TwIndexView.setSimpleIndexHandleChar(String [] handleChar) :  you must not set the indexer to use simple index handle mode !!!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_1
    iput-boolean v1, p0, Lcom/sec/widget/TwIndexScrollView;->E:Z

    .line 425
    iput-object p1, p0, Lcom/sec/widget/TwIndexScrollView;->y:[Ljava/lang/String;

    .line 427
    invoke-direct {p0, v1}, Lcom/sec/widget/TwIndexScrollView;->a(Z)V

    .line 428
    return-void
.end method

.method public setSimpleIndexHandleCharForContact([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/TwIndexScrollView;->M:Z

    .line 438
    invoke-virtual {p0, p1}, Lcom/sec/widget/TwIndexScrollView;->setSimpleIndexHandleChar([Ljava/lang/String;)V

    .line 440
    return-void
.end method

.method public setSubscrollLimit(I)V
    .locals 1

    .prologue
    .line 570
    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    .line 571
    iput p1, p0, Lcom/sec/widget/TwIndexScrollView;->v:I

    .line 573
    :cond_0
    return-void
.end method

.method public setTopPadding(I)V
    .locals 0

    .prologue
    .line 2076
    iput p1, p0, Lcom/sec/widget/TwIndexScrollView;->a:I

    .line 2077
    return-void
.end method

.method public setmIsFavoriteContactMode(Z)V
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/sec/widget/TwIndexScrollView;->g:Z

    .line 59
    return-void
.end method
