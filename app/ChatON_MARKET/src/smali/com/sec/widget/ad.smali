.class public Lcom/sec/widget/ad;
.super Ljava/lang/Object;
.source "FastScrollableExpandableListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field b:J

.field final synthetic c:Lcom/sec/widget/ac;


# direct methods
.method public constructor <init>(Lcom/sec/widget/ac;)V
    .locals 0

    .prologue
    .line 629
    iput-object p1, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 637
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/sec/widget/ad;->b:J

    .line 638
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/widget/ad;->a:J

    .line 639
    iget-object v0, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/widget/ac;->a(I)V

    .line 640
    return-void
.end method

.method b()I
    .locals 8

    .prologue
    const-wide/16 v6, 0xd0

    .line 643
    iget-object v0, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    invoke-virtual {v0}, Lcom/sec/widget/ac;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 644
    const/16 v0, 0xd0

    .line 653
    :goto_0
    return v0

    .line 647
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 648
    iget-wide v2, p0, Lcom/sec/widget/ad;->a:J

    iget-wide v4, p0, Lcom/sec/widget/ad;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 649
    const/4 v0, 0x0

    goto :goto_0

    .line 651
    :cond_1
    iget-wide v2, p0, Lcom/sec/widget/ad;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, Lcom/sec/widget/ad;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    invoke-virtual {v0}, Lcom/sec/widget/ac;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 659
    invoke-virtual {p0}, Lcom/sec/widget/ad;->a()V

    .line 668
    :goto_0
    return-void

    .line 663
    :cond_0
    invoke-virtual {p0}, Lcom/sec/widget/ad;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 664
    iget-object v0, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    invoke-static {v0}, Lcom/sec/widget/ac;->a(Lcom/sec/widget/ac;)Landroid/widget/ExpandableListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->invalidate()V

    goto :goto_0

    .line 666
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/ad;->c:Lcom/sec/widget/ac;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/ac;->a(I)V

    goto :goto_0
.end method
