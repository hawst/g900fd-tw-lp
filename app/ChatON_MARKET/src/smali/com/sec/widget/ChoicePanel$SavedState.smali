.class Lcom/sec/widget/ChoicePanel$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ChoicePanel.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/widget/ChoicePanel$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final KEY_ID_LIST:Ljava/lang/String;

.field private final KEY_TEXT_LIST:Ljava/lang/String;

.field private idList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private textList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/sec/widget/b;

    invoke-direct {v0}, Lcom/sec/widget/b;-><init>()V

    sput-object v0, Lcom/sec/widget/ChoicePanel$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 124
    const-string v0, "com.sec.chaton.widget.ChoicePanel.idList"

    iput-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->KEY_ID_LIST:Ljava/lang/String;

    .line 125
    const-string v0, "com.sec.chaton.widget.ChoicePanel.textList"

    iput-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->KEY_TEXT_LIST:Ljava/lang/String;

    .line 145
    if-eqz p1, :cond_0

    .line 147
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 149
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V

    .line 151
    const-string v1, "com.sec.chaton.widget.ChoicePanel.idList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/widget/ChoicePanel$SavedState;->idList:Ljava/util/ArrayList;

    .line 152
    const-string v1, "com.sec.chaton.widget.ChoicePanel.textList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequenceArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->textList:Ljava/util/ArrayList;

    .line 154
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/widget/a;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/sec/widget/ChoicePanel$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 124
    const-string v0, "com.sec.chaton.widget.ChoicePanel.idList"

    iput-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->KEY_ID_LIST:Ljava/lang/String;

    .line 125
    const-string v0, "com.sec.chaton.widget.ChoicePanel.textList"

    iput-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->KEY_TEXT_LIST:Ljava/lang/String;

    .line 135
    iput-object p2, p0, Lcom/sec/widget/ChoicePanel$SavedState;->idList:Ljava/util/ArrayList;

    .line 136
    iput-object p3, p0, Lcom/sec/widget/ChoicePanel$SavedState;->textList:Ljava/util/ArrayList;

    .line 137
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/ChoicePanel$SavedState;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->idList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/widget/ChoicePanel$SavedState;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/widget/ChoicePanel$SavedState;->textList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChoicePanel.SavedState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " idList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/widget/ChoicePanel$SavedState;->idList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " textList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/widget/ChoicePanel$SavedState;->textList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 160
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 161
    const-string v1, "com.sec.chaton.widget.ChoicePanel.idList"

    iget-object v2, p0, Lcom/sec/widget/ChoicePanel$SavedState;->idList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 162
    const-string v1, "com.sec.chaton.widget.ChoicePanel.textList"

    iget-object v2, p0, Lcom/sec/widget/ChoicePanel$SavedState;->textList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequenceArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 164
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 165
    return-void
.end method
