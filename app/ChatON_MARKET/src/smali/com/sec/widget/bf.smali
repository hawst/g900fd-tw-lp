.class Lcom/sec/widget/bf;
.super Landroid/database/DataSetObserver;
.source "TwIndexScrollView.java"


# instance fields
.field a:Z

.field b:Ljava/lang/Runnable;

.field final synthetic c:Lcom/sec/widget/TwIndexScrollView;

.field private final d:J


# direct methods
.method constructor <init>(Lcom/sec/widget/TwIndexScrollView;)V
    .locals 2

    .prologue
    .line 2026
    iput-object p1, p0, Lcom/sec/widget/bf;->c:Lcom/sec/widget/TwIndexScrollView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 2031
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/sec/widget/bf;->d:J

    .line 2032
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/widget/bf;->a:Z

    .line 2056
    new-instance v0, Lcom/sec/widget/bg;

    invoke-direct {v0, p0}, Lcom/sec/widget/bg;-><init>(Lcom/sec/widget/bf;)V

    iput-object v0, p0, Lcom/sec/widget/bf;->b:Ljava/lang/Runnable;

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2051
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/bf;->a:Z

    .line 2052
    iget-object v0, p0, Lcom/sec/widget/bf;->c:Lcom/sec/widget/TwIndexScrollView;

    iget-object v1, p0, Lcom/sec/widget/bf;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/sec/widget/TwIndexScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2053
    iget-object v0, p0, Lcom/sec/widget/bf;->c:Lcom/sec/widget/TwIndexScrollView;

    iget-object v1, p0, Lcom/sec/widget/bf;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/widget/TwIndexScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2054
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 2047
    iget-boolean v0, p0, Lcom/sec/widget/bf;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChanged()V
    .locals 0

    .prologue
    .line 2036
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 2037
    invoke-direct {p0}, Lcom/sec/widget/bf;->b()V

    .line 2038
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 2042
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 2043
    invoke-direct {p0}, Lcom/sec/widget/bf;->b()V

    .line 2044
    return-void
.end method
