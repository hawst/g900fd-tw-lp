.class public Lcom/sec/widget/FastScrollableListView;
.super Landroid/widget/ListView;
.source "FastScrollableListView.java"


# instance fields
.field private a:Lcom/sec/widget/ag;

.field private b:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/FastScrollableListView;)Lcom/sec/widget/ag;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/widget/FastScrollableListView;)Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/widget/ListView;->draw(Landroid/graphics/Canvas;)V

    .line 132
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0, p1}, Lcom/sec/widget/ag;->a(Landroid/graphics/Canvas;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0}, Lcom/sec/widget/ag;->c()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/widget/FastScrollableListView;->setVerticalScrollBarEnabled(Z)V

    .line 148
    return-void

    .line 144
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1083
    invoke-super {p0, p1}, Landroid/widget/ListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1084
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0}, Lcom/sec/widget/ag;->b()V

    .line 1087
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0, p1}, Lcom/sec/widget/ag;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 157
    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 90
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/widget/ag;->a(IIII)V

    .line 96
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 101
    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableListView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 107
    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableListView;->isClickable()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableListView;->isLongClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v1, :cond_3

    .line 113
    iget-object v1, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v1, p1}, Lcom/sec/widget/ag;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 115
    if-nez v1, :cond_0

    .line 123
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setFastScrollEnabled(Z)V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 61
    if-eqz p1, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/widget/ag;

    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/widget/ag;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;)V

    iput-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    .line 81
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/sec/widget/FastScrollableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 83
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    invoke-virtual {v0}, Lcom/sec/widget/ag;->b()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/FastScrollableListView;->a:Lcom/sec/widget/ag;

    goto :goto_0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/widget/FastScrollableListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 174
    new-instance v0, Lcom/sec/widget/af;

    invoke-direct {v0, p0}, Lcom/sec/widget/af;-><init>(Lcom/sec/widget/FastScrollableListView;)V

    .line 198
    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 200
    return-void
.end method
