.class public Lcom/sec/widget/FastScrollableExpandableListView;
.super Landroid/widget/ExpandableListView;
.source "FastScrollableExpandableListView.java"


# instance fields
.field public a:Z

.field private b:Lcom/sec/widget/ac;

.field private c:Landroid/widget/AbsListView$OnScrollListener;

.field private d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    iput-boolean v1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->a:Z

    .line 673
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    .line 674
    iput-boolean v1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/FastScrollableExpandableListView;)Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->c:Landroid/widget/AbsListView$OnScrollListener;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/widget/FastScrollableExpandableListView;)Lcom/sec/widget/ac;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 679
    iget-boolean v1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    if-eqz v1, :cond_0

    .line 680
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 682
    :cond_0
    :goto_0
    return v0

    .line 680
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 687
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    .line 688
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 689
    iget-object v3, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 690
    if-nez v3, :cond_0

    move v2, v1

    .line 693
    :cond_0
    iget-boolean v4, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    if-eqz v4, :cond_1

    .line 694
    if-nez v3, :cond_1

    .line 688
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 697
    :cond_2
    return v2
.end method

.method public b()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 733
    iget-boolean v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->draw(Landroid/graphics/Canvas;)V

    .line 102
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0, p1}, Lcom/sec/widget/ac;->a(Landroid/graphics/Canvas;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0}, Lcom/sec/widget/ac;->c()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->setVerticalScrollBarEnabled(Z)V

    .line 110
    return-void

    .line 108
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 807
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 808
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0}, Lcom/sec/widget/ac;->b()V

    .line 811
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0, p1}, Lcom/sec/widget/ac;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 116
    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x1

    .line 120
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 792
    check-cast p1, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;

    .line 794
    invoke-virtual {p1}, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ExpandableListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 796
    iget-object v0, p1, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;->checkedChildSet:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p1, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;->checkedChildSet:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    .line 798
    iget-boolean v0, p1, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;->childCheckInverse:Z

    iput-boolean v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    .line 800
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 786
    invoke-super {p0}, Landroid/widget/ExpandableListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 787
    new-instance v1, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;

    iget-object v2, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    iget-boolean v3, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/widget/FastScrollableExpandableListView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/util/HashSet;Z)V

    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ExpandableListView;->onSizeChanged(IIII)V

    .line 77
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/widget/ac;->a(IIII)V

    .line 80
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 84
    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableExpandableListView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 87
    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableExpandableListView;->isClickable()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableExpandableListView;->isLongClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :cond_2
    iget-object v1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v1, :cond_3

    .line 91
    iget-object v1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v1, p1}, Lcom/sec/widget/ac;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 92
    if-nez v1, :cond_0

    .line 96
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setChildCheckInverse(Z)V
    .locals 0

    .prologue
    .line 729
    iput-boolean p1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->e:Z

    .line 730
    return-void
.end method

.method public setChildChecked(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 706
    if-eqz p2, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 711
    :goto_0
    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableExpandableListView;->invalidateViews()V

    .line 712
    return-void

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setFastScrollEnabled(Z)V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ExpandableListView;->setFastScrollEnabled(Z)V

    .line 60
    if-eqz p1, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/sec/widget/ac;

    invoke-virtual {p0}, Lcom/sec/widget/FastScrollableExpandableListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/widget/ac;-><init>(Landroid/content/Context;Landroid/widget/ExpandableListView;)V

    iput-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    .line 70
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->c:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 71
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    invoke-virtual {v0}, Lcom/sec/widget/ac;->b()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/FastScrollableExpandableListView;->b:Lcom/sec/widget/ac;

    goto :goto_0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/widget/FastScrollableExpandableListView;->c:Landroid/widget/AbsListView$OnScrollListener;

    .line 126
    new-instance v0, Lcom/sec/widget/ab;

    invoke-direct {v0, p0}, Lcom/sec/widget/ab;-><init>(Lcom/sec/widget/FastScrollableExpandableListView;)V

    .line 143
    invoke-super {p0, v0}, Landroid/widget/ExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 144
    return-void
.end method
