.class Lcom/sec/widget/ac;
.super Ljava/lang/Object;
.source "FastScrollableExpandableListView.java"


# static fields
.field private static a:I


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/graphics/RectF;

.field private h:I

.field private i:Landroid/widget/ExpandableListView;

.field private j:Z

.field private k:I

.field private l:Landroid/graphics/Paint;

.field private m:I

.field private n:I

.field private o:Z

.field private p:[Ljava/lang/Object;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Lcom/sec/widget/ad;

.field private t:I

.field private u:Landroid/os/Handler;

.field private v:Landroid/widget/ExpandableListAdapter;

.field private w:Landroid/widget/SectionIndexer;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x1

    sput v0, Lcom/sec/widget/ac;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ExpandableListView;)V
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/widget/ac;->n:I

    .line 186
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ac;->u:Landroid/os/Handler;

    .line 194
    iput-object p2, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    .line 195
    invoke-direct {p0, p1}, Lcom/sec/widget/ac;->a(Landroid/content/Context;)V

    .line 196
    return-void
.end method

.method static synthetic a(Lcom/sec/widget/ac;)Landroid/widget/ExpandableListView;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method private a(F)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 429
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getCount()I

    move-result v0

    .line 430
    iput-boolean v2, p0, Lcom/sec/widget/ac;->j:Z

    .line 432
    iget-object v3, p0, Lcom/sec/widget/ac;->p:[Ljava/lang/Object;

    .line 434
    if-eqz v3, :cond_2

    array-length v4, v3

    if-le v4, v1, :cond_2

    .line 435
    array-length v4, v3

    .line 436
    int-to-float v0, v4

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 437
    if-lt v0, v4, :cond_0

    .line 438
    add-int/lit8 v0, v4, -0x1

    .line 530
    :cond_0
    iget-object v4, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget v5, p0, Lcom/sec/widget/ac;->m:I

    add-int/2addr v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/ExpandableListView;->setSelectedGroup(I)V

    .line 538
    :goto_0
    if-ltz v0, :cond_4

    .line 539
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/widget/ac;->q:Ljava/lang/String;

    .line 540
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, v1, :cond_1

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-eq v4, v5, :cond_3

    :cond_1
    array-length v3, v3

    if-ge v0, v3, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/widget/ac;->r:Z

    .line 544
    :goto_2
    return-void

    .line 532
    :cond_2
    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 533
    iget-object v4, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget-object v5, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget v6, p0, Lcom/sec/widget/ac;->m:I

    add-int/2addr v6, v0

    invoke-static {v6}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v5

    invoke-virtual {v4, v5, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 534
    iget-object v4, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget v5, p0, Lcom/sec/widget/ac;->m:I

    add-int/2addr v0, v5

    invoke-virtual {v4, v0}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 535
    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 540
    goto :goto_1

    .line 542
    :cond_4
    iput-boolean v2, p0, Lcom/sec/widget/ac;->r:Z

    goto :goto_2
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 243
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 244
    const v1, 0x7f02028c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/sec/widget/ac;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 246
    const v1, 0x7f02028b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ac;->c:Landroid/graphics/drawable/Drawable;

    .line 248
    iput-boolean v2, p0, Lcom/sec/widget/ac;->j:Z

    .line 250
    invoke-direct {p0}, Lcom/sec/widget/ac;->e()V

    .line 252
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090303

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ac;->h:I

    .line 253
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ac;->g:Landroid/graphics/RectF;

    .line 254
    new-instance v0, Lcom/sec/widget/ad;

    invoke-direct {v0, p0}, Lcom/sec/widget/ad;-><init>(Lcom/sec/widget/ac;)V

    iput-object v0, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    .line 255
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    .line 256
    iget-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 257
    iget-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 258
    iget-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/widget/ac;->h:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 266
    const v0, -0x137cde

    .line 268
    iget-object v1, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 272
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1, v5, v5}, Lcom/sec/widget/ac;->a(IIII)V

    .line 276
    :cond_0
    iput v5, p0, Lcom/sec/widget/ac;->t:I

    .line 277
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 235
    iput-object p2, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    .line 236
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090304

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ac;->e:I

    .line 237
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090305

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/widget/ac;->d:I

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/widget/ac;->x:Z

    .line 239
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/sec/widget/ac;->e:I

    sub-int v2, v0, v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/widget/ac;->d:I

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 232
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v1

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/widget/ac;->w:Landroid/widget/SectionIndexer;

    .line 393
    instance-of v0, v1, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 394
    check-cast v0, Landroid/widget/SectionIndexer;

    iput-object v0, p0, Lcom/sec/widget/ac;->w:Landroid/widget/SectionIndexer;

    .line 395
    iput-object v1, p0, Lcom/sec/widget/ac;->v:Landroid/widget/ExpandableListAdapter;

    .line 396
    iget-object v0, p0, Lcom/sec/widget/ac;->w:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/ac;->p:[Ljava/lang/Object;

    .line 426
    :cond_0
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    .line 548
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 549
    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 550
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 551
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    return v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 199
    packed-switch p1, :pswitch_data_0

    .line 217
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/widget/ac;->t:I

    .line 218
    return-void

    .line 201
    :pswitch_1
    iget-object v0, p0, Lcom/sec/widget/ac;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 202
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->invalidate()V

    goto :goto_0

    .line 205
    :pswitch_2
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 206
    invoke-direct {p0}, Lcom/sec/widget/ac;->d()V

    .line 210
    :cond_0
    :pswitch_3
    iget-object v0, p0, Lcom/sec/widget/ac;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 213
    :pswitch_4
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    .line 214
    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget v2, p0, Lcom/sec/widget/ac;->e:I

    sub-int v2, v0, v2

    iget v3, p0, Lcom/sec/widget/ac;->f:I

    iget v4, p0, Lcom/sec/widget/ac;->f:I

    iget v5, p0, Lcom/sec/widget/ac;->d:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/widget/ExpandableListView;->invalidate(IIII)V

    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method a(IIII)V
    .locals 5

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/sec/widget/ac;->e:I

    sub-int v1, p1, v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/widget/ac;->d:I

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/sec/widget/ac;->g:Landroid/graphics/RectF;

    .line 340
    iget v1, p0, Lcom/sec/widget/ac;->h:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 341
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/sec/widget/ac;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 342
    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 343
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/sec/widget/ac;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 344
    iget-object v1, p0, Lcom/sec/widget/ac;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 345
    iget-object v1, p0, Lcom/sec/widget/ac;->c:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 347
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 289
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-nez v0, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    iget v1, p0, Lcom/sec/widget/ac;->f:I

    .line 295
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v2

    .line 296
    iget-object v3, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    .line 298
    const/4 v0, -0x1

    .line 299
    iget v4, p0, Lcom/sec/widget/ac;->t:I

    if-ne v4, v9, :cond_3

    .line 300
    invoke-virtual {v3}, Lcom/sec/widget/ad;->b()I

    move-result v0

    .line 301
    const/16 v3, 0x68

    if-ge v0, v3, :cond_2

    .line 304
    :cond_2
    iget v3, p0, Lcom/sec/widget/ac;->e:I

    mul-int/2addr v3, v0

    div-int/lit16 v3, v3, 0xd0

    sub-int v3, v2, v3

    .line 305
    iget-object v4, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/sec/widget/ac;->d:I

    invoke-virtual {v4, v3, v8, v2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 306
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/widget/ac;->x:Z

    .line 309
    :cond_3
    int-to-float v3, v1

    invoke-virtual {p1, v7, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 310
    iget-object v3, p0, Lcom/sec/widget/ac;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 311
    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {p1, v7, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 314
    iget v3, p0, Lcom/sec/widget/ac;->t:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/sec/widget/ac;->r:Z

    if-eqz v3, :cond_4

    .line 315
    iget-object v0, p0, Lcom/sec/widget/ac;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/widget/ac;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 320
    iget-object v0, p0, Lcom/sec/widget/ac;->l:Landroid/graphics/Paint;

    .line 321
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v1

    .line 322
    iget-object v2, p0, Lcom/sec/widget/ac;->g:Landroid/graphics/RectF;

    .line 323
    iget-object v3, p0, Lcom/sec/widget/ac;->q:Ljava/lang/String;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget v5, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    div-float/2addr v4, v6

    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v5

    div-float/2addr v2, v6

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v5

    div-float/2addr v5, v6

    add-float/2addr v2, v5

    float-to-double v5, v2

    float-to-double v1, v1

    const-wide/high16 v7, 0x3ff8000000000000L    # 1.5

    div-double/2addr v1, v7

    sub-double v1, v5, v1

    double-to-float v1, v1

    invoke-virtual {p1, v3, v4, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 326
    :cond_4
    iget v3, p0, Lcom/sec/widget/ac;->t:I

    if-ne v3, v9, :cond_0

    .line 327
    if-nez v0, :cond_5

    .line 328
    invoke-virtual {p0, v8}, Lcom/sec/widget/ac;->a(I)V

    goto :goto_0

    .line 330
    :cond_5
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    iget v3, p0, Lcom/sec/widget/ac;->e:I

    sub-int v3, v2, v3

    iget v4, p0, Lcom/sec/widget/ac;->d:I

    add-int/2addr v4, v1

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/widget/ExpandableListView;->invalidate(IIII)V

    goto/16 :goto_0
.end method

.method a(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    iget v0, p0, Lcom/sec/widget/ac;->n:I

    if-eq v0, p4, :cond_0

    if-lez p3, :cond_0

    .line 352
    iput p4, p0, Lcom/sec/widget/ac;->n:I

    .line 353
    iget v0, p0, Lcom/sec/widget/ac;->n:I

    div-int/2addr v0, p3

    sget v3, Lcom/sec/widget/ac;->a:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/widget/ac;->o:Z

    .line 355
    :cond_0
    iget-boolean v0, p0, Lcom/sec/widget/ac;->o:Z

    if-nez v0, :cond_3

    .line 356
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-eqz v0, :cond_1

    .line 357
    invoke-virtual {p0, v2}, Lcom/sec/widget/ac;->a(I)V

    .line 377
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 353
    goto :goto_0

    .line 361
    :cond_3
    sub-int v0, p4, p3

    if-lez v0, :cond_4

    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-eq v0, v4, :cond_4

    .line 362
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getHeight()I

    move-result v0

    iget v3, p0, Lcom/sec/widget/ac;->d:I

    sub-int/2addr v0, v3

    mul-int/2addr v0, p2

    sub-int v3, p4, p3

    div-int/2addr v0, v3

    iput v0, p0, Lcom/sec/widget/ac;->f:I

    .line 363
    iget-boolean v0, p0, Lcom/sec/widget/ac;->x:Z

    if-eqz v0, :cond_4

    .line 364
    invoke-direct {p0}, Lcom/sec/widget/ac;->d()V

    .line 365
    iput-boolean v2, p0, Lcom/sec/widget/ac;->x:Z

    .line 368
    :cond_4
    iput-boolean v1, p0, Lcom/sec/widget/ac;->j:Z

    .line 369
    iget v0, p0, Lcom/sec/widget/ac;->k:I

    if-eq p2, v0, :cond_1

    .line 372
    iput p2, p0, Lcom/sec/widget/ac;->k:I

    .line 373
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-eq v0, v4, :cond_1

    .line 374
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/widget/ac;->a(I)V

    .line 375
    iget-object v0, p0, Lcom/sec/widget/ac;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method a(FF)Z
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/widget/ac;->e:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/widget/ac;->f:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/widget/ac;->f:I

    iget v1, p0, Lcom/sec/widget/ac;->d:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 554
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 555
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/widget/ac;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/widget/ac;->a(I)V

    .line 557
    const/4 v0, 0x1

    .line 560
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/widget/ac;->a(I)V

    .line 281
    return-void
.end method

.method b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 564
    iget v1, p0, Lcom/sec/widget/ac;->t:I

    if-nez v1, :cond_1

    .line 622
    :cond_0
    :goto_0
    return v0

    .line 568
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 570
    if-nez v1, :cond_4

    .line 571
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/sec/widget/ac;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 572
    invoke-virtual {p0, v4}, Lcom/sec/widget/ac;->a(I)V

    .line 573
    iget-object v0, p0, Lcom/sec/widget/ac;->v:Landroid/widget/ExpandableListAdapter;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_2

    .line 574
    invoke-direct {p0}, Lcom/sec/widget/ac;->e()V

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_3

    .line 577
    iget-object v0, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 582
    :cond_3
    invoke-direct {p0}, Lcom/sec/widget/ac;->f()V

    move v0, v2

    .line 583
    goto :goto_0

    .line 585
    :cond_4
    if-ne v1, v2, :cond_6

    .line 586
    iget v1, p0, Lcom/sec/widget/ac;->t:I

    if-ne v1, v4, :cond_0

    .line 587
    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    if-eqz v1, :cond_5

    .line 591
    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 595
    :cond_5
    invoke-virtual {p0, v5}, Lcom/sec/widget/ac;->a(I)V

    .line 596
    iget-object v0, p0, Lcom/sec/widget/ac;->u:Landroid/os/Handler;

    .line 597
    iget-object v1, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 598
    iget-object v1, p0, Lcom/sec/widget/ac;->s:Lcom/sec/widget/ad;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v2

    .line 599
    goto :goto_0

    .line 601
    :cond_6
    if-ne v1, v5, :cond_0

    .line 602
    iget v1, p0, Lcom/sec/widget/ac;->t:I

    if-ne v1, v4, :cond_0

    .line 603
    iget-object v1, p0, Lcom/sec/widget/ac;->i:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getHeight()I

    move-result v3

    .line 605
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget v4, p0, Lcom/sec/widget/ac;->d:I

    sub-int/2addr v1, v4

    add-int/lit8 v1, v1, 0xa

    .line 606
    if-gez v1, :cond_7

    .line 611
    :goto_1
    iget v1, p0, Lcom/sec/widget/ac;->f:I

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v5, :cond_8

    move v0, v2

    .line 612
    goto :goto_0

    .line 608
    :cond_7
    iget v0, p0, Lcom/sec/widget/ac;->d:I

    add-int/2addr v0, v1

    if-le v0, v3, :cond_a

    .line 609
    iget v0, p0, Lcom/sec/widget/ac;->d:I

    sub-int v0, v3, v0

    goto :goto_1

    .line 614
    :cond_8
    iput v0, p0, Lcom/sec/widget/ac;->f:I

    .line 616
    iget-boolean v0, p0, Lcom/sec/widget/ac;->j:Z

    if-eqz v0, :cond_9

    .line 617
    iget v0, p0, Lcom/sec/widget/ac;->f:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/widget/ac;->d:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sec/widget/ac;->a(F)V

    :cond_9
    move v0, v2

    .line 619
    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_1
.end method

.method c()Z
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/sec/widget/ac;->t:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
