.class public Lcom/sec/widget/u;
.super Ljava/lang/Object;
.source "DropPanelMenuImpl.java"

# interfaces
.implements Landroid/view/Menu;


# static fields
.field private static final a:[I


# instance fields
.field private b:Lcom/sec/widget/DropPanelMenu;

.field private c:Landroid/content/Context;

.field private d:Landroid/content/res/Resources;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/v;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/ContextMenu$ContextMenuInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/widget/u;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/sec/widget/u;->c:Landroid/content/Context;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    .line 65
    return-void
.end method

.method private static a(Ljava/util/ArrayList;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/widget/v;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 116
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 117
    invoke-virtual {v0}, Lcom/sec/widget/v;->a()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 118
    add-int/lit8 v0, v1, 0x1

    .line 122
    :goto_1
    return v0

    .line 115
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 7

    .prologue
    .line 99
    invoke-static {p3}, Lcom/sec/widget/u;->c(I)I

    move-result v5

    .line 101
    new-instance v0, Lcom/sec/widget/v;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/widget/v;-><init>(Lcom/sec/widget/u;IIIILjava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lcom/sec/widget/u;->f:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/widget/u;->f:Landroid/view/ContextMenu$ContextMenuInfo;

    invoke-virtual {v0, v1}, Lcom/sec/widget/v;->a(Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-static {v2, v5}, Lcom/sec/widget/u;->a(Ljava/util/ArrayList;I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 109
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 111
    return-object v0
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 264
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 270
    if-eqz p2, :cond_0

    .line 271
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/u;->a(Z)V

    goto :goto_0
.end method

.method private static c(I)I
    .locals 2

    .prologue
    .line 138
    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    .line 140
    if-ltz v0, :cond_0

    sget-object v1, Lcom/sec/widget/u;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 141
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    sget-object v1, Lcom/sec/widget/u;->a:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p0

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 3

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/sec/widget/u;->size()I

    move-result v2

    .line 202
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 204
    invoke-virtual {v0}, Lcom/sec/widget/v;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 209
    :goto_1
    return v0

    .line 202
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 209
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(II)I
    .locals 3

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/sec/widget/u;->size()I

    move-result v2

    .line 219
    if-gez p2, :cond_0

    .line 220
    const/4 p2, 0x0

    :cond_0
    move v1, p2

    .line 223
    :goto_0
    if-ge v1, v2, :cond_2

    .line 224
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 226
    invoke-virtual {v0}, Lcom/sec/widget/v;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 231
    :goto_1
    return v0

    .line 223
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/widget/u;->c:Landroid/content/Context;

    return-object v0
.end method

.method public a(Lcom/sec/widget/DropPanelMenu;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/widget/u;->b:Lcom/sec/widget/DropPanelMenu;

    .line 77
    return-void
.end method

.method a(Lcom/sec/widget/v;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/u;->a(Z)V

    .line 93
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/widget/u;->b:Lcom/sec/widget/DropPanelMenu;

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/widget/u;->b:Lcom/sec/widget/DropPanelMenu;

    invoke-virtual {v0, p1}, Lcom/sec/widget/DropPanelMenu;->setItemChanged(Z)V

    .line 87
    :cond_0
    return-void
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1, v1, v0}, Lcom/sec/widget/u;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/widget/u;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/widget/u;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 149
    invoke-direct {p0, v0, v0, v0, p1}, Lcom/sec/widget/u;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/sec/widget/u;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/widget/u;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/sec/widget/u;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/widget/u;->a(II)I

    move-result v0

    return v0
.end method

.method b()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/widget/u;->d:Landroid/content/res/Resources;

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 279
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/u;->a(Z)V

    .line 280
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/sec/widget/u;->size()I

    move-result v2

    .line 346
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 347
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 348
    invoke-virtual {v0}, Lcom/sec/widget/v;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 359
    :cond_0
    :goto_1
    return-object v0

    .line 350
    :cond_1
    invoke-virtual {v0}, Lcom/sec/widget/v;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 351
    invoke-virtual {v0}, Lcom/sec/widget/v;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 353
    if-nez v0, :cond_0

    .line 346
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 359
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 331
    invoke-virtual {p0}, Lcom/sec/widget/u;->size()I

    move-result v3

    move v2, v1

    .line 333
    :goto_0
    if-ge v2, v3, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 335
    invoke-virtual {v0}, Lcom/sec/widget/v;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    const/4 v0, 0x1

    .line 340
    :goto_1
    return v0

    .line 333
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 340
    goto :goto_1
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public performIdentifierAction(II)Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x0

    return v0
.end method

.method public removeGroup(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 236
    invoke-virtual {p0, p1}, Lcom/sec/widget/u;->b(I)I

    move-result v3

    .line 238
    if-ltz v3, :cond_1

    .line 239
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v4, v0, v3

    move v0, v1

    .line 241
    :goto_0
    add-int/lit8 v2, v0, 0x1

    if-ge v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    invoke-virtual {v0}, Lcom/sec/widget/v;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 243
    invoke-direct {p0, v3, v1}, Lcom/sec/widget/u;->a(IZ)V

    move v0, v2

    goto :goto_0

    .line 247
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/widget/u;->a(Z)V

    .line 249
    :cond_1
    return-void
.end method

.method public removeItem(I)V
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/sec/widget/u;->a(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/widget/u;->a(IZ)V

    .line 197
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 286
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 288
    invoke-virtual {v0}, Lcom/sec/widget/v;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 289
    invoke-virtual {v0, p3}, Lcom/sec/widget/v;->c(Z)V

    .line 290
    invoke-virtual {v0, p2}, Lcom/sec/widget/v;->setCheckable(Z)Landroid/view/MenuItem;

    .line 286
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 293
    :cond_1
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 4

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 321
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 322
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 323
    invoke-virtual {v0}, Lcom/sec/widget/v;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 324
    invoke-virtual {v0, p2}, Lcom/sec/widget/v;->setEnabled(Z)Landroid/view/MenuItem;

    .line 321
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 327
    :cond_1
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 297
    iget-object v2, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 303
    :goto_0
    if-ge v3, v4, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/v;

    .line 305
    invoke-virtual {v0}, Lcom/sec/widget/v;->getGroupId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 306
    invoke-virtual {v0, p2}, Lcom/sec/widget/v;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 303
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 312
    :cond_0
    if-eqz v2, :cond_1

    .line 313
    invoke-virtual {p0, v1}, Lcom/sec/widget/u;->a(Z)V

    .line 315
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public setQwertyMode(Z)V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
