.class public Lcom/sec/widget/at;
.super Landroid/text/LoginFilter$PasswordFilterGMail;
.source "RemoveCharsFilter.java"


# instance fields
.field private a:[C

.field private b:Lcom/sec/widget/aw;


# direct methods
.method private constructor <init>([CLcom/sec/widget/aw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/text/LoginFilter$PasswordFilterGMail;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/widget/at;->a:[C

    .line 24
    iput-object p2, p0, Lcom/sec/widget/at;->b:Lcom/sec/widget/aw;

    .line 25
    return-void
.end method

.method public static a(Landroid/widget/EditText;[CI)V
    .locals 5

    .prologue
    .line 53
    invoke-virtual {p0}, Landroid/widget/EditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    .line 54
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-array v2, v0, [Landroid/text/InputFilter;

    .line 55
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 56
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_1

    .line 57
    new-instance v3, Lcom/sec/widget/at;

    new-instance v4, Lcom/sec/widget/au;

    invoke-direct {v4, p0, p2}, Lcom/sec/widget/au;-><init>(Landroid/widget/EditText;I)V

    invoke-direct {v3, p1, v4}, Lcom/sec/widget/at;-><init>([CLcom/sec/widget/aw;)V

    aput-object v3, v2, v0

    .line 55
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 54
    :cond_0
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_1
    aget-object v3, v1, v0

    aput-object v3, v2, v0

    goto :goto_2

    .line 67
    :cond_2
    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 68
    return-void
.end method

.method public static a(Lcom/sec/chaton/widget/ClearableEditText;[CI)V
    .locals 5

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ClearableEditText;->b()[Landroid/text/InputFilter;

    move-result-object v1

    .line 72
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-array v2, v0, [Landroid/text/InputFilter;

    .line 73
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 74
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_1

    .line 75
    new-instance v3, Lcom/sec/widget/at;

    new-instance v4, Lcom/sec/widget/av;

    invoke-direct {v4, p0, p2}, Lcom/sec/widget/av;-><init>(Lcom/sec/chaton/widget/ClearableEditText;I)V

    invoke-direct {v3, p1, v4}, Lcom/sec/widget/at;-><init>([CLcom/sec/widget/aw;)V

    aput-object v3, v2, v0

    .line 73
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 72
    :cond_0
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_1
    aget-object v3, v1, v0

    aput-object v3, v2, v0

    goto :goto_2

    .line 85
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 86
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 29
    invoke-super/range {p0 .. p6}, Landroid/text/LoginFilter$PasswordFilterGMail;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v1, p0, Lcom/sec/widget/at;->b:Lcom/sec/widget/aw;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/sec/widget/at;->b:Lcom/sec/widget/aw;

    invoke-interface {v1}, Lcom/sec/widget/aw;->a()V

    .line 35
    :cond_0
    return-object v0
.end method

.method public isAllowed(C)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 40
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/widget/at;->a:[C

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 41
    iget-object v2, p0, Lcom/sec/widget/at;->a:[C

    aget-char v2, v2, v0

    if-ne v2, p1, :cond_0

    .line 45
    :goto_1
    return v1

    .line 40
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
