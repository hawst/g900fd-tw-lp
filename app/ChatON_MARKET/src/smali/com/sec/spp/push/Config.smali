.class public Lcom/sec/spp/push/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field public static final ACTION_LOG_SERVICE_NOTIFICATION:Ljava/lang/String; = "com.sec.spp.action.LOG_SERVICE_NOTIFICATION"

.field public static final ACTION_LOG_SERVICE_REPLY:Ljava/lang/String; = "com.sec.spp.action.LOG_SERVICE_REPLY"

.field public static final ACTION_LOG_SERVICE_REQUEST:Ljava/lang/String; = "com.sec.spp.action.LOG_SERVICE_REQUEST"

.field public static final APP_SYNC_ALARM_INTENT_EXTRA:Ljava/lang/String; = "android.intent.extra.ALARM_TARGET_TIME"

.field public static final CHATON_PACKAGE_NAME:Ljava/lang/String; = "com.sec.chaton"

.field public static final COLUMN_CURRENT_VERSION:Ljava/lang/String; = "current_version"

.field public static final COLUMN_LATEST_VERSION:Ljava/lang/String; = "latest_version"

.field public static final COLUMN_NEED_UPDATE:Ljava/lang/String; = "need_update"

.field public static final COLUMN_PORT:Ljava/lang/String; = "port"

.field public static final DEFAULT_DEREGISTRATION_ID:Ljava/lang/String; = "com.sec.spp.DeRegistrationFail"

.field public static final DEFAULT_REGISTRATION_ID:Ljava/lang/String; = "com.sec.spp.RegistrationFail"

.field public static final EXTRA_APPID:Ljava/lang/String; = "appId"

.field public static final EXTRA_APP_PACKAGE_NAME:Ljava/lang/String; = "com.sec.spp.intent.extra.APP_PACKAGE_NAME"

.field public static final EXTRA_ERROR:Ljava/lang/String; = "Error"

.field public static final EXTRA_ERROR_CODE:Ljava/lang/String; = "ErrorCode"

.field public static final EXTRA_ERROR_MESSAGE:Ljava/lang/String; = "ErrorMessage"

.field public static final EXTRA_LOG_DATA:Ljava/lang/String; = "com.sec.spp.intent.extra.LOG_DATA"

.field public static final EXTRA_LOG_ID:Ljava/lang/String; = "com.sec.spp.intent.extra.LOG_ID"

.field public static final EXTRA_LOG_MAX_SIZE:Ljava/lang/String; = "com.sec.spp.intent.extra.LOG_MAX_SIZE"

.field public static final EXTRA_LOG_PREFIX:Ljava/lang/String; = "com.sec.spp.intent.extra.LOG_PREFIX"

.field public static final EXTRA_LOG_UNLIMITED_SENDING:Ljava/lang/String; = "com.sec.spp.intent.extra.LOG_SENDER_UNLIMITED"

.field public static final EXTRA_PUSH_STATUS:Ljava/lang/String; = "com.sec.spp.Status"

.field public static final EXTRA_REGID:Ljava/lang/String; = "RegistrationID"

.field public static final EXTRA_REQTYPE:Ljava/lang/String; = "reqType"

.field public static final EXTRA_RESULT_CODE:Ljava/lang/String; = "com.sec.spp.intent.extra.RESULT_CODE"

.field public static final EXTRA_USERDATA:Ljava/lang/String; = "userdata"

.field public static final FLAG_INCLUDE_STOPPED_PACKAGES:I = 0x20

.field public static final KEYVALUE_PACKAGENAME:Ljava/lang/String; = "packageName="

.field public static final KEYVALUE_SPLIT:Ljava/lang/String; = ";"

.field public static final LOG_MAX_ROW_SIZE:I = 0x40

.field public static final LOG_MAX_SIZE_DEFAULT:I = 0x400

.field public static final NOTIFICATION_ACK_RESULT_ACTION:Ljava/lang/String; = "com.sec.spp.NotificationAckResultAction"

.field public static final NOTIFICATION_INTENT_ACK:Ljava/lang/String; = "ack"

.field public static final NOTIFICATION_INTENT_ACK_RESULT:Ljava/lang/String; = "ackResult"

.field public static final NOTIFICATION_INTENT_APPID:Ljava/lang/String; = "appId"

.field public static final NOTIFICATION_INTENT_APP_DATA:Ljava/lang/String; = "appData"

.field public static final NOTIFICATION_INTENT_CONNECTION_TERM:Ljava/lang/String; = "connectionTerm"

.field public static final NOTIFICATION_INTENT_MSG:Ljava/lang/String; = "msg"

.field public static final NOTIFICATION_INTENT_NOTIID:Ljava/lang/String; = "notificationId"

.field public static final NOTIFICATION_INTENT_SENDER:Ljava/lang/String; = "sender"

.field public static final NOTIFICATION_INTENT_SESSION_INFO:Ljava/lang/String; = "sessionInfo"

.field public static final NOTIFICATION_INTENT_TIMESTAMP:Ljava/lang/String; = "timeStamp"

.field public static final PERMISSION_LOG_SERVICE_RECEIVER:Ljava/lang/String; = "com.sec.spp.permission.LOG_SERVICE_RECEIVER"

.field public static final PERMISSION_PUSH_SERVICE_PROVIDER:Ljava/lang/String; = "com.sec.spp.permission.PUSH_SERVICE_PROVIDER"

.field public static final PROVIDER_AUTHORITY:Ljava/lang/String; = "com.sec.spp.provider"

.field public static final PROVIDER_PATH_VERSION_INFO:Ljava/lang/String; = "version_info"

.field public static final PROVIDER_PATH_WIFI_PORT:Ljava/lang/String; = "wifi_port"

.field public static final PUSH_DEREGISTRATION_FAIL:I = 0x3

.field public static final PUSH_DEREGISTRATION_SUCCESS:I = 0x2

.field public static final PUSH_DUPLICATION_DEVICEID_ERROR:Ljava/lang/String; = "com.sec.spp.push.DuplicationDeviceIdError"

.field public static final PUSH_REGISTRATION_CHANGED_ACTION:Ljava/lang/String; = "com.sec.spp.RegistrationChangedAction"

.field public static final PUSH_REGISTRATION_FAIL:I = 0x1

.field public static final PUSH_REGISTRATION_SUCCESS:I = 0x0

.field public static final PUSH_REQ_TYPE_ACTIVATE:I = 0x65

.field public static final PUSH_REQ_TYPE_DEACTIVATE:I = 0x66

.field public static final PUSH_REQ_TYPE_DEFAULT:I = 0x0

.field public static final PUSH_REQ_TYPE_DEREGISTRATION:I = 0x2

.field public static final PUSH_REQ_TYPE_GET_TERMS:I = 0x69

.field public static final PUSH_REQ_TYPE_MAX:I = 0x3

.field public static final PUSH_REQ_TYPE_REGISTRATION:I = 0x1

.field public static final PUSH_REQ_TYPE_SEND_LOG_DATA:I = 0x68

.field public static final PUSH_REQ_TYPE_TURN_TO_SEND:I = 0x67

.field public static final PUSH_SERVICE_REQUEST:Ljava/lang/String; = "com.sec.spp.action.SPP_REQUEST"

.field public static final PUSH_SERVICE_STOP_ACTION:Ljava/lang/String; = "PushServiceStopAction"

.field public static final PUSH_VERSION_CHECK:Ljava/lang/String; = "com.sec.spp.push.VersionCheck"

.field public static final SERVICE_ABNORMALLY_STOPPED_ACTION:Ljava/lang/String; = "com.sec.spp.ServiceAbnormallyStoppedAction"

.field public static final SERVICE_DATA_DELETED_ACTION:Ljava/lang/String; = "com.sec.spp.ServiceDataDeletedAction"

.field public static final SS_ACCOUNT_PACKAGE_NAME:Ljava/lang/String; = "com.sec.sso"

.field public static final WIFI_PORT_CHANGE_NOTI_ACTION:Ljava/lang/String; = "com.sec.spp.WifiPortChangeNotiAction"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getService(I)Lcom/sec/spp/push/Config$SppService;
    .locals 1

    .prologue
    const/16 v0, 0x65

    .line 73
    if-lez p0, :cond_0

    if-ge p0, v0, :cond_0

    .line 74
    sget-object v0, Lcom/sec/spp/push/Config$SppService;->SPP_PUSH_SERVICE:Lcom/sec/spp/push/Config$SppService;

    .line 78
    :goto_0
    return-object v0

    .line 75
    :cond_0
    if-lt p0, v0, :cond_1

    const/4 v0, 0x3

    if-ge p0, v0, :cond_1

    .line 76
    sget-object v0, Lcom/sec/spp/push/Config$SppService;->SPP_LOG_SERVICE:Lcom/sec/spp/push/Config$SppService;

    goto :goto_0

    .line 78
    :cond_1
    sget-object v0, Lcom/sec/spp/push/Config$SppService;->SPP_UNKNOWN_SERVICE:Lcom/sec/spp/push/Config$SppService;

    goto :goto_0
.end method
