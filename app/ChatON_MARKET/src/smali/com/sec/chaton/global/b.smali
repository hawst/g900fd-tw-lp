.class Lcom/sec/chaton/global/b;
.super Ljava/lang/Object;
.source "GlobalApplication.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/chaton/global/GlobalApplication;


# direct methods
.method constructor <init>(Lcom/sec/chaton/global/GlobalApplication;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/chaton/global/b;->a:Lcom/sec/chaton/global/GlobalApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 181
    const-string v0, "onServiceConnected : SPP"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/global/b;->a:Lcom/sec/chaton/global/GlobalApplication;

    invoke-static {p2}, Lcom/sec/chaton/push/f;->a(Landroid/os/IBinder;)Lcom/sec/chaton/push/e;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/global/GlobalApplication;->a(Lcom/sec/chaton/global/GlobalApplication;Lcom/sec/chaton/push/e;)Lcom/sec/chaton/push/e;

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/global/b;->a:Lcom/sec/chaton/global/GlobalApplication;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/chaton/global/GlobalApplication;->a(Lcom/sec/chaton/global/GlobalApplication;I)I

    .line 184
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 174
    const-string v0, "OnServiceDisConnected : SPP"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/global/b;->a:Lcom/sec/chaton/global/GlobalApplication;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/global/GlobalApplication;->a(Lcom/sec/chaton/global/GlobalApplication;I)I

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/global/b;->a:Lcom/sec/chaton/global/GlobalApplication;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/global/GlobalApplication;->a(Lcom/sec/chaton/global/GlobalApplication;Lcom/sec/chaton/push/e;)Lcom/sec/chaton/push/e;

    .line 177
    return-void
.end method
