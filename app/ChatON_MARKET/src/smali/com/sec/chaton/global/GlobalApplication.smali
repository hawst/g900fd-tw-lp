.class public Lcom/sec/chaton/global/GlobalApplication;
.super Lcom/sec/chaton/push/PushClientApplication;
.source "GlobalApplication.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Z

.field public static d:Ljava/lang/String;

.field public static e:Lcom/sec/chaton/m;

.field public static g:Z

.field private static h:I

.field private static i:I

.field private static j:Lcom/sec/chaton/global/GlobalApplication;

.field private static k:Lcom/sec/chaton/util/q;

.field private static n:Z


# instance fields
.field f:Ljava/util/concurrent/locks/Lock;

.field private l:I

.field private m:Lcom/sec/chaton/push/e;

.field private o:Landroid/content/ServiceConnection;

.field private p:Landroid/os/Handler;

.field private final q:Landroid/os/Debug$MemoryInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 83
    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->j:Lcom/sec/chaton/global/GlobalApplication;

    .line 87
    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->k:Lcom/sec/chaton/util/q;

    .line 99
    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    .line 111
    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->b:Ljava/lang/String;

    .line 112
    sput-boolean v1, Lcom/sec/chaton/global/GlobalApplication;->c:Z

    .line 113
    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->d:Ljava/lang/String;

    .line 118
    sput-boolean v1, Lcom/sec/chaton/global/GlobalApplication;->n:Z

    .line 768
    sput-boolean v1, Lcom/sec/chaton/global/GlobalApplication;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/push/PushClientApplication;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/global/GlobalApplication;->l:I

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->m:Lcom/sec/chaton/push/e;

    .line 119
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->f:Ljava/util/concurrent/locks/Lock;

    .line 171
    new-instance v0, Lcom/sec/chaton/global/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/global/b;-><init>(Lcom/sec/chaton/global/GlobalApplication;)V

    iput-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->o:Landroid/content/ServiceConnection;

    .line 673
    new-instance v0, Lcom/sec/chaton/global/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/global/d;-><init>(Lcom/sec/chaton/global/GlobalApplication;)V

    iput-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->p:Landroid/os/Handler;

    .line 682
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/global/GlobalApplication;I)I
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/chaton/global/GlobalApplication;->l:I

    return p1
.end method

.method public static a()Landroid/app/Application;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->j:Lcom/sec/chaton/global/GlobalApplication;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/global/GlobalApplication;Lcom/sec/chaton/push/e;)Lcom/sec/chaton/push/e;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/global/GlobalApplication;->m:Lcom/sec/chaton/push/e;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 549
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 550
    const-string v1, "finish"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 553
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 554
    const-string v1, "instanceOf Activity"

    const-class v2, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 577
    return-void

    .line 557
    :cond_0
    const-string v1, "No instanceOf Activity"

    const-class v2, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/global/GlobalApplication;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/global/GlobalApplication;->n()V

    return-void
.end method

.method public static b()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->j:Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v0}, Lcom/sec/chaton/global/GlobalApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 581
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 582
    const-string v1, "finish"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 585
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 586
    const-string v1, "instanceOf Activity"

    const-class v2, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 595
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 596
    return-void

    .line 589
    :cond_0
    const-string v1, "No instanceOf Activity"

    const-class v2, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static c()Lcom/sec/chaton/util/q;
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->k:Lcom/sec/chaton/util/q;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/sec/chaton/util/q;

    invoke-direct {v0}, Lcom/sec/chaton/util/q;-><init>()V

    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->k:Lcom/sec/chaton/util/q;

    .line 165
    :cond_0
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->k:Lcom/sec/chaton/util/q;

    return-object v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 616
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 617
    const-string v1, "callRestart"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 618
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x20000000

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 621
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 622
    return-void
.end method

.method public static d()V
    .locals 6

    .prologue
    const/16 v5, 0x65

    const/16 v4, 0x20

    const/16 v3, 0xc

    .line 513
    const-string v0, "activePushClient()"

    const-class v1, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.LOG_SERVICE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 516
    const-string v1, "com.sec.spp.intent.extra.APP_PACKAGE_NAME"

    const-string v2, "com.sec.chaton"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 517
    const-string v1, "reqType"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 519
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v3, :cond_0

    .line 520
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 523
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.spp.permission.LOG_SERVICE_RECEIVER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 525
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.LOG_SERVICE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526
    const-string v1, "com.sec.spp.intent.extra.APP_PACKAGE_NAME"

    const-string v2, "com.sec.chaton.error"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    const-string v1, "reqType"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 529
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v3, :cond_1

    .line 530
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 533
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.spp.permission.LOG_SERVICE_RECEIVER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 534
    return-void
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 630
    const-string v0, "tablet_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631
    const/4 v0, 0x1

    .line 633
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 739
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 740
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 742
    if-eqz v0, :cond_0

    .line 744
    const-string v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 748
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 785
    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v2

    move v0, v1

    .line 786
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 787
    aget v3, v2, v0

    invoke-static {v3}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/InputDevice;->getKeyboardType()I

    move-result v3

    .line 788
    const/16 v4, 0x101

    if-ne v3, v4, :cond_1

    .line 789
    const/4 v1, 0x1

    .line 792
    :cond_0
    return v1

    .line 786
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private m()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 486
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    .line 488
    const-string v0, "registerLogCollectorAlarm()"

    const-class v2, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 492
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 493
    const/high16 v3, 0x8000000

    invoke-static {v1, v4, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 496
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 498
    const/16 v1, 0xb

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 499
    const/16 v1, 0xc

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 500
    const/16 v1, 0xd

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 502
    const/4 v1, 0x0

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    :goto_0
    return-void

    .line 504
    :catch_0
    move-exception v0

    .line 505
    invoke-virtual {v0}, Lorg/apache/http/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method private n()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x400

    .line 685
    iget-object v0, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 686
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v0

    div-long/2addr v0, v4

    .line 687
    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v2

    div-long/2addr v2, v4

    .line 689
    const-string v4, "memory_info"

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/chaton/global/GlobalApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 691
    const-string v5, "max_total_pss"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 692
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cur:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    invoke-virtual {v7}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "(dalvik="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v7, v7, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " + native="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v7, v7, Landroid/os/Debug$MemoryInfo;->nativePss:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " + other="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v7, v7, Landroid/os/Debug$MemoryInfo;->otherPss:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), Max:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / heapAllocSize="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "DumpMemory"

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v6, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    invoke-virtual {v6}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 694
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 695
    const-string v5, "max_total_pss"

    iget-object v6, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    invoke-virtual {v6}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 696
    const-string v5, "max_dalvik_pss"

    iget-object v6, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v6, v6, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 697
    const-string v5, "max_native_pss"

    iget-object v6, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v6, v6, Landroid/os/Debug$MemoryInfo;->nativePss:I

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 698
    const-string v5, "max_other_pss"

    iget-object v6, p0, Lcom/sec/chaton/global/GlobalApplication;->q:Landroid/os/Debug$MemoryInfo;

    iget v6, v6, Landroid/os/Debug$MemoryInfo;->otherPss:I

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 699
    const-string v5, "max_heap_alloc_size"

    invoke-interface {v4, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 700
    const-string v0, "max_heap_size"

    invoke-interface {v4, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 701
    const-string v0, "max_dump_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v4, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 702
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 703
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 705
    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 715
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_version"

    const-string v2, "0.0.0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 717
    const-string v1, "0.0.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 718
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/chaton/c/a;->e:Z

    .line 723
    :goto_0
    sget-object v1, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 724
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "old version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " new version : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatONConst"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/c/a;->d:Z

    .line 732
    :goto_1
    return-void

    .line 720
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/chaton/c/a;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 729
    :catch_0
    move-exception v0

    .line 730
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatONConst"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 727
    :cond_1
    :try_start_1
    const-string v0, "ChatON version is the latest"

    const-string v1, "ChatONConst"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 343
    invoke-super {p0}, Lcom/sec/chaton/push/PushClientApplication;->onCreate()V

    .line 344
    sput-object p0, Lcom/sec/chaton/global/GlobalApplication;->j:Lcom/sec/chaton/global/GlobalApplication;

    .line 346
    invoke-static {}, Lcom/sec/chaton/global/a;->a()V

    .line 348
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/global/GlobalApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 349
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    sput v1, Lcom/sec/chaton/global/GlobalApplication;->h:I

    .line 350
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/sec/chaton/global/GlobalApplication;->i:I

    .line 352
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "auto_backup_on"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->j:Lcom/sec/chaton/global/GlobalApplication;

    invoke-static {v0, v4}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    .line 357
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0, v0}, Lcom/sec/chaton/global/GlobalApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 361
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 362
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 363
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 364
    new-instance v1, Lcom/sec/chaton/receiver/ScreenReceiver;

    invoke-direct {v1}, Lcom/sec/chaton/receiver/ScreenReceiver;-><init>()V

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/global/GlobalApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 369
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 370
    sget-object v1, Lcom/sec/chaton/receiver/AlarmReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 371
    sget-object v1, Lcom/sec/chaton/receiver/AlarmReceiver;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 372
    new-instance v1, Lcom/sec/chaton/receiver/AlarmReceiver;

    invoke-direct {v1}, Lcom/sec/chaton/receiver/AlarmReceiver;-><init>()V

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/global/GlobalApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 377
    invoke-static {}, Lcom/sec/chaton/j/ao;->f()V

    .line 380
    invoke-static {}, Lcom/sec/chaton/d/a/di;->a()Lcom/sec/chaton/d/a/di;

    .line 391
    invoke-static {}, Lcom/sec/chaton/util/y;->a()V

    .line 395
    invoke-direct {p0}, Lcom/sec/chaton/global/GlobalApplication;->o()V

    .line 397
    invoke-static {}, Lcom/sec/chaton/d/o;->a()V

    .line 401
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgsend/x;->k:Lcom/sec/chaton/msgsend/x;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    .line 408
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v5, :cond_3

    .line 410
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "|"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_version"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 412
    invoke-static {v0}, Lcom/sec/common/util/log/collector/c;->a(Ljava/lang/String;)V

    .line 415
    invoke-static {}, Lcom/sec/common/util/log/collector/c;->a()Z

    move-result v0

    .line 418
    invoke-static {}, Lcom/sec/common/util/log/collector/c;->c()V

    .line 420
    if-nez v0, :cond_2

    .line 421
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->d()V

    .line 425
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 427
    new-instance v1, Lcom/sec/chaton/global/c;

    invoke-direct {v1, p0, v0}, Lcom/sec/chaton/global/c;-><init>(Lcom/sec/chaton/global/GlobalApplication;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 478
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 479
    invoke-direct {p0}, Lcom/sec/chaton/global/GlobalApplication;->m()V

    .line 483
    :cond_3
    return-void
.end method
