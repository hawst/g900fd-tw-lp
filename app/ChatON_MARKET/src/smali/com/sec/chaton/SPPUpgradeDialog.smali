.class public Lcom/sec/chaton/SPPUpgradeDialog;
.super Landroid/app/Activity;
.source "SPPUpgradeDialog.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/common/a/d;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    const-class v0, Lcom/sec/chaton/SPPUpgradeDialog;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 49
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "showAlertDailog"

    iget-object v1, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b029a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/ao;

    invoke-direct {v2, p0}, Lcom/sec/chaton/ao;-><init>(Lcom/sec/chaton/SPPUpgradeDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/an;

    invoke-direct {v2, p0}, Lcom/sec/chaton/an;-><init>(Lcom/sec/chaton/SPPUpgradeDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 82
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 109
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "showPasswordLockActivity"

    iget-object v1, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v1}, Lcom/sec/chaton/SPPUpgradeDialog;->startActivity(Landroid/content/Intent;)V

    .line 119
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "onCreate"

    iget-object v1, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_0
    iput-object p0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->a:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->a()V

    .line 46
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 100
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/SPPUpgradeDialog;->b:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 106
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 88
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "onResume"

    iget-object v1, p0, Lcom/sec/chaton/SPPUpgradeDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/SPPUpgradeDialog;->b()V

    .line 92
    return-void
.end method
