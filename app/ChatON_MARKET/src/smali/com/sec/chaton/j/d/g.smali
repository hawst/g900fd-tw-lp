.class public Lcom/sec/chaton/j/d/g;
.super Lorg/jboss/netty/channel/SimpleChannelHandler;
.source "TcpClientChannelHandler.java"


# instance fields
.field private a:Lcom/sec/chaton/j/ak;

.field private b:Lorg/jboss/netty/bootstrap/ClientBootstrap;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Lorg/jboss/netty/channel/group/ChannelGroup;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/j/ak;Lorg/jboss/netty/bootstrap/ClientBootstrap;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/jboss/netty/channel/SimpleChannelHandler;-><init>()V

    .line 43
    new-instance v0, Lorg/jboss/netty/channel/group/DefaultChannelGroup;

    const-string v1, "ChatOnClient"

    invoke-direct {v0, v1}, Lorg/jboss/netty/channel/group/DefaultChannelGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    .line 46
    iput-object p1, p0, Lcom/sec/chaton/j/d/g;->a:Lcom/sec/chaton/j/ak;

    .line 47
    iput-object p2, p0, Lcom/sec/chaton/j/d/g;->b:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/j/d/g;->d:I

    .line 50
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/j/ao;)J
    .locals 9

    .prologue
    .line 183
    const-wide/16 v0, -0x1

    .line 184
    invoke-virtual {p0}, Lcom/sec/chaton/j/d/g;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {p1}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v6

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {v0, p1}, Lorg/jboss/netty/channel/group/ChannelGroup;->write(Ljava/lang/Object;)Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    move-result-object v8

    .line 189
    invoke-virtual {p1}, Lcom/sec/chaton/j/ao;->a()Ljava/lang/String;

    .line 190
    invoke-virtual {p1}, Lcom/sec/chaton/j/ao;->b()I

    move-result v4

    .line 191
    invoke-virtual {p1}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v2

    .line 192
    new-instance v0, Lcom/sec/chaton/j/d/h;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/j/d/h;-><init>(Lcom/sec/chaton/j/d/g;JILcom/sec/chaton/j/ao;)V

    invoke-interface {v8, v0}, Lorg/jboss/netty/channel/group/ChannelGroupFuture;->addListener(Lorg/jboss/netty/channel/group/ChannelGroupFutureListener;)V

    move-wide v0, v6

    .line 233
    :cond_0
    return-wide v0
.end method

.method public a()Lorg/jboss/netty/channel/group/ChannelGroupFuture;
    .locals 1

    .prologue
    .line 85
    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/d/g;->a(I)Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lorg/jboss/netty/channel/group/ChannelGroupFuture;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {v0}, Lorg/jboss/netty/channel/group/ChannelGroup;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cause : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iput p1, p0, Lcom/sec/chaton/j/d/g;->d:I

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {v0}, Lorg/jboss/netty/channel/group/ChannelGroup;->close()Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/group/ChannelGroupFuture;->awaitUninterruptibly()Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {v1}, Lorg/jboss/netty/channel/group/ChannelGroup;->clear()V

    .line 96
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 105
    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {v1}, Lorg/jboss/netty/channel/group/ChannelGroup;->size()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :cond_0
    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 322
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->b:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    invoke-virtual {v0}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/j/d/j;

    invoke-interface {v0, v1}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/Class;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/d/j;

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v0}, Lcom/sec/chaton/j/d/j;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public channelClosed(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V
    .locals 3

    .prologue
    .line 136
    invoke-super {p0, p1, p2}, Lorg/jboss/netty/channel/SimpleChannelHandler;->channelClosed(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->b:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    invoke-virtual {v0}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/j/d/j;

    invoke-interface {v0, v1}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/Class;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/d/j;

    .line 144
    if-eqz v0, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    const-string v2, "Connection reset by peer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/d/j;->a(I)V

    .line 154
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/j/d/g;->d:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_0
    :goto_1
    invoke-static {p1}, Lorg/jboss/netty/channel/Channels;->fireChannelClosed(Lorg/jboss/netty/channel/ChannelHandlerContext;)V

    .line 163
    invoke-static {p1}, Lorg/jboss/netty/channel/Channels;->fireChannelDisconnected(Lorg/jboss/netty/channel/ChannelHandlerContext;)V

    .line 168
    return-void

    .line 148
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/sec/chaton/j/d/g;->d:I

    if-eqz v1, :cond_2

    .line 149
    iget v1, p0, Lcom/sec/chaton/j/d/g;->d:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/d/j;->a(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    .line 158
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 152
    :cond_2
    const/16 v1, 0x16

    :try_start_2
    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/d/j;->a(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public channelConnected(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0, p1, p2}, Lorg/jboss/netty/channel/SimpleChannelHandler;->channelConnected(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V

    .line 339
    return-void
.end method

.method public channelDisconnected(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V
    .locals 0

    .prologue
    .line 344
    invoke-super {p0, p1, p2}, Lorg/jboss/netty/channel/SimpleChannelHandler;->channelDisconnected(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V

    .line 349
    return-void
.end method

.method public channelOpen(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ChannelStateEvent;)V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/j/d/g;->a()Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/j/d/g;->e:Lorg/jboss/netty/channel/group/ChannelGroup;

    invoke-interface {p2}, Lorg/jboss/netty/channel/ChannelStateEvent;->getChannel()Lorg/jboss/netty/channel/Channel;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/jboss/netty/channel/group/ChannelGroup;->add(Ljava/lang/Object;)Z

    .line 129
    return-void
.end method

.method public exceptionCaught(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/ExceptionEvent;)V
    .locals 2

    .prologue
    .line 54
    invoke-interface {p2}, Lorg/jboss/netty/channel/ExceptionEvent;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    .line 56
    instance-of v1, v0, Ljava/net/ConnectException;

    if-eqz v1, :cond_1

    .line 57
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConnectException exceptionCaught(ChannelHandlerContext, ExceptionEvent): Failed to connect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/jboss/netty/channel/ChannelHandlerContext;->getChannel()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->close()Lorg/jboss/netty/channel/ChannelFuture;

    .line 77
    return-void

    .line 60
    :cond_1
    instance-of v1, v0, Ljava/nio/channels/UnresolvedAddressException;

    if-eqz v1, :cond_2

    .line 61
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnresolvedAddressException exceptionCaught(ChannelHandlerContext, ExceptionEvent): Failed to connect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_2
    instance-of v1, v0, Ljava/net/SocketException;

    if-eqz v1, :cond_3

    .line 65
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SocketException exceptionCaught(ChannelHandlerContext, ExceptionEvent): Failed to connect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public messageReceived(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/MessageEvent;)V
    .locals 5

    .prologue
    .line 264
    invoke-interface {p2}, Lorg/jboss/netty/channel/MessageEvent;->getMessage()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    .line 265
    if-nez v0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/j/d/g;->a:Lcom/sec/chaton/j/ak;

    invoke-static {v1, v0}, Lcom/sec/chaton/j/p;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 282
    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->e()Lcom/sec/chaton/j/q;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301
    :cond_1
    invoke-interface {p1}, Lorg/jboss/netty/channel/ChannelHandlerContext;->getChannel()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getRemoteAddress()Ljava/net/SocketAddress;

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    const-string v0, "ch@t[c <~~ s]received= "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "MsgID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/jboss/netty/channel/MessageEvent;->getMessage()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "API type : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/jboss/netty/channel/MessageEvent;->getMessage()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeComplete(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/WriteCompletionEvent;)V
    .locals 0

    .prologue
    .line 354
    invoke-super {p0, p1, p2}, Lorg/jboss/netty/channel/SimpleChannelHandler;->writeComplete(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/WriteCompletionEvent;)V

    .line 359
    return-void
.end method

.method public writeRequested(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/MessageEvent;)V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0, p1, p2}, Lorg/jboss/netty/channel/SimpleChannelHandler;->writeRequested(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/MessageEvent;)V

    .line 244
    invoke-interface {p1}, Lorg/jboss/netty/channel/ChannelHandlerContext;->getChannel()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getRemoteAddress()Ljava/net/SocketAddress;

    .line 252
    return-void
.end method
