.class Lcom/sec/chaton/j/d/h;
.super Ljava/lang/Object;
.source "TcpClientChannelHandler.java"

# interfaces
.implements Lorg/jboss/netty/channel/group/ChannelGroupFutureListener;


# instance fields
.field final synthetic a:J

.field final synthetic b:I

.field final synthetic c:Lcom/sec/chaton/j/ao;

.field final synthetic d:Lcom/sec/chaton/j/d/g;


# direct methods
.method constructor <init>(Lcom/sec/chaton/j/d/g;JILcom/sec/chaton/j/ao;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/chaton/j/d/h;->d:Lcom/sec/chaton/j/d/g;

    iput-wide p2, p0, Lcom/sec/chaton/j/d/h;->a:J

    iput p4, p0, Lcom/sec/chaton/j/d/h;->b:I

    iput-object p5, p0, Lcom/sec/chaton/j/d/h;->c:Lcom/sec/chaton/j/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public operationComplete(Lorg/jboss/netty/channel/group/ChannelGroupFuture;)V
    .locals 6

    .prologue
    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    const-string v1, "wifi_80_port"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/common/util/i;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "wifi_port"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "primary_message_port"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 201
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 202
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "wifi_port"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "primary_message_wifi_port"

    const/16 v5, 0x50

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 206
    :cond_0
    const-string v1, "ch@t[c ~~> s]Write Completed= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isSuccess : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lorg/jboss/netty/channel/group/ChannelGroupFuture;->isCompleteSuccess()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MsgID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/d/h;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "API type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/j/d/h;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/h;->d:Lcom/sec/chaton/j/d/g;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-interface {p1}, Lorg/jboss/netty/channel/group/ChannelGroupFuture;->isCompleteFailure()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    const-string v0, "TCPClientChannelHandler.writeChannel() - isComplete fail. try tcp error callback"

    const-string v1, "[TCPClientChannelHandler]"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/j/d/h;->c:Lcom/sec/chaton/j/ao;

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/sec/chaton/j/d/h;->c:Lcom/sec/chaton/j/ao;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V

    .line 221
    :cond_1
    return-void
.end method
