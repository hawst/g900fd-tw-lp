.class public Lcom/sec/chaton/j/ao;
.super Ljava/lang/Object;
.source "TcpEnvelope.java"


# static fields
.field private static h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:I

.field protected c:Lcom/google/protobuf/GeneratedMessageLite;

.field protected d:J

.field protected e:Lcom/sec/chaton/j/q;

.field protected f:J

.field protected g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/j/ao;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/chaton/j/ap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/j/ao;->b:I

    .line 37
    iput-object v2, p0, Lcom/sec/chaton/j/ao;->c:Lcom/google/protobuf/GeneratedMessageLite;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/j/ao;->d:J

    .line 40
    iput-object v2, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    .line 164
    iget-object v0, p1, Lcom/sec/chaton/j/ap;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 166
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    .line 168
    :cond_0
    iget v0, p1, Lcom/sec/chaton/j/ap;->b:I

    iput v0, p0, Lcom/sec/chaton/j/ao;->b:I

    .line 169
    iget-object v0, p1, Lcom/sec/chaton/j/ap;->c:Lcom/google/protobuf/GeneratedMessageLite;

    iput-object v0, p0, Lcom/sec/chaton/j/ao;->c:Lcom/google/protobuf/GeneratedMessageLite;

    .line 170
    iget v0, p0, Lcom/sec/chaton/j/ao;->b:I

    iget-object v1, p0, Lcom/sec/chaton/j/ao;->c:Lcom/google/protobuf/GeneratedMessageLite;

    invoke-static {v0, v1}, Lcom/sec/chaton/j/g;->a(ILcom/google/protobuf/GeneratedMessageLite;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/j/ao;->d:J

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->c:Lcom/google/protobuf/GeneratedMessageLite;

    invoke-static {v0}, Lcom/sec/chaton/j/g;->a(Lcom/google/protobuf/GeneratedMessageLite;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LOG_MODE message] GPB Required field is not assigned in MessageMicro: uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/j/ao;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", msgId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/j/ao;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/j/ap;->e:Lcom/sec/chaton/j/q;

    iput-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    .line 179
    iget-wide v0, p1, Lcom/sec/chaton/j/ap;->f:J

    iput-wide v0, p0, Lcom/sec/chaton/j/ao;->f:J

    .line 180
    iget-wide v0, p1, Lcom/sec/chaton/j/ap;->g:J

    iput-wide v0, p0, Lcom/sec/chaton/j/ao;->g:J

    .line 181
    return-void
.end method

.method public static declared-synchronized f()V
    .locals 3

    .prologue
    .line 285
    const-class v1, Lcom/sec/chaton/j/ao;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/ao;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/sec/chaton/j/ao;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 289
    :cond_0
    sget-object v0, Lcom/sec/chaton/j/ao;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    monitor-exit v1

    return-void

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    if-nez v0, :cond_0

    .line 247
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/j/q;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/j/q;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    .line 226
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/chaton/j/q;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(J)Z
    .locals 4

    .prologue
    .line 272
    iget-wide v0, p0, Lcom/sec/chaton/j/ao;->g:J

    iget-wide v2, p0, Lcom/sec/chaton/j/ao;->f:J

    sub-long v2, p1, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/sec/chaton/j/ao;->b:I

    return v0
.end method

.method public c()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->c:Lcom/google/protobuf/GeneratedMessageLite;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/sec/chaton/j/ao;->d:J

    return-wide v0
.end method

.method public e()Lcom/sec/chaton/j/q;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/chaton/j/ao;->e:Lcom/sec/chaton/j/q;

    return-object v0
.end method
