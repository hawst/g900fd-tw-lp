.class public Lcom/sec/chaton/j/q;
.super Ljava/lang/Object;
.source "NetResultReceiver.java"


# instance fields
.field final a:Landroid/os/Handler;

.field private b:Lcom/sec/chaton/j/t;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/t;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/sec/chaton/j/q;->a:Landroid/os/Handler;

    .line 72
    invoke-virtual {p0, p2}, Lcom/sec/chaton/j/q;->a(Lcom/sec/chaton/j/t;)V

    .line 73
    return-void
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 197
    :goto_0
    return v0

    .line 181
    :cond_0
    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    .line 195
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xb -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1f -> :sswitch_0
        0x20 -> :sswitch_0
        0x21 -> :sswitch_0
        0x22 -> :sswitch_0
        0x23 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 201
    packed-switch p0, :pswitch_data_0

    .line 208
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 206
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/j/q;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/j/r;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/chaton/j/r;-><init>(Lcom/sec/chaton/j/q;ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 100
    return-void
.end method

.method public a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/chaton/j/q;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/j/u;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/chaton/j/u;-><init>(Lcom/sec/chaton/j/q;ILjava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method

.method public a(Lcom/sec/chaton/j/t;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/chaton/j/q;->b:Lcom/sec/chaton/j/t;

    .line 82
    return-void
.end method

.method protected b(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/chaton/j/q;->b:Lcom/sec/chaton/j/t;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/j/q;->b:Lcom/sec/chaton/j/t;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/j/t;->a(ILjava/lang/Object;)V

    .line 174
    :cond_0
    return-void
.end method

.method protected b(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/chaton/j/q;->b:Lcom/sec/chaton/j/t;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/j/q;->b:Lcom/sec/chaton/j/t;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/chaton/j/t;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 164
    :cond_0
    return-void
.end method
