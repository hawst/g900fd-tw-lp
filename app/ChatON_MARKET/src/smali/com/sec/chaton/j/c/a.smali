.class public Lcom/sec/chaton/j/c/a;
.super Ljava/lang/Object;
.source "FileDownloadManager.java"


# static fields
.field private static b:Lcom/sec/chaton/j/c/a;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/j/c/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/j/c/a;->b:Lcom/sec/chaton/j/c/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    .line 213
    return-void
.end method

.method public static a()Lcom/sec/chaton/j/c/a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/chaton/j/c/a;->b:Lcom/sec/chaton/j/c/a;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/sec/chaton/j/c/a;

    invoke-direct {v0}, Lcom/sec/chaton/j/c/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/c/a;->b:Lcom/sec/chaton/j/c/a;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/chaton/j/c/a;->b:Lcom/sec/chaton/j/c/a;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 69
    const-string v1, "\n"

    invoke-virtual {p5, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 79
    const-string v2, "[downloadFile2]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v2, Lcom/sec/chaton/j/c/f;

    invoke-direct {v2}, Lcom/sec/chaton/j/c/f;-><init>()V

    .line 84
    array-length v3, v1

    const/4 v4, 0x5

    if-lt v3, v4, :cond_3

    .line 86
    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/c/f;->a(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/chaton/j/c/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/j/c/f;

    move-result-object v2

    invoke-virtual {v2, p10}, Lcom/sec/chaton/j/c/f;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/j/c/f;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/chaton/j/c/f;->a(Landroid/view/View;)Lcom/sec/chaton/j/c/f;

    move-result-object v2

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/c/f;->b(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Lcom/sec/chaton/j/c/f;->c(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/sec/chaton/j/c/f;->a(Z)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/sec/chaton/j/c/f;->d(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1, p8, p9}, Lcom/sec/chaton/j/c/f;->a(J)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/c/f;->a(Lcom/sec/chaton/e/r;)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sec/chaton/j/c/f;->a(I)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/chaton/j/c/f;->a(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/c/f;->e(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/c/f;->a()Lcom/sec/chaton/j/c/c;

    move-result-object v1

    .line 126
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    if-eqz p6, :cond_2

    .line 127
    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/c/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 132
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/j/c/b;

    invoke-direct {v4, p0, p7, v1}, Lcom/sec/chaton/j/c/b;-><init>(Lcom/sec/chaton/j/c/a;Ljava/lang/String;Lcom/sec/chaton/j/c/c;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msdid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8, p9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", keyset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fileDownloadInProgress : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", addr="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    :goto_1
    return-void

    .line 129
    :cond_2
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/c/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 135
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 136
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00b5

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_1
.end method

.method public a(J)Z
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/b;

    .line 39
    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(JZ)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/b;

    .line 49
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/b;->b()Lcom/sec/chaton/j/c/c;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 53
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/b;->b()Lcom/sec/chaton/j/c/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/c;->a()Z

    move-result v0

    if-ne p3, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 163
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 194
    :goto_0
    return v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 168
    goto :goto_0

    .line 174
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/j/c/b;

    .line 177
    if-eqz v1, :cond_2

    .line 178
    invoke-virtual {v1}, Lcom/sec/chaton/j/c/b;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 179
    invoke-virtual {v1}, Lcom/sec/chaton/j/c/b;->b()Lcom/sec/chaton/j/c/c;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/c/c;->cancel(Z)Z

    .line 180
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 190
    iget-object v3, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    move v0, v2

    .line 194
    goto :goto_0
.end method

.method public b(J)Lcom/sec/chaton/j/c/c;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/b;

    .line 60
    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/b;->b()Lcom/sec/chaton/j/c/c;

    move-result-object v0

    goto :goto_0
.end method

.method public c(J)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/b;

    .line 151
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/b;->b()Lcom/sec/chaton/j/c/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/c/c;->cancel(Z)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 152
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->c(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 158
    :cond_0
    return-void
.end method

.method public d(J)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 210
    :goto_0
    return v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/j/c/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/b;

    .line 203
    if-eqz v0, :cond_1

    move v0, v1

    .line 205
    goto :goto_0

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 210
    goto :goto_0
.end method
