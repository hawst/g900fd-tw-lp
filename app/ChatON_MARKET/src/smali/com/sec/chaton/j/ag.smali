.class Lcom/sec/chaton/j/ag;
.super Lcom/sec/chaton/j/ar;
.source "TcpClient.java"


# instance fields
.field final synthetic a:J

.field final synthetic b:J

.field final synthetic c:Lcom/sec/chaton/j/af;


# direct methods
.method constructor <init>(Lcom/sec/chaton/j/af;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/chaton/j/ag;->c:Lcom/sec/chaton/j/af;

    iput-wide p4, p0, Lcom/sec/chaton/j/ag;->a:J

    iput-wide p6, p0, Lcom/sec/chaton/j/ag;->b:J

    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/j/ar;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 184
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 185
    iget-wide v2, p0, Lcom/sec/chaton/j/ag;->a:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/sec/chaton/j/ag;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 186
    const-string v0, "TcpClient.reconnectIMPL().run() - waiting timeout"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/ag;->c:Lcom/sec/chaton/j/af;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/af;)V

    .line 191
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic b([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/ag;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
