.class final enum Lcom/sec/chaton/j/d/f;
.super Ljava/lang/Enum;
.source "StreamEnvelopeDecoder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/j/d/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/j/d/f;

.field public static final enum b:Lcom/sec/chaton/j/d/f;

.field public static final enum c:Lcom/sec/chaton/j/d/f;

.field public static final enum d:Lcom/sec/chaton/j/d/f;

.field public static final enum e:Lcom/sec/chaton/j/d/f;

.field private static final synthetic f:[Lcom/sec/chaton/j/d/f;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 116
    new-instance v0, Lcom/sec/chaton/j/d/f;

    const-string v1, "READ_UID"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/d/f;->a:Lcom/sec/chaton/j/d/f;

    .line 117
    new-instance v0, Lcom/sec/chaton/j/d/f;

    const-string v1, "READ_RESERVED"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/j/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/d/f;->b:Lcom/sec/chaton/j/d/f;

    .line 118
    new-instance v0, Lcom/sec/chaton/j/d/f;

    const-string v1, "READ_TYPE"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/j/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/d/f;->c:Lcom/sec/chaton/j/d/f;

    .line 119
    new-instance v0, Lcom/sec/chaton/j/d/f;

    const-string v1, "READ_LENGTH"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/j/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/d/f;->d:Lcom/sec/chaton/j/d/f;

    .line 120
    new-instance v0, Lcom/sec/chaton/j/d/f;

    const-string v1, "READ_CONTENT"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/j/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/d/f;->e:Lcom/sec/chaton/j/d/f;

    .line 115
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/j/d/f;

    sget-object v1, Lcom/sec/chaton/j/d/f;->a:Lcom/sec/chaton/j/d/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/j/d/f;->b:Lcom/sec/chaton/j/d/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/j/d/f;->c:Lcom/sec/chaton/j/d/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/j/d/f;->d:Lcom/sec/chaton/j/d/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/j/d/f;->e:Lcom/sec/chaton/j/d/f;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/j/d/f;->f:[Lcom/sec/chaton/j/d/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/j/d/f;
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/sec/chaton/j/d/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/d/f;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/j/d/f;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/sec/chaton/j/d/f;->f:[Lcom/sec/chaton/j/d/f;

    invoke-virtual {v0}, [Lcom/sec/chaton/j/d/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/j/d/f;

    return-object v0
.end method
