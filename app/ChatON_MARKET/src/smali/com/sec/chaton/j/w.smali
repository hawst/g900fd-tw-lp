.class public Lcom/sec/chaton/j/w;
.super Ljava/lang/Object;
.source "QueueManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/j/w;


# instance fields
.field private final c:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private final d:Ljava/util/concurrent/locks/Lock;

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private f:Lcom/sec/chaton/j/l;

.field private g:Lcom/sec/chaton/j/l;

.field private h:Lcom/sec/chaton/j/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/chaton/j/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/w;->c:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/j/w;->c:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/j/w;->c:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/w;->e:Ljava/util/concurrent/locks/Lock;

    .line 53
    invoke-direct {p0}, Lcom/sec/chaton/j/w;->f()V

    .line 54
    return-void
.end method

.method public static a()Lcom/sec/chaton/j/w;
    .locals 2

    .prologue
    .line 28
    const-class v1, Lcom/sec/chaton/j/w;

    monitor-enter v1

    .line 29
    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/w;->b:Lcom/sec/chaton/j/w;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/chaton/j/w;

    invoke-direct {v0}, Lcom/sec/chaton/j/w;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/w;->b:Lcom/sec/chaton/j/w;

    .line 32
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    sget-object v0, Lcom/sec/chaton/j/w;->b:Lcom/sec/chaton/j/w;

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 105
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 106
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, "Start initialization all queue."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    const-string v0, "CoreQueue"

    invoke-static {v0}, Lcom/sec/chaton/j/l;->a(Ljava/lang/String;)Lcom/sec/chaton/j/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/w;->f:Lcom/sec/chaton/j/l;

    .line 110
    const-string v0, "ServiceQueue"

    invoke-static {v0}, Lcom/sec/chaton/j/l;->a(Ljava/lang/String;)Lcom/sec/chaton/j/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/w;->g:Lcom/sec/chaton/j/l;

    .line 111
    const-string v0, "MessageQueue"

    invoke-static {v0}, Lcom/sec/chaton/j/l;->a(Ljava/lang/String;)Lcom/sec/chaton/j/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/w;->h:Lcom/sec/chaton/j/l;

    .line 112
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 117
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, "Start shutdown all queue."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/w;->f:Lcom/sec/chaton/j/l;

    invoke-virtual {v0}, Lcom/sec/chaton/j/l;->a()V

    .line 123
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 124
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, " - Core queue is shutdown."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/j/w;->g:Lcom/sec/chaton/j/l;

    invoke-virtual {v0}, Lcom/sec/chaton/j/l;->a()V

    .line 129
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_2

    .line 130
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, " - Service queue is shutdown."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/j/w;->h:Lcom/sec/chaton/j/l;

    invoke-virtual {v0}, Lcom/sec/chaton/j/l;->a()V

    .line 135
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_3

    .line 136
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, " - Message queue is shutdown."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_3
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_4

    .line 140
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/j/w;->a:Ljava/lang/String;

    const-string v2, "All queue is shutdown."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_4
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/j/l;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/w;->f:Lcom/sec/chaton/j/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 60
    return-object v0

    .line 62
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public c()Lcom/sec/chaton/j/l;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/w;->g:Lcom/sec/chaton/j/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 70
    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public d()Lcom/sec/chaton/j/l;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/w;->h:Lcom/sec/chaton/j/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 80
    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/w;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/chaton/j/w;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 93
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/j/w;->g()V

    .line 95
    invoke-direct {p0}, Lcom/sec/chaton/j/w;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/j/w;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 99
    return-void

    .line 97
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/w;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
