.class Lcom/sec/chaton/j/f;
.super Ljava/util/TimerTask;
.source "GarbageConnectionCollector.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/j/e;


# direct methods
.method constructor <init>(Lcom/sec/chaton/j/e;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/chaton/j/af;->e()Z

    .line 59
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "Expired Connection collector"

    const-string v1, "GarbageConnectionCollector"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    invoke-static {v0}, Lcom/sec/chaton/j/e;->a(Lcom/sec/chaton/j/e;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    invoke-static {v0}, Lcom/sec/chaton/j/e;->b(Lcom/sec/chaton/j/e;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    invoke-static {v0}, Lcom/sec/chaton/j/e;->c(Lcom/sec/chaton/j/e;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    invoke-static {v0}, Lcom/sec/chaton/j/e;->c(Lcom/sec/chaton/j/e;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/j/e;->a(Lcom/sec/chaton/j/e;Ljava/util/Timer;)Ljava/util/Timer;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/j/f;->a:Lcom/sec/chaton/j/e;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/j/e;->a(Lcom/sec/chaton/j/e;Z)Z

    .line 69
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 70
    const-string v0, "TimerTask was canceled."

    const-string v2, "GarbageConnectionCollector"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    monitor-exit v1

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
