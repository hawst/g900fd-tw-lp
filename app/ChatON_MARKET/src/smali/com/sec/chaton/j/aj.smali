.class public final enum Lcom/sec/chaton/j/aj;
.super Ljava/lang/Enum;
.source "TcpClient.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/j/aj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/j/aj;

.field public static final enum b:Lcom/sec/chaton/j/aj;

.field public static final enum c:Lcom/sec/chaton/j/aj;

.field private static final synthetic e:[Lcom/sec/chaton/j/aj;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, Lcom/sec/chaton/j/aj;

    const-string v1, "NOT_CONNECT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/j/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    .line 47
    new-instance v0, Lcom/sec/chaton/j/aj;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/j/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/j/aj;->b:Lcom/sec/chaton/j/aj;

    .line 48
    new-instance v0, Lcom/sec/chaton/j/aj;

    const-string v1, "CONNECT"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/j/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/j/aj;->c:Lcom/sec/chaton/j/aj;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/j/aj;

    sget-object v1, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/j/aj;->b:Lcom/sec/chaton/j/aj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/j/aj;->c:Lcom/sec/chaton/j/aj;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/j/aj;->e:[Lcom/sec/chaton/j/aj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/sec/chaton/j/aj;->d:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/j/aj;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/sec/chaton/j/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/aj;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/j/aj;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/chaton/j/aj;->e:[Lcom/sec/chaton/j/aj;

    invoke-virtual {v0}, [Lcom/sec/chaton/j/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/j/aj;

    return-object v0
.end method
