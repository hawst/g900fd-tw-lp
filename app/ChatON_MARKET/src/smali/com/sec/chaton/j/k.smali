.class public final enum Lcom/sec/chaton/j/k;
.super Ljava/lang/Enum;
.source "HttpEnvelope.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/j/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/j/k;

.field public static final enum b:Lcom/sec/chaton/j/k;

.field public static final enum c:Lcom/sec/chaton/j/k;

.field public static final enum d:Lcom/sec/chaton/j/k;

.field private static final synthetic e:[Lcom/sec/chaton/j/k;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 378
    new-instance v0, Lcom/sec/chaton/j/k;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    .line 379
    new-instance v0, Lcom/sec/chaton/j/k;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/j/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    .line 382
    new-instance v0, Lcom/sec/chaton/j/k;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/j/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/k;->c:Lcom/sec/chaton/j/k;

    .line 383
    new-instance v0, Lcom/sec/chaton/j/k;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/j/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/k;->d:Lcom/sec/chaton/j/k;

    .line 377
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/j/k;

    sget-object v1, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/j/k;->c:Lcom/sec/chaton/j/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/j/k;->d:Lcom/sec/chaton/j/k;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/j/k;->e:[Lcom/sec/chaton/j/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 377
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/j/k;
    .locals 1

    .prologue
    .line 377
    const-class v0, Lcom/sec/chaton/j/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/k;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/j/k;
    .locals 1

    .prologue
    .line 377
    sget-object v0, Lcom/sec/chaton/j/k;->e:[Lcom/sec/chaton/j/k;

    invoke-virtual {v0}, [Lcom/sec/chaton/j/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/j/k;

    return-object v0
.end method
