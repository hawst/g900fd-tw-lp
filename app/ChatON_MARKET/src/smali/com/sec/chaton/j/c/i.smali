.class public Lcom/sec/chaton/j/c/i;
.super Ljava/lang/Object;
.source "FileUploadTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Ljava/io/DataOutputStream;

.field private c:Landroid/os/Handler;

.field private d:I

.field private e:Landroid/os/Handler;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/io/File;

.field private i:Lcom/sec/chaton/e/r;

.field private j:Lcom/sec/chaton/e/w;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private final q:Ljava/lang/Object;

.field private final r:Ljava/lang/Object;

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Lcom/sec/chaton/msgsend/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/chaton/j/c/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput v1, p0, Lcom/sec/chaton/j/c/i;->d:I

    .line 109
    iput-boolean v1, p0, Lcom/sec/chaton/j/c/i;->s:Z

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->m:Ljava/util/ArrayList;

    .line 111
    iput-boolean v1, p0, Lcom/sec/chaton/j/c/i;->p:Z

    .line 112
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->q:Ljava/lang/Object;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->r:Ljava/lang/Object;

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    .line 115
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/StringBuilder;)I
    .locals 20

    .prologue
    .line 553
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-static {v2}, Lcom/sec/chaton/util/r;->b(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 554
    const-string v2, "Such a file do NOT exit"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const/4 v2, 0x1

    .line 871
    :goto_0
    return v2

    .line 566
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 568
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "is_file_server_primary "

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 571
    if-eqz v11, :cond_8

    .line 572
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/chaton/util/cf;->a:Lcom/sec/chaton/util/cf;

    sget-object v4, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    invoke-static {v3, v4}, Lcom/sec/chaton/j/c;->a(Lcom/sec/chaton/util/cf;Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 577
    :goto_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "imei="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "&filename="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "&filesize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "&filepart=1/1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 594
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[File Upload/Before Encrypt] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v4

    .line 600
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 601
    new-instance v5, Lcom/sec/chaton/util/a;

    invoke-static {v4}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v4}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v5, v6, v4}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 606
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 613
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[File Upload/After Encrypt] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, "?uid="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, "&param="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 617
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload URL : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :try_start_1
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 629
    :try_start_2
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 636
    const-wide/16 v8, 0x0

    .line 638
    const/4 v5, 0x0

    .line 639
    const/4 v3, 0x0

    .line 644
    const/4 v4, 0x0

    .line 651
    :try_start_3
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_3c
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_38
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 652
    :try_start_4
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_3d
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_39
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_34
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_30
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2c
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 656
    const/4 v3, 0x1

    :try_start_5
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 657
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 658
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 659
    const-string v3, "POST"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 660
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v3, v5, :cond_1

    .line 661
    const-string v3, "cache-control"

    const-string v5, "no-transform"

    invoke-virtual {v2, v3, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    :cond_1
    const/16 v3, 0x7530

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 666
    const-string v3, "Content-Type"

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const-string v3, "Connection"

    const-string v5, "Close"

    invoke-virtual {v2, v3, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v12

    long-to-int v5, v12

    .line 671
    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 675
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 676
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[Start uploading] Uploaded FileSize: "

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v14, ", FileName:"

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v3, v14}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 682
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v14

    invoke-direct {v3, v14}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    .line 683
    const/16 v3, 0x800

    new-array v14, v3, [B

    .line 685
    const/4 v3, 0x0

    .line 687
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/j/c/i;->q:Ljava/lang/Object;

    monitor-enter v15
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_35
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_31
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2d
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 688
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/chaton/j/c/i;->s:Z

    move/from16 v16, v0

    if-nez v16, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v16

    if-eqz v16, :cond_a

    .line 689
    :cond_2
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/j/c/i;->s:Z

    if-eqz v3, :cond_3

    .line 690
    const-string v3, "Upload canceled by the user"

    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_3
    const/4 v3, 0x6

    monitor-exit v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 804
    if-eqz v4, :cond_4

    .line 806
    :try_start_7
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_20

    .line 815
    :cond_4
    :goto_3
    if-eqz v7, :cond_5

    .line 817
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_21

    .line 824
    :cond_5
    :goto_4
    if-eqz v6, :cond_6

    .line 826
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_22

    .line 833
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_7

    .line 835
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_23

    .line 842
    :cond_7
    :goto_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 692
    goto/16 :goto_0

    .line 574
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/chaton/util/cf;->b:Lcom/sec/chaton/util/cf;

    sget-object v4, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    invoke-static {v3, v4}, Lcom/sec/chaton/j/c;->a(Lcom/sec/chaton/util/cf;Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 603
    :cond_9
    :try_start_b
    const-string v2, "Fail in getting a key"

    sget-object v3, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    .line 604
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 607
    :catch_0
    move-exception v2

    .line 608
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 609
    const-string v2, "Encryption Error"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const/4 v2, 0x3

    goto/16 :goto_0

    .line 622
    :catch_1
    move-exception v2

    .line 623
    sget-object v3, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 624
    const/4 v2, 0x4

    goto/16 :goto_0

    .line 630
    :catch_2
    move-exception v2

    .line 631
    sget-object v3, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 632
    const/4 v2, 0x5

    goto/16 :goto_0

    .line 694
    :cond_a
    :try_start_c
    monitor-exit v15
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 695
    :try_start_d
    invoke-virtual {v6, v14}, Ljava/io/InputStream;->read([B)I

    move-result v15

    .line 697
    if-gtz v15, :cond_14

    .line 722
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v8, 0xc8

    if-eq v3, v8, :cond_1b

    .line 723
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    .line 724
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_d
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/net/SocketException; {:try_start_d .. :try_end_d} :catch_35
    .catch Ljava/io/EOFException; {:try_start_d .. :try_end_d} :catch_31
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 725
    :try_start_e
    invoke-static {v5}, Lcom/sec/chaton/j/c;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 726
    invoke-static {v4}, Lcom/sec/chaton/j/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 727
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Upload]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ":"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 732
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0e0000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b

    .line 733
    const-string v3, "01000006"

    const-string v4, "2001"

    invoke-static {v3, v4, v2}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/net/HttpURLConnection;)V
    :try_end_e
    .catch Ljava/net/SocketTimeoutException; {:try_start_e .. :try_end_e} :catch_3e
    .catch Ljava/net/UnknownHostException; {:try_start_e .. :try_end_e} :catch_3a
    .catch Ljava/net/SocketException; {:try_start_e .. :try_end_e} :catch_36
    .catch Ljava/io/EOFException; {:try_start_e .. :try_end_e} :catch_32
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 737
    :cond_b
    const/4 v3, 0x7

    .line 804
    if-eqz v5, :cond_c

    .line 806
    :try_start_f
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_24

    .line 815
    :cond_c
    :goto_7
    if-eqz v7, :cond_d

    .line 817
    :try_start_10
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_25

    .line 824
    :cond_d
    :goto_8
    if-eqz v6, :cond_e

    .line 826
    :try_start_11
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_26

    .line 833
    :cond_e
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_f

    .line 835
    :try_start_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_27

    .line 842
    :cond_f
    :goto_a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 737
    goto/16 :goto_0

    .line 694
    :catchall_0
    move-exception v3

    :try_start_13
    monitor-exit v15
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :try_start_14
    throw v3
    :try_end_14
    .catch Ljava/net/SocketTimeoutException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_14 .. :try_end_14} :catch_4
    .catch Ljava/net/SocketException; {:try_start_14 .. :try_end_14} :catch_35
    .catch Ljava/io/EOFException; {:try_start_14 .. :try_end_14} :catch_31
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_2d
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    .line 757
    :catch_3
    move-exception v3

    move-object v5, v6

    move-object v6, v7

    .line 762
    :goto_b
    if-eqz v11, :cond_22

    .line 763
    :try_start_15
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v7, "is_file_server_primary "

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 779
    :goto_c
    const/16 v3, 0x9

    .line 804
    if-eqz v4, :cond_10

    .line 806
    :try_start_16
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_c

    .line 815
    :cond_10
    :goto_d
    if-eqz v6, :cond_11

    .line 817
    :try_start_17
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_d

    .line 824
    :cond_11
    :goto_e
    if-eqz v5, :cond_12

    .line 826
    :try_start_18
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_e

    .line 833
    :cond_12
    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_13

    .line 835
    :try_start_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_f

    .line 842
    :cond_13
    :goto_10
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 779
    goto/16 :goto_0

    .line 701
    :cond_14
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1, v15}, Ljava/io/DataOutputStream;->write([BII)V

    .line 703
    int-to-long v15, v15

    add-long/2addr v8, v15

    .line 704
    if-lez v5, :cond_1a

    .line 705
    const-wide/16 v15, 0x64

    mul-long/2addr v15, v8

    int-to-long v0, v5

    move-wide/from16 v17, v0

    div-long v15, v15, v17

    long-to-int v3, v15

    .line 707
    sget-boolean v15, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v15, :cond_15

    rem-int/lit8 v15, v3, 0xa

    if-nez v15, :cond_15

    .line 708
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[Upload Progress (Total sent/ Entire file size)]  "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static/range {v15 .. v16}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_15
    :goto_11
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/chaton/j/c/i;->a(I)Z
    :try_end_1a
    .catch Ljava/net/SocketTimeoutException; {:try_start_1a .. :try_end_1a} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_1a .. :try_end_1a} :catch_4
    .catch Ljava/net/SocketException; {:try_start_1a .. :try_end_1a} :catch_35
    .catch Ljava/io/EOFException; {:try_start_1a .. :try_end_1a} :catch_31
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_2d
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    goto/16 :goto_2

    .line 780
    :catch_4
    move-exception v3

    .line 781
    :goto_12
    :try_start_1b
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_4

    .line 782
    const/16 v3, 0xe

    .line 804
    if-eqz v4, :cond_16

    .line 806
    :try_start_1c
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_10

    .line 815
    :cond_16
    :goto_13
    if-eqz v7, :cond_17

    .line 817
    :try_start_1d
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_11

    .line 824
    :cond_17
    :goto_14
    if-eqz v6, :cond_18

    .line 826
    :try_start_1e
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_12

    .line 833
    :cond_18
    :goto_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_19

    .line 835
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_13

    .line 842
    :cond_19
    :goto_16
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 782
    goto/16 :goto_0

    .line 712
    :cond_1a
    const/16 v15, 0x64

    if-ge v3, v15, :cond_15

    .line 713
    add-int/lit8 v3, v3, 0xa

    goto :goto_11

    .line 741
    :cond_1b
    const/16 v3, 0x64

    :try_start_20
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/chaton/j/c/i;->a(I)Z

    .line 742
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 744
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_1c

    .line 745
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[End uploading] Uploaded FileSize: "

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", FileName:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", Time(Upload): "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    :cond_1c
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_20
    .catch Ljava/net/SocketTimeoutException; {:try_start_20 .. :try_end_20} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_20 .. :try_end_20} :catch_4
    .catch Ljava/net/SocketException; {:try_start_20 .. :try_end_20} :catch_35
    .catch Ljava/io/EOFException; {:try_start_20 .. :try_end_20} :catch_31
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_2d
    .catchall {:try_start_20 .. :try_end_20} :catchall_4

    .line 750
    :try_start_21
    invoke-static {v3}, Lcom/sec/chaton/j/c;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 751
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 753
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_1d

    .line 754
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Upload IP] "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_21
    .catch Ljava/net/SocketTimeoutException; {:try_start_21 .. :try_end_21} :catch_3f
    .catch Ljava/net/UnknownHostException; {:try_start_21 .. :try_end_21} :catch_3b
    .catch Ljava/net/SocketException; {:try_start_21 .. :try_end_21} :catch_37
    .catch Ljava/io/EOFException; {:try_start_21 .. :try_end_21} :catch_33
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_2f
    .catchall {:try_start_21 .. :try_end_21} :catchall_6

    .line 804
    :cond_1d
    if-eqz v3, :cond_1e

    .line 806
    :try_start_22
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_28

    .line 815
    :cond_1e
    :goto_17
    if-eqz v7, :cond_1f

    .line 817
    :try_start_23
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_29

    .line 824
    :cond_1f
    :goto_18
    if-eqz v6, :cond_20

    .line 826
    :try_start_24
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_2a

    .line 833
    :cond_20
    :goto_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v3, :cond_21

    .line 835
    :try_start_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_2b

    .line 842
    :cond_21
    :goto_1a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 850
    if-eqz v4, :cond_36

    .line 853
    const-string v2, "http://"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 869
    const/16 v2, 0x8

    goto/16 :goto_0

    .line 765
    :cond_22
    :try_start_26
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static {v3}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v3

    .line 770
    invoke-virtual {v3}, Lcom/sec/chaton/d/l;->a()V
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    goto/16 :goto_c

    .line 804
    :catchall_1
    move-exception v3

    move-object v7, v6

    move-object v6, v5

    :goto_1b
    if-eqz v4, :cond_23

    .line 806
    :try_start_27
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_8

    .line 815
    :cond_23
    :goto_1c
    if-eqz v7, :cond_24

    .line 817
    :try_start_28
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_9

    .line 824
    :cond_24
    :goto_1d
    if-eqz v6, :cond_25

    .line 826
    :try_start_29
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_a

    .line 833
    :cond_25
    :goto_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_26

    .line 835
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2a} :catch_b

    .line 842
    :cond_26
    :goto_1f
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 804
    throw v3

    .line 785
    :catch_5
    move-exception v6

    move-object v7, v5

    move-object/from16 v19, v3

    move-object v3, v6

    move-object/from16 v6, v19

    .line 786
    :goto_20
    :try_start_2b
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_27

    .line 787
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_4

    .line 789
    :cond_27
    const/16 v3, 0xc

    .line 804
    if-eqz v4, :cond_28

    .line 806
    :try_start_2c
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_2c} :catch_14

    .line 815
    :cond_28
    :goto_21
    if-eqz v7, :cond_29

    .line 817
    :try_start_2d
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_2d} :catch_15

    .line 824
    :cond_29
    :goto_22
    if-eqz v6, :cond_2a

    .line 826
    :try_start_2e
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_2e} :catch_16

    .line 833
    :cond_2a
    :goto_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_2b

    .line 835
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_2f} :catch_17

    .line 842
    :cond_2b
    :goto_24
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 789
    goto/16 :goto_0

    .line 790
    :catch_6
    move-exception v6

    move-object v7, v5

    move-object/from16 v19, v3

    move-object v3, v6

    move-object/from16 v6, v19

    .line 791
    :goto_25
    :try_start_30
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2c

    .line 792
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_4

    .line 794
    :cond_2c
    const/16 v3, 0xd

    .line 804
    if-eqz v4, :cond_2d

    .line 806
    :try_start_31
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_31} :catch_18

    .line 815
    :cond_2d
    :goto_26
    if-eqz v7, :cond_2e

    .line 817
    :try_start_32
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_32} :catch_19

    .line 824
    :cond_2e
    :goto_27
    if-eqz v6, :cond_2f

    .line 826
    :try_start_33
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_33} :catch_1a

    .line 833
    :cond_2f
    :goto_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_30

    .line 835
    :try_start_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_34} :catch_1b

    .line 842
    :cond_30
    :goto_29
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 794
    goto/16 :goto_0

    .line 797
    :catch_7
    move-exception v6

    move-object v7, v5

    move-object/from16 v19, v3

    move-object v3, v6

    move-object/from16 v6, v19

    .line 798
    :goto_2a
    :try_start_35
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_31

    .line 799
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_4

    .line 801
    :cond_31
    const/16 v3, 0xa

    .line 804
    if-eqz v4, :cond_32

    .line 806
    :try_start_36
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_36} :catch_1c

    .line 815
    :cond_32
    :goto_2b
    if-eqz v7, :cond_33

    .line 817
    :try_start_37
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_37} :catch_1d

    .line 824
    :cond_33
    :goto_2c
    if-eqz v6, :cond_34

    .line 826
    :try_start_38
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_38} :catch_1e

    .line 833
    :cond_34
    :goto_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v4, :cond_35

    .line 835
    :try_start_39
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_39} :catch_1f

    .line 842
    :cond_35
    :goto_2e
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move v2, v3

    .line 801
    goto/16 :goto_0

    .line 871
    :cond_36
    const/4 v2, 0x7

    goto/16 :goto_0

    .line 807
    :catch_8
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_23

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1c

    .line 818
    :catch_9
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_24

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 827
    :catch_a
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_25

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1e

    .line 836
    :catch_b
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_26

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1f

    .line 807
    :catch_c
    move-exception v4

    .line 808
    sget-boolean v7, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v7, :cond_10

    .line 809
    sget-object v7, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v7}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 818
    :catch_d
    move-exception v4

    .line 819
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_11

    .line 820
    sget-object v6, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 827
    :catch_e
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_12

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 836
    :catch_f
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_13

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 807
    :catch_10
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_16

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 818
    :catch_11
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_17

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 827
    :catch_12
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_18

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_15

    .line 836
    :catch_13
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_19

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_16

    .line 807
    :catch_14
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_28

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_21

    .line 818
    :catch_15
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_29

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_22

    .line 827
    :catch_16
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2a

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_23

    .line 836
    :catch_17
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2b

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_24

    .line 807
    :catch_18
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2d

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_26

    .line 818
    :catch_19
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2e

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_27

    .line 827
    :catch_1a
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_2f

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_28

    .line 836
    :catch_1b
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_30

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_29

    .line 807
    :catch_1c
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_32

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2b

    .line 818
    :catch_1d
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_33

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2c

    .line 827
    :catch_1e
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_34

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2d

    .line 836
    :catch_1f
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_35

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 807
    :catch_20
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_4

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 818
    :catch_21
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_5

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 827
    :catch_22
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_6

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 836
    :catch_23
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_7

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 807
    :catch_24
    move-exception v4

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_c

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 818
    :catch_25
    move-exception v4

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_d

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 827
    :catch_26
    move-exception v4

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_e

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 836
    :catch_27
    move-exception v4

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_f

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 807
    :catch_28
    move-exception v3

    .line 808
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_1e

    .line 809
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 818
    :catch_29
    move-exception v3

    .line 819
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_1f

    .line 820
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 827
    :catch_2a
    move-exception v3

    .line 828
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_20

    .line 829
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_19

    .line 836
    :catch_2b
    move-exception v3

    .line 837
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_21

    .line 838
    sget-object v5, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 804
    :catchall_2
    move-exception v6

    move-object v7, v5

    move-object/from16 v19, v3

    move-object v3, v6

    move-object/from16 v6, v19

    goto/16 :goto_1b

    :catchall_3
    move-exception v5

    move-object v6, v3

    move-object v3, v5

    goto/16 :goto_1b

    :catchall_4
    move-exception v3

    goto/16 :goto_1b

    :catchall_5
    move-exception v3

    move-object v4, v5

    goto/16 :goto_1b

    :catchall_6
    move-exception v4

    move-object/from16 v19, v4

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_1b

    .line 797
    :catch_2c
    move-exception v5

    move-object v6, v3

    move-object v3, v5

    goto/16 :goto_2a

    :catch_2d
    move-exception v3

    goto/16 :goto_2a

    :catch_2e
    move-exception v3

    move-object v4, v5

    goto/16 :goto_2a

    :catch_2f
    move-exception v4

    move-object/from16 v19, v4

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_2a

    .line 790
    :catch_30
    move-exception v5

    move-object v6, v3

    move-object v3, v5

    goto/16 :goto_25

    :catch_31
    move-exception v3

    goto/16 :goto_25

    :catch_32
    move-exception v3

    move-object v4, v5

    goto/16 :goto_25

    :catch_33
    move-exception v4

    move-object/from16 v19, v4

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_25

    .line 785
    :catch_34
    move-exception v5

    move-object v6, v3

    move-object v3, v5

    goto/16 :goto_20

    :catch_35
    move-exception v3

    goto/16 :goto_20

    :catch_36
    move-exception v3

    move-object v4, v5

    goto/16 :goto_20

    :catch_37
    move-exception v4

    move-object/from16 v19, v4

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_20

    .line 780
    :catch_38
    move-exception v6

    move-object v7, v5

    move-object/from16 v19, v3

    move-object v3, v6

    move-object/from16 v6, v19

    goto/16 :goto_12

    :catch_39
    move-exception v5

    move-object v6, v3

    move-object v3, v5

    goto/16 :goto_12

    :catch_3a
    move-exception v3

    move-object v4, v5

    goto/16 :goto_12

    :catch_3b
    move-exception v4

    move-object/from16 v19, v4

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_12

    .line 757
    :catch_3c
    move-exception v6

    move-object v6, v5

    move-object v5, v3

    goto/16 :goto_b

    :catch_3d
    move-exception v5

    move-object v5, v3

    move-object v6, v7

    goto/16 :goto_b

    :catch_3e
    move-exception v3

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_b

    :catch_3f
    move-exception v4

    move-object v4, v3

    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_b
.end method

.method public static a(Ljava/io/File;Lcom/sec/chaton/e/w;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    const/4 v1, 0x0

    .line 204
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_3

    .line 206
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 217
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne p1, v1, :cond_1

    .line 218
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    const-string v0, "jpg"

    .line 220
    sget-object v1, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    const-string v2, "FILE Extention : forced to be \'JPG\' "

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    :cond_1
    if-nez v0, :cond_2

    .line 225
    sget-object v1, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    const-string v2, "FILE Extention : could not be determined \'"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    :cond_2
    return-object v0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    const-class v2, Lcom/sec/chaton/j/c/i;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->e:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 876
    const/4 v0, 0x0

    .line 886
    :goto_0
    return v0

    .line 879
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 881
    :try_start_0
    iput p1, p0, Lcom/sec/chaton/j/c/i;->d:I

    .line 882
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 885
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 886
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 882
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 171
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v0

    .line 175
    :cond_1
    sget-object v2, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    const-string v3, "isNeedToChangeStateToPending(), resultCode(%d), faultCode(%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :goto_1
    :pswitch_1
    move v0, v1

    .line 198
    goto :goto_0

    .line 186
    :pswitch_2
    const/16 v2, 0x2afe

    if-ne p1, v2, :cond_0

    goto :goto_1

    .line 177
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(ZILjava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 486
    .line 487
    if-eqz p1, :cond_3

    .line 489
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->d(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 497
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_2

    .line 498
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-static {v1, v2, v8, v8, v0}, Lcom/sec/chaton/util/r;->a(Landroid/content/Context;Ljava/io/File;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 502
    :goto_0
    if-eqz v1, :cond_0

    .line 504
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 508
    :cond_0
    iput-object p3, p0, Lcom/sec/chaton/j/c/i;->o:Ljava/lang/String;

    .line 541
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->c:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 542
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->c:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    .line 543
    iput p2, v7, Landroid/os/Message;->arg1:I

    .line 544
    iput v0, v7, Landroid/os/Message;->arg2:I

    .line 545
    new-instance v0, Lcom/sec/chaton/j/c/k;

    iget-wide v3, p0, Lcom/sec/chaton/j/c/i;->f:J

    move-object v1, p0

    move v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/j/c/k;-><init>(Lcom/sec/chaton/j/c/i;ZJLjava/lang/String;Lcom/sec/chaton/j/c/j;)V

    iput-object v0, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->c:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 549
    :cond_1
    return v8

    .line 499
    :cond_2
    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_a

    .line 500
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-static {v1, v2, v0, v8, v0}, Lcom/sec/chaton/util/r;->a(Landroid/content/Context;Ljava/io/File;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 512
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 513
    const-string v1, ":"

    invoke-virtual {p3, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_4

    .line 514
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/b/a;->a(Ljava/lang/String;)I

    move-result v0

    .line 518
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->v:Lcom/sec/chaton/msgsend/k;

    if-eqz v1, :cond_6

    .line 519
    sget-object v1, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_8

    .line 521
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->v:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {p2, v0}, Lcom/sec/chaton/j/c/i;->a(II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 522
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->l:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;)I

    .line 539
    :cond_6
    :goto_2
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/chaton/j/c/i;->o:Ljava/lang/String;

    goto/16 :goto_1

    .line 525
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->l:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;)I

    goto :goto_2

    .line 529
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->v:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-static {p2, v0}, Lcom/sec/chaton/j/c/i;->a(II)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 530
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_2

    .line 533
    :cond_9
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_2

    :cond_a
    move-object v1, v6

    goto/16 :goto_0
.end method

.method private m()Z
    .locals 4

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/sec/chaton/j/c/i;->p:Z

    if-eqz v0, :cond_0

    .line 394
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/j/c/i;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/r;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 399
    :cond_1
    const/4 v0, 0x0

    .line 482
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->g:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/chaton/j/c/i;->e:Landroid/os/Handler;

    .line 163
    return-void
.end method

.method public a(Landroid/os/Handler;Ljava/io/File;Lcom/sec/chaton/e/w;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/chaton/j/c/i;->c:Landroid/os/Handler;

    .line 234
    iput-object p4, p0, Lcom/sec/chaton/j/c/i;->e:Landroid/os/Handler;

    .line 235
    iput-object p2, p0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    .line 236
    iput-object p3, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    .line 237
    invoke-static {p2, p3}, Lcom/sec/chaton/j/c/i;->a(Ljava/io/File;Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 4

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/chaton/j/c/i;->c:Landroid/os/Handler;

    .line 247
    iput-object p2, p0, Lcom/sec/chaton/j/c/i;->n:Ljava/lang/String;

    .line 248
    iput-wide p4, p0, Lcom/sec/chaton/j/c/i;->f:J

    .line 249
    iput-object p7, p0, Lcom/sec/chaton/j/c/i;->i:Lcom/sec/chaton/e/r;

    .line 250
    iput-object p6, p0, Lcom/sec/chaton/j/c/i;->g:Ljava/lang/String;

    .line 251
    iput-object p3, p0, Lcom/sec/chaton/j/c/i;->h:Ljava/io/File;

    .line 252
    iput-object p8, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    .line 253
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->t:Ljava/lang/String;

    .line 254
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->u:Ljava/lang/String;

    .line 255
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/chaton/j/c/i;->v:Lcom/sec/chaton/msgsend/k;

    .line 257
    invoke-static {p3, p8}, Lcom/sec/chaton/j/c/i;->a(Ljava/io/File;Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    .line 278
    iput-object p11, p0, Lcom/sec/chaton/j/c/i;->l:Ljava/lang/String;

    .line 280
    const/4 v1, 0x0

    :goto_0
    array-length v2, p10

    if-ge v1, v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/sec/chaton/j/c/i;->m:Ljava/util/ArrayList;

    aget-object v3, p10, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 284
    :cond_0
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/sec/chaton/j/c/i;->p:Z

    .line 285
    return-void
.end method

.method public a(Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 890
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 891
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/chaton/j/c/i;->s:Z

    .line 892
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 893
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_1

    .line 895
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 901
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    .line 904
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 892
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 896
    :catch_0
    move-exception v0

    .line 897
    :try_start_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 898
    sget-object v1, Lcom/sec/chaton/j/c/i;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 901
    :catchall_1
    move-exception v0

    iput-object v2, p0, Lcom/sec/chaton/j/c/i;->a:Ljava/io/DataOutputStream;

    throw v0
.end method

.method public b()Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->i:Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method public c()Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->n:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->l:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->t:Ljava/lang/String;

    return-object v0
.end method

.method public i()Lcom/sec/chaton/msgsend/k;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->v:Lcom/sec/chaton/msgsend/k;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->u:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->o:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 2

    .prologue
    .line 290
    iget-object v1, p0, Lcom/sec/chaton/j/c/i;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_0
    iget v0, p0, Lcom/sec/chaton/j/c/i;->d:I

    monitor-exit v1

    return v0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 305
    invoke-direct {p0}, Lcom/sec/chaton/j/c/i;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/j/c/i;->a(ZILjava/lang/String;)Z

    .line 368
    :goto_0
    return-void

    .line 310
    :cond_0
    const-string v0, ""

    .line 312
    sget-object v3, Lcom/sec/chaton/j/c/j;->a:[I

    iget-object v4, p0, Lcom/sec/chaton/j/c/i;->j:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 346
    const-string v0, ""

    .line 350
    :cond_1
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    invoke-direct {p0, v0, v4}, Lcom/sec/chaton/j/c/i;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)I

    move-result v3

    .line 354
    if-eq v3, v7, :cond_3

    .line 355
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to upload file [nResultCode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const/16 v5, 0xc

    if-eq v3, v5, :cond_2

    const/16 v5, 0xd

    if-ne v3, v5, :cond_5

    .line 358
    :cond_2
    invoke-direct {p0, v0, v4}, Lcom/sec/chaton/j/c/i;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)I

    move-result v0

    .line 359
    if-ne v0, v7, :cond_4

    move v1, v0

    move v0, v2

    .line 367
    :goto_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/j/c/i;->a(ZILjava/lang/String;)Z

    goto :goto_0

    .line 316
    :pswitch_0
    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 317
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 323
    :pswitch_1
    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "audio/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 329
    :pswitch_2
    const-string v0, "text/x-vcalendar"

    goto :goto_1

    .line 332
    :pswitch_3
    const-string v0, "text/x-vcard"

    goto :goto_1

    .line 335
    :pswitch_4
    const-string v0, "text/x-map"

    goto :goto_1

    .line 340
    :pswitch_5
    iget-object v3, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/j/c/i;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    move v0, v2

    move v1, v3

    .line 364
    goto :goto_2

    :cond_4
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_2

    :cond_5
    move v0, v1

    move v1, v3

    goto :goto_2

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
