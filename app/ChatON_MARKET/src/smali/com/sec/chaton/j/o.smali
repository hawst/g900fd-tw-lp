.class public final enum Lcom/sec/chaton/j/o;
.super Ljava/lang/Enum;
.source "HttpResultCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/j/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/j/o;

.field public static final enum b:Lcom/sec/chaton/j/o;

.field public static final enum c:Lcom/sec/chaton/j/o;

.field public static final enum d:Lcom/sec/chaton/j/o;

.field private static final synthetic e:[Lcom/sec/chaton/j/o;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/sec/chaton/j/o;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    .line 16
    new-instance v0, Lcom/sec/chaton/j/o;

    const-string v1, "NO_CONTENT"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/j/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    .line 20
    new-instance v0, Lcom/sec/chaton/j/o;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/j/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    .line 24
    new-instance v0, Lcom/sec/chaton/j/o;

    const-string v1, "NO_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/j/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/j/o;->d:Lcom/sec/chaton/j/o;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/j/o;

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/j/o;->d:Lcom/sec/chaton/j/o;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/j/o;->e:[Lcom/sec/chaton/j/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/j/o;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/j/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/o;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/j/o;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/chaton/j/o;->e:[Lcom/sec/chaton/j/o;

    invoke-virtual {v0}, [Lcom/sec/chaton/j/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/j/o;

    return-object v0
.end method
