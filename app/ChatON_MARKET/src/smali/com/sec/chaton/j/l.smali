.class public Lcom/sec/chaton/j/l;
.super Ljava/lang/Object;
.source "HttpNetworkQueue.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/concurrent/ThreadPoolExecutor;

.field private d:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/j/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/l;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 48
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/chaton/j/l;-><init>(Ljava/lang/String;II)V

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 39
    iput-object v0, p0, Lcom/sec/chaton/j/l;->d:Ljava/util/concurrent/BlockingQueue;

    .line 52
    iput-object p1, p0, Lcom/sec/chaton/j/l;->b:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/l;->d:Ljava/util/concurrent/BlockingQueue;

    .line 56
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/sec/chaton/j/l;->d:Ljava/util/concurrent/BlockingQueue;

    move v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->prestartAllCoreThreads()I

    .line 60
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/j/l;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/chaton/j/l;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/j/l;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 120
    const v0, 0xea60

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/l;->a(I)V

    .line 121
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    int-to-long v1, p1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/d/a/a;)V
    .locals 3

    .prologue
    .line 63
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Task is reserved. Task: "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, ", Queue(name: "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/j/l;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ", size: "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/j/l;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    new-instance v0, Lcom/sec/chaton/j/m;

    invoke-direct {v0, p0, p1, p1}, Lcom/sec/chaton/j/m;-><init>(Lcom/sec/chaton/j/l;Lcom/sec/chaton/d/a/a;Lcom/sec/chaton/d/a/a;)V

    .line 107
    invoke-virtual {p1, v0}, Lcom/sec/chaton/d/a/a;->a(Ljava/util/concurrent/Future;)V

    .line 109
    iget-object v1, p0, Lcom/sec/chaton/j/l;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 110
    return-void
.end method
