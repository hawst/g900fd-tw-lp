.class Lcom/sec/chaton/j/m;
.super Lcom/sec/chaton/j/n;
.source "HttpNetworkQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/j/n",
        "<",
        "Lcom/sec/chaton/a/a/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/a/a;

.field final synthetic b:Lcom/sec/chaton/j/l;


# direct methods
.method constructor <init>(Lcom/sec/chaton/j/l;Lcom/sec/chaton/d/a/a;Lcom/sec/chaton/d/a/a;)V
    .locals 1

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/chaton/j/m;->b:Lcom/sec/chaton/j/l;

    iput-object p3, p0, Lcom/sec/chaton/j/m;->a:Lcom/sec/chaton/d/a/a;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/j/n;-><init>(Lcom/sec/chaton/d/a/a;Lcom/sec/chaton/j/m;)V

    return-void
.end method


# virtual methods
.method protected done()V
    .locals 7

    .prologue
    .line 70
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/j/m;->a:Lcom/sec/chaton/d/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/a;->j()Lcom/sec/common/d/a/c;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/h;

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->e()Landroid/os/Handler;

    move-result-object v2

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/j/m;->a:Lcom/sec/chaton/d/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/a;->j()Lcom/sec/common/d/a/c;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/h;

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->d()I

    move-result v3

    .line 75
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/j/m;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 101
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 104
    :cond_1
    :goto_1
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 78
    const-string v1, "Task is interrupted."

    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 83
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/j/m;->a:Lcom/sec/chaton/d/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/a;->k()Lcom/sec/common/d/a/e;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 85
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Throwable;)V

    .line 87
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_0

    .line 88
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Task is crashed. Message: "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "\nEntry: "

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :catch_2
    move-exception v0

    .line 93
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 94
    const-string v1, "Task is cancelled."

    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/sec/chaton/j/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method
