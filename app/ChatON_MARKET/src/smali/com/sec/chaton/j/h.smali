.class public Lcom/sec/chaton/j/h;
.super Lcom/sec/common/d/a/a/b;
.source "HttpEnvelope.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Lcom/sec/chaton/util/cg;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/chaton/j/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/h;->b:Ljava/lang/String;

    .line 41
    const/16 v0, 0x190

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/common/d/a/a/b;-><init>()V

    .line 52
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/j/j;)V
    .locals 4

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/sec/common/d/a/a/b;-><init>()V

    .line 310
    iget-object v0, p1, Lcom/sec/chaton/j/j;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/h;->d:Ljava/lang/String;

    .line 316
    sget-object v0, Lcom/sec/chaton/j/i;->b:[I

    iget-object v1, p1, Lcom/sec/chaton/j/j;->c:Lcom/sec/chaton/j/k;

    invoke-virtual {v1}, Lcom/sec/chaton/j/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 334
    :goto_0
    iget-object v0, p1, Lcom/sec/chaton/j/j;->h:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Ljava/util/Map;)V

    .line 337
    iget-object v0, p1, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    const-string v1, "r"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p1, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    const-string v1, "r"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    :cond_0
    iget-object v0, p1, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    const-string v1, "unauth"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 341
    iget-object v0, p1, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    const-string v1, "unauth"

    sget-object v2, Lcom/sec/chaton/j/h;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->b(Ljava/util/Map;)V

    .line 346
    iget-object v0, p1, Lcom/sec/chaton/j/j;->e:Ljava/lang/Class;

    iput-object v0, p0, Lcom/sec/chaton/j/h;->e:Ljava/lang/Class;

    .line 348
    iget v0, p1, Lcom/sec/chaton/j/j;->f:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(I)V

    .line 350
    iget-object v0, p1, Lcom/sec/chaton/j/j;->b:Lcom/sec/chaton/util/cg;

    iput-object v0, p0, Lcom/sec/chaton/j/h;->c:Lcom/sec/chaton/util/cg;

    .line 352
    invoke-virtual {p0}, Lcom/sec/chaton/j/h;->j()Ljava/util/Map;

    move-result-object v0

    const-string v1, "Content-Type"

    iget-object v2, p1, Lcom/sec/chaton/j/j;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v0, p1, Lcom/sec/chaton/j/j;->l:Ljava/lang/Class;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Ljava/lang/Class;)V

    .line 356
    iget-object v0, p1, Lcom/sec/chaton/j/j;->m:Lcom/sec/common/d/a/a;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Lcom/sec/common/d/a/a;)V

    .line 359
    iget-boolean v0, p1, Lcom/sec/chaton/j/j;->i:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Z)V

    .line 360
    iget-boolean v0, p1, Lcom/sec/chaton/j/j;->j:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->b(Z)V

    .line 362
    iget v0, p1, Lcom/sec/chaton/j/j;->k:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->b(I)V

    .line 363
    iget v0, p1, Lcom/sec/chaton/j/j;->k:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->c(I)V

    .line 370
    return-void

    .line 318
    :pswitch_0
    sget-object v0, Lcom/sec/common/d/a/d;->a:Lcom/sec/common/d/a/d;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Lcom/sec/common/d/a/d;)V

    goto :goto_0

    .line 322
    :pswitch_1
    sget-object v0, Lcom/sec/common/d/a/d;->b:Lcom/sec/common/d/a/d;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Lcom/sec/common/d/a/d;)V

    goto :goto_0

    .line 326
    :pswitch_2
    sget-object v0, Lcom/sec/common/d/a/d;->c:Lcom/sec/common/d/a/d;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Lcom/sec/common/d/a/d;)V

    goto :goto_0

    .line 330
    :pswitch_3
    sget-object v0, Lcom/sec/common/d/a/d;->d:Lcom/sec/common/d/a/d;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/h;->a(Lcom/sec/common/d/a/d;)V

    goto/16 :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/j/j;Lcom/sec/chaton/j/i;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/chaton/j/h;-><init>(Lcom/sec/chaton/j/j;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/util/cg;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/j/h;->c:Lcom/sec/chaton/util/cg;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/chaton/j/h;->f:I

    .line 95
    return-void
.end method

.method public a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/chaton/j/h;->g:Ljava/lang/ref/WeakReference;

    .line 103
    return-void
.end method

.method public a(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 168
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/j/h;->e:Ljava/lang/Class;

    if-eqz v1, :cond_3

    .line 169
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/common/d/a/a;

    .line 171
    instance-of v2, v1, Lcom/sec/chaton/util/af;

    if-eqz v2, :cond_1

    .line 172
    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/util/af;

    move-object v2, v0

    iget-object v3, p0, Lcom/sec/chaton/j/h;->e:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Class;)V

    .line 181
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/j/h;->b(Lcom/sec/common/d/a/a;)V

    .line 188
    :goto_1
    return-void

    .line 175
    :cond_1
    instance-of v2, v1, Lcom/sec/chaton/io/a/b;

    if-eqz v2, :cond_2

    .line 176
    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/io/a/b;

    move-object v2, v0

    iget-object v3, p0, Lcom/sec/chaton/j/h;->e:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/io/a/b;->a(Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 185
    :catch_0
    move-exception v1

    .line 186
    sget-object v2, Lcom/sec/chaton/j/h;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 177
    :cond_2
    :try_start_1
    instance-of v2, v1, Lcom/sec/chaton/util/v;

    if-eqz v2, :cond_0

    .line 178
    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/util/v;

    move-object v2, v0

    iget-object v3, p0, Lcom/sec/chaton/j/h;->e:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/v;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 183
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/j/h;->b(Lcom/sec/common/d/a/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/chaton/j/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/j/h;->c:Lcom/sec/chaton/util/cg;

    sget-object v1, Lcom/sec/chaton/util/cg;->h:Lcom/sec/chaton/util/cg;

    if-ne v0, v1, :cond_1

    .line 66
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p0}, Lcom/sec/chaton/j/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-virtual {v0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v0

    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 74
    sget-object v1, Lcom/sec/chaton/j/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 77
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 80
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/j/h;->c:Lcom/sec/chaton/util/cg;

    invoke-static {v1}, Lcom/sec/chaton/util/cd;->b(Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/j/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/chaton/j/h;->f:I

    return v0
.end method

.method public e()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/chaton/j/h;->g:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/h;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 127
    invoke-virtual {p0}, Lcom/sec/chaton/j/h;->o()Ljava/util/Map;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_0

    .line 130
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v1, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_0
    return-object v2
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/chaton/j/h;->j()Ljava/util/Map;

    move-result-object v0

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
