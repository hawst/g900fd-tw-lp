.class public Lcom/sec/chaton/j/ap;
.super Ljava/lang/Object;
.source "TcpEnvelope.java"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:I

.field protected c:Lcom/google/protobuf/GeneratedMessageLite;

.field protected d:J

.field protected e:Lcom/sec/chaton/j/q;

.field protected f:J

.field protected g:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v2, p0, Lcom/sec/chaton/j/ap;->a:Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/j/ap;->b:I

    .line 55
    iput-object v2, p0, Lcom/sec/chaton/j/ap;->c:Lcom/google/protobuf/GeneratedMessageLite;

    .line 57
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/j/ap;->d:J

    .line 58
    iput-object v2, p0, Lcom/sec/chaton/j/ap;->e:Lcom/sec/chaton/j/q;

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/j/ap;->a()Lcom/sec/chaton/j/ap;

    .line 65
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/j/ap;
    .locals 2

    .prologue
    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/j/ap;->f:J

    .line 149
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/sec/chaton/j/ap;->g:J

    .line 150
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/j/ap;
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/chaton/j/ap;->b:I

    .line 90
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/j/ap;
    .locals 2

    .prologue
    .line 133
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/j/ap;->f:J

    .line 134
    iput-wide p1, p0, Lcom/sec/chaton/j/ap;->g:J

    .line 135
    return-object p0
.end method

.method public a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/chaton/j/ap;->c:Lcom/google/protobuf/GeneratedMessageLite;

    .line 102
    return-object p0
.end method

.method public a(Lcom/sec/chaton/j/q;)Lcom/sec/chaton/j/ap;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/j/ap;->e:Lcom/sec/chaton/j/q;

    .line 114
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/j/ap;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/chaton/j/ap;->a:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public b()Lcom/sec/chaton/j/ao;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/sec/chaton/j/ao;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/ao;-><init>(Lcom/sec/chaton/j/ap;)V

    return-object v0
.end method
