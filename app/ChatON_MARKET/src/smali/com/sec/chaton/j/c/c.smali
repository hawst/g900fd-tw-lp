.class public Lcom/sec/chaton/j/c/c;
.super Landroid/os/AsyncTask;
.source "FileDownloadTask2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static v:Ljava/lang/String;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Z

.field protected e:Ljava/lang/String;

.field protected f:J

.field protected g:Lcom/sec/chaton/e/r;

.field protected h:Ljava/lang/String;

.field protected i:Z

.field j:Landroid/widget/ProgressBar;

.field k:Landroid/widget/ProgressBar;

.field l:Landroid/widget/Button;

.field protected m:I

.field protected n:I

.field protected o:Lcom/sec/chaton/e/w;

.field protected p:Ljava/lang/String;

.field protected q:Ljava/lang/String;

.field r:Ljava/lang/Boolean;

.field s:Lcom/sec/chaton/chat/ChatFragment;

.field t:Ljava/lang/String;

.field u:Landroid/widget/Toast;

.field private w:Landroid/os/Handler;

.field private x:Landroid/os/Handler;

.field private y:Landroid/view/View;

.field private z:Lcom/sec/chaton/widget/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "FileDownloadTask2"

    sput-object v0, Lcom/sec/chaton/j/c/c;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/j/c/f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 212
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->a:Ljava/lang/String;

    .line 63
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    .line 64
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    .line 65
    iput-boolean v3, p0, Lcom/sec/chaton/j/c/c;->d:Z

    .line 66
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/j/c/c;->f:J

    .line 68
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->g:Lcom/sec/chaton/e/r;

    .line 69
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->h:Ljava/lang/String;

    .line 70
    iput-boolean v3, p0, Lcom/sec/chaton/j/c/c;->i:Z

    .line 72
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    .line 74
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    .line 75
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    .line 76
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    .line 81
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->l:Landroid/widget/Button;

    .line 85
    iput v3, p0, Lcom/sec/chaton/j/c/c;->m:I

    .line 86
    iput v3, p0, Lcom/sec/chaton/j/c/c;->n:I

    .line 93
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->p:Ljava/lang/String;

    .line 94
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->q:Ljava/lang/String;

    .line 98
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->r:Ljava/lang/Boolean;

    .line 99
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->s:Lcom/sec/chaton/chat/ChatFragment;

    .line 100
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->t:Ljava/lang/String;

    .line 103
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->u:Landroid/widget/Toast;

    .line 218
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->a:Landroid/os/Handler;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    .line 219
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->b:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    .line 220
    iget-boolean v0, p1, Lcom/sec/chaton/j/c/f;->f:Z

    iput-boolean v0, p0, Lcom/sec/chaton/j/c/c;->d:Z

    .line 221
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    .line 222
    iget-wide v0, p1, Lcom/sec/chaton/j/c/f;->h:J

    iput-wide v0, p0, Lcom/sec/chaton/j/c/c;->f:J

    .line 223
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->i:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->g:Lcom/sec/chaton/e/r;

    .line 224
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->h:Ljava/lang/String;

    .line 225
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    .line 226
    iget v0, p1, Lcom/sec/chaton/j/c/f;->k:I

    iput v0, p0, Lcom/sec/chaton/j/c/c;->m:I

    .line 228
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->l:Lcom/sec/chaton/e/w;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    .line 232
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->p:Ljava/lang/String;

    .line 233
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->q:Ljava/lang/String;

    .line 236
    iget-object v0, p1, Lcom/sec/chaton/j/c/f;->m:Lcom/sec/chaton/chat/ChatFragment;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->s:Lcom/sec/chaton/chat/ChatFragment;

    .line 342
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b5

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->u:Landroid/widget/Toast;

    .line 344
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/j/c/c;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->z:Lcom/sec/chaton/widget/c;

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 21

    .prologue
    .line 488
    .line 493
    const/4 v9, 0x0

    .line 494
    const/4 v8, 0x0

    .line 497
    new-instance v13, Lcom/sec/chaton/j/c/d;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/chaton/j/c/d;-><init>(Lcom/sec/chaton/j/c/c;)V

    .line 539
    new-instance v2, Lcom/sec/chaton/j/j;

    sget-object v3, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    const-string v4, "/file"

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v3, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    .line 544
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "imei="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&buddyid="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->p:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&filename="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 547
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filedownLoadUrl:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/j/c/c;->v:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v4, ""

    .line 551
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v4

    .line 553
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 554
    new-instance v5, Lcom/sec/chaton/util/a;

    invoke-static {v4}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v4}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v5, v6, v4}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 562
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/j/c/c;->d:Z

    if-eqz v4, :cond_4

    .line 563
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&size=239"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 564
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 574
    :goto_0
    const-string v4, "param"

    invoke-virtual {v3, v4, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 577
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/chaton/j/c/c;->i:Z

    .line 578
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/j/c/c;->i:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 579
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    .line 585
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    .line 586
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/j/c/c;->d:Z

    if-eqz v3, :cond_0

    .line 587
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "thumbnail//"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    .line 592
    :cond_0
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    invoke-direct {v14, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 594
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/j/c/c;->r:Ljava/lang/Boolean;

    .line 595
    invoke-static {v14}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 596
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/j/c/c;->t:Ljava/lang/String;

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/c;->t:Ljava/lang/String;

    .line 926
    :cond_1
    :goto_2
    return-object v2

    .line 556
    :cond_2
    :try_start_1
    const-string v2, "Fail in getting a key"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v2, :cond_3

    .line 558
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    invoke-virtual {v2, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 560
    :cond_3
    sget-object v2, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    goto :goto_2

    .line 566
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto/16 :goto_0

    .line 569
    :catch_0
    move-exception v2

    .line 570
    sget-object v3, Lcom/sec/chaton/j/c/c;->v:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 571
    sget-object v2, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    goto :goto_2

    .line 581
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 601
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->q:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?uid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&param="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/j/c/c;->a:Ljava/lang/String;

    .line 602
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Donwload Url : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/c/c;->v:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const-string v2, "[Before connnecting] "

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/j/c/c;->d:Z

    if-nez v2, :cond_7

    .line 608
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/j/c/c;->g:Lcom/sec/chaton/e/r;

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/sec/chaton/e/r;Z)I

    .line 614
    :cond_7
    const-wide/16 v6, 0x0

    .line 615
    const-wide/16 v4, 0x0

    .line 616
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/j/c/c;->c:Ljava/lang/String;

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v3, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 619
    new-instance v2, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 620
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 622
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 623
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 624
    const-string v3, "GET"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 625
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v10, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v3, v10, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v10, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v3, v10, :cond_9

    .line 626
    :cond_8
    const-string v3, "cache-control"

    const-string v10, "no-transform"

    invoke-virtual {v2, v3, v10}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_9
    const/16 v3, 0x7530

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 631
    const/16 v3, 0x7530

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 635
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    invoke-direct {v10, v3, v15}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const/4 v3, 0x0

    .line 638
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_27

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v11

    const-wide/16 v16, 0x0

    cmp-long v11, v11, v16

    if-lez v11, :cond_27

    .line 639
    const-string v3, "Range"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    const/4 v3, 0x1

    .line 641
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v4

    move v10, v3

    move-wide v11, v4

    move-wide v6, v4

    .line 647
    :goto_3
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 650
    const/4 v3, 0x0

    .line 651
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 652
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    const-string v4, "Content-Length"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 656
    :cond_a
    if-eqz v3, :cond_b

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 657
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 658
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_10
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 680
    :cond_b
    :try_start_3
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_11
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 682
    :try_start_4
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/c;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 683
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_c

    .line 684
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 690
    :cond_c
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v3, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 694
    if-eqz v10, :cond_13

    .line 695
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 702
    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v3

    .line 704
    const/16 v8, 0x400

    new-array v8, v8, [B

    .line 706
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/chaton/j/c/c;->n:I

    .line 711
    :goto_5
    invoke-virtual {v5, v8}, Ljava/io/InputStream;->read([B)I

    move-result v10

    const/4 v15, -0x1

    if-eq v10, v15, :cond_17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/j/c/c;->isCancelled()Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_17

    .line 713
    int-to-long v15, v10

    add-long/2addr v6, v15

    .line 715
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    if-nez v15, :cond_d

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/j/c/c;->z:Lcom/sec/chaton/widget/c;

    if-eqz v15, :cond_e

    .line 718
    :cond_d
    if-lez v3, :cond_14

    .line 719
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Integer;

    const/16 v16, 0x0

    const-wide/16 v17, 0x64

    mul-long v17, v17, v6

    int-to-long v0, v3

    move-wide/from16 v19, v0

    add-long v19, v19, v11

    div-long v17, v17, v19

    move-wide/from16 v0, v17

    long-to-int v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/chaton/j/c/c;->publishProgress([Ljava/lang/Object;)V

    .line 729
    :cond_e
    :goto_6
    const/4 v15, 0x0

    invoke-virtual {v4, v8, v15, v10}, Ljava/io/OutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    .line 795
    :catch_1
    move-exception v3

    .line 797
    :goto_7
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 798
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v3, :cond_f

    .line 800
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    invoke-virtual {v3, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 824
    :cond_f
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v6, 0x7f0e0000

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_10

    .line 825
    const-string v3, "01000006"

    const-string v6, "2002"

    invoke-static {v3, v6, v2}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/net/HttpURLConnection;)V

    .line 834
    :cond_10
    sget-object v3, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 838
    :try_start_7
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 902
    if-eqz v5, :cond_11

    .line 903
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    .line 909
    :cond_11
    :goto_8
    if-eqz v4, :cond_12

    .line 910
    :try_start_9
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    :cond_12
    :goto_9
    move-object v2, v3

    .line 834
    goto/16 :goto_2

    .line 698
    :cond_13
    :try_start_a
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    goto/16 :goto_4

    .line 795
    :catch_2
    move-exception v3

    move-object v4, v8

    goto :goto_7

    .line 721
    :cond_14
    :try_start_b
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/chaton/j/c/c;->n:I

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ge v15, v0, :cond_e

    .line 722
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/chaton/j/c/c;->n:I

    add-int/lit8 v15, v15, 0x5

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/chaton/j/c/c;->n:I

    .line 723
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Integer;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/chaton/j/c/c;->n:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/chaton/j/c/c;->publishProgress([Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 838
    :catchall_0
    move-exception v3

    :goto_a
    :try_start_c
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v3
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 884
    :catch_3
    move-exception v2

    move-object v8, v4

    move-object v9, v5

    .line 885
    :goto_b
    :try_start_d
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 887
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v2, :cond_15

    .line 889
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    invoke-virtual {v2, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 896
    :cond_15
    sget-object v2, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 902
    if-eqz v9, :cond_16

    .line 903
    :try_start_e
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    .line 909
    :cond_16
    :goto_c
    if-eqz v8, :cond_1

    .line 910
    :try_start_f
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4

    goto/16 :goto_2

    .line 912
    :catch_4
    move-exception v3

    .line 913
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 735
    :cond_17
    :try_start_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/j/c/c;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 736
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v3, :cond_18

    .line 737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    invoke-virtual {v3, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 739
    :cond_18
    sget-object v3, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 838
    :try_start_11
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 902
    if-eqz v5, :cond_19

    .line 903
    :try_start_12
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a

    .line 909
    :cond_19
    :goto_d
    if-eqz v4, :cond_1a

    .line 910
    :try_start_13
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b

    :cond_1a
    :goto_e
    move-object v2, v3

    .line 739
    goto/16 :goto_2

    .line 753
    :cond_1b
    :try_start_14
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 754
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v6, 0xc8

    if-eq v3, v6, :cond_1c

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v6, 0xce

    if-ne v3, v6, :cond_20

    .line 756
    :cond_1c
    invoke-virtual {v9, v14}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_1f

    .line 757
    invoke-static {v14}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result-object v3

    .line 838
    :goto_f
    :try_start_15
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 879
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "File Downlad Task#2"

    invoke-static {v2, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 902
    if-eqz v5, :cond_1d

    .line 903
    :try_start_16
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_c

    .line 909
    :cond_1d
    :goto_10
    if-eqz v4, :cond_1e

    .line 910
    :try_start_17
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_d

    .line 926
    :cond_1e
    :goto_11
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 759
    :cond_1f
    :try_start_18
    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_f

    .line 762
    :cond_20
    const-string v3, "ResponseCode is NOT 200 OK"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v3, :cond_21

    .line 764
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    invoke-virtual {v3, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 768
    :cond_21
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v6, 0x7f0e0000

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_22

    .line 769
    const-string v3, "01000006"

    const-string v6, "2002"

    invoke-static {v3, v6, v2}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/net/HttpURLConnection;)V

    .line 772
    :cond_22
    sget-object v3, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_1
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 838
    :try_start_19
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_3
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    .line 902
    if-eqz v5, :cond_23

    .line 903
    :try_start_1a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_e

    .line 909
    :cond_23
    :goto_12
    if-eqz v4, :cond_24

    .line 910
    :try_start_1b
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_f

    :cond_24
    :goto_13
    move-object v2, v3

    .line 772
    goto/16 :goto_2

    .line 901
    :catchall_1
    move-exception v2

    .line 902
    :goto_14
    if-eqz v9, :cond_25

    .line 903
    :try_start_1c
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_5

    .line 909
    :cond_25
    :goto_15
    if-eqz v8, :cond_26

    .line 910
    :try_start_1d
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_6

    .line 901
    :cond_26
    :goto_16
    throw v2

    .line 905
    :catch_5
    move-exception v3

    .line 906
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_15

    .line 912
    :catch_6
    move-exception v3

    .line 913
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_16

    .line 905
    :catch_7
    move-exception v3

    .line 906
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_c

    .line 905
    :catch_8
    move-exception v2

    .line 906
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    .line 912
    :catch_9
    move-exception v2

    .line 913
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_9

    .line 905
    :catch_a
    move-exception v2

    .line 906
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_d

    .line 912
    :catch_b
    move-exception v2

    .line 913
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_e

    .line 905
    :catch_c
    move-exception v2

    .line 906
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_10

    .line 912
    :catch_d
    move-exception v2

    .line 913
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_11

    .line 905
    :catch_e
    move-exception v2

    .line 906
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 912
    :catch_f
    move-exception v2

    .line 913
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13

    .line 901
    :catchall_2
    move-exception v2

    move-object v8, v4

    move-object v9, v5

    goto :goto_14

    .line 884
    :catch_10
    move-exception v2

    goto/16 :goto_b

    .line 838
    :catchall_3
    move-exception v3

    move-object v4, v8

    move-object v5, v9

    goto/16 :goto_a

    :catchall_4
    move-exception v3

    move-object v4, v8

    goto/16 :goto_a

    .line 795
    :catch_11
    move-exception v3

    move-object v4, v8

    move-object v5, v9

    goto/16 :goto_7

    :cond_27
    move v10, v3

    move-wide v11, v4

    goto/16 :goto_3
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1146
    iput-object p1, p0, Lcom/sec/chaton/j/c/c;->x:Landroid/os/Handler;

    .line 1147
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 347
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/j/c/c;->d:Z

    if-nez v0, :cond_1

    .line 348
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/el;

    .line 353
    iget-object v1, v0, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    .line 354
    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/sec/chaton/j/c/c;->n:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 357
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    .line 366
    iput-object p1, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    .line 399
    iget-object v0, v0, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->l:Landroid/widget/Button;

    .line 412
    const-string v0, "[setProgressView]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_1
    return-void
.end method

.method public a(Lcom/sec/chaton/widget/c;)V
    .locals 0

    .prologue
    .line 1153
    iput-object p1, p0, Lcom/sec/chaton/j/c/c;->z:Lcom/sec/chaton/widget/c;

    .line 1154
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 1005
    const-string v0, "[onPostExecute]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->x:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->x:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1010
    iget-wide v1, p0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1011
    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1012
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1013
    const-string v2, "download_uri"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1015
    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->x:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1019
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1021
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->c(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 1063
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_9

    .line 1064
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_4

    .line 1065
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1073
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->l:Landroid/widget/Button;

    if-eqz v0, :cond_5

    .line 1074
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->l:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 1077
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    if-eqz v0, :cond_6

    .line 1078
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->w:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/j/c/e;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/j/c/e;-><init>(Lcom/sec/chaton/j/c/c;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1128
    :cond_6
    :goto_2
    return-void

    .line 1023
    :cond_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    iget-wide v3, p0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/chaton/j/c/c;->g:Lcom/sec/chaton/e/r;

    iget-boolean v6, p0, Lcom/sec/chaton/j/c/c;->i:Z

    iget-boolean v7, p0, Lcom/sec/chaton/j/c/c;->d:Z

    move-object v4, p1

    invoke-static/range {v0 .. v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/sec/chaton/e/r;ZZ)I

    .line 1025
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1034
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/a;->d(J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1035
    const-string v0, "Successfully removed from hashmap after down-load"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1056
    :cond_8
    sget-object v0, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_2

    .line 1057
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/j/c/c;->e:Ljava/lang/String;

    iget-wide v2, p0, Lcom/sec/chaton/j/c/c;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/chaton/util/r;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/j/c/c;->h:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1068
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_4

    .line 1069
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 944
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->z:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_0

    .line 945
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->z:Lcom/sec/chaton/widget/c;

    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 949
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/j/c/c;->d:Z

    if-nez v0, :cond_2

    .line 950
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/el;

    .line 952
    iget-wide v0, v0, Lcom/sec/chaton/chat/el;->c:J

    iget-wide v2, p0, Lcom/sec/chaton/j/c/c;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_1

    .line 967
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 969
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 975
    :cond_1
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/j/c/c;->n:I

    .line 990
    :cond_2
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1160
    iget-boolean v0, p0, Lcom/sec/chaton/j/c/c;->d:Z

    return v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/c/c;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 1139
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1140
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/c/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 434
    const-string v0, "[onPreExecute]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/j/c/c;->d:Z

    if-nez v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->o:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_2

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    const v1, 0x7f0701c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 445
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    .line 446
    const-string v0, "FileDownloadTask2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPreExecutemProgessDown"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    :cond_1
    :goto_0
    return-void

    .line 449
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/j/c/c;->y:Landroid/view/View;

    const v1, 0x7f0701c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    .line 454
    iput-object v2, p0, Lcom/sec/chaton/j/c/c;->j:Landroid/widget/ProgressBar;

    .line 455
    const-string v0, "FileDownloadTask2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPreExecutemProgressBalloon"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/c/c;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/c/c;->a([Ljava/lang/Integer;)V

    return-void
.end method
