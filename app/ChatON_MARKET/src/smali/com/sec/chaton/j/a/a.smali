.class public Lcom/sec/chaton/j/a/a;
.super Ljava/lang/Object;
.source "Disconnect.java"


# static fields
.field private static a:Lcom/sec/chaton/j/q;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/j/a/a;->a:Lcom/sec/chaton/j/q;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/a/bu;Lcom/sec/chaton/a/bp;)V
    .locals 5

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/chaton/a/br;->newBuilder()Lcom/sec/chaton/a/bs;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/bs;->a(J)Lcom/sec/chaton/a/bs;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/bp;)Lcom/sec/chaton/a/bs;

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DisconnectReply]UID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MsgID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bs;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", DisconnectActionType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bs;->g()Lcom/sec/chaton/a/bp;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/j/a/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bs;->d()Lcom/sec/chaton/a/br;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 58
    invoke-static {p0, v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 59
    return-void
.end method
