.class public Lcom/sec/chaton/j/ak;
.super Ljava/lang/Object;
.source "TcpContext.java"


# static fields
.field static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/j/ak;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/String;


# instance fields
.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:I

.field protected e:Lcom/sec/chaton/util/a;

.field protected f:J

.field g:Lcom/sec/chaton/j/af;

.field protected h:J

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/chaton/j/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/j/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/chaton/j/an;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    const/4 v2, 0x0

    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object v2, p0, Lcom/sec/chaton/j/ak;->b:Ljava/lang/String;

    .line 235
    iput-object v2, p0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    .line 237
    iput v3, p0, Lcom/sec/chaton/j/ak;->d:I

    .line 239
    iput-object v2, p0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;

    .line 241
    const-wide/32 v0, -0x80000000

    iput-wide v0, p0, Lcom/sec/chaton/j/ak;->f:J

    .line 244
    iput-object v2, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;

    .line 247
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/j/ak;->h:J

    .line 342
    iget-object v0, p1, Lcom/sec/chaton/j/an;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string v1, "No Context ID"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    iget-object v0, p1, Lcom/sec/chaton/j/an;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->b:Ljava/lang/String;

    .line 347
    iget-object v0, p1, Lcom/sec/chaton/j/an;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 348
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string v1, "No Domain"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/j/an;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 350
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string v1, "No Domain actually. Domain length is 0"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/j/an;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    .line 354
    iget v0, p1, Lcom/sec/chaton/j/an;->c:I

    if-ne v0, v3, :cond_3

    .line 355
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string v1, "No Port"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_3
    iget v0, p1, Lcom/sec/chaton/j/an;->c:I

    iput v0, p0, Lcom/sec/chaton/j/ak;->d:I

    .line 359
    iget-object v0, p1, Lcom/sec/chaton/j/an;->d:Lcom/sec/chaton/util/a;

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;

    .line 361
    iget-wide v0, p1, Lcom/sec/chaton/j/an;->e:J

    iput-wide v0, p0, Lcom/sec/chaton/j/ak;->f:J

    .line 364
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/sec/chaton/j/ak;->h:J

    .line 367
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    .line 368
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;

    .line 369
    return-void
.end method

.method static declared-synchronized a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;
    .locals 5

    .prologue
    .line 43
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ak;

    .line 44
    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/sec/chaton/j/af;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/af;-><init>(Lcom/sec/chaton/j/ak;)V

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;

    .line 49
    sget-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    const-string v2, "TcpContext, getClient(), created and inserted. %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 53
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->e()I

    move-result v2

    iput v2, v0, Lcom/sec/chaton/j/ak;->d:I

    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->g()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/chaton/j/ak;->f:J

    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->i()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/chaton/j/ak;->h:J

    .line 64
    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized a()Z
    .locals 4

    .prologue
    .line 188
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/chaton/j/aq;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 189
    new-instance v2, Lcom/sec/chaton/j/am;

    const-string v3, "expiredCollectAll"

    invoke-direct {v2, v0, v3}, Lcom/sec/chaton/j/am;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    .line 197
    invoke-static {v2}, Lcom/sec/chaton/j/aq;->a(Lcom/sec/chaton/j/ar;)Z

    move-result v0

    .line 198
    if-eqz v0, :cond_0

    .line 199
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ar;->a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    :goto_0
    monitor-exit v1

    return v0

    .line 201
    :cond_0
    :try_start_1
    const-string v2, "tcp_queue, TcpContext.expiredCollectAll() : queue offer fail."

    sget-object v3, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized a(I)Z
    .locals 4

    .prologue
    .line 124
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/chaton/j/aq;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 125
    new-instance v2, Lcom/sec/chaton/j/al;

    const-string v3, "closeAll"

    invoke-direct {v2, v0, v3, p0}, Lcom/sec/chaton/j/al;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;I)V

    .line 132
    invoke-static {v2}, Lcom/sec/chaton/j/aq;->a(Lcom/sec/chaton/j/ar;)Z

    move-result v0

    .line 133
    if-eqz v0, :cond_0

    .line 134
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ar;->a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :goto_0
    monitor-exit v1

    return v0

    .line 136
    :cond_0
    :try_start_1
    const-string v2, "tcp_queue, TcpContext.closeAll() : queue offer fail."

    sget-object v3, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized b()I
    .locals 4

    .prologue
    .line 218
    const-class v2, Lcom/sec/chaton/j/ak;

    monitor-enter v2

    const/4 v0, 0x0

    .line 219
    :try_start_0
    sget-object v1, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ak;

    .line 220
    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/af;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 223
    :cond_0
    monitor-exit v2

    return v1

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static declared-synchronized b(I)I
    .locals 4

    .prologue
    .line 154
    const-class v2, Lcom/sec/chaton/j/ak;

    monitor-enter v2

    const/4 v0, 0x0

    .line 158
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 159
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ak;

    .line 160
    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/j/af;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171
    :cond_0
    monitor-exit v2

    return v1

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static declared-synchronized b(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/ak;
    .locals 5

    .prologue
    .line 70
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ak;

    .line 71
    if-nez v0, :cond_0

    .line 72
    sget-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    const-string v2, "TcpContext, getContext(), created and inserted. %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :goto_0
    monitor-exit v1

    return-object p0

    .line 77
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->e()I

    move-result v2

    iput v2, v0, Lcom/sec/chaton/j/ak;->d:I

    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;

    .line 80
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->g()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/chaton/j/ak;->f:J

    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->i()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/chaton/j/ak;->h:J

    .line 83
    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object p0, v0

    .line 84
    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized c(Lcom/sec/chaton/j/ak;)V
    .locals 2

    .prologue
    .line 99
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->h()Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/af;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit v1

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized d(Lcom/sec/chaton/j/ak;)V
    .locals 5

    .prologue
    .line 110
    const-class v1, Lcom/sec/chaton/j/ak;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/ak;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ak;

    .line 111
    if-eqz v0, :cond_0

    .line 112
    invoke-direct {v0}, Lcom/sec/chaton/j/ak;->j()V

    .line 113
    sget-object v0, Lcom/sec/chaton/j/ak;->i:Ljava/lang/String;

    const-string v2, "TcpContext, removeTcpContext() %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeTcpContext: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "TcpContext"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit v1

    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 468
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILcom/sec/chaton/j/q;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 437
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->b(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/ak;

    move-result-object v0

    .line 438
    iput-wide p1, p0, Lcom/sec/chaton/j/ak;->h:J

    .line 439
    iput-wide p1, v0, Lcom/sec/chaton/j/ak;->h:J

    .line 440
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    .line 420
    iput p2, p0, Lcom/sec/chaton/j/ak;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([B[B)V
    .locals 1

    .prologue
    .line 447
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/sec/chaton/util/a;

    invoke-direct {v0, p1, p2}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    iput-object v0, p0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    monitor-exit p0

    return-void

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)Lcom/sec/chaton/j/q;
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->j:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/q;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 4

    .prologue
    .line 380
    const-string v0, "wifi_80_port"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/common/util/i;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 381
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "wifi_port"

    iget v2, p0, Lcom/sec/chaton/j/ak;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 382
    sget v1, Lcom/sec/chaton/util/bi;->a:I

    if-ne v0, v1, :cond_1

    .line 383
    iget v0, p0, Lcom/sec/chaton/j/ak;->d:I

    .line 391
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[WIFI 80] WiFi "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TcpContext"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :goto_1
    return v0

    .line 384
    :cond_1
    sget v1, Lcom/sec/chaton/util/bi;->b:I

    if-ne v0, v1, :cond_0

    .line 385
    invoke-static {}, Lcom/sec/chaton/util/bi;->d()I

    move-result v0

    .line 386
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "wifi_port"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 387
    sget v1, Lcom/sec/chaton/util/bi;->b:I

    if-ne v0, v1, :cond_0

    .line 388
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "primary_message_wifi_port"

    const/16 v2, 0x50

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 394
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[WIFI 80] dataNetwork getPort::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/j/ak;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TcpContext"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget v0, p0, Lcom/sec/chaton/j/ak;->d:I

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 258
    :try_start_0
    instance-of v1, p1, Lcom/sec/chaton/j/ak;

    if-eqz v1, :cond_0

    .line 259
    check-cast p1, Lcom/sec/chaton/j/ak;

    .line 261
    iget-object v1, p0, Lcom/sec/chaton/j/ak;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/j/ak;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 262
    const/4 v0, 0x1

    .line 267
    :cond_0
    :goto_0
    return v0

    .line 266
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/util/a;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->e:Lcom/sec/chaton/util/a;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 403
    iget-wide v0, p0, Lcom/sec/chaton/j/ak;->f:J

    return-wide v0
.end method

.method h()Lcom/sec/chaton/j/af;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/chaton/j/ak;->g:Lcom/sec/chaton/j/af;

    return-object v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 412
    iget-wide v0, p0, Lcom/sec/chaton/j/ak;->h:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 89
    const-string v0, "%s : id(%s), domain(%s), port(%d), expireTime(%d), aliveTime(%d), "

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/j/ak;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/chaton/j/ak;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/sec/chaton/j/ak;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/sec/chaton/j/ak;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/sec/chaton/j/ak;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
