.class public Lcom/sec/chaton/j/j;
.super Ljava/lang/Object;
.source "HttpEnvelope.java"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/sec/chaton/util/cg;

.field protected c:Lcom/sec/chaton/j/k;

.field protected d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field protected f:I

.field protected g:Ljava/lang/String;

.field protected h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected i:Z

.field protected j:Z

.field protected k:I

.field protected l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field protected m:Lcom/sec/common/d/a/a;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    sget-object v0, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    iput-object v0, p0, Lcom/sec/chaton/j/j;->c:Lcom/sec/chaton/j/k;

    .line 202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    .line 205
    const-string v0, "text/xml"

    iput-object v0, p0, Lcom/sec/chaton/j/j;->g:Ljava/lang/String;

    .line 206
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/j;->h:Ljava/util/Map;

    .line 209
    const/16 v0, 0x7530

    iput v0, p0, Lcom/sec/chaton/j/j;->k:I

    .line 212
    const-class v0, Lcom/sec/chaton/util/af;

    iput-object v0, p0, Lcom/sec/chaton/j/j;->l:Ljava/lang/Class;

    .line 215
    new-instance v0, Lcom/sec/chaton/io/a/a;

    invoke-direct {v0}, Lcom/sec/chaton/io/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/j;->m:Lcom/sec/common/d/a/a;

    .line 218
    iput-object p1, p0, Lcom/sec/chaton/j/j;->b:Lcom/sec/chaton/util/cg;

    .line 219
    iput-object p2, p0, Lcom/sec/chaton/j/j;->a:Ljava/lang/String;

    .line 220
    iput-boolean v1, p0, Lcom/sec/chaton/j/j;->i:Z

    .line 221
    iput-boolean v1, p0, Lcom/sec/chaton/j/j;->j:Z

    .line 222
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/j/h;
    .locals 2

    .prologue
    .line 276
    new-instance v0, Lcom/sec/chaton/j/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/j/h;-><init>(Lcom/sec/chaton/j/j;Lcom/sec/chaton/j/i;)V

    return-object v0
.end method

.method public a(I)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 266
    iput p1, p0, Lcom/sec/chaton/j/j;->f:I

    .line 267
    return-object p0
.end method

.method public a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/chaton/j/j;->c:Lcom/sec/chaton/j/k;

    .line 234
    return-object p0
.end method

.method public a(Lcom/sec/common/d/a/a;)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/chaton/j/j;->m:Lcom/sec/common/d/a/a;

    .line 286
    return-object p0
.end method

.method public a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/sec/chaton/j/j;"
        }
    .end annotation

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/chaton/j/j;->e:Ljava/lang/Class;

    .line 262
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/chaton/j/j;->g:Ljava/lang/String;

    .line 272
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;
    .locals 1

    .prologue
    .line 243
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/j/j;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 291
    iput-boolean p1, p0, Lcom/sec/chaton/j/j;->i:Z

    .line 292
    return-object p0
.end method

.method public b(I)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 303
    iput p1, p0, Lcom/sec/chaton/j/j;->k:I

    .line 304
    return-object p0
.end method

.method public b(Ljava/lang/Class;)Lcom/sec/chaton/j/j;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/sec/chaton/j/j;"
        }
    .end annotation

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/chaton/j/j;->l:Ljava/lang/Class;

    .line 281
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;
    .locals 1

    .prologue
    .line 250
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 251
    const-string v0, "Content-Type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    invoke-virtual {p0, p2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 257
    :cond_0
    :goto_0
    return-object p0

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/j/j;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public b(Z)Lcom/sec/chaton/j/j;
    .locals 0

    .prologue
    .line 297
    iput-boolean p1, p0, Lcom/sec/chaton/j/j;->j:Z

    .line 298
    return-object p0
.end method
