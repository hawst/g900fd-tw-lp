.class public Lcom/sec/chaton/j/af;
.super Ljava/lang/Object;
.source "TcpClient.java"


# instance fields
.field protected a:Ljava/util/concurrent/locks/Lock;

.field protected b:Lcom/sec/chaton/j/ak;

.field protected c:Lcom/sec/chaton/j/d/g;

.field protected d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

.field e:Lorg/jboss/netty/channel/ChannelFuture;

.field protected final f:Ljava/lang/Object;

.field protected g:Lcom/sec/chaton/j/aj;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/j/ak;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    .line 66
    iput-object p1, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/j/af;Lcom/sec/chaton/j/ao;Z)J
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ao;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lcom/sec/chaton/j/ao;Z)J
    .locals 4

    .prologue
    .line 515
    const-wide/16 v0, -0x1

    .line 517
    const-string v2, "TcpClient.lockSend() - STT"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v2, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 522
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    invoke-virtual {v2}, Lcom/sec/chaton/j/d/g;->b()Z

    move-result v2

    if-nez v2, :cond_3

    .line 523
    :cond_0
    if-nez p2, :cond_1

    .line 524
    const-string v2, "TcpClient.lockSend() - END with no auto-connect. try tcp error callback"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/16 v2, 0x18

    invoke-virtual {p1, v2, p1}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 558
    iget-object v2, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    :goto_0
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 563
    :goto_1
    return-wide v0

    .line 528
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 529
    :try_start_2
    sget-object v3, Lcom/sec/chaton/j/aj;->b:Lcom/sec/chaton/j/aj;

    iput-object v3, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 530
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 531
    :try_start_3
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->j()Z

    .line 532
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->h()Lorg/jboss/netty/channel/ChannelFuture;

    move-result-object v2

    .line 534
    if-eqz v2, :cond_2

    invoke-interface {v2}, Lorg/jboss/netty/channel/ChannelFuture;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_3

    .line 535
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 536
    :try_start_4
    sget-object v3, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    iput-object v3, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 537
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 538
    :try_start_5
    const-string v2, "TcpClient.lockSend() - END with auto-connect fail. try tcp error callback"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const/16 v2, 0x17

    invoke-virtual {p1, v2, p1}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 558
    iget-object v2, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    goto :goto_0

    .line 530
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 558
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 537
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0

    .line 543
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    if-eqz v2, :cond_4

    .line 544
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 545
    :try_start_a
    sget-object v0, Lcom/sec/chaton/j/aj;->c:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 546
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 547
    :try_start_b
    const-string v0, "TcpClient.lockSend(): write req"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/j/d/g;->a(Lcom/sec/chaton/j/ao;)J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-wide v0

    .line 558
    :goto_2
    iget-object v2, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 561
    const-string v2, "TcpClient.lockSend() - END"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 546
    :catchall_3
    move-exception v0

    :try_start_c
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v0

    .line 550
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 551
    :try_start_e
    sget-object v3, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    iput-object v3, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 552
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 554
    :try_start_f
    const-string v2, "TcpClient.lockSend(): The connection was not established."

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const/16 v2, 0x18

    invoke-virtual {p1, v2, p1}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto :goto_2

    .line 552
    :catchall_4
    move-exception v0

    :try_start_10
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1
.end method

.method static synthetic a(Lcom/sec/chaton/j/af;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->g()V

    return-void
.end method

.method public static a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V
    .locals 6

    .prologue
    .line 447
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/j/aq;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    const-wide/32 v3, 0x80e8

    const/4 v5, 0x1

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/j/af;->a(Ljava/util/concurrent/ExecutorService;Lcom/sec/chaton/j/ao;JZ)V

    .line 448
    return-void
.end method

.method public static a(Lcom/sec/chaton/j/ak;)Z
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/af;->a()Z

    move-result v0

    return v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 405
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 406
    :try_start_1
    sget-object v0, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 407
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/j/af;->c(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 412
    return-void

    .line 407
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 410
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic b(Lcom/sec/chaton/j/af;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->i()V

    return-void
.end method

.method public static b(Lcom/sec/chaton/j/ak;)Z
    .locals 1

    .prologue
    .line 114
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/af;->b()Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/sec/chaton/j/ak;)V
    .locals 4

    .prologue
    .line 160
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->a(Lcom/sec/chaton/j/ak;)Lcom/sec/chaton/j/af;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/j/aq;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    const-wide/32 v2, 0x80e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/j/af;->a(Ljava/util/concurrent/ExecutorService;J)V

    .line 161
    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 330
    const/16 v0, 0x22

    invoke-static {v0}, Lcom/sec/chaton/j/ak;->a(I)Z

    move-result v0

    return v0
.end method

.method private c(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 419
    const-string v0, "TcpClient.nonLockClose() - STT"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    if-eqz v0, :cond_0

    .line 422
    const-string v0, "TcpClient.nonLockClose() - mChannelHandler"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/j/d/g;->a(I)Lorg/jboss/netty/channel/group/ChannelGroupFuture;

    .line 424
    iput-object v2, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    if-eqz v0, :cond_1

    .line 428
    const-string v0, "TcpClient.nonLockClose() - mBootstrap"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    invoke-virtual {v0}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->releaseExternalResources()V

    .line 430
    iput-object v2, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    .line 433
    :cond_1
    const-string v0, "TcpClient.nonLockClose() - END"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const/4 v0, 0x1

    return v0
.end method

.method public static d(Lcom/sec/chaton/j/ak;)V
    .locals 0

    .prologue
    .line 337
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->c(Lcom/sec/chaton/j/ak;)V

    .line 338
    return-void
.end method

.method public static e(Lcom/sec/chaton/j/ak;)V
    .locals 0

    .prologue
    .line 347
    invoke-static {p0}, Lcom/sec/chaton/j/ak;->d(Lcom/sec/chaton/j/ak;)V

    .line 348
    return-void
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 580
    invoke-static {}, Lcom/sec/chaton/j/ak;->a()Z

    move-result v0

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 219
    const-string v0, "TcpClient.lockReconnect() - STT"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 223
    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/aj;->b:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 224
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 231
    :try_start_1
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->j()Z

    .line 232
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->h()Lorg/jboss/netty/channel/ChannelFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 234
    iget-object v1, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 237
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/jboss/netty/channel/ChannelFuture;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_2
    sget-object v0, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 240
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 241
    const-string v0, "TcpClient.lockReconnect() - END"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :goto_0
    return-void

    .line 224
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 234
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 240
    :catchall_2
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_5
    sget-object v0, Lcom/sec/chaton/j/aj;->c:Lcom/sec/chaton/j/aj;

    iput-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    .line 248
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 249
    const-string v0, "TcpClient.lockReconnect() - END"

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :catchall_3
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0
.end method

.method private h()Lorg/jboss/netty/channel/ChannelFuture;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 257
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    invoke-virtual {v2}, Lcom/sec/chaton/j/ak;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 258
    if-gez v1, :cond_0

    .line 259
    packed-switch v1, :pswitch_data_0

    .line 267
    :pswitch_0
    const-string v1, "TcpClient.nonLockConnect(): unchecked"

    const-string v2, "TcpClient"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-object v0

    .line 261
    :pswitch_1
    const-string v1, "TcpClient.nonLockConnect(): Network isn\'t available."

    const-string v2, "TcpClient"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 314
    :catch_0
    move-exception v1

    .line 315
    const-string v2, "TcpClient"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :pswitch_2
    :try_start_1
    const-string v1, "TcpClient.nonLockConnect(): host lookup failed."

    const-string v2, "TcpClient"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    if-nez v1, :cond_2

    .line 275
    :cond_1
    new-instance v1, Lorg/jboss/netty/channel/socket/nio/NioClientSocketChannelFactory;

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/jboss/netty/channel/socket/nio/NioClientSocketChannelFactory;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 276
    new-instance v2, Lorg/jboss/netty/bootstrap/ClientBootstrap;

    invoke-direct {v2, v1}, Lorg/jboss/netty/bootstrap/ClientBootstrap;-><init>(Lorg/jboss/netty/channel/ChannelFactory;)V

    iput-object v2, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    .line 277
    new-instance v1, Lcom/sec/chaton/j/d/g;

    iget-object v2, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    iget-object v3, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/j/d/g;-><init>(Lcom/sec/chaton/j/ak;Lorg/jboss/netty/bootstrap/ClientBootstrap;)V

    iput-object v1, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    .line 278
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    new-instance v2, Lcom/sec/chaton/j/d/i;

    iget-object v3, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    iget-object v4, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/j/d/i;-><init>(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/d/g;)V

    invoke-virtual {v2}, Lcom/sec/chaton/j/d/i;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->setPipeline(Lorg/jboss/netty/channel/ChannelPipeline;)V

    .line 280
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    const-string v2, "tcpNoDelay"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->setOption(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    const-string v2, "keepAlive"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->setOption(Ljava/lang/String;Ljava/lang/Object;)V

    .line 282
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    const-string v2, "connectTimeoutMillis"

    invoke-static {}, Lcom/sec/chaton/c/g;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->setOption(Ljava/lang/String;Ljava/lang/Object;)V

    .line 285
    :cond_2
    const-string v1, "TcpClient.nonLockConnect(): Connetion Tried"

    const-string v2, "TcpClient"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lcom/sec/chaton/j/af;->d:Lorg/jboss/netty/bootstrap/ClientBootstrap;

    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    invoke-virtual {v3}, Lcom/sec/chaton/j/ak;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/j/af;->b:Lcom/sec/chaton/j/ak;

    invoke-virtual {v4}, Lcom/sec/chaton/j/ak;->e()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lorg/jboss/netty/bootstrap/ClientBootstrap;->connect(Ljava/net/SocketAddress;)Lorg/jboss/netty/channel/ChannelFuture;

    move-result-object v1

    .line 288
    iput-object v1, p0, Lcom/sec/chaton/j/af;->e:Lorg/jboss/netty/channel/ChannelFuture;

    .line 294
    invoke-interface {v1}, Lorg/jboss/netty/channel/ChannelFuture;->awaitUninterruptibly()Lorg/jboss/netty/channel/ChannelFuture;

    .line 295
    invoke-interface {v1}, Lorg/jboss/netty/channel/ChannelFuture;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_4

    .line 296
    const-string v2, "TcpClient.nonLockConnect(): Connetion Fail"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-interface {v1}, Lorg/jboss/netty/channel/ChannelFuture;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 298
    const-string v2, "Point 1"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-interface {v1}, Lorg/jboss/netty/channel/ChannelFuture;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 302
    :cond_3
    const-string v2, "Point 2"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->j()Z

    :goto_1
    move-object v0, v1

    .line 313
    goto/16 :goto_0

    .line 305
    :cond_4
    invoke-interface {v1}, Lorg/jboss/netty/channel/ChannelFuture;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 306
    const-string v2, "TcpClient.nonLockConnect(): Connetion Cancelled"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-direct {p0}, Lcom/sec/chaton/j/af;->j()Z

    goto :goto_1

    .line 309
    :cond_5
    const-string v2, "TcpClient.nonLockConnect(): Connetion Success"

    const-string v3, "TcpClient"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 259
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private i()V
    .locals 1

    .prologue
    .line 399
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/sec/chaton/j/af;->b(I)V

    .line 400
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 415
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/sec/chaton/j/af;->c(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0, p1}, Lcom/sec/chaton/j/af;->b(I)V

    .line 396
    return-void
.end method

.method public a(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 365
    new-instance v0, Lcom/sec/chaton/j/ah;

    const-string v1, "closeIMPL"

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/chaton/j/ah;-><init>(Lcom/sec/chaton/j/af;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    .line 373
    invoke-static {v0}, Lcom/sec/chaton/j/aq;->a(Lcom/sec/chaton/j/ar;)Z

    move-result v1

    .line 374
    if-eqz v1, :cond_1

    .line 375
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ar;->a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 377
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 378
    const-string v0, "tcp_queue, TcpClient.closeIMPL() : queue offer fail."

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/util/concurrent/ExecutorService;J)V
    .locals 8

    .prologue
    .line 179
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 181
    new-instance v0, Lcom/sec/chaton/j/ag;

    const-string v3, "reconnectIMPL"

    move-object v1, p0

    move-object v2, p1

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/j/ag;-><init>(Lcom/sec/chaton/j/af;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;JJ)V

    .line 195
    invoke-static {v0}, Lcom/sec/chaton/j/aq;->a(Lcom/sec/chaton/j/ar;)Z

    move-result v1

    .line 196
    if-eqz v1, :cond_1

    .line 197
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ar;->a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "tcp_queue, TcpClient.reconnectIMPL() : queue offer fail."

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/util/concurrent/ExecutorService;Lcom/sec/chaton/j/ao;JZ)V
    .locals 10

    .prologue
    .line 473
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendIMPL - offerTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/4 v3, 0x0

    .line 477
    invoke-virtual {p2}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {p2}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    .line 481
    :cond_0
    new-instance v0, Lcom/sec/chaton/j/ai;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-wide v7, p3

    move v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/j/ai;-><init>(Lcom/sec/chaton/j/af;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Lcom/sec/chaton/j/ao;JJZ)V

    .line 503
    invoke-static {v0}, Lcom/sec/chaton/j/aq;->a(Lcom/sec/chaton/j/ar;)Z

    move-result v1

    .line 504
    if-eqz v1, :cond_1

    .line 505
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ar;->a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;

    .line 512
    :goto_0
    return-void

    .line 507
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 508
    const-string v0, "tcp_queue, TcpClient.sendIMPL() : queue offer fail. try tcp error callback."

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_2
    const/16 v0, 0x21

    invoke-virtual {p2, v0, p2}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 96
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    sget-object v2, Lcom/sec/chaton/j/aj;->c:Lcom/sec/chaton/j/aj;

    if-ne v0, v2, :cond_0

    .line 98
    const/4 v0, 0x1

    monitor-exit v1

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 138
    iget-object v1, p0, Lcom/sec/chaton/j/af;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/af;->g:Lcom/sec/chaton/j/aj;

    sget-object v2, Lcom/sec/chaton/j/aj;->a:Lcom/sec/chaton/j/aj;

    if-ne v0, v2, :cond_0

    .line 140
    const/4 v0, 0x0

    monitor-exit v1

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 341
    invoke-static {}, Lcom/sec/chaton/j/aq;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/af;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 342
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 610
    iget-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 612
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/chaton/j/af;->c:Lcom/sec/chaton/j/d/g;

    invoke-virtual {v0}, Lcom/sec/chaton/j/d/g;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 618
    return-void

    .line 616
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/af;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
