.class Lcom/sec/chaton/j/ai;
.super Lcom/sec/chaton/j/ar;
.source "TcpClient.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/j/ao;

.field final synthetic b:J

.field final synthetic c:J

.field final synthetic d:Z

.field final synthetic e:Lcom/sec/chaton/j/af;


# direct methods
.method constructor <init>(Lcom/sec/chaton/j/af;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Lcom/sec/chaton/j/ao;JJZ)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/sec/chaton/j/ai;->e:Lcom/sec/chaton/j/af;

    iput-object p4, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    iput-wide p5, p0, Lcom/sec/chaton/j/ai;->b:J

    iput-wide p7, p0, Lcom/sec/chaton/j/ai;->c:J

    iput-boolean p9, p0, Lcom/sec/chaton/j/ai;->d:Z

    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/j/ar;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 490
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 491
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tcp_queue, sendIMPL - executeTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-wide v2, p0, Lcom/sec/chaton/j/ai;->b:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/sec/chaton/j/ai;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 493
    const-string v0, "tcp_queue, TcpClient.sendIMPL().run() : waiting timeout. try tcp error callback."

    const-string v1, "TcpClient"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V

    .line 495
    const/4 v0, 0x0

    .line 499
    :goto_0
    return-object v0

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/ai;->e:Lcom/sec/chaton/j/af;

    iget-object v1, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    iget-boolean v2, p0, Lcom/sec/chaton/j/ai;->d:Z

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/af;Lcom/sec/chaton/j/ao;Z)J

    .line 499
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic b([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 481
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/ai;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    const/16 v1, 0x23

    iget-object v2, p0, Lcom/sec/chaton/j/ai;->a:Lcom/sec/chaton/j/ao;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V

    .line 485
    invoke-super {p0}, Lcom/sec/chaton/j/ar;->c()V

    .line 486
    return-void
.end method
