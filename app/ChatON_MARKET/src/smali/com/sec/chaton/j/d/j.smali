.class public Lcom/sec/chaton/j/d/j;
.super Lcom/sec/chaton/j/d/c;
.source "TcpEnvelopeBicoder.java"


# instance fields
.field protected a:Lcom/sec/chaton/j/ak;

.field private b:Landroid/os/PowerManager$WakeLock;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/j/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/util/a;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/j/ak;Landroid/os/PowerManager$WakeLock;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/j/d/c;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    .line 43
    iput-object p1, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    .line 44
    iput-object p2, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    .line 47
    return-void
.end method

.method private a(Lcom/sec/chaton/j/d/a;)Lcom/sec/chaton/j/ao;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 108
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->b()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    .line 110
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v0

    iget-object v2, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->d()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/a;->c([B)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/j/g;->a(I[B)Lcom/google/protobuf/GeneratedMessageLite;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 125
    :goto_0
    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v2

    invoke-static {v2, v0}, Lcom/sec/chaton/j/g;->a(ILcom/google/protobuf/GeneratedMessageLite;)J

    move-result-wide v2

    .line 128
    new-instance v4, Lcom/sec/chaton/j/ap;

    invoke-direct {v4}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/j/ap;->a(Ljava/lang/String;)Lcom/sec/chaton/j/ap;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v4

    .line 132
    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/j/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 138
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received msgID is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    const-wide/16 v5, -0x1

    cmp-long v0, v2, v5

    if-nez v0, :cond_3

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    const/16 v2, 0x270f

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/ak;->c(I)Lcom/sec/chaton/j/q;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/sec/chaton/j/q;)Lcom/sec/chaton/j/ap;

    .line 148
    invoke-virtual {v4}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 149
    invoke-virtual {v0, v1, v0}, Lcom/sec/chaton/j/ao;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 210
    :goto_1
    return-object v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_2
    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/Throwable;)V

    .line 113
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 114
    const-string v0, "StreamEnvelopeDecoder.toRevelope(): \uc554\ud638\ud654 \ud574\uc11d \uc2e4\ud328"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    .line 116
    goto :goto_1

    .line 119
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->d()[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/j/g;->a(I[B)Lcom/google/protobuf/GeneratedMessageLite;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 121
    :catch_1
    move-exception v0

    .line 122
    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 123
    goto :goto_1

    .line 151
    :cond_3
    invoke-virtual {p0, v2, v3}, Lcom/sec/chaton/j/d/j;->a(J)Lcom/sec/chaton/j/ao;

    move-result-object v2

    .line 152
    if-nez v2, :cond_5

    .line 153
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".toRevelope(): \uc774 \uc751\ub2f5\uc5d0 \ub300\uc751\ub418\ub294 \uc9c8\ubb38\uc744 \ud55c \uae30\ub85d\uc774 \uc5c6\uc74c."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 156
    goto :goto_1

    .line 158
    :cond_5
    invoke-virtual {v2}, Lcom/sec/chaton/j/ao;->e()Lcom/sec/chaton/j/q;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/sec/chaton/j/q;)Lcom/sec/chaton/j/ap;

    .line 160
    invoke-virtual {v4}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 162
    invoke-virtual {v0, v2, v0}, Lcom/sec/chaton/j/ao;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 167
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_7

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    invoke-virtual {p1}, Lcom/sec/chaton/j/d/a;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/ak;->c(I)Lcom/sec/chaton/j/q;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_8

    .line 170
    invoke-virtual {v4, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/sec/chaton/j/q;)Lcom/sec/chaton/j/ap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 206
    :cond_7
    :goto_2
    invoke-virtual {v4}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 208
    invoke-virtual {v0, v1, v0}, Lcom/sec/chaton/j/ao;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 172
    :cond_8
    :try_start_4
    const-string v0, "Should not reach here."

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 202
    :catch_2
    move-exception v0

    .line 203
    const-string v2, "TcpEnvelopeBicoder"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized a(J)Lcom/sec/chaton/j/ao;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    .line 222
    if-nez v0, :cond_2

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".extractReqFormHistory() -> already handled error callback"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move-object v0, v1

    .line 243
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 228
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 229
    goto :goto_0

    .line 239
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->b()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 240
    const-string v0, "Should not reach here."

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 241
    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 55
    :cond_0
    :try_start_0
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 57
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/j/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    invoke-virtual {p0, p3}, Lcom/sec/chaton/j/d/j;->a(Lcom/sec/chaton/j/ao;)V

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_2

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->a:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    if-nez v0, :cond_5

    .line 70
    new-instance v0, Lcom/sec/chaton/j/d/a;

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v3

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/GeneratedMessageLite;->toByteArray()[B

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/j/d/a;-><init>(Ljava/lang/String;II[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_3

    .line 75
    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 78
    :cond_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".encode() - END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_4
    return-object v0

    :cond_5
    :try_start_1
    new-instance v0, Lcom/sec/chaton/j/d/a;

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/j/d/j;->d:Lcom/sec/chaton/util/a;

    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/protobuf/GeneratedMessageLite;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/j/d/a;-><init>(Ljava/lang/String;II[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_6

    .line 75
    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 78
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".encode() - END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_7

    .line 75
    iget-object v1, p0, Lcom/sec/chaton/j/d/j;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 78
    :cond_7
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_8

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".encode() - END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_8
    throw v0
.end method

.method public declared-synchronized a()V
    .locals 9

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "expiredCollector() - STT"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 278
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    const/4 v0, 0x0

    .line 282
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    .line 283
    if-eqz v0, :cond_1

    .line 286
    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/j/ao;->a(J)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 290
    add-int/lit8 v1, v1, 0x1

    .line 291
    iget-object v5, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    const-string v6, "[ExpiredCollector] TimeOut Error= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "MsgID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "API type : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/chaton/j/ao;->b()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/16 v5, 0x15

    invoke-virtual {v0, v5, v0}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 299
    :cond_2
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expiredCollector() : collected "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 306
    const-string v0, "expiredCollector() - END"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/ao;

    .line 255
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 256
    const-string v2, "TcpEnvelopeBicoder.kickHistory() -> try tcp error callback"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    invoke-virtual {v0, p1, v0}, Lcom/sec/chaton/j/ao;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 260
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/sec/chaton/j/ao;)V
    .locals 3

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/j/d/j;->c:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/chaton/j/ao;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 87
    :try_start_0
    check-cast p3, Lcom/sec/chaton/j/d/a;

    .line 88
    invoke-interface {p2}, Lorg/jboss/netty/channel/Channel;->getRemoteAddress()Ljava/net/SocketAddress;

    .line 89
    invoke-direct {p0, p3}, Lcom/sec/chaton/j/d/j;->a(Lcom/sec/chaton/j/d/a;)Lcom/sec/chaton/j/ao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/Throwable;)V

    .line 92
    const/4 v0, 0x0

    goto :goto_0
.end method
