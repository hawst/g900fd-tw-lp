.class public Lcom/sec/chaton/j/c/f;
.super Ljava/lang/Object;
.source "FileDownloadTask2.java"


# instance fields
.field protected a:Landroid/os/Handler;

.field protected b:Landroid/view/View;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Z

.field protected g:Ljava/lang/String;

.field protected h:J

.field protected i:Lcom/sec/chaton/e/r;

.field protected j:Ljava/lang/String;

.field protected k:I

.field protected l:Lcom/sec/chaton/e/w;

.field m:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->a:Landroid/os/Handler;

    .line 108
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->b:Landroid/view/View;

    .line 109
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->c:Ljava/lang/String;

    .line 110
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->d:Ljava/lang/String;

    .line 111
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->e:Ljava/lang/String;

    .line 112
    iput-boolean v3, p0, Lcom/sec/chaton/j/c/f;->f:Z

    .line 113
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->g:Ljava/lang/String;

    .line 114
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/j/c/f;->h:J

    .line 115
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/j/c/f;->i:Lcom/sec/chaton/e/r;

    .line 116
    iput-object v2, p0, Lcom/sec/chaton/j/c/f;->j:Ljava/lang/String;

    .line 117
    iput v3, p0, Lcom/sec/chaton/j/c/f;->k:I

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/j/c/c;
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/sec/chaton/j/c/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/c/c;-><init>(Lcom/sec/chaton/j/c/f;)V

    return-object v0
.end method

.method public a(I)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lcom/sec/chaton/j/c/f;->k:I

    .line 187
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/sec/chaton/j/c/f;->h:J

    .line 170
    return-object p0
.end method

.method public a(Landroid/os/Handler;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->a:Landroid/os/Handler;

    .line 130
    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->b:Landroid/view/View;

    .line 135
    return-object p0
.end method

.method public a(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->m:Lcom/sec/chaton/chat/ChatFragment;

    .line 201
    return-object p0
.end method

.method public a(Lcom/sec/chaton/e/r;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->i:Lcom/sec/chaton/e/r;

    .line 176
    return-object p0
.end method

.method public a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->l:Lcom/sec/chaton/e/w;

    .line 193
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->c:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/j/c/f;->f:Z

    .line 158
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->d:Ljava/lang/String;

    .line 146
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->e:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->g:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/chaton/j/c/f;
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/chaton/j/c/f;->j:Ljava/lang/String;

    .line 182
    return-object p0
.end method
