.class public abstract Lcom/sec/chaton/j/x;
.super Ljava/lang/Object;
.source "TcpBaseWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/sec/chaton/j/ac;


# instance fields
.field private c:Lcom/sec/chaton/j/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/j/ae",
            "<TParams;TResult;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<TResult;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/concurrent/ExecutorService;

.field private f:Lcom/sec/chaton/j/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/j/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/x;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/sec/chaton/j/ac;

    invoke-direct {v0}, Lcom/sec/chaton/j/ac;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/x;->b:Lcom/sec/chaton/j/ac;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/sec/chaton/j/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/y;-><init>(Lcom/sec/chaton/j/x;)V

    iput-object v0, p0, Lcom/sec/chaton/j/x;->c:Lcom/sec/chaton/j/ae;

    .line 54
    new-instance v0, Lcom/sec/chaton/j/z;

    iget-object v1, p0, Lcom/sec/chaton/j/x;->c:Lcom/sec/chaton/j/ae;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/j/z;-><init>(Lcom/sec/chaton/j/x;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lcom/sec/chaton/j/x;->d:Ljava/util/concurrent/FutureTask;

    .line 83
    iput-object p1, p0, Lcom/sec/chaton/j/x;->e:Ljava/util/concurrent/ExecutorService;

    .line 85
    sget-object v0, Lcom/sec/chaton/j/ad;->a:Lcom/sec/chaton/j/ad;

    iput-object v0, p0, Lcom/sec/chaton/j/x;->f:Lcom/sec/chaton/j/ad;

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/j/x;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/chaton/j/x;->b(Ljava/lang/Object;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/j/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 p1, 0x0

    .line 144
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/j/x;->a(Ljava/lang/Object;)V

    .line 145
    sget-object v0, Lcom/sec/chaton/j/ad;->c:Lcom/sec/chaton/j/ad;

    iput-object v0, p0, Lcom/sec/chaton/j/x;->f:Lcom/sec/chaton/j/ad;

    .line 146
    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/chaton/j/x;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e()Lcom/sec/chaton/j/ac;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/chaton/j/x;->b:Lcom/sec/chaton/j/ac;

    return-object v0
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)Lcom/sec/chaton/j/x;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/sec/chaton/j/x",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/chaton/j/x;->f:Lcom/sec/chaton/j/ad;

    sget-object v1, Lcom/sec/chaton/j/ad;->a:Lcom/sec/chaton/j/ad;

    if-eq v0, v1, :cond_0

    .line 110
    sget-object v0, Lcom/sec/chaton/j/aa;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/j/x;->f:Lcom/sec/chaton/j/ad;

    invoke-virtual {v1}, Lcom/sec/chaton/j/ad;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 118
    :cond_0
    sget-object v0, Lcom/sec/chaton/j/ad;->b:Lcom/sec/chaton/j/ad;

    iput-object v0, p0, Lcom/sec/chaton/j/x;->f:Lcom/sec/chaton/j/ad;

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/j/x;->b()V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/j/x;->c:Lcom/sec/chaton/j/ae;

    iput-object p1, v0, Lcom/sec/chaton/j/ae;->b:[Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/j/x;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/sec/chaton/j/x;->d:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 125
    return-object p0

    .line 112
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 134
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/chaton/j/x;->d:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public a(Z)Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/chaton/j/x;->d:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method protected varargs abstract b([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method
