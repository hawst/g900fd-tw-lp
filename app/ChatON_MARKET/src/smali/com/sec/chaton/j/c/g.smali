.class public Lcom/sec/chaton/j/c/g;
.super Ljava/lang/Object;
.source "FileUploadManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static c:Lcom/sec/chaton/j/c/g;


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/j/c/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/j/c/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/j/c/g;->a:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/j/c/g;->c:Lcom/sec/chaton/j/c/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    .line 231
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/j/c/g;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lcom/sec/chaton/j/c/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/c/g;->c:Lcom/sec/chaton/j/c/g;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/sec/chaton/j/c/g;

    invoke-direct {v0}, Lcom/sec/chaton/j/c/g;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/c/g;->c:Lcom/sec/chaton/j/c/g;

    .line 29
    :cond_0
    sget-object v0, Lcom/sec/chaton/j/c/g;->c:Lcom/sec/chaton/j/c/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(J)Lcom/sec/chaton/j/c/i;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/h;

    .line 43
    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/h;->b()Lcom/sec/chaton/j/c/i;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/os/Handler;Ljava/io/File;Lcom/sec/chaton/e/w;Landroid/os/Handler;)Lcom/sec/chaton/j/c/i;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/sec/chaton/j/c/i;

    invoke-direct {v0}, Lcom/sec/chaton/j/c/i;-><init>()V

    .line 53
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;Ljava/io/File;Lcom/sec/chaton/e/w;Landroid/os/Handler;)V

    .line 54
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 55
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 57
    return-object v0
.end method

.method public a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 17

    .prologue
    .line 100
    const/4 v15, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v16, p15

    invoke-virtual/range {v0 .. v16}, Lcom/sec/chaton/j/c/g;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)V

    .line 101
    return-void
.end method

.method public a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)V
    .locals 19

    .prologue
    .line 116
    new-instance v3, Lcom/sec/chaton/j/c/i;

    invoke-direct {v3}, Lcom/sec/chaton/j/c/i;-><init>()V

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-wide/from16 v7, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move/from16 v15, p12

    move-object/from16 v16, p13

    move-object/from16 v17, p14

    move-object/from16 v18, p16

    .line 117
    invoke-virtual/range {v3 .. v18}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v6, Lcom/sec/chaton/j/c/h;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, p15

    invoke-direct {v6, v0, v1, v3, v2}, Lcom/sec/chaton/j/c/h;-><init>(Lcom/sec/chaton/j/c/g;Ljava/lang/String;Lcom/sec/chaton/j/c/i;Z)V

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 136
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 137
    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 142
    sget-object v0, Lcom/sec/chaton/j/c/g;->a:Ljava/lang/String;

    const-string v1, "cancel upload file inbox(%s), msgId(%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/h;

    .line 145
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/h;->b()Lcom/sec/chaton/j/c/i;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/chaton/j/c/i;->a(Z)Z

    .line 147
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 148
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-static {p1, p2, p3, v0}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;)V

    .line 149
    return-void
.end method

.method public a(Lcom/sec/chaton/j/c/i;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 62
    if-eqz p1, :cond_0

    .line 63
    invoke-virtual {p1, v0}, Lcom/sec/chaton/j/c/i;->a(Z)Z

    .line 64
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;)V

    .line 67
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 152
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 174
    :goto_0
    return v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v3

    .line 157
    goto :goto_0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 164
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/j/c/h;

    .line 165
    if-eqz v1, :cond_2

    .line 166
    invoke-virtual {v1}, Lcom/sec/chaton/j/c/h;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 167
    invoke-virtual {v1}, Lcom/sec/chaton/j/c/h;->b()Lcom/sec/chaton/j/c/i;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/c/i;->a(Z)Z

    .line 168
    iget-object v1, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move v0, v3

    .line 174
    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 195
    :cond_0
    return v3

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 186
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/h;

    .line 187
    if-eqz v0, :cond_2

    .line 188
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/h;->b()Lcom/sec/chaton/j/c/i;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_2

    .line 190
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/c/i;->a(Z)Z

    goto :goto_0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 211
    :goto_0
    return v0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/h;

    .line 204
    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/h;->c()Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0

    :cond_2
    move v0, v1

    .line 211
    goto :goto_0
.end method

.method public c(J)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 228
    :goto_0
    return v0

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/j/c/g;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/c/h;

    .line 220
    if-eqz v0, :cond_1

    move v0, v1

    .line 222
    goto :goto_0

    .line 225
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 228
    goto :goto_0
.end method
