.class public Lcom/sec/chaton/j/e;
.super Ljava/lang/Object;
.source "GarbageConnectionCollector.java"


# static fields
.field private static a:Lcom/sec/chaton/j/e;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/j/e;->a:Lcom/sec/chaton/j/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/sec/chaton/j/e;->c:I

    .line 30
    iput-boolean v0, p0, Lcom/sec/chaton/j/e;->d:Z

    .line 31
    iput-boolean v0, p0, Lcom/sec/chaton/j/e;->e:Z

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/e;->b:Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/j/e;
    .locals 2

    .prologue
    .line 21
    const-class v1, Lcom/sec/chaton/j/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/e;->a:Lcom/sec/chaton/j/e;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/sec/chaton/j/e;

    invoke-direct {v0}, Lcom/sec/chaton/j/e;-><init>()V

    sput-object v0, Lcom/sec/chaton/j/e;->a:Lcom/sec/chaton/j/e;

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/chaton/j/e;->a:Lcom/sec/chaton/j/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/j/e;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/chaton/j/e;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/j/e;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lcom/sec/chaton/j/e;->f:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/j/e;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Lcom/sec/chaton/j/e;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/j/e;)Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lcom/sec/chaton/j/e;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/j/e;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/chaton/j/e;->f:Ljava/util/Timer;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 7

    .prologue
    .line 36
    iget-object v6, p0, Lcom/sec/chaton/j/e;->b:Ljava/lang/Object;

    monitor-enter v6

    .line 37
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/chaton/j/e;->d:Z

    if-nez v0, :cond_2

    .line 38
    iget-boolean v0, p0, Lcom/sec/chaton/j/e;->e:Z

    if-nez v0, :cond_1

    .line 55
    new-instance v1, Lcom/sec/chaton/j/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/j/f;-><init>(Lcom/sec/chaton/j/e;)V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/j/e;->e:Z

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/j/e;->f:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/j/e;->f:Ljava/util/Timer;

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/j/e;->f:Ljava/util/Timer;

    const-wide/16 v2, 0x1388

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 86
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 87
    const-string v0, "TimerTask was scheduled."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/j/e;->d:Z

    .line 91
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 92
    const-string v0, "StartCollector : true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_2
    iget v0, p0, Lcom/sec/chaton/j/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/j/e;->c:I

    .line 96
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartCollector : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/j/e;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_3
    monitor-exit v6

    .line 100
    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/chaton/j/e;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    iget v0, p0, Lcom/sec/chaton/j/e;->c:I

    if-lez v0, :cond_0

    .line 105
    iget v0, p0, Lcom/sec/chaton/j/e;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/j/e;->c:I

    .line 107
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StopCollector : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/j/e;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_1
    iget v0, p0, Lcom/sec/chaton/j/e;->c:I

    if-nez v0, :cond_2

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/j/e;->d:Z

    .line 113
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "StopCollector : true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_2
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
