.class public Lcom/sec/chaton/j/d/d;
.super Lorg/jboss/netty/handler/codec/replay/ReplayingDecoder;
.source "StreamEnvelopeDecoder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/jboss/netty/handler/codec/replay/ReplayingDecoder",
        "<",
        "Lcom/sec/chaton/j/d/f;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/sec/chaton/j/d/f;

.field private b:Landroid/os/PowerManager$WakeLock;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Landroid/os/PowerManager$WakeLock;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    sget-object v0, Lcom/sec/chaton/j/d/f;->a:Lcom/sec/chaton/j/d/f;

    invoke-direct {p0, v0}, Lorg/jboss/netty/handler/codec/replay/ReplayingDecoder;-><init>(Ljava/lang/Enum;)V

    .line 27
    sget-object v0, Lcom/sec/chaton/j/d/f;->a:Lcom/sec/chaton/j/d/f;

    iput-object v0, p0, Lcom/sec/chaton/j/d/d;->a:Lcom/sec/chaton/j/d/f;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/j/d/d;->c:Ljava/lang/String;

    .line 31
    iput v1, p0, Lcom/sec/chaton/j/d/d;->d:I

    .line 32
    iput v1, p0, Lcom/sec/chaton/j/d/d;->e:I

    .line 33
    iput v1, p0, Lcom/sec/chaton/j/d/d;->f:I

    .line 43
    iput-object p1, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/j/d/d;->getState()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/d/f;

    iput-object v0, p0, Lcom/sec/chaton/j/d/d;->a:Lcom/sec/chaton/j/d/f;

    .line 45
    return-void
.end method

.method public static a(Landroid/os/PowerManager$WakeLock;)Lcom/sec/chaton/j/d/d;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/chaton/j/d/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/j/d/d;-><init>(Landroid/os/PowerManager$WakeLock;)V

    return-object v0
.end method


# virtual methods
.method protected a(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Lorg/jboss/netty/buffer/ChannelBuffer;Lcom/sec/chaton/j/d/f;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 59
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/chaton/j/d/e;->a:[I

    invoke-virtual {p4}, Lcom/sec/chaton/j/d/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/Error;

    const-string v1, "Should not reach here."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 100
    :cond_1
    throw v0

    .line 61
    :pswitch_0
    const/16 v0, 0x24

    :try_start_1
    invoke-interface {p3, v0}, Lorg/jboss/netty/buffer/ChannelBuffer;->readBytes(I)Lorg/jboss/netty/buffer/ChannelBuffer;

    move-result-object v0

    .line 62
    new-instance v1, Ljava/lang/String;

    invoke-interface {v0}, Lorg/jboss/netty/buffer/ChannelBuffer;->array()[B

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/j/d/d;->c:Ljava/lang/String;

    .line 63
    sget-object v0, Lcom/sec/chaton/j/d/f;->b:Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/d/d;->checkpoint(Ljava/lang/Enum;)V

    .line 68
    :pswitch_1
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readByte()B

    move-result v0

    iput v0, p0, Lcom/sec/chaton/j/d/d;->d:I

    .line 69
    sget-object v0, Lcom/sec/chaton/j/d/f;->c:Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/d/d;->checkpoint(Ljava/lang/Enum;)V

    .line 74
    :pswitch_2
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readByte()B

    move-result v0

    iput v0, p0, Lcom/sec/chaton/j/d/d;->e:I

    .line 75
    sget-object v0, Lcom/sec/chaton/j/d/f;->d:Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/d/d;->checkpoint(Ljava/lang/Enum;)V

    .line 82
    :pswitch_3
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/j/d/d;->f:I

    .line 84
    sget-object v0, Lcom/sec/chaton/j/d/f;->e:Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/j/d/d;->checkpoint(Ljava/lang/Enum;)V

    .line 89
    :pswitch_4
    iget v0, p0, Lcom/sec/chaton/j/d/d;->f:I

    invoke-interface {p3, v0}, Lorg/jboss/netty/buffer/ChannelBuffer;->readBytes(I)Lorg/jboss/netty/buffer/ChannelBuffer;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/j/d/d;->a:Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/j/d/d;->checkpoint(Ljava/lang/Enum;)V

    .line 95
    new-instance v1, Lcom/sec/chaton/j/d/a;

    iget-object v2, p0, Lcom/sec/chaton/j/d/d;->c:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/j/d/d;->d:I

    iget v4, p0, Lcom/sec/chaton/j/d/d;->e:I

    invoke-interface {v0}, Lorg/jboss/netty/buffer/ChannelBuffer;->array()[B

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sec/chaton/j/d/a;-><init>(Ljava/lang/String;II[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/j/d/d;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 95
    :cond_2
    return-object v1

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected synthetic decode(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Lorg/jboss/netty/buffer/ChannelBuffer;Ljava/lang/Enum;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p4, Lcom/sec/chaton/j/d/f;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/chaton/j/d/d;->a(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Lorg/jboss/netty/buffer/ChannelBuffer;Lcom/sec/chaton/j/d/f;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
