.class Lcom/sec/chaton/hide/b;
.super Ljava/lang/Object;
.source "HideBuddyAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/hide/a;


# direct methods
.method constructor <init>(Lcom/sec/chaton/hide/a;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/hide/b;->a:Lcom/sec/chaton/hide/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 90
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/hide/b;->a:Lcom/sec/chaton/hide/a;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    const-string v3, "BUDDY_DIALOG_BUDDY_NO"

    iget-object v0, p0, Lcom/sec/chaton/hide/b;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, v0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v3, "BUDDY_DIALOG_BUDDY_NAME"

    iget-object v0, p0, Lcom/sec/chaton/hide/b;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, v0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/hide/b;->a:Lcom/sec/chaton/hide/a;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 94
    return-void
.end method
