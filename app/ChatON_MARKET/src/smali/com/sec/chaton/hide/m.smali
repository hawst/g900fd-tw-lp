.class public Lcom/sec/chaton/hide/m;
.super Ljava/lang/Object;
.source "HideImpl.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field a:Lcom/sec/chaton/e/a/u;

.field final synthetic b:Lcom/sec/chaton/hide/g;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/hide/g;)V
    .locals 2

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/hide/m;->a:Lcom/sec/chaton/e/a/u;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->a:Lcom/sec/chaton/e/a/u;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-static {p1}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/m;->a:Lcom/sec/chaton/e/a/u;

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->a:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    invoke-static {}, Lcom/sec/chaton/e/i;->l()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 226
    const-string v0, "onDeleteComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 214
    const-string v0, "onInsertComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const v5, 0x7f0b031c

    .line 128
    const-string v0, "onQueryComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->b(Lcom/sec/chaton/hide/g;)V

    .line 209
    :goto_0
    return-void

    .line 134
    :cond_1
    if-ne p1, v6, :cond_b

    .line 135
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 136
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 137
    if-gtz v0, :cond_6

    .line 138
    const-string v0, "Has no member of hide buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->d(Lcom/sec/chaton/hide/g;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->f(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v1}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/a;->a(Ljava/util/ArrayList;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->f(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/hide/a;->notifyDataSetChanged()V

    .line 147
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 198
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->h(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;)I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v1}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v1

    .line 201
    sub-int v0, v1, v0

    if-gtz v0, :cond_c

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->h(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 206
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->h(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->b(Lcom/sec/chaton/hide/g;)V

    goto/16 :goto_0

    .line 152
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    iget-object v0, v0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 156
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " member of hide buddy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 158
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 160
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/hide/f;

    const-string v2, "buddy_no"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "buddy_name"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_orginal_number"

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/chaton/hide/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_7

    .line 165
    :cond_8
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/hide/n;

    invoke-direct {v1, p0}, Lcom/sec/chaton/hide/n;-><init>(Lcom/sec/chaton/hide/m;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 179
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->f(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v1}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/a;->a(Ljava/util/ArrayList;)V

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->f(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/hide/a;->notifyDataSetChanged()V

    .line 182
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 187
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    iget-object v0, v0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v2}, Lcom/sec/chaton/hide/g;->e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 191
    :cond_b
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->g(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/m;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    iget-object v0, v0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    iget-object v0, v0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->g(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/hide/m;->a()V

    goto/16 :goto_1

    .line 204
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/hide/m;->b:Lcom/sec/chaton/hide/g;

    invoke-static {v0}, Lcom/sec/chaton/hide/g;->h(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_2
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 220
    const-string v0, "onUpdateComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    return-void
.end method
