.class public Lcom/sec/chaton/hide/HideFragment;
.super Landroid/support/v4/app/Fragment;
.source "HideFragment.java"


# instance fields
.field private a:Lcom/sec/chaton/hide/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 31
    new-instance v0, Lcom/sec/chaton/hide/g;

    invoke-direct {v0}, Lcom/sec/chaton/hide/g;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/hide/g;->a(IILandroid/content/Intent;)V

    .line 95
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 36
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/app/Activity;)V

    .line 37
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/hide/g;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 55
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/hide/g;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->d()V

    .line 88
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->b()V

    .line 43
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/hide/HideFragment;->a:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->c()V

    .line 81
    return-void
.end method
