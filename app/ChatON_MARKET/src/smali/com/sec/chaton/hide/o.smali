.class Lcom/sec/chaton/hide/o;
.super Landroid/widget/ArrayAdapter;
.source "HideListBuddyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/hide/f;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation
.end field

.field public c:[Z

.field private d:Landroid/content/Context;

.field private e:Landroid/view/LayoutInflater;

.field private f:Landroid/app/ProgressDialog;

.field private g:Landroid/view/ViewGroup;

.field private h:Lcom/sec/chaton/hide/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 71
    iput-object p1, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    .line 72
    iput-object p3, p0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/chaton/hide/o;->c:[Z

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/hide/o;->e:Landroid/view/LayoutInflater;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->e:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/hide/o;->g:Landroid/view/ViewGroup;

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/hide/o;->f:Landroid/app/ProgressDialog;

    .line 81
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 85
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 86
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move v1, v0

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->a:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/d;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 91
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->h:Lcom/sec/chaton/hide/p;

    invoke-interface {v0}, Lcom/sec/chaton/hide/p;->a()V

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0320

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 94
    invoke-virtual {p0}, Lcom/sec/chaton/hide/o;->notifyDataSetChanged()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 102
    :goto_1
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 97
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 98
    :catch_1
    move-exception v0

    .line 100
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/hide/p;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/hide/o;->h:Lcom/sec/chaton/hide/p;

    .line 66
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->c:[Z

    array-length v1, v0

    .line 127
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 128
    iget-object v2, p0, Lcom/sec/chaton/hide/o;->c:[Z

    aput-boolean p1, v2, v0

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_0
    if-eqz p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 136
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/hide/o;->notifyDataSetChanged()V

    .line 137
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 147
    if-nez p2, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/hide/o;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f030125

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 149
    new-instance v2, Lcom/sec/chaton/hide/q;

    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-direct {v2, v0}, Lcom/sec/chaton/hide/q;-><init>(Lcom/sec/chaton/widget/CheckableRelativeLayout;)V

    .line 150
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object p2, v1

    move-object v1, v2

    .line 155
    :goto_0
    iget-object v0, v1, Lcom/sec/chaton/hide/q;->d:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/chaton/hide/o;->c:[Z

    aget-boolean v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 157
    invoke-virtual {p0, p1}, Lcom/sec/chaton/hide/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    .line 158
    iget-object v2, v1, Lcom/sec/chaton/hide/q;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/hide/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v2, p0, Lcom/sec/chaton/hide/o;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/hide/q;->c:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 160
    return-object p2

    .line 152
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/q;

    move-object v1, v0

    goto :goto_0
.end method
