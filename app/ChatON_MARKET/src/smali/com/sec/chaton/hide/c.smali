.class Lcom/sec/chaton/hide/c;
.super Ljava/lang/Object;
.source "HideBuddyAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/hide/a;


# direct methods
.method constructor <init>(Lcom/sec/chaton/hide/a;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 102
    iget-object v2, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, v0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/hide/a;->a(Lcom/sec/chaton/hide/a;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    iget-object v2, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    iget-object v0, v0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/hide/a;->b(Lcom/sec/chaton/hide/a;Ljava/lang/String;)Ljava/lang/String;

    .line 106
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-static {v2}, Lcom/sec/chaton/hide/a;->a(Lcom/sec/chaton/hide/a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/d;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-static {v0}, Lcom/sec/chaton/hide/a;->b(Lcom/sec/chaton/hide/a;)Lcom/sec/chaton/hide/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/hide/d;->a()V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-static {v0}, Lcom/sec/chaton/hide/a;->c(Lcom/sec/chaton/hide/a;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-static {v1}, Lcom/sec/chaton/hide/a;->c(Lcom/sec/chaton/hide/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0320

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/hide/c;->a:Lcom/sec/chaton/hide/a;

    invoke-static {v1}, Lcom/sec/chaton/hide/a;->d(Lcom/sec/chaton/hide/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is unhide"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 115
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 118
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_0
.end method
