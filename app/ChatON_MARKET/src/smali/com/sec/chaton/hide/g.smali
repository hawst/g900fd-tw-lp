.class public Lcom/sec/chaton/hide/g;
.super Ljava/lang/Object;
.source "HideImpl.java"


# instance fields
.field a:Z

.field b:Landroid/app/ProgressDialog;

.field c:Landroid/app/Dialog;

.field d:Landroid/view/View;

.field e:[Ljava/lang/String;

.field public f:I

.field g:Landroid/widget/TextView;

.field h:Landroid/database/ContentObserver;

.field private i:Landroid/widget/ListView;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/sec/chaton/hide/a;

.field private l:Landroid/view/ViewStub;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/app/Activity;

.field private r:Landroid/view/MenuItem;

.field private s:Landroid/view/MenuItem;

.field private t:Lcom/sec/chaton/hide/m;

.field private u:Lcom/sec/chaton/hide/d;

.field private v:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/hide/g;->a:Z

    .line 62
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->c:Landroid/app/Dialog;

    .line 76
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    .line 77
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    .line 78
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->e:[Ljava/lang/String;

    .line 82
    const/16 v0, 0x3f

    iput v0, p0, Lcom/sec/chaton/hide/g;->f:I

    .line 84
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    .line 93
    iput-object v1, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 247
    new-instance v0, Lcom/sec/chaton/hide/h;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/hide/h;-><init>(Lcom/sec/chaton/hide/g;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->h:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/hide/g;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/hide/g;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/hide/g;->f()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/hide/g;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/hide/g;->g()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/hide/g;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/a;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->k:Lcom/sec/chaton/hide/a;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 234
    :catch_0
    move-exception v0

    .line 235
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/hide/g;)Lcom/sec/chaton/hide/m;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->m:Landroid/view/View;

    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->l:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->m:Landroid/view/View;

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->m:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->n:Landroid/widget/ImageView;

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->n:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->m:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->o:Landroid/widget/TextView;

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0323

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->m:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->p:Landroid/widget/TextView;

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->l:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 480
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/hide/g;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 305
    const-string v0, "onCreateView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const v0, 0x7f0300ad

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 307
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    .line 311
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const v0, 0x7f070416

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 313
    const v0, 0x7f07050b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 314
    const v3, 0x7f0b01e5

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 315
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 316
    new-instance v3, Lcom/sec/chaton/hide/i;

    invoke-direct {v3, p0}, Lcom/sec/chaton/hide/i;-><init>(Lcom/sec/chaton/hide/g;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    const v0, 0x7f07050d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    const v3, 0x7f0b031c

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 325
    const v0, 0x7f07050e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 326
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 330
    :cond_0
    const v0, 0x7f0700a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->l:Landroid/view/ViewStub;

    .line 333
    new-instance v0, Lcom/sec/chaton/hide/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/hide/j;-><init>(Lcom/sec/chaton/hide/g;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->u:Lcom/sec/chaton/hide/d;

    .line 341
    new-instance v0, Lcom/sec/chaton/hide/a;

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    const v3, 0x7f030124

    iget-object v4, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/hide/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->k:Lcom/sec/chaton/hide/a;

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->k:Lcom/sec/chaton/hide/a;

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->u:Lcom/sec/chaton/hide/d;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/hide/a;->a(Lcom/sec/chaton/hide/d;)V

    .line 343
    const v0, 0x7f070298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->i:Landroid/widget/ListView;

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->i:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/hide/k;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/k;-><init>(Lcom/sec/chaton/hide/g;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->i:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/hide/l;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/l;-><init>(Lcom/sec/chaton/hide/g;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->i:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->k:Lcom/sec/chaton/hide/a;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/m;->a()V

    .line 382
    :cond_1
    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/m;->a()V

    .line 245
    :cond_0
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 442
    sparse-switch p1, :sswitch_data_0

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 444
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/m;->a()V

    goto :goto_0

    .line 450
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 451
    const-string v0, "Remove multiple hide buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v0, "blindlist"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->e:[Ljava/lang/String;

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->k:Lcom/sec/chaton/hide/a;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->e:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/a;->a([Ljava/lang/String;)V

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/m;->a()V

    goto :goto_0

    .line 442
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 257
    const-string v0, "onAttach"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iput-object p1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    .line 259
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 267
    const-string v0, "onCreate"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v0, Lcom/sec/chaton/hide/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/hide/m;-><init>(Lcom/sec/chaton/hide/g;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->t:Lcom/sec/chaton/hide/m;

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/hide/g;->h:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 270
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 386
    const v0, 0x7f0f0030

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 387
    const v0, 0x7f0705b0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    .line 388
    const v0, 0x7f0705b1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    .line 390
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;)I

    move-result v0

    .line 392
    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v1

    .line 393
    sub-int v0, v1, v0

    if-gtz v0, :cond_4

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 399
    :goto_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 409
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_2

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 420
    :cond_3
    return-void

    .line 396
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->r:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/chaton/hide/g;->v:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 97
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 423
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 438
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 425
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/hide/HideListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    const-string v1, "hide_buddy_list"

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 427
    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    const/16 v2, 0xc8

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 430
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 431
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 432
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 433
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 434
    iget-object v1, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 423
    :pswitch_data_0
    .packed-switch 0x7f0705b0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 262
    const-string v0, "onDetach"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    .line 264
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const v3, 0x7f0b031c

    .line 273
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 276
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 284
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 296
    :cond_1
    :goto_1
    return-void

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 293
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    .line 299
    const-string v0, "onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/g;->h:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 301
    invoke-direct {p0}, Lcom/sec/chaton/hide/g;->f()V

    .line 302
    return-void
.end method

.method public e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/chaton/hide/g;->j:Ljava/util/ArrayList;

    return-object v0
.end method
