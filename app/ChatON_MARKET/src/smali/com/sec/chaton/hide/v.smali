.class Lcom/sec/chaton/hide/v;
.super Ljava/lang/Object;
.source "HideListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/hide/HideListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/hide/HideListFragment;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    .line 308
    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/o;->a(Z)V

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move v0, v1

    .line 312
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v2}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v2}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/hide/o;->a(Z)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 319
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 319
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/hide/o;->notifyDataSetChanged()V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/hide/v;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 325
    return-void
.end method
