.class public Lcom/sec/chaton/hide/HideListFragment;
.super Landroid/support/v4/app/Fragment;
.source "HideListFragment.java"


# static fields
.field static a:Z


# instance fields
.field b:Landroid/app/ProgressDialog;

.field c:Landroid/app/Dialog;

.field d:Landroid/view/View;

.field private e:Landroid/widget/ListView;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/sec/chaton/hide/o;

.field private h:Landroid/widget/CheckedTextView;

.field private i:Lcom/sec/chaton/hide/p;

.field private j:Landroid/app/Activity;

.field private k:Landroid/view/MenuItem;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/hide/HideListFragment;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->c:Landroid/app/Dialog;

    .line 65
    const-string v0, "hide_list"

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->l:Ljava/lang/String;

    .line 66
    const-string v0, "m_data"

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->m:Ljava/lang/String;

    .line 67
    const-string v0, "hide_data"

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->n:Ljava/lang/String;

    .line 68
    const-string v0, "is_check"

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->o:Ljava/lang/String;

    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 332
    move v1, v0

    .line 333
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 334
    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 335
    add-int/lit8 v1, v1, 0x1

    .line 333
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 338
    :cond_1
    return v1
.end method

.method static synthetic a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->h:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/hide/HideListFragment;Lcom/sec/chaton/hide/o;)Lcom/sec/chaton/hide/o;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/hide/HideListFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 358
    :catch_0
    move-exception v0

    .line 359
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/hide/HideListFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/hide/HideListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/p;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->i:Lcom/sec/chaton/hide/p;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->k:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/hide/HideListFragment;)I
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/hide/HideListFragment;->a()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 74
    iput-object p1, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    .line 75
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 103
    const v0, 0x7f0f002d

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 104
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 105
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 154
    const v0, 0x7f0300ae

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 156
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "hide_buddy_list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->b:Landroid/app/ProgressDialog;

    .line 163
    new-instance v0, Lcom/sec/chaton/hide/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/hide/r;-><init>(Lcom/sec/chaton/hide/HideListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->i:Lcom/sec/chaton/hide/p;

    .line 216
    if-eqz p3, :cond_0

    .line 217
    const-string v0, "hide_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    .line 219
    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 220
    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    const-string v3, "m_data"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 224
    :cond_0
    new-instance v0, Lcom/sec/chaton/hide/o;

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    const v3, 0x7f030125

    iget-object v4, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/hide/o;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->i:Lcom/sec/chaton/hide/p;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/hide/o;->a(Lcom/sec/chaton/hide/p;)V

    .line 227
    if-eqz p3, :cond_1

    .line 228
    const-string v0, "hide_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 229
    if-eqz v2, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v0, v0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v0, v0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v3, v0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    const-string v0, "hide_data"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v3, v0, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    const-string v0, "m_data"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    const-string v3, "is_check"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/hide/o;->c:[Z

    .line 238
    :cond_1
    const v0, 0x7f070298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/hide/s;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/s;-><init>(Lcom/sec/chaton/hide/HideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/hide/t;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/t;-><init>(Lcom/sec/chaton/hide/HideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->e:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/hide/u;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/u;-><init>(Lcom/sec/chaton/hide/HideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 303
    const v0, 0x7f070324

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->h:Landroid/widget/CheckedTextView;

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->h:Landroid/widget/CheckedTextView;

    new-instance v2, Lcom/sec/chaton/hide/v;

    invoke-direct {v2, p0}, Lcom/sec/chaton/hide/v;-><init>(Lcom/sec/chaton/hide/HideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 350
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 351
    invoke-direct {p0}, Lcom/sec/chaton/hide/HideListFragment;->b()V

    .line 352
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    .line 81
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 133
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070575

    if-ne v0, v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/o;->a()V

    .line 137
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070574

    if-ne v0, v1, :cond_1

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 141
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 87
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 112
    const v0, 0x7f070575

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->k:Landroid/view/MenuItem;

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->k:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->i:Lcom/sec/chaton/hide/p;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/hide/HideListFragment;->i:Lcom/sec/chaton/hide/p;

    invoke-interface {v0}, Lcom/sec/chaton/hide/p;->onClick()V

    .line 128
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 344
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 345
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 366
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 367
    const-string v1, "m_data"

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 368
    const-string v1, "hide_data"

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v2, v2, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 369
    const-string v1, "is_check"

    iget-object v2, p0, Lcom/sec/chaton/hide/HideListFragment;->g:Lcom/sec/chaton/hide/o;

    iget-object v2, v2, Lcom/sec/chaton/hide/o;->c:[Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 370
    const-string v1, "hide_list"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 371
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 93
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 98
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 99
    return-void
.end method
