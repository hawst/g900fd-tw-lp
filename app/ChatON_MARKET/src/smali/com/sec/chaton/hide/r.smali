.class Lcom/sec/chaton/hide/r;
.super Ljava/lang/Object;
.source "HideListFragment.java"

# interfaces
.implements Lcom/sec/chaton/hide/p;


# instance fields
.field final synthetic a:Lcom/sec/chaton/hide/HideListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/hide/HideListFragment;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    iget-object v1, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    new-instance v1, Lcom/sec/chaton/hide/o;

    iget-object v2, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v2}, Lcom/sec/chaton/hide/HideListFragment;->c(Lcom/sec/chaton/hide/HideListFragment;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030125

    iget-object v4, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v4}, Lcom/sec/chaton/hide/HideListFragment;->d(Lcom/sec/chaton/hide/HideListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/chaton/hide/o;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;Lcom/sec/chaton/hide/o;)Lcom/sec/chaton/hide/o;

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->e(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/o;->a(Lcom/sec/chaton/hide/p;)V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->d(Lcom/sec/chaton/hide/HideListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->d(Lcom/sec/chaton/hide/HideListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/HideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/chaton/hide/o;->a(Z)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 196
    return-void
.end method

.method public onClick()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/r;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->g(Lcom/sec/chaton/hide/HideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
