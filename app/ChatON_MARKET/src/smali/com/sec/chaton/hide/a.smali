.class Lcom/sec/chaton/hide/a;
.super Landroid/widget/ArrayAdapter;
.source "HideBuddyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/hide/f;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Landroid/view/LayoutInflater;

.field private f:Landroid/app/ProgressDialog;

.field private g:Landroid/view/ViewGroup;

.field private h:Lcom/sec/chaton/hide/d;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 84
    new-instance v0, Lcom/sec/chaton/hide/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/hide/b;-><init>(Lcom/sec/chaton/hide/a;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/a;->i:Landroid/view/View$OnClickListener;

    .line 97
    new-instance v0, Lcom/sec/chaton/hide/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/hide/c;-><init>(Lcom/sec/chaton/hide/a;)V

    iput-object v0, p0, Lcom/sec/chaton/hide/a;->j:Landroid/view/View$OnClickListener;

    .line 72
    iput-object p1, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    .line 73
    iput-object p3, p0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/hide/a;->e:Landroid/view/LayoutInflater;

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->e:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/hide/a;->g:Landroid/view/ViewGroup;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/hide/a;->f:Landroid/app/ProgressDialog;

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/hide/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/hide/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/chaton/hide/a;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/hide/a;)Lcom/sec/chaton/hide/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->h:Lcom/sec/chaton/hide/d;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/hide/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/chaton/hide/a;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/hide/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/hide/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/hide/d;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/chaton/hide/a;->h:Lcom/sec/chaton/hide/d;

    .line 63
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/hide/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/chaton/hide/a;->a:Ljava/util/ArrayList;

    .line 82
    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 172
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 173
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 174
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 175
    aget-object v3, p1, v0

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/d;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 185
    :goto_1
    return-void

    .line 178
    :catch_0
    move-exception v0

    .line 180
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 181
    :catch_1
    move-exception v0

    .line 183
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 129
    if-nez p2, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f030124

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 131
    new-instance v1, Lcom/sec/chaton/hide/e;

    invoke-direct {v1}, Lcom/sec/chaton/hide/e;-><init>()V

    .line 132
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/hide/e;->a:Landroid/widget/TextView;

    .line 133
    const v0, 0x7f07014b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/hide/e;->b:Landroid/widget/ImageView;

    .line 134
    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    .line 135
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 140
    :goto_0
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->a:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 141
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->a:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 142
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->b:Landroid/widget/ImageView;

    const v2, 0x7f02028d

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 143
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/hide/a;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 146
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 148
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    const/high16 v3, 0x42c80000    # 100.0f

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 149
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 150
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setMaxLines(I)V

    .line 151
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 152
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    const v2, 0x7f0b031e

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 153
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 155
    iget-object v0, v1, Lcom/sec/chaton/hide/e;->c:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/hide/a;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    invoke-virtual {p0, p1}, Lcom/sec/chaton/hide/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/f;

    .line 158
    iget-object v2, v1, Lcom/sec/chaton/hide/e;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/hide/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v2, p0, Lcom/sec/chaton/hide/a;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/hide/e;->b:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/hide/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 160
    return-object p2

    .line 137
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/hide/e;

    move-object v1, v0

    goto/16 :goto_0
.end method
