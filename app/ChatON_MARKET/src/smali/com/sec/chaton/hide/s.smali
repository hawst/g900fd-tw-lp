.class Lcom/sec/chaton/hide/s;
.super Ljava/lang/Object;
.source "HideListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/hide/HideListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/hide/HideListFragment;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 252
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 258
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->e(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/hide/p;->onClick()V

    .line 259
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->h(Lcom/sec/chaton/hide/HideListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->f(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->a(Lcom/sec/chaton/hide/HideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/hide/o;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/hide/s;->a:Lcom/sec/chaton/hide/HideListFragment;

    invoke-static {v1}, Lcom/sec/chaton/hide/HideListFragment;->b(Lcom/sec/chaton/hide/HideListFragment;)Lcom/sec/chaton/hide/o;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/hide/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
