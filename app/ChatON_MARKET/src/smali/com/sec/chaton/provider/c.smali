.class final enum Lcom/sec/chaton/provider/c;
.super Ljava/lang/Enum;
.source "ChatONProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/provider/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/provider/c;

.field public static final enum b:Lcom/sec/chaton/provider/c;

.field public static final enum c:Lcom/sec/chaton/provider/c;

.field public static final enum d:Lcom/sec/chaton/provider/c;

.field public static final enum e:Lcom/sec/chaton/provider/c;

.field private static final synthetic g:[Lcom/sec/chaton/provider/c;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 274
    new-instance v0, Lcom/sec/chaton/provider/c;

    const-string v1, "RESULT_FAIL_INSERT"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/provider/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/provider/c;->a:Lcom/sec/chaton/provider/c;

    .line 275
    new-instance v0, Lcom/sec/chaton/provider/c;

    const-string v1, "RESULT_FAIL_UPDATE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/provider/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/provider/c;->b:Lcom/sec/chaton/provider/c;

    .line 276
    new-instance v0, Lcom/sec/chaton/provider/c;

    const-string v1, "RESULT_FAIL"

    invoke-direct {v0, v1, v5, v3}, Lcom/sec/chaton/provider/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/provider/c;->c:Lcom/sec/chaton/provider/c;

    .line 277
    new-instance v0, Lcom/sec/chaton/provider/c;

    const-string v1, "RESULT_SUCCESS_INSERT"

    invoke-direct {v0, v1, v6, v4}, Lcom/sec/chaton/provider/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/provider/c;->d:Lcom/sec/chaton/provider/c;

    .line 278
    new-instance v0, Lcom/sec/chaton/provider/c;

    const-string v1, "RESULT_SUCCESS_UPDATE"

    invoke-direct {v0, v1, v7, v5}, Lcom/sec/chaton/provider/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/provider/c;->e:Lcom/sec/chaton/provider/c;

    .line 273
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/provider/c;

    sget-object v1, Lcom/sec/chaton/provider/c;->a:Lcom/sec/chaton/provider/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/provider/c;->b:Lcom/sec/chaton/provider/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/provider/c;->c:Lcom/sec/chaton/provider/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/provider/c;->d:Lcom/sec/chaton/provider/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/provider/c;->e:Lcom/sec/chaton/provider/c;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/provider/c;->g:[Lcom/sec/chaton/provider/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 283
    iput p3, p0, Lcom/sec/chaton/provider/c;->f:I

    .line 284
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/provider/c;
    .locals 1

    .prologue
    .line 273
    const-class v0, Lcom/sec/chaton/provider/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/provider/c;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/provider/c;
    .locals 1

    .prologue
    .line 273
    sget-object v0, Lcom/sec/chaton/provider/c;->g:[Lcom/sec/chaton/provider/c;

    invoke-virtual {v0}, [Lcom/sec/chaton/provider/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/provider/c;

    return-object v0
.end method
