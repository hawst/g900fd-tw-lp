.class public Lcom/sec/chaton/provider/a/a;
.super Ljava/lang/Object;
.source "ChatListSyncOperation.java"


# static fields
.field private static t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

.field private static u:Lcom/sec/chaton/provider/a/a;

.field private static v:Z

.field private static w:Z

.field private static x:J

.field private static y:J


# instance fields
.field a:Z

.field private b:Lcom/sec/chaton/e/r;

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/StringBuilder;

.field private n:Ljava/lang/StringBuilder;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Landroid/database/sqlite/SQLiteDatabase;

.field private s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->w:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->o:Z

    .line 54
    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->p:Z

    .line 55
    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->q:Z

    .line 56
    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->a:Z

    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/provider/a/a;
    .locals 2

    .prologue
    .line 75
    const-class v1, Lcom/sec/chaton/provider/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/provider/a/a;->u:Lcom/sec/chaton/provider/a/a;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/sec/chaton/provider/a/a;

    invoke-direct {v0}, Lcom/sec/chaton/provider/a/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/provider/a/a;->u:Lcom/sec/chaton/provider/a/a;

    .line 79
    :cond_0
    sget-object v0, Lcom/sec/chaton/provider/a/a;->u:Lcom/sec/chaton/provider/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 818
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v0

    .line 819
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->a:Z

    .line 820
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 821
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 822
    const-string v2, "inbox_no"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const-string v2, "buddy_no"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    const-string v2, "chat_type"

    invoke-virtual {p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 826
    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "inbox_buddy_relation"

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 828
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 829
    const-string v2, "participants_inbox_no"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const-string v2, "participants_buddy_no"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const-string v2, "participants_buddy_name"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "participant"

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 836
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 837
    const-string v2, "inbox_no"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const-string v2, "inbox_chat_type"

    invoke-virtual {p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 839
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne p3, v2, :cond_2

    .line 840
    const-string v2, "inbox_last_chat_type"

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 844
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 845
    const-string v2, "inbox_session_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :cond_1
    const-string v2, "inbox_last_time"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 849
    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "inbox"

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 850
    return-object v0

    .line 842
    :cond_2
    const-string v2, "inbox_last_chat_type"

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 9

    .prologue
    .line 566
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 568
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 569
    const-string v1, ""

    .line 571
    array-length v4, v3

    const/4 v0, 0x0

    move v8, v0

    move-object v0, v1

    move v1, v8

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 573
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 574
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 575
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 576
    const-string v7, "participants_buddy_no"

    invoke-virtual {v6, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v7, "participants_old_buddy_no"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const-string v5, "participant_mapping"

    invoke-virtual {p0, v5, v6}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 579
    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 583
    :cond_1
    const-string v0, "\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " makeOldUserList :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_2
    return-object v2
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 1105
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1106
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1107
    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1108
    const-string v7, ""

    .line 1109
    const-string v6, ""

    .line 1111
    :goto_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1113
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1115
    const-string v1, "message_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    const-string v1, "message_session_id"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    const-string v1, "message_inbox_no"

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string v1, "message"

    const-string v2, "message_inbox_no=?"

    new-array v3, v12, [Ljava/lang/String;

    aput-object v8, v3, v13

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1120
    const-string v0, "N"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1121
    const-string v1, "participant"

    const-string v2, "participants_inbox_no=?"

    new-array v3, v12, [Ljava/lang/String;

    aput-object v8, v3, v13

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1123
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 1124
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1125
    const-string v1, "participants_buddy_no"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1126
    const-string v1, "participants_buddy_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    move-object v3, v7

    .line 1129
    :goto_1
    if-eqz v0, :cond_0

    .line 1130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1133
    :cond_0
    const-string v0, "inbox_buddy_relation"

    const-string v1, "inbox_no=?"

    new-array v5, v12, [Ljava/lang/String;

    aput-object v8, v5, v13

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1135
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lookForInvalidInbox() inboxNo["

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "] is invalided."

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    const-string v1, "inbox_valid"

    const-string v5, "N"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    const-string v1, "inbox_session_id"

    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v5, v5, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    const-string v1, "inbox"

    const-string v5, "inbox_no=?"

    new-array v6, v12, [Ljava/lang/String;

    aput-object v8, v6, v13

    invoke-virtual {p0, v1, v5, v6, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1142
    invoke-virtual {p0, v8}, Lcom/sec/chaton/provider/a/a;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 1143
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-nez v5, :cond_1

    .line 1144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1148
    :goto_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1149
    const-string v6, "_id"

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1150
    const-string v6, "message_inbox_no"

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    const-string v6, "message_type"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1152
    const-string v6, "message_content_type"

    sget-object v7, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1153
    const-string v6, "message_content"

    const-string v7, "%d,%s,%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    sget-object v11, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    invoke-virtual {v11}, Lcom/sec/chaton/e/aj;->a()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v13

    aput-object v3, v10, v12

    const/4 v11, 0x2

    aput-object v2, v10, v11

    invoke-static {v7, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    const-string v6, "message_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1155
    const-string v6, "message_time_text"

    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    const-string v0, "message"

    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    move-object v0, v2

    move-object v1, v3

    .line 1159
    :goto_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1160
    return-void

    .line 1146
    :cond_1
    const-wide/16 v5, 0x1

    add-long/2addr v0, v5

    goto :goto_2

    :cond_2
    move-object v2, v6

    move-object v3, v7

    goto/16 :goto_1

    :cond_3
    move-object v0, v6

    move-object v1, v7

    goto :goto_3

    :cond_4
    move-object v6, v0

    move-object v7, v1

    goto/16 :goto_0
.end method

.method public static a(Lcom/sec/chaton/io/entry/ChatListInfoEntry;)V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/chaton/provider/a/a;->t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/provider/a/a;->t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

    .line 87
    :cond_0
    sput-object p0, Lcom/sec/chaton/provider/a/a;->t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

    .line 88
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->v:Z

    .line 89
    return-void
.end method

.method private a(Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 483
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 484
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->olduserid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->olduserid:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->olduserid:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 494
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 495
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_0

    .line 496
    iput-boolean v3, p0, Lcom/sec/chaton/provider/a/a;->o:Z

    .line 497
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    .line 523
    :cond_0
    :goto_1
    return-void

    .line 488
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 489
    const-string v1, "empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    iput-boolean v3, p0, Lcom/sec/chaton/provider/a/a;->q:Z

    .line 502
    iget v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 505
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    .line 506
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    goto :goto_1

    .line 510
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 511
    iput-boolean v3, p0, Lcom/sec/chaton/provider/a/a;->p:Z

    .line 512
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_0

    .line 513
    iput-boolean v3, p0, Lcom/sec/chaton/provider/a/a;->o:Z

    .line 514
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    goto :goto_1

    .line 517
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 518
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    .line 519
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    goto :goto_1
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 92
    sget-boolean v0, Lcom/sec/chaton/provider/a/a;->w:Z

    return v0
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->w:Z

    .line 98
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 397
    const-string v1, "participant"

    const-string v2, "participants_inbox_no=?"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    aput-object p1, v3, v6

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 398
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 400
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 402
    :cond_0
    const-string v1, "participants_buddy_no"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 403
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 406
    :cond_1
    if-eqz v0, :cond_2

    .line 407
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    move v1, v6

    .line 411
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "empty"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 415
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v5, "chaton_id"

    invoke-virtual {v3, v5, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 418
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 422
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert additional Participants_name : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "Participants_id : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_6
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 428
    const-string v5, "participants_buddy_no"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v0, "participants_inbox_no"

    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v5, "participants_buddy_name"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const-string v0, "participant"

    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 434
    :cond_7
    return-void
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/chaton/provider/a/a;->x:J

    .line 102
    return-void
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->v:Z

    .line 106
    return-void
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/sec/chaton/provider/a/a;->v:Z

    return v0
.end method

.method private n()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 357
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    .line 358
    iput-boolean v6, p0, Lcom/sec/chaton/provider/a/a;->a:Z

    move v1, v2

    .line 360
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "empty"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert Participants_name : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "Participants_id : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :cond_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 372
    const-string v4, "participants_buddy_no"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v0, "participants_inbox_no"

    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v4, "participants_buddy_name"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v0, "participant"

    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 378
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 379
    const-string v0, "inbox_no"

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v0, "inbox_chat_type"

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 381
    const-string v0, "inbox_last_chat_type"

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    const-string v0, "inbox_session_id"

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v0, "inbox_last_time"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 385
    const-string v0, "Y"

    .line 386
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Y"

    .line 387
    :goto_2
    const-string v2, "inbox_enable_noti"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v0, "inbox"

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 393
    :cond_4
    :goto_3
    return-void

    .line 386
    :cond_5
    const-string v0, "N"

    goto :goto_2

    .line 390
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_4

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/provider/a/a;->c(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->p()V

    .line 440
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 441
    const-string v0, " Delete  ChatRoom :: In this chat room, No Participants - my insession false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_0
    return-void
.end method

.method private p()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450
    const-string v0, "inbox"

    const-string v1, "inbox_no=?"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 451
    const-string v0, "participant"

    const-string v1, "participants_inbox_no=?"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 452
    const-string v0, "message"

    const-string v1, "message_inbox_no=?"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 453
    const-string v0, "inbox_buddy_relation"

    const-string v1, "inbox_no=?"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 455
    :cond_0
    return-void
.end method

.method private q()V
    .locals 8

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->oldsessionid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->oldsessionid:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 460
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 462
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 463
    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 469
    const-string v6, "inbox_session_id"

    iget-object v7, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v7, v7, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v6, "inbox_old_session_id"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v4, "inbox_session_id_mapping"

    invoke-virtual {p0, v4, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 462
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 475
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    .line 477
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 478
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " makeOldSessionID :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_2
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->oldsessionid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 530
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->s()V

    .line 542
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->s()V

    goto :goto_0
.end method

.method private s()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 545
    move v1, v2

    :goto_0
    iget v0, p0, Lcom/sec/chaton/provider/a/a;->c:I

    if-ge v1, v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    const-string v3, "true"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->olduserid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->olduserid:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 552
    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    .line 553
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 555
    const-string v8, "participants_buddy_no"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const-string v0, "participants_old_buddy_no"

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const-string v0, "participant_mapping"

    invoke-virtual {p0, v0, v7}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 552
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 545
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 563
    :cond_1
    return-void
.end method

.method private t()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x1

    .line 663
    .line 665
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 669
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->h()Ljava/util/List;

    move-result-object v0

    .line 675
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 676
    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 679
    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    const-string v6, "\'"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\'"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    invoke-virtual {p0, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 686
    const-string v3, "inbox"

    const-string v6, "inbox_no=?"

    new-array v7, v2, [Ljava/lang/String;

    aput-object v0, v7, v10

    invoke-virtual {p0, v3, v6, v7}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 689
    :cond_0
    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    move v1, v2

    move-object v3, v0

    .line 690
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 691
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 692
    const-string v6, "inbox_last_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-string v8, "inbox_last_time"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_1

    .line 693
    const-string v6, "inbox_unread_count"

    const-string v7, "inbox_unread_count"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v8, "inbox_unread_count"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 694
    const-string v6, "inbox_trunk_unread_count"

    const-string v7, "inbox_trunk_unread_count"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v8, "inbox_trunk_unread_count"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v3, v0

    .line 690
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 697
    :cond_1
    const-string v6, "inbox_unread_count"

    const-string v7, "inbox_unread_count"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v8, "inbox_unread_count"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 698
    const-string v6, "inbox_trunk_unread_count"

    const-string v7, "inbox_trunk_unread_count"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v7, "inbox_trunk_unread_count"

    invoke-virtual {v3, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 701
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 702
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 703
    const-string v0, "inbox_old_no"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    :cond_3
    const-string v0, "inbox"

    const-string v1, "inbox_no=?"

    new-array v5, v2, [Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v6, v5, v10

    invoke-virtual {p0, v0, v1, v5, v3}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 707
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v2, :cond_4

    .line 708
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MergeInbox List :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :cond_4
    return-void
.end method

.method private u()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 713
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 718
    const-string v1, "message_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 719
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 720
    const-string v3, "message_session_id"

    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v4, v4, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const-string v3, "message_inbox_no"

    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    const-string v3, "message"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v0, v7, v1}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 725
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_2

    .line 726
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "empty"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 725
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 729
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    invoke-virtual {v3, v4, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 734
    const-string v0, "empty"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 735
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 736
    const-string v0, "message_sender"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 737
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 739
    const-string v6, "message_sender"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v0, "message"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4, v7, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 744
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 746
    const-string v0, "participants_buddy_no"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "participants_inbox_no"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 748
    const-string v6, "participants_buddy_no"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v0, "participants_inbox_no"

    iget-object v6, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const-string v6, "participants_buddy_name"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const-string v0, "participant"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4, v7, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 754
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 756
    const-string v0, "buddy_no"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "inbox_no"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " IN ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 759
    const-string v5, "buddy_no"

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v0, "inbox_no"

    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const-string v0, "inbox_buddy_relation"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4, v7, v3}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 765
    :cond_2
    return-void
.end method

.method private v()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 768
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 769
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 770
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 771
    const-string v0, ""

    .line 772
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    invoke-virtual {v4, v5, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 773
    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    const-string v4, "empty"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 783
    const-string v4, "participants_buddy_no"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "participants_inbox_no"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    const-string v4, "message_sender"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "message_inbox_no"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    const-string v4, "inbox_last_msg_sender"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "inbox_no"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 787
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 788
    const-string v4, "message_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "chaton_id"

    invoke-virtual {v5, v6, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 791
    const-string v5, "inbox_last_msg_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "chaton_id"

    invoke-virtual {v6, v7, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 795
    const-string v5, "message"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v6, v9, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v2, v6, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 796
    const-string v0, "participant"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v5, v2, v8

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 797
    const-string v0, "inbox"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_0
.end method

.method private w()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 802
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "old_chaton_id"

    invoke-virtual {v1, v2, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 804
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 816
    :cond_0
    :goto_0
    return-void

    .line 808
    :cond_1
    const-string v2, "message_sender"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 811
    const-string v3, "message_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 814
    const-string v3, "message"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v0, v4, v2}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 594
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 596
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "inbox_last_message"

    aput-object v0, v4, v7

    const-string v0, "inbox_last_msg_sender"

    aput-object v0, v4, v8

    const-string v0, "inbox_last_msg_no"

    aput-object v0, v4, v9

    const-string v0, "inbox_last_time"

    aput-object v0, v4, v10

    const-string v0, "inbox_unread_count"

    aput-object v0, v4, v11

    const/4 v0, 0x5

    const-string v1, "inbox_trunk_unread_count"

    aput-object v1, v4, v0

    .line 597
    const-string v1, "inbox"

    const-string v2, "inbox_no=?"

    new-array v3, v8, [Ljava/lang/String;

    aput-object p1, v3, v7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 599
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 600
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 601
    const-string v1, "inbox_last_message"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const-string v1, "inbox_last_msg_sender"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v1, "inbox_last_msg_no"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 604
    const-string v1, "inbox_last_time"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 605
    const-string v1, "inbox_unread_count"

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 606
    const-string v1, "inbox_trunk_unread_count"

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 609
    :cond_0
    if-eqz v0, :cond_1

    .line 610
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 612
    :cond_1
    return-object v6
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 1164
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 1165
    invoke-virtual {v0, p1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1, p4, p5}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1166
    return-object v0
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 113
    iput-object p1, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 122
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    .line 126
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->w:Z

    move v3, v2

    .line 128
    :goto_0
    sget-object v0, Lcom/sec/chaton/provider/a/a;->t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ChatListInfoEntry;->chatroominfodetail:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1b

    .line 129
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_0

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "============== ChatRoom#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " =============="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    sget-object v0, Lcom/sec/chaton/provider/a/a;->t:Lcom/sec/chaton/io/entry/ChatListInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ChatListInfoEntry;->chatroominfodetail:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->g()V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chattype:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/provider/a/a;->c:I

    .line 140
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->activated:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deactivated ChatRoom#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " activated : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->activated:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , sessionid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , chattype : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chattype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , ip : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", port : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->port:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,  creatorChatOnId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->creatorchatonid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_4

    .line 152
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not Supported chat room type Chat TYPE ==>> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 145
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatRoom#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " activated : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->activated:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , sessionid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , chattype : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chattype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , ip : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", port : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->port:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,  creatorChatOnId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->creatorchatonid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 319
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 320
    sput-boolean v7, Lcom/sec/chaton/provider/a/a;->v:Z

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->g()V

    .line 319
    throw v0

    .line 161
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->creatorchatonid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 162
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not Supported receiver of BR2  ==>> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "creator  ==>> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->creatorchatonid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 171
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/sec/chaton/provider/a/a;->c:I

    if-ne v0, v7, :cond_6

    .line 172
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->o()V

    .line 173
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 174
    const-string v0, "No ChatMember list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 179
    :cond_6
    iget v0, p0, Lcom/sec/chaton/provider/a/a;->c:I

    if-nez v0, :cond_7

    .line 180
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 181
    const-string v0, "No ChatMember list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 187
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->q()V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->activated:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->activated:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_8

    .line 191
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->r()V

    .line 192
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 193
    const-string v0, "This chat room was deactivated..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 198
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_9

    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "My ChatON ID :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "chaton_id"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v1, v2

    .line 202
    :goto_3
    iget v0, p0, Lcom/sec/chaton/provider/a/a;->c:I

    if-ge v1, v0, :cond_f

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->chatroommemberinfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;

    .line 206
    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v5, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v4, v5, :cond_a

    .line 207
    const-string v4, "true"

    iput-object v4, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    .line 210
    :cond_a
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 211
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-ne v4, v7, :cond_b

    .line 212
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    const-string v5, "false"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 213
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ME] Exit ChatMember #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ==>> insession : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",userID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", starttime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->starttime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , profileImageAddr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimageaddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,  profileImagePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimagepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_b
    :goto_4
    invoke-direct {p0, v0}, Lcom/sec/chaton/provider/a/a;->a(Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;)V

    .line 202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 216
    :cond_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ME] ChatMember #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ==>> insession : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",userID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", starttime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->starttime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , profileImageAddr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimageaddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,  profileImagePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimagepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 221
    :cond_d
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-ne v4, v7, :cond_b

    .line 222
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    const-string v5, "false"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exit ChatMember #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ==>> insession : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",userID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", starttime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->starttime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , profileImageAddr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimageaddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,  profileImagePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimagepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 226
    :cond_e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChatMember #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ==>> insession : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->insession:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",userID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->userid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", starttime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->starttime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , profileImageAddr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimageaddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,  profileImagePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetailMemberInfo;->profileimagepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 233
    :cond_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_10

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " makeUserList :: Participants count("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ChatType("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_10
    iget-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->p:Z

    if-eqz v0, :cond_11

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->p:Z

    .line 239
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->p()V

    .line 240
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 241
    const-string v0, "In chat list, Mutidevice close Chat room  "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "chaton_id"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 248
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_2

    .line 249
    const-string v0, "In chat list, My chaton id was not inculded.... wrong chat list "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 255
    :cond_12
    iget-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->q:Z

    if-eqz v0, :cond_13

    .line 256
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/provider/a/a;->q:Z

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 261
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_17

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 262
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    .line 263
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    .line 273
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_19

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    .line 288
    :cond_14
    :goto_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_15

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InBoxNo :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Title :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :cond_15
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->i()V

    .line 296
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->t()V

    .line 299
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->u()V

    .line 303
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->v()V

    .line 305
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_16

    .line 306
    const-string v0, "========================================"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_16
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/provider/a/a;->w:Z

    goto/16 :goto_2

    .line 266
    :cond_17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    .line 268
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    .line 269
    iget v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    .line 270
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->n()V

    goto/16 :goto_5

    .line 278
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    goto :goto_6

    .line 282
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    goto :goto_6

    .line 285
    :cond_1a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    goto/16 :goto_6

    .line 311
    :cond_1b
    invoke-direct {p0}, Lcom/sec/chaton/provider/a/a;->w()V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 314
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/chaton/provider/a/a;->y:J

    .line 315
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-ne v0, v7, :cond_1c

    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "==========duration :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v1, Lcom/sec/chaton/provider/a/a;->y:J

    sget-wide v3, Lcom/sec/chaton/provider/a/a;->x:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=========="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 320
    sput-boolean v7, Lcom/sec/chaton/provider/a/a;->v:Z

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/provider/a/a;->g()V

    .line 323
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1171
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1174
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 1175
    invoke-virtual {v0, p1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 1176
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 1179
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 1180
    invoke-virtual {v0, p1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->r:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1, p4}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    .line 1181
    return-void
.end method

.method public b(Ljava/lang/String;)J
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 992
    const-wide/16 v6, 0x0

    .line 993
    const-string v1, "inbox"

    const-string v2, "inbox_no=?"

    new-array v3, v0, [Ljava/lang/String;

    aput-object p1, v3, v8

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "inbox_last_time"

    aput-object v0, v4, v8

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 994
    if-eqz v2, :cond_1

    .line 995
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 996
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 998
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1000
    :goto_1
    return-wide v0

    :cond_0
    move-wide v0, v6

    goto :goto_0

    :cond_1
    move-wide v0, v6

    goto :goto_1
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 330
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    .line 331
    iput v1, p0, Lcom/sec/chaton/provider/a/a;->c:I

    .line 333
    iput-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->o:Z

    .line 334
    iput-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->p:Z

    .line 335
    iput-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->q:Z

    .line 336
    iput-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->a:Z

    .line 337
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/provider/a/a;->d:I

    .line 339
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    .line 341
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    .line 342
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 345
    iput-object v2, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    .line 347
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->m:Ljava/lang/StringBuilder;

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    .line 350
    iput-object v2, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    .line 352
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/a/a;->n:Ljava/lang/StringBuilder;

    .line 353
    return-void
.end method

.method public h()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 617
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 618
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v4, v7

    .line 619
    const-string v1, "inbox"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inbox_session_id IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 620
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 621
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 623
    :cond_0
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 628
    :cond_1
    if-eqz v0, :cond_2

    .line 629
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 631
    :cond_2
    return-object v6
.end method

.method public i()V
    .locals 6

    .prologue
    .line 636
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 638
    const-string v1, "inbox_no"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->a:Z

    if-eqz v1, :cond_0

    .line 640
    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v1, v2, :cond_3

    .line 641
    iget-boolean v1, p0, Lcom/sec/chaton/provider/a/a;->o:Z

    if-eqz v1, :cond_2

    .line 642
    const-string v1, "inbox_title_fixed"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    const-string v1, "inbox_title"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->ip:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 653
    const-string v1, "inbox_server_ip"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string v1, "inbox_server_port"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->port:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    :cond_1
    const-string v1, "inbox_participants"

    iget v2, p0, Lcom/sec/chaton/provider/a/a;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 657
    const-string v1, "inbox_session_id"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    const-string v1, "inbox"

    const-string v2, "inbox_no=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/provider/a/a;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 660
    return-void

    .line 645
    :cond_2
    const-string v1, "inbox_title"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 648
    :cond_3
    const-string v1, "inbox_title"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 930
    .line 932
    sget-object v0, Lcom/sec/chaton/provider/a/b;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 971
    :cond_0
    :goto_0
    return-object v5

    .line 934
    :pswitch_0
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 935
    sget-object v1, Lcom/sec/chaton/e/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    .line 937
    const-string v1, "inbox"

    const-string v2, "inbox_session_id=?"

    new-array v3, v4, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    aput-object v0, v3, v6

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 940
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 941
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 942
    const-string v0, "inbox_no"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 947
    :goto_1
    if-eqz v1, :cond_0

    .line 948
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 944
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->b:Lcom/sec/chaton/e/r;

    iget-object v4, p0, Lcom/sec/chaton/provider/a/a;->i:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_1

    .line 947
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 948
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 947
    :cond_2
    throw v0

    .line 954
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 957
    const-string v2, "inbox"

    const-string v3, "inbox_session_id=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    aput-object v0, v4, v6

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 959
    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 960
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 961
    const-string v0, "inbox_no"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v5

    .line 965
    :cond_3
    if-eqz v1, :cond_0

    .line 966
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 965
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_4

    .line 966
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 965
    :cond_4
    throw v0

    .line 932
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public k()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 975
    const-string v1, "inbox"

    const-string v2, "inbox_session_id=?"

    new-array v3, v7, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    aput-object v0, v3, v6

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v4, v6

    const-string v0, "inbox_valid"

    aput-object v0, v4, v7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 977
    const-string v0, ""

    .line 978
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 979
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 980
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 981
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 982
    const-string v0, "INVALID_INBOX"

    .line 985
    :cond_0
    if-eqz v1, :cond_1

    .line 986
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 988
    :cond_1
    return-object v0
.end method

.method public l()Z
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1004
    const-string v1, "inbox"

    const-string v2, "inbox_session_id=?"

    new-array v3, v8, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    aput-object v0, v3, v9

    new-array v4, v13, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v4, v9

    const-string v0, "inbox_valid"

    aput-object v0, v4, v8

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1005
    const-string v1, ""

    .line 1006
    const-string v0, ""

    .line 1009
    if-eqz v12, :cond_7

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_7

    .line 1010
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1011
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1012
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v1

    .line 1014
    :goto_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1015
    const-string v11, ""

    .line 1016
    const-string v10, ""

    .line 1018
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1020
    const-string v2, "message_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    const-string v2, "message_session_id"

    iget-object v3, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    const-string v2, "message"

    const-string v3, "message_inbox_no=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object v7, v4, v9

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1024
    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1025
    const-string v2, "participant"

    const-string v3, "participants_inbox_no=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object v7, v4, v9

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1027
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1029
    const-string v1, "inbox_valid"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const-string v1, "inbox_session_id"

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    const-string v1, "inbox_last_msg_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v5, ""

    invoke-virtual {v2, v3, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const-string v1, "inbox"

    const-string v2, "inbox_no=?"

    new-array v3, v8, [Ljava/lang/String;

    aput-object v7, v3, v9

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1034
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1035
    if-eqz v4, :cond_6

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 1036
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1037
    const-string v0, "participants_buddy_no"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1038
    const-string v0, "participants_buddy_name"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    .line 1041
    :goto_1
    if-eqz v4, :cond_0

    .line 1042
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1044
    :cond_0
    const-string v0, "inbox_buddy_relation"

    const-string v1, "inbox_no=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object v7, v4, v9

    invoke-virtual {p0, v0, v1, v4}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1046
    invoke-virtual {p0, v7}, Lcom/sec/chaton/provider/a/a;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 1047
    const-wide/16 v10, 0x0

    cmp-long v4, v0, v10

    if-nez v4, :cond_3

    .line 1048
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1052
    :goto_2
    const-string v4, "_id"

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1053
    const-string v4, "message_inbox_no"

    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v4, "message_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1055
    const-string v4, "message_content_type"

    sget-object v6, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v6}, Lcom/sec/chaton/e/w;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1056
    const-string v4, "message_content"

    const-string v6, "%d,%s,%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v10, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    invoke-virtual {v10}, Lcom/sec/chaton/e/aj;->a()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    aput-object v3, v7, v8

    aput-object v2, v7, v13

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    const-string v2, "message_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1058
    const-string v2, "message_time_text"

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    const-string v0, "message"

    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1061
    :cond_1
    if-eqz v12, :cond_2

    .line 1062
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v8

    .line 1070
    :goto_3
    return v0

    .line 1050
    :cond_3
    const-wide/16 v10, 0x1

    add-long/2addr v0, v10

    goto :goto_2

    .line 1066
    :cond_4
    if-eqz v12, :cond_5

    .line 1067
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v9

    goto :goto_3

    :cond_6
    move-object v2, v10

    move-object v3, v11

    goto/16 :goto_1

    :cond_7
    move-object v7, v1

    goto/16 :goto_0
.end method

.method public m()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 1074
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v4, v7

    const-string v0, "inbox_valid"

    aput-object v0, v4, v6

    .line 1077
    const-string v1, "inbox"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inbox_session_id IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1078
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "upateInboxInvalidAndChangeSessionId() mOldSessionIDs["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/provider/a/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] is invalided."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 1081
    invoke-direct {p0, v8}, Lcom/sec/chaton/provider/a/a;->a(Landroid/database/Cursor;)V

    .line 1083
    const-string v1, "inbox"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inbox_session_id IN (\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/provider/a/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1084
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "upateInboxInvalidAndChangeSessionId() mNewSessionIDs["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/provider/a/a;->s:Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/ChatListInfoEntryDetail;->sessionid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is invalided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1087
    invoke-direct {p0, v0}, Lcom/sec/chaton/provider/a/a;->a(Landroid/database/Cursor;)V

    .line 1089
    :cond_0
    if-eqz v0, :cond_1

    .line 1090
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v6

    .line 1096
    :goto_0
    if-eqz v8, :cond_2

    .line 1097
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1100
    :cond_2
    return v0

    :cond_3
    move v0, v7

    goto :goto_0
.end method
