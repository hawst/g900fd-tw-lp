.class public Lcom/sec/chaton/provider/ChatONProvider2;
.super Lcom/sec/chaton/provider/BaseContentProvider;
.source "ChatONProvider2.java"


# instance fields
.field private a:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/chaton/provider/BaseContentProvider;-><init>()V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 860
    .line 866
    const-string v1, "replace"

    invoke-direct {p0, p3, v1, v6}, Lcom/sec/chaton/provider/ChatONProvider2;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v5

    .line 868
    sparse-switch p2, :sswitch_data_0

    .line 980
    :goto_0
    return-object v0

    .line 870
    :sswitch_0
    sget-object p3, Lcom/sec/chaton/e/am;->a:Landroid/net/Uri;

    .line 871
    const-string v3, "anicon_category"

    .line 872
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "category_id"

    aput-object v2, v1, v6

    const-string v2, "=?"

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 873
    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "category_id"

    invoke-virtual {p4, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 875
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "category_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v7, v1

    move-object v1, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v7

    .line 969
    :goto_1
    if-nez v5, :cond_1

    .line 971
    invoke-virtual {p1, v4, v0, p4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_0
    :goto_2
    move-object v0, v1

    .line 980
    goto :goto_0

    .line 879
    :sswitch_1
    sget-object p3, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    .line 880
    const-string v3, "anicon_item"

    .line 881
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "anicon_id"

    aput-object v2, v1, v6

    const-string v2, "=?"

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 882
    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "anicon_id"

    invoke-virtual {p4, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 884
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "anicon_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v7, v1

    move-object v1, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v7

    .line 885
    goto :goto_1

    .line 937
    :sswitch_2
    if-eqz p3, :cond_2

    .line 939
    const-string v3, "download_item"

    .line 940
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "item_id"

    aput-object v2, v1, v6

    const-string v2, "=?"

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 941
    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "item_id"

    invoke-virtual {p4, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 943
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "item_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v7, v1

    move-object v1, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_1

    .line 948
    :sswitch_3
    sget-object p3, Lcom/sec/chaton/e/at;->a:Landroid/net/Uri;

    .line 949
    const-string v3, "font_filter"

    .line 950
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "filter_id"

    aput-object v2, v1, v6

    const-string v2, "=?"

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 951
    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "filter_id"

    invoke-virtual {p4, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 953
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "filter_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v7, v1

    move-object v1, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v7

    .line 954
    goto/16 :goto_1

    .line 957
    :sswitch_4
    sget-object p3, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    .line 958
    const-string v3, "more_apps"

    .line 959
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "id"

    aput-object v2, v1, v6

    const-string v2, "=?"

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 960
    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "id"

    invoke-virtual {p4, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 962
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v7, v1

    move-object v1, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v7

    .line 963
    goto/16 :goto_1

    .line 973
    :cond_1
    invoke-virtual {p0, p3, p4, v3, v2}, Lcom/sec/chaton/provider/ChatONProvider2;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 975
    if-nez v2, :cond_0

    .line 976
    invoke-virtual {p1, v4, v0, p4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_2

    :cond_2
    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    move-object p3, v0

    move-object v1, v0

    goto/16 :goto_1

    .line 868
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0xf -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 992
    if-nez p1, :cond_1

    .line 1002
    :cond_0
    :goto_0
    return p3

    .line 996
    :cond_1
    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 998
    if-eqz v0, :cond_0

    .line 1002
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p3

    goto :goto_0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 102
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    const-string v2, "anicon_category"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "anicon_category"

    aput-object v3, v2, v5

    const-string v3, "/*"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    const-string v2, "anicon_item"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "anicon_item"

    aput-object v3, v2, v5

    const-string v3, "/"

    aput-object v3, v2, v4

    const-string v3, "recentused"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "anicon_item"

    aput-object v3, v2, v5

    const-string v3, "/"

    aput-object v3, v2, v4

    const-string v3, "group"

    aput-object v3, v2, v6

    const-string v3, "/*"

    aput-object v3, v2, v7

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "anicon_item"

    aput-object v3, v2, v5

    const-string v3, "/*"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/"

    aput-object v3, v2, v4

    const-string v3, "delete_not_installed_item"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/"

    aput-object v3, v2, v4

    const-string v3, "init_newly_installed"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*/"

    aput-object v3, v2, v4

    const-string v3, "join_font_filter"

    aput-object v3, v2, v6

    const-string v3, "/*"

    aput-object v3, v2, v7

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*/"

    aput-object v3, v2, v4

    const-string v3, "install"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*/"

    aput-object v3, v2, v4

    const-string v3, "newly_installed"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*/"

    aput-object v3, v2, v4

    const-string v3, "anicon_group"

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "download_item"

    aput-object v3, v2, v5

    const-string v3, "/*/*"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "font_filter"

    aput-object v3, v2, v5

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "font_filter"

    aput-object v3, v2, v5

    const-string v3, "/*"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.provider2"

    const-string v2, "more_apps"

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 746
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 747
    iget-object v2, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 754
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 757
    sparse-switch v2, :sswitch_data_0

    move-object v2, v0

    move-object v3, v0

    .line 837
    :goto_0
    :try_start_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 838
    new-instance v1, Lcom/sec/chaton/e/bk;

    invoke-direct {v1}, Lcom/sec/chaton/e/bk;-><init>()V

    invoke-virtual {v1, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v1

    .line 840
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 841
    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    .line 844
    :cond_0
    invoke-virtual {v1, v4}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 846
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 848
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    .line 849
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 853
    :cond_1
    :goto_1
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 856
    return v0

    .line 759
    :sswitch_0
    const/4 v0, 0x2

    :try_start_1
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "category_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 760
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 761
    const-string v0, "anicon_category"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 762
    goto :goto_0

    .line 764
    :sswitch_1
    const-string v2, "anicon_category"

    move-object v3, v2

    move-object v2, v0

    .line 765
    goto :goto_0

    .line 768
    :sswitch_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "anicon_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 769
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 770
    const-string v0, "anicon_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 771
    goto/16 :goto_0

    .line 773
    :sswitch_3
    const-string v2, "anicon_item"

    move-object v3, v2

    move-object v2, v0

    .line 774
    goto/16 :goto_0

    .line 777
    :sswitch_4
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "item_type"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=? AND "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "item_id"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 778
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 779
    const-string v0, "download_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 780
    goto/16 :goto_0

    .line 782
    :sswitch_5
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "item_type"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 783
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 784
    const-string v0, "download_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 785
    goto/16 :goto_0

    .line 787
    :sswitch_6
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "install"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 788
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "0"

    aput-object v5, v0, v3

    .line 789
    const-string v3, "download_item"

    goto/16 :goto_0

    .line 793
    :sswitch_7
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "filter_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 794
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 795
    const-string v0, "font_filter"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 796
    goto/16 :goto_0

    .line 798
    :sswitch_8
    const-string v2, "font_filter"

    move-object v3, v2

    move-object v2, v0

    .line 799
    goto/16 :goto_0

    .line 833
    :sswitch_9
    const-string v2, "more_apps"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_0

    .line 853
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    .line 757
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_3
        0x6 -> :sswitch_2
        0xf -> :sswitch_5
        0x11 -> :sswitch_4
        0x12 -> :sswitch_6
        0x19 -> :sswitch_8
        0x1a -> :sswitch_7
        0x22 -> :sswitch_9
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 162
    sparse-switch v0, :sswitch_data_0

    .line 214
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 164
    :sswitch_0
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.emoticon.category"

    goto :goto_0

    .line 166
    :sswitch_1
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.emoticon.category"

    goto :goto_0

    .line 169
    :sswitch_2
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.emoticon.item"

    goto :goto_0

    .line 171
    :sswitch_3
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.emoticon.item"

    goto :goto_0

    .line 173
    :sswitch_4
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.emoticon.item"

    goto :goto_0

    .line 176
    :sswitch_5
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.download.item"

    goto :goto_0

    .line 178
    :sswitch_6
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.download.item"

    goto :goto_0

    .line 180
    :sswitch_7
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.download.item"

    goto :goto_0

    .line 211
    :sswitch_8
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.moreapps.item"

    goto :goto_0

    .line 162
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_4
        0x8 -> :sswitch_7
        0xf -> :sswitch_5
        0x11 -> :sswitch_6
        0x22 -> :sswitch_8
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 593
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 594
    iget-object v2, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 596
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 599
    sparse-switch v2, :sswitch_data_0

    .line 623
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 626
    return-object v0

    .line 612
    :sswitch_0
    :try_start_0
    invoke-direct {p0, v1, v2, p1, p2}, Lcom/sec/chaton/provider/ChatONProvider2;->a(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 613
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 615
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->a()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 616
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 623
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 599
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0xc -> :sswitch_0
        0xf -> :sswitch_0
        0x13 -> :sswitch_0
        0x15 -> :sswitch_0
        0x17 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_0
        0x22 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->b()V

    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 224
    const/4 v4, 0x0

    .line 226
    const/4 v3, 0x0

    .line 227
    const/4 v2, 0x0

    .line 228
    const/4 v1, 0x0

    .line 229
    const/4 v0, 0x0

    .line 231
    packed-switch v6, :pswitch_data_0

    .line 574
    :goto_0
    :pswitch_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 575
    new-instance v4, Lcom/sec/chaton/e/bk;

    invoke-direct {v4}, Lcom/sec/chaton/e/bk;-><init>()V

    invoke-virtual {v4, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v3

    .line 577
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 578
    invoke-virtual {v3, v2, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    .line 581
    :cond_0
    invoke-virtual {v3, v5, p2, p5}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 583
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 584
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_1
    move-object v0, v1

    .line 588
    :goto_1
    return-object v0

    .line 234
    :pswitch_1
    const-string v3, "anicon_category"

    .line 235
    sget-object v0, Lcom/sec/chaton/e/am;->a:Landroid/net/Uri;

    goto :goto_0

    .line 242
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    const-string v3, "anicon_category"

    .line 245
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "category_id"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 246
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v1, v6

    .line 247
    sget-object v6, Lcom/sec/chaton/e/am;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 254
    :pswitch_3
    const-string v3, "anicon_item"

    .line 255
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    goto :goto_0

    .line 262
    :pswitch_4
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 264
    const-string v3, "anicon_item"

    .line 265
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "package_id"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 266
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v1, v6

    .line 267
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    goto/16 :goto_0

    .line 274
    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM anicon_item WHERE sent_time IS NOT NULL AND package_id IN (SELECT item_id FROM download_item WHERE install != 0 AND item_type == \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sent_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT 50"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 278
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 285
    :pswitch_6
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 287
    const/16 v1, 0x3a

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "SELECT * FROM "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "anicon_item"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, " D3 "

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "\tINNER JOIN ("

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "\t\tSELECT D1."

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "item_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, " AS "

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "package_id"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, " \t\t\tFROM "

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "download_item"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, " D1 "

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, " \t\t\tINNER JOIN "

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "\t\t\t(SELECT "

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, ", "

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "data1"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ", "

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "data2"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, ", "

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "install"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "\t\t\t\tFROM "

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "download_item"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, " WHERE "

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "item_id"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "=\'"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    aput-object v0, v1, v2

    const/16 v0, 0x1a

    const-string v2, "\' AND "

    aput-object v2, v1, v0

    const/16 v0, 0x1b

    const-string v2, "item_type"

    aput-object v2, v1, v0

    const/16 v0, 0x1c

    const-string v2, "=\'"

    aput-object v2, v1, v0

    const/16 v0, 0x1d

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x1e

    const-string v2, "\') AS D2"

    aput-object v2, v1, v0

    const/16 v0, 0x1f

    const-string v2, "\t\t\tON D1."

    aput-object v2, v1, v0

    const/16 v0, 0x20

    const-string v2, "data1"

    aput-object v2, v1, v0

    const/16 v0, 0x21

    const-string v2, "=D2."

    aput-object v2, v1, v0

    const/16 v0, 0x22

    const-string v2, "data1"

    aput-object v2, v1, v0

    const/16 v0, 0x23

    const-string v2, " AND "

    aput-object v2, v1, v0

    const/16 v0, 0x24

    const-string v2, "\t\t\t   D1."

    aput-object v2, v1, v0

    const/16 v0, 0x25

    const-string v2, "data2"

    aput-object v2, v1, v0

    const/16 v0, 0x26

    const-string v2, "=D2."

    aput-object v2, v1, v0

    const/16 v0, 0x27

    const-string v2, "data2"

    aput-object v2, v1, v0

    const/16 v0, 0x28

    const-string v2, " AND "

    aput-object v2, v1, v0

    const/16 v0, 0x29

    const-string v2, "\t\t\t   D1."

    aput-object v2, v1, v0

    const/16 v0, 0x2a

    const-string v2, "install"

    aput-object v2, v1, v0

    const/16 v0, 0x2b

    const-string v2, "!=0 OR "

    aput-object v2, v1, v0

    const/16 v0, 0x2c

    const-string v2, "\t\t\t   D1."

    aput-object v2, v1, v0

    const/16 v0, 0x2d

    const-string v2, "_id"

    aput-object v2, v1, v0

    const/16 v0, 0x2e

    const-string v2, "=D2."

    aput-object v2, v1, v0

    const/16 v0, 0x2f

    const-string v2, "_id"

    aput-object v2, v1, v0

    const/16 v0, 0x30

    const-string v2, "\t\t\t   ORDER BY D1."

    aput-object v2, v1, v0

    const/16 v0, 0x31

    const-string v2, "install"

    aput-object v2, v1, v0

    const/16 v0, 0x32

    const-string v2, " DESC"

    aput-object v2, v1, v0

    const/16 v0, 0x33

    const-string v2, "\t\t) D4"

    aput-object v2, v1, v0

    const/16 v0, 0x34

    const-string v2, "\tWHERE D3."

    aput-object v2, v1, v0

    const/16 v0, 0x35

    const-string v2, "package_id"

    aput-object v2, v1, v0

    const/16 v0, 0x36

    const-string v2, "=D4."

    aput-object v2, v1, v0

    const/16 v0, 0x37

    const-string v2, "package_id"

    aput-object v2, v1, v0

    const/16 v0, 0x38

    const-string v2, " ORDER BY "

    aput-object v2, v1, v0

    const/16 v0, 0x39

    const-string v2, "anicon_id"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 320
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 327
    :pswitch_7
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 329
    const-string v3, "anicon_item"

    .line 330
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "anicon_id"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 331
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v1, v6

    .line 332
    sget-object v6, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 338
    :pswitch_8
    const-string v3, "download_item"

    .line 340
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/ar;->a(Ljava/lang/String;)Lcom/sec/chaton/e/ar;

    move-result-object v0

    .line 342
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "item_type"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 343
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v6

    .line 345
    invoke-static {v0}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 352
    :pswitch_9
    const-string v3, "download_item"

    .line 354
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/ar;->a(Ljava/lang/String;)Lcom/sec/chaton/e/ar;

    move-result-object v0

    .line 356
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "item_type"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=? AND "

    aput-object v6, v1, v2

    const/4 v2, 0x2

    const-string v6, "install"

    aput-object v6, v1, v2

    const/4 v2, 0x3

    const-string v6, "!=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 357
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v1, v6

    .line 359
    invoke-static {v0}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 366
    :pswitch_a
    const-string v3, "download_item"

    .line 368
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/ar;->a(Ljava/lang/String;)Lcom/sec/chaton/e/ar;

    move-result-object v0

    .line 370
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "item_type"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=? AND "

    aput-object v6, v1, v2

    const/4 v2, 0x2

    const-string v6, "newly_installed"

    aput-object v6, v1, v2

    const/4 v2, 0x3

    const-string v6, "!=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 371
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v1, v6

    .line 373
    invoke-static {v0}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 380
    :pswitch_b
    const-string v3, "download_item"

    .line 382
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/ar;->a(Ljava/lang/String;)Lcom/sec/chaton/e/ar;

    move-result-object v6

    .line 383
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 385
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v7, "item_type"

    aput-object v7, v1, v2

    const/4 v2, 0x1

    const-string v7, "=? AND "

    aput-object v7, v1, v2

    const/4 v2, 0x2

    const-string v7, "item_id"

    aput-object v7, v1, v2

    const/4 v2, 0x3

    const-string v7, "=?"

    aput-object v7, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 386
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v6}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v7

    const/4 v7, 0x1

    aput-object v0, v1, v7

    .line 388
    invoke-static {v6}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 394
    :pswitch_c
    const/16 v0, 0x58

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "SELECT * FROM ( "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "\tSELECT "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "\t\tD1."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ", D2."

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "newly_installed"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ", D1."

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ", D1."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\t\tD1."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "extras"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", D2.max_install"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ", D1."

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ", D1."

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\tFROM "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "download_item"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " D1"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "\tINNER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "\t(SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "\t\tMAX("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ") AS max_install,"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "\t\tMAX("

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "newly_installed"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "newly_installed"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "\t\tMIN("

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "\tFROM "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "download_item"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "\tWHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "=\'"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "\' AND "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " != 0 AND "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NOT NULL AND "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " IS NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "\tGROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, ") D2 "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "\tON D1."

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " = D2."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " UNION "

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "\tSELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "newly_installed"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "extras"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, " AS max_install, "

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "\tFROM "

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "download_item"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "\tWHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "=\'"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "\' AND "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " != 0 AND "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " IS NULL AND "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, " IS NULL "

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, ") ORDER BY max_install DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 427
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 436
    :pswitch_d
    const-string v3, "font_filter"

    .line 437
    sget-object v0, Lcom/sec/chaton/e/at;->a:Landroid/net/Uri;

    goto/16 :goto_0

    .line 444
    :pswitch_e
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 446
    const-string v3, "font_filter"

    .line 447
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v6, "filter_id"

    aput-object v6, v1, v2

    const/4 v2, 0x1

    const-string v6, "=?"

    aput-object v6, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 448
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v1, v6

    .line 449
    sget-object v6, Lcom/sec/chaton/e/at;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 455
    :pswitch_f
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 456
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT * FROM download_item, font_filter WHERE reference_id == filter_id AND item_id == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "item_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 459
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 566
    :pswitch_10
    const-string v3, "more_apps"

    .line 567
    sget-object v0, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    goto/16 :goto_0

    :cond_2
    move-object v0, v4

    goto/16 :goto_1

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 631
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 632
    iget-object v2, p0, Lcom/sec/chaton/provider/ChatONProvider2;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 639
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 642
    sparse-switch v2, :sswitch_data_0

    move-object v2, v0

    move-object v3, v0

    .line 723
    :goto_0
    :try_start_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 724
    new-instance v1, Lcom/sec/chaton/e/bk;

    invoke-direct {v1}, Lcom/sec/chaton/e/bk;-><init>()V

    invoke-virtual {v1, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v1

    .line 726
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 727
    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    .line 730
    :cond_0
    invoke-virtual {v1, v4, p2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 731
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 733
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    .line 734
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 738
    :cond_1
    :goto_1
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 741
    return v0

    .line 645
    :sswitch_0
    const/4 v0, 0x4

    :try_start_1
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "item_type"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=? AND "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "item_id"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 646
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 647
    const-string v0, "download_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 648
    goto :goto_0

    .line 651
    :sswitch_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "item_type"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 652
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 653
    const-string v0, "download_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 654
    goto/16 :goto_0

    .line 657
    :sswitch_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "anicon_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 658
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 659
    const-string v0, "anicon_item"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 660
    goto/16 :goto_0

    .line 662
    :sswitch_3
    const-string v2, "anicon_item"

    move-object v3, v2

    move-object v2, v0

    .line 663
    goto/16 :goto_0

    .line 666
    :sswitch_4
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "category_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 667
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 668
    const-string v0, "anicon_category"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 669
    goto/16 :goto_0

    .line 671
    :sswitch_5
    const-string v2, "anicon_category"

    move-object v3, v2

    move-object v2, v0

    .line 672
    goto/16 :goto_0

    .line 675
    :sswitch_6
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "filter_id"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "=?"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 676
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v5

    .line 677
    const-string v0, "font_filter"

    move-object v7, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v7

    .line 678
    goto/16 :goto_0

    .line 680
    :sswitch_7
    const-string v2, "font_filter"

    move-object v3, v2

    move-object v2, v0

    .line 681
    goto/16 :goto_0

    .line 714
    :sswitch_8
    const-string v2, "download_item"

    move-object v3, v2

    move-object v2, v0

    .line 715
    goto/16 :goto_0

    .line 718
    :sswitch_9
    const-string v2, "more_apps"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_0

    .line 738
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    .line 642
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x6 -> :sswitch_2
        0xf -> :sswitch_1
        0x11 -> :sswitch_0
        0x19 -> :sswitch_7
        0x1a -> :sswitch_6
        0x21 -> :sswitch_8
        0x22 -> :sswitch_9
    .end sparse-switch
.end method
