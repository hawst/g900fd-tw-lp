.class public Lcom/sec/chaton/provider/ChatONProvider;
.super Lcom/sec/chaton/provider/BaseContentProvider;
.source "ChatONProvider.java"


# static fields
.field private static final a:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/sec/chaton/provider/ChatONProvider;->b()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/chaton/provider/BaseContentProvider;-><init>()V

    .line 2797
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 2929
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2933
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2934
    const-wide v2, 0x1cf7c5800L

    .line 2935
    sub-long v12, v0, v2

    .line 2938
    :try_start_0
    const-string v1, "inbox"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 2939
    if-eqz v8, :cond_2

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 2940
    const/4 v0, 0x2

    .line 2972
    :goto_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2974
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2975
    if-eqz v9, :cond_0

    .line 2976
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2978
    :cond_0
    if-eqz v8, :cond_1

    .line 2979
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2982
    :cond_1
    return v0

    .line 2943
    :cond_2
    :try_start_2
    const-string v1, "inbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "inbox_no"

    aput-object v3, v2, v0

    const-string v3, "inbox_last_time < ? AND inbox_last_time NOT NULL AND inbox_last_time <> 0"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2945
    const-string v1, ""

    .line 2948
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 2949
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2950
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2952
    :cond_3
    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "inbox_no"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2953
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2956
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 2957
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2958
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2959
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2961
    const-string v1, "inbox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "inbox_no IN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2962
    const-string v1, "message"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message_inbox_no IN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2963
    const-string v1, "participant"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "participants_inbox_no IN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2964
    const-string v1, "inbox_buddy_relation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "inbox_no IN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v10

    .line 2966
    goto/16 :goto_0

    :cond_5
    move v0, v11

    .line 2968
    goto/16 :goto_0

    .line 2974
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2975
    if-eqz v9, :cond_6

    .line 2976
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2978
    :cond_6
    if-eqz v1, :cond_7

    .line 2979
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2974
    :cond_7
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2755
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2760
    :try_start_0
    const-string v1, "inbox"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "inbox_last_time"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "inbox_unread_count"

    aput-object v3, v2, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2761
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 2762
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2763
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2764
    const-string v0, "inbox_last_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2767
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2769
    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 2771
    invoke-virtual {v0, p2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 2775
    :cond_0
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2776
    const-string v3, "inbox_unread_count"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    .line 2777
    const-string v3, "inbox_unread_count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2779
    const-string v2, "inbox"

    invoke-virtual {p1, v2, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2781
    :goto_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2783
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2784
    if-eqz v1, :cond_1

    .line 2785
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2789
    :cond_1
    return v0

    .line 2783
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2784
    if-eqz v1, :cond_2

    .line 2785
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2783
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v9

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 5

    .prologue
    .line 2094
    const-wide/16 v0, 0x0

    .line 2097
    :try_start_0
    const-string v2, "cover_story_sample"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2106
    :cond_0
    :goto_0
    return-wide v0

    .line 2098
    :catch_0
    move-exception v2

    .line 2099
    const-string v2, "coverstory_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2100
    const-string v0, "cover_story_sample"

    const-string v1, "coverstory_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "coverstory_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2518
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2519
    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2520
    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524
    const-string v1, "participants_buddy_name"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p3, v3}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2527
    const-string v1, "participant"

    invoke-virtual {p1, v1, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)J
    .locals 3

    .prologue
    .line 2544
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2545
    const-string v1, "inbox_no"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546
    const-string v1, "buddy_no"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2547
    const-string v1, "chat_type"

    invoke-virtual {p4}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2549
    const-string v1, "inbox_buddy_relation"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/chaton/e/bk;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16

    .prologue
    .line 2603
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2605
    const-string v3, ""

    .line 2606
    move-object/from16 v0, p5

    array-length v4, v0

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 2607
    const/4 v3, 0x1

    aget-object v3, p5, v3

    .line 2609
    :cond_0
    sget-object v4, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 2610
    move-object/from16 v0, p5

    array-length v5, v0

    const/4 v6, 0x2

    if-le v5, v6, :cond_1

    .line 2611
    const/4 v4, 0x2

    aget-object v4, p5, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    .line 2613
    :cond_1
    const/4 v5, 0x0

    aget-object v8, p5, v5

    .line 2615
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2616
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, ""

    aput-object v7, v5, v6

    .line 2620
    :goto_0
    const/4 v7, 0x0

    .line 2622
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-virtual {v6, v0, v1, v2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2623
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-nez v9, :cond_5

    .line 2625
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8, v3, v4}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    .line 2626
    const/4 v3, 0x1

    .line 2679
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2681
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2684
    if-eqz v3, :cond_7

    .line 2686
    if-eqz v6, :cond_3

    .line 2687
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2690
    :cond_3
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2692
    :goto_2
    return-object v3

    .line 2618
    :cond_4
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    goto :goto_0

    .line 2628
    :cond_5
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2629
    const-string v8, "inbox_session_id"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2630
    const-string v9, "inbox_chat_type"

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v9

    .line 2632
    sget-object v10, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v10, v9, :cond_8

    .line 2633
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2636
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2637
    const-string v4, "inbox_no"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2638
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2639
    const-string v8, "inbox_session_id"

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2640
    const-string v3, "inbox"

    const-string v8, "inbox_no=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2642
    const/4 v3, 0x1

    .line 2643
    goto :goto_1

    .line 2647
    :cond_6
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 2649
    const-string v7, "inbox_no"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2650
    const-string v8, "inbox_buddy_relation"

    const-string v9, "inbox_no=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2653
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2654
    const-string v9, "inbox_valid"

    const-string v10, "N"

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2655
    const-string v9, "inbox"

    const-string v10, "inbox_no = ?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v7, v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2658
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2659
    const-string v10, "buddy_name"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2660
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 2661
    const-string v12, "_id"

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2662
    const-string v12, "message_inbox_no"

    invoke-virtual {v11, v12, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2663
    const-string v7, "message_type"

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2664
    const-string v7, "message_content_type"

    sget-object v12, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v12}, Lcom/sec/chaton/e/w;->a()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2665
    const-string v7, "message_content"

    const-string v12, "%d,%s,%s"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sget-object v15, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    invoke-virtual {v15}, Lcom/sec/chaton/e/aj;->a()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x0

    aget-object v15, p5, v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-static {v10}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2666
    const-string v7, "message_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v11, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2667
    const-string v7, "message_time_text"

    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string v12, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v10, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2668
    const-string v7, "message"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v11}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2671
    const/4 v7, 0x0

    aget-object v7, p5, v7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7, v3, v4}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2673
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 2681
    :catchall_0
    move-exception v3

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_7
    move-object v3, v6

    goto/16 :goto_2

    :cond_8
    move v3, v7

    goto/16 :goto_1
.end method

.method private a(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1534
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 1535
    sparse-switch p2, :sswitch_data_0

    .line 1871
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1538
    :sswitch_0
    const-string v1, "buddy"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1542
    :sswitch_1
    invoke-static {p1}, Lcom/sec/chaton/e/i;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1543
    const-string v2, "buddy"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "buddy_no=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1547
    :sswitch_2
    sget-object v1, Lcom/sec/chaton/e/ax;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1551
    :sswitch_3
    sget-object v1, Lcom/sec/chaton/e/ax;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1555
    :sswitch_4
    sget-object v1, Lcom/sec/chaton/e/ax;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1559
    :sswitch_5
    sget-object v1, Lcom/sec/chaton/e/ax;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1563
    :sswitch_6
    sget-object v1, Lcom/sec/chaton/e/ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1568
    :sswitch_7
    sget-object v1, Lcom/sec/chaton/e/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1572
    :sswitch_8
    sget-object v1, Lcom/sec/chaton/e/ax;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1576
    :sswitch_9
    sget-object v1, Lcom/sec/chaton/e/ax;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1580
    :sswitch_a
    sget-object v1, Lcom/sec/chaton/e/ax;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1584
    :sswitch_b
    sget-object v1, Lcom/sec/chaton/e/ax;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1588
    :sswitch_c
    sget-object v1, Lcom/sec/chaton/e/ax;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1592
    :sswitch_d
    sget-object v1, Lcom/sec/chaton/e/ax;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1596
    :sswitch_e
    const-string v1, "(select buddy_no from buddy where buddy_extra_info like \'%voip=1%\' and length(buddy_sainfo) > 0 intersect select distinct buddy_no from grouprelation a left outer join buddy as b on a.group_relation_buddy = b.buddy_no)"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1600
    :sswitch_f
    const-string v1, "(SELECT r.group_relation_buddy AS buddy_no, g._id AS _id, g.group_name AS group_name FROM grouprelation r JOIN buddy_group g ON r.group_relation_group=g._id WHERE g._id <> 1)"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1604
    :sswitch_10
    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1608
    :sswitch_11
    invoke-static {p1}, Lcom/sec/chaton/e/j;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1609
    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1613
    :sswitch_12
    const-string v1, "buddy_group"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1617
    :sswitch_13
    invoke-static {p1}, Lcom/sec/chaton/e/n;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1618
    const-string v2, "buddy_group"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1622
    :sswitch_14
    invoke-static {p1}, Lcom/sec/chaton/e/j;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1623
    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "conatct_number=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1627
    :sswitch_15
    sget-object v1, Lcom/sec/chaton/e/az;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1632
    :sswitch_16
    const-string v1, "grouprelation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1636
    :sswitch_17
    sget-object v1, Lcom/sec/chaton/e/ba;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1640
    :sswitch_18
    sget-object v1, Lcom/sec/chaton/e/ba;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1644
    :sswitch_19
    invoke-static {p1}, Lcom/sec/chaton/e/m;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1645
    const-string v2, "grouprelation"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "group_relation_buddy=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1649
    :sswitch_1a
    sget-object v1, Lcom/sec/chaton/e/ba;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1653
    :sswitch_1b
    invoke-static {p1}, Lcom/sec/chaton/e/m;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1654
    const-string v2, "grouprelation"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "group_relation_group=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1658
    :sswitch_1c
    sget-object v1, Lcom/sec/chaton/e/ax;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1662
    :sswitch_1d
    const-string v1, "inbox"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1666
    :sswitch_1e
    invoke-static {p1}, Lcom/sec/chaton/e/q;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1667
    const-string v2, "inbox"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1671
    :sswitch_1f
    invoke-static {p1}, Lcom/sec/chaton/e/q;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1672
    const-string v2, "inbox"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "inbox_no=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1676
    :sswitch_20
    sget-object v1, Lcom/sec/chaton/e/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1680
    :sswitch_21
    const-string v1, "SELECT a.*, r.buddy_no AS relation_buddy_no\tFROM (  SELECT j.buddy_no, j.buddy_name, j.buddy_show_phone_number, j.buddy_extra_info, j.buddy_msisdns, j.buddy_status_message, j.weburl, j.is_buddy, i.* FROM inbox i LEFT OUTER JOIN ( SELECT p.participants_buddy_no AS buddy_no, ifnull(b.buddy_name, p.participants_buddy_name) AS buddy_name,p.participants_inbox_no, b.buddy_show_phone_number, b.buddy_extra_info, b.buddy_msisdns, b.buddy_status_message, b.weburl, b.is_buddy FROM participant p LEFT OUTER JOIN ( SELECT buddy_no,buddy_name, buddy_show_phone_number, buddy_extra_info, buddy_msisdns, buddy_status_message, \'\' AS weburl, \'Y\' AS is_buddy FROM buddy UNION ALL SELECT buddy_no AS buddy_no,buddy_name AS buddy_name,\'\' AS buddy_show_phone_number, \'\' AS buddy_extra_info, \'\' AS buddy_msisdns, msgstatus AS buddy_status_message,weburl, \'Y\' AS is_buddy FROM specialbuddy ) b  ON p.participants_buddy_no = b.buddy_no WHERE p.participants_inbox_no NOT null ) j  ON i.inbox_no = j.participants_inbox_no WHERE i.inbox_last_chat_type != 12 GROUP BY i.inbox_no ) a  LEFT OUTER JOIN inbox_buddy_relation r  ON a.inbox_no = r.inbox_no ORDER BY inbox_last_time DESC, a._id"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1684
    :sswitch_22
    sget-object v1, Lcom/sec/chaton/e/bb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1688
    :sswitch_23
    sget-object v1, Lcom/sec/chaton/e/bb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1692
    :sswitch_24
    sget-object v1, Lcom/sec/chaton/e/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1696
    :sswitch_25
    sget-object v1, Lcom/sec/chaton/e/bb;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1701
    :sswitch_26
    const-string v1, "inbox_buddy_relation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1706
    :sswitch_27
    const-string v1, "inbox_session_id_mapping"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1712
    :sswitch_28
    invoke-static {p1}, Lcom/sec/chaton/e/v;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1713
    const-string v2, "message"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1717
    :sswitch_29
    invoke-static {p1}, Lcom/sec/chaton/e/v;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1718
    const-string v2, "message"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1722
    :sswitch_2a
    const-string v1, "message"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1726
    :sswitch_2b
    sget-object v1, Lcom/sec/chaton/e/bh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1730
    :sswitch_2c
    const-string v1, "message"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1734
    :sswitch_2d
    const-string v1, "relation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1738
    :sswitch_2e
    sget-object v1, Lcom/sec/chaton/e/bf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1742
    :sswitch_2f
    invoke-static {p1}, Lcom/sec/chaton/e/z;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1743
    sget-object v2, Lcom/sec/chaton/e/bh;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "participants_inbox_no=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1748
    :sswitch_30
    sget-object v1, Lcom/sec/chaton/e/bh;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1756
    :sswitch_31
    sget-object v1, Lcom/sec/chaton/e/bh;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1762
    :sswitch_32
    const-string v1, "memo"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1766
    :sswitch_33
    const-string v1, "memo_sessions"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1773
    :sswitch_34
    const-string v1, "buddy_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1777
    :sswitch_35
    const-string v1, "poston_comments"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1781
    :sswitch_36
    const-string v1, "poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1785
    :sswitch_37
    const-string v1, "my_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1789
    :sswitch_38
    const-string v1, "my_multimedia_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1794
    :sswitch_39
    const-string v1, "buddy_multimedia_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1801
    :sswitch_3a
    sget-object v1, Lcom/sec/chaton/e/bd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1807
    :sswitch_3b
    sget-object v1, Lcom/sec/chaton/e/bd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1812
    :sswitch_3c
    sget-object v1, Lcom/sec/chaton/e/bd;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1818
    :sswitch_3d
    sget-object v1, Lcom/sec/chaton/e/bd;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1823
    :sswitch_3e
    sget-object v1, Lcom/sec/chaton/e/bd;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1828
    :sswitch_3f
    sget-object v1, Lcom/sec/chaton/e/bd;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1837
    :sswitch_40
    const-string v1, "cover_story_sample"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1841
    :sswitch_41
    const-string v1, "specialbuddy"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1844
    :sswitch_42
    sget-object v1, Lcom/sec/chaton/e/ax;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1848
    :sswitch_43
    const-string v1, "recommendee"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1853
    :sswitch_44
    sget-object v1, Lcom/sec/chaton/e/ax;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1858
    :sswitch_45
    sget-object v1, Lcom/sec/chaton/e/ax;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1863
    :sswitch_46
    const-string v1, "message_notification"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1868
    :sswitch_47
    const-string v1, "participant_mapping"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1535
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x66 -> :sswitch_1
        0x67 -> :sswitch_6
        0x68 -> :sswitch_7
        0x69 -> :sswitch_9
        0x6a -> :sswitch_5
        0x6b -> :sswitch_2
        0x6c -> :sswitch_4
        0x6d -> :sswitch_8
        0x6e -> :sswitch_a
        0x6f -> :sswitch_c
        0x70 -> :sswitch_b
        0x73 -> :sswitch_e
        0x74 -> :sswitch_f
        0x75 -> :sswitch_d
        0x76 -> :sswitch_3
        0xc8 -> :sswitch_12
        0xc9 -> :sswitch_13
        0xcb -> :sswitch_15
        0x12c -> :sswitch_10
        0x12d -> :sswitch_11
        0x12e -> :sswitch_14
        0x190 -> :sswitch_1d
        0x191 -> :sswitch_1e
        0x192 -> :sswitch_1f
        0x193 -> :sswitch_20
        0x194 -> :sswitch_25
        0x196 -> :sswitch_24
        0x197 -> :sswitch_21
        0x198 -> :sswitch_22
        0x199 -> :sswitch_23
        0x1cd -> :sswitch_26
        0x1d7 -> :sswitch_27
        0x1f4 -> :sswitch_2a
        0x1f5 -> :sswitch_28
        0x1f6 -> :sswitch_29
        0x1f9 -> :sswitch_2b
        0x1ff -> :sswitch_2c
        0x258 -> :sswitch_2d
        0x25b -> :sswitch_2e
        0x2bc -> :sswitch_16
        0x2bd -> :sswitch_17
        0x2be -> :sswitch_18
        0x2c7 -> :sswitch_1b
        0x2c8 -> :sswitch_19
        0x2c9 -> :sswitch_1a
        0x2ca -> :sswitch_1c
        0x320 -> :sswitch_31
        0x321 -> :sswitch_2f
        0x322 -> :sswitch_30
        0x384 -> :sswitch_32
        0x385 -> :sswitch_33
        0x386 -> :sswitch_36
        0x387 -> :sswitch_35
        0x388 -> :sswitch_3a
        0x389 -> :sswitch_3f
        0x38a -> :sswitch_37
        0x38b -> :sswitch_34
        0x38c -> :sswitch_3b
        0x38d -> :sswitch_3c
        0x38e -> :sswitch_3d
        0x38f -> :sswitch_3e
        0x390 -> :sswitch_38
        0x391 -> :sswitch_39
        0x392 -> :sswitch_40
        0x3e8 -> :sswitch_43
        0x3e9 -> :sswitch_44
        0x514 -> :sswitch_45
        0x578 -> :sswitch_45
        0x7d0 -> :sswitch_41
        0x7d1 -> :sswitch_42
        0xbb8 -> :sswitch_46
        0xfa0 -> :sswitch_47
    .end sparse-switch
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2875
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2878
    const/16 v9, -0xa

    .line 2880
    :try_start_0
    const-string v1, "inbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "inbox_unread_count"

    aput-object v3, v2, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2881
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 2882
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2883
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2884
    if-lez v0, :cond_2

    .line 2885
    const-string v0, "inbox"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2888
    :goto_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2890
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2891
    if-eqz v1, :cond_0

    .line 2892
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2896
    :cond_0
    return v0

    .line 2890
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2891
    if-eqz v1, :cond_1

    .line 2892
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2890
    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v9

    goto :goto_0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 5

    .prologue
    .line 2117
    const-wide/16 v0, 0x0

    .line 2120
    :try_start_0
    const-string v2, "buddy"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2127
    :cond_0
    :goto_0
    return-wide v0

    .line 2121
    :catch_0
    move-exception v2

    .line 2123
    const-string v2, "buddy_no"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124
    const-string v0, "buddy"

    const-string v1, "buddy_no=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private static b()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    .line 1390
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 1391
    const-string v1, "com.sec.chaton.provider"

    .line 1393
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/buddy_no/*"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1394
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/buddy_count"

    const/16 v3, 0x6f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1395
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/hide_buddy_count"

    const/16 v3, 0x75

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1396
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/participants_no_same"

    const/16 v3, 0x6c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1397
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/participants"

    const/16 v3, 0x6b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1398
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/unknown_calllogs"

    const/16 v3, 0x76

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1399
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/profile"

    const/16 v3, 0x6a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1400
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/group"

    const/16 v3, 0x67

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1401
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/alphabetical"

    const/16 v3, 0x68

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1402
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/alphabetical2"

    const/16 v3, 0x6d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1403
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/relation"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1404
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/alphabetical_special"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1405
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/get_hide_buddy"

    const/16 v3, 0x70

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1406
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/buddygroup_chatonv"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1407
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy/buddy_included_group"

    const/16 v3, 0x74

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1408
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1410
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_group/group_info"

    const/16 v3, 0xcb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1411
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_group/group_name/*"

    const/16 v3, 0xca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1412
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_group/group_refresh"

    const/16 v3, 0x2ca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1413
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_group/*"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1414
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_group"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1416
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "contacts/conatct_number/*"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1417
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "contacts/*"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1418
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "contacts"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1420
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation/group_relation_group/*"

    const/16 v3, 0x2c7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1421
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation/group_relation_buddy/*"

    const/16 v3, 0x2c8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1422
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation/group"

    const/16 v3, 0x2c9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1423
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation/buddy_in"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1424
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation/buddy_not_in"

    const/16 v3, 0x2be

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1425
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "grouprelation"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1427
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/chat_list_sync"

    const/16 v3, 0x1004

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1428
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/delete_old_chat_rooms"

    const/16 v3, 0x1005

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1430
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/update_unread_count"

    const/16 v3, 0x1068

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1431
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/update_inbox_with_current_unread_count"

    const/16 v3, 0x19c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1433
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/update_for_push"

    const/16 v3, 0x19a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1434
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/get_inbox_no"

    const/16 v3, 0x196

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1435
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/inbox_title_for_search"

    const/16 v3, 0x19b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1437
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/inbox_invalid_update"

    const/16 v3, 0x195

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1439
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/group_title_update"

    const/16 v3, 0x194

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1440
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/buddy_participant_join"

    const/16 v3, 0x193

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1441
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/buddy_participant_join_for_list"

    const/16 v3, 0x197

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1442
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/buddy_participant_join_for_list_of_forward"

    const/16 v3, 0x198

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1443
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/buddy_participant_join_for_list_of_poll"

    const/16 v3, 0x199

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1444
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/inbox_no"

    const/16 v3, 0x192

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1445
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox/*"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1446
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1449
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox_buddy_relation"

    const/16 v3, 0x1cd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1452
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/read_ack"

    const/16 v3, 0x1f8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1453
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/message_sever_id/*"

    const/16 v3, 0x1f6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1454
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/message_buddy"

    const/16 v3, 0x1f9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1455
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/message_buddy/*"

    const/16 v3, 0x1fd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1456
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/message_buddy_noti/*"

    const/16 v3, 0x201

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1457
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/media"

    const/16 v3, 0x202

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1458
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/get_pendings"

    const/16 v3, 0x203

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1460
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/update"

    const/16 v3, 0x200

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1461
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/delete"

    const/16 v3, 0x1ff

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1462
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/count/*"

    const/16 v3, 0x1fe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1464
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/send"

    const/16 v3, 0x1fa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1465
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/received"

    const/16 v3, 0x1fb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1467
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/send/inbox"

    const/16 v3, 0x1fc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1468
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/message_for_search"

    const/16 v3, 0x204

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1469
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message/*"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1470
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1472
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "relation/insert"

    const/16 v3, 0x25c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1473
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "relation/day"

    const/16 v3, 0x25b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1474
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "relation"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1476
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "participant/name_join_buddy"

    const/16 v3, 0x322

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1477
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "participant/*"

    const/16 v3, 0x321

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1478
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "participant"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1481
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "memo"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1482
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "memo_sessions"

    const/16 v3, 0x385

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1486
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "poston"

    const/16 v3, 0x386

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1487
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "my_poston"

    const/16 v3, 0x38a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1488
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_poston"

    const/16 v3, 0x38b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1489
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "poston_comments"

    const/16 v3, 0x387

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1490
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "my_multimedia_poston"

    const/16 v3, 0x390

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1491
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_multimedia_poston"

    const/16 v3, 0x391

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1495
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "cover_story_sample"

    const/16 v3, 0x392

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1498
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "poston/joined_buddy"

    const/16 v3, 0x388

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1499
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "poston_comments/joined_buddy"

    const/16 v3, 0x389

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1500
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "my_poston/joined_buddy"

    const/16 v3, 0x38c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1501
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_poston/joined_buddy"

    const/16 v3, 0x38d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1502
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "my_multimedia_poston/joined_my_multimedia"

    const/16 v3, 0x38e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1503
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddy_multimedia_poston/joined_buddy_multimedia"

    const/16 v3, 0x38f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1506
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "recommendee"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1507
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "recommendee/recommendee_count"

    const/16 v3, 0x3e9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1509
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "specialbuddy"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1510
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "specialbuddy/specialbuddy_count"

    const/16 v3, 0x7d1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1512
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "me"

    const/16 v3, 0x44c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1514
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "myextrainfo"

    const/16 v3, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1515
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddyextrainfo/#"

    const/16 v3, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1516
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "buddyextrainfoall"

    const/16 v3, 0x578

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1520
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message_notification"

    const/16 v3, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1521
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "message_notification/parse_message_noti"

    const/16 v3, 0xbb9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1525
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "participant_mapping"

    const/16 v3, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1528
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "inbox_session_id_mapping"

    const/16 v3, 0x1d7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1529
    const-string v1, "com.sec.chaton.provider"

    const-string v2, "setting_info"

    const/16 v3, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1530
    return-object v0
.end method

.method private b(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1883
    new-instance v0, Lcom/sec/chaton/e/bk;

    invoke-direct {v0}, Lcom/sec/chaton/e/bk;-><init>()V

    .line 1884
    sparse-switch p2, :sswitch_data_0

    .line 2081
    :goto_0
    return-object v0

    .line 1887
    :sswitch_0
    const-string v1, "buddy"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1891
    :sswitch_1
    invoke-static {p1}, Lcom/sec/chaton/e/i;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1892
    const-string v2, "buddy"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "buddy_no=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1896
    :sswitch_2
    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1900
    :sswitch_3
    invoke-static {p1}, Lcom/sec/chaton/e/j;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1901
    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1905
    :sswitch_4
    invoke-static {p1}, Lcom/sec/chaton/e/j;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1906
    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "conatct_number=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1910
    :sswitch_5
    const-string v1, "buddy_group"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1914
    :sswitch_6
    invoke-static {p1}, Lcom/sec/chaton/e/n;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1915
    const-string v2, "buddy_group"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1919
    :sswitch_7
    const-string v1, "grouprelation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto :goto_0

    .line 1923
    :sswitch_8
    invoke-static {p1}, Lcom/sec/chaton/e/m;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1924
    const-string v2, "grouprelation"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "group_relation_buddy=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1928
    :sswitch_9
    invoke-static {p1}, Lcom/sec/chaton/e/m;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1929
    const-string v2, "grouprelation"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "group_relation_group=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1933
    :sswitch_a
    const-string v1, "inbox"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1937
    :sswitch_b
    invoke-static {p1}, Lcom/sec/chaton/e/q;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1938
    const-string v2, "inbox"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1942
    :sswitch_c
    invoke-static {p1}, Lcom/sec/chaton/e/q;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1943
    const-string v2, "inbox"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    const-string v2, "inbox_no=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1948
    :sswitch_d
    const-string v1, "inbox_buddy_relation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1953
    :sswitch_e
    const-string v1, "inbox_session_id_mapping"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1959
    :sswitch_f
    const-string v1, "message"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1963
    :sswitch_10
    invoke-static {p1}, Lcom/sec/chaton/e/v;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1964
    const-string v2, "message"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1968
    :sswitch_11
    invoke-static {p1}, Lcom/sec/chaton/e/v;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1969
    const-string v2, "message"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1973
    :sswitch_12
    const-string v1, "message"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1977
    :sswitch_13
    sget-object v1, Lcom/sec/chaton/e/bj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1981
    :sswitch_14
    const-string v1, "message"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1985
    :sswitch_15
    const-string v1, "relation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1989
    :sswitch_16
    invoke-static {p1}, Lcom/sec/chaton/e/z;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1990
    const-string v2, "participant"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1995
    :sswitch_17
    sget-object v1, Lcom/sec/chaton/e/bh;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2000
    :sswitch_18
    const-string v1, "participant"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2005
    :sswitch_19
    const-string v1, "memo"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2010
    :sswitch_1a
    const-string v1, "memo_sessions"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2017
    :sswitch_1b
    const-string v1, "poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2022
    :sswitch_1c
    const-string v1, "my_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2027
    :sswitch_1d
    const-string v1, "buddy_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2032
    :sswitch_1e
    const-string v1, "poston_comments"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2037
    :sswitch_1f
    const-string v1, "my_multimedia_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2042
    :sswitch_20
    const-string v1, "buddy_multimedia_poston"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2047
    :sswitch_21
    const-string v1, "cover_story_sample"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2056
    :sswitch_22
    const-string v1, "recommendee"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2061
    :sswitch_23
    const-string v1, "specialbuddy"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2067
    :sswitch_24
    const-string v1, "message_notification"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2071
    :sswitch_25
    const-string v1, "SELECT noti_buddy_no,noti_inbox_no,noti_sent_time,noti_message,noti_chat_type,noti_msg_type,buddy_name FROM message_notification n LEFT OUTER JOIN (SELECT p.participants_buddy_no AS buddy_no,ifnull (b.buddy_name, p.participants_buddy_name)  AS buddy_name, p.participants_inbox_no FROM participant p LEFT OUTER JOIN (SELECT buddy_no,buddy_name FROM buddy UNION ALL SELECT buddy_no AS buddy_no,buddy_name AS buddy_name FROM specialbuddy) b ON p.participants_buddy_no=b.buddy_no) j ON n.noti_inbox_no=j.participants_inbox_no AND n.noti_buddy_no=j.buddy_no ORDER BY n.noti_sent_time DESC;"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 2076
    :sswitch_26
    const-string v1, "participant_mapping"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v0

    goto/16 :goto_0

    .line 1884
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x66 -> :sswitch_1
        0xc8 -> :sswitch_5
        0xc9 -> :sswitch_6
        0x12c -> :sswitch_2
        0x12d -> :sswitch_3
        0x12e -> :sswitch_4
        0x190 -> :sswitch_a
        0x191 -> :sswitch_b
        0x192 -> :sswitch_c
        0x1cd -> :sswitch_d
        0x1d7 -> :sswitch_e
        0x1f4 -> :sswitch_f
        0x1f5 -> :sswitch_10
        0x1f6 -> :sswitch_11
        0x1f8 -> :sswitch_13
        0x1ff -> :sswitch_12
        0x200 -> :sswitch_14
        0x258 -> :sswitch_15
        0x2bc -> :sswitch_7
        0x2c7 -> :sswitch_9
        0x2c8 -> :sswitch_8
        0x320 -> :sswitch_18
        0x321 -> :sswitch_16
        0x322 -> :sswitch_17
        0x384 -> :sswitch_19
        0x385 -> :sswitch_1a
        0x386 -> :sswitch_1b
        0x387 -> :sswitch_1e
        0x38a -> :sswitch_1c
        0x38b -> :sswitch_1d
        0x390 -> :sswitch_1f
        0x391 -> :sswitch_20
        0x392 -> :sswitch_21
        0x3e8 -> :sswitch_22
        0x7d0 -> :sswitch_23
        0xbb8 -> :sswitch_24
        0xbb9 -> :sswitch_25
        0xfa0 -> :sswitch_26
    .end sparse-switch
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2555
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v2

    .line 2559
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2561
    invoke-direct {p0, p1, v2, p2, p4}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)J

    .line 2563
    invoke-direct {p0, p1, v2, p2}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    .line 2569
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2570
    const-string v0, "0999"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_4

    .line 2571
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/chaton/e/a/af;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2574
    :goto_0
    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    .line 2575
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "spbd weburl, insertRelationInboxBuddyAndInbox(), "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2581
    const-string v4, "inbox_no"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2582
    const-string v2, "inbox_chat_type"

    invoke-virtual {p4}, Lcom/sec/chaton/e/r;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2583
    const-string v2, "inbox_last_chat_type"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2584
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2585
    const-string v2, "inbox_session_id"

    invoke-virtual {v3, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2588
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2589
    const-string v2, "inbox_web_url"

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597
    :cond_3
    const-string v0, "inbox"

    invoke-virtual {p1, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2598
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2900
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2903
    const/16 v9, -0xa

    .line 2905
    :try_start_0
    const-string v1, "inbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "inbox_unread_count"

    aput-object v3, v2, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2906
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 2907
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2908
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2909
    const-string v2, "inbox_unread_count"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2910
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_0

    .line 2911
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateChatRoomWithCurrentUnreadCount - current unread count :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", added unread count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatONProvider"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2913
    :cond_0
    const-string v3, "inbox_unread_count"

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2915
    const-string v0, "inbox"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2917
    :goto_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2919
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2920
    if-eqz v1, :cond_1

    .line 2921
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2925
    :cond_1
    return v0

    .line 2919
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2920
    if-eqz v1, :cond_2

    .line 2921
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2919
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v9

    goto :goto_0
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 5

    .prologue
    .line 2139
    const-wide/16 v0, 0x0

    .line 2142
    :try_start_0
    const-string v2, "specialbuddy"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2149
    :cond_0
    :goto_0
    return-wide v0

    .line 2143
    :catch_0
    move-exception v2

    .line 2145
    const-string v2, "buddy_no"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2146
    const-string v0, "specialbuddy"

    const-string v1, "buddy_no=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 5

    .prologue
    .line 2161
    const-wide/16 v0, 0x0

    .line 2164
    :try_start_0
    const-string v2, "recommendee"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2171
    :cond_0
    :goto_0
    return-wide v0

    .line 2165
    :catch_0
    move-exception v2

    .line 2167
    const-string v2, "buddy_no"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2168
    const-string v0, "recommendee"

    const-string v1, "buddy_no=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177
    const-string v0, "memoSessionInsertOrUpdatert"

    const-string v1, "ChatONProvider"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    const-wide/16 v0, 0x0

    .line 2179
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 2181
    :try_start_0
    const-string v3, "memo_sessions"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2187
    :cond_0
    :goto_0
    return-wide v0

    .line 2182
    :catch_0
    move-exception v3

    .line 2183
    const-string v3, "buddy_no"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2184
    const-string v0, "memo_sessions"

    const-string v1, "buddy_no=?"

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private f(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2194
    .line 2195
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2198
    :try_start_0
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->a()I

    move-result v0

    const-string v1, "inbox_chat_type"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 2199
    const-string v0, "buddy_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2200
    const-string v1, "inbox_buddy_relation"

    const/4 v2, 0x0

    const-string v3, "buddy_no = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v9, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2201
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2203
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2204
    const-string v0, "inbox_no"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2205
    const-string v2, "inbox_no"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2207
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2208
    const-string v3, "inbox_valid"

    const-string v4, "N"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209
    const-string v3, "inbox_title_fixed"

    const-string v4, "Y"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2210
    const-string v3, "inbox"

    const-string v4, "inbox_no = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {p1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2213
    const-string v0, "inbox_buddy_relation"

    const-string v2, "buddy_no = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2217
    :cond_0
    const-string v0, "inbox_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "inbox_chat_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    invoke-direct {p0, p1, v0, v9, v2}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)J

    .line 2220
    const-string v0, "buddy_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2223
    :goto_0
    const-string v0, "inbox"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2224
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2226
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2227
    if-eqz v1, :cond_1

    .line 2228
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2232
    :cond_1
    return-wide v2

    .line 2226
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2227
    if-eqz v1, :cond_2

    .line 2228
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2226
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move-object v1, v8

    goto :goto_0
.end method

.method private g(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2236
    .line 2238
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2241
    :try_start_0
    const-string v0, "buddy_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2242
    const-string v1, "inbox_buddy_relation"

    const/4 v2, 0x0

    const-string v3, "buddy_no = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 2243
    if-eqz v9, :cond_3

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 2244
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2245
    const-string v0, "inbox_no"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2246
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2247
    const-string v1, "inbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "inbox_valid"

    aput-object v3, v2, v0

    const-string v3, "inbox_no = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2248
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 2249
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2251
    const-string v1, "N"

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 2323
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2324
    if-eqz v9, :cond_0

    .line 2325
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v8

    .line 2329
    :cond_1
    :goto_0
    return v0

    .line 2257
    :cond_2
    :try_start_2
    const-string v0, "inbox_buddy_relation"

    const-string v1, "buddy_no = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2260
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2261
    const-string v1, "inbox_valid"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    const-string v1, "inbox_title_fixed"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    const-string v1, "inbox"

    const-string v2, "inbox_no = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v11, v3, v4

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 2266
    const-string v0, "message_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2267
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2268
    const-string v3, "_id"

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2269
    const-string v3, "message_inbox_no"

    invoke-virtual {v2, v3, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2270
    const-string v3, "message_type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2271
    const-string v3, "message_content_type"

    sget-object v4, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2273
    const-string v3, "KEY_DEACTIVATED"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2275
    const-string v3, "message_content"

    const-string v4, "%d,%s,%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    invoke-virtual {v7}, Lcom/sec/chaton/e/aj;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    const/4 v6, 0x2

    const-string v7, "deactivated"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2285
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2286
    const-string v4, "message_read_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2287
    const-string v4, "message_sender"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "chaton_id"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289
    const-string v4, "message"

    const-string v5, "message_inbox_no = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    invoke-virtual {p1, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2296
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2297
    const-string v4, "message_type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2299
    const-string v4, "message"

    const-string v5, "message_inbox_no = ? AND message_type = 2"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    invoke-virtual {p1, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 2316
    :goto_1
    const-string v3, "message_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2317
    const-string v3, "message_time_text"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318
    const-string v0, "message"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_3
    move v0, v8

    .line 2321
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2323
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2324
    if-eqz v9, :cond_1

    .line 2325
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2306
    :cond_4
    :try_start_3
    const-string v3, "message_content"

    const-string v4, "%d,%s,%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    invoke-virtual {v7}, Lcom/sec/chaton/e/aj;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    const/4 v6, 0x2

    const-string v7, "buddy_name"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2323
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2324
    if-eqz v1, :cond_5

    .line 2325
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2323
    :cond_5
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v9

    goto :goto_2
.end method

.method private h(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 13

    .prologue
    .line 2335
    .line 2336
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2338
    :try_start_0
    const-string v0, "message"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 2341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2345
    const-string v2, "message_inbox_no"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2346
    const-string v3, "message_content_type"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2347
    const-string v4, "message_sender"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2348
    const-string v5, "message_read_status"

    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2349
    const-string v6, "message_time"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2350
    const-string v8, "message_content"

    invoke-virtual {p2, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2354
    const-string v9, "%d;%d;%s;%s"

    .line 2355
    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v11

    const/4 v3, 0x2

    invoke-static {v8}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v3

    const/4 v3, 0x3

    const-string v8, ""

    aput-object v8, v10, v3

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2358
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2359
    const-string v9, "inbox_last_msg_sender"

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2360
    const-string v4, "inbox_last_message"

    invoke-virtual {v8, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    const-string v3, "inbox_last_time"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2362
    const-string v3, "inbox_participants"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2363
    const-string v3, "inbox"

    const-string v4, "inbox_no=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {p1, v3, v8, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2365
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2367
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2370
    return-wide v0

    .line 2367
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private i(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)Lcom/sec/chaton/provider/c;
    .locals 13

    .prologue
    .line 2388
    .line 2389
    sget-object v8, Lcom/sec/chaton/provider/c;->c:Lcom/sec/chaton/provider/c;

    .line 2390
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2391
    const/4 v9, 0x0

    .line 2393
    :try_start_0
    const-string v0, "message_sever_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2394
    const-string v0, "message_inbox_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2395
    const-string v0, "message_sender"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2396
    const-string v1, "message"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "message_need_update"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "message_time"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "message_type"

    aput-object v3, v2, v0

    const-string v3, "message_sever_id=? AND message_inbox_no=? AND message_sender=?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v0, 0x1

    aput-object v11, v4, v0

    const/4 v0, 0x2

    aput-object v12, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2398
    if-eqz v1, :cond_8

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 2399
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2400
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2401
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2405
    const-string v4, "Y"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "Y"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "message_time"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_9

    .line 2407
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2410
    const-string v5, "Y"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "message_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v2, v5

    if-gez v0, :cond_3

    .line 2411
    const-string v0, "message_time_text"

    const-string v2, "message_time_text"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412
    const-string v0, "message_time"

    const-string v2, "message_time"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2444
    :cond_1
    :goto_0
    const-string v0, "message"

    const-string v2, "message_sever_id=? AND message_inbox_no=? AND message_sender=?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v3, v5

    const/4 v5, 0x1

    aput-object v11, v3, v5

    const/4 v5, 0x2

    aput-object v12, v3, v5

    invoke-virtual {p1, v0, v4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v2, v0

    .line 2445
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_9

    .line 2446
    sget-object v0, Lcom/sec/chaton/provider/c;->e:Lcom/sec/chaton/provider/c;

    .line 2484
    :goto_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2486
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2487
    if-eqz v1, :cond_2

    .line 2488
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2492
    :cond_2
    return-object v0

    .line 2414
    :cond_3
    :try_start_2
    const-string v0, "message_need_update"

    const-string v2, "message_need_update"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2415
    const-string v0, "message_content"

    const-string v2, "message_content"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416
    const-string v0, "message_content_type"

    const-string v2, "message_content_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2417
    const-string v0, "message_time"

    const-string v2, "message_time"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2418
    const-string v0, "message_tid"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2419
    const-string v0, "message_tid"

    const-string v2, "message_tid"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2421
    :cond_4
    const-string v0, "message_time_text"

    const-string v2, "message_time_text"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2423
    const-string v0, "message_formatted"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2424
    const-string v0, "message_formatted"

    const-string v2, "message_formatted"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2429
    :cond_5
    const-string v0, "message_is_truncated"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2430
    const-string v0, "message_is_truncated"

    const-string v2, "message_is_truncated"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2435
    :cond_6
    const-string v0, "message_type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2436
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2437
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    .line 2438
    const-string v0, "message_type"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2486
    :catchall_0
    move-exception v0

    :goto_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2487
    if-eqz v1, :cond_7

    .line 2488
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2486
    :cond_7
    throw v0

    .line 2476
    :cond_8
    :try_start_3
    const-string v0, "message"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2479
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_9

    .line 2480
    sget-object v0, Lcom/sec/chaton/provider/c;->d:Lcom/sec/chaton/provider/c;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 2486
    :catchall_1
    move-exception v0

    move-object v1, v9

    goto :goto_2

    :cond_9
    move-object v0, v8

    goto/16 :goto_1
.end method

.method private j(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2698
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2704
    if-eqz p2, :cond_8

    :try_start_0
    const-string v3, "message_sever_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2705
    const-string v3, "message_sever_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 2707
    :goto_0
    if-eqz p2, :cond_7

    const-string v3, "count"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2708
    const-string v3, "count"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v4, v3

    .line 2710
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-gtz v4, :cond_3

    .line 2711
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2735
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2736
    if-eqz v2, :cond_1

    .line 2737
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v1

    .line 2741
    :cond_2
    :goto_2
    return v0

    .line 2715
    :cond_3
    :try_start_1
    const-string v1, "SELECT inbox_no, inbox_chat_type FROM inbox WHERE inbox_no = (SELECT message_inbox_no FROM message WHERE message_sever_id = ?)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v3, v6

    invoke-virtual {p1, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 2716
    :try_start_2
    const-string v3, ""

    .line 2717
    sget-object v2, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 2718
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 2720
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_4

    .line 2721
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2722
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2723
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    .line 2726
    :cond_4
    sget-object v6, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v6, :cond_5

    .line 2727
    sget-object v2, Lcom/sec/chaton/e/bj;->b:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v4, v6

    const/4 v3, 0x1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "chaton_id"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v3

    const/4 v3, 0x2

    aput-object v5, v4, v3

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2733
    :goto_3
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2735
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2736
    if-eqz v1, :cond_2

    .line 2737
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2729
    :cond_5
    :try_start_3
    sget-object v2, Lcom/sec/chaton/e/bj;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v4, v3, v6

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const/4 v4, 0x2

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 2735
    :catchall_0
    move-exception v0

    :goto_4
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2736
    if-eqz v1, :cond_6

    .line 2737
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2735
    :cond_6
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :cond_7
    move v4, v1

    goto/16 :goto_1

    :cond_8
    move-object v5, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 1269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB delete : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1276
    sget-object v2, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 1277
    const/16 v3, 0x1005

    if-ne v2, v3, :cond_2

    .line 1278
    invoke-direct {p0, v1}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1308
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 1309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnInteger : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    .line 1312
    :cond_1
    :goto_0
    return v0

    .line 1280
    :cond_2
    :try_start_1
    invoke-direct {p0, p1, v2}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    .line 1281
    invoke-virtual {v2, v1}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 1283
    if-lez v1, :cond_3

    .line 1284
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DB delete Success : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1286
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1290
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DB delete end : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1308
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v1

    .line 1291
    goto/16 :goto_0

    .line 1292
    :catch_0
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 1293
    :goto_1
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1294
    const/4 v0, -0x3

    .line 1308
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    :goto_2
    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1295
    :catch_1
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 1296
    :goto_3
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1297
    const/4 v0, -0x4

    .line 1308
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto :goto_2

    .line 1298
    :catch_2
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 1299
    :goto_4
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1300
    const/4 v0, -0x5

    .line 1308
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto :goto_2

    .line 1301
    :catch_3
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 1302
    :goto_5
    :try_start_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1303
    const/4 v0, -0x2

    .line 1308
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_2

    .line 1304
    :catch_4
    move-exception v1

    move v2, v0

    .line 1305
    :goto_6
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delete ::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ChatONProvider"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1308
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_2

    .line 1308
    :catchall_0
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    :goto_7
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 1309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnInteger : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    :cond_5
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_7

    :catchall_2
    move-exception v0

    move v1, v2

    goto :goto_7

    .line 1304
    :catch_5
    move-exception v2

    move-object v5, v2

    move v2, v1

    move-object v1, v5

    goto :goto_6

    .line 1301
    :catch_6
    move-exception v0

    goto/16 :goto_5

    .line 1298
    :catch_7
    move-exception v0

    goto/16 :goto_4

    .line 1295
    :catch_8
    move-exception v0

    goto/16 :goto_3

    .line 1292
    :catch_9
    move-exception v0

    goto/16 :goto_1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 298
    sget-object v0, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 299
    sparse-switch v0, :sswitch_data_0

    .line 453
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UnKonwn Uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :sswitch_0
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.buddy"

    .line 449
    :goto_0
    return-object v0

    .line 303
    :sswitch_1
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 305
    :sswitch_2
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 307
    :sswitch_3
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 309
    :sswitch_4
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 311
    :sswitch_5
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.buddy"

    goto :goto_0

    .line 313
    :sswitch_6
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 315
    :sswitch_7
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 317
    :sswitch_8
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 319
    :sswitch_9
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 321
    :sswitch_a
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 323
    :sswitch_b
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 325
    :sswitch_c
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 327
    :sswitch_d
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.specialbuddy"

    goto :goto_0

    .line 329
    :sswitch_e
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.recommendee"

    goto :goto_0

    .line 331
    :sswitch_f
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddy"

    goto :goto_0

    .line 333
    :sswitch_10
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.group"

    goto :goto_0

    .line 335
    :sswitch_11
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.group"

    goto :goto_0

    .line 337
    :sswitch_12
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.group"

    goto :goto_0

    .line 339
    :sswitch_13
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.group"

    goto :goto_0

    .line 341
    :sswitch_14
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.contacts"

    goto :goto_0

    .line 343
    :sswitch_15
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.contacts"

    goto :goto_0

    .line 345
    :sswitch_16
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.contacts"

    goto :goto_0

    .line 347
    :sswitch_17
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.grouprelation"

    goto :goto_0

    .line 349
    :sswitch_18
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.grouprelation"

    goto :goto_0

    .line 351
    :sswitch_19
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.grouprelation"

    goto :goto_0

    .line 353
    :sswitch_1a
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.grouprelation"

    goto :goto_0

    .line 355
    :sswitch_1b
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.grouprelation"

    goto :goto_0

    .line 357
    :sswitch_1c
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.inbox"

    goto :goto_0

    .line 359
    :sswitch_1d
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.inbox"

    goto :goto_0

    .line 361
    :sswitch_1e
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.inbox"

    goto :goto_0

    .line 363
    :sswitch_1f
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.inbox"

    goto :goto_0

    .line 366
    :sswitch_20
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.inboxbuddyrelation"

    goto :goto_0

    .line 369
    :sswitch_21
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 372
    :sswitch_22
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 374
    :sswitch_23
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 376
    :sswitch_24
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 379
    :sswitch_25
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.message"

    goto :goto_0

    .line 381
    :sswitch_26
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.message"

    goto :goto_0

    .line 383
    :sswitch_27
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 385
    :sswitch_28
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 387
    :sswitch_29
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 389
    :sswitch_2a
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.relation"

    goto :goto_0

    .line 391
    :sswitch_2b
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.relation"

    goto/16 :goto_0

    .line 394
    :sswitch_2c
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.memo"

    goto/16 :goto_0

    .line 398
    :sswitch_2d
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.poston"

    goto/16 :goto_0

    .line 401
    :sswitch_2e
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.myposton"

    goto/16 :goto_0

    .line 404
    :sswitch_2f
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddyposton"

    goto/16 :goto_0

    .line 407
    :sswitch_30
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postoncomments"

    goto/16 :goto_0

    .line 410
    :sswitch_31
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postonmymultimedia"

    goto/16 :goto_0

    .line 413
    :sswitch_32
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postonbuddymultimedia"

    goto/16 :goto_0

    .line 418
    :sswitch_33
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.poston"

    goto/16 :goto_0

    .line 421
    :sswitch_34
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.myposton"

    goto/16 :goto_0

    .line 424
    :sswitch_35
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.buddyposton"

    goto/16 :goto_0

    .line 427
    :sswitch_36
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postoncomments"

    goto/16 :goto_0

    .line 430
    :sswitch_37
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postonmymultimedia"

    goto/16 :goto_0

    .line 433
    :sswitch_38
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.postonbuddymultimedia"

    goto/16 :goto_0

    .line 437
    :sswitch_39
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.coverstorysample"

    goto/16 :goto_0

    .line 440
    :sswitch_3a
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.recommendee"

    goto/16 :goto_0

    .line 443
    :sswitch_3b
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.specialbuddy"

    goto/16 :goto_0

    .line 445
    :sswitch_3c
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.buddy"

    goto/16 :goto_0

    .line 449
    :sswitch_3d
    const-string v0, "com.sec.chaton.provider/buddyextrainfo"

    goto/16 :goto_0

    .line 299
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_f
        0x66 -> :sswitch_0
        0x67 -> :sswitch_1
        0x68 -> :sswitch_2
        0x69 -> :sswitch_4
        0x6a -> :sswitch_5
        0x6b -> :sswitch_6
        0x6c -> :sswitch_8
        0x6d -> :sswitch_3
        0x6e -> :sswitch_9
        0x6f -> :sswitch_a
        0x73 -> :sswitch_b
        0x74 -> :sswitch_c
        0x76 -> :sswitch_7
        0xc8 -> :sswitch_10
        0xc9 -> :sswitch_11
        0xcb -> :sswitch_12
        0x12c -> :sswitch_14
        0x12d -> :sswitch_15
        0x12e -> :sswitch_16
        0x190 -> :sswitch_1c
        0x191 -> :sswitch_1d
        0x192 -> :sswitch_1e
        0x194 -> :sswitch_1f
        0x1cd -> :sswitch_20
        0x1f4 -> :sswitch_21
        0x1f5 -> :sswitch_25
        0x1f6 -> :sswitch_26
        0x1f9 -> :sswitch_27
        0x1fa -> :sswitch_22
        0x1fb -> :sswitch_23
        0x1fc -> :sswitch_24
        0x1ff -> :sswitch_28
        0x200 -> :sswitch_29
        0x258 -> :sswitch_2a
        0x25b -> :sswitch_2b
        0x2bc -> :sswitch_17
        0x2bd -> :sswitch_18
        0x2be -> :sswitch_13
        0x2c7 -> :sswitch_1b
        0x2c8 -> :sswitch_19
        0x2c9 -> :sswitch_1a
        0x384 -> :sswitch_2c
        0x386 -> :sswitch_2d
        0x387 -> :sswitch_30
        0x388 -> :sswitch_33
        0x389 -> :sswitch_36
        0x38a -> :sswitch_2e
        0x38b -> :sswitch_2f
        0x38c -> :sswitch_34
        0x38d -> :sswitch_35
        0x38e -> :sswitch_37
        0x38f -> :sswitch_38
        0x390 -> :sswitch_31
        0x391 -> :sswitch_32
        0x392 -> :sswitch_39
        0x3e8 -> :sswitch_3a
        0x3e9 -> :sswitch_e
        0x44c -> :sswitch_3c
        0x514 -> :sswitch_3d
        0x7d0 -> :sswitch_3b
        0x7d1 -> :sswitch_d
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12

    .prologue
    .line 774
    sget-object v1, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 777
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 778
    sget-object v0, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 779
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 780
    const-wide/16 v2, -0x1

    .line 781
    const-wide/16 v4, -0x1

    .line 782
    const-string v0, ""

    .line 784
    sget-boolean v7, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v7, :cond_0

    .line 785
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DB insert start: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    :cond_0
    sget-boolean v7, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v7, :cond_1

    .line 788
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DB status : "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :cond_1
    sparse-switch v6, :sswitch_data_0

    .line 1103
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown Uri : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "ChatONProvider"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    :cond_2
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DB insert end: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->a()Z

    move-result v0

    if-nez v0, :cond_14

    .line 1112
    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-lez v0, :cond_14

    if-eqz v9, :cond_14

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_14

    .line 1113
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1114
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1120
    :catch_0
    move-exception v0

    .line 1121
    :try_start_1
    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1122
    sget-object v0, Lcom/sec/chaton/provider/b;->d:Lcom/sec/chaton/provider/b;

    invoke-static {v0}, Lcom/sec/chaton/provider/a;->a(Lcom/sec/chaton/provider/b;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    :goto_2
    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    :cond_4
    :goto_3
    return-object v0

    .line 793
    :sswitch_0
    :try_start_2
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 794
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 795
    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    .line 796
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 798
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1123
    :catch_1
    move-exception v0

    .line 1124
    :try_start_3
    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1125
    sget-object v0, Lcom/sec/chaton/provider/b;->c:Lcom/sec/chaton/provider/b;

    invoke-static {v0}, Lcom/sec/chaton/provider/a;->a(Lcom/sec/chaton/provider/b;)Landroid/net/Uri;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto :goto_2

    .line 803
    :sswitch_1
    :try_start_4
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 804
    const-string v0, "contacts"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 805
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/j;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 806
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 807
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 808
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1126
    :catch_2
    move-exception v0

    .line 1127
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1128
    sget-object v0, Lcom/sec/chaton/provider/b;->e:Lcom/sec/chaton/provider/b;

    invoke-static {v0}, Lcom/sec/chaton/provider/a;->a(Lcom/sec/chaton/provider/b;)Landroid/net/Uri;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 1136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_2

    .line 813
    :sswitch_2
    :try_start_6
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 814
    const-string v0, "buddy_group"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 815
    sget-object v1, Lcom/sec/chaton/e/n;->a:Landroid/net/Uri;

    .line 816
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 818
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1129
    :catch_3
    move-exception v0

    .line 1130
    :try_start_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    sget-object v0, Lcom/sec/chaton/provider/b;->b:Lcom/sec/chaton/provider/b;

    invoke-static {v0}, Lcom/sec/chaton/provider/a;->a(Lcom/sec/chaton/provider/b;)Landroid/net/Uri;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .line 1136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_2

    .line 823
    :sswitch_3
    :try_start_8
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 824
    const-string v0, "grouprelation"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 825
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/m;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 826
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 828
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 1132
    :catch_4
    move-exception v0

    .line 1133
    :try_start_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert ::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    sget-object v0, Lcom/sec/chaton/provider/b;->a:Lcom/sec/chaton/provider/b;

    invoke-static {v0}, Lcom/sec/chaton/provider/a;->a(Lcom/sec/chaton/provider/b;)Landroid/net/Uri;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    .line 1136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_2

    .line 834
    :sswitch_4
    :try_start_a
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->f(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 836
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/q;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 837
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_a .. :try_end_a} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_a .. :try_end_a} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 1136
    :catchall_0
    move-exception v0

    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 1137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnUri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    :cond_5
    throw v0

    .line 843
    :sswitch_5
    :try_start_b
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 844
    const-string v0, "inbox_buddy_relation"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 845
    sget-object v1, Lcom/sec/chaton/e/o;->a:Landroid/net/Uri;

    .line 846
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 847
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 848
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 855
    :sswitch_6
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 856
    const-string v0, "inbox_session_id_mapping"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 857
    sget-object v1, Lcom/sec/chaton/e/p;->a:Landroid/net/Uri;

    .line 858
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 859
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 860
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 866
    :sswitch_7
    const-string v6, "_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 867
    const-string v2, "_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 870
    :cond_6
    const-string v6, "message_inbox_no"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 871
    const-string v0, "message_inbox_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 873
    :cond_7
    const-string v6, "message_sever_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 874
    const-string v4, "message_sever_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 876
    :cond_8
    sget-boolean v6, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v6, :cond_9

    .line 877
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 878
    const-string v7, "DB insert : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "MsgID : "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "MsgID(ServerNO) : "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "InboxNO : "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 879
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ChatONProvider"

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_9
    const-string v6, "message"

    const/4 v7, 0x0

    invoke-virtual {v8, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-lez v6, :cond_a

    .line 883
    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    .line 884
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 889
    :cond_a
    sget-boolean v6, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v6, :cond_2

    .line 890
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 891
    const-string v7, "DB insert end : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID(ServerNO) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "InboxNO : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "ChatONProvider"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 898
    :sswitch_8
    const-string v6, "message_inbox_no"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 899
    const-string v0, "message_inbox_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 902
    :cond_b
    const-string v6, "message_sever_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 903
    const-string v4, "message_sever_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 905
    :cond_c
    const-string v6, "_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 906
    const-string v2, "_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 909
    :cond_d
    sget-boolean v6, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v6, :cond_e

    .line 910
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 911
    const-string v7, "DB insert : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID(ServerNO) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "InboxNO : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 912
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ChatONProvider"

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    :cond_e
    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    .line 915
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 916
    sget-boolean v6, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v6, :cond_2

    .line 917
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 918
    const-string v7, "DB insert end : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MsgID(ServerNO) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "InboxNO : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "ChatONProvider"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 926
    :sswitch_9
    const-string v6, "message_inbox_no"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 927
    const-string v0, "message_inbox_no"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 929
    :cond_f
    const-string v6, "_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 930
    const-string v2, "_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-wide v6, v2

    .line 932
    :goto_4
    const-string v2, "message_sever_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 933
    const-string v2, "message_sever_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 936
    :goto_5
    sget-boolean v4, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v4, :cond_10

    .line 937
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 938
    const-string v5, "DB insert : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "MsgID : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "MsgID(ServerNO) : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "InboxNO : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ChatONProvider"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    :cond_10
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->i(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)Lcom/sec/chaton/provider/c;

    move-result-object v4

    .line 944
    sget-object v5, Lcom/sec/chaton/provider/c;->d:Lcom/sec/chaton/provider/c;

    if-ne v4, v5, :cond_13

    .line 945
    const-string v4, "_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 946
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/e/v;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 950
    :cond_11
    :goto_6
    sget-object v4, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 951
    sget-boolean v4, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v4, :cond_12

    .line 952
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 953
    const-string v5, "DB insert end : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "MsgID : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "MsgID(ServerNO) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "InboxNO : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 954
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    :cond_12
    const-wide/16 v2, 0x1

    .line 959
    goto/16 :goto_0

    .line 947
    :cond_13
    sget-object v5, Lcom/sec/chaton/provider/c;->e:Lcom/sec/chaton/provider/c;

    if-ne v4, v5, :cond_11

    .line 948
    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    goto :goto_6

    .line 962
    :sswitch_a
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->h(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 963
    sget-object v0, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 964
    sget-object v0, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 968
    :sswitch_b
    const-string v0, "relation"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 969
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/ah;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 970
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 976
    :sswitch_c
    sget-object v0, Lcom/sec/chaton/e/be;->a:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 977
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 978
    const-wide/16 v2, 0x1

    .line 983
    goto/16 :goto_0

    .line 987
    :sswitch_d
    const-string v0, "participant"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 988
    sget-object v1, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    .line 989
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 996
    :sswitch_e
    const-string v0, "memo"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 997
    sget-object v1, Lcom/sec/chaton/e/t;->a:Landroid/net/Uri;

    .line 998
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1004
    :sswitch_f
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->e(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1005
    sget-object v1, Lcom/sec/chaton/e/s;->a:Landroid/net/Uri;

    .line 1006
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1014
    :sswitch_10
    const-string v0, "poston"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1015
    sget-object v1, Lcom/sec/chaton/e/af;->a:Landroid/net/Uri;

    .line 1016
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1022
    :sswitch_11
    const-string v0, "my_poston"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1023
    sget-object v1, Lcom/sec/chaton/e/ae;->a:Landroid/net/Uri;

    .line 1024
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1030
    :sswitch_12
    const-string v0, "buddy_poston"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1031
    sget-object v1, Lcom/sec/chaton/e/ab;->a:Landroid/net/Uri;

    .line 1032
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1038
    :sswitch_13
    const-string v0, "my_multimedia_poston"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1039
    sget-object v1, Lcom/sec/chaton/e/ad;->a:Landroid/net/Uri;

    .line 1040
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1046
    :sswitch_14
    const-string v0, "buddy_multimedia_poston"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1047
    sget-object v1, Lcom/sec/chaton/e/aa;->a:Landroid/net/Uri;

    .line 1048
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1054
    :sswitch_15
    const-string v0, "poston_comments"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1055
    sget-object v1, Lcom/sec/chaton/e/ac;->a:Landroid/net/Uri;

    .line 1056
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1063
    :sswitch_16
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1064
    sget-object v1, Lcom/sec/chaton/e/k;->a:Landroid/net/Uri;

    .line 1065
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1071
    :sswitch_17
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->d(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1072
    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    .line 1073
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1078
    :sswitch_18
    invoke-direct {p0, v8, p2}, Lcom/sec/chaton/provider/ChatONProvider;->c(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1079
    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    .line 1080
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1087
    :sswitch_19
    const-string v0, "message_notification"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1088
    sget-object v1, Lcom/sec/chaton/e/u;->a:Landroid/net/Uri;

    .line 1089
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1095
    :sswitch_1a
    const-string v0, "participant_mapping"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1096
    sget-object v1, Lcom/sec/chaton/e/y;->a:Landroid/net/Uri;

    .line 1097
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1118
    :cond_14
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_b .. :try_end_b} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_b .. :try_end_b} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1136
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_15

    .line 1137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnUri : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    move-object v0, v1

    .line 1119
    goto/16 :goto_3

    :cond_16
    move-wide v2, v4

    goto/16 :goto_5

    :cond_17
    move-wide v6, v2

    goto/16 :goto_4

    .line 790
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_1
        0x190 -> :sswitch_4
        0x1cd -> :sswitch_5
        0x1d7 -> :sswitch_6
        0x1f4 -> :sswitch_7
        0x1fa -> :sswitch_8
        0x1fb -> :sswitch_9
        0x1fc -> :sswitch_a
        0x258 -> :sswitch_b
        0x25c -> :sswitch_c
        0x2bc -> :sswitch_3
        0x320 -> :sswitch_d
        0x384 -> :sswitch_e
        0x385 -> :sswitch_f
        0x386 -> :sswitch_10
        0x387 -> :sswitch_15
        0x38a -> :sswitch_11
        0x38b -> :sswitch_12
        0x390 -> :sswitch_13
        0x391 -> :sswitch_14
        0x392 -> :sswitch_16
        0x3e8 -> :sswitch_17
        0x7d0 -> :sswitch_18
        0xbb8 -> :sswitch_19
        0xfa0 -> :sswitch_1a
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 460
    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    const/high16 v3, 0x10000000

    .line 1351
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/api/access_token/a;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1352
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access Token is invalid."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1355
    :cond_0
    sget-object v0, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 1356
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openFile : uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    sparse-switch v0, :sswitch_data_0

    .line 1384
    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1360
    :sswitch_0
    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1364
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1365
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1366
    invoke-static {v1, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 1368
    :sswitch_1
    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1372
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1373
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1374
    invoke-static {v1, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 1376
    :sswitch_2
    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1380
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1381
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1382
    invoke-static {v1, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 1358
    nop

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0xca -> :sswitch_1
        0x191 -> :sswitch_2
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/api/access_token/a;->a(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 469
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "Access Token is invalid."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 473
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 475
    sget-object v2, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 476
    invoke-direct {p0, p1, v2}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;

    move-result-object v4

    .line 477
    const/4 v5, 0x0

    .line 478
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_1

    .line 479
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DB query : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_1
    sparse-switch v2, :sswitch_data_0

    .line 747
    if-eqz v4, :cond_17

    .line 748
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 752
    :cond_2
    :goto_0
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_3

    .line 753
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DB query end: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :cond_3
    :goto_1
    return-object v2

    .line 485
    :sswitch_0
    const-string v2, "SELECT a.*, r.buddy_no AS relation_buddy_no\tFROM (  SELECT j.buddy_no, j.buddy_name, j.buddy_show_phone_number, j.buddy_extra_info, j.buddy_msisdns, j.buddy_status_message, j.weburl, j.is_buddy, i.* FROM inbox i LEFT OUTER JOIN ( SELECT p.participants_buddy_no AS buddy_no, ifnull(b.buddy_name, p.participants_buddy_name) AS buddy_name,p.participants_inbox_no, b.buddy_show_phone_number, b.buddy_extra_info, b.buddy_msisdns, b.buddy_status_message, b.weburl, b.is_buddy FROM participant p LEFT OUTER JOIN ( SELECT buddy_no,buddy_name, buddy_show_phone_number, buddy_extra_info, buddy_msisdns, buddy_status_message, \'\' AS weburl, \'Y\' AS is_buddy FROM buddy UNION ALL SELECT buddy_no AS buddy_no,buddy_name AS buddy_name,\'\' AS buddy_show_phone_number, \'\' AS buddy_extra_info, \'\' AS buddy_msisdns, msgstatus AS buddy_status_message,weburl, \'Y\' AS is_buddy FROM specialbuddy ) b  ON p.participants_buddy_no = b.buddy_no WHERE p.participants_inbox_no NOT null ) j  ON i.inbox_no = j.participants_inbox_no WHERE i.inbox_last_chat_type != 12 GROUP BY i.inbox_no ) a  LEFT OUTER JOIN inbox_buddy_relation r  ON a.inbox_no = r.inbox_no ORDER BY inbox_last_time DESC, a._id"

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 486
    goto :goto_0

    .line 489
    :sswitch_1
    sget-object v2, Lcom/sec/chaton/e/bb;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 490
    goto :goto_0

    .line 493
    :sswitch_2
    sget-object v2, Lcom/sec/chaton/e/bb;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 494
    goto :goto_0

    .line 497
    :sswitch_3
    const-string v2, "SELECT inbox_no, inbox_title, inbox_chat_type, inbox_last_msg_sender, m.message_time_text message_time_text, m.message_content message_content, p.participants_buddy_no participants_buddy_no FROM inbox LEFT OUTER JOIN message m  ON inbox_last_msg_no = m._id LEFT OUTER JOIN participant p  ON (inbox_chat_type = \'0\' and inbox_no = p.participants_inbox_no) "

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 498
    goto :goto_0

    .line 502
    :sswitch_4
    invoke-static {p1}, Lcom/sec/chaton/e/v;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 504
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 505
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 506
    move-object/from16 v0, p4

    array-length v6, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_4

    aget-object v7, p4, v2

    .line 507
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 509
    :cond_4
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 512
    sget-object v4, Lcom/sec/chaton/e/bh;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 513
    goto/16 :goto_0

    .line 517
    :sswitch_5
    invoke-static {p1}, Lcom/sec/chaton/e/v;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 519
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 520
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 521
    move-object/from16 v0, p4

    array-length v6, v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v6, :cond_5

    aget-object v7, p4, v2

    .line 522
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 524
    :cond_5
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 527
    sget-object v4, Lcom/sec/chaton/e/bh;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 528
    goto/16 :goto_0

    .line 531
    :sswitch_6
    if-eqz v4, :cond_17

    move-object v2, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 532
    invoke-direct/range {v2 .. v8}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/chaton/e/bk;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    goto/16 :goto_0

    .line 536
    :sswitch_7
    invoke-static {p1}, Lcom/sec/chaton/e/v;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 537
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 538
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 539
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 543
    sget-object v4, Lcom/sec/chaton/e/bh;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 544
    goto/16 :goto_0

    .line 548
    :sswitch_8
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 549
    if-eqz v5, :cond_1a

    .line 551
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v5, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v2, v5

    goto/16 :goto_0

    .line 557
    :sswitch_9
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "country_num"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "country_iso_code"

    aput-object v4, v2, v3

    .line 558
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 559
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 560
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "country_code"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 561
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v7, "country_ISO"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 562
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_6

    .line 563
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Querued my info from "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "id"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "name"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "country_num"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "country_iso_code"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v8, "ChatONProvider"

    invoke-static {v5, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_6
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-direct {v5, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 569
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 570
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v3, v2, v8

    const/4 v3, 0x1

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v7, v2, v3

    invoke-virtual {v5, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_7
    move-object v2, v5

    .line 575
    goto/16 :goto_0

    .line 580
    :sswitch_a
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "voip"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "voicecall_support"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "videocall_support"

    aput-object v4, v2, v3

    .line 582
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 586
    const-string v4, "OFF"

    .line 587
    const-string v6, "OFF"

    .line 588
    const-string v7, "OFF"

    .line 590
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_8

    .line 591
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Querued my extra info : id="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "voip"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "voicecall_support"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "videocall_support"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v8, "ChatONProvider"

    invoke-static {v5, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    :cond_8
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-direct {v5, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 595
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v3, v2, v8

    const/4 v3, 0x1

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v7, v2, v3

    invoke-virtual {v5, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object v2, v5

    .line 597
    goto/16 :goto_0

    .line 600
    :sswitch_b
    invoke-static {p1}, Lcom/sec/chaton/e/i;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 601
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BUDDYEXTRAINFO : id ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ChatONProvider"

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_17

    .line 604
    const/4 v6, 0x3

    new-array v7, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "buddy_no"

    aput-object v8, v7, v6

    const/4 v6, 0x1

    const-string v8, "buddy_orginal_number"

    aput-object v8, v7, v6

    const/4 v6, 0x2

    const-string v8, "buddy_extra_info"

    aput-object v8, v7, v6

    .line 606
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 607
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 610
    const/4 v6, 0x0

    .line 612
    if-eqz v3, :cond_9

    .line 613
    const-string v6, "buddy_no = ?"

    invoke-virtual {v4, v6, v2}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v7, v4}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object v6, v2

    .line 616
    :cond_9
    if-eqz v6, :cond_b

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 617
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "orgnum"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "voip"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "voicecall_support"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "videocall_support"

    aput-object v4, v2, v3

    .line 619
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-direct {v5, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 620
    const-string v2, "buddy_no"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 621
    const-string v2, "buddy_orginal_number"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 622
    const-string v2, "buddy_extra_info"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 624
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 626
    invoke-static {v9}, Lcom/sec/chaton/d/a/dw;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "ON"

    move-object v4, v2

    .line 627
    :goto_4
    invoke-static {v9}, Lcom/sec/chaton/d/a/dw;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "ON"

    move-object v3, v2

    .line 628
    :goto_5
    invoke-static {v9}, Lcom/sec/chaton/d/a/dw;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "ON"

    .line 630
    :goto_6
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 631
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 633
    sget-boolean v9, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v9, :cond_a

    .line 634
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Querued buddy extra info : id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "orgnum"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "voip"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "voicecall_support"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "videocall_support"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "ChatONProvider"

    invoke-static {v9, v10}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    :cond_a
    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    const/4 v7, 0x1

    aput-object v8, v9, v7

    const/4 v7, 0x2

    aput-object v4, v9, v7

    const/4 v4, 0x3

    aput-object v3, v9, v4

    const/4 v3, 0x4

    aput-object v2, v9, v3

    invoke-virtual {v5, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 641
    :cond_b
    if-eqz v6, :cond_c

    .line 642
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_c
    move-object v2, v5

    .line 645
    goto/16 :goto_0

    .line 626
    :cond_d
    const-string v2, "OFF"

    move-object v4, v2

    goto/16 :goto_4

    .line 627
    :cond_e
    const-string v2, "OFF"

    move-object v3, v2

    goto/16 :goto_5

    .line 628
    :cond_f
    const-string v2, "OFF"

    goto/16 :goto_6

    .line 650
    :sswitch_c
    const/4 v2, 0x0

    .line 651
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "buddy_no"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "buddy_orginal_number"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "buddy_extra_info"

    aput-object v8, v6, v7

    .line 653
    if-eqz v3, :cond_19

    .line 654
    const/4 v2, 0x0

    invoke-virtual {v4, v3, v6, v2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object v6, v2

    .line 657
    :goto_7
    const/4 v2, 0x5

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "id"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string v4, "orgnum"

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const-string v4, "voip"

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "voicecall_support"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, "videocall_support"

    aput-object v4, v3, v2
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 660
    if-eqz v6, :cond_18

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 661
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 662
    const-string v3, "buddy_no"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 663
    const-string v3, "buddy_orginal_number"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 664
    const-string v3, "buddy_extra_info"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 667
    :cond_10
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 668
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 669
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 671
    invoke-static {v12}, Lcom/sec/chaton/d/a/dw;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const-string v3, "ON"

    move-object v5, v3

    .line 672
    :goto_8
    invoke-static {v12}, Lcom/sec/chaton/d/a/dw;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    const-string v3, "ON"

    move-object v4, v3

    .line 673
    :goto_9
    invoke-static {v12}, Lcom/sec/chaton/d/a/dw;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    const-string v3, "ON"

    .line 675
    :goto_a
    sget-boolean v12, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v12, :cond_11

    .line 676
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Querued buddy extra info : id="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "orgnum"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "voip"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "voicecall_support"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "videocall_support"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ChatONProvider"

    invoke-static {v12, v13}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_11
    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v10, v12, v13

    const/4 v10, 0x1

    aput-object v11, v12, v10

    const/4 v10, 0x2

    aput-object v5, v12, v10

    const/4 v5, 0x3

    aput-object v4, v12, v5

    const/4 v4, 0x4

    aput-object v3, v12, v4

    invoke-virtual {v2, v12}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 681
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_10

    .line 686
    :goto_b
    if-eqz v6, :cond_2

    .line 687
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    .line 756
    :catch_0
    move-exception v2

    .line 757
    :try_start_3
    const-string v3, "ChatONProvider"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 769
    :goto_c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 671
    :cond_12
    :try_start_4
    const-string v3, "OFF"

    move-object v5, v3

    goto/16 :goto_8

    .line 672
    :cond_13
    const-string v3, "OFF"

    move-object v4, v3

    goto/16 :goto_9

    .line 673
    :cond_14
    const-string v3, "OFF"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_a

    .line 686
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_15

    .line 687
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 686
    :cond_15
    throw v2
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 758
    :catch_1
    move-exception v2

    .line 759
    :try_start_6
    const-string v3, "ChatONProvider"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_c

    .line 766
    :catchall_1
    move-exception v2

    throw v2

    .line 696
    :sswitch_d
    :try_start_7
    sget-object v2, Lcom/sec/chaton/e/ax;->l:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    sget-object v2, Lcom/sec/chaton/e/ax;->l:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 698
    goto/16 :goto_0

    .line 701
    :sswitch_e
    sget-object v2, Lcom/sec/chaton/e/ax;->o:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    sget-object v2, Lcom/sec/chaton/e/ax;->o:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 703
    goto/16 :goto_0

    .line 705
    :sswitch_f
    sget-object v2, Lcom/sec/chaton/e/ax;->q:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    sget-object v2, Lcom/sec/chaton/e/ax;->q:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 707
    goto/16 :goto_0

    .line 709
    :sswitch_10
    const-string v2, "SELECT noti_buddy_no,noti_inbox_no,noti_sent_time,noti_message,noti_chat_type,noti_msg_type,buddy_name FROM message_notification n LEFT OUTER JOIN (SELECT p.participants_buddy_no AS buddy_no,ifnull (b.buddy_name, p.participants_buddy_name)  AS buddy_name, p.participants_inbox_no FROM participant p LEFT OUTER JOIN (SELECT buddy_no,buddy_name FROM buddy UNION ALL SELECT buddy_no AS buddy_no,buddy_name AS buddy_name FROM specialbuddy) b ON p.participants_buddy_no=b.buddy_no) j ON n.noti_inbox_no=j.participants_inbox_no AND n.noti_buddy_no=j.buddy_no ORDER BY n.noti_sent_time DESC;"

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 710
    goto/16 :goto_0

    .line 713
    :sswitch_11
    sget-object v2, Lcom/sec/chaton/e/bh;->g:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 714
    goto/16 :goto_0

    .line 717
    :sswitch_12
    const-string v2, "SELECT M._id AS _id, M.message_sever_id AS message_sever_id, M.message_inbox_no AS message_inbox_no, M.message_content_type AS message_content_type, M.message_time AS message_time, M.message_content AS message_content, M.message_type AS message_type, M.message_download_uri, M.message_formatted, M.message_is_failed AS message_is_failed, M.message_is_file_upload, M.message_content_translated, I.inbox_no AS inbox_no, I.inbox_chat_type AS inbox_chat_type, I.inbox_server_ip AS inbox_server_ip, I.inbox_server_port AS inbox_server_port, I.inbox_participants AS inbox_participants, I.inbox_session_id AS inbox_session_id, I.inbox_valid AS inbox_valid, I.lasst_session_merge_time AS lasst_session_merge_time FROM message AS M INNER JOIN inbox AS I ON M.message_inbox_no = I.inbox_no WHERE M.message_sender = ?  AND ( M.message_type = 0 OR M.message_type = -1 OR M.message_type = 6 ) "

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 718
    goto/16 :goto_0

    .line 721
    :sswitch_13
    const-string v2, "SELECT message_content, message_sender, message_time_text, message_inbox_no, i.inbox_title inbox_title, i.inbox_chat_type inbox_chat_type, p.participants_buddy_no participants_buddy_no FROM message LEFT OUTER JOIN inbox i  ON message_inbox_no = i.inbox_no LEFT OUTER JOIN participant p  ON (i.inbox_chat_type = \'0\' and message_inbox_no = p.participants_inbox_no) "

    move-object/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v2, v5

    .line 722
    goto/16 :goto_0

    .line 726
    :sswitch_14
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "theme"

    aput-object v4, v2, v3

    .line 729
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "Theme"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 730
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_16

    .line 731
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Querued setting info from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "theme"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ChatONProvider"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :cond_16
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-direct {v5, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 736
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v5, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v2, v5

    .line 741
    goto/16 :goto_0

    .line 760
    :catch_2
    move-exception v2

    .line 761
    :try_start_8
    const-string v3, "ChatONProvider"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 762
    :catch_3
    move-exception v2

    .line 763
    const-string v3, "ChatONProvider"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 764
    :catch_4
    move-exception v2

    .line 765
    const-string v3, "ChatONProvider"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_c

    :cond_17
    move-object v2, v5

    goto/16 :goto_0

    :cond_18
    move-object v2, v5

    goto/16 :goto_b

    :cond_19
    move-object v6, v2

    goto/16 :goto_7

    :cond_1a
    move-object v2, v5

    goto/16 :goto_0

    .line 482
    nop

    :sswitch_data_0
    .sparse-switch
        0x6f -> :sswitch_d
        0x196 -> :sswitch_6
        0x197 -> :sswitch_0
        0x198 -> :sswitch_1
        0x199 -> :sswitch_2
        0x19b -> :sswitch_3
        0x1fd -> :sswitch_4
        0x1fe -> :sswitch_7
        0x201 -> :sswitch_5
        0x202 -> :sswitch_11
        0x203 -> :sswitch_12
        0x204 -> :sswitch_13
        0x3e8 -> :sswitch_8
        0x3e9 -> :sswitch_f
        0x44c -> :sswitch_9
        0x4b0 -> :sswitch_a
        0x514 -> :sswitch_b
        0x578 -> :sswitch_c
        0x7d0 -> :sswitch_8
        0x7d1 -> :sswitch_e
        0xbb9 -> :sswitch_10
        0x1388 -> :sswitch_14
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    .line 1145
    .line 1150
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1151
    sget-object v4, Lcom/sec/chaton/provider/ChatONProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    .line 1152
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1154
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_0

    .line 1155
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DB update start: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    :cond_0
    sparse-switch v7, :sswitch_data_0

    .line 1226
    invoke-direct {p0, p1, v7}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;

    move-result-object v2

    .line 1227
    invoke-virtual {v2, p3, p4}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    invoke-virtual {v2, v0, p2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    .line 1228
    :try_start_1
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1231
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DB update end: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1236
    if-lez v2, :cond_8

    if-eqz v8, :cond_8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 1237
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1238
    invoke-virtual {p0}, Lcom/sec/chaton/provider/ChatONProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1244
    :catch_0
    move-exception v0

    move v1, v2

    .line 1245
    :goto_2
    :try_start_2
    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1246
    const/4 v0, -0x4

    .line 1260
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    :goto_3
    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    :cond_3
    :goto_4
    return v0

    .line 1160
    :sswitch_0
    :try_start_3
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    .line 1161
    :try_start_4
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1247
    :catch_1
    move-exception v0

    .line 1248
    :goto_5
    :try_start_5
    const-string v1, "ChatONProvider"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1249
    const/4 v0, -0x3

    .line 1260
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto :goto_3

    .line 1166
    :sswitch_1
    :try_start_6
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/sec/chaton/provider/ChatONProvider;->c(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v2

    .line 1167
    :try_start_7
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 1250
    :catch_2
    move-exception v0

    .line 1251
    :goto_6
    :try_start_8
    const-string v1, "ChatONProvider"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1252
    const/4 v0, -0x5

    .line 1260
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto :goto_3

    .line 1173
    :sswitch_2
    :try_start_9
    invoke-static {}, Lcom/sec/chaton/provider/a/a;->a()Lcom/sec/chaton/provider/a/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/provider/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1174
    const/4 v2, 0x1

    .line 1175
    :try_start_a
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_a .. :try_end_a} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_a .. :try_end_a} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 1253
    :catch_3
    move-exception v0

    .line 1254
    :goto_7
    :try_start_b
    const-string v1, "ChatONProvider"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1255
    const/4 v0, -0x2

    .line 1260
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    goto/16 :goto_3

    .line 1180
    :sswitch_3
    :try_start_c
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/sec/chaton/provider/ChatONProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_c .. :try_end_c} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_c .. :try_end_c} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_c .. :try_end_c} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result v2

    .line 1181
    :try_start_d
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_d .. :try_end_d} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_d .. :try_end_d} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_d .. :try_end_d} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 1256
    :catch_4
    move-exception v0

    .line 1257
    :goto_8
    :try_start_e
    const-string v3, "ChatONProvider"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1260
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnInteger : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ChatONProvider"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v1

    .line 1263
    goto/16 :goto_4

    .line 1185
    :sswitch_4
    :try_start_f
    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/provider/ChatONProvider;->j(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_f .. :try_end_f} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_f .. :try_end_f} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_f .. :try_end_f} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    move-result v2

    .line 1186
    :try_start_10
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_10
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    .line 1260
    :catchall_0
    move-exception v0

    :goto_9
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_5

    .line 1261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnInteger : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChatONProvider"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1260
    :cond_5
    throw v0

    .line 1190
    :sswitch_5
    :try_start_11
    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/provider/ChatONProvider;->g(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_11 .. :try_end_11} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_11 .. :try_end_11} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-result v2

    .line 1191
    if-lez v2, :cond_1

    .line 1192
    :try_start_12
    sget-object v0, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1193
    sget-object v0, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_12
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_12 .. :try_end_12} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_4
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    .line 1199
    :sswitch_6
    :try_start_13
    const-string v4, "_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1200
    const-string v4, "_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide v5, v4

    .line 1202
    :goto_a
    const-string v4, "message_sever_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1203
    const-string v2, "message_sever_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-wide v3, v2

    .line 1206
    :goto_b
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_6

    .line 1207
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1208
    const-string v9, "DB update : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "MsgID : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "MsgID(ServerNO) : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1209
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v9, "ChatONProvider"

    invoke-static {v2, v9}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    :cond_6
    invoke-direct {p0, p1, v7}, Lcom/sec/chaton/provider/ChatONProvider;->b(Landroid/net/Uri;I)Lcom/sec/chaton/e/bk;

    move-result-object v2

    .line 1212
    invoke-virtual {v2, p3, p4}, Lcom/sec/chaton/e/bk;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/e/bk;

    move-result-object v2

    invoke-virtual {v2, v0, p2}, Lcom/sec/chaton/e/bk;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_13 .. :try_end_13} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_6
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    move-result v2

    .line 1213
    if-eqz p4, :cond_7

    .line 1214
    :try_start_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ContentProvider : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v7, 0x0

    aget-object v7, p4, v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UdateMessage Result : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1218
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 1219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1220
    const-string v7, "DB update end : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "MsgID : "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "MsgID(ServerNO) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1221
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ChatONProvider"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1242
    :cond_8
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V
    :try_end_14
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_14 .. :try_end_14} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 1260
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_9

    .line 1261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "returnInteger : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatONProvider"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v0, v2

    .line 1243
    goto/16 :goto_4

    .line 1260
    :catchall_1
    move-exception v0

    move v2, v1

    goto/16 :goto_9

    .line 1256
    :catch_5
    move-exception v0

    move v2, v1

    goto/16 :goto_8

    .line 1253
    :catch_6
    move-exception v0

    move v2, v1

    goto/16 :goto_7

    .line 1250
    :catch_7
    move-exception v0

    move v2, v1

    goto/16 :goto_6

    .line 1247
    :catch_8
    move-exception v0

    move v2, v1

    goto/16 :goto_5

    .line 1244
    :catch_9
    move-exception v0

    goto/16 :goto_2

    :cond_a
    move-wide v3, v2

    goto/16 :goto_b

    :cond_b
    move-wide v5, v2

    goto/16 :goto_a

    .line 1158
    nop

    :sswitch_data_0
    .sparse-switch
        0x195 -> :sswitch_5
        0x19a -> :sswitch_3
        0x19c -> :sswitch_1
        0x1f4 -> :sswitch_6
        0x1f8 -> :sswitch_4
        0x1004 -> :sswitch_2
        0x1068 -> :sswitch_0
    .end sparse-switch
.end method
