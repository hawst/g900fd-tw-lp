.class public Lcom/sec/chaton/provider/SuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "SuggestionsProvider.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Landroid/content/UriMatcher;


# instance fields
.field private c:Lcom/sec/chaton/search/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/chaton/provider/SuggestionsProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/provider/SuggestionsProvider;->a:Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/sec/chaton/provider/SuggestionsProvider;->a()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/provider/SuggestionsProvider;->b:Landroid/content/UriMatcher;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 80
    return-void
.end method

.method private static a()Landroid/content/UriMatcher;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 57
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 60
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search_suggest_query"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 61
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 62
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search/search_suggest_regex_query"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 63
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search/search_suggest_regex_query/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 71
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search_suggest_shortcut"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    const-string v1, "com.sec.chaton.provider.SuggestionsProvider"

    const-string v2, "search_suggest_shortcut/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 75
    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/provider/SuggestionsProvider;->c:Lcom/sec/chaton/search/b;

    invoke-virtual {v0}, Lcom/sec/chaton/search/b;->a()V

    .line 125
    new-instance v0, Lcom/sec/chaton/search/e;

    invoke-virtual {p0}, Lcom/sec/chaton/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/chaton/search/e;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->a([Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->a(Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->b([Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/search/e;->a(Z)Lcom/sec/chaton/search/e;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/search/g;->a:Lcom/sec/chaton/search/g;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/search/e;->a(Lcom/sec/chaton/search/g;)Lcom/sec/chaton/search/e;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/provider/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/provider/d;-><init>(Lcom/sec/chaton/provider/SuggestionsProvider;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/search/e;->a(Lcom/sec/chaton/search/f;)Lcom/sec/chaton/search/e;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcom/sec/chaton/search/e;->a()Lcom/sec/chaton/search/d;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/sec/chaton/search/d;->f()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/search/d;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/search/d;->c()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/provider/SuggestionsProvider;->c:Lcom/sec/chaton/search/b;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/search/b;->a(Lcom/sec/chaton/search/d;)V

    .line 145
    :cond_0
    new-instance v0, Lcom/sec/chaton/search/e;

    invoke-virtual {p0}, Lcom/sec/chaton/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/chaton/search/e;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->a([Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->a(Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/search/e;->b([Ljava/lang/String;)Lcom/sec/chaton/search/e;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/search/e;->a(Z)Lcom/sec/chaton/search/e;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/search/g;->b:Lcom/sec/chaton/search/g;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/search/e;->a(Lcom/sec/chaton/search/g;)Lcom/sec/chaton/search/e;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/provider/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/provider/e;-><init>(Lcom/sec/chaton/provider/SuggestionsProvider;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/search/e;->a(Lcom/sec/chaton/search/f;)Lcom/sec/chaton/search/e;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/sec/chaton/search/e;->a()Lcom/sec/chaton/search/d;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcom/sec/chaton/provider/SuggestionsProvider;->c:Lcom/sec/chaton/search/b;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/search/b;->a(Lcom/sec/chaton/search/d;)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/provider/SuggestionsProvider;->c:Lcom/sec/chaton/search/b;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/search/b;->a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 164
    return-object v0
.end method

.method private a(Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;
    .locals 10

    .prologue
    .line 168
    new-instance v5, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/sec/chaton/search/b;->d:[Ljava/lang/String;

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 171
    if-eqz p1, :cond_5

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 173
    :cond_0
    const-string v0, "inbox_title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    const-string v1, "message_content"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 175
    const-string v1, "message_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 177
    const/4 v1, 0x0

    .line 178
    const-wide/16 v7, 0x0

    cmp-long v4, v2, v7

    if-eqz v4, :cond_9

    .line 179
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 182
    :goto_0
    const-string v1, "message_inbox_no"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 183
    const-string v1, "inbox_chat_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v7

    .line 184
    const-string v1, "participants_buddy_no"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 186
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 188
    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v7, v1, :cond_7

    .line 190
    if-eqz v2, :cond_4

    .line 193
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 206
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 207
    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v7, v3, :cond_1

    .line 208
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0155

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_1
    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v7, v3, :cond_2

    .line 211
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b00b4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 213
    :cond_2
    invoke-static {v7}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 214
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0155

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v2, v1}, Lcom/sec/chaton/search/b;->a(Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    aput-object v6, v8, v0

    const/4 v0, 0x2

    aput-object v4, v8, v0

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v7}, Lcom/sec/chaton/search/b;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v5, v3, v1, v0, v2}, Lcom/sec/chaton/search/b;->a(Landroid/database/MatrixCursor;ILandroid/net/Uri;Ljava/util/List;Landroid/content/Intent;)Landroid/database/MatrixCursor;

    .line 221
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 224
    :cond_5
    if-eqz p1, :cond_6

    .line 225
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 230
    :cond_6
    return-object v5

    .line 198
    :cond_7
    :try_start_1
    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v7, v1, :cond_4

    .line 200
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move-object v2, v3

    goto :goto_1

    .line 224
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_8

    .line 225
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 224
    :cond_8
    throw v0

    :cond_9
    move-object v4, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/provider/SuggestionsProvider;Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/provider/SuggestionsProvider;->b(Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;
    .locals 8

    .prologue
    .line 234
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/sec/chaton/search/b;->d:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 236
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    :cond_0
    const-string v1, "buddy_no"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 239
    const-string v2, "buddy_name"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 240
    const-string v3, "suggest_text_2"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 242
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v5}, Lcom/sec/chaton/search/b;->a(Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object v3, v6, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-object v3, v6, v2

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-static {v1, v3}, Lcom/sec/chaton/search/b;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v4, v5, v2, v1}, Lcom/sec/chaton/search/b;->a(Landroid/database/MatrixCursor;ILandroid/net/Uri;Ljava/util/List;Landroid/content/Intent;)Landroid/database/MatrixCursor;

    .line 246
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 249
    :cond_1
    if-eqz p1, :cond_2

    .line 250
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 255
    :cond_2
    return-object v0

    .line 249
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_3

    .line 250
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_3
    throw v0
.end method

.method static synthetic b(Lcom/sec/chaton/provider/SuggestionsProvider;Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/provider/SuggestionsProvider;->a(Landroid/database/Cursor;Lcom/sec/chaton/search/d;)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/chaton/search/b;

    invoke-direct {v0}, Lcom/sec/chaton/search/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/provider/SuggestionsProvider;->c:Lcom/sec/chaton/search/b;

    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 91
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query(), uri ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/provider/SuggestionsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query(), projection ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/provider/SuggestionsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query(), selection ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/provider/SuggestionsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query(), selectionArgs ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/provider/SuggestionsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/api/access_token/a;->a(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access Token is invalid."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 104
    sget-object v2, Lcom/sec/chaton/provider/SuggestionsProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 119
    :cond_2
    :goto_0
    return-object v0

    .line 106
    :pswitch_0
    if-nez p4, :cond_2

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectionArgs must be provided for the Uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_3

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectionArgs must be provided for the Uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_3
    invoke-direct {p0, v1, p1}, Lcom/sec/chaton/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    return v0
.end method
