.class public Lcom/sec/chaton/event/NewFeatureFragment;
.super Landroid/support/v4/app/Fragment;
.source "NewFeatureFragment.java"


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Lcom/sec/chaton/event/i;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/view/View;

.field private g:Lcom/sec/chaton/event/NewFeature;

.field private h:I

.field private i:I

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/webkit/WebView;

.field private l:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 46
    iput v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->h:I

    .line 47
    iput v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    .line 385
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/event/NewFeatureFragment;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/event/NewFeatureFragment;I)I
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/chaton/event/NewFeatureFragment;->h:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/event/NewFeatureFragment;Landroid/webkit/WebView;)Landroid/webkit/WebView;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/chaton/event/NewFeatureFragment;->k:Landroid/webkit/WebView;

    return-object p1
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, -0x2

    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    const v2, 0x7f0704fc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->e:Landroid/widget/LinearLayout;

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    move v0, v1

    .line 60
    :goto_0
    iget v2, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    if-ge v0, v2, :cond_2

    .line 62
    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->g:Lcom/sec/chaton/event/NewFeature;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 64
    iget v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->h:I

    if-ne v3, v0, :cond_1

    .line 65
    const v3, 0x7f0201c2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 70
    :goto_1
    iget v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 71
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 72
    const/16 v4, 0xd

    invoke-virtual {v3, v1, v1, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 73
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    const v3, 0x7f0201c1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 77
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/event/NewFeatureFragment;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/event/NewFeatureFragment;->a()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/event/NewFeatureFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/event/NewFeatureFragment;)Lcom/sec/chaton/event/NewFeature;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->g:Lcom/sec/chaton/event/NewFeature;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->k:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/event/NewFeatureFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->d:Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 83
    check-cast p1, Lcom/sec/chaton/event/NewFeature;

    iput-object p1, p0, Lcom/sec/chaton/event/NewFeatureFragment;->g:Lcom/sec/chaton/event/NewFeature;

    .line 85
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 192
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 89
    invoke-super {p0, p3}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f030138

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    const v2, 0x7f0704fa

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->d:Landroid/widget/LinearLayout;

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    const v2, 0x7f0704fd

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->l:Landroid/widget/CheckBox;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->l:Landroid/widget/CheckBox;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->j:Ljava/util/ArrayList;

    .line 101
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "new_feature_count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    .line 104
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "new_feature_url_list"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 105
    :goto_0
    iget v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    if-ge v0, v3, :cond_0

    .line 106
    iget-object v3, p0, Lcom/sec/chaton/event/NewFeatureFragment;->j:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    .line 110
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 117
    :cond_0
    iget v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    if-le v0, v5, :cond_1

    .line 118
    invoke-direct {p0}, Lcom/sec/chaton/event/NewFeatureFragment;->a()V

    .line 120
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->c:Ljava/util/List;

    move v0, v1

    .line 121
    :goto_1
    iget v2, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    if-ge v0, v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/sec/chaton/event/NewFeatureFragment;->c:Ljava/util/List;

    const-string v3, ""

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    const v2, 0x7f0702d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 132
    const v2, 0x7f0b03fc

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 133
    new-instance v2, Lcom/sec/chaton/event/h;

    invoke-direct {v2, p0}, Lcom/sec/chaton/event/h;-><init>(Lcom/sec/chaton/event/NewFeatureFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const-string v0, "new_feature_count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    const-string v0, "new_feature_url_list"

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "new_feature_check"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 155
    const-string v0, "new_feature_dont_show_check_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    const v2, 0x7f0704fb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->a:Landroid/support/v4/view/ViewPager;

    .line 176
    new-instance v0, Lcom/sec/chaton/event/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/event/i;-><init>(Lcom/sec/chaton/event/NewFeatureFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->b:Lcom/sec/chaton/event/i;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/chaton/event/NewFeatureFragment;->b:Lcom/sec/chaton/event/i;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->a:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/sec/chaton/event/NewFeatureFragment;->i:I

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->a:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/sec/chaton/event/l;

    invoke-direct {v2, p0}, Lcom/sec/chaton/event/l;-><init>(Lcom/sec/chaton/event/NewFeatureFragment;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/event/NewFeatureFragment;->f:Landroid/view/View;

    return-object v0
.end method
