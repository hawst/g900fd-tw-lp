.class public Lcom/sec/chaton/event/a;
.super Ljava/lang/Object;
.source "EventBannerImageTask.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/chaton/event/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/event/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 28
    const/4 v0, 0x0

    .line 30
    if-eqz p0, :cond_0

    .line 31
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 33
    if-lez v2, :cond_1

    .line 34
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 36
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 37
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    if-nez v0, :cond_2

    .line 47
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/sec/chaton/event/f;->a(Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-static {v3}, Lcom/sec/chaton/event/f;->a(Z)V

    goto :goto_0

    .line 49
    :cond_2
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadImage: Url = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "filename = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/event/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/event/a;->a:Ljava/lang/String;

    invoke-virtual {v1, p1, p0, v0, v2}, Lcom/sec/common/util/a/a;->b(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 54
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 55
    invoke-static {v3}, Lcom/sec/chaton/event/f;->a(Z)V

    goto :goto_0

    .line 56
    :catch_1
    move-exception v0

    .line 58
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 59
    invoke-static {v3}, Lcom/sec/chaton/event/f;->a(Z)V

    goto :goto_0
.end method
