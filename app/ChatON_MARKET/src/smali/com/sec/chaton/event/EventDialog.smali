.class public Lcom/sec/chaton/event/EventDialog;
.super Landroid/app/Activity;
.source "EventDialog.java"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/event/EventDialog;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->c:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 154
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/chaton/event/EventDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 157
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p0, v1}, Lcom/sec/chaton/event/EventDialog;->startActivity(Landroid/content/Intent;)V

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 128
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 41
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0, v2}, Lcom/sec/chaton/event/EventDialog;->requestWindowFeature(I)Z

    .line 44
    const v0, 0x7f0300a6

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->setContentView(I)V

    .line 46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->setFinishOnTouchOutside(Z)V

    .line 50
    :cond_0
    const v0, 0x7f0702d9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/event/EventDialog;->a:Landroid/widget/Button;

    .line 51
    const v0, 0x7f0702d8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/event/EventDialog;->b:Landroid/widget/Button;

    .line 52
    const v0, 0x7f070314

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/event/EventDialog;->c:Landroid/widget/CheckBox;

    .line 53
    const v0, 0x7f070313

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/event/EventDialog;->d:Landroid/widget/LinearLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->a:Landroid/widget/Button;

    const v1, 0x7f0b02da

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->b:Landroid/widget/Button;

    const v1, 0x7f0b0176

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->c:Landroid/widget/CheckBox;

    const v1, 0x7f0b0123

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 61
    const v0, 0x7f070312

    invoke-virtual {p0, v0}, Lcom/sec/chaton/event/EventDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/event/EventDialog;->e:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/event/b;

    invoke-direct {v1, p0}, Lcom/sec/chaton/event/b;-><init>(Lcom/sec/chaton/event/EventDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->a:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/event/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/event/c;-><init>(Lcom/sec/chaton/event/EventDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/event/EventDialog;->b:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/event/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/event/d;-><init>(Lcom/sec/chaton/event/EventDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    invoke-static {}, Lcom/sec/chaton/event/f;->n()Ljava/io/File;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/sec/chaton/event/EventDialog;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 118
    invoke-static {}, Lcom/sec/chaton/event/f;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "event_do_not_show_popup_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 121
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 122
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/event/EventDialog;->finish()V

    .line 134
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 111
    invoke-direct {p0}, Lcom/sec/chaton/event/EventDialog;->a()V

    .line 112
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 142
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/event/EventDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 151
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
