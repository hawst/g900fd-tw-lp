.class public Lcom/sec/chaton/event/i;
.super Landroid/support/v4/view/PagerAdapter;
.source "NewFeatureFragment.java"


# instance fields
.field a:Ljava/lang/String;

.field final synthetic b:Lcom/sec/chaton/event/NewFeatureFragment;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/event/NewFeatureFragment;)V
    .locals 1

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/event/i;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 223
    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Landroid/widget/RelativeLayout;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 225
    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->c(Lcom/sec/chaton/event/NewFeatureFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 382
    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 243
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->d(Lcom/sec/chaton/event/NewFeatureFragment;)Lcom/sec/chaton/event/NewFeature;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 245
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    new-instance v3, Landroid/webkit/WebView;

    iget-object v4, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v4}, Lcom/sec/chaton/event/NewFeatureFragment;->d(Lcom/sec/chaton/event/NewFeatureFragment;)Lcom/sec/chaton/event/NewFeature;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, Lcom/sec/chaton/event/NewFeatureFragment;->a(Lcom/sec/chaton/event/NewFeatureFragment;Landroid/webkit/WebView;)Landroid/webkit/WebView;

    .line 263
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 264
    iget-object v3, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v3}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 265
    iget-object v3, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v3}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 266
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 267
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 268
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 269
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 270
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->requestFocusFromTouch()Z

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    new-instance v3, Landroid/webkit/WebChromeClient;

    invoke-direct {v3}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 274
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    .line 275
    iget-object v1, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v1}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v1

    new-instance v3, Landroid/webkit/WebViewClient;

    invoke-direct {v3}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    move v1, v0

    .line 293
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->a(Lcom/sec/chaton/event/NewFeatureFragment;)I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 294
    if-ne p2, v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->f(Lcom/sec/chaton/event/NewFeatureFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->a(Lcom/sec/chaton/event/NewFeatureFragment;)I

    move-result v0

    if-le v0, v5, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->b(Lcom/sec/chaton/event/NewFeatureFragment;)V

    .line 293
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/event/j;

    invoke-direct {v1, p0}, Lcom/sec/chaton/event/j;-><init>(Lcom/sec/chaton/event/i;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 355
    iget-object v0, p0, Lcom/sec/chaton/event/i;->b:Lcom/sec/chaton/event/NewFeatureFragment;

    invoke-static {v0}, Lcom/sec/chaton/event/NewFeatureFragment;->e(Lcom/sec/chaton/event/NewFeatureFragment;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 357
    check-cast p1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1, v2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 358
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 359
    return-object v2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 364
    check-cast p2, Landroid/widget/RelativeLayout;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 378
    return-void
.end method
