.class final Lcom/sec/chaton/bx;
.super Landroid/widget/BaseAdapter;
.source "TabActivity.java"


# instance fields
.field final a:[Ljava/lang/String;

.field final synthetic b:Lcom/sec/chaton/TabActivity;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 4

    .prologue
    .line 2871
    iput-object p1, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2872
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0147

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0148

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0181

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0348

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/ap;)V
    .locals 0

    .prologue
    .line 2871
    invoke-direct {p0, p1}, Lcom/sec/chaton/bx;-><init>(Lcom/sec/chaton/TabActivity;)V

    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 3003
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 2980
    const/4 v0, 0x0

    .line 2982
    packed-switch p1, :pswitch_data_0

    .line 2999
    :goto_0
    return v0

    .line 2984
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->c(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2985
    goto :goto_0

    .line 2988
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->L(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    goto :goto_0

    .line 2992
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->M(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    goto :goto_0

    .line 2996
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->N(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    goto :goto_0

    .line 2982
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2877
    iget-object v0, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v3, 0x63

    const/4 v4, 0x0

    .line 2950
    if-nez p2, :cond_0

    .line 2951
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030006

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2954
    :cond_0
    const v0, 0x7f070026

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2955
    iget-object v1, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2957
    const v0, 0x7f070027

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2959
    invoke-direct {p0, p1}, Lcom/sec/chaton/bx;->a(I)I

    move-result v2

    .line 2961
    if-nez v2, :cond_2

    .line 2962
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2976
    :cond_1
    :goto_0
    return-object p2

    .line 2965
    :cond_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 2966
    if-le v2, v3, :cond_3

    .line 2967
    const-string v1, "%d+"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2969
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2970
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2971
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 2972
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDropDownView setBadge : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2882
    iget-object v0, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 2887
    packed-switch p1, :pswitch_data_0

    .line 2901
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    .line 2889
    :pswitch_0
    const-wide/32 v0, 0x7f070008

    goto :goto_0

    .line 2892
    :pswitch_1
    const-wide/32 v0, 0x7f070009

    goto :goto_0

    .line 2895
    :pswitch_2
    const-wide/32 v0, 0x7f07000a

    goto :goto_0

    .line 2898
    :pswitch_3
    const-wide/32 v0, 0x7f07000b

    goto :goto_0

    .line 2887
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f070028

    const/16 v3, 0x63

    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 2907
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 2908
    if-nez p2, :cond_1

    .line 2909
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030008

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2910
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2912
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2913
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/widget/NavigationTabWidget;

    .line 2914
    invoke-direct {p0}, Lcom/sec/chaton/bx;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/NavigationTabWidget;->setMaxWidth(I)V

    :cond_0
    move-object p2, v0

    .line 2918
    :cond_1
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/NavigationTabWidget;

    .line 2919
    iget-object v1, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/NavigationTabWidget;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2921
    const v0, 0x7f070029

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2923
    invoke-direct {p0, p1}, Lcom/sec/chaton/bx;->a(I)I

    move-result v2

    .line 2925
    if-nez v2, :cond_2

    .line 2926
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2945
    :goto_0
    return-object p2

    .line 2929
    :cond_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 2930
    if-le v2, v3, :cond_3

    .line 2931
    const-string v1, "%d+"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2933
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2934
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2939
    :cond_4
    check-cast p2, Landroid/widget/TextView;

    .line 2941
    if-nez p2, :cond_5

    .line 2942
    iget-object v0, p0, Lcom/sec/chaton/bx;->b:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030007

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2944
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/bx;->a:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object p2, v0

    .line 2945
    goto :goto_0

    :cond_5
    move-object v0, p2

    goto :goto_1
.end method
