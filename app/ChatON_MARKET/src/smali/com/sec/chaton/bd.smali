.class Lcom/sec/chaton/bd;
.super Ljava/lang/Object;
.source "TabActivity.java"

# interfaces
.implements Lcom/sec/chaton/util/bs;


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 1775
    iput-object p1, p0, Lcom/sec/chaton/bd;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1781
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1782
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Running mChatonVUpgradeCheckTask by polling scheduler at time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    iget-object v1, p0, Lcom/sec/chaton/bd;->a:Lcom/sec/chaton/TabActivity;

    iget-object v2, p0, Lcom/sec/chaton/bd;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v2}, Lcom/sec/chaton/TabActivity;->B(Lcom/sec/chaton/TabActivity;)Lcom/coolots/sso/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V

    .line 1786
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
