.class public Lcom/sec/chaton/d/o;
.super Ljava/lang/Object;
.source "MessageControl2.java"

# interfaces
.implements Lcom/sec/chaton/j/t;


# static fields
.field private static b:Landroid/os/HandlerThread;

.field private static o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/d/o;",
            ">;"
        }
    .end annotation
.end field

.field private static x:I


# instance fields
.field private A:Landroid/os/Handler;

.field a:Lcom/sec/chaton/j/q;

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/lang/Object;

.field private g:Landroid/os/Handler;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/j/ak;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/d/bd;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/background/h;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lcom/sec/chaton/e/r;

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private w:J

.field private y:Landroid/os/Handler;

.field private z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    .line 143
    const/16 v0, 0x168

    sput v0, Lcom/sec/chaton/d/o;->x:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-boolean v1, p0, Lcom/sec/chaton/d/o;->k:Z

    .line 122
    iput-boolean v1, p0, Lcom/sec/chaton/d/o;->m:Z

    .line 124
    iput-boolean v1, p0, Lcom/sec/chaton/d/o;->n:Z

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    .line 131
    iput-boolean v1, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->v:Ljava/util/ArrayList;

    .line 281
    new-instance v0, Lcom/sec/chaton/d/p;

    invoke-static {}, Lcom/sec/chaton/d/o;->b()Landroid/os/HandlerThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/p;-><init>(Lcom/sec/chaton/d/o;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->y:Landroid/os/Handler;

    .line 474
    new-instance v0, Lcom/sec/chaton/d/q;

    invoke-static {}, Lcom/sec/chaton/d/o;->b()Landroid/os/HandlerThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/q;-><init>(Lcom/sec/chaton/d/o;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->z:Landroid/os/Handler;

    .line 490
    new-instance v0, Lcom/sec/chaton/d/r;

    invoke-static {}, Lcom/sec/chaton/d/o;->b()Landroid/os/HandlerThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/r;-><init>(Lcom/sec/chaton/d/o;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    .line 1175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->h:Ljava/util/List;

    .line 1176
    iput-object p1, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    .line 1177
    iput-object p1, p0, Lcom/sec/chaton/d/o;->l:Ljava/lang/String;

    .line 1178
    iput-object p2, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    .line 1179
    new-instance v0, Lcom/sec/chaton/j/q;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    invoke-direct {v0, v1, p0}, Lcom/sec/chaton/j/q;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/t;)V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    .line 1181
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->c:Ljava/lang/Object;

    .line 1182
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    .line 1183
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->e:Ljava/lang/Object;

    .line 1184
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/o;->f:Ljava/lang/Object;

    .line 1185
    return-void
.end method

.method private a(JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 14

    .prologue
    .line 1644
    const-string v2, "doSendMessage"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1649
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_0

    .line 1650
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object v13, v2

    .line 1655
    :goto_0
    new-instance v2, Lcom/sec/chaton/d/a/ab;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide v4, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p6

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, Lcom/sec/chaton/d/a/ab;-><init>(Landroid/os/Handler;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 1657
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v13, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1659
    iget-object v2, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1676
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    return-wide v2

    .line 1652
    :cond_0
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object v13, v2

    goto :goto_0
.end method

.method private a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 12

    .prologue
    .line 2616
    const/4 v0, 0x0

    .line 2617
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2618
    const/4 v0, 0x6

    move v1, v0

    .line 2621
    :goto_0
    invoke-static {p3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2622
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v7

    const/4 v10, 0x0

    move-object/from16 v1, p6

    move-wide v2, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p13

    move-object/from16 v6, p12

    move-object/from16 v8, p8

    move-object/from16 v9, p10

    invoke-static/range {v0 .. v11}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 2629
    return-wide p1

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public static declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;
    .locals 3

    .prologue
    .line 1188
    const-class v1, Lcom/sec/chaton/d/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1189
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/chaton/d/o;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/d/o;-><init>(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    :goto_0
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1191
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;

    iput-object p0, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    .line 1192
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;

    iput-object p0, v0, Lcom/sec/chaton/d/o;->l:Ljava/lang/String;

    .line 1193
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;

    iput-object p1, v0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1188
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/d/o;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/d/o;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    return-object v0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    if-nez v0, :cond_1

    .line 147
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MessageControl"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    .line 148
    sget-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    sget-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    goto :goto_0
.end method

.method private a(IIILjava/lang/Object;J)V
    .locals 5

    .prologue
    .line 266
    iget-object v1, p0, Lcom/sec/chaton/d/o;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/o;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 268
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 269
    iput p1, v3, Landroid/os/Message;->arg1:I

    .line 270
    iput p2, v3, Landroid/os/Message;->arg2:I

    .line 271
    iput p3, v3, Landroid/os/Message;->what:I

    .line 273
    invoke-static {p5, p6, p4}, Lcom/sec/chaton/chat/fj;->a(JLjava/lang/Object;)Lcom/sec/chaton/chat/fj;

    move-result-object v4

    iput-object v4, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 275
    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RequestID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p5, p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V
    .locals 0

    .prologue
    .line 89
    invoke-direct/range {p0 .. p6}, Lcom/sec/chaton/d/o;->a(IIILjava/lang/Object;J)V

    return-void
.end method

.method private a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14

    .prologue
    .line 2707
    sget-object v13, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)Z

    move-result v0

    return v0
.end method

.method private a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)Z
    .locals 19

    .prologue
    .line 2716
    const-string v2, "MessageControl"

    const-string v3, "doSendFileInternal(), msgId(%d), extension(%s), msgType(%s), chatType(%s), sessionId(%s), isInit(%s), message(%s), result(%s), extraChat(%s), ignoreAutoResend(%s)"

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    const/4 v5, 0x3

    aput-object p5, v4, v5

    const/4 v5, 0x4

    aput-object p6, v4, v5

    const/4 v5, 0x5

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    aput-object p9, v4, v5

    const/4 v5, 0x7

    aput-object p10, v4, v5

    const/16 v5, 0x8

    aput-object p11, v4, v5

    const/16 v5, 0x9

    aput-object p13, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2719
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2720
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 2721
    const-string v2, "result is null"

    const-string v3, "MessageControl"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2723
    :cond_0
    const/4 v2, 0x0

    .line 2761
    :goto_0
    return v2

    .line 2726
    :cond_1
    if-eqz p8, :cond_3

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_3

    .line 2727
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 2728
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_2

    .line 2729
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 2731
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->e(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->d(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p11

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->f(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p12

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->g(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p13

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;

    .line 2742
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 2743
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2745
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 2748
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 2747
    :cond_2
    const/4 v2, 0x1

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    .line 2748
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2751
    :cond_3
    new-instance v18, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 2753
    new-instance v2, Lcom/sec/chaton/d/a/df;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide/from16 v5, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p10

    move/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p11

    move-object/from16 v16, p12

    move-object/from16 v17, p13

    invoke-direct/range {v2 .. v17}, Lcom/sec/chaton/d/a/df;-><init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2756
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 2758
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/d/bd;->d()Z

    .line 2761
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I[Ljava/lang/String;)Z
    .locals 15

    .prologue
    .line 1699
    const-string v1, ""

    .line 1700
    const/4 v2, 0x0

    .line 1702
    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    .line 1710
    sget-object v3, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v3, :cond_10

    .line 1719
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1720
    const/4 v1, 0x0

    .line 1858
    :goto_0
    return v1

    .line 1724
    :cond_0
    invoke-static/range {p5 .. p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1725
    new-instance v2, Ljava/io/File;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1726
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "."

    move-object/from16 v0, p5

    invoke-direct {v3, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    :goto_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1728
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1730
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v6, v2

    move-object v12, v1

    .line 1732
    :goto_2
    const/4 v10, 0x0

    .line 1733
    iget-object v1, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1734
    iget-object v1, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v1, v2, :cond_2

    .line 1736
    :cond_2
    const/4 v10, 0x1

    .line 1739
    :cond_3
    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_5

    .line 1743
    :cond_4
    iget-object v6, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v7, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object/from16 v5, p6

    move-object/from16 v8, p9

    move-object/from16 v9, p3

    invoke-virtual/range {v1 .. v10}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)J

    .line 1858
    :goto_3
    const/4 v1, 0x1

    goto :goto_0

    .line 1753
    :cond_5
    sget-object v1, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_6

    .line 1754
    iget-object v7, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v9, p9

    move-object/from16 v11, p3

    invoke-virtual/range {v4 .. v11}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J

    goto :goto_3

    .line 1765
    :cond_6
    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_9

    .line 1766
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 1767
    const-string v1, "\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1769
    const/4 v1, 0x2

    aget-object v1, v2, v1

    .line 1770
    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1772
    array-length v1, v2

    const/4 v3, 0x6

    if-le v1, v3, :cond_8

    const-string v1, "mixed"

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1773
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1774
    const/4 v1, 0x6

    :goto_4
    array-length v4, v2

    if-ge v1, v4, :cond_7

    .line 1775
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v2, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1774
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1777
    :cond_7
    iget-object v7, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    const-wide/16 v12, -0x1

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    move-object v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v9, p9

    invoke-virtual/range {v4 .. v14}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J

    goto :goto_3

    .line 1786
    :cond_8
    iget-object v7, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    const-wide/16 v12, -0x1

    const/4 v14, 0x0

    move-object v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v9, p9

    invoke-virtual/range {v4 .. v14}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J

    goto/16 :goto_3

    .line 1797
    :cond_9
    sget-object v1, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_a

    .line 1798
    iget-object v4, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object/from16 v6, p9

    move-object/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_3

    .line 1805
    :cond_a
    sget-object v1, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_b

    .line 1806
    iget-object v4, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object/from16 v6, p9

    move-object/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_3

    .line 1815
    :cond_b
    const/4 v1, 0x1

    move/from16 v0, p8

    if-ne v0, v1, :cond_c

    .line 1816
    iget-object v6, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v7, p3

    move-object/from16 v9, p9

    move-object/from16 v11, p7

    move-object/from16 v13, p6

    invoke-virtual/range {v3 .. v13}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)J

    goto/16 :goto_3

    .line 1828
    :cond_c
    if-nez v6, :cond_d

    .line 1829
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1831
    :cond_d
    const/4 v12, 0x0

    .line 1832
    if-eqz p3, :cond_f

    .line 1833
    const-string v1, "\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1835
    array-length v1, v2

    const/4 v3, 0x6

    if-le v1, v3, :cond_f

    .line 1836
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1837
    const/4 v1, 0x6

    :goto_5
    array-length v4, v2

    if-ge v1, v4, :cond_e

    .line 1838
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v2, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1837
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1840
    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1844
    :cond_f
    const/4 v5, 0x0

    iget-object v7, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object v4, v6

    move-object/from16 v6, p6

    move-object/from16 v9, p9

    move-object/from16 v11, p7

    invoke-virtual/range {v1 .. v12}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_3

    :cond_10
    move-object v6, v2

    move-object v12, v1

    goto/16 :goto_2
.end method

.method private a(Lcom/sec/chaton/chat/background/h;Z)Z
    .locals 17

    .prologue
    .line 370
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->e()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v5

    .line 371
    if-eqz v5, :cond_8

    .line 373
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->h()J

    move-result-wide v2

    .line 374
    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-eq v1, v5, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    if-eq v1, v5, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    if-eq v1, v5, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    if-eq v1, v5, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-ne v1, v5, :cond_4

    .line 379
    :cond_0
    const-wide/16 v6, -0x1

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 380
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v11

    move-object/from16 v1, p0

    move/from16 v10, p2

    invoke-virtual/range {v1 .. v12}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJ)J

    .line 441
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 443
    :goto_1
    return v1

    .line 386
    :cond_2
    if-eqz p2, :cond_3

    .line 387
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v13

    move-object/from16 v6, p0

    move-object v8, v5

    move-wide v15, v2

    invoke-virtual/range {v6 .. v16}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    goto :goto_0

    .line 392
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v11

    sget-object v13, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v13}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    .line 397
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 398
    const-string v1, "MessageControl"

    const-string v2, "sendBufferedMessage(), SHOULD NOT BE HAPPENED HERE !! (TEXT) : No Msg ID? "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 402
    :cond_4
    sget-object v1, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-ne v1, v5, :cond_6

    .line 403
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 404
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v9

    move-object/from16 v1, p0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v10}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;J)J

    goto :goto_0

    .line 409
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->l()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object/from16 v1, p0

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v10}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    .line 413
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 414
    const-string v1, "MessageControl"

    const-string v2, "sendBufferedMessage(), SHOULD NOT BE HAPPENED HERE !! (GEO) : No Msg ID? "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 419
    :cond_6
    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v1, v5, :cond_7

    .line 420
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->h()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->l()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v1, p0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v11}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J

    goto/16 :goto_0

    .line 432
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->h()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->l()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/chat/background/h;->m()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v1, p0

    move/from16 v9, p2

    invoke-direct/range {v1 .. v13}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 443
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/d/o;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)Z
    .locals 1

    .prologue
    .line 89
    invoke-direct/range {p0 .. p13}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/d/o;Z)Z
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/chaton/d/o;->m:Z

    return p1
.end method

.method public static b()Landroid/os/HandlerThread;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/sec/chaton/d/o;->b:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/d/o;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/d/o;Z)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/o;->c(Z)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/o;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Z)V
    .locals 4

    .prologue
    .line 447
    if-eqz p1, :cond_2

    .line 448
    iget-object v1, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 449
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v0, :cond_1

    .line 450
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/background/h;

    .line 454
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/chat/background/h;Z)Z

    goto :goto_0

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 456
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 459
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    :goto_1
    return-void

    .line 461
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 462
    :try_start_2
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v0, :cond_3

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/background/h;

    .line 465
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/chat/background/h;Z)Z

    .line 470
    :cond_3
    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 467
    :cond_4
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lcom/sec/chaton/d/o;->r:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2
.end method

.method public static declared-synchronized d(Ljava/lang/String;)Lcom/sec/chaton/d/o;
    .locals 2

    .prologue
    .line 1206
    const-class v1, Lcom/sec/chaton/d/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1207
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1209
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1206
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Lcom/sec/chaton/d/o;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/o;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/d/o;)Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/d/o;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3489
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3490
    :cond_0
    const/4 v0, 0x0

    .line 3492
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static declared-synchronized g()V
    .locals 3

    .prologue
    .line 1219
    const-class v1, Lcom/sec/chaton/d/o;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1221
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/o;

    .line 1222
    if-eqz v0, :cond_0

    .line 1223
    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1219
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1227
    :cond_1
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)J
    .locals 12

    .prologue
    .line 2835
    sget-object v11, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/msgsend/k;)J
    .locals 15

    .prologue
    .line 2858
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2859
    const-string v1, "\n"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2862
    const/4 v13, 0x0

    .line 2863
    array-length v1, v2

    const/4 v3, 0x6

    if-le v1, v3, :cond_1

    .line 2864
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 2865
    const/4 v1, 0x6

    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 2866
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v2, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2865
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2868
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 2872
    :cond_1
    array-length v1, v2

    const/4 v3, 0x4

    if-le v1, v3, :cond_2

    .line 2873
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 2874
    iget-object v4, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v12, p10

    move-object/from16 v14, p11

    invoke-virtual/range {v1 .. v14}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    .line 2878
    :goto_1
    return-wide v1

    :cond_2
    const-wide/16 v1, -0x1

    goto :goto_1
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    .prologue
    .line 2461
    const-wide/16 v7, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)J
    .locals 12

    .prologue
    .line 2465
    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v9, 0x1

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v10, p7

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJ)J

    move-result-wide v0

    return-wide v0

    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J
    .locals 9

    .prologue
    .line 2254
    sget-object v8, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;J)J
    .locals 11

    .prologue
    .line 2270
    sget-object v10, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-wide/from16 v8, p8

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 18

    .prologue
    .line 2277
    if-eqz p6, :cond_2

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_2

    .line 2278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 2279
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_1

    .line 2280
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 2282
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    invoke-virtual {v5}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;

    .line 2288
    const-wide/16 v5, -0x1

    cmp-long v3, p8, v5

    if-eqz v3, :cond_0

    .line 2289
    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 2295
    :goto_0
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 2296
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2298
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 2340
    :goto_1
    return-wide v2

    .line 2291
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_0

    .line 2301
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 2300
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    .line 2301
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2306
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_3

    .line 2307
    new-instance v2, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object/from16 v17, v2

    .line 2312
    :goto_2
    new-instance v2, Lcom/sec/chaton/d/a/df;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    const-string v7, ""

    sget-object v8, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    const-string v12, ""

    const/4 v15, 0x0

    move-wide/from16 v5, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v16, p10

    invoke-direct/range {v2 .. v16}, Lcom/sec/chaton/d/a/df;-><init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2320
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 2322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2330
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 2338
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->d()Z

    .line 2340
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto :goto_1

    .line 2310
    :cond_3
    new-instance v2, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object/from16 v17, v2

    goto :goto_2
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J
    .locals 12

    .prologue
    .line 3076
    sget-object v11, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 15

    .prologue
    .line 3086
    if-eqz p6, :cond_2

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_2

    .line 3087
    iget-object v4, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 3088
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_1

    .line 3089
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 3091
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    invoke-virtual {v5}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->f(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    .line 3099
    const-wide/16 v5, -0x1

    cmp-long v3, p8, v5

    if-eqz v3, :cond_0

    .line 3100
    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 3106
    :goto_0
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 3107
    iget-object v3, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3109
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 3138
    :goto_1
    return-wide v2

    .line 3102
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_0

    .line 3112
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 3111
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 3112
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3115
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_3

    .line 3116
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object v14, v2

    .line 3122
    :goto_2
    new-instance v2, Lcom/sec/chaton/d/a/n;

    invoke-virtual {v14}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide/from16 v5, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    invoke-direct/range {v2 .. v13}, Lcom/sec/chaton/d/a/n;-><init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 3135
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v14, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3136
    iget-object v2, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v14}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3137
    invoke-virtual {v14}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3138
    invoke-virtual {v14}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto :goto_1

    .line 3119
    :cond_3
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object v14, v2

    goto :goto_2
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 18

    .prologue
    .line 2184
    if-eqz p6, :cond_2

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_2

    .line 2185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 2186
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_1

    .line 2187
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 2189
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    invoke-virtual {v5}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->f(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    .line 2195
    const-wide/16 v5, -0x1

    cmp-long v3, p8, v5

    if-eqz v3, :cond_0

    .line 2196
    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 2201
    :goto_0
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 2202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2204
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 2245
    :goto_1
    return-wide v2

    .line 2198
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_0

    .line 2207
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 2206
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    .line 2207
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_3

    .line 2214
    new-instance v2, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object/from16 v17, v2

    .line 2220
    :goto_2
    invoke-static/range {p11 .. p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "geo"

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "mixed"

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2223
    const-string v2, "MessageControl"

    const-string v3, "doSendGeoTag(), ERROR : SEND GEO"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p11, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2224
    const-string v2, "MessageControl"

    const-string v3, "doSendGeoTag(), ERROR : SEND GEO"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p11, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2225
    const-string v2, "MessageControl"

    const-string v3, "doSendGeoTag(), ERROR : SEND GEO"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p11, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2226
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 2217
    :cond_3
    new-instance v2, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object/from16 v17, v2

    goto :goto_2

    .line 2230
    :cond_4
    new-instance v2, Lcom/sec/chaton/d/a/df;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    const-string v7, ""

    sget-object v8, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    const-string v12, ""

    const/4 v15, 0x0

    move-wide/from16 v5, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v13, p6

    move-object/from16 v14, p11

    move-object/from16 v16, p12

    invoke-direct/range {v2 .. v16}, Lcom/sec/chaton/d/a/df;-><init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2238
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 2240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2242
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->d()Z

    .line 2245
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto/16 :goto_1
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 11

    .prologue
    .line 2259
    const-wide/16 v8, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v10, p8

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 18

    .prologue
    .line 2996
    if-eqz p6, :cond_1

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_1

    .line 2997
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 2998
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_0

    .line 2999
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 3001
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    invoke-virtual/range {p11 .. p11}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->e(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p12

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->f(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v4

    move-object/from16 v0, p13

    invoke-virtual {v4, v0}, Lcom/sec/chaton/chat/background/i;->a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;

    .line 3008
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 3010
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3012
    monitor-exit v3

    .line 3028
    :goto_0
    return-wide p1

    .line 3014
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/o;->r:Z

    .line 3015
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3020
    :cond_1
    new-instance v17, Lcom/sec/chaton/d/bd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 3021
    new-instance v2, Lcom/sec/chaton/d/a/df;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide/from16 v5, p1

    move-object/from16 v7, p9

    move-object/from16 v8, p11

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p10

    move/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    invoke-direct/range {v2 .. v16}, Lcom/sec/chaton/d/a/df;-><init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 3022
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3025
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_0

    .line 3015
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 14

    .prologue
    .line 2778
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v13, p11

    invoke-virtual/range {v0 .. v13}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J
    .locals 14

    .prologue
    .line 2795
    const/4 v12, 0x0

    sget-object v13, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-virtual/range {v0 .. v13}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 17

    .prologue
    .line 2807
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->y:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    const/4 v13, 0x1

    move-object/from16 v3, p7

    move-object/from16 v4, p3

    move-wide/from16 v5, p1

    move-object/from16 v8, p6

    move-object/from16 v9, p5

    move-object/from16 v10, p4

    move-object/from16 v11, p8

    move-object/from16 v12, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    invoke-virtual/range {v1 .. v16}, Lcom/sec/chaton/j/c/g;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2822
    return-wide p1
.end method

.method public a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)J
    .locals 18

    .prologue
    .line 2661
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->y:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v3, p7

    move-object/from16 v4, p3

    move-wide/from16 v5, p1

    move-object/from16 v8, p6

    move-object/from16 v9, p5

    move-object/from16 v10, p4

    move-object/from16 v11, p8

    move-object/from16 v12, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move/from16 v16, p13

    move-object/from16 v17, p14

    invoke-virtual/range {v1 .. v17}, Lcom/sec/chaton/j/c/g;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)V

    .line 2693
    return-wide p1
.end method

.method public a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)J
    .locals 11

    .prologue
    .line 2352
    sget-object v10, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJ)J
    .locals 13

    .prologue
    .line 2369
    sget-object v12, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJLcom/sec/chaton/msgsend/k;)J
    .locals 14

    .prologue
    .line 2375
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 2376
    const-string v2, "reSend"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    :cond_0
    if-eqz p9, :cond_3

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_3

    .line 2380
    iget-object v4, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 2381
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_2

    .line 2382
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 2384
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-wide v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p12

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;

    .line 2390
    const-wide/16 v5, -0x1

    cmp-long v3, p10, v5

    if-eqz v3, :cond_1

    .line 2391
    move-wide/from16 v0, p10

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 2396
    :goto_0
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 2397
    iget-object v3, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2399
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 2457
    :goto_1
    return-wide v2

    .line 2393
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_0

    .line 2402
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 2401
    :cond_2
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 2402
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407
    :cond_3
    const-wide/16 v2, -0x1

    cmp-long v2, p10, v2

    if-eqz v2, :cond_4

    .line 2408
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p10

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object v13, v2

    .line 2413
    :goto_2
    if-eqz p9, :cond_5

    .line 2415
    sget-object v2, Lcom/sec/chaton/d/s;->a:[I

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2428
    :goto_3
    :pswitch_0
    new-instance v2, Lcom/sec/chaton/d/a/cy;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v4, p3

    move-wide v5, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p12

    invoke-direct/range {v2 .. v11}, Lcom/sec/chaton/d/a/cy;-><init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2429
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v13, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 2453
    :goto_4
    iget-object v2, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2454
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->d()Z

    .line 2457
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto :goto_1

    .line 2410
    :cond_4
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object v13, v2

    goto :goto_2

    .line 2418
    :pswitch_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_3

    .line 2433
    :cond_5
    sget-object v2, Lcom/sec/chaton/d/s;->a:[I

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2448
    :goto_5
    :pswitch_2
    new-instance v2, Lcom/sec/chaton/d/a/ab;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide v4, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p7

    move-object/from16 v12, p12

    invoke-direct/range {v2 .. v12}, Lcom/sec/chaton/d/a/ab;-><init>(Landroid/os/Handler;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 2450
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v13, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2436
    :pswitch_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_5

    .line 2415
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 2433
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)J
    .locals 13

    .prologue
    .line 2358
    const-wide/16 v10, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v12, p10

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 1450
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1451
    new-instance v1, Lcom/sec/chaton/d/a/dn;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v1, v2, p1, p2, v3}, Lcom/sec/chaton/d/a/dn;-><init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1453
    iget-object v1, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1455
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 3251
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 3252
    new-instance v1, Lcom/sec/chaton/d/a/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/sec/chaton/d/a/aw;-><init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 3253
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3254
    iget-object v1, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3255
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3257
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 7

    .prologue
    .line 1926
    sget-object v6, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 12

    .prologue
    .line 1948
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_2

    .line 1949
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1950
    const-string v0, "chatMsg is null or recipients is null"

    const-string v1, "MessageControl"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    :cond_1
    const-wide/16 v0, -0x1

    .line 2007
    :goto_0
    return-wide v0

    .line 1957
    :cond_2
    :try_start_0
    invoke-static/range {p4 .. p5}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->createJson(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1958
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2002
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual/range {p8 .. p8}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2004
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    const/4 v6, 0x0

    const-wide/16 v9, -0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p3

    move-wide/from16 v7, p6

    move-object/from16 v11, p8

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    goto :goto_0

    .line 1959
    :catch_0
    move-exception v0

    .line 1960
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 1961
    const-string v1, "MessageControl"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1963
    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2007
    :cond_5
    sget-object v1, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 9

    .prologue
    .line 1931
    const-wide/16 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/e/r;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/msgsend/ChatONMsgEntity;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1520
    sget-object v5, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;Lcom/sec/chaton/msgsend/k;)J
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/e/r;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/msgsend/ChatONMsgEntity;",
            ">;",
            "Lcom/sec/chaton/msgsend/k;",
            ")J"
        }
    .end annotation

    .prologue
    .line 1524
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1525
    :cond_0
    const-wide/16 v0, -0x1

    .line 1570
    :goto_0
    return-wide v0

    .line 1528
    :cond_1
    const-string v0, "doSendBulkMessage"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    new-instance v10, Lcom/sec/chaton/d/bd;

    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v10, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1540
    invoke-static {}, Lcom/sec/chaton/msgsend/a;->a()Lcom/sec/chaton/msgsend/a;

    move-result-object v8

    .line 1541
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1542
    invoke-virtual {v8, v0}, Lcom/sec/chaton/msgsend/a;->a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/a;

    goto :goto_1

    .line 1545
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v2

    .line 1546
    new-instance v0, Lcom/sec/chaton/d/a/ds;

    invoke-virtual {v10}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/d/a/ds;-><init>(Landroid/os/Handler;JLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/k;)V

    .line 1547
    sget-object v1, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v10, v1, v0}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1549
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v10}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1552
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    invoke-static {v0, v8, v1}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/aa;)I

    .line 1568
    invoke-virtual {v10}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1570
    invoke-virtual {v10}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 19

    .prologue
    .line 2139
    .line 2141
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "geo"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "mixed"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2142
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "geo\n\n\n\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2149
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v15

    .line 2151
    const/4 v8, 0x0

    .line 2152
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p9 .. p9}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2153
    const/4 v8, 0x6

    .line 2156
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    const/4 v6, 0x0

    move-object/from16 v0, p3

    array-length v7, v0

    invoke-static/range {v1 .. v8}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    .line 2161
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual/range {p9 .. p9}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2162
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    sget-object v10, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    const/4 v12, 0x0

    invoke-static {v8}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v13

    move-wide v8, v15

    move-object v11, v5

    move-object/from16 v14, p3

    invoke-static/range {v6 .. v14}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;)V

    move-wide v1, v15

    .line 2167
    :goto_1
    return-wide v1

    .line 2145
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mixed\ngeo\n\n\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    move-object/from16 v6, p0

    move-wide v7, v15

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move/from16 v12, p4

    move-object/from16 v13, p5

    move-wide/from16 v14, p6

    move-object/from16 v16, p8

    move-object/from16 v17, v5

    move-object/from16 v18, p9

    .line 2167
    invoke-virtual/range {v6 .. v18}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    goto :goto_1

    :cond_3
    move-object/from16 v5, p5

    goto/16 :goto_0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)J
    .locals 11

    .prologue
    .line 3037
    sget-object v10, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 15

    .prologue
    .line 3048
    .line 3049
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v12, p9

    .line 3052
    :goto_0
    move-object/from16 v0, p5

    invoke-static {v0, v12}, Lcom/sec/chaton/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 3053
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v13

    .line 3055
    const/4 v9, 0x0

    .line 3056
    invoke-virtual/range {p10 .. p10}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3057
    const/4 v9, 0x6

    .line 3060
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p5

    move-object/from16 v1, p8

    invoke-static {v0, v1}, Lcom/sec/chaton/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    array-length v8, v0

    move-object/from16 v7, p9

    invoke-static/range {v2 .. v9}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    .line 3065
    invoke-virtual/range {p10 .. p10}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3066
    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    sget-object v6, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    const/4 v8, 0x0

    invoke-static {v9}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v9

    move-wide v4, v13

    move-object v7, v10

    move-object/from16 v10, p3

    invoke-static/range {v2 .. v10}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;)V

    move-wide v2, v13

    .line 3070
    :goto_1
    return-wide v2

    :cond_1
    move-object v2, p0

    move-wide v3, v13

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    move-object/from16 v13, p10

    invoke-virtual/range {v2 .. v13}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v2

    goto :goto_1

    :cond_2
    move-object/from16 v12, p8

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 10

    .prologue
    .line 2098
    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v9, p6

    invoke-virtual/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 2109
    sget-object v7, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 10

    .prologue
    .line 2121
    const-wide/16 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 1579
    sget-object v7, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 11

    .prologue
    .line 1614
    .line 1615
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object/from16 v8, p6

    .line 1618
    :goto_0
    const-string v0, "requestSendMessage"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1620
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v9

    .line 1621
    const/4 v7, 0x0

    .line 1622
    invoke-virtual/range {p9 .. p9}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623
    const/4 v7, 0x6

    .line 1625
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    array-length v6, p4

    move-object v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v0 .. v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    .line 1632
    invoke-virtual/range {p9 .. p9}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    const/4 v6, 0x0

    invoke-static {v7}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v7

    move-wide v2, v9

    move-object v4, p1

    move-object v5, v8

    move-object v8, p4

    invoke-static/range {v0 .. v8}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;)V

    move-wide v0, v9

    .line 1637
    :goto_1
    return-wide v0

    :cond_1
    move-object v0, p0

    move-wide v1, v9

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, v8

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    goto :goto_1

    :cond_2
    move-object/from16 v8, p5

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 10

    .prologue
    .line 1598
    const-wide/16 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 3206
    new-instance v7, Lcom/sec/chaton/d/bd;

    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v7, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 3207
    new-instance v0, Lcom/sec/chaton/d/a/dd;

    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v6, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/d/a/dd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    sget-object v1, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v7, v1, v0}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3209
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3210
    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3212
    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J
    .locals 10

    .prologue
    .line 2482
    sget-object v9, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 12

    .prologue
    .line 2488
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v11, p9

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 12

    .prologue
    .line 2499
    sget-object v11, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 24

    .prologue
    .line 2505
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v13

    .line 2509
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2510
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/chaton/j/c/i;->a(Ljava/io/File;Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v2

    .line 2511
    if-nez v2, :cond_0

    .line 2513
    const-string v2, "MessageControl"

    const-string v3, "requestSendFile(), FILE Extention : forced to be \'%s\' with parameter"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-static {v2, v3, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v2, p2

    .line 2517
    :cond_0
    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/sec/chaton/util/r;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/32 v7, 0xa00000

    cmp-long v3, v5, v7

    if-lez v3, :cond_2

    .line 2518
    :cond_1
    const-string v2, "MessageControl"

    const-string v3, "requestSendFile(), not support ext. or excess limited size"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2519
    const-wide/16 v2, -0x1

    .line 2609
    :goto_0
    return-wide v2

    .line 2523
    :cond_2
    const/4 v6, 0x0

    .line 2526
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2527
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2528
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2529
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 2530
    const-string v3, ""

    .line 2531
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-le v7, v8, :cond_4

    .line 2532
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v7, "."

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2538
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2542
    :cond_3
    if-eqz v6, :cond_b

    .line 2544
    new-instance v12, Ljava/io/File;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2548
    :goto_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2549
    const-string v2, "MessageControl"

    const-string v3, "requestSendFile(), invalid Local File URI"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2550
    const-wide/16 v2, -0x1

    goto/16 :goto_0

    .line 2534
    :cond_4
    if-eqz v2, :cond_c

    .line 2535
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "."

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2555
    :cond_5
    const-string v8, "\n\n\n\n\n\n"

    .line 2556
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2557
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mixed\n\n\n\n\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2561
    :cond_6
    const/4 v9, 0x0

    .line 2562
    invoke-virtual/range {p11 .. p11}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2563
    const/4 v9, 0x6

    .line 2566
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object/from16 v0, p6

    array-length v7, v0

    move-object/from16 v5, p3

    move-object/from16 v10, p10

    invoke-static/range {v2 .. v10}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Landroid/net/Uri;

    .line 2579
    sget-object v2, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_a

    .line 2580
    invoke-static {v6}, Lcom/sec/chaton/util/r;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2585
    :goto_3
    sget-object v2, Lcom/sec/chaton/d/s;->b:[I

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2606
    :goto_4
    invoke-virtual/range {p11 .. p11}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    move-object/from16 v9, p0

    move-wide v10, v13

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move/from16 v18, p7

    move-object/from16 v20, p9

    move-object/from16 v21, p10

    move-object/from16 v22, v8

    .line 2607
    invoke-direct/range {v9 .. v22}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    goto/16 :goto_0

    .line 2591
    :pswitch_0
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2592
    const-wide/16 v2, -0x1

    goto/16 :goto_0

    .line 2594
    :cond_8
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-static {v2, v3, v4, v0}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_4

    .line 2609
    :cond_9
    const/16 v22, 0x1

    move-object/from16 v9, p0

    move-wide v10, v13

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move/from16 v18, p7

    move-object/from16 v20, p9

    move-object/from16 v21, p10

    move-object/from16 v23, p11

    invoke-virtual/range {v9 .. v23}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)J

    move-result-wide v2

    goto/16 :goto_0

    :cond_a
    move-object/from16 v19, p8

    goto :goto_3

    :cond_b
    move-object v6, v4

    move-object/from16 v12, p1

    goto/16 :goto_2

    :cond_c
    move-object v2, v3

    goto/16 :goto_1

    .line 2585
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 1262
    sget-object v7, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J
    .locals 12

    .prologue
    .line 1268
    sget-object v11, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J
    .locals 13

    .prologue
    .line 1293
    const-string v2, "initChatRoom"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    const-wide/16 v2, -0x1

    cmp-long v2, v2, p9

    if-nez v2, :cond_1

    .line 1296
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide p9

    .line 1297
    const/4 v9, 0x0

    .line 1298
    invoke-virtual/range {p11 .. p11}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1299
    const/4 v9, 0x6

    .line 1302
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p9 .. p10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    array-length v8, v0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v2 .. v9}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    :cond_1
    move-wide/from16 v5, p9

    .line 1306
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v10, p6

    .line 1310
    :goto_0
    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_4

    .line 1311
    iget-object v4, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 1312
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_3

    .line 1313
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 1315
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/chaton/e/w;->a()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual {v3, v5, v6}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p11

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;

    .line 1321
    const-wide/16 v5, -0x1

    cmp-long v3, p7, v5

    if-eqz v3, :cond_2

    .line 1322
    move-wide/from16 v0, p7

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 1327
    :goto_1
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 1328
    iget-object v3, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1330
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 1349
    :goto_2
    return-wide v2

    .line 1324
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_1

    .line 1333
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1332
    :cond_3
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 1333
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1336
    :cond_4
    const-wide/16 v2, -0x1

    cmp-long v2, p7, v2

    if-eqz v2, :cond_5

    .line 1337
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p7

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object v12, v2

    .line 1341
    :goto_3
    new-instance v2, Lcom/sec/chaton/d/a/cy;

    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    move-object v4, p1

    move-object v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v11, p11

    invoke-direct/range {v2 .. v11}, Lcom/sec/chaton/d/a/cy;-><init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 1344
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v12, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1346
    iget-object v2, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347
    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1349
    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto :goto_2

    .line 1339
    :cond_5
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object v12, v2

    goto :goto_3

    :cond_6
    move-object/from16 v10, p5

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J
    .locals 12

    .prologue
    .line 1286
    const-wide/16 v7, -0x1

    const-wide/16 v9, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v11, p7

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Z)J
    .locals 8

    .prologue
    .line 3217
    new-instance v7, Lcom/sec/chaton/d/bd;

    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v7, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 3218
    new-instance v0, Lcom/sec/chaton/d/a/y;

    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v6, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/d/a/y;-><init>(Landroid/os/Handler;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)V

    .line 3219
    sget-object v1, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v7, v1, v0}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3220
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3221
    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3222
    invoke-virtual {v7}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;)J
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 3263
    new-instance v1, Lcom/sec/chaton/d/bd;

    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v1, v0, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 3264
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3265
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3266
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 3267
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    .line 3268
    if-lez v0, :cond_0

    sget v5, Lcom/sec/chaton/d/o;->x:I

    rem-int v5, v0, v5

    if-nez v5, :cond_0

    .line 3269
    new-instance v5, Lcom/sec/chaton/d/a/q;

    invoke-virtual {v1}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v5, v6, v2, v3}, Lcom/sec/chaton/d/a/q;-><init>(Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3270
    sget-object v6, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v1, v6, v5}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3272
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3273
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 3276
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3277
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3280
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 3281
    new-instance v0, Lcom/sec/chaton/d/a/q;

    invoke-virtual {v1}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v0, v4, v2, v3}, Lcom/sec/chaton/d/a/q;-><init>(Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3282
    sget-object v4, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3284
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3285
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 3288
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3289
    invoke-virtual {v1}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3291
    invoke-virtual {v1}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/util/List;Lcom/sec/chaton/e/r;Ljava/lang/String;)J
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;",
            "Lcom/sec/chaton/e/r;",
            "Ljava/lang/String;",
            ")J"
        }
    .end annotation

    .prologue
    .line 1459
    new-instance v6, Lcom/sec/chaton/d/bd;

    iget-object v0, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v6, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1460
    new-instance v0, Lcom/sec/chaton/d/a/dn;

    invoke-virtual {v6}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/d/a/dn;-><init>(Ljava/util/List;Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    sget-object v1, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v6, v1, v0}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1462
    iget-object v0, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v6}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463
    invoke-virtual {v6}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1464
    invoke-virtual {v6}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 3415
    return-void
.end method

.method public a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 3419
    if-eqz p3, :cond_0

    .line 3421
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 3424
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3486
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 3429
    :cond_1
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 3431
    :sswitch_1
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bg;

    .line 3432
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeliveryChat From Server : Session ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3433
    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/a/bg;)V

    goto :goto_0

    .line 3437
    :sswitch_2
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/r;

    .line 3438
    const-string v1, "AnswerBack From Server"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3439
    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/a/r;)V

    goto :goto_0

    .line 3449
    :sswitch_3
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/l;

    .line 3450
    const-string v1, "AnnounceChange From Server"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3451
    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/a/l;)V

    goto :goto_0

    .line 3455
    :sswitch_4
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cf;

    .line 3456
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ForwardOnlineMessage From Server : Session ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cf;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3457
    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/a/cf;)V

    goto/16 :goto_0

    .line 3461
    :sswitch_5
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cl;

    .line 3462
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ForwardStoredMessage From Server : Session ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cl;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3463
    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/a/cl;)V

    goto/16 :goto_0

    .line 3429
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0xa -> :sswitch_2
        0xe -> :sswitch_0
        0x10 -> :sswitch_3
        0x22 -> :sswitch_4
        0x24 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 248
    iput-wide p1, p0, Lcom/sec/chaton/d/o;->w:J

    .line 249
    return-void
.end method

.method public a(Lcom/sec/chaton/a/bg;)V
    .locals 4

    .prologue
    .line 1409
    const-string v0, "makeDeliveryChatReply"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1414
    new-instance v1, Lcom/sec/chaton/d/a/aq;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1, p0}, Lcom/sec/chaton/d/a/aq;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/bg;Lcom/sec/chaton/d/o;)V

    .line 1415
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1416
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1429
    return-void
.end method

.method public a(Lcom/sec/chaton/a/cf;)V
    .locals 4

    .prologue
    .line 1432
    const-string v0, "makeForwardOnlineMessageReply"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1435
    new-instance v1, Lcom/sec/chaton/d/a/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1, p0}, Lcom/sec/chaton/d/a/ba;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/cf;Lcom/sec/chaton/d/o;)V

    .line 1436
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1437
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1438
    return-void
.end method

.method public a(Lcom/sec/chaton/a/cl;)V
    .locals 4

    .prologue
    .line 1441
    const-string v0, "makeForwardStoredMessageReply"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1444
    new-instance v1, Lcom/sec/chaton/d/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1, p0}, Lcom/sec/chaton/d/a/bc;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/cl;Lcom/sec/chaton/d/o;)V

    .line 1445
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1446
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1447
    return-void
.end method

.method public a(Lcom/sec/chaton/a/l;)V
    .locals 4

    .prologue
    .line 1490
    if-nez p1, :cond_0

    .line 1498
    :goto_0
    return-void

    .line 1494
    :cond_0
    new-instance v0, Lcom/sec/chaton/d/bd;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1495
    new-instance v1, Lcom/sec/chaton/d/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v3}, Lcom/sec/chaton/d/a/o;-><init>(Landroid/os/Handler;Lcom/sec/chaton/a/l;Ljava/lang/String;)V

    .line 1496
    sget-object v2, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1497
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/r;)V
    .locals 2

    .prologue
    .line 3193
    new-instance v0, Lcom/sec/chaton/d/a/p;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->g:Landroid/os/Handler;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/d/a/p;-><init>(Landroid/os/Handler;Lcom/sec/chaton/a/r;)V

    .line 3194
    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a/p;->a(Lcom/sec/chaton/j/ak;)V

    .line 3195
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    iget-object v1, p0, Lcom/sec/chaton/d/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 211
    :try_start_0
    iput-object p1, p0, Lcom/sec/chaton/d/o;->t:Ljava/lang/String;

    .line 212
    monitor-exit v1

    .line 213
    return-void

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 3380
    iput-boolean p1, p0, Lcom/sec/chaton/d/o;->k:Z

    .line 3381
    return-void
.end method

.method public a(Landroid/os/Handler;)Z
    .locals 3

    .prologue
    .line 157
    iget-object v1, p0, Lcom/sec/chaton/d/o;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/o;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addHandler : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/o;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    .line 164
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 9

    .prologue
    .line 1366
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z
    .locals 13

    .prologue
    .line 1373
    if-nez p8, :cond_1

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v4}, Lcom/sec/chaton/j/af;->b(Lcom/sec/chaton/j/ak;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1374
    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne p1, v4, :cond_0

    .line 1375
    iget-object v4, p0, Lcom/sec/chaton/d/o;->z:Landroid/os/Handler;

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    move-wide/from16 v2, p6

    invoke-static {v4, v0, v1, v2, v3}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1377
    :cond_0
    const/4 v4, 0x0

    .line 1405
    :goto_0
    return v4

    .line 1384
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 1391
    new-instance v12, Lcom/sec/chaton/d/bd;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v12, v4, v5}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    .line 1392
    new-instance v4, Lcom/sec/chaton/d/a/i;

    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    move-object v6, p1

    move-object/from16 v7, p5

    move-object v8, p2

    move-object/from16 v11, p4

    invoke-direct/range {v4 .. v11}, Lcom/sec/chaton/d/a/i;-><init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1393
    sget-object v5, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v12, v5, v4}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 1395
    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne p1, v4, :cond_2

    .line 1396
    iget-object v4, p0, Lcom/sec/chaton/d/o;->z:Landroid/os/Handler;

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    move-wide/from16 v2, p6

    invoke-static {v4, v0, v1, v2, v3}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1398
    :cond_2
    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1399
    iget-object v4, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v12}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;[Ljava/lang/String;)Z
    .locals 10

    .prologue
    .line 1680
    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->f()Lcom/sec/chaton/e/w;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->k()I

    move-result v8

    move-object v0, p0

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/util/ArrayList;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/e/w;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2886
    sget-object v8, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/util/ArrayList;Lcom/sec/chaton/msgsend/k;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/util/ArrayList;Lcom/sec/chaton/msgsend/k;)Z
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/e/w;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/chaton/msgsend/k;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2892
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2893
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/Application;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2894
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 2895
    const-string v1, "\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2896
    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 2898
    array-length v1, v2

    .line 2899
    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    .line 2901
    const-string v2, "MessageControl"

    const-string v3, "requestSendMediaChatForward(), token length is too short (%d)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2902
    const/4 v1, 0x0

    .line 2976
    :goto_0
    return v1

    .line 2909
    :cond_0
    const/4 v3, 0x5

    if-le v1, v3, :cond_1

    const/4 v3, 0x5

    aget-object v11, v2, v3

    .line 2910
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 2914
    if-eqz p5, :cond_b

    .line 2915
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2916
    const/4 v4, 0x5

    if-ne v1, v4, :cond_9

    .line 2917
    const/4 v1, 0x0

    :goto_2
    const/4 v4, 0x5

    if-ge v1, v4, :cond_2

    .line 2918
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v2, v1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2917
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2909
    :cond_1
    const/4 v11, 0x0

    goto :goto_1

    .line 2920
    :cond_2
    const-string v1, "\n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2927
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2928
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2931
    :goto_3
    const/4 v8, 0x0

    .line 2932
    invoke-virtual/range {p8 .. p8}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2933
    const/4 v8, 0x6

    .line 2935
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v9, 0x0

    move-object/from16 v4, p6

    invoke-static/range {v1 .. v9}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Landroid/net/Uri;

    .line 2937
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v14}, Lcom/sec/chaton/e/a/q;->d(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 2940
    sget-object v1, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_6

    .line 2943
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v1, v2, v14, v11}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    .line 2947
    :cond_6
    invoke-virtual/range {p8 .. p8}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    .line 2948
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2949
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    const/4 v12, 0x1

    move-object/from16 v6, p6

    invoke-static/range {v2 .. v13}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 2952
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/Integer;)V

    .line 2954
    sget-object v1, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p6

    if-ne v0, v1, :cond_8

    .line 2957
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3, v11}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 2976
    :cond_8
    :goto_4
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 2923
    :cond_9
    const/4 v1, 0x0

    :goto_5
    const/4 v4, 0x6

    if-ge v1, v4, :cond_3

    .line 2924
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v2, v1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2923
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2961
    :cond_a
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v12, p0

    move-object/from16 v17, v10

    move/from16 v18, p2

    move-object/from16 v19, v11

    move-object/from16 v20, p1

    move-object/from16 v21, p4

    move-object/from16 v23, p6

    move-object/from16 v24, p5

    move-object/from16 v25, p8

    invoke-virtual/range {v12 .. v25}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    goto :goto_4

    :cond_b
    move-object/from16 v7, p3

    goto/16 :goto_3
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 3295
    new-instance v1, Lcom/sec/chaton/j/an;

    invoke-direct {v1}, Lcom/sec/chaton/j/an;-><init>()V

    .line 3296
    iput-object p1, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    .line 3306
    const/4 v0, 0x0

    .line 3315
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/sec/chaton/j/an;->a(Ljava/lang/String;)Lcom/sec/chaton/j/an;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/chaton/j/an;->b(Ljava/lang/String;)Lcom/sec/chaton/j/an;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/chaton/j/an;->a(I)Lcom/sec/chaton/j/an;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/j/an;->a(Lcom/sec/chaton/util/a;)Lcom/sec/chaton/j/an;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3322
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Lcom/sec/chaton/j/an;->a()Lcom/sec/chaton/j/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    .line 3325
    invoke-virtual {p0}, Lcom/sec/chaton/d/o;->h()V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3327
    const/4 v0, 0x1

    .line 3333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatRoom Info : InboxNO = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SERVER = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v0

    .line 3317
    :catch_0
    move-exception v0

    .line 3318
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3328
    :catch_1
    move-exception v0

    .line 3329
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TCP Context buiilder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3331
    const/4 v0, 0x0

    .line 3333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatRoom Info : InboxNO = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SERVER = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatRoom Info : InboxNO = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SERVER = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method public b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    .prologue
    .line 2469
    const-wide/16 v7, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)J
    .locals 12

    .prologue
    .line 2473
    iget-object v3, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v9, 0x1

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v10, p7

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZJ)J

    move-result-wide v0

    return-wide v0

    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J
    .locals 14

    .prologue
    .line 3147
    if-eqz p6, :cond_2

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_2

    .line 3148
    iget-object v4, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 3149
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    if-eqz v2, :cond_1

    .line 3150
    new-instance v2, Lcom/sec/chaton/chat/background/i;

    invoke-direct {v2}, Lcom/sec/chaton/chat/background/i;-><init>()V

    .line 3152
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    invoke-virtual {v5}, Lcom/sec/chaton/e/w;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-wide v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    move-wide v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    .line 3154
    const-wide/16 v5, -0x1

    cmp-long v3, p8, v5

    if-eqz v3, :cond_0

    .line 3155
    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    .line 3160
    :goto_0
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/i;->a()Lcom/sec/chaton/chat/background/h;

    move-result-object v2

    .line 3161
    iget-object v3, p0, Lcom/sec/chaton/d/o;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3163
    invoke-virtual {v2}, Lcom/sec/chaton/chat/background/h;->i()J

    move-result-wide v2

    monitor-exit v4

    .line 3186
    :goto_1
    return-wide v2

    .line 3157
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/chat/background/i;->c(J)Lcom/sec/chaton/chat/background/i;

    goto :goto_0

    .line 3166
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 3165
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/chaton/d/o;->r:Z

    .line 3166
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3169
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_3

    .line 3170
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    move-wide/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V

    move-object v13, v2

    .line 3175
    :goto_2
    new-instance v2, Lcom/sec/chaton/d/a/n;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->b()Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    move-wide v5, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, Lcom/sec/chaton/d/a/n;-><init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 3178
    sget-object v3, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    invoke-virtual {v13, v3, v2}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z

    .line 3180
    iget-object v2, p0, Lcom/sec/chaton/d/o;->p:Ljava/util/Map;

    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3182
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->e(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 3184
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->d()Z

    .line 3186
    invoke-virtual {v13}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v2

    goto :goto_1

    .line 3172
    :cond_3
    new-instance v2, Lcom/sec/chaton/d/bd;

    iget-object v3, p0, Lcom/sec/chaton/d/o;->A:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/d/bd;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V

    move-object v13, v2

    goto :goto_2
.end method

.method public b(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    .prologue
    .line 2017
    const-wide/16 v6, -0x1

    sget-object v8, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 13

    .prologue
    .line 2029
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p3, :cond_2

    .line 2030
    :cond_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 2031
    const-string v1, "requestSendLiveShare(), chatMsg is null or recipients is null"

    const-string v2, "MessageControl"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033
    :cond_1
    const-wide/16 v1, -0x1

    .line 2047
    :goto_0
    return-wide v1

    .line 2036
    :cond_2
    invoke-static/range {p4 .. p4}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2037
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2038
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestSendLiveShare(), failed in converting chat msg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MessageControl"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    const-wide/16 v1, -0x1

    goto :goto_0

    .line 2042
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual/range {p8 .. p8}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2044
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v3, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v5, p3

    move-wide/from16 v8, p6

    move-object/from16 v12, p8

    invoke-virtual/range {v1 .. v12}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    goto :goto_0

    .line 2047
    :cond_5
    sget-object v2, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    invoke-virtual/range {v1 .. v10}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    goto :goto_0
.end method

.method public b(J)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 252
    iget-wide v0, p0, Lcom/sec/chaton/d/o;->w:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 257
    iput-wide p1, p0, Lcom/sec/chaton/d/o;->w:J

    goto :goto_0
.end method

.method public b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1469
    const-string v0, "sendDeliveryChatReply"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/es;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/es;

    move-result-object v0

    .line 1473
    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v1}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1474
    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->a()V

    .line 1486
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1482
    new-instance v0, Lcom/sec/chaton/d/a/ap;

    iget-object v1, p0, Lcom/sec/chaton/d/o;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/sec/chaton/d/a/ap;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 1484
    iget-object v1, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a/ap;->a(Lcom/sec/chaton/j/ak;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 3388
    iput-boolean p1, p0, Lcom/sec/chaton/d/o;->n:Z

    .line 3389
    return-void
.end method

.method public b(Landroid/os/Handler;)Z
    .locals 2

    .prologue
    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "removeHandler : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/d/o;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/o;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 224
    iget-object v1, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v1

    .line 225
    if-eqz v1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 229
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 231
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    .prologue
    .line 2057
    const-wide/16 v6, -0x1

    sget-object v8, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J
    .locals 13

    .prologue
    .line 2063
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p3, :cond_2

    .line 2064
    :cond_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 2065
    const-string v1, "requestSendLiveRecommend(), chatMsg is null or recipients is null"

    const-string v2, "MessageControl"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    :cond_1
    const-wide/16 v1, -0x1

    .line 2081
    :goto_0
    return-wide v1

    .line 2070
    :cond_2
    invoke-static/range {p4 .. p4}, Lcom/sec/chaton/specialbuddy/g;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2071
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2072
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestSendLiveRecommend(), failed in converting chat msg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MessageControl"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2073
    const-wide/16 v1, -0x1

    goto :goto_0

    .line 2076
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual/range {p8 .. p8}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2078
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    sget-object v3, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v5, p3

    move-wide/from16 v8, p6

    move-object/from16 v12, p8

    invoke-virtual/range {v1 .. v12}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    goto :goto_0

    .line 2081
    :cond_5
    sget-object v2, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    invoke-virtual/range {v1 .. v10}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/msgsend/k;)J

    move-result-wide v1

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/chaton/d/o;->t:Ljava/lang/String;

    return-object v0
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 3341
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_0

    .line 3342
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/j/ak;->a(J)V

    .line 3345
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/chaton/d/o;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    iput-object p1, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    .line 240
    monitor-exit v1

    .line 241
    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    return-object v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 244
    iget-wide v0, p0, Lcom/sec/chaton/d/o;->w:J

    return-wide v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sec/chaton/d/o;->l:Ljava/lang/String;

    .line 3398
    return-void
.end method

.method public f()Lcom/sec/chaton/j/ak;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    return-object v0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 1245
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1246
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1247
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0x24

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1248
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1249
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1250
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1251
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1252
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    const/16 v1, 0x270f

    iget-object v2, p0, Lcom/sec/chaton/d/o;->a:Lcom/sec/chaton/j/q;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ak;->a(ILcom/sec/chaton/j/q;)V

    .line 1254
    return-void
.end method

.method public i()V
    .locals 5

    .prologue
    .line 3235
    const-string v0, "MessageControl"

    const-string v1, "------------------- Message Control CLEARED (%s ) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ---------------------"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3238
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_0

    .line 3239
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->e(Lcom/sec/chaton/j/ak;)V

    .line 3240
    const-class v1, Lcom/sec/chaton/d/o;

    monitor-enter v1

    .line 3241
    :try_start_0
    sget-object v0, Lcom/sec/chaton/d/o;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    .line 3244
    :cond_0
    return-void

    .line 3241
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 3351
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/es;->a(Ljava/lang/String;)V

    .line 3356
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3357
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->d(Lcom/sec/chaton/j/ak;)V

    .line 3360
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 3365
    iget-object v0, p0, Lcom/sec/chaton/d/o;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/es;->a(Ljava/lang/String;)V

    .line 3366
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_0

    .line 3367
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->c(Lcom/sec/chaton/j/ak;)V

    .line 3369
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 3373
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    if-eqz v0, :cond_0

    .line 3374
    iget-object v0, p0, Lcom/sec/chaton/d/o;->j:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    .line 3376
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 3384
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->k:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 3392
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->n:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3401
    iget-object v0, p0, Lcom/sec/chaton/d/o;->l:Ljava/lang/String;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 3407
    iget-object v1, p0, Lcom/sec/chaton/d/o;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 3408
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/chaton/d/o;->r:Z

    monitor-exit v1

    return v0

    .line 3409
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public q()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3498
    iget-object v2, p0, Lcom/sec/chaton/d/o;->u:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_1

    .line 3506
    :cond_0
    :goto_0
    return v0

    .line 3502
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/d/o;->s:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v2

    if-eq v2, v1, :cond_0

    move v0, v1

    .line 3506
    goto :goto_0
.end method
