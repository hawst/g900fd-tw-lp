.class public final enum Lcom/sec/chaton/d/bh;
.super Ljava/lang/Enum;
.source "TaskContainer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/d/bh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/d/bh;

.field public static final enum b:Lcom/sec/chaton/d/bh;

.field private static final synthetic d:[Lcom/sec/chaton/d/bh;


# instance fields
.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/chaton/d/bh;

    const-string v1, "HttpTask"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/chaton/d/bh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/bh;->a:Lcom/sec/chaton/d/bh;

    .line 25
    new-instance v0, Lcom/sec/chaton/d/bh;

    const-string v1, "NetTask"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/chaton/d/bh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/d/bh;

    sget-object v1, Lcom/sec/chaton/d/bh;->a:Lcom/sec/chaton/d/bh;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/d/bh;->b:Lcom/sec/chaton/d/bh;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/d/bh;->d:[Lcom/sec/chaton/d/bh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/sec/chaton/d/bh;->c:I

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/d/bh;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/chaton/d/bh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bh;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/d/bh;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/chaton/d/bh;->d:[Lcom/sec/chaton/d/bh;

    invoke-virtual {v0}, [Lcom/sec/chaton/d/bh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/d/bh;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/chaton/d/bh;->c:I

    return v0
.end method
