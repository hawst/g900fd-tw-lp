.class public Lcom/sec/chaton/d/a/eg;
.super Lcom/sec/chaton/d/a/a;
.source "UnBlockTask.java"


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 31
    iput-object p3, p0, Lcom/sec/chaton/d/a/eg;->b:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 64
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 65
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v5}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0, v8}, Lcom/sec/chaton/d/h;->a(Z)V

    .line 66
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v5}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    .line 69
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "participants_country_code"

    aput-object v3, v2, v6

    const-string v3, "participants_is_auth"

    aput-object v3, v2, v8

    const-string v3, "participants_buddy_no=?"

    new-array v4, v8, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/eg;->b:Ljava/lang/String;

    aput-object v7, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 72
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 73
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 74
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 76
    const/4 v1, -0x1

    .line 77
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v6

    .line 81
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 82
    iget-object v4, p0, Lcom/sec/chaton/d/a/eg;->b:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 86
    :cond_1
    if-eqz v2, :cond_2

    .line 87
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_2
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method
