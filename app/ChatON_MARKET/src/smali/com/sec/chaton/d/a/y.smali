.class public Lcom/sec/chaton/d/a/y;
.super Lcom/sec/chaton/d/a/e;
.source "ChangeMemberTask.java"


# instance fields
.field private h:Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:Landroid/content/ContentResolver;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/y;->f:J

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/d/a/y;->h:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/sec/chaton/d/a/y;->i:[Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    .line 52
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    .line 53
    iput-boolean p5, p0, Lcom/sec/chaton/d/a/y;->m:Z

    .line 54
    iput-object p6, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 122
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/e;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 126
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    .line 127
    const/16 v0, 0x1f

    iput v0, v6, Landroid/os/Message;->what:I

    .line 129
    if-eqz p3, :cond_13

    .line 130
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 131
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/sec/chaton/a/am;

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ch@t[ChangeMemberReply]UID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ResultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ResultMessage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", InBoxNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ReceivedTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->h()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MsgID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->d()I

    move-result v7

    .line 139
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/y;->m:Z

    if-eqz v0, :cond_e

    .line 140
    const/16 v0, 0x3e8

    if-eq v7, v0, :cond_0

    const/16 v0, 0x3e9

    if-eq v7, v0, :cond_0

    const/16 v0, 0xbbd

    if-ne v7, v0, :cond_6

    .line 144
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 145
    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    array-length v4, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 146
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    const/16 v0, 0x3e9

    if-ne v7, v0, :cond_4

    .line 149
    const-string v0, "\\[.+?\\]"

    .line 151
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 152
    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 156
    :cond_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 158
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 160
    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v8, v4, v0

    .line 161
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 162
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 160
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 172
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 175
    iget-object v4, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 183
    const-string v4, "%d,%s,%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    invoke-virtual {v9}, Lcom/sec/chaton/e/aj;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object v0, v5, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    invoke-static {v9, v0}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 190
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->h()J

    move-result-wide v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 202
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 256
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->b:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 273
    :goto_4
    return-void

    .line 204
    :cond_6
    const/16 v0, 0x1b5b

    if-ne v7, v0, :cond_8

    .line 205
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    .line 238
    :cond_7
    :goto_5
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_3

    .line 206
    :cond_8
    const/16 v0, 0x1b5e

    if-ne v7, v0, :cond_7

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "update invite list : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/sec/chaton/a/ea;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dm;

    .line 211
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->d()Ljava/lang/String;

    move-result-object v3

    .line 212
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->f()Ljava/lang/String;

    move-result-object v4

    .line 213
    const-string v0, "(old:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ",new:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "),"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const/4 v0, 0x0

    :goto_6
    iget-object v5, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_9

    .line 216
    iget-object v5, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 217
    iget-object v5, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    aput-object v4, v5, v0

    .line 215
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 221
    :cond_b
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 223
    iget-object v3, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v4, :cond_d

    aget-object v5, v3, v0

    .line 224
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 225
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 228
    :cond_d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    .line 233
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "7006(NON_WEB_USER_DETECTED) - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChangeMemberTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 241
    :cond_e
    const/16 v0, 0x3e8

    if-eq v7, v0, :cond_f

    const/16 v0, 0x3e9

    if-eq v7, v0, :cond_f

    const/16 v0, 0xbbd

    if-ne v7, v0, :cond_11

    .line 242
    :cond_f
    const/4 v0, 0x0

    :goto_8
    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_10

    .line 243
    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    const-string v3, "%d,%s,%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v8, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v8}, Lcom/sec/chaton/e/aj;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    iget-object v8, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    aget-object v8, v8, v0

    aput-object v8, v4, v5

    const/4 v5, 0x2

    iget-object v8, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v9, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    aget-object v9, v9, v0

    invoke-static {v8, v9}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v8, ""

    invoke-virtual {v4, v5, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/e/a/q;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 246
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 247
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_3

    .line 249
    :cond_11
    const/16 v0, 0x1b5b

    if-ne v7, v0, :cond_12

    .line 250
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    .line 252
    :cond_12
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_3

    .line 260
    :cond_13
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/y;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0xb

    if-eq p1, v0, :cond_14

    const/16 v0, 0x16

    if-eq p1, v0, :cond_14

    const/4 v0, 0x3

    if-ne p1, v0, :cond_16

    .line 262
    :cond_14
    const/16 v0, 0x18

    if-eq p1, v0, :cond_15

    const/16 v0, 0x17

    if-eq p1, v0, :cond_15

    const/16 v0, 0x15

    if-ne p1, v0, :cond_16

    .line 263
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->k:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 267
    :cond_16
    const-string v0, "Result NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->b:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-super {p0}, Lcom/sec/chaton/d/a/e;->b()Lcom/sec/chaton/j/ao;

    .line 60
    iget-wide v1, p0, Lcom/sec/chaton/d/a/y;->f:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 61
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/d/a/y;->f:J

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 67
    iput-object v1, p0, Lcom/sec/chaton/d/a/y;->h:Ljava/lang/String;

    .line 70
    :cond_1
    invoke-static {}, Lcom/sec/chaton/a/ap;->newBuilder()Lcom/sec/chaton/a/aq;

    move-result-object v1

    .line 71
    iget-wide v2, p0, Lcom/sec/chaton/d/a/y;->f:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/aq;->a(J)Lcom/sec/chaton/a/aq;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/a/bc;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/aq;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/a/y;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/aq;->a(Ljava/lang/String;)Lcom/sec/chaton/a/aq;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/aq;->b(Ljava/lang/String;)Lcom/sec/chaton/a/aq;

    .line 79
    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->i:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 80
    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->i:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/aq;->c(Ljava/lang/String;)Lcom/sec/chaton/a/aq;

    .line 84
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 85
    iget-object v3, p0, Lcom/sec/chaton/d/a/y;->j:[Ljava/lang/String;

    array-length v4, v3

    :goto_0
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 86
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/aq;->a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/aq;

    .line 90
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/y;->m:Z

    if-eqz v0, :cond_5

    .line 91
    sget-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/aq;

    .line 98
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v2

    .line 100
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    .line 101
    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/aq;->b(J)Lcom/sec/chaton/a/aq;

    .line 106
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ch@t[ChangeMemberRequest]UID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", InBoxNo : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/y;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", SessionID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChatType : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->g()Lcom/sec/chaton/a/bc;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", MsgID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChangingMembersCount : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->n()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChangingMembersList : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->m()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Sender : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", LastSessionMergeTime : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->o()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ReceiverCount : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->k()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ReceiversList : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->j()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChangeMemberType : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->l()Lcom/sec/chaton/a/as;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/sec/chaton/j/ap;

    invoke-direct {v0}, Lcom/sec/chaton/j/ap;-><init>()V

    .line 113
    invoke-virtual {v1}, Lcom/sec/chaton/a/aq;->d()Lcom/sec/chaton/a/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v1

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    .line 116
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0

    .line 93
    :cond_5
    sget-object v0, Lcom/sec/chaton/a/as;->b:Lcom/sec/chaton/a/as;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/aq;

    goto/16 :goto_1
.end method
