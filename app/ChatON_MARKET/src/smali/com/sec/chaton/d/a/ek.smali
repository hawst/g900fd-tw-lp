.class public Lcom/sec/chaton/d/a/ek;
.super Lcom/sec/chaton/d/a/a;
.source "UploadAddressTask.java"


# instance fields
.field b:Lcom/sec/chaton/m/c;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private r:I

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 61
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 62
    const-string v0, "+"

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/chaton/d/a/el;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/el;-><init>(Lcom/sec/chaton/d/a/ek;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;ILjava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/sec/chaton/j/h;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 61
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 62
    const-string v0, "+"

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/chaton/d/a/el;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/el;-><init>(Lcom/sec/chaton/d/a/ek;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    .line 114
    iput p3, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 115
    iput-object p4, p0, Lcom/sec/chaton/d/a/ek;->n:Ljava/util/ArrayList;

    .line 116
    iput-object p5, p0, Lcom/sec/chaton/d/a/ek;->o:Ljava/util/HashMap;

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 61
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 62
    const-string v0, "+"

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/chaton/d/a/el;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/el;-><init>(Lcom/sec/chaton/d/a/ek;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    .line 82
    iput-object p3, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 83
    iput-object p4, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 61
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 62
    const-string v0, "+"

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/chaton/d/a/el;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/el;-><init>(Lcom/sec/chaton/d/a/ek;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->h:Ljava/util/ArrayList;

    .line 95
    iput-object p3, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 96
    iput-object p4, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 97
    iput p6, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 99
    if-eqz p5, :cond_0

    move v6, v1

    .line 100
    :goto_0
    array-length v0, p5

    if-ge v6, v0, :cond_0

    .line 101
    iget-object v7, p0, Lcom/sec/chaton/d/a/ek;->h:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/m/d;

    aget-object v2, p5, v6

    const-string v4, ""

    const-string v5, ""

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 120
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 61
    iput v1, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 62
    const-string v0, "+"

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/chaton/d/a/el;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/el;-><init>(Lcom/sec/chaton/d/a/ek;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->p:Ljava/util/ArrayList;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/ek;->q:Ljava/util/ArrayList;

    .line 130
    if-eqz p4, :cond_0

    move v6, v1

    .line 131
    :goto_0
    array-length v0, p4

    if-ge v6, v0, :cond_0

    .line 132
    aget-object v0, p4, v6

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 133
    iget-object v7, p0, Lcom/sec/chaton/d/a/ek;->p:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/m/d;

    aget-object v2, p4, v6

    const-string v4, ""

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 137
    :cond_0
    if-eqz p5, :cond_1

    move v6, v1

    .line 138
    :goto_1
    array-length v0, p5

    if-ge v6, v0, :cond_1

    .line 139
    aget-object v0, p5, v6

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 140
    iget-object v7, p0, Lcom/sec/chaton/d/a/ek;->q:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/m/d;

    aget-object v2, p5, v6

    const-string v4, ""

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 144
    :cond_1
    iput-object p3, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    .line 145
    iput p6, p0, Lcom/sec/chaton/d/a/ek;->m:I

    .line 146
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 444
    const-string v6, ""

    .line 445
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "buddy_no"

    aput-object v4, v2, v7

    const/4 v4, 0x1

    const-string v5, "buddy_orginal_number"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "buddy_orginal_numbers"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 448
    if-eqz v2, :cond_5

    .line 451
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 453
    :cond_0
    const-string v0, "buddy_orginal_numbers"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 454
    const-string v0, "buddy_orginal_number"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 455
    const-string v0, "buddy_no"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 456
    if-eqz v1, :cond_3

    .line 457
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 458
    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    move v1, v7

    .line 459
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 460
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    .line 473
    if-eqz v2, :cond_1

    .line 474
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 478
    :cond_1
    :goto_2
    return-object v0

    .line 459
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 464
    :cond_3
    if-eqz v3, :cond_4

    .line 465
    :try_start_1
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 473
    if-eqz v2, :cond_1

    goto :goto_1

    .line 469
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 473
    if-eqz v2, :cond_5

    .line 474
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v0, v6

    .line 478
    goto :goto_2

    .line 470
    :catch_0
    move-exception v0

    .line 471
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 473
    if-eqz v2, :cond_5

    goto :goto_3

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_6

    .line 474
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_6
    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 483
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "|"

    invoke-direct {v1, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v2, ""

    .line 485
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 486
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 487
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    :cond_0
    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 504
    .line 506
    const-string v0, "[^\\+\\*\\#0-9]"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 405
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_4

    .line 406
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 411
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/sec/chaton/m/d;

    .line 412
    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v1

    .line 413
    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 415
    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const-string v3, "buddy_raw_contact_id = ? and buddy_no = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->b()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x1

    aput-object v8, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 421
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "afterRequest deletedPerson "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", num="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UploadAddressTask"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    iget-object v3, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Lcom/sec/chaton/m/d;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_push_name"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v9, "buddy_original_name"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v5, v9}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 429
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 431
    :cond_1
    const-string v1, "Start making local Contact"

    const-string v2, "UploadAddressTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 433
    const-string v1, "com.sec.chaton.provider"

    iget-object v2, p0, Lcom/sec/chaton/d/a/ek;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 435
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 436
    const-string v1, "com.android.contacts"

    iget-object v2, p0, Lcom/sec/chaton/d/a/ek;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 439
    :cond_3
    const-string v0, "End making local Contact"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_4
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x148

    const/16 v7, 0x30

    const/4 v3, 0x0

    .line 269
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    const-string v0, "Cotact Compare Start"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->b:Lcom/sec/chaton/m/c;

    invoke-static {v0}, Lcom/sec/chaton/m/a;->a(Lcom/sec/chaton/m/c;)V

    .line 272
    const-string v0, "Cotact Compare End"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 292
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0x8

    if-lt v2, v4, :cond_0

    .line 296
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 300
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v6, 0x40

    if-le v4, v6, :cond_2

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Number is too long : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UploadAddressTask"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 274
    :cond_1
    const-string v0, "User do not conduct contact sync"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 308
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v7, :cond_4

    .line 309
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/chaton/io/entry/inner/Address;->name:Ljava/lang/String;

    .line 316
    :cond_3
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 317
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 312
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/chaton/io/entry/inner/Address;->name:Ljava/lang/String;

    goto :goto_2

    .line 320
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 321
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 323
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Address;->name:Ljava/lang/String;

    .line 324
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    :cond_6
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    const/16 v1, 0x145

    if-ne v0, v1, :cond_7

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 329
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 330
    iget-object v3, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 331
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 332
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 334
    :cond_7
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    const/16 v1, 0x146

    if-ne v0, v1, :cond_8

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 336
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 337
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 338
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 339
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 341
    :cond_8
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    const/16 v1, 0x147

    if-ne v0, v1, :cond_9

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 343
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 344
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 345
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 346
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 348
    :cond_9
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    if-ne v0, v8, :cond_b

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->o:Ljava/util/HashMap;

    if-eqz v0, :cond_e

    move v2, v3

    .line 350
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 352
    if-eqz v0, :cond_a

    move v4, v3

    .line 353
    :goto_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v4, v1, :cond_a

    .line 354
    new-instance v6, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v6}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 355
    iget-object v1, p0, Lcom/sec/chaton/d/a/ek;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v6, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 356
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 357
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    .line 350
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 362
    :cond_b
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    const/16 v1, 0x149

    if-ne v0, v1, :cond_d

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 364
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 365
    iget-object v3, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 366
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 367
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 369
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/d/a/ek;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/m/d;

    .line 370
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Address;-><init>()V

    .line 371
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->l:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Address;->group:Ljava/lang/String;

    .line 372
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/d/a/ek;->s:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/m/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/Address;->value:Ljava/lang/String;

    .line 373
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 376
    :cond_d
    const-string v0, "Can\'t upload group information, groupMode is mandatory"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :cond_e
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/d/a/ek;->r:I

    .line 381
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_f

    iget v0, p0, Lcom/sec/chaton/d/a/ek;->m:I

    if-eq v0, v8, :cond_f

    .line 382
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/ek;->p()V

    .line 385
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sent Contact Count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    new-instance v0, Lcom/sec/chaton/util/af;

    invoke-direct {v0}, Lcom/sec/chaton/util/af;-><init>()V

    .line 388
    invoke-virtual {v0, v5}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 389
    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lcom/sec/chaton/d/a/ek;->r:I

    return v0
.end method
