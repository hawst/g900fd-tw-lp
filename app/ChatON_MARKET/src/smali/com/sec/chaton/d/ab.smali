.class Lcom/sec/chaton/d/ab;
.super Ljava/lang/Object;
.source "PublicPushControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/z;

.field private b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/d/z;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/chaton/d/ab;->a:Lcom/sec/chaton/d/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p2, p0, Lcom/sec/chaton/d/ab;->b:Landroid/os/Handler;

    .line 215
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/ab;->a:Lcom/sec/chaton/d/z;

    iget-object v0, v0, Lcom/sec/chaton/d/z;->b:Ljava/lang/Object;

    check-cast v0, Lcom/sec/spp/push/IPushClientService;

    const-string v1, "db9fac80131928e1"

    invoke-interface {v0, v1}, Lcom/sec/spp/push/IPushClientService;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/d/ab;->b:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 223
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 224
    const/16 v2, 0x3ec

    iput v2, v1, Landroid/os/Message;->what:I

    .line 225
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/d/ab;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/ab;->a:Lcom/sec/chaton/d/z;

    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/d/z;->c()V

    .line 244
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 231
    invoke-static {}, Lcom/sec/chaton/d/z;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/ab;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 235
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 236
    const/16 v1, 0x3ec

    iput v1, v0, Landroid/os/Message;->what:I

    .line 237
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 239
    iget-object v1, p0, Lcom/sec/chaton/d/ab;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/ab;->a:Lcom/sec/chaton/d/z;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/d/ab;->a:Lcom/sec/chaton/d/z;

    invoke-virtual {v1}, Lcom/sec/chaton/d/z;->c()V

    throw v0
.end method
