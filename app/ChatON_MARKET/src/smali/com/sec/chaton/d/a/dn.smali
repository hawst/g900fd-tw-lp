.class public Lcom/sec/chaton/d/a/dn;
.super Lcom/sec/chaton/d/a/e;
.source "ReadMessageTask.java"


# instance fields
.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/e/r;

.field private k:Landroid/content/ContentResolver;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 51
    iput-object p3, p0, Lcom/sec/chaton/d/a/dn;->h:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/sec/chaton/d/a/dn;->j:Lcom/sec/chaton/e/r;

    .line 54
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/dn;->k:Landroid/content/ContentResolver;

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/dn;->f:J

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;",
            "Landroid/os/Handler;",
            "Lcom/sec/chaton/e/r;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p2}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 60
    iput-object p4, p0, Lcom/sec/chaton/d/a/dn;->h:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/sec/chaton/d/a/dn;->j:Lcom/sec/chaton/e/r;

    .line 63
    iput-object p1, p0, Lcom/sec/chaton/d/a/dn;->l:Ljava/util/List;

    .line 64
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/dn;->k:Landroid/content/ContentResolver;

    .line 65
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/dn;->f:J

    .line 66
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 189
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/e;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 191
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 192
    const/16 v0, 0x26

    iput v0, v2, Landroid/os/Message;->what:I

    .line 194
    if-eqz p3, :cond_3

    .line 195
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 196
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ed;

    .line 197
    iget-object v1, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    const-string v3, "ReadMessageTask"

    invoke-static {v1, v0, v3}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/ed;Ljava/lang/String;)V

    .line 199
    const/4 v1, -0x1

    .line 201
    invoke-virtual {v0}, Lcom/sec/chaton/a/ed;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 202
    invoke-virtual {v0}, Lcom/sec/chaton/a/ed;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->d()I

    move-result v0

    .line 205
    :goto_0
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    .line 206
    new-instance v1, Lcom/sec/chaton/a/a/k;

    invoke-direct {v1, v7, v0}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 213
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message_sever_id IN ( "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 216
    const-string v3, "message_is_read"

    sget-object v4, Lcom/sec/chaton/e/x;->d:Lcom/sec/chaton/e/x;

    invoke-virtual {v4}, Lcom/sec/chaton/e/x;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 217
    iget-object v3, p0, Lcom/sec/chaton/d/a/dn;->k:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v1, v0, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 243
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->b:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 244
    :cond_1
    :goto_3
    return-void

    .line 219
    :cond_2
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v4, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    .line 222
    :cond_3
    if-ne p1, v7, :cond_4

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 224
    const-string v0, "do not request read message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 229
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/dn;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xb

    if-eq p1, v0, :cond_5

    const/16 v0, 0x16

    if-eq p1, v0, :cond_5

    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    .line 230
    :cond_5
    const/16 v0, 0x18

    if-eq p1, v0, :cond_6

    const/16 v0, 0x17

    if-eq p1, v0, :cond_6

    const/16 v0, 0x15

    if-ne p1, v0, :cond_7

    .line 233
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->k:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 238
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_8

    .line 239
    const-string v0, "Fail to send ReadMessage"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_8
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v4, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 70
    invoke-super {p0}, Lcom/sec/chaton/d/a/e;->b()Lcom/sec/chaton/j/ao;

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    iput-object v0, p0, Lcom/sec/chaton/d/a/dn;->h:Ljava/lang/String;

    .line 78
    :cond_0
    iget-wide v0, p0, Lcom/sec/chaton/d/a/dn;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 79
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/d/a/dn;->f:J

    .line 82
    :cond_1
    invoke-static {}, Lcom/sec/chaton/a/eg;->newBuilder()Lcom/sec/chaton/a/eh;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/d/a/dn;->f:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/eh;->a(J)Lcom/sec/chaton/a/eh;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eh;->b(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eh;->c(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    move-result-object v7

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v0

    .line 86
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 87
    invoke-virtual {v7, v0, v1}, Lcom/sec/chaton/a/eh;->b(J)Lcom/sec/chaton/a/eh;

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->j:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 91
    :cond_3
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/d/a/dn;->j:Lcom/sec/chaton/e/r;

    .line 92
    const-string v0, ""

    invoke-virtual {v7, v0}, Lcom/sec/chaton/a/eh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    .line 97
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->l:Ljava/util/List;

    if-nez v0, :cond_a

    .line 100
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 101
    const-string v0, "read message from db"

    const-string v1, "ReadMessageTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "message_inbox_no =? AND message_is_read != "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/x;->d:Lcom/sec/chaton/e/x;

    invoke-virtual {v1}, Lcom/sec/chaton/e/x;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != ? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v5, ""

    invoke-virtual {v1, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->k:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 116
    if-eqz v1, :cond_e

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 117
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 118
    const-string v0, "message_sender"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    const-string v2, "message_sever_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 120
    const-string v4, "message_time"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 121
    const-string v9, "message_content_type"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v9

    .line 122
    invoke-static {v9}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/dp;

    move-result-object v9

    .line 124
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v10

    const-string v11, "chaton_id"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/dn;->j:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v0

    .line 133
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    :goto_2
    :try_start_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_5

    .line 138
    const-string v2, "ReadMessageTask"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 141
    :cond_5
    if-eqz v1, :cond_6

    .line 142
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 164
    :cond_6
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_c

    .line 165
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 166
    const-string v0, "There is no message to read"

    const-string v1, "ReadMessageTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object v0, v6

    .line 184
    :goto_4
    return-object v0

    .line 94
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->h:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/sec/chaton/a/eh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    goto/16 :goto_0

    .line 141
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_5
    if-eqz v1, :cond_9

    .line 142
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_9
    throw v0

    .line 146
    :cond_a
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_b

    .line 147
    const-string v0, "read message from mItemList"

    const-string v1, "ReadMessageTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    .line 151
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->j()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->l()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->n()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v0

    .line 160
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 176
    :cond_c
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    .line 177
    invoke-virtual {v7, v0}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/eh;

    goto :goto_7

    .line 179
    :cond_d
    iput-object v8, p0, Lcom/sec/chaton/d/a/dn;->l:Ljava/util/List;

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/d/a/dn;->i:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/chaton/a/eh;->d()Lcom/sec/chaton/a/eg;

    move-result-object v1

    const-string v2, "ReadMessageTask"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/eg;Ljava/lang/String;)V

    .line 183
    new-instance v0, Lcom/sec/chaton/j/ap;

    invoke-direct {v0}, Lcom/sec/chaton/j/ap;-><init>()V

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/a/eh;->d()Lcom/sec/chaton/a/eg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    goto/16 :goto_4

    .line 141
    :cond_e
    if-eqz v1, :cond_6

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    goto/16 :goto_5

    .line 136
    :catch_1
    move-exception v0

    move-object v1, v6

    goto/16 :goto_2
.end method
