.class public Lcom/sec/chaton/d/a/bn;
.super Ljava/lang/Object;
.source "GetAllUnReadMessageTask.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/a/bk;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(Lcom/sec/chaton/d/a/bk;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1108
    iput-object p1, p0, Lcom/sec/chaton/d/a/bn;->a:Lcom/sec/chaton/d/a/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109
    iput-object p2, p0, Lcom/sec/chaton/d/a/bn;->b:Ljava/lang/String;

    .line 1110
    iput-boolean p3, p0, Lcom/sec/chaton/d/a/bn;->c:Z

    .line 1111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/bn;->d:Ljava/util/ArrayList;

    .line 1112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/bn;->e:Ljava/util/ArrayList;

    .line 1113
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/d/a/bn;->f:I

    .line 1114
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/sec/chaton/d/a/bn;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1149
    iput p1, p0, Lcom/sec/chaton/d/a/bn;->f:I

    .line 1150
    return-void
.end method

.method public a(Lcom/sec/chaton/io/entry/inner/Msg;)V
    .locals 2

    .prologue
    .line 1137
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v1, "noti"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/sec/chaton/d/a/bn;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1142
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/bn;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/sec/chaton/d/a/bn;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/sec/chaton/d/a/bn;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1145
    iget v0, p0, Lcom/sec/chaton/d/a/bn;->f:I

    return v0
.end method
