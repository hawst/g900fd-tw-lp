.class public Lcom/sec/chaton/d/a/er;
.super Lcom/sec/chaton/d/a/a;
.source "WritePostONTask.java"


# instance fields
.field private b:Lcom/sec/chaton/poston/k;

.field private c:Landroid/content/ContentResolver;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/poston/k;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 41
    iput-object p3, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    .line 42
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/er;->c:Landroid/content/ContentResolver;

    .line 44
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/d/a/er;->e:Z

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/d/a/er;->e:Z

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 97
    const-string v0, "afterRequest"

    const-string v1, "WritePostONTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_9

    .line 100
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONList;

    .line 103
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    .line 104
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 105
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONList;

    .line 106
    if-nez v0, :cond_1

    .line 107
    const-string v0, "list is null"

    const-string v1, "WritePostONTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PostONList:hasmore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->hasmore:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isblind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WritePostONTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 114
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string v2, "true"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 115
    const-string v0, "blind is true"

    const-string v2, "WritePostONTask"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/er;->e:Z

    if-ne v0, v4, :cond_2

    .line 117
    invoke-static {}, Lcom/sec/chaton/e/a/z;->a()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-static {}, Lcom/sec/chaton/e/a/z;->c()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->c:Landroid/content/ContentResolver;

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {}, Lcom/sec/chaton/e/a/z;->b()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    invoke-static {}, Lcom/sec/chaton/e/a/z;->d()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 125
    :cond_3
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->poston:Ljava/util/ArrayList;

    if-nez v2, :cond_4

    .line 126
    const-string v0, "list.poston is null"

    const-string v1, "WritePostONTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_4
    iget-boolean v2, p0, Lcom/sec/chaton/d/a/er;->e:Z

    if-ne v2, v4, :cond_6

    .line 132
    invoke-static {}, Lcom/sec/chaton/e/a/z;->a()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-static {}, Lcom/sec/chaton/e/a/z;->c()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert to DB: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->poston:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "WritePostONTask"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->poston:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONEntry;

    .line 142
    iget-boolean v3, p0, Lcom/sec/chaton/d/a/er;->e:Z

    if-ne v3, v4, :cond_7

    .line 143
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->a(Lcom/sec/chaton/io/entry/inner/PostONEntry;)Ljava/util/ArrayList;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 145
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 135
    :cond_6
    invoke-static {}, Lcom/sec/chaton/e/a/z;->b()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-static {}, Lcom/sec/chaton/e/a/z;->d()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 148
    :cond_7
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->b(Lcom/sec/chaton/io/entry/inner/PostONEntry;)Ljava/util/ArrayList;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 150
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 154
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->c:Landroid/content/ContentResolver;

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    goto/16 :goto_0

    .line 159
    :cond_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 160
    const-string v0, "01000004"

    const-string v1, "1003"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 7

    .prologue
    .line 53
    new-instance v0, Lcom/sec/chaton/io/entry/inner/PostON;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/PostON;-><init>()V

    .line 54
    new-instance v1, Lcom/sec/chaton/io/entry/inner/PostONMessage;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/inner/PostONMessage;-><init>()V

    .line 56
    new-instance v2, Lcom/sec/chaton/util/af;

    invoke-direct {v2}, Lcom/sec/chaton/util/af;-><init>()V

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 59
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<param>\n"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    iget-object v5, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v5}, Lcom/sec/chaton/poston/k;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 61
    iget-object v5, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v5}, Lcom/sec/chaton/poston/k;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/chaton/io/entry/inner/PostON;->buddyid:Ljava/lang/String;

    .line 62
    invoke-virtual {v2, v0}, Lcom/sec/chaton/util/af;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 63
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "yeseul / postonXml = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/io/entry/inner/PostONMessage;->value:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v1}, Lcom/sec/chaton/util/af;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "yeseul / messageXml = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 73
    const-string v0, "<multimedialist>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 76
    new-instance v5, Lcom/sec/chaton/io/entry/inner/Multimedia;

    invoke-direct {v5}, Lcom/sec/chaton/io/entry/inner/Multimedia;-><init>()V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bz;

    iget-object v0, v0, Lcom/sec/chaton/poston/bz;->a:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/chaton/io/entry/inner/Multimedia;->host:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bz;

    iget-object v0, v0, Lcom/sec/chaton/poston/bz;->c:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/chaton/io/entry/inner/Multimedia;->metatype:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/d/a/er;->b:Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bz;

    iget-object v0, v0, Lcom/sec/chaton/poston/bz;->b:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/chaton/io/entry/inner/Multimedia;->metacontents:Ljava/lang/String;

    .line 81
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/af;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "yeseul / multimediaXml = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const-string v0, "</multimedialist>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_3
    const-string v0, "</param>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yeseul / StringBuilder sb = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
