.class Lcom/sec/chaton/d/a/bj;
.super Ljava/lang/Object;
.source "GetAllMessagesTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/e/w;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:J

.field final synthetic g:Lcom/sec/chaton/e/r;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Landroid/os/Handler;

.field final synthetic j:Ljava/lang/String;

.field final synthetic k:Lcom/sec/chaton/d/a/bh;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/a/bh;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/chaton/d/a/bj;->k:Lcom/sec/chaton/d/a/bh;

    iput-object p2, p0, Lcom/sec/chaton/d/a/bj;->a:Lcom/sec/chaton/e/w;

    iput-object p3, p0, Lcom/sec/chaton/d/a/bj;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/chaton/d/a/bj;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/chaton/d/a/bj;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/chaton/d/a/bj;->e:Ljava/lang/String;

    iput-wide p7, p0, Lcom/sec/chaton/d/a/bj;->f:J

    iput-object p9, p0, Lcom/sec/chaton/d/a/bj;->g:Lcom/sec/chaton/e/r;

    iput-object p10, p0, Lcom/sec/chaton/d/a/bj;->h:Ljava/lang/String;

    iput-object p11, p0, Lcom/sec/chaton/d/a/bj;->i:Landroid/os/Handler;

    iput-object p12, p0, Lcom/sec/chaton/d/a/bj;->j:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v13, 0x0

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/d/a/bj;->a:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/d/a/bj;->a:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v2, :cond_2

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/bj;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/d/a/bj;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/bj;->d:Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/chaton/d/a/bj;->e:Ljava/lang/String;

    iget-wide v5, p0, Lcom/sec/chaton/d/a/bj;->f:J

    iget-object v7, p0, Lcom/sec/chaton/d/a/bj;->g:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/a/bj;->h:Ljava/lang/String;

    invoke-static/range {v0 .. v8}, Lcom/sec/chaton/d/a/ay;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;)Lcom/sec/chaton/d/a/ay;

    move-result-object v0

    .line 297
    new-array v1, v13, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a/ay;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 317
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Download (URL)] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Download (FileName)] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    return-void

    .line 310
    :cond_2
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/bj;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/d/a/bj;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/bj;->i:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/d/a/bj;->j:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/bj;->e:Ljava/lang/String;

    iget-wide v8, p0, Lcom/sec/chaton/d/a/bj;->f:J

    iget-object v10, p0, Lcom/sec/chaton/d/a/bj;->a:Lcom/sec/chaton/e/w;

    iget-object v11, p0, Lcom/sec/chaton/d/a/bj;->g:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/d/a/bj;->h:Ljava/lang/String;

    move-object v2, v1

    move v4, v13

    move v6, v13

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto :goto_0
.end method
