.class public Lcom/sec/chaton/d/a/df;
.super Lcom/sec/chaton/d/a/s;
.source "MediaChatTask.java"


# instance fields
.field protected h:Ljava/lang/String;

.field protected i:Ljava/lang/String;

.field protected j:Ljava/lang/String;

.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Lcom/sec/chaton/e/w;

.field private r:Lcom/sec/chaton/e/r;

.field private s:Ljava/lang/String;

.field private t:[Ljava/lang/String;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Landroid/content/ContentResolver;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p14}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;ILcom/sec/chaton/msgsend/k;)V

    .line 112
    iput-object p5, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    .line 114
    iput-object p6, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    .line 115
    iput-object p7, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    .line 116
    iput-object p8, p0, Lcom/sec/chaton/d/a/df;->s:Ljava/lang/String;

    .line 117
    iput-object p9, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    .line 118
    iput-object p10, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    .line 119
    iput-boolean p11, p0, Lcom/sec/chaton/d/a/df;->u:Z

    .line 120
    iput-object p12, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    .line 121
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    .line 122
    iput-wide p3, p0, Lcom/sec/chaton/d/a/df;->f:J

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    .line 125
    iput-object p13, p0, Lcom/sec/chaton/d/a/df;->k:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 2

    .prologue
    .line 132
    const/4 v1, 0x1

    move-object/from16 v0, p15

    invoke-direct {p0, p1, v1, v0}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;ILcom/sec/chaton/msgsend/k;)V

    .line 133
    iput-object p5, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    .line 134
    iput-object p2, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    .line 135
    iput-object p6, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    .line 136
    iput-object p7, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    .line 137
    iput-object p8, p0, Lcom/sec/chaton/d/a/df;->s:Ljava/lang/String;

    .line 138
    iput-object p9, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    .line 139
    iput-object p10, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    .line 140
    iput-boolean p11, p0, Lcom/sec/chaton/d/a/df;->u:Z

    .line 141
    iput-object p12, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    .line 142
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    .line 143
    iput-wide p3, p0, Lcom/sec/chaton/d/a/df;->f:J

    .line 144
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    .line 146
    iput-object p13, p0, Lcom/sec/chaton/d/a/df;->k:Ljava/lang/String;

    .line 147
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->l:Ljava/lang/String;

    .line 148
    return-void
.end method

.method private a([Ljava/lang/String;J)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 677
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 678
    const-string v1, ""

    .line 679
    array-length v5, p1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, p1, v2

    .line 680
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v0, v6, v7}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 681
    const-string v0, ""

    .line 682
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v0, v7, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 683
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 686
    :goto_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 687
    if-nez v0, :cond_1

    .line 688
    const-string v1, "1001 error - invalidUserName: NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    :cond_0
    :goto_2
    const-string v1, "%d,%s,%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v8, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v8}, Lcom/sec/chaton/e/aj;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x1

    aput-object v6, v7, v8

    const/4 v8, 0x2

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ";"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v1, v7, v6}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 690
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "1001 error - invalidUserName:"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 697
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 700
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 709
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 20

    .prologue
    .line 332
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p2

    invoke-super {v0, v1, v2, v3}, Lcom/sec/chaton/d/a/s;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 334
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "onPostExecute()"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    new-instance v17, Landroid/os/Message;

    invoke-direct/range {v17 .. v17}, Landroid/os/Message;-><init>()V

    .line 337
    if-eqz p3, :cond_22

    .line 338
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 343
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 665
    :goto_0
    :pswitch_0
    return-void

    .line 347
    :pswitch_1
    const/16 v4, 0x6a

    move-object/from16 v0, v17

    iput v4, v0, Landroid/os/Message;->what:I

    .line 350
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v6

    check-cast v6, Lcom/sec/chaton/a/cx;

    .line 352
    sget-boolean v4, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v4, :cond_0

    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[InitChatReply]MsgID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->d()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", InboxNO : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", UID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v7, "uid"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ResultCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ResultMessage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", MsgReceivedTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ServerInfo(address) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->j()Lcom/sec/chaton/a/ey;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->j()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ServerInfo(port) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->j()Lcom/sec/chaton/a/ey;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->j()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SessionID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->d()I

    move-result v14

    .line 359
    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v15

    .line 360
    const/16 v4, 0x3e8

    if-eq v14, v4, :cond_1

    const/16 v4, 0x3e9

    if-eq v14, v4, :cond_1

    const/16 v4, 0xbba

    if-ne v14, v4, :cond_8

    .line 362
    :cond_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 363
    const-string v4, "buddyname retrieve end"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    invoke-static {v4, v5, v7}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/util/Map;)Z

    .line 365
    const-string v4, "participant insert start"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v8, v4

    .line 368
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 369
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v8, :cond_4

    .line 372
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    aget-object v11, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v10, v11, v4}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 353
    :cond_2
    const-string v4, "null(broadcast)"

    goto/16 :goto_1

    :cond_3
    const-string v4, "null(broadcast)"

    goto/16 :goto_2

    .line 375
    :cond_4
    :try_start_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    const-string v5, "com.sec.chaton.provider"

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    :cond_5
    :goto_4
    const-string v4, "participant insert end"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    invoke-static {v4, v5, v7, v8, v9}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 389
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    invoke-static/range {v4 .. v10}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/a/cx;Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Lcom/sec/chaton/e/w;)Landroid/net/Uri;

    .line 391
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->d()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v13, v4

    invoke-static/range {v7 .. v13}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;JLjava/lang/Long;I)I

    .line 395
    const/16 v4, 0x3e9

    if-ne v14, v4, :cond_7

    .line 397
    const-string v4, "["

    invoke-virtual {v15, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const-string v5, "]"

    invoke-virtual {v15, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v15, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 398
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 399
    const-string v4, ""

    .line 401
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 403
    array-length v7, v5

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v7, :cond_6

    aget-object v8, v5, v4

    .line 404
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v10, v11, v8}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 405
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "1001 error - invalidUserName:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v10, v11, v8}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 378
    :catch_0
    move-exception v4

    .line 379
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v4, "Fail to insert participant to DB"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 410
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 412
    new-instance v4, Lcom/sec/chaton/d/a/dh;

    const/4 v8, 0x1

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/chaton/d/a/df;->f:J

    move-object/from16 v5, p0

    move v6, v8

    move v8, v14

    invoke-direct/range {v4 .. v11}, Lcom/sec/chaton/d/a/dh;-><init>(Lcom/sec/chaton/d/a/df;ZLjava/lang/String;ILjava/util/ArrayList;J)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 421
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 414
    :cond_7
    new-instance v4, Lcom/sec/chaton/d/a/dh;

    const/4 v8, 0x1

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/chaton/d/a/df;->f:J

    move-object/from16 v5, p0

    move v6, v8

    move v8, v14

    invoke-direct/range {v4 .. v10}, Lcom/sec/chaton/d/a/dh;-><init>(Lcom/sec/chaton/d/a/df;ZLjava/lang/String;IJ)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_6

    .line 424
    :cond_8
    const/16 v4, 0x1b5e

    if-ne v14, v4, :cond_9

    .line 425
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v9

    invoke-virtual {v4, v5, v7, v8, v9}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/ej;)V

    .line 434
    :cond_9
    new-instance v7, Lcom/sec/chaton/d/a/dh;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/chaton/d/a/df;->f:J

    move-object/from16 v8, p0

    move v11, v14

    invoke-direct/range {v7 .. v13}, Lcom/sec/chaton/d/a/dh;-><init>(Lcom/sec/chaton/d/a/df;ZLjava/lang/String;IJ)V

    move-object/from16 v0, v17

    iput-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 436
    const/16 v4, 0xbb9

    if-ne v14, v4, :cond_a

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v4, v5, v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/Long;)I

    .line 444
    :goto_7
    const/16 v4, 0x1772

    if-ne v14, v4, :cond_e

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 446
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 448
    array-length v7, v5

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v7, :cond_b

    aget-object v9, v5, v4

    .line 449
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v10, v11, v9}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 440
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_7

    .line 452
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 453
    array-length v4, v5

    if-lez v4, :cond_c

    .line 454
    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7}, Lcom/sec/chaton/d/a/df;->a([Ljava/lang/String;J)V

    .line 459
    :cond_c
    new-instance v4, Lcom/sec/chaton/d/a/dh;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/chaton/d/a/df;->f:J

    move-object/from16 v5, p0

    move v7, v14

    invoke-direct/range {v4 .. v10}, Lcom/sec/chaton/d/a/dh;-><init>(Lcom/sec/chaton/d/a/df;ZILjava/util/ArrayList;J)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 466
    :cond_d
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 460
    :cond_e
    const/16 v4, 0x1b5e

    if-ne v14, v4, :cond_d

    .line 461
    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    if-eqz v4, :cond_d

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v4

    if-eqz v4, :cond_d

    .line 462
    new-instance v4, Lcom/sec/chaton/d/a/dh;

    const/4 v7, 0x0

    invoke-virtual {v6}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v8

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/chaton/d/a/df;->f:J

    move-object/from16 v5, p0

    move v6, v7

    move v7, v14

    invoke-direct/range {v4 .. v10}, Lcom/sec/chaton/d/a/dh;-><init>(Lcom/sec/chaton/d/a/df;ZILcom/sec/chaton/a/ea;J)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_9

    .line 475
    :pswitch_2
    const/16 v4, 0x66

    move-object/from16 v0, v17

    iput v4, v0, Landroid/os/Message;->what:I

    .line 476
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v4

    check-cast v4, Lcom/sec/chaton/a/f;

    .line 477
    sget-boolean v5, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v5, :cond_f

    .line 478
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[AllowChatReply]MsgID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->d()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", InboxNO : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", UID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "uid"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ResultCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/ej;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ResultMsg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_f
    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->d()I

    move-result v6

    .line 484
    const/16 v5, 0x3e8

    if-eq v6, v5, :cond_10

    const/16 v5, 0xbbb

    if-ne v6, v5, :cond_11

    .line 485
    :cond_10
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/4 v5, 0x1

    const/16 v6, 0x384

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-direct/range {v4 .. v9}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 529
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 491
    :cond_11
    const/16 v5, 0x7d6

    if-eq v6, v5, :cond_12

    const/16 v5, 0xbc0

    if-ne v6, v5, :cond_13

    .line 494
    :cond_12
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/chaton/util/bk;->b()I

    move-result v8

    invoke-static {v4, v5, v7, v8}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    .line 527
    :goto_b
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-direct/range {v4 .. v9}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_a

    .line 501
    :cond_13
    const/16 v5, 0x1b59

    if-ne v6, v5, :cond_14

    .line 502
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->n:Ljava/lang/String;

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v7, v8, v9}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_b

    .line 503
    :cond_14
    const/16 v5, 0x1b5a

    if-ne v6, v5, :cond_15

    .line 504
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v5, v7, v8, v4}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_b

    .line 505
    :cond_15
    const/16 v5, 0x1b5b

    if-ne v6, v5, :cond_16

    .line 506
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v5, v7, v4}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_b

    .line 524
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_b

    .line 535
    :pswitch_3
    const/16 v4, 0x66

    move-object/from16 v0, v17

    iput v4, v0, Landroid/os/Message;->what:I

    .line 537
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v4

    move-object/from16 v16, v4

    check-cast v16, Lcom/sec/chaton/a/aw;

    .line 539
    sget-boolean v4, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v4, :cond_17

    .line 540
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ChatReply]MsgID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", InboxNO : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", UID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "uid"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", MsgReceivedTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SessionID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ResultCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ResultMsg : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ServerInfo(address) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v4

    if-eqz v4, :cond_1b

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v4

    :goto_c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ServerInfo(port) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v4

    if-eqz v4, :cond_1c

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_17
    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->d()I

    move-result v18

    .line 551
    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v19

    .line 553
    const/16 v4, 0x3e8

    move/from16 v0, v18

    if-eq v0, v4, :cond_18

    const/16 v4, 0x3e9

    move/from16 v0, v18

    if-ne v0, v4, :cond_1d

    .line 559
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v10, v10

    invoke-static/range {v4 .. v10}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;JLjava/lang/Long;I)I

    .line 561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "chaton_id"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/df;->i:Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v8

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v11

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/chaton/util/bk;->b()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    invoke-static/range {v4 .. v15}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;ILcom/sec/chaton/e/w;)I

    .line 577
    const/16 v4, 0x3e9

    move/from16 v0, v18

    if-ne v0, v4, :cond_19

    .line 579
    const-string v4, "["

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const-string v5, "]"

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 580
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 581
    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/chaton/d/a/df;->a([Ljava/lang/String;J)V

    .line 585
    :cond_19
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/chaton/d/a/df;->f:J

    move/from16 v0, v18

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 616
    :cond_1a
    :goto_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 540
    :cond_1b
    const-string v4, "null"

    goto/16 :goto_c

    :cond_1c
    const-string v4, "null"

    goto/16 :goto_d

    .line 587
    :cond_1d
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/chaton/d/a/df;->f:J

    move/from16 v0, v18

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 589
    const/16 v4, 0x1b59

    move/from16 v0, v18

    if-ne v0, v4, :cond_1e

    .line 590
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 591
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v7

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_e

    .line 592
    :cond_1e
    const/16 v4, 0x1b5a

    move/from16 v0, v18

    if-ne v0, v4, :cond_1f

    .line 593
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_e

    .line 594
    :cond_1f
    const/16 v4, 0x1b5b

    move/from16 v0, v18

    if-ne v0, v4, :cond_20

    .line 595
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_e

    .line 597
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 601
    const/16 v4, 0x1772

    move/from16 v0, v18

    if-ne v0, v4, :cond_1a

    .line 602
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    sget-object v5, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v4, v5, :cond_21

    .line 603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 604
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v7

    invoke-static {v5, v6, v4, v7, v8}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;J)I

    goto/16 :goto_e

    .line 606
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 610
    invoke-virtual/range {v16 .. v16}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/chaton/d/a/df;->a([Ljava/lang/String;J)V

    goto/16 :goto_e

    .line 622
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/d/a/df;->a()Z

    move-result v4

    if-eqz v4, :cond_23

    const/16 v4, 0xb

    move/from16 v0, p1

    if-eq v0, v4, :cond_23

    const/16 v4, 0x16

    move/from16 v0, p1

    if-eq v0, v4, :cond_23

    const/4 v4, 0x3

    move/from16 v0, p1

    if-ne v0, v4, :cond_25

    .line 640
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->m:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v4

    if-nez v4, :cond_26

    invoke-static/range {p1 .. p1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 641
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 648
    :goto_f
    const/16 v4, 0x18

    move/from16 v0, p1

    if-eq v0, v4, :cond_24

    const/16 v4, 0x17

    move/from16 v0, p1

    if-eq v0, v4, :cond_24

    const/16 v4, 0x15

    move/from16 v0, p1

    if-ne v0, v4, :cond_25

    .line 653
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 661
    :cond_25
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/chaton/d/a/df;->f:J

    move/from16 v0, p1

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    move-object/from16 v0, v17

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 663
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->b:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 644
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_f

    .line 343
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 152
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->b()Lcom/sec/chaton/j/ao;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    .line 156
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->p:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    :cond_0
    sget-object v2, Lcom/sec/chaton/d/a/dg;->a:[I

    iget-object v3, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    invoke-virtual {v3}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 198
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-ne v2, v5, :cond_4

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    .line 272
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->i:Ljava/lang/String;

    .line 288
    sget-object v0, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_1

    sget-object v0, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-wide v2, p0, Lcom/sec/chaton/d/a/df;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v7

    move-object v2, v1

    invoke-static/range {v0 .. v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Z)I

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iget-wide v1, p0, Lcom/sec/chaton/d/a/df;->f:J

    iget-object v3, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/p;->b(Ljava/lang/String;JLjava/lang/String;)V

    .line 294
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/df;->u:Z

    if-eqz v0, :cond_c

    .line 295
    const-string v0, "broadcast2_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_2

    .line 297
    sget-object v0, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    .line 301
    :cond_2
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iget-wide v2, p0, Lcom/sec/chaton/d/a/df;->f:J

    iget-object v4, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/aq;->a(Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v1

    .line 325
    :cond_3
    :goto_2
    return-object v1

    .line 165
    :pswitch_0
    const-string v2, "image"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto :goto_0

    .line 168
    :pswitch_1
    const-string v2, "audio"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto :goto_0

    .line 171
    :pswitch_2
    const-string v2, "calendar"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto :goto_0

    .line 174
    :pswitch_3
    const-string v2, "contact"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 177
    :pswitch_4
    const-string v2, "geo"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 180
    :pswitch_5
    const-string v2, "video"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 183
    :pswitch_6
    const-string v2, "doc"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    .line 184
    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 187
    :pswitch_7
    const-string v2, "file"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    .line 188
    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 191
    :pswitch_8
    const-string v2, "ams"

    iput-object v2, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 206
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v2, v5, :cond_5

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    goto/16 :goto_1

    .line 213
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 218
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->k:Ljava/lang/String;

    .line 219
    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->l:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 220
    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->l:Ljava/lang/String;

    .line 224
    :cond_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 225
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 229
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    if-eq v0, v5, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    if-eq v0, v5, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-eq v0, v5, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    if-ne v0, v5, :cond_9

    .line 256
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->j:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    goto/16 :goto_1

    .line 235
    :cond_b
    const-string v0, "mixed\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string v0, "mixed\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 305
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->w:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/j/ak;)V

    .line 309
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v4

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_e

    .line 312
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->n:Ljava/lang/String;

    .line 317
    :goto_4
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/d/a/df;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->s:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/aq;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v1

    goto/16 :goto_2

    .line 314
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/df;->n:Ljava/lang/String;

    goto :goto_4

    .line 320
    :cond_f
    const-string v8, ""

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_10

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/d/a/df;->t:[Ljava/lang/String;

    aget-object v8, v0, v9

    .line 325
    :cond_10
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/d/a/df;->f:J

    iget-object v3, p0, Lcom/sec/chaton/d/a/df;->o:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/df;->q:Lcom/sec/chaton/e/w;

    iget-object v5, p0, Lcom/sec/chaton/d/a/df;->r:Lcom/sec/chaton/e/r;

    iget-object v6, p0, Lcom/sec/chaton/d/a/df;->s:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/df;->x:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/aq;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v1

    goto/16 :goto_2

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
