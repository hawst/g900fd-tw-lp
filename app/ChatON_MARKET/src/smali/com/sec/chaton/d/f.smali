.class public Lcom/sec/chaton/d/f;
.super Ljava/lang/Object;
.source "BlockControl.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/chaton/d/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/f;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 48
    const-string v0, "/v5/blockbuddy"

    .line 50
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const-string v0, "/blockbuddy"

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/GetBlockBuddyList;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x259

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 65
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/bo;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/sec/chaton/d/a/bo;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 66
    return-void

    .line 54
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    const-string v0, "/blockbuddy"

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 58
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 131
    const-string v0, "/v5/unblock"

    .line 133
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    const-string v0, "/unblock"

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    const-class v0, Lcom/sec/chaton/io/entry/UnBlock;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x25b

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 147
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/eg;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1}, Lcom/sec/chaton/d/a/eg;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 148
    return-void

    .line 137
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    const-string v0, "/unblock"

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 75
    const-string v0, "/v5/block"

    .line 77
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string v0, "/block"

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x25a

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 92
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/x;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/sec/chaton/d/a/x;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 93
    return-void

    .line 81
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    const-string v0, "/block"

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 158
    const-string v0, "/v5/blockbuddy"

    .line 160
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const-string v0, "/blockbuddy"

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    const-class v0, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x25c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 174
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/ef;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1}, Lcom/sec/chaton/d/a/ef;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 175
    return-void

    .line 164
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 165
    const-string v0, "/blockbuddy"

    .line 166
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 168
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a([Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 104
    const-string v0, "/v5/blockbuddy"

    .line 106
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    const-string v0, "/blockbuddy"

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    const-class v0, Lcom/sec/chaton/io/entry/inner/Address;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x25e

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 120
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/w;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1}, Lcom/sec/chaton/d/a/w;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 121
    return-void

    .line 110
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const-string v0, "/blockbuddy"

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 114
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 184
    const-string v0, "/spammer/report"

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access server "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x25d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 194
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/x;

    iget-object v3, p0, Lcom/sec/chaton/d/f;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/sec/chaton/d/a/x;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 195
    return-void
.end method
