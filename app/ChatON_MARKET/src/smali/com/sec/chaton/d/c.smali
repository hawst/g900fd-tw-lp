.class Lcom/sec/chaton/d/c;
.super Ljava/lang/Object;
.source "AbstractServiceWrapper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/b;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/b;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 44
    const-string v0, "ServiceConnection.onServiceConnected"

    invoke-static {}, Lcom/sec/chaton/d/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    iget-object v1, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-virtual {v1, p2}, Lcom/sec/chaton/d/b;->a(Landroid/os/IBinder;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/d/b;->b:Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/b;->a(Lcom/sec/chaton/d/b;Z)Z

    .line 50
    iget-object v1, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    monitor-enter v1

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-virtual {v0}, Lcom/sec/chaton/d/b;->a()V

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-virtual {v0}, Lcom/sec/chaton/d/b;->b()V

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-static {v0}, Lcom/sec/chaton/d/b;->a(Lcom/sec/chaton/d/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 56
    iget-object v3, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-virtual {v3}, Lcom/sec/chaton/d/b;->b()V

    .line 58
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 61
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-virtual {v0}, Lcom/sec/chaton/d/b;->c()V

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    invoke-static {v0}, Lcom/sec/chaton/d/b;->a(Lcom/sec/chaton/d/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 64
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/d/c;->a:Lcom/sec/chaton/d/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/chaton/d/b;->b:Ljava/lang/Object;

    .line 40
    return-void
.end method
