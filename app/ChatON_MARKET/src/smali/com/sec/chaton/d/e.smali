.class public final enum Lcom/sec/chaton/d/e;
.super Ljava/lang/Enum;
.source "AmsItemMessageControl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/d/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/d/e;

.field public static final enum b:Lcom/sec/chaton/d/e;

.field public static final enum c:Lcom/sec/chaton/d/e;

.field public static final enum d:Lcom/sec/chaton/d/e;

.field private static final synthetic f:[Lcom/sec/chaton/d/e;


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, Lcom/sec/chaton/d/e;

    const-string v1, "Default"

    const-string v2, "-1"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/d/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/d/e;->a:Lcom/sec/chaton/d/e;

    .line 44
    new-instance v0, Lcom/sec/chaton/d/e;

    const-string v1, "Background"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/d/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    .line 45
    new-instance v0, Lcom/sec/chaton/d/e;

    const-string v1, "Stamp"

    const-string v2, "2"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/d/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    .line 46
    new-instance v0, Lcom/sec/chaton/d/e;

    const-string v1, "Template"

    const-string v2, "3"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/d/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/d/e;

    sget-object v1, Lcom/sec/chaton/d/e;->a:Lcom/sec/chaton/d/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/d/e;->f:[Lcom/sec/chaton/d/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-object p3, p0, Lcom/sec/chaton/d/e;->e:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/d/e;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    .line 66
    :goto_0
    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    goto :goto_0

    .line 66
    :cond_2
    sget-object v0, Lcom/sec/chaton/d/e;->a:Lcom/sec/chaton/d/e;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/d/e;
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/d/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/e;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/d/e;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/d/e;->f:[Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, [Lcom/sec/chaton/d/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/d/e;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/d/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/e/ar;
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget-object v0, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    .line 78
    :goto_0
    return-object v0

    .line 73
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    sget-object v0, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 75
    :cond_1
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    sget-object v0, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 78
    :cond_2
    sget-object v0, Lcom/sec/chaton/e/ar;->a:Lcom/sec/chaton/e/ar;

    goto :goto_0
.end method
