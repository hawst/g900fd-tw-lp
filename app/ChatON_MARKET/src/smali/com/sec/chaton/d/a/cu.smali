.class public Lcom/sec/chaton/d/a/cu;
.super Lcom/sec/chaton/d/a/a;
.source "GetVersionTask.java"


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_1

    .line 50
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->d()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 51
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "regid"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Save get version push registration id to pref: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/d/a/cu;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "get_version_push_reg_id"

    invoke-virtual {v2, v3, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 63
    const-string v0, "01000002"

    const-string v1, "0001"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    .line 66
    :cond_2
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return-object v0
.end method
