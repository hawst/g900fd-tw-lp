.class public Lcom/sec/chaton/d/a/eq;
.super Lcom/sec/chaton/d/a/a;
.source "WritePostONCommentTask.java"


# instance fields
.field private b:Lcom/sec/chaton/poston/p;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Lcom/sec/chaton/poston/p;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 37
    iput-object p4, p0, Lcom/sec/chaton/d/a/eq;->b:Lcom/sec/chaton/poston/p;

    .line 38
    iput-object p3, p0, Lcom/sec/chaton/d/a/eq;->c:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/sec/chaton/d/a/eq;->e:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/eq;->f:Landroid/content/ContentResolver;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 4

    .prologue
    .line 79
    const-string v0, "afterRequest"

    const-string v1, "WritePostONCommentTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_5

    .line 83
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 84
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;

    .line 86
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;->commentlist:Ljava/util/ArrayList;

    .line 87
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 88
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;

    .line 89
    if-nez v0, :cond_1

    .line 90
    const-string v0, "list is null"

    const-string v1, "WritePostONCommentTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PostONCommentList:hasmore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->hasmore:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isblind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->isblind:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WritePostONCommentTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 96
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->isblind:Ljava/lang/String;

    if-eqz v2, :cond_2

    const-string v2, "true"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->isblind:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    const-string v0, "blind is true"

    const-string v2, "WritePostONCommentTask"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/sec/chaton/e/a/z;->e()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_2
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->comment:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    .line 101
    const-string v0, "list.comment is null"

    const-string v1, "WritePostONCommentTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_3
    invoke-static {}, Lcom/sec/chaton/e/a/z;->e()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert to DB: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->comment:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "WritePostONCommentTask"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->comment:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONComment;

    .line 110
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->a(Lcom/sec/chaton/io/entry/inner/PostONComment;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/d/a/eq;->f:Landroid/content/ContentResolver;

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    goto/16 :goto_0

    .line 117
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 118
    const-string v0, "01000004"

    const-string v1, "1102"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    new-instance v1, Lcom/sec/chaton/io/entry/inner/Comment;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/inner/Comment;-><init>()V

    .line 48
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Comment;->buddyid:Ljava/lang/String;

    .line 51
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Comment;->parentpostonid:Ljava/lang/String;

    .line 60
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->b:Lcom/sec/chaton/poston/p;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/p;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 61
    iget-object v2, p0, Lcom/sec/chaton/d/a/eq;->b:Lcom/sec/chaton/poston/p;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/p;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Comment;->value:Ljava/lang/String;

    .line 64
    :cond_2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 67
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/eq;->p()V

    .line 70
    :cond_3
    new-instance v1, Lcom/sec/chaton/util/af;

    invoke-direct {v1}, Lcom/sec/chaton/util/af;-><init>()V

    .line 71
    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 73
    const-string v1, "WritePostONCommentTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-object v0
.end method
