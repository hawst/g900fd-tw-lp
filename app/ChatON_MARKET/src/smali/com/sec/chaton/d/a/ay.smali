.class public Lcom/sec/chaton/d/a/ay;
.super Landroid/os/AsyncTask;
.source "FileDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Lcom/sec/chaton/e/r;

.field private h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/chaton/d/a/ay;->a:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/sec/chaton/d/a/ay;->b:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/sec/chaton/d/a/ay;->c:Ljava/lang/String;

    .line 31
    iput-boolean p4, p0, Lcom/sec/chaton/d/a/ay;->d:Z

    .line 32
    iput-object p5, p0, Lcom/sec/chaton/d/a/ay;->e:Ljava/lang/String;

    .line 33
    iput-wide p6, p0, Lcom/sec/chaton/d/a/ay;->f:J

    .line 34
    iput-object p8, p0, Lcom/sec/chaton/d/a/ay;->g:Lcom/sec/chaton/e/r;

    .line 35
    iput-object p9, p0, Lcom/sec/chaton/d/a/ay;->h:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;)Lcom/sec/chaton/d/a/ay;
    .locals 10

    .prologue
    .line 25
    new-instance v0, Lcom/sec/chaton/d/a/ay;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-wide v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/d/a/ay;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/d/a/ay;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ay;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ay;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/sec/chaton/d/a/ay;->d:Z

    iget-object v4, p0, Lcom/sec/chaton/d/a/ay;->e:Ljava/lang/String;

    iget-wide v5, p0, Lcom/sec/chaton/d/a/ay;->f:J

    iget-object v7, p0, Lcom/sec/chaton/d/a/ay;->g:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/d/a/ay;->h:Ljava/lang/String;

    invoke-static/range {v0 .. v8}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/ay;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
