.class public Lcom/sec/chaton/d/a/q;
.super Lcom/sec/chaton/d/a/e;
.source "AsyncEndChatTask.java"


# instance fields
.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 62
    invoke-virtual {p3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/d/a/q;->h:Ljava/util/ArrayList;

    .line 63
    invoke-virtual {p2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/d/a/q;->i:Ljava/util/ArrayList;

    .line 64
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/q;->k:Ljava/lang/String;

    .line 65
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/q;->j:Ljava/lang/String;

    .line 66
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/q;->l:Landroid/content/ContentResolver;

    .line 67
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/q;->f:J

    .line 68
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 215
    new-instance v0, Lcom/sec/chaton/d/a/r;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/a/r;-><init>(Lcom/sec/chaton/d/a/q;Ljava/lang/String;)V

    .line 222
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 224
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 225
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    .line 226
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Delete File] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AsyncEndChatTask"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v13, 0x1

    const/4 v8, 0x0

    .line 140
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 143
    invoke-direct {p0, v6}, Lcom/sec/chaton/d/a/q;->a(Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v11

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 148
    const-string v7, ""

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->l:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "inbox_old_no"

    aput-object v3, v2, v8

    const-string v3, "inbox_session_id"

    aput-object v3, v2, v13

    const-string v3, "inbox_no=?"

    new-array v4, v13, [Ljava/lang/String;

    aput-object v6, v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 151
    if-eqz v2, :cond_7

    .line 152
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 153
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-interface {v2, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 159
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 160
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 161
    array-length v1, v2

    if-lez v1, :cond_1

    .line 162
    array-length v3, v2

    move v1, v8

    :goto_2
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 163
    invoke-direct {p0, v4}, Lcom/sec/chaton/d/a/q;->a(Ljava/lang/String;)V

    .line 164
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "/"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 162
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 169
    :cond_1
    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "inbox_no=?"

    new-array v3, v13, [Ljava/lang/String;

    aput-object v6, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "message_inbox_no=?"

    new-array v3, v13, [Ljava/lang/String;

    aput-object v6, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    sget-object v1, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "participants_inbox_no=?"

    new-array v3, v13, [Ljava/lang/String;

    aput-object v6, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-static {v6}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    .line 174
    if-eqz v1, :cond_2

    .line 175
    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->j()V

    .line 176
    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->i()V

    .line 179
    :cond_2
    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 180
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/chaton/trunk/database/a/a;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    goto :goto_4

    .line 182
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/chaton/j/c/a;->a(Ljava/lang/String;)Z

    .line 183
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 185
    :catch_0
    move-exception v0

    .line 186
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 202
    :cond_4
    :try_start_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->l:Landroid/content/ContentResolver;

    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_2

    .line 210
    :cond_5
    :goto_5
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 211
    return-void

    .line 205
    :catch_1
    move-exception v0

    .line 206
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 207
    :catch_2
    move-exception v0

    .line 208
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    move-object v0, v7

    move-object v1, v5

    goto/16 :goto_0

    :cond_7
    move-object v0, v7

    move-object v1, v5

    goto/16 :goto_1
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 94
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/e;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 96
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 97
    const/16 v0, 0x19

    iput v0, v1, Landroid/os/Message;->what:I

    .line 99
    if-eqz p3, :cond_1

    .line 100
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 101
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 135
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 137
    return-void

    .line 104
    :pswitch_0
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/aa;

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ch@t[AsyncEndChatReply]UID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ResultCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ResultMessage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MsgID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->d()I

    move-result v0

    .line 110
    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/sec/chaton/d/a/q;->c()V

    .line 112
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v2

    const/4 v3, -0x1

    new-instance v4, Lcom/sec/chaton/e/b/m;

    const/4 v5, 0x0

    sget v6, Lcom/sec/chaton/chat/notification/a;->d:I

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 113
    new-instance v2, Lcom/sec/chaton/a/a/k;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v0}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 116
    :cond_0
    new-instance v2, Lcom/sec/chaton/a/a/k;

    invoke-direct {v2, v7, v0}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/q;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xb

    if-eq p1, v0, :cond_2

    const/16 v0, 0x16

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 125
    :cond_2
    const/16 v0, 0x18

    if-eq p1, v0, :cond_3

    const/16 v0, 0x17

    if-eq p1, v0, :cond_3

    const/16 v0, 0x15

    if-ne p1, v0, :cond_4

    .line 126
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/d/a/q;->l:Landroid/content/ContentResolver;

    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 130
    :cond_4
    const-string v0, "Fail to send endChat message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v7, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/d/a/q;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
    .end packed-switch
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 5

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/chaton/d/a/e;->b()Lcom/sec/chaton/j/ao;

    .line 74
    invoke-static {}, Lcom/sec/chaton/a/ad;->newBuilder()Lcom/sec/chaton/a/ae;

    move-result-object v0

    .line 76
    iget-wide v1, p0, Lcom/sec/chaton/d/a/q;->f:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 77
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/d/a/q;->f:J

    .line 79
    :cond_0
    iget-wide v1, p0, Lcom/sec/chaton/d/a/q;->f:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/ae;->a(J)Lcom/sec/chaton/a/ae;

    .line 80
    iget-object v1, p0, Lcom/sec/chaton/d/a/q;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ae;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ae;

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/d/a/q;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ae;->b(Ljava/lang/String;)Lcom/sec/chaton/a/ae;

    .line 82
    iget-object v1, p0, Lcom/sec/chaton/d/a/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ae;->a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/ae;

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ch@t[AsyncEndChatReqeust] UID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/a/q;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MsgID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/d/a/q;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Sender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/a/q;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AsyncEndChatTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/a/ae;->d()Lcom/sec/chaton/a/ad;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
