.class public final enum Lcom/sec/chaton/d/a/b;
.super Ljava/lang/Enum;
.source "AbstractHttpTask2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/d/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/d/a/b;

.field public static final enum b:Lcom/sec/chaton/d/a/b;

.field public static final enum c:Lcom/sec/chaton/d/a/b;

.field public static final enum d:Lcom/sec/chaton/d/a/b;

.field private static final synthetic f:[Lcom/sec/chaton/d/a/b;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/sec/chaton/d/a/b;

    const-string v1, "SYSTEM"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/d/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/a/b;->a:Lcom/sec/chaton/d/a/b;

    .line 33
    new-instance v0, Lcom/sec/chaton/d/a/b;

    const-string v1, "UI"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/d/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    .line 34
    new-instance v0, Lcom/sec/chaton/d/a/b;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/d/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/a/b;->c:Lcom/sec/chaton/d/a/b;

    .line 35
    new-instance v0, Lcom/sec/chaton/d/a/b;

    const-string v1, "API"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/d/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/d/a/b;->d:Lcom/sec/chaton/d/a/b;

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/d/a/b;

    sget-object v1, Lcom/sec/chaton/d/a/b;->a:Lcom/sec/chaton/d/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/d/a/b;->c:Lcom/sec/chaton/d/a/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/d/a/b;->d:Lcom/sec/chaton/d/a/b;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/d/a/b;->f:[Lcom/sec/chaton/d/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/sec/chaton/d/a/b;->e:I

    .line 41
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/d/a/b;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/d/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/b;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/d/a/b;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/chaton/d/a/b;->f:[Lcom/sec/chaton/d/a/b;

    invoke-virtual {v0}, [Lcom/sec/chaton/d/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/d/a/b;

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/chaton/d/a/b;->e:I

    return v0
.end method
