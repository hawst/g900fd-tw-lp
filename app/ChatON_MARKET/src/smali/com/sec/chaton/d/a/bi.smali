.class Lcom/sec/chaton/d/a/bi;
.super Ljava/lang/Object;
.source "GetAllMessagesTask.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/chaton/io/entry/inner/Message;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/a/bh;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/a/bh;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/chaton/d/a/bi;->a:Lcom/sec/chaton/d/a/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/io/entry/inner/Message;Lcom/sec/chaton/io/entry/inner/Message;)I
    .locals 4

    .prologue
    .line 99
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 100
    const/4 v0, -0x1

    .line 104
    :goto_0
    return v0

    .line 101
    :cond_0
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 96
    check-cast p1, Lcom/sec/chaton/io/entry/inner/Message;

    check-cast p2, Lcom/sec/chaton/io/entry/inner/Message;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/d/a/bi;->a(Lcom/sec/chaton/io/entry/inner/Message;Lcom/sec/chaton/io/entry/inner/Message;)I

    move-result v0

    return v0
.end method
