.class public Lcom/sec/chaton/d/a/bc;
.super Lcom/sec/chaton/d/a/c;
.source "ForwardStoredMessageTask.java"


# instance fields
.field h:Lcom/sec/chaton/a/cr;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/chaton/e/r;

.field private l:Lcom/sec/chaton/a/cl;

.field private m:Landroid/content/ContentResolver;

.field private n:I

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/cl;Lcom/sec/chaton/d/o;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/c;-><init>(Landroid/os/Handler;)V

    .line 47
    iput v1, p0, Lcom/sec/chaton/d/a/bc;->n:I

    .line 49
    iput-boolean v1, p0, Lcom/sec/chaton/d/a/bc;->o:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/chaton/d/a/bc;->p:Z

    .line 54
    iput-object p2, p0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    .line 56
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/bc;->m:Landroid/content/ContentResolver;

    .line 57
    iput-object p4, p0, Lcom/sec/chaton/d/a/bc;->g:Lcom/sec/chaton/d/o;

    .line 58
    iput v1, p0, Lcom/sec/chaton/d/a/bc;->n:I

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    .line 60
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 207
    if-eqz p3, :cond_1

    .line 208
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 209
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/co;

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 212
    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/co;->i()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 213
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] MsgID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/co;->a(I)Lcom/sec/chaton/a/cu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", MsgType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/co;->a(I)Lcom/sec/chaton/a/cu;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/cu;->l()Lcom/sec/chaton/a/dp;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", Receiver : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/co;->a(I)Lcom/sec/chaton/a/cu;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/cu;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", Sender : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/co;->a(I)Lcom/sec/chaton/a/cu;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/cu;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 216
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ForwardStoredMessageReply]InboxNO : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", UID : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "uid"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", ForwardStoredMessageReplyItemsCount : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/co;->j()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :goto_1
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 222
    const/16 v0, 0x24

    iput v0, v1, Landroid/os/Message;->what:I

    .line 225
    new-instance v0, Lcom/sec/chaton/a/a/e;

    invoke-direct {v0}, Lcom/sec/chaton/a/a/e;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/e;->a(Z)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cl;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/e;->a(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/e;->a(Lcom/sec/chaton/a/a/n;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget v4, p0, Lcom/sec/chaton/d/a/bc;->n:I

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/e;->a(I)Lcom/sec/chaton/a/a/e;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_2

    move-object v0, v3

    :goto_2
    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/a/e;->c(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_3

    move-object v0, v3

    :goto_3
    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/a/e;->b(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    if-nez v4, :cond_4

    :goto_4
    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/e;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    .line 234
    iget-object v3, p0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cl;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 235
    iget-object v2, p0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->h()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/a/e;->b(Z)Lcom/sec/chaton/a/a/e;

    .line 239
    :goto_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/e;->a()Lcom/sec/chaton/a/a/d;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/d/a/bc;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 242
    return-void

    .line 218
    :cond_1
    const-string v0, "Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v3

    goto :goto_4

    .line 237
    :cond_5
    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/a/e;->b(Z)Lcom/sec/chaton/a/a/e;

    goto :goto_5
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 18

    .prologue
    .line 64
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    .line 65
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->o()Ljava/lang/String;

    move-result-object v5

    .line 69
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->m:Landroid/content/ContentResolver;

    invoke-static {v2, v5}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v14

    .line 70
    if-nez v14, :cond_1

    .line 71
    const-string v2, "Error - InBoxData does not exist."

    const-string v3, "ForwardStoredMessageTask"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v2, 0x0

    .line 199
    :goto_0
    return-object v2

    .line 78
    :cond_1
    iget v2, v14, Lcom/sec/chaton/e/a/p;->c:I

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/d/a/bc;->k:Lcom/sec/chaton/e/r;

    .line 80
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/d/a/bc;->j:Ljava/lang/String;

    .line 81
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 82
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 83
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 84
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 85
    const/4 v12, 0x0

    .line 86
    const/4 v3, 0x0

    .line 87
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->h()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/a/bc;->p:Z

    .line 89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    const-string v7, "ForwardStoredMessageTask"

    invoke-static {v2, v6, v7}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/cl;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/sec/chaton/a/co;->newBuilder()Lcom/sec/chaton/a/cp;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "uid"

    const-string v11, ""

    invoke-virtual {v6, v7, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/chaton/a/cp;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cp;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/chaton/a/cp;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cp;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/bc;->j:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/sec/chaton/a/cp;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cp;

    move-result-object v2

    const/16 v6, 0x64

    invoke-virtual {v2, v6}, Lcom/sec/chaton/a/cp;->a(I)Lcom/sec/chaton/a/cp;

    move-result-object v15

    .line 97
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Lcom/sec/chaton/a/cp;->d(Ljava/lang/String;)Lcom/sec/chaton/a/cp;

    .line 99
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/a/bc;->o:Z

    .line 102
    :cond_2
    const/4 v2, 0x0

    .line 103
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/chaton/d/a/bc;->o:Z

    if-nez v6, :cond_e

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/chaton/d/a/bc;->p:Z

    if-nez v6, :cond_e

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isFirstMsgOfFullMsg == true : isNeedNextPagination("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/chaton/d/a/bc;->o:Z

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "), hasMoreResult("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/chaton/d/a/bc;->p:Z

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v2, 0x1

    move v11, v2

    .line 108
    :goto_1
    new-instance v6, Lcom/sec/chaton/i/a/c;

    invoke-direct {v6}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 109
    const-string v2, "%04d%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/16 v16, 0x24

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v7, v13

    const/4 v13, 0x1

    const-string v16, "0001"

    aput-object v16, v7, v13

    invoke-static {v2, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 110
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->l:Lcom/sec/chaton/a/cl;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cl;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/a/cr;

    .line 112
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v16, ", "

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 115
    :cond_3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v13, 0x2

    if-le v2, v13, :cond_4

    .line 116
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 118
    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 119
    const-string v2, "CH"

    invoke-virtual {v6}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->b(Ljava/util/List;)V

    .line 124
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v13, v3

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/chaton/a/cr;

    .line 125
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/bc;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/bc;->k:Lcom/sec/chaton/e/r;

    invoke-virtual/range {v2 .. v10}, Lcom/sec/chaton/chat/fe;->a(ZLcom/sec/chaton/a/cr;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/util/ArrayList;Ljava/util/List;Ljava/util/HashMap;)Lcom/sec/chaton/chat/fg;

    move-result-object v3

    .line 126
    iget-boolean v2, v3, Lcom/sec/chaton/chat/fg;->c:Z

    if-eqz v2, :cond_5

    .line 127
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/d/a/bc;->n:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/d/a/bc;->n:I

    .line 130
    :cond_5
    if-nez v12, :cond_d

    iget-boolean v2, v3, Lcom/sec/chaton/chat/fg;->a:Z

    if-eqz v2, :cond_d

    move-object v2, v4

    .line 135
    :goto_4
    iget-boolean v6, v3, Lcom/sec/chaton/chat/fg;->b:Z

    if-eqz v6, :cond_6

    .line 136
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    .line 139
    :cond_6
    iget v3, v3, Lcom/sec/chaton/chat/fg;->d:I

    add-int/2addr v3, v13

    move v13, v3

    move-object v12, v2

    .line 140
    goto :goto_3

    .line 143
    :cond_7
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/sec/chaton/chat/fe;->a(Ljava/util/HashMap;)V

    .line 146
    if-eqz v11, :cond_8

    if-eqz v12, :cond_8

    .line 147
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bc;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/bc;->k:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v5, v3, v4, v12}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/cr;)Z

    move-result v2

    .line 149
    if-eqz v2, :cond_8

    .line 150
    const/16 v2, 0xb

    iput v2, v14, Lcom/sec/chaton/e/a/p;->m:I

    .line 155
    :cond_8
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/bc;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2, v5, v3, v4, v14}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;ILcom/sec/chaton/a/cr;Lcom/sec/chaton/e/a/p;)V

    .line 158
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    invoke-virtual {v2, v11, v8, v5}, Lcom/sec/chaton/chat/fe;->a(ZLjava/util/ArrayList;Ljava/lang/String;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bc;->k:Lcom/sec/chaton/e/r;

    invoke-static {v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v2

    .line 163
    if-eqz v2, :cond_9

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bc;->k:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/bc;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;)J

    .line 168
    :cond_9
    new-instance v3, Lcom/sec/chaton/i/a/c;

    invoke-direct {v3}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 169
    const-string v2, "%04d%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v6, 0x25

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0001"

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 170
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/a/cu;

    .line 173
    invoke-virtual {v15, v2}, Lcom/sec/chaton/a/cp;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cp;

    .line 174
    invoke-virtual {v2}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 177
    :cond_a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v5, 0x2

    if-le v2, v5, :cond_b

    .line 178
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 180
    :cond_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 181
    const-string v2, "CH"

    invoke-virtual {v3}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->i:Ljava/lang/String;

    invoke-virtual {v15}, Lcom/sec/chaton/a/cp;->d()Lcom/sec/chaton/a/co;

    move-result-object v3

    const-string v4, "ForwardStoredMessageTask"

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/co;Ljava/lang/String;)V

    .line 186
    new-instance v2, Lcom/sec/chaton/j/ap;

    invoke-direct {v2}, Lcom/sec/chaton/j/ap;-><init>()V

    const/16 v3, 0x25

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v2

    invoke-virtual {v15}, Lcom/sec/chaton/a/cp;->d()Lcom/sec/chaton/a/co;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v2

    .line 188
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bc;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v2}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bc;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->m()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 191
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/chaton/d/a/bd;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/chaton/d/a/bd;-><init>(Lcom/sec/chaton/d/a/bc;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_d
    move-object v2, v12

    goto/16 :goto_4

    :cond_e
    move v11, v2

    goto/16 :goto_1
.end method
