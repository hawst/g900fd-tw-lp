.class Lcom/sec/chaton/d/an;
.super Lcom/sec/chaton/push/i;
.source "PushControl.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/am;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/am;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    invoke-direct {p0}, Lcom/sec/chaton/push/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 101
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "registration Error. errorCode : %s, ErrorMessage : %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/d/ag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    iget-object v0, v0, Lcom/sec/chaton/d/am;->a:Lcom/sec/chaton/d/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/d/ag;->c()V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    invoke-static {v0}, Lcom/sec/chaton/d/am;->a(Lcom/sec/chaton/d/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 108
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 109
    const/16 v1, 0x3e9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 110
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 111
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 112
    iget-object v1, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    invoke-static {v1}, Lcom/sec/chaton/d/am;->a(Lcom/sec/chaton/d/am;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 114
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 79
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 80
    const-string v0, "registration Success. message : %s, regId : %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/d/ag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    sput-object p2, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    iget-object v0, v0, Lcom/sec/chaton/d/am;->a:Lcom/sec/chaton/d/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/d/ag;->c()V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    invoke-static {v0}, Lcom/sec/chaton/d/am;->a(Lcom/sec/chaton/d/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 92
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 93
    const/16 v1, 0x3e9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 94
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 95
    iget-object v1, p0, Lcom/sec/chaton/d/an;->a:Lcom/sec/chaton/d/am;

    invoke-static {v1}, Lcom/sec/chaton/d/am;->a(Lcom/sec/chaton/d/am;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 97
    :cond_1
    return-void
.end method
