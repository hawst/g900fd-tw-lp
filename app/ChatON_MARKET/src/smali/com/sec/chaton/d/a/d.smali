.class Lcom/sec/chaton/d/a/d;
.super Ljava/lang/Object;
.source "AbstractNetTask.java"

# interfaces
.implements Lcom/sec/chaton/j/t;


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/a/c;

.field private b:Lcom/sec/chaton/j/ao;

.field private c:Lcom/sec/chaton/j/ak;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/d/a/c;Lcom/sec/chaton/j/ao;Lcom/sec/chaton/j/ak;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/chaton/d/a/d;->a:Lcom/sec/chaton/d/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p2, p0, Lcom/sec/chaton/d/a/d;->b:Lcom/sec/chaton/j/ao;

    .line 168
    iput-object p3, p0, Lcom/sec/chaton/d/a/d;->c:Lcom/sec/chaton/j/ak;

    .line 169
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/chaton/d/a/d;->b:Lcom/sec/chaton/j/ao;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/d/a/d;->c:Lcom/sec/chaton/j/ak;

    iget-object v1, p0, Lcom/sec/chaton/d/a/d;->b:Lcom/sec/chaton/j/ao;

    invoke-static {v0, v1}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/sec/chaton/d/a/d;->b:Lcom/sec/chaton/j/ao;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/d/a/d;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 188
    const-string v0, "onPostExecute - start(error)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/d/a/d;->a:Lcom/sec/chaton/d/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 190
    const-string v0, "onPostExecute - end(error)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 181
    const-string v0, "onPostExecute - start"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/d/a/d;->a:Lcom/sec/chaton/d/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 183
    const-string v0, "onPostExecute - end"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method
