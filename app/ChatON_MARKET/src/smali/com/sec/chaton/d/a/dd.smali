.class public Lcom/sec/chaton/d/a/dd;
.super Lcom/sec/chaton/d/a/e;
.source "InviteChatTask.java"


# instance fields
.field private h:Lcom/sec/chaton/e/w;

.field private i:Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:[Ljava/lang/String;

.field private l:Landroid/content/ContentResolver;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/dd;->f:J

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/d/a/dd;->h:Lcom/sec/chaton/e/w;

    .line 50
    iput-object p3, p0, Lcom/sec/chaton/d/a/dd;->i:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/sec/chaton/d/a/dd;->j:[Ljava/lang/String;

    .line 52
    iput-object p5, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    .line 53
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    .line 54
    iput-object p6, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/e;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 120
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    .line 121
    const/16 v0, 0xc

    iput v0, v6, Landroid/os/Message;->what:I

    .line 123
    if-eqz p3, :cond_10

    .line 124
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 125
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/sec/chaton/a/dd;

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ch@t[InviteChatReply]UID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ResultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ResultMessage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", InBoxNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ReceivedTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->h()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MsgID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->d()I

    move-result v7

    .line 131
    const/16 v0, 0x3e8

    if-eq v7, v0, :cond_0

    const/16 v0, 0x3e9

    if-eq v7, v0, :cond_0

    const/16 v0, 0xbbd

    if-ne v7, v0, :cond_6

    .line 135
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 136
    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    array-length v4, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 137
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    const/16 v0, 0x3e9

    if-ne v7, v0, :cond_4

    .line 140
    const-string v0, "\\[.+?\\]"

    .line 142
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 143
    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 147
    :cond_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 148
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 149
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 151
    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v8, v4, v0

    .line 152
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 153
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 163
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    iget-object v4, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 174
    const-string v4, "%d,%s,%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    invoke-virtual {v9}, Lcom/sec/chaton/e/aj;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object v0, v5, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    invoke-static {v9, v0}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->h()J

    move-result-wide v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 193
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 235
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->b:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 252
    :goto_4
    return-void

    .line 195
    :cond_6
    const/16 v0, 0x1b59

    if-ne v7, v0, :cond_8

    .line 196
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->j:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->h()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 233
    :cond_7
    :goto_5
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_3

    .line 197
    :cond_8
    const/16 v0, 0x1b5a

    if-ne v7, v0, :cond_9

    .line 198
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->b:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_5

    .line 199
    :cond_9
    const/16 v0, 0x1b5b

    if-ne v7, v0, :cond_a

    .line 200
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_5

    .line 201
    :cond_a
    const/16 v0, 0x1b5e

    if-ne v7, v0, :cond_7

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "update invite list : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v3}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lcom/sec/chaton/a/ea;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dm;

    .line 206
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->d()Ljava/lang/String;

    move-result-object v3

    .line 207
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->f()Ljava/lang/String;

    move-result-object v4

    .line 208
    const-string v0, "(old:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ",new:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "),"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const/4 v0, 0x0

    :goto_6
    iget-object v5, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_b

    .line 211
    iget-object v5, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 212
    iget-object v5, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    aput-object v4, v5, v0

    .line 210
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 216
    :cond_d
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 218
    iget-object v3, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v4, :cond_f

    aget-object v5, v3, v0

    .line 219
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 220
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 223
    :cond_f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    .line 228
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "7006(NON_WEB_USER_DETECTED) - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InviteChatTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 239
    :cond_10
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/dd;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0xb

    if-eq p1, v0, :cond_11

    const/16 v0, 0x16

    if-eq p1, v0, :cond_11

    const/4 v0, 0x3

    if-ne p1, v0, :cond_13

    .line 241
    :cond_11
    const/16 v0, 0x18

    if-eq p1, v0, :cond_12

    const/16 v0, 0x17

    if-eq p1, v0, :cond_12

    const/16 v0, 0x15

    if-ne p1, v0, :cond_13

    .line 242
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 246
    :cond_13
    const-string v0, "Result NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->b:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-super {p0}, Lcom/sec/chaton/d/a/e;->b()Lcom/sec/chaton/j/ao;

    .line 62
    iget-wide v1, p0, Lcom/sec/chaton/d/a/dd;->f:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 63
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/d/a/dd;->f:J

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    iput-object v1, p0, Lcom/sec/chaton/d/a/dd;->i:Ljava/lang/String;

    .line 73
    :cond_1
    invoke-static {}, Lcom/sec/chaton/a/dg;->newBuilder()Lcom/sec/chaton/a/dh;

    move-result-object v1

    .line 74
    iget-wide v2, p0, Lcom/sec/chaton/d/a/dd;->f:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/dh;->a(J)Lcom/sec/chaton/a/dh;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/a/dd;->h:Lcom/sec/chaton/e/w;

    invoke-virtual {v3}, Lcom/sec/chaton/e/w;->a()I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dh;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/a/dd;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/dh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/dh;->b(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    .line 82
    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->j:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 83
    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->j:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/dh;->c(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    .line 87
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->k:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 88
    invoke-virtual {v1, v4}, Lcom/sec/chaton/a/dh;->d(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v2

    .line 95
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    .line 96
    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/dh;->b(J)Lcom/sec/chaton/a/dh;

    .line 101
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ch@t[InviteChatRequest]UID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", InBoxNo : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/dd;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", MsgID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", InvitingMembersCount : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->k()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", InvitingMembersList : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->j()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Sender : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", LastSessionMergeTime : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->l()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ReceiverCount : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->i()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ReceiversList : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->h()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/sec/chaton/j/ap;

    invoke-direct {v0}, Lcom/sec/chaton/j/ap;-><init>()V

    .line 107
    invoke-virtual {v1}, Lcom/sec/chaton/a/dh;->d()Lcom/sec/chaton/a/dg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    .line 110
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
