.class public Lcom/sec/chaton/d/ag;
.super Lcom/sec/chaton/d/a;
.source "PushControl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/d/a",
        "<",
        "Lcom/sec/chaton/push/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/d/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/ag;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/a;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/chaton/d/ag;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/ag;->b(Landroid/os/IBinder;)Lcom/sec/chaton/push/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/sec/chaton/d/am;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/am;-><init>(Lcom/sec/chaton/d/ag;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/ag;->a(Ljava/lang/Runnable;)V

    .line 36
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/chaton/d/al;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/al;-><init>(Lcom/sec/chaton/d/ag;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/ag;->a(Ljava/lang/Runnable;)V

    .line 56
    return-void
.end method

.method protected b(Landroid/os/IBinder;)Lcom/sec/chaton/push/e;
    .locals 1

    .prologue
    .line 60
    invoke-static {p1}, Lcom/sec/chaton/push/f;->a(Landroid/os/IBinder;)Lcom/sec/chaton/push/e;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/sec/chaton/d/ah;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/ah;-><init>(Lcom/sec/chaton/d/ag;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/ag;->a(Ljava/lang/Runnable;)V

    .line 41
    return-void
.end method

.method public c(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/sec/chaton/d/ak;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/ak;-><init>(Lcom/sec/chaton/d/ag;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/ag;->a(Ljava/lang/Runnable;)V

    .line 46
    return-void
.end method

.method public d(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/chaton/d/aj;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/aj;-><init>(Lcom/sec/chaton/d/ag;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/ag;->a(Ljava/lang/Runnable;)V

    .line 51
    return-void
.end method
