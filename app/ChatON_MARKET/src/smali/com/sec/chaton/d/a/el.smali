.class Lcom/sec/chaton/d/a/el;
.super Ljava/lang/Object;
.source "UploadAddressTask.java"

# interfaces
.implements Lcom/sec/chaton/m/c;


# instance fields
.field a:Z

.field final synthetic b:Lcom/sec/chaton/d/a/ek;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/a/ek;)V
    .locals 1

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/chaton/d/a/el;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x1

    .line 165
    const-string v0, "onNumberInserted"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->a(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v6

    new-instance v0, Lcom/sec/chaton/m/d;

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/chaton/e/a/g;->a(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/el;->a:Z

    if-eqz v0, :cond_1

    .line 177
    invoke-static {v5}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->c(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_1

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_1
    return-void
.end method

.method public b(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 197
    .line 198
    const-string v0, "onNumberDeleted"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->d(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v6

    new-instance v0, Lcom/sec/chaton/m/d;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x3

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/g;->a(I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Lcom/sec/chaton/e/a/g;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-static {v2}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->c(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_0
    return-void
.end method

.method public c(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 222
    .line 225
    const-string v0, "onNumberChanged"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 228
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->a(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v6

    new-instance v0, Lcom/sec/chaton/m/d;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->d(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v6, Lcom/sec/chaton/m/d;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v6 .. v11}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "conatct_number"

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/chaton/e/a/g;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-static {v11}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    .line 238
    iget-object v1, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->c(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_0
    return-void
.end method

.method public d(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x3

    .line 248
    .line 250
    const-string v0, "onNameChanged"

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->b(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "contacts_name"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/chaton/e/a/g;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v0}, Lcom/sec/chaton/d/a/ek;->a(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v6

    new-instance v0, Lcom/sec/chaton/m/d;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/m/d;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onNameChanged: buddy db has user of Contact rawid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", buddyName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UploadAddressTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-static {v5}, Lcom/sec/chaton/e/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    iget-object v1, p0, Lcom/sec/chaton/d/a/el;->b:Lcom/sec/chaton/d/a/ek;

    invoke-static {v1}, Lcom/sec/chaton/d/a/ek;->c(Lcom/sec/chaton/d/a/ek;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    :cond_0
    return-void
.end method
