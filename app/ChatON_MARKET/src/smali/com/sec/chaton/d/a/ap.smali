.class public Lcom/sec/chaton/d/a/ap;
.super Lcom/sec/chaton/d/a/c;
.source "DeliveryChatReplyTask.java"


# instance fields
.field private h:Ljava/lang/String;

.field private i:Lcom/sec/chaton/chat/es;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/c;-><init>(Landroid/os/Handler;)V

    .line 26
    iput-object p2, p0, Lcom/sec/chaton/d/a/ap;->h:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 11

    .prologue
    const/4 v10, 0x7

    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/d/a/ap;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/es;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/es;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ap;->i:Lcom/sec/chaton/chat/es;

    .line 34
    new-instance v2, Lcom/sec/chaton/i/a/c;

    invoke-direct {v2}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 35
    const-string v0, "%04d%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    const-string v5, "0001"

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 37
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    invoke-static {}, Lcom/sec/chaton/a/bj;->newBuilder()Lcom/sec/chaton/a/bk;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bk;

    move-result-object v4

    .line 43
    iget-object v0, p0, Lcom/sec/chaton/d/a/ap;->i:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cr;

    .line 63
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/a/bm;->a(J)Lcom/sec/chaton/a/bm;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/chaton/a/bm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;

    move-result-object v6

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/chaton/a/bm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/bm;->d()Lcom/sec/chaton/a/bl;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bk;

    .line 65
    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 70
    const-string v0, "CH"

    invoke-virtual {v2}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/d/a/ap;->i:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->a()V

    .line 83
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 85
    :goto_1
    invoke-virtual {v4}, Lcom/sec/chaton/a/bk;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] Receiver : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/bl;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", MsgType : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/bl;->j()Lcom/sec/chaton/a/dp;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", MsgID : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/bl;->h()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", Sender : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/bl;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 89
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ch@t["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "uid"

    const-string v5, ""

    invoke-virtual {v1, v3, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DeliveryChatReplyItemsCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/sec/chaton/a/bk;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_2
    new-instance v0, Lcom/sec/chaton/j/ap;

    invoke-direct {v0}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {v0, v10}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-virtual {v4}, Lcom/sec/chaton/a/bk;->d()Lcom/sec/chaton/a/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
