.class public Lcom/sec/chaton/d/a/eb;
.super Lcom/sec/chaton/d/a/a;
.source "TranslateTask.java"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 27
    const-class v0, Lcom/sec/chaton/d/a/eb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/eb;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/sec/chaton/d/a/eb;->c:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 9

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    .line 63
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    .line 64
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v4

    .line 66
    sget-object v0, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v3, v0, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/TranslationEntry;

    .line 71
    :goto_0
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v5, Lcom/sec/chaton/chat/b/m;->e:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v5, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V

    .line 73
    :try_start_0
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_0

    .line 74
    const-string v5, "afterRequest(), connectionSuccess(%s), httpResultCode(%s), httpStatus(%d)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 78
    iget-object v6, p0, Lcom/sec/chaton/d/a/eb;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    sget-object v5, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v3, v5, :cond_2

    .line 82
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V

    invoke-virtual {p1, v1}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_1
    return-void

    .line 69
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/TranslationEntry;

    goto :goto_0

    .line 86
    :cond_2
    if-eqz v4, :cond_3

    if-nez v0, :cond_4

    .line 88
    :cond_3
    :try_start_1
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->b:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V

    .line 89
    invoke-virtual {p1, v1}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 105
    :catch_0
    move-exception v1

    .line 106
    iget-object v2, p0, Lcom/sec/chaton/d/a/eb;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 107
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->f:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V

    move-object v0, v1

    .line 110
    :goto_2
    invoke-virtual {p1, v0}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 92
    :cond_4
    sparse-switch v2, :sswitch_data_0

    :cond_5
    move-object v0, v1

    goto :goto_2

    .line 94
    :sswitch_0
    :try_start_2
    iget-object v2, v0, Lcom/sec/chaton/io/entry/TranslationEntry;->rcode:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x4a40

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 95
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->d:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V

    move-object v0, v1

    goto :goto_2

    .line 102
    :sswitch_1
    new-instance v1, Lcom/sec/chaton/d/a/ee;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->c:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/chaton/d/a/ee;-><init>(Lcom/sec/chaton/d/a/eb;Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationEntry;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    goto :goto_2

    .line 92
    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x194 -> :sswitch_1
        0x198 -> :sswitch_1
        0x1f4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 51
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/d/a/eb;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/b/n;->b(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 37
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 39
    :try_start_0
    const-string v0, "source_text"

    iget-object v2, p0, Lcom/sec/chaton/d/a/eb;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    iget-object v2, p0, Lcom/sec/chaton/d/a/eb;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
