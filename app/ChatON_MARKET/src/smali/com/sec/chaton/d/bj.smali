.class public Lcom/sec/chaton/d/bj;
.super Ljava/lang/Object;
.source "VersionControl.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/sec/chaton/d/bj;->a:Landroid/os/Handler;

    .line 56
    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/chaton/d/bj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/bj;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 62
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v1, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v2, "/version"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x44d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/GetVersion;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 64
    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "version"

    sget-object v3, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 68
    sget-object v1, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    const-string v1, "regid"

    sget-object v2, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "pushtype"

    const-string v3, "SPP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 73
    :cond_0
    const-string v1, "region"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "primary_region"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 75
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/cu;

    iget-object v3, p0, Lcom/sec/chaton/d/bj;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/sec/chaton/d/a/cu;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 76
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 220
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "primary_contact_addrss"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 223
    :cond_0
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v1, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v2, "/disclaimer/accept"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x451

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 227
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/f;

    iget-object v3, p0, Lcom/sec/chaton/d/bj;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1}, Lcom/sec/chaton/d/a/f;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/d/a/ct;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 171
    const-string v0, "enter getVersionNoti"

    const-string v2, "ChatON"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "primary_contact_addrss"

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 214
    :goto_0
    return-object v0

    .line 179
    :cond_0
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v3, "/version/versionnotidis"

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v2, 0x450

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/io/entry/GetVersionNotice;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v2

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exist iso from sim : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "iso from file : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "country_letter"

    const-string v5, "GB"

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v0, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "iso"

    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_1

    const-string v0, "GB"

    :goto_1
    invoke-virtual {v3, v4, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "platform"

    const-string v4, "android"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "osversion"

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "model"

    invoke-static {}, Lcom/sec/chaton/util/am;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "appversion"

    sget-object v4, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "language"

    invoke-static {}, Lcom/sec/chaton/util/am;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "timestamp"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "accept_time"

    const-string v6, "0"

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "screensize"

    invoke-static {}, Lcom/sec/chaton/util/am;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "samsungapps"

    invoke-static {}, Lcom/sec/chaton/util/am;->m()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "iso2"

    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_2

    const-string v0, "GB"

    :goto_2
    invoke-virtual {v3, v4, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v3, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 210
    new-instance v0, Lcom/sec/chaton/d/a/ct;

    iget-object v1, p0, Lcom/sec/chaton/d/bj;->a:Landroid/os/Handler;

    invoke-virtual {v2}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/a/ct;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 212
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    goto/16 :goto_0

    .line 188
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public c()V
    .locals 5

    .prologue
    .line 234
    const-string v0, ""

    .line 236
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/am;->p()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 242
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->g:Lcom/sec/chaton/util/cg;

    const-string v3, "/product/appCheck.as"

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const/16 v2, 0x452

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/io/entry/AvaliableApps;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v2

    .line 244
    const-string v1, "appInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.sec.chaton@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v3, "deviceId"

    invoke-static {}, Lcom/sec/chaton/util/am;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "mcc"

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v4, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "mnc"

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v4, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v3, "csc"

    invoke-virtual {v1, v3, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "openApi"

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "pd"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 257
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/d/a/u;

    iget-object v3, p0, Lcom/sec/chaton/d/bj;->a:Landroid/os/Handler;

    invoke-virtual {v2}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lcom/sec/chaton/d/a/u;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 258
    return-void

    .line 237
    :catch_0
    move-exception v1

    .line 239
    const-string v2, "there is exception "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 244
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/am;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
