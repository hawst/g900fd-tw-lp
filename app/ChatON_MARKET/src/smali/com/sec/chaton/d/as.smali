.class public Lcom/sec/chaton/d/as;
.super Ljava/lang/Object;
.source "SPPUpdateControl.java"


# static fields
.field private static c:Ljava/lang/String;


# instance fields
.field final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private d:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 45
    const-string v1, "com.sec.spp.push"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 46
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/d/as;->c:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    const-string v0, "SPPPushClient is not installed yet."

    const-string v1, "SPPVersionControl()"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "com.sec.android.app.samsungapps"

    iput-object v0, p0, Lcom/sec/chaton/d/as;->a:Ljava/lang/String;

    .line 38
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/as;->b:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/sec/chaton/d/as;->d:Landroid/os/Handler;

    .line 56
    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/sec/chaton/d/as;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/chaton/d/as;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/as;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 59
    const-string v0, "com.sec.android.app.samsungapps"

    invoke-static {p1, v0}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 60
    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const-string v0, ""

    .line 66
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/am;->p()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    :goto_1
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->g:Lcom/sec/chaton/util/cg;

    const-string v3, "/product/appCheck.as"

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const/16 v2, 0x452

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/io/entry/AvaliableApps;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v2

    .line 74
    const-string v1, "appInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.sec.spp.push@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/d/as;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v3, "deviceId"

    invoke-static {}, Lcom/sec/chaton/util/am;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "mcc"

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v4, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v3

    const-string v4, "mnc"

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v3, v4, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v3, "csc"

    invoke-virtual {v1, v3, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "openApi"

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "pd"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 88
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/d/a/u;

    iget-object v3, p0, Lcom/sec/chaton/d/as;->d:Landroid/os/Handler;

    invoke-virtual {v2}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lcom/sec/chaton/d/a/u;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    goto/16 :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 69
    const-string v2, "progressSPPUpdate() : IOException at DeviceInfoUtil.getCSC()"

    iget-object v3, p0, Lcom/sec/chaton/d/as;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 74
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/am;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
