.class Lcom/sec/chaton/d/ak;
.super Ljava/lang/Object;
.source "PushControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/ag;

.field private b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/d/ag;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/chaton/d/ak;->a:Lcom/sec/chaton/d/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    iput-object p2, p0, Lcom/sec/chaton/d/ak;->b:Landroid/os/Handler;

    .line 216
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/ak;->a:Lcom/sec/chaton/d/ag;

    iget-object v0, v0, Lcom/sec/chaton/d/ag;->b:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/push/e;

    invoke-interface {v0}, Lcom/sec/chaton/push/e;->a()Z

    move-result v0

    .line 223
    iget-object v1, p0, Lcom/sec/chaton/d/ak;->b:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 224
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 225
    const/16 v2, 0x3eb

    iput v2, v1, Landroid/os/Message;->what:I

    .line 226
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/d/ak;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/ak;->a:Lcom/sec/chaton/d/ag;

    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/d/ag;->c()V

    .line 245
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 231
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 232
    invoke-static {}, Lcom/sec/chaton/d/ag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/ak;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 236
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 237
    const/16 v1, 0x3eb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 238
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 240
    iget-object v1, p0, Lcom/sec/chaton/d/ak;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/ak;->a:Lcom/sec/chaton/d/ag;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/d/ak;->a:Lcom/sec/chaton/d/ag;

    invoke-virtual {v1}, Lcom/sec/chaton/d/ag;->c()V

    throw v0
.end method
