.class public Lcom/sec/chaton/d/ao;
.super Ljava/lang/Object;
.source "PushControlFactory.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/d/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/d/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/ao;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/chaton/d/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 14
    invoke-static {}, Lcom/sec/chaton/d/ao;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15
    invoke-static {}, Lcom/sec/chaton/d/z;->e()Lcom/sec/chaton/d/z;

    move-result-object v0

    .line 20
    :goto_0
    return-object v0

    .line 16
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/ao;->b:Lcom/sec/chaton/d/ag;

    if-nez v0, :cond_1

    .line 17
    new-instance v0, Lcom/sec/chaton/d/ag;

    invoke-direct {v0}, Lcom/sec/chaton/d/ag;-><init>()V

    sput-object v0, Lcom/sec/chaton/d/ao;->b:Lcom/sec/chaton/d/ag;

    .line 20
    :cond_1
    sget-object v0, Lcom/sec/chaton/d/ao;->b:Lcom/sec/chaton/d/ag;

    goto :goto_0
.end method

.method public static b()Z
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.spp.push"

    invoke-static {v0, v1}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 43
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 44
    const-string v1, "Public push service is insatlled, using it."

    sget-object v2, Lcom/sec/chaton/d/ao;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_0
    return v0
.end method
