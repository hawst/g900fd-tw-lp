.class public Lcom/sec/chaton/d/n;
.super Ljava/lang/Object;
.source "IgnoreControl.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/chaton/d/n;->a:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/sec/chaton/d/n;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sec/chaton/d/n;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/n;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v1, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v2, "/buddyrecommendee/ignorelist"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 39
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/cx;

    iget-object v3, p0, Lcom/sec/chaton/d/n;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/sec/chaton/d/a/cx;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 40
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 60
    if-lez v0, :cond_0

    .line 61
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v1, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v2, "/buddyrecommendee/ignorelist"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/chaton/j/k;->b:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 63
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/d/a/cx;

    iget-object v3, p0, Lcom/sec/chaton/d/n;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    const-string v4, "true"

    invoke-direct {v2, v3, v0, v4, p1}, Lcom/sec/chaton/d/a/cx;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 65
    :cond_0
    return-void
.end method
