.class Lcom/sec/chaton/d/p;
.super Landroid/os/Handler;
.source "MessageControl2.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/o;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/o;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/chaton/d/p;->a:Lcom/sec/chaton/d/o;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 22

    .prologue
    .line 284
    if-nez p1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    .line 290
    packed-switch v19, :pswitch_data_0

    goto :goto_0

    .line 293
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v18, v4

    check-cast v18, Lcom/sec/chaton/j/c/k;

    .line 294
    if-eqz v18, :cond_0

    .line 295
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/j/c/k;->b()J

    move-result-wide v5

    .line 296
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    .line 297
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v21, v0

    .line 298
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/j/c/k;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 299
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/j/c/g;->b(J)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 300
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v17

    .line 304
    if-eqz v17, :cond_3

    .line 307
    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->f()Ljava/lang/String;

    move-result-object v10

    .line 308
    const/4 v12, 0x0

    .line 309
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 310
    const/4 v12, 0x1

    .line 314
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/p;->a:Lcom/sec/chaton/d/o;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->c()Lcom/sec/chaton/e/w;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->b()Lcom/sec/chaton/e/r;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->e()[Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->g()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->k()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->h()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->j()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v17 .. v17}, Lcom/sec/chaton/j/c/i;->i()Lcom/sec/chaton/msgsend/k;

    move-result-object v17

    invoke-static/range {v4 .. v17}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)Z

    move-result v4

    .line 325
    if-nez v4, :cond_3

    .line 327
    if-eqz v21, :cond_4

    .line 328
    new-instance v11, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    move/from16 v0, v21

    invoke-direct {v11, v4, v0}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    .line 332
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/p;->a:Lcom/sec/chaton/d/o;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x65

    move-wide v12, v5

    invoke-static/range {v7 .. v13}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 360
    :cond_3
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v18

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/p;->a(IIILcom/sec/chaton/j/c/k;)V

    .line 361
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/j/c/g;->c(J)Z

    goto/16 :goto_0

    .line 330
    :cond_4
    new-instance v11, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    sget-object v7, Lcom/sec/chaton/a/a/n;->d:Lcom/sec/chaton/a/a/n;

    invoke-direct {v11, v4, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZLcom/sec/chaton/a/a/n;)V

    goto :goto_1

    .line 339
    :cond_5
    new-instance v11, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x1

    sget-object v7, Lcom/sec/chaton/a/a/n;->b:Lcom/sec/chaton/a/a/n;

    invoke-direct {v11, v4, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZLcom/sec/chaton/a/a/n;)V

    .line 340
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v4

    .line 341
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/sec/chaton/j/c/i;->k()Ljava/lang/String;

    move-result-object v4

    .line 342
    :goto_3
    iput-object v4, v11, Lcom/sec/chaton/a/a/k;->c:Ljava/lang/String;

    .line 343
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/p;->a:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p1

    iget v8, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v9, v0, Landroid/os/Message;->arg2:I

    const/16 v10, 0x65

    move-wide v12, v5

    invoke-static/range {v7 .. v13}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto :goto_2

    .line 341
    :cond_6
    const-string v4, ""

    goto :goto_3

    .line 351
    :cond_7
    if-eqz v21, :cond_8

    .line 352
    new-instance v11, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    move/from16 v0, v21

    invoke-direct {v11, v4, v0}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    .line 356
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/p;->a:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p1

    iget v8, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v9, v0, Landroid/os/Message;->arg2:I

    const/16 v10, 0x65

    move-wide v12, v5

    invoke-static/range {v7 .. v13}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto :goto_2

    .line 354
    :cond_8
    new-instance v11, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    sget-object v7, Lcom/sec/chaton/a/a/n;->d:Lcom/sec/chaton/a/a/n;

    invoke-direct {v11, v4, v7}, Lcom/sec/chaton/a/a/k;-><init>(ZLcom/sec/chaton/a/a/n;)V

    goto :goto_4

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
