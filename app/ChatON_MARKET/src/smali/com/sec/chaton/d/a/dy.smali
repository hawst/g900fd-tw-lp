.class public Lcom/sec/chaton/d/a/dy;
.super Lcom/sec/chaton/d/a/a;
.source "SetSNSIdTask.java"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 27
    iput-object p3, p0, Lcom/sec/chaton/d/a/dy;->b:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/sec/chaton/d/a/dy;->c:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 2

    .prologue
    .line 55
    const-string v0, "in SetSNSIdTask.afterRequest()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    new-instance v1, Lcom/sec/chaton/io/entry/inner/SnsId;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/inner/SnsId;-><init>()V

    .line 37
    iget-object v2, p0, Lcom/sec/chaton/d/a/dy;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/sec/chaton/d/a/dy;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/SnsId;->sp:Ljava/lang/String;

    .line 40
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/d/a/dy;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 41
    iget-object v2, p0, Lcom/sec/chaton/d/a/dy;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/SnsId;->value:Ljava/lang/String;

    .line 44
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v1, Lcom/sec/chaton/util/af;

    invoke-direct {v1}, Lcom/sec/chaton/util/af;-><init>()V

    .line 47
    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-object v0
.end method
