.class public Lcom/sec/chaton/d/a/o;
.super Lcom/sec/chaton/d/a/c;
.source "AnnounceChangeTask.java"


# instance fields
.field private h:Lcom/sec/chaton/a/l;

.field private i:Ljava/lang/String;

.field private j:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/a/l;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/c;-><init>(Landroid/os/Handler;)V

    .line 36
    iput-object p2, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    .line 37
    iput-object p3, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    .line 38
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    .line 39
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 43
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 45
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 46
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Member = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", Name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v0}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", Status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v0}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->d()Lcom/sec/chaton/a/au;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ch@t["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "UID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", MsgID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->d()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", SessionID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", GroupMemberListCount : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->j()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", GroupMemberList : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->i()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", ChangeMemberListCount : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->h()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", ChangeMemberList : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Inviter : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v2}, Lcom/sec/chaton/a/l;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_1
    const-string v0, ""

    .line 58
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v1

    .line 59
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v0}, Lcom/sec/chaton/a/l;->h()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v4, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/aj;->d()Lcom/sec/chaton/a/au;

    move-result-object v0

    invoke-static {v1}, Lcom/sec/chaton/a/au;->a(I)Lcom/sec/chaton/a/au;

    move-result-object v5

    if-ne v0, v5, :cond_4

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    invoke-static {v0, v5, v6}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 72
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    .line 73
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "chaton_id"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v6}, Lcom/sec/chaton/a/l;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 74
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 77
    :cond_2
    const-string v5, "%d,%s,%s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/e/aj;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v6, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6, v4}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 59
    :cond_3
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 81
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/aj;->d()Lcom/sec/chaton/a/au;

    move-result-object v0

    invoke-static {v8}, Lcom/sec/chaton/a/au;->a(I)Lcom/sec/chaton/a/au;

    move-result-object v5

    if-ne v0, v5, :cond_3

    .line 83
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 84
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 85
    const/16 v4, 0x10

    iput v4, v0, Landroid/os/Message;->what:I

    .line 86
    new-instance v4, Lcom/sec/chaton/a/a/k;

    const/16 v5, 0xbbe

    invoke-direct {v4, v8, v5}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 87
    iget-object v4, p0, Lcom/sec/chaton/d/a/o;->b:Landroid/os/Handler;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 89
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    invoke-static {v0, v5, v6}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    const-string v0, "%d,%s,%s"

    new-array v5, v10, [Ljava/lang/Object;

    sget-object v6, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v6}, Lcom/sec/chaton/e/aj;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v6, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v9

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/l;->a(I)Lcom/sec/chaton/a/aj;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 98
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v0}, Lcom/sec/chaton/a/l;->n()Ljava/lang/String;

    move-result-object v6

    .line 100
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 101
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 103
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v3}, Lcom/sec/chaton/a/l;->l()J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v5}, Lcom/sec/chaton/a/l;->d()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 112
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/o;->j:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/o;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 117
    invoke-static {}, Lcom/sec/chaton/a/o;->newBuilder()Lcom/sec/chaton/a/p;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lcom/sec/chaton/d/a/o;->h:Lcom/sec/chaton/a/l;

    invoke-virtual {v1}, Lcom/sec/chaton/a/l;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/p;->a(J)Lcom/sec/chaton/a/p;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/ek;->a(I)Lcom/sec/chaton/a/ek;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/p;

    .line 121
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_9

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ch@t["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MsgID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/p;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_9
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/a/p;->d()Lcom/sec/chaton/a/o;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
