.class Lcom/sec/chaton/d/r;
.super Landroid/os/Handler;
.source "MessageControl2.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/o;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/o;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 1117
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/d/bi;

    .line 1119
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1120
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 1121
    iget v2, p1, Landroid/os/Message;->arg2:I

    iput v2, v1, Landroid/os/Message;->arg2:I

    .line 1122
    const/16 v2, 0x67

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1123
    new-instance v2, Lcom/sec/chaton/d/bi;

    const-wide/16 v3, -0x1

    invoke-virtual {v0}, Lcom/sec/chaton/d/bi;->c()Lcom/sec/chaton/d/bh;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/sec/chaton/d/bi;-><init>(JLcom/sec/chaton/d/bh;Ljava/lang/Object;)V

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1125
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->f(Lcom/sec/chaton/d/o;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1126
    return-void
.end method

.method private a(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V
    .locals 8

    .prologue
    .line 1129
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/d/bi;

    .line 1130
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 1132
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_1

    .line 1133
    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134
    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->f()V

    .line 1135
    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1144
    :goto_0
    return-void

    .line 1137
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 1138
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1141
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 1142
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private b(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V
    .locals 9

    .prologue
    .line 1147
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/d/bi;

    .line 1148
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/a/a/k;

    .line 1151
    const/16 v0, 0x66

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_1

    .line 1152
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v5

    .line 1156
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 1157
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bd;

    .line 1158
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v0

    .line 1159
    instance-of v1, v0, Lcom/sec/chaton/d/a/ds;

    if-eqz v1, :cond_2

    .line 1160
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v1

    .line 1161
    check-cast v0, Lcom/sec/chaton/d/a/ds;

    .line 1162
    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/a/ds;->a(J)I

    move-result v0

    .line 1163
    if-gtz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169
    :cond_0
    :goto_1
    return-void

    .line 1154
    :cond_1
    invoke-virtual {p2}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    goto :goto_0

    .line 1167
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    .line 494
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/d/bi;

    .line 496
    if-nez v7, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/d/bd;

    .line 501
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 505
    const-string v0, "oResult.getResultEntry() is null"

    const-string v1, "MessageControl"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 510
    :cond_2
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 511
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 512
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v2

    .line 513
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    .line 515
    if-nez v8, :cond_5

    .line 516
    const-string v4, "MessageControl"

    const-string v5, " [mCONTROL_HANDLER]   (TaskContainer is NULL), gpbType(%s, %d), inboxNo(%s), msgId(%d), resultCode(%d), result(%s)"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Lcom/sec/chaton/j/g;->c(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x1

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v10}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v9

    const/4 v2, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    const/4 v1, 0x5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    :goto_1
    if-eqz v8, :cond_2b

    .line 528
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 529
    new-instance v1, Lcom/sec/chaton/i/a/c;

    invoke-direct {v1}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 530
    const-string v2, "%04d%04d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 531
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 534
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_3

    .line 535
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 536
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 540
    :cond_3
    const-string v0, "CH"

    invoke-virtual {v1}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 990
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->e()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 991
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->f()V

    .line 992
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    .line 1113
    :cond_4
    :goto_2
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;)Ljava/lang/String;

    move-result-object v2

    if-eqz v8, :cond_37

    const/4 v0, 0x1

    :goto_3
    invoke-static {v1, v2, v7, v0}, Lcom/sec/chaton/msgsend/p;->a(ILjava/lang/String;Lcom/sec/chaton/d/bi;Z)V

    goto/16 :goto_0

    .line 520
    :cond_5
    const-string v4, "MessageControl"

    const-string v5, " [mCONTROL_HANDLER]   TaskContainerID(%d), gpbType(%s, %d), inboxNo(%s), msgId(%d), resultCode(%d), result(%s)"

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x1

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Lcom/sec/chaton/j/g;->c(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x2

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x3

    iget-object v10, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v10}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    const/4 v9, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v9

    const/4 v2, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    const/4 v1, 0x6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 563
    :sswitch_0
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 564
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 565
    const/16 v2, 0x7d6

    if-eq v1, v2, :cond_6

    const/16 v2, 0xbc0

    if-ne v1, v2, :cond_8

    .line 568
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget-object v2, v0, Lcom/sec/chaton/a/a/k;->c:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 570
    iget-object v1, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inboxNO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Z)Z

    .line 577
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 578
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->f()V

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    .line 595
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 596
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 600
    :cond_8
    const/16 v0, 0x15

    if-eq v1, v0, :cond_9

    const/4 v0, 0x2

    if-eq v1, v0, :cond_9

    const/16 v0, 0x1f

    if-eq v1, v0, :cond_9

    const/16 v0, 0x21

    if-eq v1, v0, :cond_9

    const/16 v0, 0x19

    if-eq v1, v0, :cond_9

    const/16 v0, 0x18

    if-eq v1, v0, :cond_9

    const/16 v0, 0x17

    if-eq v1, v0, :cond_9

    const/16 v0, 0x1b5a

    if-eq v1, v0, :cond_9

    const/16 v0, 0x1b5b

    if-eq v1, v0, :cond_9

    const/16 v0, 0x23

    if-ne v1, v0, :cond_b

    .line 611
    :cond_9
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/c;

    .line 613
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 614
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 616
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto/16 :goto_2

    .line 635
    :cond_b
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 643
    :sswitch_1
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 644
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 645
    const/16 v2, 0x15

    if-eq v1, v2, :cond_c

    const/4 v2, 0x2

    if-eq v1, v2, :cond_c

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_c

    const/16 v2, 0x21

    if-eq v1, v2, :cond_c

    const/16 v2, 0x23

    if-eq v1, v2, :cond_c

    const/16 v2, 0x19

    if-eq v1, v2, :cond_c

    const/16 v2, 0x18

    if-eq v1, v2, :cond_c

    const/16 v2, 0x17

    if-ne v1, v2, :cond_f

    .line 654
    :cond_c
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/d/a/c;

    .line 658
    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/sec/chaton/d/a/c;->a()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 659
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 667
    :cond_d
    const/16 v1, 0x6a

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_e

    .line 668
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v5

    .line 673
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 674
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;Z)V

    goto/16 :goto_2

    .line 670
    :cond_e
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    goto :goto_4

    .line 680
    :cond_f
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v1, :cond_14

    .line 681
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->e()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 682
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->f()V

    .line 683
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 691
    :cond_10
    const/16 v1, 0x6a

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_12

    .line 692
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v5

    .line 697
    :goto_5
    iget-object v1, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Lcom/sec/chaton/d/o;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 698
    :try_start_0
    instance-of v2, v0, Lcom/sec/chaton/d/a/cz;

    if-eqz v2, :cond_13

    .line 699
    iget-object v2, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    check-cast v0, Lcom/sec/chaton/d/a/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Ljava/lang/String;)Ljava/lang/String;

    .line 703
    :cond_11
    :goto_6
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 705
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;Z)V

    goto/16 :goto_2

    .line 694
    :cond_12
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    goto :goto_5

    .line 700
    :cond_13
    :try_start_1
    instance-of v2, v0, Lcom/sec/chaton/d/a/dh;

    if-eqz v2, :cond_11

    .line 701
    iget-object v2, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    check-cast v0, Lcom/sec/chaton/d/a/dh;

    iget-object v0, v0, Lcom/sec/chaton/d/a/dh;->d:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_6

    .line 703
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 710
    :cond_14
    const/16 v1, 0x6a

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_15

    .line 711
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v5

    .line 716
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 717
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 718
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/d/o;Z)V

    goto/16 :goto_2

    .line 713
    :cond_15
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    goto :goto_7

    .line 728
    :sswitch_2
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 729
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 730
    const/16 v1, 0x15

    if-eq v0, v1, :cond_16

    const/4 v1, 0x2

    if-eq v0, v1, :cond_16

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_16

    const/16 v1, 0x21

    if-eq v0, v1, :cond_16

    const/16 v1, 0x19

    if-eq v0, v1, :cond_16

    const/16 v1, 0x18

    if-eq v0, v1, :cond_16

    const/16 v1, 0x17

    if-eq v0, v1, :cond_16

    const/16 v1, 0x1b5a

    if-eq v0, v1, :cond_16

    const/16 v1, 0x1b5b

    if-eq v0, v1, :cond_16

    const/16 v1, 0x1b5e

    if-eq v0, v1, :cond_16

    const/16 v1, 0x23

    if-ne v0, v1, :cond_18

    .line 734
    :cond_16
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/c;

    .line 737
    if-eqz v0, :cond_17

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 738
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 740
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 741
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 757
    :cond_18
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 766
    :sswitch_3
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 773
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    .line 774
    const/16 v1, 0x15

    if-eq v2, v1, :cond_19

    const/4 v1, 0x2

    if-eq v2, v1, :cond_19

    const/16 v1, 0x1f

    if-eq v2, v1, :cond_19

    const/16 v1, 0x21

    if-eq v2, v1, :cond_19

    const/16 v1, 0x19

    if-eq v2, v1, :cond_19

    const/16 v1, 0x18

    if-eq v2, v1, :cond_19

    const/16 v1, 0x17

    if-eq v2, v1, :cond_19

    const/16 v1, 0x1a

    if-eq v2, v1, :cond_19

    const/16 v1, 0x1b5a

    if-eq v2, v1, :cond_19

    const/16 v1, 0x1b5b

    if-eq v2, v1, :cond_19

    const/16 v1, 0x23

    if-ne v2, v1, :cond_1e

    .line 778
    :cond_19
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/c;

    .line 780
    sget-object v1, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    .line 781
    instance-of v3, v0, Lcom/sec/chaton/d/a/s;

    if-eqz v3, :cond_1a

    move-object v1, v0

    .line 782
    check-cast v1, Lcom/sec/chaton/d/a/s;

    invoke-virtual {v1}, Lcom/sec/chaton/d/a/s;->c()Lcom/sec/chaton/msgsend/k;

    move-result-object v1

    .line 785
    :cond_1a
    if-eqz v0, :cond_1d

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_1b

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_1d

    :cond_1b
    invoke-virtual {v0}, Lcom/sec/chaton/d/a/c;->a()Z

    move-result v0

    if-nez v0, :cond_1c

    const/16 v0, 0x1a

    if-eq v2, v0, :cond_1c

    const/16 v0, 0x1b5a

    if-eq v2, v0, :cond_1c

    const/16 v0, 0x1b5b

    if-ne v2, v0, :cond_1d

    .line 792
    :cond_1c
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 817
    :cond_1d
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->b(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 821
    :cond_1e
    const/16 v1, 0x7d6

    if-eq v2, v1, :cond_1f

    const/16 v1, 0xbc0

    if-ne v2, v1, :cond_20

    .line 823
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget-object v0, v0, Lcom/sec/chaton/a/a/k;->c:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->b()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 827
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    .line 828
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Z)Z

    .line 833
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 845
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    .line 846
    :cond_20
    const/16 v1, 0x384

    if-ne v2, v1, :cond_21

    .line 851
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    .line 867
    :cond_21
    const/16 v1, 0x1772

    if-ne v1, v2, :cond_22

    .line 868
    new-instance v1, Lcom/sec/chaton/d/h;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v1}, Lcom/sec/chaton/d/h;->a()V

    .line 871
    :cond_22
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_24

    .line 873
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->e()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 874
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->f()V

    .line 875
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 896
    :cond_23
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->b(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 919
    :cond_24
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->b(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 929
    :sswitch_4
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 930
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 931
    const/16 v1, 0x15

    if-eq v0, v1, :cond_25

    const/4 v1, 0x2

    if-eq v0, v1, :cond_25

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_25

    const/16 v1, 0x21

    if-eq v0, v1, :cond_25

    const/16 v1, 0x19

    if-eq v0, v1, :cond_25

    const/16 v1, 0x18

    if-eq v0, v1, :cond_25

    const/16 v1, 0x17

    if-eq v0, v1, :cond_25

    const/16 v1, 0x23

    if-ne v0, v1, :cond_27

    .line 937
    :cond_25
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/c;

    .line 938
    if-eqz v0, :cond_26

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 939
    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->d()Z

    goto/16 :goto_2

    .line 941
    :cond_26
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 942
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 944
    :cond_27
    const/16 v1, 0xbbb

    if-eq v0, v1, :cond_28

    const/16 v1, 0x2710

    if-ne v0, v1, :cond_29

    .line 951
    :cond_28
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 963
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    .line 978
    :cond_29
    invoke-direct {p0, p1, v8}, Lcom/sec/chaton/d/r;->a(Landroid/os/Message;Lcom/sec/chaton/d/bd;)V

    goto/16 :goto_2

    .line 986
    :sswitch_5
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto/16 :goto_2

    .line 994
    :cond_2a
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8}, Lcom/sec/chaton/d/bd;->a()J

    move-result-wide v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 995
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 999
    :cond_2b
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_1

    .line 1109
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v5, -0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    .line 1110
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1002
    :sswitch_6
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 1003
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_4

    goto/16 :goto_2

    .line 1026
    :sswitch_7
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->e(Lcom/sec/chaton/d/o;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1027
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 1028
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Z)Z

    .line 1031
    :cond_2c
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1036
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2d
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1037
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bd;

    .line 1038
    if-eqz v0, :cond_2d

    .line 1039
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_8

    .line 1043
    :cond_2e
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_2

    .line 1054
    :sswitch_8
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/b;

    .line 1055
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/b;->a()Z

    move-result v0

    if-nez v0, :cond_32

    .line 1056
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->e(Lcom/sec/chaton/d/o;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 1057
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 1058
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Z)Z

    .line 1061
    :cond_2f
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_32

    .line 1066
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_30
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1067
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bd;

    .line 1068
    if-eqz v0, :cond_30

    .line 1069
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_9

    .line 1073
    :cond_31
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1077
    :cond_32
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v5, -0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto/16 :goto_2

    .line 1082
    :sswitch_9
    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/d;

    .line 1083
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_36

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->e(Lcom/sec/chaton/d/o;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1085
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 1086
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;Z)Z

    .line 1089
    :cond_33
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_36

    .line 1091
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_34
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1092
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bd;

    .line 1093
    if-eqz v0, :cond_34

    .line 1094
    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_a

    .line 1097
    :cond_35
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/d/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1100
    :cond_36
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v5, -0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto/16 :goto_2

    .line 1105
    :sswitch_a
    iget-object v0, p0, Lcom/sec/chaton/d/r;->a:Lcom/sec/chaton/d/o;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v5, -0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/d/o;IIILjava/lang/Object;J)V

    goto/16 :goto_2

    .line 1113
    :cond_37
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 544
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0xc -> :sswitch_2
        0x1f -> :sswitch_2
        0x26 -> :sswitch_2
        0x66 -> :sswitch_3
        0x6a -> :sswitch_1
        0x6b -> :sswitch_5
    .end sparse-switch

    .line 999
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_6
        0x6 -> :sswitch_8
        0x24 -> :sswitch_9
        0x67 -> :sswitch_7
        0x68 -> :sswitch_a
        0x6b -> :sswitch_a
    .end sparse-switch
.end method
