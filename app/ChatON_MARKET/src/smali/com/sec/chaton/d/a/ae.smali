.class public Lcom/sec/chaton/d/a/ae;
.super Lcom/sec/chaton/d/a/a;
.source "CompatibilityTask.java"


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_1

    .line 150
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "compatibility"

    sget-object v2, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    :goto_0
    const-string v0, "CHATON_COMPATIBILITY"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "compatibility"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    return-void

    .line 152
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpEntry.getHttpResultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "compatibility"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 157
    const-string v0, "01000099"

    const-string v1, "0201"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 19

    .prologue
    .line 33
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 34
    new-instance v3, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v3}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 36
    const-string v4, "mixed message"

    iput-object v4, v3, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 37
    const-string v4, "true"

    iput-object v4, v3, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 40
    new-instance v4, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v4}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 41
    const-string v5, "mixed image"

    iput-object v5, v4, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 42
    const-string v5, "true"

    iput-object v5, v4, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 44
    new-instance v5, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v5}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 45
    const-string v6, "mixed video"

    iput-object v6, v5, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 46
    const-string v6, "true"

    iput-object v6, v5, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 48
    new-instance v6, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v6}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 49
    const-string v7, "mixed geo"

    iput-object v7, v6, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 50
    const-string v7, "true"

    iput-object v7, v6, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 52
    new-instance v7, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v7}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 53
    const-string v8, "mixed anicon"

    iput-object v8, v7, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 54
    const-string v8, "true"

    iput-object v8, v7, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 56
    new-instance v8, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v8}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 57
    const-string v9, "mixed ams"

    iput-object v9, v8, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 58
    const-string v9, "true"

    iput-object v9, v8, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 61
    new-instance v9, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v9}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 63
    const-string v10, "doc"

    iput-object v10, v9, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 64
    const-string v10, "true"

    iput-object v10, v9, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 72
    new-instance v10, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v10}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 74
    const-string v11, "file"

    iput-object v11, v10, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 75
    const-string v11, "true"

    iput-object v11, v10, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 79
    new-instance v11, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v11}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 80
    const-string v12, "ams"

    iput-object v12, v11, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 81
    const-string v12, "true"

    iput-object v12, v11, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 85
    new-instance v12, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v12}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 86
    const-string v13, "anicon"

    iput-object v13, v12, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 87
    const-string v13, "true"

    iput-object v13, v12, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 91
    new-instance v13, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v13}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 92
    const-string v14, "multidevice"

    iput-object v14, v13, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 93
    const-string v14, "true"

    iput-object v14, v13, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 97
    new-instance v14, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v14}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 98
    const-string v15, "mirror msg push alert"

    iput-object v15, v14, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 99
    const-string v15, "true"

    iput-object v15, v14, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 103
    new-instance v15, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct {v15}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 104
    const-string v16, "push encryption"

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 105
    const-string v16, "true"

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 109
    new-instance v16, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct/range {v16 .. v16}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 110
    const-string v17, "br2"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 111
    const-string v17, "true"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 115
    new-instance v17, Lcom/sec/chaton/io/entry/Compatibility;

    invoke-direct/range {v17 .. v17}, Lcom/sec/chaton/io/entry/Compatibility;-><init>()V

    .line 116
    const-string v18, "forward message"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/chaton/io/entry/Compatibility;->type:Ljava/lang/String;

    .line 117
    const-string v18, "true"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/chaton/io/entry/Compatibility;->value:Ljava/lang/String;

    .line 120
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v3, "push_message_encrypt_feature"

    invoke-static {v3}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    const-string v3, "broadcast2_feature"

    invoke-static {v3}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 137
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_1
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v3, Lcom/sec/chaton/util/af;

    invoke-direct {v3}, Lcom/sec/chaton/util/af;-><init>()V

    .line 142
    invoke-virtual {v3, v2}, Lcom/sec/chaton/util/af;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-object v2
.end method
