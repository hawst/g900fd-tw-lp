.class public Lcom/sec/chaton/d/a/ab;
.super Lcom/sec/chaton/d/a/s;
.source "ChatTask.java"


# instance fields
.field private h:Lcom/sec/chaton/e/w;

.field private i:Lcom/sec/chaton/e/r;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:[Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Landroid/content/ContentResolver;

.field private q:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p10}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;ILcom/sec/chaton/msgsend/k;)V

    .line 103
    iput-wide p2, p0, Lcom/sec/chaton/d/a/ab;->f:J

    .line 104
    iput-object p4, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    .line 105
    iput-object p5, p0, Lcom/sec/chaton/d/a/ab;->h:Lcom/sec/chaton/e/w;

    .line 106
    iput-object p7, p0, Lcom/sec/chaton/d/a/ab;->j:Ljava/lang/String;

    .line 107
    iput-object p6, p0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    .line 108
    iput-object p8, p0, Lcom/sec/chaton/d/a/ab;->n:Ljava/lang/String;

    .line 109
    iput-object p9, p0, Lcom/sec/chaton/d/a/ab;->l:[Ljava/lang/String;

    .line 111
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    .line 112
    if-eqz p9, :cond_0

    array-length v0, p9

    if-lez v0, :cond_0

    .line 113
    aget-object v0, p9, v1

    iput-object v0, p0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    .line 114
    array-length v0, p9

    iput v0, p0, Lcom/sec/chaton/d/a/ab;->q:I

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    .line 117
    iput v1, p0, Lcom/sec/chaton/d/a/ab;->q:I

    goto :goto_0
.end method

.method private a([Ljava/lang/String;J)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 382
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 383
    const-string v1, ""

    .line 384
    array-length v5, p1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, p1, v2

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v0, v6, v7}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 386
    const-string v0, ""

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v0, v7, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 391
    :goto_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 392
    if-nez v0, :cond_1

    .line 393
    const-string v1, "1001 error - invalidUserName: NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_0
    :goto_2
    const-string v1, "%d,%s,%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v8, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v8}, Lcom/sec/chaton/e/aj;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x1

    aput-object v6, v7, v8

    const/4 v8, 0x2

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ";"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v1, v7, v6}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 395
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "1001 error - invalidUserName:"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 402
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 414
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 17

    .prologue
    .line 149
    invoke-super/range {p0 .. p3}, Lcom/sec/chaton/d/a/s;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 151
    const-string v1, "ChatTask"

    const-string v2, "onPostExecute()"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    new-instance v14, Landroid/os/Message;

    invoke-direct {v14}, Landroid/os/Message;-><init>()V

    .line 153
    const/4 v1, 0x4

    iput v1, v14, Landroid/os/Message;->what:I

    .line 155
    if-eqz p3, :cond_14

    .line 156
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 159
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 308
    :cond_0
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->b:Landroid/os/Handler;

    invoke-virtual {v1, v14}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 371
    :goto_1
    return-void

    .line 162
    :pswitch_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/a/f;

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    const-string v3, "ChatTask"

    invoke-static {v2, v1, v3}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/f;Ljava/lang/String;)V

    .line 174
    invoke-virtual {v1}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/ej;->d()I

    move-result v3

    .line 175
    const/16 v2, 0x3e8

    if-eq v3, v2, :cond_1

    const/16 v2, 0xbbb

    if-ne v3, v2, :cond_2

    .line 176
    :cond_1
    new-instance v1, Lcom/sec/chaton/a/a/k;

    const/4 v2, 0x1

    const/16 v3, 0x384

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-direct/range {v1 .. v6}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    iput-object v1, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 180
    :cond_2
    const/16 v2, 0x1b59

    if-ne v3, v2, :cond_3

    .line 181
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-virtual {v1, v2, v4, v5, v6}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 200
    :goto_2
    new-instance v1, Lcom/sec/chaton/a/a/k;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-direct/range {v1 .. v6}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    iput-object v1, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 182
    :cond_3
    const/16 v2, 0x1b5a

    if-ne v3, v2, :cond_4

    .line 183
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v2, v4, v5, v1}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_2

    .line 184
    :cond_4
    const/16 v2, 0x1b5b

    if-ne v3, v2, :cond_5

    .line 185
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_2

    .line 189
    :cond_5
    const/16 v1, 0x7d6

    if-eq v3, v1, :cond_6

    const/16 v1, 0xbc0

    if-ne v3, v1, :cond_7

    .line 192
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-static {v1, v2, v4, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_2

    .line 197
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_2

    .line 206
    :pswitch_2
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/sec/chaton/a/aw;

    .line 207
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    const-string v2, "ChatTask"

    invoke-static {v1, v13, v2}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/aw;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->d()I

    move-result v15

    .line 224
    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v16

    .line 226
    const/16 v1, 0x3e8

    if-eq v15, v1, :cond_8

    const/16 v1, 0x3e9

    if-ne v15, v1, :cond_f

    .line 233
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/chaton/d/a/ab;->q:I

    invoke-static/range {v1 .. v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;JLjava/lang/Long;I)I

    .line 235
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->n:Ljava/lang/String;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v5

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/a/ey;->f()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/chaton/d/a/ab;->q:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/chaton/d/a/ab;->h:Lcom/sec/chaton/e/w;

    invoke-static/range {v1 .. v12}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;ILcom/sec/chaton/e/w;)I

    .line 239
    const/16 v1, 0x3e9

    if-ne v15, v1, :cond_c

    .line 241
    const-string v1, "["

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const-string v2, "]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 243
    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/d/a/ab;->a([Ljava/lang/String;J)V

    .line 245
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_c

    .line 246
    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->k()Ljava/util/List;

    move-result-object v2

    .line 248
    if-eqz v2, :cond_c

    .line 249
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_9

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1001 error blockedReciversList : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ChatTask"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->l:[Ljava/lang/String;

    if-eqz v1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->l:[Ljava/lang/String;

    array-length v1, v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v1, v3, :cond_e

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->l:[Ljava/lang/String;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v4, :cond_a

    aget-object v5, v3, v1

    .line 256
    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 257
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    .line 266
    :cond_a
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->g:Lcom/sec/chaton/d/o;

    if-nez v1, :cond_b

    .line 267
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    invoke-static {v1, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/d/a/ab;->g:Lcom/sec/chaton/d/o;

    .line 269
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->g:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ab;->j:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    aput-object v6, v5, v1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v3, v4, v5, v1, v2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Z)J

    .line 274
    :cond_c
    new-instance v1, Lcom/sec/chaton/a/a/k;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-direct {v1, v2, v15, v3, v4}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v1, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 255
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 263
    :cond_e
    const-string v1, ""

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    goto :goto_4

    .line 276
    :cond_f
    new-instance v1, Lcom/sec/chaton/a/a/k;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-direct {v1, v2, v15, v3, v4}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v1, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 278
    const/16 v1, 0x1b59

    if-ne v15, v1, :cond_10

    .line 279
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 280
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 281
    :cond_10
    const/16 v1, 0x1b5a

    if-ne v15, v1, :cond_11

    .line 282
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto/16 :goto_0

    .line 283
    :cond_11
    const/16 v1, 0x1b5b

    if-ne v15, v1, :cond_12

    .line 284
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto/16 :goto_0

    .line 286
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 288
    const/16 v1, 0x1772

    if-ne v15, v1, :cond_0

    .line 289
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_13

    .line 290
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v4

    invoke-static {v2, v3, v1, v4, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;J)I

    goto/16 :goto_0

    .line 293
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {v13}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/d/a/ab;->a([Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 313
    :cond_14
    const/4 v1, 0x5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/chaton/d/a/ab;->f:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ab;->j:Ljava/lang/String;

    const-string v7, "ChatTask"

    move/from16 v3, p1

    invoke-static/range {v1 .. v7}, Lcom/sec/chaton/chat/en;->a(ILjava/lang/String;IJLjava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/d/a/ab;->a()Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v1, 0xb

    move/from16 v0, p1

    if-eq v0, v1, :cond_15

    const/16 v1, 0x16

    move/from16 v0, p1

    if-eq v0, v1, :cond_15

    const/4 v1, 0x3

    move/from16 v0, p1

    if-ne v0, v1, :cond_16

    .line 345
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->m:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v1

    if-nez v1, :cond_17

    invoke-static/range {p1 .. p1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 346
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 354
    :goto_5
    invoke-static/range {p1 .. p1}, Lcom/sec/chaton/j/q;->b(I)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 357
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 367
    :cond_16
    new-instance v1, Lcom/sec/chaton/a/a/k;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/chaton/d/a/ab;->f:J

    move/from16 v0, p1

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v1, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 369
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->b:Landroid/os/Handler;

    invoke-virtual {v1, v14}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 349
    :cond_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_5

    .line 159
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 125
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->b()Lcom/sec/chaton/j/ao;

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->p:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ab;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/j/ak;)V

    .line 131
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v4

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v3, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    .line 139
    :goto_0
    const-string v0, "ChatTask"

    const-string v1, "ConPreExecute(), REQUEST - ALLOW CHAT"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/ab;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/aq;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 144
    :goto_1
    return-object v0

    .line 136
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    goto :goto_0

    .line 143
    :cond_1
    const-string v0, "ChatTask"

    const-string v1, "onPreExecute(), REQUEST - CHAT REQUEST (msgId(%d), chatMsg(%s))"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/sec/chaton/d/a/ab;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/chaton/d/a/ab;->n:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/d/a/ab;->f:J

    iget-object v3, p0, Lcom/sec/chaton/d/a/ab;->o:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/ab;->h:Lcom/sec/chaton/e/w;

    iget-object v5, p0, Lcom/sec/chaton/d/a/ab;->i:Lcom/sec/chaton/e/r;

    iget-object v6, p0, Lcom/sec/chaton/d/a/ab;->j:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ab;->n:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/d/a/ab;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/aq;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    goto :goto_1
.end method
