.class public Lcom/sec/chaton/d/a/ds;
.super Lcom/sec/chaton/d/a/s;
.source "SerialChatTask.java"


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field private i:Lcom/sec/chaton/e/r;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:[Ljava/lang/String;

.field private n:Lcom/sec/chaton/msgsend/a;

.field private o:Lcom/sec/chaton/msgsend/a;

.field private p:Ljava/lang/String;

.field private q:Landroid/content/ContentResolver;

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/chaton/d/a/ds;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;JLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/k;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p9}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;ILcom/sec/chaton/msgsend/k;)V

    .line 95
    iput-wide p2, p0, Lcom/sec/chaton/d/a/ds;->f:J

    .line 96
    iput-object p4, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    .line 98
    iput-object p6, p0, Lcom/sec/chaton/d/a/ds;->j:Ljava/lang/String;

    .line 99
    iput-object p5, p0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    .line 101
    iput-object p8, p0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    .line 102
    iput-object p8, p0, Lcom/sec/chaton/d/a/ds;->o:Lcom/sec/chaton/msgsend/a;

    .line 103
    iput-object p7, p0, Lcom/sec/chaton/d/a/ds;->l:[Ljava/lang/String;

    .line 105
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    .line 106
    if-eqz p7, :cond_0

    array-length v0, p7

    if-lez v0, :cond_0

    .line 107
    aget-object v0, p7, v1

    iput-object v0, p0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    .line 108
    array-length v0, p7

    iput v0, p0, Lcom/sec/chaton/d/a/ds;->r:I

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    .line 112
    iput v1, p0, Lcom/sec/chaton/d/a/ds;->r:I

    goto :goto_0
.end method

.method private a([Ljava/lang/String;J)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 468
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 469
    const-string v1, ""

    .line 470
    array-length v5, p1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, p1, v2

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v0, v6, v7}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 473
    const-string v0, ""

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v0, v7, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 475
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 478
    :goto_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 479
    if-nez v0, :cond_1

    .line 480
    const-string v1, "1001 error - invalidUserName: NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_0
    :goto_2
    const-string v1, "%d,%s,%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v8, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v8}, Lcom/sec/chaton/e/aj;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x1

    aput-object v6, v7, v8

    const/4 v8, 0x2

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ";"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v1, v7, v6}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 482
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "1001 error - invalidUserName:"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 490
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 509
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public a(J)I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->o:Lcom/sec/chaton/msgsend/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/msgsend/a;->a(J)I

    move-result v0

    return v0
.end method

.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 31

    .prologue
    .line 149
    invoke-super/range {p0 .. p3}, Lcom/sec/chaton/d/a/s;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 151
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onPostExecute()"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    if-eqz p3, :cond_1d

    .line 156
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 159
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 162
    :sswitch_0
    new-instance v9, Landroid/os/Message;

    invoke-direct {v9}, Landroid/os/Message;-><init>()V

    .line 163
    const/4 v3, 0x4

    iput v3, v9, Landroid/os/Message;->what:I

    .line 164
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/a/f;

    .line 165
    sget-boolean v4, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v4, :cond_1

    .line 166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    const-string v5, "[AllowChatReply]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "InboxNO : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "MsgID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->d()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "UID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "uid"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SessionID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->j:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ResultCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/ej;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ResultMsg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_1
    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ej;->d()I

    move-result v5

    .line 181
    const/16 v4, 0x3e8

    if-eq v5, v4, :cond_2

    const/16 v4, 0xbbb

    if-ne v5, v4, :cond_3

    .line 182
    :cond_2
    new-instance v3, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x1

    const/16 v5, 0x384

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/chaton/d/a/ds;->f:J

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    iput-object v3, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 186
    :cond_3
    const/16 v4, 0x1b59

    if-ne v5, v4, :cond_4

    .line 187
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    const-wide/16 v7, 0x0

    invoke-virtual {v3, v4, v6, v7, v8}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 211
    :goto_1
    new-instance v3, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/chaton/d/a/ds;->f:J

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;J)V

    iput-object v3, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 188
    :cond_4
    const/16 v4, 0x1b5a

    if-ne v5, v4, :cond_5

    .line 189
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v4, v6, v7, v3}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_1

    .line 190
    :cond_5
    const/16 v4, 0x1b5b

    if-ne v5, v4, :cond_6

    .line 191
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_1

    .line 195
    :cond_6
    const/16 v3, 0x7d6

    if-eq v5, v3, :cond_7

    const/16 v3, 0xbc0

    if-ne v5, v3, :cond_8

    .line 198
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/util/bk;->b()I

    move-result v7

    invoke-static {v3, v4, v6, v7}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_1

    .line 208
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    sget-object v6, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-static {v3, v4, v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/aa;)I

    goto :goto_1

    .line 217
    :sswitch_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/a/em;

    .line 218
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->d()J

    move-result-wide v22

    .line 219
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->f()Ljava/lang/String;

    move-result-object v5

    .line 220
    const/4 v13, 0x0

    .line 221
    const/4 v14, 0x0

    .line 222
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->i()Lcom/sec/chaton/a/ey;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 223
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->i()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v13

    .line 224
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->i()Lcom/sec/chaton/a/ey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/ey;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 226
    :cond_9
    invoke-virtual {v3}, Lcom/sec/chaton/a/em;->g()Ljava/util/List;

    move-result-object v24

    .line 228
    const/4 v4, 0x0

    .line 229
    const/4 v3, -0x1

    .line 230
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    invoke-virtual {v6}, Lcom/sec/chaton/msgsend/a;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v25

    .line 231
    if-eqz v25, :cond_a

    move-object/from16 v0, v25

    array-length v6, v0

    if-gtz v6, :cond_b

    .line 232
    :cond_a
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_0

    .line 233
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception. localChatMsgList is null or empty. > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_b
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    move/from16 v20, v4

    move v4, v3

    :goto_2
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v18, v3

    check-cast v18, Lcom/sec/chaton/a/ev;

    .line 239
    new-instance v27, Landroid/os/Message;

    invoke-direct/range {v27 .. v27}, Landroid/os/Message;-><init>()V

    .line 240
    const/4 v3, 0x4

    move-object/from16 v0, v27

    iput v3, v0, Landroid/os/Message;->what:I

    .line 241
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->d()J

    move-result-wide v6

    .line 242
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v10

    .line 243
    aget-object v3, v25, v20

    check-cast v3, Lcom/sec/chaton/msgsend/b;

    .line 244
    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/b;->a()Ljava/lang/String;

    move-result-object v12

    .line 245
    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v28

    .line 246
    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/b;->b()Lcom/sec/chaton/e/w;

    move-result-object v17

    .line 248
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_c

    .line 249
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    const-string v3, "[SerialChatReply]"

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v15, v20, 0x1

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v15, "/"

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v15, "]"

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Time : "

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "ResultMessage : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "ResultCode : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/a/ej;->d()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "MsgID : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "RequestID : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "UID : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v15, "uid"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "InboxNO : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "SessionID : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "Server_address : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v13, :cond_13

    move-object v3, v13

    :goto_3
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "Server_port : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v14, :cond_14

    move-object v3, v14

    :goto_4
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "ReceivedTime : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v8, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_c
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->d()I

    move-result v3

    .line 274
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v30

    .line 277
    const/16 v8, 0x13ec

    if-ne v3, v8, :cond_15

    .line 278
    if-ltz v4, :cond_23

    move/from16 v21, v4

    move/from16 v19, v4

    .line 285
    :goto_5
    const/16 v3, 0x3e8

    move/from16 v0, v21

    if-eq v0, v3, :cond_d

    const/16 v3, 0x3e9

    move/from16 v0, v21

    if-ne v0, v3, :cond_18

    .line 292
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/chaton/d/a/ds;->r:I

    invoke-static/range {v3 .. v9}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;JLjava/lang/Long;I)I

    .line 301
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v4, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/chaton/d/a/ds;->r:I

    move/from16 v16, v0

    move-object v6, v3

    move-object v7, v4

    move-object v9, v12

    move-object v12, v5

    invoke-static/range {v6 .. v17}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;ILcom/sec/chaton/e/w;)I

    .line 316
    const/16 v3, 0x3e9

    move/from16 v0, v21

    if-ne v0, v3, :cond_11

    .line 319
    const-string v3, "["

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string v4, "]"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 320
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 321
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/sec/chaton/d/a/ds;->a([Ljava/lang/String;J)V

    .line 323
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v3, v4, :cond_11

    .line 324
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->k()Ljava/util/List;

    move-result-object v4

    .line 326
    if-eqz v4, :cond_11

    .line 327
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_e

    .line 328
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1001 error blockedReciversList : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->l:[Ljava/lang/String;

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->l:[Ljava/lang/String;

    array-length v3, v3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-le v3, v6, :cond_17

    .line 333
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->l:[Ljava/lang/String;

    array-length v7, v6

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v7, :cond_f

    aget-object v8, v6, v3

    .line 334
    invoke-interface {v4, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_16

    .line 335
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    .line 344
    :cond_f
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->g:Lcom/sec/chaton/d/o;

    if-nez v3, :cond_10

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    invoke-static {v3, v6}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/d/a/ds;->g:Lcom/sec/chaton/d/o;

    .line 347
    :cond_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->g:Lcom/sec/chaton/d/o;

    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    aput-object v8, v7, v3

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6, v5, v7, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Z)J

    .line 353
    :cond_11
    new-instance v3, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x1

    move/from16 v0, v21

    move-wide/from16 v1, v28

    invoke-direct {v3, v4, v0, v1, v2}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    move-object/from16 v0, v27

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 384
    :cond_12
    :goto_8
    add-int/lit8 v3, v20, 0x1

    .line 385
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->b:Landroid/os/Handler;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move/from16 v4, v19

    move/from16 v20, v3

    .line 386
    goto/16 :goto_2

    .line 253
    :cond_13
    const-string v3, "null"

    goto/16 :goto_3

    :cond_14
    const-string v3, "null"

    goto/16 :goto_4

    :cond_15
    move/from16 v21, v3

    move/from16 v19, v3

    .line 282
    goto/16 :goto_5

    .line 333
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 341
    :cond_17
    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    goto :goto_7

    .line 356
    :cond_18
    new-instance v3, Lcom/sec/chaton/a/a/k;

    const/4 v4, 0x0

    move/from16 v0, v21

    move-wide/from16 v1, v28

    invoke-direct {v3, v4, v0, v1, v2}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    move-object/from16 v0, v27

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 358
    const/16 v3, 0x1b59

    move/from16 v0, v21

    if-ne v0, v3, :cond_19

    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 360
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v7

    invoke-virtual {v3, v4, v6, v7, v8}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_8

    .line 361
    :cond_19
    const/16 v3, 0x1b5a

    move/from16 v0, v21

    if-ne v0, v3, :cond_1a

    .line 362
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v7

    invoke-virtual {v3, v4, v6, v7}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_8

    .line 363
    :cond_1a
    const/16 v3, 0x1b5b

    move/from16 v0, v21

    if-ne v0, v3, :cond_1b

    .line 364
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto/16 :goto_8

    .line 366
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 368
    const/16 v3, 0x1772

    move/from16 v0, v21

    if-ne v0, v3, :cond_12

    .line 369
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v3, v4, :cond_1c

    .line 370
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    invoke-static {v3, v4, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 371
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v7

    invoke-static {v4, v6, v3, v7, v8}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;J)I

    goto/16 :goto_8

    .line 374
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 378
    invoke-virtual/range {v18 .. v18}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/sec/chaton/d/a/ds;->a([Ljava/lang/String;J)V

    goto/16 :goto_8

    .line 396
    :cond_1d
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_1e

    .line 397
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    const-string v4, "[SerialChatReply]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Time : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ResultMessage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ResultCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RequestID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/chaton/d/a/ds;->f:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "UID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "uid"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "InboxNO : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SessionID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/ds;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Server_address : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Server_port : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ReceivedTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/d/a/ds;->h:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/d/a/ds;->a()Z

    move-result v3

    if-eqz v3, :cond_1f

    const/16 v3, 0xb

    move/from16 v0, p1

    if-eq v0, v3, :cond_1f

    const/16 v3, 0x16

    move/from16 v0, p1

    if-eq v0, v3, :cond_1f

    const/4 v3, 0x3

    move/from16 v0, p1

    if-ne v0, v3, :cond_21

    .line 423
    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->m:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v3

    if-nez v3, :cond_22

    invoke-static/range {p1 .. p1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    sget-object v5, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/aa;)I

    .line 432
    :goto_9
    const/16 v3, 0x18

    move/from16 v0, p1

    if-eq v0, v3, :cond_20

    const/16 v3, 0x17

    move/from16 v0, p1

    if-eq v0, v3, :cond_20

    const/16 v3, 0x15

    move/from16 v0, p1

    if-ne v0, v3, :cond_21

    .line 437
    :cond_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 447
    :cond_21
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/a;->b()Ljava/util/List;

    move-result-object v3

    .line 448
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/msgsend/b;

    .line 450
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 451
    const/4 v6, 0x4

    iput v6, v5, Landroid/os/Message;->what:I

    .line 452
    new-instance v6, Lcom/sec/chaton/a/a/k;

    const/4 v7, 0x0

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v8

    move/from16 v0, p1

    invoke-direct {v6, v7, v0, v8, v9}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v6, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 454
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->b:Landroid/os/Handler;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_a

    .line 427
    :cond_22
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    sget-object v5, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/msgsend/a;Lcom/sec/chaton/msgsend/aa;)I

    goto :goto_9

    :cond_23
    move/from16 v21, v3

    move/from16 v19, v4

    goto/16 :goto_5

    .line 159
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x1c -> :sswitch_1
    .end sparse-switch
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 120
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->b()Lcom/sec/chaton/j/ao;

    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute()"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->q:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ds;->c:Lcom/sec/chaton/j/ak;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/j/ak;)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v3, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    .line 134
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute(), REQUEST - ALLOW CHAT"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v4

    .line 136
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/ds;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/aq;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    .line 144
    :goto_1
    return-object v0

    .line 131
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    goto :goto_0

    .line 139
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute(), REQUEST - SERIAL CHAT REQUEST (taskId(%d))"

    new-array v2, v8, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/sec/chaton/d/a/ds;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/a;->b()Ljava/util/List;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/b;

    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "  -- msgId(%d), msgType(%s), chatMsg(%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->b()Lcom/sec/chaton/e/w;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 144
    :cond_2
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/d/a/ds;->f:J

    iget-object v3, p0, Lcom/sec/chaton/d/a/ds;->p:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/ds;->i:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/d/a/ds;->j:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/ds;->k:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/ds;->n:Lcom/sec/chaton/msgsend/a;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/aq;->a(JLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/a;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    goto :goto_1
.end method
