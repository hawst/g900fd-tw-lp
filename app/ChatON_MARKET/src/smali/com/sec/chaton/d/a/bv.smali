.class Lcom/sec/chaton/d/a/bv;
.super Ljava/lang/Object;
.source "GetChatProfileImageTitleTask.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/sec/chaton/d/a/bu;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/a/bu;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/chaton/d/a/bv;->b:Lcom/sec/chaton/d/a/bu;

    iput-object p2, p0, Lcom/sec/chaton/d/a/bv;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 109
    const-string v0, "_chat_profile.png_."

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 110
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 112
    iget-object v1, p0, Lcom/sec/chaton/d/a/bv;->b:Lcom/sec/chaton/d/a/bu;

    invoke-static {v1}, Lcom/sec/chaton/d/a/bu;->a(Lcom/sec/chaton/d/a/bu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bv;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
