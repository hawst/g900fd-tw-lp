.class public Lcom/sec/chaton/d/a/n;
.super Lcom/sec/chaton/d/a/df;
.source "AniconChatTask.java"


# instance fields
.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 51
    sget-object v11, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/sec/chaton/d/a/n;-><init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 17

    .prologue
    .line 73
    const-string v7, "jpg"

    sget-object v8, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move/from16 v13, p8

    move-object/from16 v15, p10

    move-object/from16 v16, p11

    invoke-direct/range {v2 .. v16}, Lcom/sec/chaton/d/a/df;-><init>(Landroid/os/Handler;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 76
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/chaton/d/a/n;->o:Ljava/lang/String;

    .line 77
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/d/a/n;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/d/a/n;->j:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 151
    .line 155
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 158
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 160
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    const-string v0, "cd_proxy_url"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v2, "delegate_url"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 168
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "mixed"

    aput-object v3, v1, v6

    const-string v3, "\n"

    aput-object v3, v1, v7

    const-string v3, "anicon"

    aput-object v3, v1, v8

    const-string v3, "\n"

    aput-object v3, v1, v9

    const/4 v3, 0x4

    invoke-static {v0, p0}, Lcom/sec/chaton/settings/downloads/u;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const/4 v0, 0x5

    const-string v3, "\n"

    aput-object v3, v1, v0

    const/4 v0, 0x6

    aput-object v2, v1, v0

    const/4 v0, 0x7

    const-string v2, "\n"

    aput-object v2, v1, v0

    const/16 v0, 0x8

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x9

    const-string v2, "\n"

    aput-object v2, v1, v0

    const/16 v0, 0xa

    const-string v2, "\n"

    aput-object v2, v1, v0

    const/16 v0, 0xb

    aput-object p1, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_1
    return-object v0

    :cond_0
    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "anicon"

    aput-object v3, v1, v6

    const-string v3, "\n"

    aput-object v3, v1, v7

    const-string v3, "jpg"

    aput-object v3, v1, v8

    const-string v3, "\n"

    aput-object v3, v1, v9

    const/4 v3, 0x4

    invoke-static {v0, p0}, Lcom/sec/chaton/settings/downloads/u;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const/4 v0, 0x5

    const-string v3, "\n"

    aput-object v3, v1, v0

    const/4 v0, 0x6

    aput-object v2, v1, v0

    const/4 v0, 0x7

    const-string v2, "\n"

    aput-object v2, v1, v0

    const/16 v0, 0x8

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x9

    const-string v2, "\n"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto/16 :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/d/a/n;->o:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/d/a/n;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 200
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 201
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/d/a/n;->o:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/df;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 205
    return-void
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 134
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/n;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 137
    const-string v1, "anicon"

    iput-object v1, p0, Lcom/sec/chaton/d/a/n;->h:Ljava/lang/String;

    .line 138
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/d/a/n;->i:Ljava/lang/String;

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 143
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 144
    const-string v4, "sent_time"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 146
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 148
    invoke-super {p0}, Lcom/sec/chaton/d/a/df;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
