.class public Lcom/sec/chaton/d/a/co;
.super Lcom/sec/chaton/d/a/a;
.source "GetUserInfoTask.java"


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 25
    const-string v0, "GetUserInfoTask"

    iput-object v0, p0, Lcom/sec/chaton/d/a/co;->b:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 7

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v1

    .line 41
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    .line 42
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v3

    .line 45
    sget-object v0, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v0, :cond_3

    .line 46
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;

    .line 51
    :goto_0
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_0

    .line 52
    const-string v4, "afterRequest(), connectionSuccess(%s), httpResultCode(%s), httpStatus(%d), resultEntry(%s)"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v2, v5, v3

    const/4 v3, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x3

    aput-object v0, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 57
    iget-object v4, p0, Lcom/sec/chaton/d/a/co;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    const/16 v3, 0xc8

    if-ne v1, v3, :cond_4

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v1, :cond_4

    .line 61
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->getUrlPrimary()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->getUrlSecondary()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 64
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "primary_translation_address"

    invoke-virtual {v3, v4, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 67
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "secondary_translation_address"

    invoke-virtual {v1, v3, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_2
    new-instance v1, Lcom/sec/chaton/d/a/cr;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/d/a/cr;-><init>(Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationAuthEntry;)V

    invoke-virtual {p1, v1}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Object;)V

    .line 74
    :goto_1
    return-void

    .line 48
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;

    goto :goto_0

    .line 73
    :cond_4
    new-instance v1, Lcom/sec/chaton/d/a/cr;

    sget-object v2, Lcom/sec/chaton/chat/b/m;->e:Lcom/sec/chaton/chat/b/m;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/d/a/cr;-><init>(Lcom/sec/chaton/chat/b/m;Lcom/sec/chaton/io/entry/TranslationAuthEntry;)V

    invoke-virtual {p1, v1}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method
