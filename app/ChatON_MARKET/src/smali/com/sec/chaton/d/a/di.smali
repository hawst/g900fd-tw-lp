.class public Lcom/sec/chaton/d/a/di;
.super Ljava/lang/Object;
.source "NetTaskHandler.java"


# static fields
.field private static a:Lcom/sec/chaton/d/a/di;


# instance fields
.field private b:Landroid/os/HandlerThread;

.field private c:Lcom/sec/chaton/d/a/dj;

.field private d:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/d/a/di;->a:Lcom/sec/chaton/d/a/di;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AsyncNetTask"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/di;->b:Landroid/os/HandlerThread;

    .line 23
    iget-object v0, p0, Lcom/sec/chaton/d/a/di;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 24
    iget-object v0, p0, Lcom/sec/chaton/d/a/di;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/di;->d:Landroid/os/Looper;

    .line 25
    new-instance v0, Lcom/sec/chaton/d/a/dj;

    iget-object v1, p0, Lcom/sec/chaton/d/a/di;->d:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/a/dj;-><init>(Lcom/sec/chaton/d/a/di;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/d/a/di;->c:Lcom/sec/chaton/d/a/dj;

    .line 26
    return-void
.end method

.method public static a()Lcom/sec/chaton/d/a/di;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/d/a/di;->a:Lcom/sec/chaton/d/a/di;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/chaton/d/a/di;

    invoke-direct {v0}, Lcom/sec/chaton/d/a/di;-><init>()V

    sput-object v0, Lcom/sec/chaton/d/a/di;->a:Lcom/sec/chaton/d/a/di;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/a/di;->a:Lcom/sec/chaton/d/a/di;

    return-object v0
.end method


# virtual methods
.method public b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/d/a/di;->c:Lcom/sec/chaton/d/a/dj;

    return-object v0
.end method
