.class public Lcom/sec/chaton/d/a/j;
.super Lcom/sec/chaton/d/a/a;
.source "AmsItemDownloadTask.java"


# static fields
.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/d/a/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/a/j;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 52
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 54
    sget-object v1, Lcom/sec/chaton/d/e;->a:Lcom/sec/chaton/d/e;

    .line 55
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->d()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->f()Ljava/util/List;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 57
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "type"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/e;->a(Ljava/lang/String;)Lcom/sec/chaton/d/e;

    move-result-object v0

    .line 64
    :goto_0
    sget-object v1, Lcom/sec/chaton/d/e;->a:Lcom/sec/chaton/d/e;

    if-eq v0, v1, :cond_1

    .line 65
    sget-object v1, Lcom/sec/chaton/d/a/k;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/d/e;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 79
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 80
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;

    .line 82
    if-nez v0, :cond_3

    .line 83
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 84
    const-string v0, "download_ams, Http result object is null."

    sget-object v1, Lcom/sec/chaton/d/a/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_2
    :goto_2
    return-void

    .line 67
    :pswitch_0
    const-string v0, "01000011"

    const-string v1, "2002"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_1

    .line 70
    :pswitch_1
    const-string v0, "01000011"

    const-string v1, "2102"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_1

    .line 73
    :pswitch_2
    const-string v0, "01000011"

    const-string v1, "2202"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_1

    .line 90
    :cond_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 91
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "download_ams, Entry: "

    aput-object v3, v1, v2

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method
