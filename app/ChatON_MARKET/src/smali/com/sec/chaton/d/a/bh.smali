.class public Lcom/sec/chaton/d/a/bh;
.super Lcom/sec/chaton/d/a/a;
.source "GetAllMessagesTask.java"


# instance fields
.field b:Landroid/content/ContentResolver;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/sec/chaton/e/r;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 59
    iput-object p3, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/sec/chaton/d/a/bh;->e:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/sec/chaton/d/a/bh;->f:Lcom/sec/chaton/e/r;

    .line 62
    return-void
.end method

.method private a(Lcom/sec/chaton/e/a/p;Lcom/sec/chaton/io/entry/inner/Message;)Landroid/content/ContentProviderOperation;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 198
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/sec/chaton/e/a/p;->g:J

    .line 199
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/sec/chaton/e/a/p;->j:J

    .line 200
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    iput-object v0, p1, Lcom/sec/chaton/e/a/p;->k:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->a()I

    move-result v0

    iput v0, p1, Lcom/sec/chaton/e/a/p;->c:I

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :goto_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    const-string v2, "0999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v5, :cond_2

    .line 215
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 216
    const-string v2, "push_message"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_1
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->f:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v2, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/sec/chaton/e/a/p;->e:Ljava/lang/String;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/a/p;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0

    .line 207
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 222
    :cond_2
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/sec/chaton/io/entry/inner/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236
    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/bh;->e:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Message;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/sec/chaton/io/entry/inner/Message;

    .line 345
    iget-object v0, v6, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    const-string v0, ""

    .line 347
    iget-object v0, v6, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 351
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 352
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v0, v2, v1

    .line 353
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Row : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v5, ","

    const/4 v8, 0x3

    invoke-static {v0, v5, v8}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 358
    array-length v0, v5

    const/4 v8, 0x2

    if-le v0, v8, :cond_2

    .line 359
    const/4 v0, 0x1

    aget-object v8, v5, v0

    .line 360
    const/4 v0, 0x2

    aget-object v0, v5, v0

    .line 361
    iget-object v9, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    invoke-static {v9, v8, v0}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 367
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 368
    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-static {v10}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v10

    sget-object v11, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v10, v11, :cond_4

    .line 370
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v5, "chaton_id"

    const-string v10, ""

    invoke-virtual {v0, v5, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, v6, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 371
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 382
    :cond_1
    :goto_2
    sget-object v5, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    if-eq v0, v5, :cond_2

    .line 383
    const-string v5, "%d,%s,%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aj;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v11

    const/4 v0, 0x1

    aput-object v8, v10, v0

    const/4 v0, 0x2

    invoke-static {v9}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v0

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 374
    :cond_3
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_2

    .line 378
    :cond_4
    const/4 v10, 0x0

    aget-object v5, v5, v10

    invoke-static {v5}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v5

    sget-object v10, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    if-ne v5, v10, :cond_1

    .line 379
    sget-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    .line 380
    const-string v5, ""

    invoke-static {v5, v8, v9}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 387
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 388
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    iget-object v3, v6, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, v6, Lcom/sec/chaton/io/entry/inner/Message;->id:Ljava/lang/String;

    iget-object v6, v6, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 393
    :cond_6
    return-void
.end method

.method private b(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/sec/chaton/io/entry/inner/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .line 243
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/chaton/e/a/q;->a(Lorg/json/JSONObject;)Lcom/sec/chaton/e/w;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/bh;->e:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Message;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 245
    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/sec/chaton/io/entry/inner/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .line 253
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v3

    .line 254
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 258
    sget-object v1, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    if-eq v3, v1, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    if-eq v3, v1, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-eq v3, v1, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    if-ne v3, v1, :cond_4

    :cond_0
    array-length v1, v2

    const/4 v4, 0x6

    if-lt v1, v4, :cond_4

    .line 261
    const/4 v1, 0x5

    aget-object v1, v2, v1

    move-object v14, v1

    .line 268
    :goto_0
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v3, v1, :cond_1

    .line 270
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    .line 272
    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/u;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    sget-object v3, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 279
    :cond_1
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v3, v1, :cond_2

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v3, v1, :cond_2

    sget-object v1, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne v3, v1, :cond_3

    .line 280
    :cond_2
    array-length v1, v2

    const/4 v4, 0x5

    if-lt v1, v4, :cond_5

    .line 281
    const/4 v1, 0x2

    aget-object v4, v2, v1

    .line 282
    const/4 v1, 0x4

    aget-object v5, v2, v1

    .line 283
    const/4 v1, 0x3

    aget-object v6, v2, v1

    .line 284
    iget-object v7, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    .line 285
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Message;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 286
    iget-object v10, p0, Lcom/sec/chaton/d/a/bh;->f:Lcom/sec/chaton/e/r;

    .line 287
    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    .line 288
    new-instance v12, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v12, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 290
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    .line 291
    new-instance v1, Lcom/sec/chaton/d/a/bj;

    move-object v2, p0

    invoke-direct/range {v1 .. v13}, Lcom/sec/chaton/d/a/bj;-><init>(Lcom/sec/chaton/d/a/bh;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/r;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 338
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/d/a/bh;->e:Ljava/lang/String;

    move-object/from16 v4, p2

    move-object v6, v14

    invoke-static/range {v1 .. v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Message;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    return-void

    .line 263
    :cond_4
    const/4 v1, 0x0

    move-object v14, v1

    goto :goto_0

    .line 326
    :cond_5
    array-length v1, v2

    const/4 v4, 0x4

    if-lt v1, v4, :cond_6

    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No need to update and insert: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 330
    :cond_6
    const-string v1, "Token Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 83
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_d

    .line 84
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetAllMessageList;

    .line 85
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetAllMessageList;->has_more:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "has_more : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v4, v0, Lcom/sec/chaton/io/entry/GetAllMessageList;->messages:Ljava/util/ArrayList;

    .line 89
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 90
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "allMessage.messages.size() is 0"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    new-instance v0, Lcom/sec/chaton/d/a/bi;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/a/bi;-><init>(Lcom/sec/chaton/d/a/bh;)V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 108
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    .line 109
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 110
    const/4 v1, 0x0

    .line 111
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v7

    .line 114
    if-eqz v7, :cond_0

    .line 119
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Message;

    .line 120
    iget-object v2, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-static {v2, v9}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    .line 121
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_4

    .line 122
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->msg:Ljava/lang/String;

    .line 123
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x5

    if-le v9, v10, :cond_3

    .line 124
    const/4 v9, 0x4

    invoke-virtual {v2, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 126
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Msg : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ",Tid : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->tid:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", Type : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", MsgId : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->id:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", Sender : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", Time : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->time:Ljava/lang/Long;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    iget-object v9, v0, Lcom/sec/chaton/io/entry/inner/Message;->sender:Ljava/lang/String;

    iget-object v10, v0, Lcom/sec/chaton/io/entry/inner/Message;->id:Ljava/lang/String;

    iget-object v11, p0, Lcom/sec/chaton/d/a/bh;->e:Ljava/lang/String;

    invoke-static {v2, v9, v10, v11}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    const-string v9, "msg"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 134
    invoke-direct {p0, v5, v0}, Lcom/sec/chaton/d/a/bh;->a(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V

    :goto_2
    move-object v1, v0

    .line 146
    goto/16 :goto_1

    .line 137
    :cond_5
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    const-string v9, "media"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 138
    invoke-direct {p0, v5, v0}, Lcom/sec/chaton/d/a/bh;->c(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V

    goto :goto_2

    .line 141
    :cond_6
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    const-string v9, "content"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 142
    invoke-direct {p0, v5, v0}, Lcom/sec/chaton/d/a/bh;->b(Ljava/util/ArrayList;Lcom/sec/chaton/io/entry/inner/Message;)V

    goto :goto_2

    .line 145
    :cond_7
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Message;->type:Ljava/lang/String;

    const-string v9, "noti"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 146
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    move-object v0, v1

    goto :goto_2

    .line 150
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 151
    invoke-direct {p0, v5, v6}, Lcom/sec/chaton/d/a/bh;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 154
    :cond_a
    iget-object v0, v7, Lcom/sec/chaton/e/a/p;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-eqz v1, :cond_b

    .line 155
    invoke-direct {p0, v7, v1}, Lcom/sec/chaton/d/a/bh;->a(Lcom/sec/chaton/e/a/p;Lcom/sec/chaton/io/entry/inner/Message;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_b
    const-string v0, "0"

    .line 159
    if-eqz v3, :cond_c

    .line 160
    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Message;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Message;->tid:Ljava/lang/String;

    .line 165
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/e/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :try_start_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/d/a/bh;->b:Landroid/content/ContentResolver;

    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    :cond_c
    const-string v0, "-1"

    goto :goto_3

    .line 174
    :cond_d
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_f

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 176
    iget-object v1, p0, Lcom/sec/chaton/d/a/bh;->c:Ljava/lang/String;

    const-string v2, "-1"

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    :try_start_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_e

    .line 179
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 184
    :cond_e
    :goto_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAllMessages fail. code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 181
    :catch_1
    move-exception v0

    .line 182
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 188
    :cond_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAllMessages fail. code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method
