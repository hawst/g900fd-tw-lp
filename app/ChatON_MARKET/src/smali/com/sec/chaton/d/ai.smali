.class Lcom/sec/chaton/d/ai;
.super Lcom/sec/chaton/push/c;
.source "PushControl.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/ah;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/ah;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    invoke-direct {p0}, Lcom/sec/chaton/push/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 177
    const-string v0, "deregistration Error. errorCode : %s, ErrorMessage : %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/d/ag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    iget-object v0, v0, Lcom/sec/chaton/d/ah;->a:Lcom/sec/chaton/d/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/d/ag;->c()V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    invoke-static {v0}, Lcom/sec/chaton/d/ah;->a(Lcom/sec/chaton/d/ah;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 183
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 184
    const/16 v1, 0x3ea

    iput v1, v0, Landroid/os/Message;->what:I

    .line 185
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 186
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 187
    iget-object v1, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    invoke-static {v1}, Lcom/sec/chaton/d/ah;->a(Lcom/sec/chaton/d/ah;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 189
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 149
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "deregistration Success. message : %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/d/ag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    .line 161
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "get_version_push_reg_id"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    iget-object v0, v0, Lcom/sec/chaton/d/ah;->a:Lcom/sec/chaton/d/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/d/ag;->c()V

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    invoke-static {v0}, Lcom/sec/chaton/d/ah;->a(Lcom/sec/chaton/d/ah;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 167
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 168
    const/16 v1, 0x3ea

    iput v1, v0, Landroid/os/Message;->what:I

    .line 169
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 170
    iget-object v1, p0, Lcom/sec/chaton/d/ai;->a:Lcom/sec/chaton/d/ah;

    invoke-static {v1}, Lcom/sec/chaton/d/ah;->a(Lcom/sec/chaton/d/ah;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 172
    :cond_1
    return-void
.end method
