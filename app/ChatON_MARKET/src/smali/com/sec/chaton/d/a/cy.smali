.class public Lcom/sec/chaton/d/a/cy;
.super Lcom/sec/chaton/d/a/s;
.source "InitChatRoomTask.java"


# instance fields
.field private h:Lcom/sec/chaton/e/w;

.field private i:Lcom/sec/chaton/e/r;

.field private j:[Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/content/ContentResolver;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p9}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;ILcom/sec/chaton/msgsend/k;)V

    .line 86
    iput-wide p3, p0, Lcom/sec/chaton/d/a/cy;->f:J

    .line 87
    iput-object p2, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    .line 88
    iput-object p5, p0, Lcom/sec/chaton/d/a/cy;->h:Lcom/sec/chaton/e/w;

    .line 89
    iput-object p6, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    .line 90
    iput-object p7, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    .line 91
    iput-object p8, p0, Lcom/sec/chaton/d/a/cy;->k:Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    .line 95
    return-void
.end method

.method private a([Ljava/lang/String;J)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 350
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    const-string v0, ""

    .line 352
    array-length v5, p1

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v6, p1, v1

    .line 353
    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v3, v6, v7}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 355
    const-string v3, ""

    .line 356
    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v3, v7, v6}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 357
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    move-object v0, v3

    .line 360
    :cond_0
    if-nez v0, :cond_2

    .line 361
    const-string v3, "6002 error - invalidUserName: NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :goto_1
    const-string v3, "%d,%s,%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v8, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    invoke-virtual {v8}, Lcom/sec/chaton/e/aj;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x1

    aput-object v6, v7, v8

    const/4 v8, 0x2

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ";"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v3, v7, v6}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 363
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "6002 error - invalidUserName:"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 370
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 387
    :cond_4
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 115
    invoke-super/range {p0 .. p3}, Lcom/sec/chaton/d/a/s;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPostExecute()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    new-instance v10, Landroid/os/Message;

    invoke-direct {v10}, Landroid/os/Message;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput v0, v10, Landroid/os/Message;->what:I

    .line 121
    if-eqz p3, :cond_c

    .line 122
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 123
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/a/cx;

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/cx;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->d()I

    move-result v11

    .line 136
    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v12

    .line 137
    const/16 v0, 0x3e8

    if-eq v11, v0, :cond_0

    const/16 v0, 0x3e9

    if-eq v11, v0, :cond_0

    const/16 v0, 0xbba

    if-ne v11, v0, :cond_6

    .line 143
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 144
    const-string v0, "buddyname retrieve end"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/util/Map;)Z

    .line 146
    const-string v0, "participant insert start"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    array-length v4, v0

    .line 149
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 153
    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    aget-object v7, v0, v1

    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v6, v7, v0}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 156
    :cond_1
    :try_start_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :cond_2
    :goto_1
    const-string v0, "participant insert end"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v3, v4, v5}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->h:Lcom/sec/chaton/e/w;

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/a/cx;Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Lcom/sec/chaton/e/w;)Landroid/net/Uri;

    .line 172
    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->d()J

    move-result-wide v6

    iget-wide v0, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    array-length v9, v0

    invoke-static/range {v3 .. v9}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/String;JLjava/lang/Long;I)I

    .line 176
    const/16 v0, 0x3e9

    if-ne v11, v0, :cond_5

    .line 178
    const-string v0, "["

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const-string v1, "]"

    invoke-virtual {v12, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 180
    const-string v0, ""

    .line 182
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 184
    array-length v3, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 185
    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v6, v7, v4}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 186
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1001 error - invalidUserName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v6, v7, v4}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :catch_0
    move-exception v0

    .line 160
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "Fail to insert participant to DB"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 193
    new-instance v0, Lcom/sec/chaton/d/a/cz;

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v3

    iget-wide v6, p0, Lcom/sec/chaton/d/a/cy;->f:J

    move-object v1, p0

    move v2, v4

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/d/a/cz;-><init>(Lcom/sec/chaton/d/a/cy;ZLjava/lang/String;ILjava/util/ArrayList;J)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 241
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->b:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 288
    :goto_4
    return-void

    .line 195
    :cond_5
    new-instance v0, Lcom/sec/chaton/d/a/cz;

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v3

    iget-wide v5, p0, Lcom/sec/chaton/d/a/cy;->f:J

    move-object v1, p0

    move v2, v4

    move v4, v11

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/d/a/cz;-><init>(Lcom/sec/chaton/d/a/cy;ZLjava/lang/String;IJ)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_3

    .line 202
    :cond_6
    const/16 v0, 0x1b5e

    if-ne v11, v0, :cond_7

    .line 203
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/ej;)V

    .line 206
    :cond_7
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    iget-wide v3, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-direct {v0, v1, v11, v3, v4}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitChatReply : InBoxNO = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", InitChat Fail"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const/16 v0, 0xbb9

    if-ne v11, v0, :cond_8

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-wide v3, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Ljava/lang/Long;)I

    .line 219
    :goto_5
    const/16 v0, 0x1772

    if-ne v11, v0, :cond_b

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 221
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 223
    array-length v3, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v3, :cond_9

    aget-object v5, v1, v0

    .line 224
    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v6, v7, v5}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 215
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-wide v3, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_5

    .line 227
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 228
    array-length v0, v1

    if-lez v0, :cond_a

    .line 229
    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/chaton/d/a/cy;->a([Ljava/lang/String;J)V

    .line 234
    :cond_a
    new-instance v0, Lcom/sec/chaton/d/a/cz;

    const/4 v2, 0x0

    iget-wide v5, p0, Lcom/sec/chaton/d/a/cy;->f:J

    move-object v1, p0

    move v3, v11

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/d/a/cz;-><init>(Lcom/sec/chaton/d/a/cy;ZILjava/util/ArrayList;J)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_3

    .line 235
    :cond_b
    const/16 v0, 0x1b5e

    if-ne v11, v0, :cond_4

    .line 236
    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 237
    new-instance v0, Lcom/sec/chaton/d/a/cz;

    const/4 v3, 0x0

    invoke-virtual {v2}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v4

    iget-wide v5, p0, Lcom/sec/chaton/d/a/cy;->f:J

    move-object v1, p0

    move v2, v3

    move v3, v11

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/d/a/cz;-><init>(Lcom/sec/chaton/d/a/cy;ZILcom/sec/chaton/a/ea;J)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_3

    .line 247
    :cond_c
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/cy;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xb

    if-eq p1, v0, :cond_d

    const/16 v0, 0x16

    if-eq p1, v0, :cond_d

    const/4 v0, 0x3

    if-ne p1, v0, :cond_f

    .line 251
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->m:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {p1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-wide v1, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 258
    :goto_7
    const/16 v0, 0x18

    if-eq p1, v0, :cond_e

    const/16 v0, 0x17

    if-eq p1, v0, :cond_e

    const/16 v0, 0x15

    if-ne p1, v0, :cond_f

    .line 263
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 271
    :cond_f
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/sec/chaton/a/a/k;-><init>(ZIJ)V

    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->b:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 275
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    iget-wide v3, p0, Lcom/sec/chaton/d/a/cy;->f:J

    const-string v5, ""

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    move v2, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/chat/en;->a(ILjava/lang/String;IJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 255
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->l:Landroid/content/ContentResolver;

    iget-wide v1, p0, Lcom/sec/chaton/d/a/cy;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_7
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->m:Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->a()Z

    move-result v0

    goto :goto_0
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 8

    .prologue
    .line 101
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->b()Lcom/sec/chaton/j/ao;

    .line 103
    const-string v0, "broadcast2_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 105
    sget-object v0, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    .line 109
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/cy;->n:Ljava/lang/String;

    iget-wide v2, p0, Lcom/sec/chaton/d/a/cy;->f:J

    iget-object v4, p0, Lcom/sec/chaton/d/a/cy;->h:Lcom/sec/chaton/e/w;

    iget-object v5, p0, Lcom/sec/chaton/d/a/cy;->i:Lcom/sec/chaton/e/r;

    iget-object v6, p0, Lcom/sec/chaton/d/a/cy;->j:[Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/d/a/cy;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/aq;->a(Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
