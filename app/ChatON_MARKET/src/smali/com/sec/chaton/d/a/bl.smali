.class Lcom/sec/chaton/d/a/bl;
.super Ljava/lang/Object;
.source "GetAllUnReadMessageTask.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/chaton/io/entry/inner/Msg;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/a/bk;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/a/bk;)V
    .locals 0

    .prologue
    .line 467
    iput-object p1, p0, Lcom/sec/chaton/d/a/bl;->a:Lcom/sec/chaton/d/a/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/io/entry/inner/Msg;Lcom/sec/chaton/io/entry/inner/Msg;)I
    .locals 4

    .prologue
    .line 470
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 471
    const/4 v0, -0x1

    .line 475
    :goto_0
    return v0

    .line 472
    :cond_0
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    if-ne v0, v1, :cond_1

    .line 473
    const/4 v0, 0x0

    goto :goto_0

    .line 475
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 467
    check-cast p1, Lcom/sec/chaton/io/entry/inner/Msg;

    check-cast p2, Lcom/sec/chaton/io/entry/inner/Msg;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/d/a/bl;->a(Lcom/sec/chaton/io/entry/inner/Msg;Lcom/sec/chaton/io/entry/inner/Msg;)I

    move-result v0

    return v0
.end method
