.class Lcom/sec/chaton/d/bf;
.super Landroid/os/Handler;
.source "TaskContainer.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/bd;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/d/bd;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    .line 161
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 162
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 167
    const/4 v2, 0x0

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v0}, Lcom/sec/chaton/d/bd;->b(Lcom/sec/chaton/d/bd;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bd;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bg;

    .line 171
    sget-object v1, Lcom/sec/chaton/d/be;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->a()Lcom/sec/chaton/d/bh;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/d/bh;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    :cond_0
    move v1, v2

    .line 186
    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v1}, Lcom/sec/chaton/d/bd;->a(Lcom/sec/chaton/d/bd;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v2}, Lcom/sec/chaton/d/bd;->b(Lcom/sec/chaton/d/bd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v1}, Lcom/sec/chaton/d/bd;->c(Lcom/sec/chaton/d/bd;)Landroid/os/Handler;

    move-result-object v1

    if-nez v1, :cond_3

    .line 202
    :goto_1
    return-void

    .line 173
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/chaton/a/a/f;

    .line 174
    invoke-virtual {v1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-eq v3, v4, :cond_2

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_0

    .line 176
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 181
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/chaton/a/a/k;

    .line 182
    iget-boolean v1, v1, Lcom/sec/chaton/a/a/k;->a:Z

    goto :goto_0

    .line 191
    :cond_3
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 192
    iget v2, p1, Landroid/os/Message;->what:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 193
    new-instance v2, Lcom/sec/chaton/d/bi;

    iget-object v3, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v3}, Lcom/sec/chaton/d/bd;->d(Lcom/sec/chaton/d/bd;)J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->a()Lcom/sec/chaton/d/bh;

    move-result-object v0

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/sec/chaton/d/bi;-><init>(JLcom/sec/chaton/d/bh;Ljava/lang/Object;)V

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v0}, Lcom/sec/chaton/d/bd;->c(Lcom/sec/chaton/d/bd;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 198
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-static {v0}, Lcom/sec/chaton/d/bd;->e(Lcom/sec/chaton/d/bd;)I

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/d/bf;->a:Lcom/sec/chaton/d/bd;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bd;->d()Z

    goto :goto_1

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
