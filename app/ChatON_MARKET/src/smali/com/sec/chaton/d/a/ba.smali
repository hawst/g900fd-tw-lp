.class public Lcom/sec/chaton/d/a/ba;
.super Lcom/sec/chaton/d/a/c;
.source "ForwardOnlineMessageTask.java"


# instance fields
.field h:Lcom/sec/chaton/a/cr;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/chaton/e/r;

.field private l:Lcom/sec/chaton/a/cf;

.field private m:Landroid/content/ContentResolver;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/cf;Lcom/sec/chaton/d/o;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/c;-><init>(Landroid/os/Handler;)V

    .line 45
    iput v1, p0, Lcom/sec/chaton/d/a/ba;->n:I

    .line 50
    iput-object p2, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    .line 52
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ba;->m:Landroid/content/ContentResolver;

    .line 53
    iput-object p4, p0, Lcom/sec/chaton/d/a/ba;->g:Lcom/sec/chaton/d/o;

    .line 54
    iput v1, p0, Lcom/sec/chaton/d/a/ba;->n:I

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    .line 56
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 181
    if-eqz p3, :cond_0

    .line 182
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 183
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ci;

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MsgID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->j()Lcom/sec/chaton/a/cu;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", MsgType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->j()Lcom/sec/chaton/a/cu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/cu;->l()Lcom/sec/chaton/a/dp;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", Receiver : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->j()Lcom/sec/chaton/a/cu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/cu;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", Sender : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->j()Lcom/sec/chaton/a/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ForwardOnlineMessageReply]InboxNO : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", UID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_0
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 194
    const/16 v0, 0x22

    iput v0, v2, Landroid/os/Message;->what:I

    .line 197
    new-instance v0, Lcom/sec/chaton/a/a/e;

    invoke-direct {v0}, Lcom/sec/chaton/a/a/e;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/e;->a(Z)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cf;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/e;->a(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/e;->a(Lcom/sec/chaton/a/a/n;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget v3, p0, Lcom/sec/chaton/d/a/ba;->n:I

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/e;->a(I)Lcom/sec/chaton/a/a/e;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/sec/chaton/a/a/e;->c(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/sec/chaton/a/a/e;->b(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    if-nez v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/a/e;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/a/e;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/e;->a()Lcom/sec/chaton/a/a/d;

    move-result-object v0

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->b:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 209
    return-void

    .line 190
    :cond_0
    const-string v0, "Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v3

    invoke-static {v1, v3}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v1

    goto :goto_3
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 14

    .prologue
    .line 60
    iget-object v3, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->o()Ljava/lang/String;

    move-result-object v3

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->m:Landroid/content/ContentResolver;

    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v10

    .line 66
    if-nez v10, :cond_1

    .line 67
    const-string v0, "Error - InBoxData does not exist."

    const-string v1, "ForwardOnlineMessageTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    .line 173
    :goto_0
    return-object v0

    .line 72
    :cond_1
    iget v0, v10, Lcom/sec/chaton/e/a/p;->c:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ba;->k:Lcom/sec/chaton/e/r;

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    invoke-virtual {v0}, Lcom/sec/chaton/a/cf;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/ba;->j:Ljava/lang/String;

    .line 74
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 77
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 78
    const/4 v9, 0x0

    .line 79
    const/4 v11, 0x0

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    const-string v4, "ForwardOnlineMessageTask"

    invoke-static {v1, v2, v4}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/cf;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/sec/chaton/a/ci;->newBuilder()Lcom/sec/chaton/a/cj;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/cj;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/cj;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/a/ba;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/cj;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    move-result-object v12

    .line 89
    new-instance v1, Lcom/sec/chaton/i/a/c;

    invoke-direct {v1}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 90
    const-string v2, "%04d%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v13, 0x22

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x1

    const-string v13, "0001"

    aput-object v13, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    iget-object v4, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cf;->f()Lcom/sec/chaton/a/cr;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v4, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cf;->f()Lcom/sec/chaton/a/cr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_2

    .line 95
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 97
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 98
    const-string v2, "CH"

    invoke-virtual {v1}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/util/List;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->l:Lcom/sec/chaton/a/cf;

    invoke-virtual {v0}, Lcom/sec/chaton/a/cf;->f()Lcom/sec/chaton/a/cr;

    move-result-object v2

    .line 104
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/sec/chaton/d/a/ba;->j:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/d/a/ba;->k:Lcom/sec/chaton/e/r;

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/chat/fe;->a(ZLcom/sec/chaton/a/cr;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/util/ArrayList;Ljava/util/List;Ljava/util/HashMap;)Lcom/sec/chaton/chat/fg;

    move-result-object v1

    .line 105
    iget-boolean v0, v1, Lcom/sec/chaton/chat/fg;->c:Z

    if-eqz v0, :cond_3

    .line 106
    iget v0, p0, Lcom/sec/chaton/d/a/ba;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/d/a/ba;->n:I

    .line 109
    :cond_3
    if-nez v9, :cond_9

    iget-boolean v0, v1, Lcom/sec/chaton/chat/fg;->a:Z

    if-eqz v0, :cond_9

    move-object v0, v2

    .line 114
    :goto_1
    iget-boolean v4, v1, Lcom/sec/chaton/chat/fg;->b:Z

    if-eqz v4, :cond_4

    .line 115
    iput-object v2, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    .line 118
    :cond_4
    iget v1, v1, Lcom/sec/chaton/chat/fg;->d:I

    add-int/2addr v1, v11

    .line 121
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/chaton/chat/fe;->a(Ljava/util/HashMap;)V

    .line 124
    if-eqz v0, :cond_5

    .line 125
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/d/a/ba;->j:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/d/a/ba;->k:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/cr;)Z

    move-result v0

    .line 127
    if-eqz v0, :cond_5

    .line 128
    const/16 v0, 0xb

    iput v0, v10, Lcom/sec/chaton/e/a/p;->m:I

    .line 133
    :cond_5
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/ba;->h:Lcom/sec/chaton/a/cr;

    invoke-virtual {v0, v3, v1, v2, v10}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;ILcom/sec/chaton/a/cr;Lcom/sec/chaton/e/a/p;)V

    .line 136
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6, v3}, Lcom/sec/chaton/chat/fe;->a(ZLjava/util/ArrayList;Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/sec/chaton/d/a/ba;->k:Lcom/sec/chaton/e/r;

    iget-object v9, p0, Lcom/sec/chaton/d/a/ba;->j:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    invoke-virtual/range {v5 .. v10}, Lcom/sec/chaton/chat/fe;->a(ZLjava/util/List;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v1, Lcom/sec/chaton/i/a/c;

    invoke-direct {v1}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 143
    const-string v0, "%04d%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x23

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "0001"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 144
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    .line 147
    invoke-virtual {v12, v0}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cj;

    .line 148
    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 151
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v3, 0x2

    if-le v0, v3, :cond_7

    .line 152
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 154
    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 155
    const-string v0, "CH"

    invoke-virtual {v1}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->i:Ljava/lang/String;

    invoke-virtual {v12}, Lcom/sec/chaton/a/cj;->d()Lcom/sec/chaton/a/ci;

    move-result-object v1

    const-string v2, "ForwardOnlineMessageTask"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/ci;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/sec/chaton/j/ap;

    invoke-direct {v0}, Lcom/sec/chaton/j/ap;-><init>()V

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-virtual {v12}, Lcom/sec/chaton/a/cj;->d()Lcom/sec/chaton/a/ci;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/d/a/ba;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/d/a/ba;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->m()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 165
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/chaton/d/a/bb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/d/a/bb;-><init>(Lcom/sec/chaton/d/a/ba;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 173
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    move-object v0, v9

    goto/16 :goto_1
.end method
