.class public Lcom/sec/chaton/d/a/x;
.super Lcom/sec/chaton/d/a/a;
.source "BlockTask.java"


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 25
    iput-object p3, p0, Lcom/sec/chaton/d/a/x;->b:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_0

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 38
    iget-object v1, p0, Lcom/sec/chaton/d/a/x;->b:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v1, p0, Lcom/sec/chaton/d/a/x;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/e/a/n;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 44
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v3}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->a(Z)V

    .line 45
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v3}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    .line 47
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/f;->b(Ljava/lang/String;)V

    .line 49
    :cond_0
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return-object v0
.end method
