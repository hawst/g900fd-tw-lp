.class public Lcom/sec/chaton/d/a/aq;
.super Lcom/sec/chaton/d/a/c;
.source "DeliveryChatTask.java"


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field i:Lcom/sec/chaton/a/cr;

.field j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Lcom/sec/chaton/a/bg;

.field private m:Landroid/content/ContentResolver;

.field private n:Lcom/sec/chaton/chat/es;

.field private o:I

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/sec/chaton/d/a/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/bg;Lcom/sec/chaton/d/o;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/c;-><init>(Landroid/os/Handler;)V

    .line 75
    iput v1, p0, Lcom/sec/chaton/d/a/aq;->o:I

    .line 80
    iput-boolean v1, p0, Lcom/sec/chaton/d/a/aq;->q:Z

    .line 86
    iput-object p2, p0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    .line 87
    iput-object p3, p0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    .line 88
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    .line 91
    iput-object p4, p0, Lcom/sec/chaton/d/a/aq;->g:Lcom/sec/chaton/d/o;

    .line 92
    iput v1, p0, Lcom/sec/chaton/d/a/aq;->o:I

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/aq;->p:Ljava/util/ArrayList;

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/aq;->j:Ljava/util/ArrayList;

    .line 98
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 889
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/es;->a(Ljava/util/ArrayList;)V

    .line 894
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/aq;->q:Z

    if-eqz v0, :cond_0

    .line 971
    :goto_0
    return-void

    .line 900
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/a/cr;

    .line 901
    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 902
    const-string v0, ""

    .line 906
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 907
    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_6

    aget-object v0, v3, v2

    .line 910
    const-string v6, ","

    const/4 v8, 0x3

    invoke-static {v0, v6, v8}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    .line 912
    array-length v0, v6

    const/4 v8, 0x2

    if-le v0, v8, :cond_3

    .line 913
    const/4 v0, 0x1

    aget-object v8, v6, v0

    .line 914
    const/4 v0, 0x2

    aget-object v0, v6, v0

    .line 917
    iget-object v9, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v9, v8, v0}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 924
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v0, v8, p2}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 925
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 926
    const/4 v11, 0x0

    aget-object v11, v6, v11

    invoke-static {v11}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v11

    sget-object v12, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v11, v12, :cond_5

    .line 928
    if-nez v10, :cond_2

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v0, p2, v8, v9}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 932
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v6, "chaton_id"

    const-string v10, ""

    invoke-virtual {v0, v6, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 933
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 947
    :cond_2
    :goto_3
    sget-object v6, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    if-eq v0, v6, :cond_3

    .line 948
    const-string v6, "%d,%s,%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aj;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v11

    const/4 v0, 0x1

    aput-object v8, v10, v0

    const/4 v0, 0x2

    invoke-static {v9}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v0

    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ";"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 907
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 936
    :cond_4
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_3

    .line 940
    :cond_5
    const/4 v11, 0x0

    aget-object v6, v6, v11

    invoke-static {v6}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v6

    sget-object v11, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    if-ne v6, v11, :cond_2

    .line 942
    if-eqz v10, :cond_2

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v0, p2, v8}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    sget-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    goto :goto_3

    .line 957
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 958
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 959
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual {v1}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 964
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 965
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v0, p2}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 970
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/es;->e()V

    goto/16 :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 864
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() inboxNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldBuddyNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newBuddyNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    if-eqz p4, :cond_0

    invoke-virtual {p4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v0, p3, p2}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() - old buddyNo is exist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", skip insert"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const-string v0, "message_sender=? AND message_inbox_no =?"

    .line 874
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 876
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 877
    const-string v3, "message_sender"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 886
    :goto_0
    return-void

    .line 884
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() - Insert Participants : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    invoke-static {p2, p4}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/cr;)V
    .locals 21

    .prologue
    .line 810
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 811
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v5

    .line 812
    if-nez v5, :cond_1

    .line 813
    const-string v2, "Error - InBoxData does not exist."

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_0
    :goto_0
    return-void

    .line 817
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 823
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v3}, Lcom/sec/chaton/d/o;->n()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 831
    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v8

    .line 832
    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v4

    .line 833
    sget-object v2, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    if-ne v8, v2, :cond_3

    .line 835
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 836
    const-string v3, "push_message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 837
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 843
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v6

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v10}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v10

    iget-boolean v12, v5, Lcom/sec/chaton/e/a/p;->r:Z

    invoke-virtual/range {p4 .. p4}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v13

    iget-object v15, v5, Lcom/sec/chaton/e/a/p;->o:Ljava/lang/String;

    iget v0, v5, Lcom/sec/chaton/e/a/p;->p:I

    move/from16 v16, v0

    iget v0, v5, Lcom/sec/chaton/e/a/p;->h:I

    move/from16 v17, v0

    iget-wide v0, v5, Lcom/sec/chaton/e/a/p;->s:J

    move-wide/from16 v18, v0

    const-string v20, "N"

    move-object/from16 v5, p2

    invoke-virtual/range {v2 .. v20}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/w;ILjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IIJLjava/lang/String;)V

    goto/16 :goto_0

    .line 838
    :catch_0
    move-exception v2

    .line 839
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1020
    if-eqz p3, :cond_1

    .line 1021
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 1022
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bj;

    .line 1025
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 1026
    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->e()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1027
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] MsgID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bj;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/a/bl;->h()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", MsgType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bj;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->j()Lcom/sec/chaton/a/dp;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", Receiver : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bj;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", Sender : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bj;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1030
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[DeliveryChatReply]InboxNO : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", UID : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "uid"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", DeliveryChatReplyItemsCount : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->f()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", ResultCode : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->h()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->d()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", ResultMsg : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->h()Lcom/sec/chaton/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", DeliveryChatReplyItemsCount :"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    :goto_1
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1039
    const/4 v0, 0x6

    iput v0, v1, Landroid/os/Message;->what:I

    .line 1042
    new-instance v0, Lcom/sec/chaton/a/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/a/a/c;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/c;->a(Z)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v4}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/c;->a(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/c;->a(Lcom/sec/chaton/a/a/n;)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    iget v4, p0, Lcom/sec/chaton/d/a/aq;->o:I

    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/c;->a(I)Lcom/sec/chaton/a/a/c;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_2

    move-object v0, v3

    :goto_2
    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/a/c;->c(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    if-nez v0, :cond_3

    move-object v0, v3

    :goto_3
    invoke-virtual {v4, v0}, Lcom/sec/chaton/a/a/c;->b(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    if-nez v4, :cond_4

    :goto_4
    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/c;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/aq;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/a/c;->a(Ljava/util/List;)Lcom/sec/chaton/a/a/c;

    move-result-object v0

    .line 1054
    iget-object v3, p0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v3}, Lcom/sec/chaton/a/bg;->k()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1055
    iget-object v2, p0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bg;->l()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/a/c;->b(Z)Lcom/sec/chaton/a/a/c;

    .line 1074
    :goto_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/c;->a()Lcom/sec/chaton/a/a/b;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1076
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1078
    return-void

    .line 1033
    :cond_1
    const-string v0, "Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1042
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v4}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v3

    goto :goto_4

    .line 1064
    :cond_5
    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/a/c;->b(Z)Lcom/sec/chaton/a/a/c;

    goto :goto_5
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 30

    .prologue
    .line 110
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->o()Ljava/lang/String;

    move-result-object v6

    .line 116
    :cond_0
    invoke-static {v6}, Lcom/sec/chaton/chat/es;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/es;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bg;->f()Lcom/sec/chaton/a/bc;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v9

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/bg;Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v2, v6}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v24

    .line 149
    if-nez v24, :cond_1

    .line 150
    const-string v2, "Error - InBoxData does not exist."

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const/4 v2, 0x0

    .line 803
    :goto_0
    return-object v2

    .line 154
    :cond_1
    invoke-static {}, Lcom/sec/chaton/a/bj;->newBuilder()Lcom/sec/chaton/a/bk;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bk;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/bk;->b(I)Lcom/sec/chaton/a/bk;

    move-result-object v25

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bg;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bg;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bk;

    .line 160
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/chaton/d/a/aq;->q:Z

    .line 164
    :cond_2
    new-instance v3, Lcom/sec/chaton/i/a/c;

    invoke-direct {v3}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 165
    const-string v2, "%04d%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v7, 0x6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "0001"

    aput-object v7, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 167
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 170
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bg;->g()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/a/cr;

    .line 173
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ", "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 178
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v7, 0x2

    if-le v2, v7, :cond_4

    .line 179
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 182
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 183
    const-string v2, "CH"

    invoke-virtual {v3}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    new-instance v2, Lcom/sec/chaton/d/a/ar;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/chaton/d/a/ar;-><init>(Lcom/sec/chaton/d/a/aq;)V

    invoke-static {v5, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 198
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 200
    const/16 v23, 0x0

    .line 204
    const/4 v3, 0x0

    .line 214
    new-instance v28, Ljava/util/HashMap;

    invoke-direct/range {v28 .. v28}, Ljava/util/HashMap;-><init>()V

    .line 218
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_2
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/chaton/a/cr;

    .line 220
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_5
    :goto_3
    move-object v2, v3

    move/from16 v3, v23

    :goto_4
    move/from16 v23, v3

    move-object v3, v2

    .line 587
    goto :goto_2

    .line 226
    :pswitch_0
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v4

    invoke-static {v2, v4}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v4

    .line 228
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 240
    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    array-length v2, v15

    const/4 v7, 0x5

    if-ge v2, v7, :cond_6

    .line 241
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 242
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    .line 243
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v4

    .line 244
    const-string v2, "[throw away message] this one is not valid to process and show"

    sget-object v7, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " message id : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    move/from16 v3, v23

    .line 246
    goto :goto_4

    .line 254
    :cond_6
    sget-object v2, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_7

    .line 255
    const/4 v2, 0x2

    aget-object v2, v15, v2

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 257
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 259
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v7

    .line 261
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 262
    sget-object v8, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-eq v7, v8, :cond_12

    .line 263
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Ljava/lang/String;)V

    .line 278
    :cond_7
    :goto_5
    if-nez v3, :cond_31

    move-object/from16 v22, v5

    .line 286
    :goto_6
    const/4 v14, 0x0

    .line 287
    const/4 v13, 0x0

    .line 293
    sget-object v2, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_8

    sget-object v2, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_8

    sget-object v2, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_8

    sget-object v2, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_14

    :cond_8
    array-length v2, v15

    const/4 v3, 0x6

    if-lt v2, v3, :cond_14

    .line 298
    const/4 v2, 0x5

    aget-object v8, v15, v2

    .line 305
    :goto_7
    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_9

    .line 307
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 309
    sget-object v4, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 316
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 319
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/n;->n(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 320
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/n;->o(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 322
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/n;->m(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 324
    const/4 v11, 0x0

    .line 325
    const/4 v10, 0x0

    .line 329
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v3}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/a/cr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/l;

    move-result-object v2

    .line 344
    sget-object v3, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    if-ne v2, v3, :cond_15

    .line 345
    const/4 v2, 0x1

    .line 346
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/d/a/aq;->o:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/chaton/d/a/aq;->o:I

    .line 349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v14

    .line 364
    :goto_8
    sget-object v8, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-eq v4, v8, :cond_d

    .line 369
    sget-object v8, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v4, v8, :cond_b

    sget-object v8, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v4, v8, :cond_b

    sget-object v8, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne v4, v8, :cond_d

    .line 382
    :cond_b
    if-nez v2, :cond_c

    if-eqz v3, :cond_16

    :cond_c
    array-length v8, v15

    const/4 v10, 0x5

    if-lt v8, v10, :cond_16

    .line 397
    const/4 v8, 0x2

    aget-object v20, v15, v8

    .line 398
    const/4 v8, 0x3

    aget-object v21, v15, v8

    .line 400
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v13

    .line 406
    new-instance v15, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v15, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 409
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v16

    .line 410
    new-instance v10, Lcom/sec/chaton/d/a/as;

    move-object/from16 v11, p0

    move-object v12, v4

    move-object/from16 v17, v6

    move-object/from16 v18, v9

    move-object/from16 v19, v7

    invoke-direct/range {v10 .. v21}, Lcom/sec/chaton/d/a/as;-><init>(Lcom/sec/chaton/d/a/aq;Lcom/sec/chaton/e/w;JLandroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 494
    :cond_d
    :goto_9
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 495
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v4

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Lcom/sec/chaton/a/bm;->a(J)Lcom/sec/chaton/a/bm;

    move-result-object v4

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/chaton/a/bm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v4

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v8

    const-string v10, "chaton_id"

    const-string v11, ""

    invoke-virtual {v8, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/chaton/a/bm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/bm;->d()Lcom/sec/chaton/a/bl;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v4, v23

    .line 518
    :goto_a
    if-nez v2, :cond_e

    if-eqz v3, :cond_f

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v8

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/Long;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 521
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6, v9, v5}, Lcom/sec/chaton/d/a/aq;->a(ZLjava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/cr;)V

    .line 526
    :cond_f
    if-nez v2, :cond_10

    if-eqz v3, :cond_11

    .line 527
    :cond_10
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    .line 530
    :cond_11
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/n;->m(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 531
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v10

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v5, ""

    invoke-virtual {v2, v3, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    const/16 v17, 0x1

    :goto_b
    invoke-virtual/range {v10 .. v17}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 534
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable translate, inboxNo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/d/a/aq;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v2, v22

    move v3, v4

    goto/16 :goto_4

    .line 266
    :cond_12
    sget-object v7, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 270
    :cond_13
    sget-object v7, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 271
    sget-object v7, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 300
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_7

    .line 359
    :cond_15
    sget-object v3, Lcom/sec/chaton/e/l;->c:Lcom/sec/chaton/e/l;

    if-ne v2, v3, :cond_34

    .line 360
    const/4 v2, 0x1

    move v3, v2

    move v2, v13

    goto/16 :goto_8

    .line 458
    :cond_16
    array-length v4, v15

    const/4 v8, 0x4

    if-lt v4, v8, :cond_17

    .line 459
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No need to update and insert: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v8, 0x3

    aget-object v8, v15, v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 462
    :cond_17
    const-string v4, "Token Error"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 500
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/aq;->n:Lcom/sec/chaton/chat/es;

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/es;->a(Lcom/sec/chaton/a/cr;)V

    .line 506
    if-eqz v2, :cond_33

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v8, "chaton_id"

    const-string v10, ""

    invoke-virtual {v4, v8, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_33

    .line 507
    add-int/lit8 v23, v23, 0x1

    move/from16 v4, v23

    goto/16 :goto_a

    .line 531
    :cond_19
    const/16 v17, 0x0

    goto/16 :goto_b

    .line 549
    :pswitch_1
    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/sec/chaton/a/bm;->a(J)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v7, ""

    invoke-virtual {v4, v5, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/bm;->d()Lcom/sec/chaton/a/bl;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    move/from16 v3, v23

    .line 563
    goto/16 :goto_4

    .line 570
    :pswitch_2
    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 571
    const/4 v4, 0x1

    .line 572
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 574
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1a

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v8

    const-string v10, "chaton_id"

    const-string v11, ""

    invoke-virtual {v8, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 575
    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 576
    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v4

    .line 578
    :goto_c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    :cond_1a
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/sec/chaton/a/bm;->a(J)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/bm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/bm;->d()Lcom/sec/chaton/a/bl;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 592
    :cond_1b
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 597
    invoke-virtual/range {v28 .. v28}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 598
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 600
    invoke-static {}, Lcom/sec/chaton/e/v;->a()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 601
    const-string v8, "message_sever_id"

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v7, v8, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v10, "count"

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v8, v10, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 602
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 609
    :cond_1c
    :try_start_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1d

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    const-string v5, "com.sec.chaton.provider"

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 615
    :cond_1d
    :goto_e
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 619
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/a/aq;->q:Z

    if-nez v2, :cond_21

    if-eqz v3, :cond_21

    .line 620
    const/4 v2, 0x0

    .line 623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/n;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_20

    .line 625
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 631
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "chaton_id"

    const-string v10, ""

    invoke-virtual {v7, v8, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1e

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-static {v7, v4, v6}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1e

    .line 632
    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v2, v4}, Lcom/sec/chaton/d/a/aq;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const/4 v2, 0x1

    .line 639
    :cond_1e
    sget-object v7, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v9, v7, :cond_24

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_24

    move v3, v2

    .line 660
    :goto_f
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1f

    .line 661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    const-string v4, "com.sec.chaton.provider"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 669
    :cond_1f
    :goto_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    const/16 v4, 0xb

    invoke-static {v2, v6, v4}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    .line 670
    const/16 v2, 0xb

    move-object/from16 v0, v24

    iput v2, v0, Lcom/sec/chaton/e/a/p;->m:I

    move v2, v3

    .line 676
    :cond_20
    if-eqz v2, :cond_21

    .line 677
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->l:Lcom/sec/chaton/a/bg;

    invoke-virtual {v3}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v2, v6, v3, v4, v5}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 682
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    if-eqz v2, :cond_29

    .line 683
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v4

    move-object/from16 v0, v24

    iget-wide v7, v0, Lcom/sec/chaton/e/a/p;->g:J

    cmp-long v2, v4, v7

    if-ltz v2, :cond_22

    .line 694
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v7, "chaton_id"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 696
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    :goto_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v5

    invoke-static {v2, v5}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v2

    .line 702
    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 706
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onPreExecute - (lastmsg) last msg sender : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v7}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v5

    const-string v7, "0999"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    const/4 v7, 0x1

    if-ne v5, v7, :cond_28

    .line 709
    :try_start_2
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 710
    const-string v2, "push_message"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 711
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 721
    :goto_12
    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 723
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    iput-object v2, v0, Lcom/sec/chaton/e/a/p;->e:Ljava/lang/String;

    .line 724
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v2

    move-object/from16 v0, v24

    iput-wide v2, v0, Lcom/sec/chaton/e/a/p;->j:J

    .line 725
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    iput-object v2, v0, Lcom/sec/chaton/e/a/p;->k:Ljava/lang/String;

    .line 726
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->i:Lcom/sec/chaton/a/cr;

    invoke-virtual {v2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v2

    move-object/from16 v0, v24

    iput-wide v2, v0, Lcom/sec/chaton/e/a/p;->g:J

    .line 730
    :cond_22
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/d/a/aq;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 742
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 743
    const/16 v23, 0x0

    .line 748
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-static {v2, v6, v0, v1}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/a/p;I)I

    .line 761
    :goto_13
    new-instance v3, Lcom/sec/chaton/i/a/c;

    invoke-direct {v3}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 762
    const-string v2, "%04d%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v7, 0x7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "0001"

    aput-object v7, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 764
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 766
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_14
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/a/bl;

    .line 767
    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bk;

    .line 769
    invoke-virtual {v2}, Lcom/sec/chaton/a/bl;->h()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ", "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_14

    .line 612
    :catch_0
    move-exception v2

    .line 613
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_e

    .line 615
    :catchall_0
    move-exception v2

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    throw v2

    .line 645
    :cond_24
    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->f()I

    move-result v7

    .line 646
    const-string v4, ""

    .line 647
    const/4 v4, 0x0

    :goto_15
    if-ge v4, v7, :cond_2f

    .line 648
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 649
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v10, "chaton_id"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_25

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v10, "old_chaton_id"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_26

    .line 647
    :cond_25
    :goto_16
    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    .line 653
    :cond_26
    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v2, v8}, Lcom/sec/chaton/d/a/aq;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    const/4 v2, 0x1

    goto :goto_16

    .line 663
    :catch_1
    move-exception v2

    .line 664
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 698
    :cond_27
    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 712
    :catch_2
    move-exception v2

    .line 713
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_12

    .line 716
    :cond_28
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    .line 751
    :cond_29
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/d/a/aq;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 755
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->m:Landroid/content/ContentResolver;

    move-object/from16 v0, v24

    invoke-static {v2, v6, v0}, Lcom/sec/chaton/e/a/n;->b(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/a/p;)I

    goto/16 :goto_13

    .line 773
    :cond_2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v5, 0x2

    if-le v2, v5, :cond_2b

    .line 774
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 777
    :cond_2b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 778
    const-string v2, "CH"

    invoke-virtual {v3}, Lcom/sec/chaton/i/a/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_2d

    .line 782
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 783
    const/4 v2, 0x0

    :goto_17
    invoke-virtual/range {v25 .. v25}, Lcom/sec/chaton/a/bk;->f()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2c

    .line 784
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] Receiver : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/a/bl;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", MsgType : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->j()Lcom/sec/chaton/a/dp;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", MsgID : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->h()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", Sender : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/bk;->a(I)Lcom/sec/chaton/a/bl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/a/bl;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_17

    .line 787
    :cond_2c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DeliveryChatReply]InboxNO : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", UID : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", DeliveryChatReplyItemsCount: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v25 .. v25}, Lcom/sec/chaton/a/bk;->g()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", NextPaginationKey: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v25 .. v25}, Lcom/sec/chaton/a/bk;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :cond_2d
    new-instance v2, Lcom/sec/chaton/j/ap;

    invoke-direct {v2}, Lcom/sec/chaton/j/ap;-><init>()V

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v2

    invoke-virtual/range {v25 .. v25}, Lcom/sec/chaton/a/bk;->d()Lcom/sec/chaton/a/bj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v2

    .line 792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/aq;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v2}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;Lcom/sec/chaton/j/ao;)V

    .line 794
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/aq;->g:Lcom/sec/chaton/d/o;

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->m()Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 795
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/chaton/d/a/at;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/chaton/d/a/at;-><init>(Lcom/sec/chaton/d/a/aq;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 803
    :cond_2e
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2f
    move v3, v2

    goto/16 :goto_f

    :cond_30
    move v2, v4

    goto/16 :goto_c

    :cond_31
    move-object/from16 v22, v3

    goto/16 :goto_6

    :cond_32
    move-object/from16 v2, v22

    move v3, v4

    goto/16 :goto_4

    :cond_33
    move/from16 v4, v23

    goto/16 :goto_a

    :cond_34
    move v2, v13

    move v3, v14

    goto/16 :goto_8

    .line 220
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
