.class public Lcom/sec/chaton/d/a/en;
.super Lcom/sec/chaton/d/a/a;
.source "UploadProfileTask2.java"


# instance fields
.field private b:Lcom/sec/chaton/a/a/o;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/a/a/o;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 32
    iput-object p3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    .line 33
    iput-object p4, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 73
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_9

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/o;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "Push Name"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/o;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 78
    const-string v0, "status_message"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/o;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 81
    const-string v0, "birthday"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "birthday_type"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 85
    :cond_2
    const-string v0, "birthday_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 89
    :goto_0
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 90
    :cond_3
    const-string v0, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 95
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/o;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 96
    const-string v0, "email"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/o;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/o;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 99
    const-string v0, "samsung_email"

    iget-object v1, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/o;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_6
    :goto_2
    return-void

    .line 87
    :cond_7
    const-string v0, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 92
    :cond_8
    const-string v0, "birthday_year_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 104
    :cond_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v2, :cond_6

    .line 105
    const-string v0, "01000004"

    const-string v1, "0001"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_2
.end method

.method public f()Ljava/lang/String;
    .locals 6

    .prologue
    .line 39
    new-instance v0, Lcom/sec/chaton/util/af;

    invoke-direct {v0}, Lcom/sec/chaton/util/af;-><init>()V

    .line 40
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 41
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 42
    const-string v2, "name"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 45
    const-string v2, "status"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 48
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/d/a/en;->c:Ljava/lang/String;

    .line 49
    const-string v2, "FULL"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 50
    const-string v2, "birthday"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 60
    const-string v2, "email"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/o;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 63
    const-string v2, "samsungemail"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_4
    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-object v0

    .line 51
    :cond_5
    const-string v2, "SHORT"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 52
    const-string v2, "birthday"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->b:Lcom/sec/chaton/a/a/o;

    invoke-virtual {v3}, Lcom/sec/chaton/a/a/o;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    const/16 v5, 0xa

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 53
    :cond_6
    const-string v2, "DONT"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "FULL_HIDE"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "SHORT_HIDE"

    iget-object v3, p0, Lcom/sec/chaton/d/a/en;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 54
    :cond_7
    const-string v2, "birthday"

    const-string v3, "0000-12-31"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 56
    :cond_8
    const-string v2, "Unknown birthday option"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
