.class public final enum Lcom/sec/chaton/d/az;
.super Ljava/lang/Enum;
.source "SnsAccountControl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/d/az;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/d/az;

.field public static final enum b:Lcom/sec/chaton/d/az;

.field public static final enum c:Lcom/sec/chaton/d/az;

.field public static final enum d:Lcom/sec/chaton/d/az;

.field public static final enum e:Lcom/sec/chaton/d/az;

.field public static final enum f:Lcom/sec/chaton/d/az;

.field public static final enum g:Lcom/sec/chaton/d/az;

.field private static h:Lcom/sec/chaton/d/az;

.field private static final synthetic i:[Lcom/sec/chaton/d/az;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "STARTING"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->a:Lcom/sec/chaton/d/az;

    .line 52
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "SKIPING_SMS"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->b:Lcom/sec/chaton/d/az;

    .line 53
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "SIGNING_UP"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->c:Lcom/sec/chaton/d/az;

    .line 54
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "SYNCING_FRIENDS"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->d:Lcom/sec/chaton/d/az;

    .line 55
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "RETRIEVING_FRIENDS"

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->e:Lcom/sec/chaton/d/az;

    .line 56
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "ADDING_BUDDIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->f:Lcom/sec/chaton/d/az;

    .line 57
    new-instance v0, Lcom/sec/chaton/d/az;

    const-string v1, "PROFILE_UPLOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/d/az;->g:Lcom/sec/chaton/d/az;

    .line 50
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/chaton/d/az;

    sget-object v1, Lcom/sec/chaton/d/az;->a:Lcom/sec/chaton/d/az;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/d/az;->b:Lcom/sec/chaton/d/az;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/d/az;->c:Lcom/sec/chaton/d/az;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/d/az;->d:Lcom/sec/chaton/d/az;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/d/az;->e:Lcom/sec/chaton/d/az;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/d/az;->f:Lcom/sec/chaton/d/az;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/d/az;->g:Lcom/sec/chaton/d/az;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/d/az;->i:[Lcom/sec/chaton/d/az;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()Lcom/sec/chaton/d/az;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/chaton/d/az;->h:Lcom/sec/chaton/d/az;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/d/az;)V
    .locals 0

    .prologue
    .line 61
    sput-object p0, Lcom/sec/chaton/d/az;->h:Lcom/sec/chaton/d/az;

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/d/az;
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/chaton/d/az;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/az;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/d/az;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/chaton/d/az;->i:[Lcom/sec/chaton/d/az;

    invoke-virtual {v0}, [Lcom/sec/chaton/d/az;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/d/az;

    return-object v0
.end method
