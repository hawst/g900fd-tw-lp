.class public Lcom/sec/chaton/d/a/aw;
.super Lcom/sec/chaton/d/a/e;
.source "EndChatTask.java"


# instance fields
.field private h:Lcom/sec/chaton/e/r;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/e;-><init>(Landroid/os/Handler;)V

    .line 54
    iput-object p2, p0, Lcom/sec/chaton/d/a/aw;->h:Lcom/sec/chaton/e/r;

    .line 55
    iput-object p3, p0, Lcom/sec/chaton/d/a/aw;->i:Ljava/lang/String;

    .line 56
    iput-object p4, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    .line 58
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    .line 60
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/d/a/aw;->f:J

    .line 61
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 347
    new-instance v0, Lcom/sec/chaton/d/a/ax;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/a/ax;-><init>(Lcom/sec/chaton/d/a/aw;Ljava/lang/String;)V

    .line 354
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 355
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 356
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 357
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    .line 358
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Delete File] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "EndChatTask"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363
    :cond_0
    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/a/aw;->a(Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v7

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "inbox_old_no"

    aput-object v3, v2, v6

    const-string v3, "inbox_no=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    aput-object v8, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_1

    .line 316
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 319
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 324
    const-string v0, ","

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 325
    array-length v0, v1

    if-lez v0, :cond_2

    .line 326
    array-length v2, v1

    move v0, v6

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 327
    invoke-direct {p0, v3}, Lcom/sec/chaton/d/a/aw;->a(Ljava/lang/String;)V

    .line 328
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 332
    :cond_2
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/16 v8, 0x1b5a

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 151
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/e;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 153
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 154
    const/16 v0, 0x8

    iput v0, v1, Landroid/os/Message;->what:I

    .line 156
    if-eqz p3, :cond_5

    .line 157
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 160
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 297
    :goto_0
    return-void

    .line 189
    :pswitch_0
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bz;

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ch@t[EndChatReply]UID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ResultCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ResultMessage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->f()Lcom/sec/chaton/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MsgID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/ej;->d()I

    move-result v2

    .line 199
    const/16 v3, 0x3e8

    if-eq v2, v3, :cond_0

    const/16 v3, 0x1772

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3e9

    if-eq v2, v3, :cond_0

    const/16 v3, 0xbbe

    if-eq v2, v3, :cond_0

    const/16 v3, 0x1b59

    if-eq v2, v3, :cond_0

    if-eq v2, v8, :cond_0

    const/16 v3, 0x1b5b

    if-ne v2, v3, :cond_4

    .line 207
    :cond_0
    if-ne v2, v8, :cond_1

    .line 208
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/aw;->b:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    .line 212
    :cond_1
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v7, v2}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 216
    invoke-direct {p0}, Lcom/sec/chaton/d/a/aw;->c()V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const-string v2, "message_inbox_no=?"

    new-array v3, v7, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v2, "inbox_no=?"

    new-array v3, v7, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 224
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    sget v2, Lcom/sec/chaton/chat/notification/a;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    .line 247
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 252
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 253
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    goto :goto_1

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    const-string v2, "participants_inbox_no=?"

    new-array v3, v7, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->i()V

    .line 264
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 265
    :catch_0
    move-exception v0

    .line 266
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 269
    :cond_4
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v6, v2}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 281
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/aw;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0xb

    if-eq p1, v0, :cond_6

    const/16 v0, 0x16

    if-eq p1, v0, :cond_6

    const/4 v0, 0x3

    if-ne p1, v0, :cond_8

    .line 283
    :cond_6
    const/16 v0, 0x18

    if-eq p1, v0, :cond_7

    const/16 v0, 0x17

    if-eq p1, v0, :cond_7

    const/16 v0, 0x15

    if-ne p1, v0, :cond_8

    .line 286
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 291
    :cond_8
    const-string v0, "Fail to send endChat message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/sec/chaton/a/a/k;

    invoke-direct {v0, v6, p1}, Lcom/sec/chaton/a/a/k;-><init>(ZI)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 5

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/chaton/d/a/e;->b()Lcom/sec/chaton/j/ao;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    iput-object v0, p0, Lcom/sec/chaton/d/a/aw;->i:Ljava/lang/String;

    .line 95
    :cond_0
    iget-wide v0, p0, Lcom/sec/chaton/d/a/aw;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 96
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/d/a/aw;->f:J

    .line 100
    :cond_1
    invoke-static {}, Lcom/sec/chaton/a/cc;->newBuilder()Lcom/sec/chaton/a/cd;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/d/a/aw;->f:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/cd;->a(J)Lcom/sec/chaton/a/cd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->h:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cd;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cd;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cd;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cd;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->k:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 115
    if-eqz v1, :cond_2

    array-length v2, v1

    if-lez v2, :cond_2

    .line 116
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cd;->d(Ljava/lang/String;)Lcom/sec/chaton/a/cd;

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 123
    iget-object v1, p0, Lcom/sec/chaton/d/a/aw;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v1

    .line 124
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    .line 125
    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/cd;->b(J)Lcom/sec/chaton/a/cd;

    .line 130
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ch@t[EndChatRequest]UID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ChatType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->g()Lcom/sec/chaton/a/bc;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MsgID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Receivers : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->j()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Sender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", LastSessionMergeTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->k()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SessionID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/a/cd;->d()Lcom/sec/chaton/a/cc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
