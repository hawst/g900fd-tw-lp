.class Lcom/sec/chaton/d/aw;
.super Ljava/lang/Object;
.source "SnsAccountControl.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/ad;


# instance fields
.field final synthetic a:Lcom/sec/chaton/d/au;


# direct methods
.method constructor <init>(Lcom/sec/chaton/d/au;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 418
    const-string v0, "onCancelled get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    const/16 v1, 0x517

    invoke-static {v0, v1}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/d/au;I)V

    .line 421
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 425
    const-string v0, "onError get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    const/16 v1, 0x516

    invoke-static {v0, v1}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/d/au;I)V

    .line 427
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 396
    const-string v0, "onComplete get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v0}, Lcom/sec/chaton/d/au;->c(Lcom/sec/chaton/d/au;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 400
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v0}, Lcom/sec/chaton/d/au;->d(Lcom/sec/chaton/d/au;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/tellfriends/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v0}, Lcom/sec/chaton/d/au;->c(Lcom/sec/chaton/d/au;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/settings/tellfriends/a;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/sec/chaton/settings/tellfriends/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v0}, Lcom/sec/chaton/d/au;->c(Lcom/sec/chaton/d/au;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/settings/tellfriends/a;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/sec/chaton/settings/tellfriends/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 409
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    iget-object v1, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v1}, Lcom/sec/chaton/d/au;->e(Lcom/sec/chaton/d/au;)[J

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/d/au;[J)[J

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    invoke-static {v0}, Lcom/sec/chaton/d/au;->f(Lcom/sec/chaton/d/au;)V

    .line 414
    :goto_2
    return-void

    .line 412
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/d/aw;->a:Lcom/sec/chaton/d/au;

    const/16 v1, 0x515

    invoke-static {v0, v1}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/d/au;I)V

    goto :goto_2
.end method
