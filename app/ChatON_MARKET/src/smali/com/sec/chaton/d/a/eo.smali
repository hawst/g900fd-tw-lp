.class public Lcom/sec/chaton/d/a/eo;
.super Lcom/sec/chaton/d/a/a;
.source "VerifySMSTask.java"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 33
    iput-object p3, p0, Lcom/sec/chaton/d/a/eo;->b:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/sec/chaton/d/a/eo;->c:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/sec/chaton/d/a/eo;->e:Ljava/lang/String;

    .line 37
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/eo;->f:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 58
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/RegisterSSMServer;

    .line 59
    const-string v0, "Success, verifySMS task"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/sec/chaton/util/af;

    invoke-direct {v0}, Lcom/sec/chaton/util/af;-><init>()V

    .line 45
    new-instance v1, Ljava/util/LinkedHashMap;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 46
    const-string v2, "msisdn"

    iget-object v3, p0, Lcom/sec/chaton/d/a/eo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v2, "token"

    iget-object v3, p0, Lcom/sec/chaton/d/a/eo;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v2, "authnum"

    iget-object v3, p0, Lcom/sec/chaton/d/a/eo;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-object v0
.end method
