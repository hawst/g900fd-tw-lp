.class public Lcom/sec/chaton/d/bd;
.super Ljava/lang/Object;
.source "TaskContainer.java"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/d/bg;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Handler;

.field private d:I

.field private e:Lcom/sec/chaton/j/ak;

.field private f:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/d/bd;->d:I

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    .line 49
    iput-object p1, p0, Lcom/sec/chaton/d/bd;->c:Landroid/os/Handler;

    .line 50
    iput-object p2, p0, Lcom/sec/chaton/d/bd;->e:Lcom/sec/chaton/j/ak;

    .line 51
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/d/bd;->f:J

    .line 53
    new-instance v0, Lcom/sec/chaton/d/bf;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/bf;-><init>(Lcom/sec/chaton/d/bd;Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/bd;->a(Landroid/os/Handler;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/ak;J)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/d/bd;->d:I

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    .line 60
    iput-object p1, p0, Lcom/sec/chaton/d/bd;->c:Landroid/os/Handler;

    .line 61
    iput-object p2, p0, Lcom/sec/chaton/d/bd;->e:Lcom/sec/chaton/j/ak;

    .line 62
    iput-wide p3, p0, Lcom/sec/chaton/d/bd;->f:J

    .line 64
    new-instance v0, Lcom/sec/chaton/d/bf;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/d/bf;-><init>(Lcom/sec/chaton/d/bd;Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/bd;->a(Landroid/os/Handler;)V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/d/bd;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/chaton/d/bd;->d:I

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/d/bd;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/d/bd;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/d/bd;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/sec/chaton/d/bd;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcom/sec/chaton/d/bd;)I
    .locals 2

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/chaton/d/bd;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/d/bd;->d:I

    return v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/sec/chaton/d/bd;->f:J

    return-wide v0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/d/bd;->b:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method public a(Lcom/sec/chaton/d/bh;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddTaskToList : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/d/bh;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TaskContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/d/bg;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/chaton/d/bg;-><init>(Lcom/sec/chaton/d/bd;Lcom/sec/chaton/d/bh;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/d/bd;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bg;

    .line 87
    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/d/bd;->d:I

    iget-object v2, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 112
    :goto_0
    return v0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/d/bd;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/bg;

    .line 100
    sget-object v2, Lcom/sec/chaton/d/be;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->a()Lcom/sec/chaton/d/bh;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/d/bh;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 109
    goto :goto_0

    .line 102
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/w;->b()Lcom/sec/chaton/j/l;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 112
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/d/bg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/c;

    iget-object v1, p0, Lcom/sec/chaton/d/bd;->e:Lcom/sec/chaton/j/ak;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a/c;->a(Lcom/sec/chaton/j/ak;)V

    goto :goto_1

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 127
    const/4 v0, 0x1

    .line 128
    iget v1, p0, Lcom/sec/chaton/d/bd;->d:I

    add-int/lit8 v1, v1, 0x1

    .line 130
    iget-object v2, p0, Lcom/sec/chaton/d/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 134
    :cond_0
    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/chaton/d/bd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/d/bd;->d:I

    .line 139
    return-void
.end method
