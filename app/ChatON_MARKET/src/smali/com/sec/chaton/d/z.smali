.class public Lcom/sec/chaton/d/z;
.super Lcom/sec/chaton/d/a;
.source "PublicPushControl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/d/a",
        "<",
        "Lcom/sec/spp/push/IPushClientService;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static d:Lcom/sec/chaton/d/z;


# instance fields
.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/d/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/chaton/d/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/z;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 45
    const-string v0, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/a;-><init>(Ljava/lang/String;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/z;->e:Ljava/util/Map;

    .line 48
    new-instance v0, Lcom/sec/chaton/d/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/ae;-><init>(Lcom/sec/chaton/d/z;)V

    iput-object v0, p0, Lcom/sec/chaton/d/z;->f:Lcom/sec/chaton/d/ae;

    .line 51
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/z;->f:Lcom/sec/chaton/d/ae;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.spp.RegistrationChangedAction"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/d/z;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/d/z;->e:Ljava/util/Map;

    return-object v0
.end method

.method public static e()Lcom/sec/chaton/d/z;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/chaton/d/z;->d:Lcom/sec/chaton/d/z;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/chaton/d/z;

    invoke-direct {v0}, Lcom/sec/chaton/d/z;-><init>()V

    sput-object v0, Lcom/sec/chaton/d/z;->d:Lcom/sec/chaton/d/z;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/z;->d:Lcom/sec/chaton/d/z;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/chaton/d/z;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/z;->b(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/sec/chaton/d/af;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/af;-><init>(Lcom/sec/chaton/d/z;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/z;->a(Ljava/lang/Runnable;)V

    .line 73
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/sec/chaton/d/ad;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/ad;-><init>(Lcom/sec/chaton/d/z;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/z;->a(Ljava/lang/Runnable;)V

    .line 93
    return-void
.end method

.method protected b(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;
    .locals 1

    .prologue
    .line 97
    invoke-static {p1}, Lcom/sec/spp/push/IPushClientService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/sec/chaton/d/aa;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/aa;-><init>(Lcom/sec/chaton/d/z;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/z;->a(Ljava/lang/Runnable;)V

    .line 78
    return-void
.end method

.method public c(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/sec/chaton/d/ac;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/ac;-><init>(Lcom/sec/chaton/d/z;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/z;->a(Ljava/lang/Runnable;)V

    .line 83
    return-void
.end method

.method public d(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/sec/chaton/d/ab;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/d/ab;-><init>(Lcom/sec/chaton/d/z;Landroid/os/Handler;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/d/z;->a(Ljava/lang/Runnable;)V

    .line 88
    return-void
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 59
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/z;->f:Lcom/sec/chaton/d/ae;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 60
    return-void
.end method
