.class public Lcom/sec/chaton/d/a/bk;
.super Lcom/sec/chaton/d/a/a;
.source "GetAllUnReadMessageTask.java"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/io/entry/inner/MsgTid;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/ContentResolver;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/d/a/bn;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 60
    const-string v0, "GetAllUnReadMessageTask"

    iput-object v0, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    .line 79
    iput-boolean v1, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    .line 83
    iput-boolean v1, p0, Lcom/sec/chaton/d/a/bk;->h:Z

    .line 90
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    .line 100
    return-void
.end method

.method private a(Lcom/sec/chaton/io/entry/inner/Msg;Ljava/lang/String;)V
    .locals 21

    .prologue
    .line 851
    if-eqz p1, :cond_0

    .line 852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v5

    .line 854
    if-nez v5, :cond_1

    .line 855
    const-string v2, "Error - InBoxData does not exist."

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    :cond_0
    :goto_0
    return-void

    .line 856
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v6, v5, Lcom/sec/chaton/e/a/p;->g:J

    cmp-long v2, v2, v6

    if-gez v2, :cond_2

    .line 857
    const-string v2, "already received msg. so don\'t have to show notification"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 862
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/chaton/d/a/bk;->g:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 865
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v8

    .line 869
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v2

    .line 870
    const-string v3, "callChatList"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 872
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v9

    .line 873
    sget-object v3, Lcom/sec/chaton/d/a/bm;->a:[I

    invoke-virtual {v9}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 895
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v3}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;)I

    move-result v3

    .line 896
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    const/4 v6, -0x1

    const/high16 v7, 0x10000000

    invoke-static {v4, v6, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    .line 897
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 898
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[NOTI] ChatType: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", unReadCount: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Memory Address:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    .line 904
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    const-string v4, "0999"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 906
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 907
    const-string v4, "push_message"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 913
    :cond_4
    :goto_2
    sget-object v3, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    if-ne v8, v3, :cond_6

    .line 915
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 916
    const-string v4, "push_message"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 917
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    move-object v4, v2

    .line 927
    :goto_3
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-nez v2, :cond_5

    .line 928
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 932
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v9}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v11, v0, v12}, Lcom/sec/chaton/e/a/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iget-boolean v12, v5, Lcom/sec/chaton/e/a/p;->r:Z

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    iget-object v15, v5, Lcom/sec/chaton/e/a/p;->o:Ljava/lang/String;

    iget v0, v5, Lcom/sec/chaton/e/a/p;->p:I

    move/from16 v16, v0

    iget v0, v5, Lcom/sec/chaton/e/a/p;->h:I

    move/from16 v17, v0

    iget-wide v0, v5, Lcom/sec/chaton/e/a/p;->s:J

    move-wide/from16 v18, v0

    const-string v20, "N"

    move-object/from16 v5, p2

    invoke-virtual/range {v2 .. v20}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/w;ILjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IIJLjava/lang/String;)V

    goto/16 :goto_0

    .line 876
    :pswitch_0
    const-string v3, "inboxNO"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 877
    const-string v3, "chatType"

    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v4}, Lcom/sec/chaton/e/r;->a()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 878
    const-string v3, "fromPush"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 879
    const-string v3, "buddyNO"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 882
    :pswitch_1
    const-string v3, "inboxNO"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 883
    const-string v3, "chatType"

    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v4}, Lcom/sec/chaton/e/r;->a()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 884
    const-string v3, "fromPush"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 885
    const-string v3, "buddyNO"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 888
    :pswitch_2
    const-string v3, "inboxNO"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 889
    const-string v3, "chatType"

    sget-object v4, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v4}, Lcom/sec/chaton/e/r;->a()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 890
    const-string v3, "fromPush"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 891
    const-string v3, "buddyNO"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 908
    :catch_0
    move-exception v3

    .line 909
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_2

    .line 918
    :catch_1
    move-exception v3

    .line 919
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_6
    move-object v4, v2

    goto/16 :goto_3

    .line 873
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/bn;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/bn;->d()I

    move-result v1

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/bn;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a/bn;->a(I)V

    .line 434
    :goto_0
    return-void

    .line 431
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error - inboxNO:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not in inboxMsgMap. You can not reach here"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V
    .locals 3

    .prologue
    .line 109
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    .line 111
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    .line 112
    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type : Message, Sender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MsgServerID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ChatType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Tid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Content : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ContentLength : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MsgType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SessionId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Receiver : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_1
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    .line 119
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, p2, v2}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Msg;Ljava/lang/String;)Lcom/sec/chaton/e/l;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    if-ne v0, v1, :cond_3

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;)V

    .line 127
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    .line 138
    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 283
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MsgID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Time : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Type : NOTI, SessionID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ChatType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MSG : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_0
    if-eqz p1, :cond_6

    iget-object v0, v7, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 296
    const-string v0, ""

    .line 297
    iget-object v0, v7, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 301
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v0, v2, v1

    .line 303
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Row : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const-string v5, ","

    const/4 v6, 0x3

    invoke-static {v0, v5, v6}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 308
    array-length v0, v5

    const/4 v6, 0x2

    if-le v0, v6, :cond_2

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const/4 v6, 0x1

    aget-object v6, v5, v6

    invoke-static {v0, v6}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 310
    const/4 v0, 0x2

    aget-object v0, v5, v0

    .line 311
    iget-object v9, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v9, v6, v0}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v0, v6, p1}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 319
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 320
    const/4 v11, 0x0

    aget-object v11, v5, v11

    invoke-static {v11}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v11

    sget-object v12, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v11, v12, :cond_4

    .line 322
    if-nez v10, :cond_1

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v0, p1, v6, v9}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 326
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v5, "chaton_id"

    const-string v10, ""

    invoke-virtual {v0, v5, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, v7, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 341
    :cond_1
    :goto_2
    sget-object v5, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    if-eq v0, v5, :cond_2

    .line 342
    const-string v5, "%d,%s,%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aj;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v11

    const/4 v0, 0x1

    aput-object v6, v10, v0

    const/4 v0, 0x2

    invoke-static {v9}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 330
    :cond_3
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_2

    .line 334
    :cond_4
    const/4 v11, 0x0

    aget-object v5, v5, v11

    invoke-static {v5}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v5

    sget-object v11, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    if-ne v5, v11, :cond_1

    .line 336
    if-eqz v10, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v0, p1, v6}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    sget-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    goto :goto_2

    .line 350
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 351
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, v7, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    iget-object v6, v7, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 359
    :cond_6
    new-instance v0, Lcom/sec/chaton/io/entry/inner/MsgTid;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/MsgTid;-><init>()V

    .line 360
    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/MsgTid;->value:Ljava/lang/String;

    .line 361
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 363
    :cond_7
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Msg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 373
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 377
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 378
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type : ANS , Sender : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Receiver : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChatType : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", MsgID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", tid : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_0
    const/4 v2, 0x1

    .line 382
    iget-object v0, v1, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    .line 383
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "chaton_id"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 384
    iget-object v0, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 385
    iget-object v0, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    .line 387
    :goto_1
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    :cond_1
    new-instance v0, Lcom/sec/chaton/io/entry/inner/MsgTid;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/MsgTid;-><init>()V

    .line 391
    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/MsgTid;->value:Ljava/lang/String;

    .line 392
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 398
    :cond_2
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 399
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 400
    invoke-static {}, Lcom/sec/chaton/e/v;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 401
    const-string v4, "message_sever_id"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "count"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 402
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 406
    :cond_3
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const-string v1, "com.sec.chaton.provider"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    :cond_4
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 415
    return-void

    .line 409
    :catch_0
    move-exception v0

    .line 410
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 412
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V
    .locals 3

    .prologue
    .line 141
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    .line 143
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    .line 144
    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 146
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type : Message, Sender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MsgServerID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ChatType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Tid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Content : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ContentLength : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MsgType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SessionId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Receiver : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_1
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/chaton/e/a/q;->a(Lorg/json/JSONObject;)Lcom/sec/chaton/e/w;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, p2, v2}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Msg;Ljava/lang/String;)Lcom/sec/chaton/e/l;

    move-result-object v0

    .line 159
    sget-object v1, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    if-ne v0, v1, :cond_3

    .line 161
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;)V

    .line 166
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    .line 177
    :cond_3
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V
    .locals 6

    .prologue
    const/4 v4, 0x5

    const/4 v5, 0x1

    .line 180
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Type : Media, Sender : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ChatType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Tid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Content : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MsgType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SessionId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Receiver : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 185
    const-string v0, ""

    iput-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    .line 188
    :cond_1
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v1

    .line 189
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 192
    const/4 v0, 0x0

    .line 193
    sget-object v3, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    if-ne v1, v3, :cond_3

    .line 194
    :cond_2
    array-length v3, v2

    if-le v3, v4, :cond_3

    .line 195
    aget-object v0, v2, v4

    .line 201
    :cond_3
    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v1, v3, :cond_4

    .line 203
    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    .line 205
    invoke-static {v3}, Lcom/sec/chaton/settings/downloads/u;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 207
    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 212
    :cond_4
    iget-object v3, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v3, p1, v1, p2, v0}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/inner/Msg;Ljava/lang/String;)Lcom/sec/chaton/e/l;

    move-result-object v0

    .line 213
    sget-object v3, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    if-ne v0, v3, :cond_6

    .line 226
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 227
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;)V

    .line 232
    :cond_5
    iput-boolean v5, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    .line 240
    :cond_6
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v1, v0, :cond_7

    .line 252
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    if-eq v0, v5, :cond_7

    iget-boolean v0, p0, Lcom/sec/chaton/d/a/bk;->h:Z

    if-ne v0, v5, :cond_8

    .line 278
    :cond_7
    :goto_0
    return-void

    .line 273
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No need to update and insert: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 12

    .prologue
    .line 439
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_25

    .line 440
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;

    .line 444
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 445
    :cond_0
    const-string v0, "Error - HttpResultObject is null"

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    :cond_1
    :goto_0
    return-void

    .line 450
    :cond_2
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_3

    .line 451
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 452
    const-string v0, "unReadMessage.msg.size() is 0"

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 457
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 458
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 460
    const-string v1, ""

    .line 467
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/d/a/bl;

    invoke-direct {v3, p0}, Lcom/sec/chaton/d/a/bl;-><init>(Lcom/sec/chaton/d/a/bk;)V

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 493
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 496
    iget-object v4, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    .line 501
    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 502
    iget-object v4, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/n;->k(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 505
    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 507
    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v7, "ans"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v7, "noti"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 508
    new-instance v6, Lcom/sec/chaton/io/entry/inner/MsgTid;

    invoke-direct {v6}, Lcom/sec/chaton/io/entry/inner/MsgTid;-><init>()V

    .line 509
    iget-object v7, v1, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    iput-object v7, v6, Lcom/sec/chaton/io/entry/inner/MsgTid;->value:Ljava/lang/String;

    .line 510
    iget-object v7, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 514
    :cond_5
    iput-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    .line 518
    :cond_6
    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v6, "ans"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 519
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 523
    :cond_7
    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 527
    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    const-string v6, "0999"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 528
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    iget-object v6, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 534
    :cond_8
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 538
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 539
    const-string v1, "ch@t[c <~~ s] MsgID : "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 541
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ","

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 543
    :cond_a
    const-string v1, "API type : "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x6c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 544
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_b
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 568
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Msg;->isValid()Z

    move-result v2

    if-nez v2, :cond_d

    .line 569
    :cond_c
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_b

    .line 570
    const-string v1, "msg is invalid"

    const-string v2, "GetAllUnReadMessageTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 577
    :cond_d
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "media"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "msg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "noti"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 588
    :cond_e
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    iget-object v7, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-static {v2, v3, v4, v7}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 593
    const/4 v7, 0x1

    .line 597
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "noti"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 598
    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    .line 612
    :cond_f
    :goto_4
    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    .line 613
    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v4, :cond_10

    .line 614
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v4, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 615
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 616
    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    .line 619
    :cond_10
    iget-object v4, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    invoke-static {v4, v2, v3, v8}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 623
    const/4 v3, 0x1

    .line 624
    if-nez v4, :cond_11

    .line 628
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v4

    .line 631
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "can\'t find inbox. inboxNO:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is generated"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v9, "noti"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_18

    .line 637
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "( msg type noti )can\'t find inbox. inboxNO:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " is generated"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const/4 v3, 0x0

    .line 646
    :cond_11
    :goto_5
    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v8, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_16

    .line 647
    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    new-instance v9, Lcom/sec/chaton/d/a/bn;

    invoke-direct {v9, p0, v4, v7}, Lcom/sec/chaton/d/a/bn;-><init>(Lcom/sec/chaton/d/a/bk;Ljava/lang/String;Z)V

    invoke-virtual {v8, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "inbox:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is created in inboxMsgMap"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    if-eqz v3, :cond_16

    .line 654
    const/4 v3, 0x0

    .line 658
    iget-object v7, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v7, v4}, Lcom/sec/chaton/e/a/n;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 660
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 665
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v8

    const-string v9, "chaton_id"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_12

    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v9, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v8, v9, v4}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_12

    .line 668
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GetAllUnreadMessageTask - InboxNO :"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "Insert sender : "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    const/4 v3, 0x1

    .line 676
    :cond_12
    sget-object v8, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v8, :cond_19

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 701
    :cond_13
    :try_start_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_14

    .line 702
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const-string v8, "com.sec.chaton.provider"

    invoke-virtual {v2, v8, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 709
    :cond_14
    :goto_6
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    const/16 v7, 0xb

    invoke-static {v2, v4, v7}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    .line 710
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v2, v4}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 715
    :cond_15
    if-eqz v3, :cond_16

    .line 716
    const/4 v2, 0x0

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    const-wide/16 v7, 0x0

    invoke-static {v2, v4, v3, v7, v8}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    .line 725
    :cond_16
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "noti"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 726
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/d/a/bn;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/d/a/bn;->a(Lcom/sec/chaton/io/entry/inner/Msg;)V

    goto/16 :goto_3

    .line 600
    :cond_17
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    .line 603
    sget-object v3, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_f

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v8, ""

    invoke-virtual {v3, v4, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 604
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    goto/16 :goto_4

    .line 640
    :cond_18
    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    iget-object v9, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v8

    .line 641
    iget-object v9, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v9, v4, v2, v1, v8}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/io/entry/inner/Msg;Lcom/sec/chaton/e/w;)Landroid/net/Uri;

    goto/16 :goto_5

    .line 683
    :cond_19
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 684
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v8, v1, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    const-string v9, ","

    invoke-direct {v2, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    const-string v8, ""

    .line 686
    :cond_1a
    :goto_7
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 687
    iget-object v8, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 688
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v10, "chaton_id"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1a

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v10, "old_chaton_id"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1a

    .line 692
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GetAllUnreadMessageTask - InboxNO :"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "Insert receiver : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v3, v9}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    invoke-static {v4, v8}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 695
    const/4 v3, 0x1

    goto :goto_7

    .line 704
    :catch_0
    move-exception v2

    .line 705
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 736
    :cond_1b
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/chaton/d/a/bk;->g:Z

    .line 738
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/d/a/bn;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/d/a/bn;->a(Lcom/sec/chaton/io/entry/inner/Msg;)V

    .line 742
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MsgID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "msg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 744
    invoke-direct {p0, v4, v1}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V

    .line 752
    :cond_1c
    :goto_8
    invoke-direct {p0, v1, v4}, Lcom/sec/chaton/d/a/bk;->a(Lcom/sec/chaton/io/entry/inner/Msg;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 745
    :cond_1d
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "media"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 746
    invoke-direct {p0, v4, v1}, Lcom/sec/chaton/d/a/bk;->c(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V

    goto :goto_8

    .line 747
    :cond_1e
    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 748
    invoke-direct {p0, v4, v1}, Lcom/sec/chaton/d/a/bk;->b(Ljava/lang/String;Lcom/sec/chaton/io/entry/inner/Msg;)V

    goto :goto_8

    .line 757
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 758
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 759
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/d/a/bn;

    .line 769
    invoke-virtual {p0, v1}, Lcom/sec/chaton/d/a/bk;->a(Lcom/sec/chaton/d/a/bn;)V

    goto :goto_9

    .line 775
    :cond_20
    invoke-direct {p0, v5}, Lcom/sec/chaton/d/a/bk;->a(Ljava/util/List;)V

    .line 778
    const-wide/16 v1, 0x0

    .line 779
    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 780
    if-lez v3, :cond_21

    .line 781
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->msg:Ljava/util/ArrayList;

    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 782
    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 784
    :cond_21
    const-string v3, ""

    .line 785
    iget-object v4, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->nextstartkey:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 786
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetUnReadMessageList;->nextstartkey:Ljava/lang/String;

    .line 791
    :goto_a
    iget-object v3, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_22

    .line 792
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/d/a/bk;->c:Ljava/util/List;

    invoke-static {v3, v4, v1, v2, v0}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/util/List;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 794
    :cond_22
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_23

    .line 795
    const-string v3, "get_all_unread_message_timestamp"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 797
    :cond_23
    const-string v3, "get_all_unread_message_nextstartkey"

    invoke-static {v3, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_24

    .line 800
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_all_unread_message_timestamp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "get_all_unread_message_nextstartkey"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_24
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 806
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 838
    :cond_25
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 839
    const-string v0, "01000006"

    const-string v1, "1001"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    :cond_26
    move-object v0, v3

    goto/16 :goto_a
.end method

.method public a(Lcom/sec/chaton/d/a/bn;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 958
    invoke-virtual {p1}, Lcom/sec/chaton/d/a/bn;->a()Ljava/lang/String;

    move-result-object v3

    .line 959
    invoke-virtual {p1}, Lcom/sec/chaton/d/a/bn;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 964
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v4

    .line 969
    if-nez v4, :cond_0

    .line 977
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/d/a/bn;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1098
    :goto_0
    return-void

    .line 985
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 986
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Msg;

    .line 988
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<LastMsg> - MsgID : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Time : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    invoke-virtual {p1}, Lcom/sec/chaton/d/a/bn;->c()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0, v3, v2}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 997
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-wide v7, v4, Lcom/sec/chaton/e/a/p;->g:J

    cmp-long v2, v5, v7

    if-ltz v2, :cond_2

    .line 1001
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/sec/chaton/e/a/p;->g:J

    .line 1002
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/sec/chaton/e/a/p;->j:J

    .line 1003
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    iput-object v2, v4, Lcom/sec/chaton/e/a/p;->k:Ljava/lang/String;

    .line 1004
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->chattype:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v5

    .line 1005
    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v5, v2, :cond_4

    .line 1006
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    iput v2, v4, Lcom/sec/chaton/e/a/p;->c:I

    .line 1019
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1022
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "chaton_id"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1023
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ";"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1028
    :goto_2
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v2

    .line 1029
    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ";"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1035
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateWithLastMsg - (lastmsg) last msg sender : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    const-string v7, "0999"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v10, :cond_6

    .line 1038
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-direct {v2, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1039
    const-string v7, "push_message"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1040
    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1049
    :goto_3
    const-string v2, ";"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v5, v2, :cond_1

    .line 1056
    iget-object v2, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1061
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1064
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/e/a/p;->e:Ljava/lang/String;

    .line 1072
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/a/bk;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/bn;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/bn;->d()I

    move-result v0

    .line 1073
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 1096
    :cond_3
    :goto_4
    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->e:Landroid/content/ContentResolver;

    invoke-static {v1, v3, v4, v0}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/a/p;I)I

    .line 1097
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateInBox inboxNO:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/bk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1008
    :cond_4
    invoke-virtual {v5}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    iput v2, v4, Lcom/sec/chaton/e/a/p;->c:I

    .line 1009
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->address:Ljava/lang/String;

    iput-object v2, v4, Lcom/sec/chaton/e/a/p;->o:Ljava/lang/String;

    .line 1010
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->port:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v4, Lcom/sec/chaton/e/a/p;->p:I

    .line 1011
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    iput-object v2, v4, Lcom/sec/chaton/e/a/p;->i:Ljava/lang/String;

    goto/16 :goto_1

    .line 1025
    :cond_5
    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ";"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 1041
    :catch_0
    move-exception v2

    .line 1042
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 1045
    :cond_6
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Msg;->value:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1080
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/d/a/bn;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/sec/chaton/d/a/bk;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    move v0, v1

    goto :goto_4
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method
