.class public Lcom/sec/chaton/d/g;
.super Ljava/lang/Object;
.source "ChatONVMessageControl.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "uxNHas4pyn"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->a:Ljava/lang/String;

    .line 31
    const-string v0, "appId"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->b:Ljava/lang/String;

    .line 32
    const-string v0, "model"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->c:Ljava/lang/String;

    .line 33
    const-string v0, "osVersion"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->d:Ljava/lang/String;

    .line 34
    const-string v0, "build"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->e:Ljava/lang/String;

    .line 35
    const-string v0, "mcc"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->f:Ljava/lang/String;

    .line 36
    const-string v0, "mnc"

    iput-object v0, p0, Lcom/sec/chaton/d/g;->g:Ljava/lang/String;

    .line 47
    iput-object p1, p0, Lcom/sec/chaton/d/g;->h:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/sec/chaton/d/g;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/chaton/d/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/g;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/d/a/b;)Lcom/sec/chaton/d/a/z;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lcom/sec/chaton/j/j;

    sget-object v1, Lcom/sec/chaton/util/cg;->k:Lcom/sec/chaton/util/cg;

    const-string v2, "/api/application"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "application/json;charset=utf-8"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "appId"

    const-string v2, "uxNHas4pyn"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "model"

    invoke-static {}, Lcom/sec/chaton/util/am;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "osVersion"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "build"

    sget-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$ChatONVApplicatoinInfoParser;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->b(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x44d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 66
    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    const-string v1, "mcc"

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "mnc"

    invoke-static {}, Lcom/sec/chaton/util/am;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    .line 72
    :cond_0
    new-instance v1, Lcom/sec/chaton/d/a/z;

    iget-object v2, p0, Lcom/sec/chaton/d/g;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v1, v2, v0, p1}, Lcom/sec/chaton/d/a/z;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 73
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->c()Lcom/sec/chaton/j/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 75
    return-object v1
.end method
