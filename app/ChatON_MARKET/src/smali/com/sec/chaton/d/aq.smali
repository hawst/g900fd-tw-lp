.class public Lcom/sec/chaton/d/aq;
.super Ljava/lang/Object;
.source "RequestGenerator.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/d/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/d/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/aq;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/d/aq;
    .locals 2

    .prologue
    .line 37
    const-class v1, Lcom/sec/chaton/d/aq;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/d/aq;->b:Lcom/sec/chaton/d/aq;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/chaton/d/aq;

    invoke-direct {v0}, Lcom/sec/chaton/d/aq;-><init>()V

    sput-object v0, Lcom/sec/chaton/d/aq;->b:Lcom/sec/chaton/d/aq;

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/aq;->b:Lcom/sec/chaton/d/aq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(JLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/a;)Lcom/sec/chaton/j/ao;
    .locals 6

    .prologue
    .line 210
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 212
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p5, v0

    .line 218
    :cond_0
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 219
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->c()Ljava/lang/String;

    move-result-object v0

    .line 222
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object p6, v0

    .line 227
    :cond_1
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 228
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;)V

    .line 233
    :cond_2
    if-nez p5, :cond_3

    .line 234
    const/4 v0, 0x0

    .line 317
    :goto_0
    return-object v0

    .line 238
    :cond_3
    invoke-static {}, Lcom/sec/chaton/a/ep;->newBuilder()Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/a/eq;->a(J)Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-virtual {p4}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eq;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sec/chaton/a/eq;->a(Ljava/lang/String;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eq;->d(Ljava/lang/String;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eq;->e(Ljava/lang/String;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eq;->b(Ljava/lang/String;)Lcom/sec/chaton/a/eq;

    move-result-object v1

    .line 250
    invoke-virtual {p7}, Lcom/sec/chaton/msgsend/a;->b()Ljava/util/List;

    move-result-object v0

    .line 251
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/b;

    .line 252
    invoke-static {}, Lcom/sec/chaton/a/es;->newBuilder()Lcom/sec/chaton/a/et;

    move-result-object v3

    .line 253
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/a/et;->a(J)Lcom/sec/chaton/a/et;

    .line 254
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->b()Lcom/sec/chaton/e/w;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/et;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/et;

    .line 255
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/chaton/a/et;->a(Ljava/lang/String;)Lcom/sec/chaton/a/et;

    .line 256
    invoke-virtual {v1, v3}, Lcom/sec/chaton/a/eq;->a(Lcom/sec/chaton/a/et;)Lcom/sec/chaton/a/eq;

    goto :goto_1

    .line 261
    :cond_4
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 262
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v2

    .line 263
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    .line 264
    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/eq;->b(J)Lcom/sec/chaton/a/eq;

    .line 269
    :cond_5
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 270
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    .line 272
    :cond_6
    invoke-virtual {v1, p6}, Lcom/sec/chaton/a/eq;->c(Ljava/lang/String;)Lcom/sec/chaton/a/eq;

    .line 274
    invoke-virtual {v1}, Lcom/sec/chaton/a/eq;->d()Lcom/sec/chaton/a/ep;

    move-result-object v0

    .line 275
    sget-object v1, Lcom/sec/chaton/d/aq;->a:Ljava/lang/String;

    invoke-static {p3, v0, v1}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/ep;Ljava/lang/String;)V

    .line 312
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->d()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;
    .locals 5

    .prologue
    .line 137
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p6, v0

    .line 146
    :cond_0
    if-nez p6, :cond_1

    .line 147
    const/4 v0, 0x0

    .line 205
    :goto_0
    return-object v0

    .line 151
    :cond_1
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 152
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->c()Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move-object p8, v0

    .line 160
    :cond_2
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 161
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;)V

    .line 165
    :cond_3
    invoke-static {}, Lcom/sec/chaton/a/az;->newBuilder()Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/a/ba;->a(J)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-virtual {p5}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ba;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sec/chaton/a/ba;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ba;->d(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ba;->e(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ba;->b(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/sec/chaton/a/ba;->f(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    move-result-object v0

    .line 168
    invoke-static {p4}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/dp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ba;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/ba;

    .line 172
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 173
    invoke-static {p3}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v1

    .line 174
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_4

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/a/ba;->b(J)Lcom/sec/chaton/a/ba;

    .line 180
    :cond_4
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 181
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 183
    :cond_5
    invoke-virtual {v0, p8}, Lcom/sec/chaton/a/ba;->c(Ljava/lang/String;)Lcom/sec/chaton/a/ba;

    .line 186
    invoke-virtual {v0}, Lcom/sec/chaton/a/ba;->d()Lcom/sec/chaton/a/az;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/sec/chaton/d/aq;->a:Ljava/lang/String;

    invoke-static {p3, v0, v1}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/az;Ljava/lang/String;)V

    .line 199
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->d()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/sec/chaton/j/ao;
    .locals 6

    .prologue
    .line 83
    invoke-static {}, Lcom/sec/chaton/a/i;->newBuilder()Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lcom/sec/chaton/a/j;->a(J)Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->b(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->d(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->e(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->a(I)Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->c(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    move-result-object v1

    .line 89
    invoke-static {p2}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    invoke-static {p2}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object p6, v0

    .line 97
    :cond_0
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq p1, v0, :cond_1

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    :cond_1
    const-string v0, ""

    invoke-virtual {v1, v0}, Lcom/sec/chaton/a/j;->a(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 105
    :goto_0
    invoke-static {p2}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 106
    invoke-static {p2}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->e()J

    move-result-wide v2

    .line 107
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 108
    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/a/j;->c(J)Lcom/sec/chaton/a/j;

    .line 124
    :cond_2
    invoke-virtual {v1}, Lcom/sec/chaton/a/j;->d()Lcom/sec/chaton/a/i;

    move-result-object v0

    .line 125
    sget-object v1, Lcom/sec/chaton/d/aq;->a:Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/i;Ljava/lang/String;)V

    .line 128
    new-instance v1, Lcom/sec/chaton/j/ap;

    invoke-direct {v1}, Lcom/sec/chaton/j/ap;-><init>()V

    .line 129
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    .line 132
    invoke-virtual {v1}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0

    .line 100
    :cond_3
    invoke-virtual {v1, p6}, Lcom/sec/chaton/a/j;->a(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/ao;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-static {}, Lcom/sec/chaton/a/da;->newBuilder()Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/a/db;->a(J)Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-virtual {p5}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/db;->a(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/db;->b(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/db;->c(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/sec/chaton/a/db;->e(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    move-result-object v2

    .line 54
    invoke-static {p4}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/db;

    .line 56
    array-length v3, p6

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p6, v0

    .line 57
    invoke-virtual {v2, v4}, Lcom/sec/chaton/a/db;->d(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    invoke-virtual {v2}, Lcom/sec/chaton/a/db;->d()Lcom/sec/chaton/a/da;

    move-result-object v0

    .line 67
    sget-object v2, Lcom/sec/chaton/d/aq;->a:Ljava/lang/String;

    invoke-static {p1, v0, v2}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/da;Ljava/lang/String;)V

    .line 69
    new-instance v2, Lcom/sec/chaton/j/ap;

    invoke-direct {v2}, Lcom/sec/chaton/j/ap;-><init>()V

    invoke-virtual {v2, v1}, Lcom/sec/chaton/j/ap;->a(I)Lcom/sec/chaton/j/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/j/ap;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/j/ap;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/c/g;->e()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/ap;->a(J)Lcom/sec/chaton/j/ap;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/sec/chaton/j/ap;->b()Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
