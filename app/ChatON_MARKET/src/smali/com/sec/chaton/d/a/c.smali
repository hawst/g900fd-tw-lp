.class public abstract Lcom/sec/chaton/d/a/c;
.super Ljava/lang/Object;
.source "AbstractNetTask.java"


# static fields
.field protected static a:I


# instance fields
.field protected b:Landroid/os/Handler;

.field protected c:Lcom/sec/chaton/j/ak;

.field protected d:I

.field protected e:I

.field protected f:J

.field protected g:Lcom/sec/chaton/d/o;

.field private h:Lcom/sec/chaton/j/ao;

.field private i:Lcom/sec/chaton/d/a/dj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/d/a/c;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {}, Lcom/sec/chaton/d/a/di;->a()Lcom/sec/chaton/d/a/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/di;->b()Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/dj;

    iput-object v0, p0, Lcom/sec/chaton/d/a/c;->i:Lcom/sec/chaton/d/a/dj;

    .line 44
    iput-object p1, p0, Lcom/sec/chaton/d/a/c;->b:Landroid/os/Handler;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/d/a/c;->d:I

    .line 46
    sget v0, Lcom/sec/chaton/d/a/c;->a:I

    iput v0, p0, Lcom/sec/chaton/d/a/c;->e:I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/c;->g:Lcom/sec/chaton/d/o;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/sec/chaton/d/a/di;->a()Lcom/sec/chaton/d/a/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/di;->b()Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/dj;

    iput-object v0, p0, Lcom/sec/chaton/d/a/c;->i:Lcom/sec/chaton/d/a/dj;

    .line 52
    iput-object p1, p0, Lcom/sec/chaton/d/a/c;->b:Landroid/os/Handler;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/d/a/c;->d:I

    .line 54
    iput p2, p0, Lcom/sec/chaton/d/a/c;->e:I

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/c;->g:Lcom/sec/chaton/d/o;

    .line 56
    return-void
.end method


# virtual methods
.method protected abstract a(ILjava/lang/Object;Ljava/lang/Object;)V
.end method

.method protected a(Lcom/sec/chaton/d/a/c;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 89
    iget v0, p0, Lcom/sec/chaton/d/a/c;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/d/a/c;->d:I

    .line 94
    const/4 v3, 0x1

    .line 97
    :try_start_0
    const-string v0, "onPreExecute - start"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/c;->b()Lcom/sec/chaton/j/ao;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 99
    :try_start_1
    const-string v1, "onPreExecute - end"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 113
    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, v3, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 152
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 101
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 102
    const/16 v1, 0x2b

    .line 113
    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, v1, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 104
    :goto_2
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 105
    const/16 v1, 0x2a

    .line 113
    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, v1, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 107
    :goto_3
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 108
    const/16 v1, 0x29

    .line 113
    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, v1, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :catch_3
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 110
    :goto_4
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 111
    const/4 v1, 0x3

    .line 113
    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, v1, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    if-nez v1, :cond_0

    .line 114
    invoke-virtual {p0, v3, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_0
    throw v0

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/d/a/c;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v1}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v1

    if-nez v1, :cond_2

    .line 122
    const-string v1, ""

    .line 123
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 125
    invoke-static {v1}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v3

    .line 126
    invoke-static {v1}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v1

    .line 127
    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 129
    :try_start_6
    iget-object v4, p0, Lcom/sec/chaton/d/a/c;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v4, v3, v1}, Lcom/sec/chaton/j/ak;->a([B[B)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 137
    :cond_2
    :goto_6
    iget-object v1, p0, Lcom/sec/chaton/d/a/c;->c:Lcom/sec/chaton/j/ak;

    invoke-virtual {v1}, Lcom/sec/chaton/j/ak;->f()Lcom/sec/chaton/util/a;

    move-result-object v1

    if-nez v1, :cond_3

    .line 140
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/chaton/d/a/c;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 130
    :catch_4
    move-exception v1

    .line 131
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, "AesCipher"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 146
    :cond_3
    iput-object v0, p0, Lcom/sec/chaton/d/a/c;->h:Lcom/sec/chaton/j/ao;

    .line 147
    new-instance v0, Lcom/sec/chaton/d/a/d;

    iget-object v1, p0, Lcom/sec/chaton/d/a/c;->h:Lcom/sec/chaton/j/ao;

    iget-object v2, p0, Lcom/sec/chaton/d/a/c;->c:Lcom/sec/chaton/j/ak;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/chaton/d/a/d;-><init>(Lcom/sec/chaton/d/a/c;Lcom/sec/chaton/j/ao;Lcom/sec/chaton/j/ak;)V

    .line 149
    new-instance v1, Lcom/sec/chaton/j/q;

    iget-object v2, p0, Lcom/sec/chaton/d/a/c;->i:Lcom/sec/chaton/d/a/dj;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/j/q;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/t;)V

    .line 150
    iget-object v2, p0, Lcom/sec/chaton/d/a/c;->h:Lcom/sec/chaton/j/ao;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/j/ao;->a(Lcom/sec/chaton/j/q;)V

    .line 151
    invoke-virtual {v0}, Lcom/sec/chaton/d/a/d;->a()V

    goto/16 :goto_0

    .line 113
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_5

    .line 109
    :catch_5
    move-exception v1

    goto :goto_4

    .line 106
    :catch_6
    move-exception v1

    goto/16 :goto_3

    .line 103
    :catch_7
    move-exception v1

    goto/16 :goto_2

    .line 100
    :catch_8
    move-exception v1

    goto/16 :goto_1
.end method

.method public a(Lcom/sec/chaton/j/ak;)V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 156
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 157
    iput-object p1, p0, Lcom/sec/chaton/d/a/c;->c:Lcom/sec/chaton/j/ak;

    .line 158
    iget-object v1, p0, Lcom/sec/chaton/d/a/c;->i:Lcom/sec/chaton/d/a/dj;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/a/dj;->sendMessage(Landroid/os/Message;)Z

    .line 159
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/chaton/d/a/c;->d:I

    iget v1, p0, Lcom/sec/chaton/d/a/c;->e:I

    if-lt v0, v1, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract b()Lcom/sec/chaton/j/ao;
.end method
