.class public Lcom/sec/chaton/d/a/ak;
.super Lcom/sec/chaton/d/a/a;
.source "DeRegistrationTask.java"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 62
    return-void
.end method

.method public static a(Z)I
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 176
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    if-nez p0, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 369
    :goto_0
    return v0

    .line 183
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    .line 185
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 189
    const-string v0, "com.sec.chaton"

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v0, v6

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 190
    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v4, v5, v8}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    :cond_1
    invoke-static {v7}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/a;->a()V

    .line 198
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 214
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "account_type=\'com.sec.chaton\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 215
    if-eqz v2, :cond_9

    .line 216
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 217
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 220
    :goto_2
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "account_type=\'com.sec.chaton\'"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 221
    if-eq v1, v0, :cond_2

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to delete contacts contract"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_2
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->d()V

    .line 229
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->e()V

    .line 232
    invoke-static {}, Lcom/sec/chaton/j/af;->c()Z

    .line 236
    invoke-static {}, Lcom/sec/chaton/d/o;->g()V

    .line 240
    invoke-static {v7}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->close()V

    .line 244
    invoke-static {}, Lcom/sec/chaton/e/aw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 245
    invoke-static {}, Lcom/sec/chaton/e/aw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 248
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/aw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_3
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/database/a/b;->close()V

    .line 258
    const-string v0, "trunk.db"

    invoke-virtual {v7, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 259
    const-string v0, "trunk.db"

    invoke-virtual {v7, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 261
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 262
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_4

    .line 264
    const-string v0, "Failed to delete trunk.db"

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_4
    invoke-static {}, Lcom/sec/chaton/localbackup/database/b;->a()Lcom/sec/chaton/localbackup/database/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/database/b;->close()V

    .line 272
    const-string v0, "chaton_local_backup.db"

    invoke-virtual {v7, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 273
    const-string v0, "chaton_local_backup.db"

    invoke-virtual {v7, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 276
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_5

    .line 277
    const-string v0, "Failed to delete chaton_local_backup.db"

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 290
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 291
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/io/File;)V

    .line 293
    invoke-virtual {v7}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/io/File;)V

    .line 307
    :goto_3
    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/io/File;)V

    .line 317
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->a(Landroid/content/Context;)V

    .line 318
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Landroid/content/Context;)V

    .line 319
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->a(Landroid/content/Context;)V

    .line 323
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->c()V

    .line 327
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PASSWORD_LOCK"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 328
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 329
    const-string v1, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 330
    const-string v1, "PASSWORD"

    const-string v2, "0000"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 331
    const-string v1, "PASSWORD_HINT"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 332
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 337
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->a()V

    .line 338
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/ab;->a()V

    .line 339
    invoke-static {}, Lcom/sec/chaton/util/aa;->b()V

    .line 341
    const-string v0, "BUDDY_ACTIVITY"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 342
    const-string v0, "chaton_pref_without_encrypt"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 345
    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 359
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->b()Lcom/sec/common/f/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/f/b;->b()V

    .line 361
    const-string v0, "Success to cleat data"

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 362
    goto/16 :goto_0

    .line 295
    :cond_6
    const-string v0, "Failed to delete multimedia"

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 363
    :catch_0
    move-exception v0

    .line 364
    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 366
    if-eqz p0, :cond_8

    move v0, v6

    .line 367
    goto/16 :goto_0

    .line 299
    :cond_7
    :try_start_1
    const-string v0, "Failed to delete multimedia cause storage Not enough"

    const-string v1, "DeRegistrationTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 369
    :cond_8
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_9
    move v1, v6

    goto/16 :goto_2
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 72
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 158
    invoke-static {v0}, Lcom/sec/chaton/d/a/ak;->a(Z)I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/chaton/d/a/ak;->b:Z

    .line 165
    :cond_0
    :goto_1
    return-void

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 161
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 162
    const-string v0, "01000002"

    const-string v1, "0102"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/sec/chaton/d/a/ak;->b:Z

    return v0
.end method
