.class public Lcom/sec/chaton/d/a/i;
.super Lcom/sec/chaton/d/a/s;
.source "AllowChatTask.java"


# instance fields
.field h:J

.field i:Lcom/sec/chaton/e/r;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 37
    sget-object v8, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/d/a/i;-><init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/msgsend/k;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p8}, Lcom/sec/chaton/d/a/s;-><init>(Landroid/os/Handler;Lcom/sec/chaton/msgsend/k;)V

    .line 42
    iput-object p2, p0, Lcom/sec/chaton/d/a/i;->i:Lcom/sec/chaton/e/r;

    .line 43
    iput-object p4, p0, Lcom/sec/chaton/d/a/i;->k:Ljava/lang/String;

    .line 44
    iput-wide p5, p0, Lcom/sec/chaton/d/a/i;->h:J

    .line 45
    iput-object p7, p0, Lcom/sec/chaton/d/a/i;->j:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/s;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPostExecute()"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 63
    const/4 v0, 0x2

    iput v0, v1, Landroid/os/Message;->what:I

    .line 65
    if-eqz p3, :cond_7

    .line 66
    check-cast p3, Lcom/sec/chaton/j/ao;

    .line 67
    invoke-virtual {p3}, Lcom/sec/chaton/j/ao;->c()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/f;

    .line 69
    iget-object v2, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/f;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/ej;->d()I

    move-result v2

    .line 81
    const/16 v3, 0x3e8

    if-eq v2, v3, :cond_0

    const/16 v3, 0xbbb

    if-ne v2, v3, :cond_1

    .line 83
    :cond_0
    new-instance v0, Lcom/sec/chaton/a/a/k;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-direct {v0, v3, v2, v4}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/i;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 127
    :goto_1
    return-void

    .line 87
    :cond_1
    const/16 v3, 0x7d6

    if-eq v2, v3, :cond_2

    const/16 v3, 0xbc0

    if-ne v2, v3, :cond_4

    .line 88
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-static {v0, v3, v4, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    .line 98
    :cond_3
    :goto_2
    new-instance v0, Lcom/sec/chaton/a/a/k;

    iget-object v3, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-direct {v0, v7, v2, v3}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 89
    :cond_4
    const/16 v3, 0xbbe

    if-eq v2, v3, :cond_3

    .line 91
    const/16 v3, 0x1b59

    if-ne v2, v3, :cond_5

    .line 92
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/d/a/i;->k:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_2

    .line 93
    :cond_5
    const/16 v3, 0x1b5a

    if-ne v2, v3, :cond_6

    .line 94
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/i;->b:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/chaton/chat/fd;->a(Landroid/os/Handler;Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_2

    .line 95
    :cond_6
    const/16 v3, 0x1b5b

    if-ne v2, v3, :cond_3

    .line 96
    invoke-static {}, Lcom/sec/chaton/chat/fd;->a()Lcom/sec/chaton/chat/fd;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/chaton/chat/fd;->a(Ljava/lang/String;Lcom/sec/chaton/a/ej;)V

    goto :goto_2

    .line 105
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/i;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xb

    if-eq p1, v0, :cond_8

    const/16 v0, 0x16

    if-eq p1, v0, :cond_8

    const/4 v0, 0x3

    if-ne p1, v0, :cond_a

    .line 107
    :cond_8
    const/16 v0, 0x18

    if-eq p1, v0, :cond_9

    const/16 v0, 0x17

    if-eq p1, v0, :cond_9

    const/16 v0, 0x15

    if-ne p1, v0, :cond_a

    .line 110
    :cond_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/n;->e(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 115
    :cond_a
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_b

    .line 116
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(I)Lcom/sec/chaton/a/ek;

    move-result-object v0

    const-string v2, "Fail to send allowChat message"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/ek;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 117
    invoke-static {}, Lcom/sec/chaton/a/f;->newBuilder()Lcom/sec/chaton/a/g;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/chaton/d/a/i;->h:J

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/g;->a(J)Lcom/sec/chaton/a/g;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/g;

    move-result-object v0

    .line 118
    iget-object v2, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/a/g;->d()Lcom/sec/chaton/a/f;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Lcom/sec/chaton/a/f;Ljava/lang/String;)V

    .line 123
    :cond_b
    new-instance v0, Lcom/sec/chaton/a/a/k;

    iget-object v2, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    invoke-direct {v0, v7, p1, v2}, Lcom/sec/chaton/a/a/k;-><init>(ZILjava/lang/String;)V

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/d/a/i;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method

.method protected b()Lcom/sec/chaton/j/ao;
    .locals 7

    .prologue
    .line 51
    invoke-super {p0}, Lcom/sec/chaton/d/a/s;->b()Lcom/sec/chaton/j/ao;

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    invoke-static {}, Lcom/sec/chaton/d/aq;->a()Lcom/sec/chaton/d/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/i;->i:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/d/a/i;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/d/a/i;->k:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/chaton/d/a/i;->h:J

    iget-object v6, p0, Lcom/sec/chaton/d/a/i;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/aq;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/sec/chaton/j/ao;

    move-result-object v0

    return-object v0
.end method
