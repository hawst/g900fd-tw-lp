.class public Lcom/sec/chaton/d/au;
.super Ljava/lang/Object;
.source "SnsAccountControl.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/chaton/settings/tellfriends/aa;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;"
    }
.end annotation


# instance fields
.field a:Lcom/sec/chaton/d/h;

.field b:Lcom/sec/chaton/d/ap;

.field c:Lcom/sec/chaton/d/w;

.field d:Lcom/sec/chaton/d/bb;

.field private final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private f:Landroid/app/Activity;

.field private g:Lcom/sec/chaton/settings/tellfriends/aa;

.field private final h:Landroid/os/Handler;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:[J

.field private m:Lcom/sec/chaton/settings/tellfriends/ad;

.field private n:Lcom/sec/chaton/settings/tellfriends/ac;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->i:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->j:Ljava/util/ArrayList;

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/au;->l:[J

    .line 393
    new-instance v0, Lcom/sec/chaton/d/aw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/aw;-><init>(Lcom/sec/chaton/d/au;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->m:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 430
    new-instance v0, Lcom/sec/chaton/d/ax;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d/ax;-><init>(Lcom/sec/chaton/d/au;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->n:Lcom/sec/chaton/settings/tellfriends/ac;

    .line 90
    iput-object p1, p0, Lcom/sec/chaton/d/au;->f:Landroid/app/Activity;

    .line 91
    iput-object p2, p0, Lcom/sec/chaton/d/au;->e:Ljava/lang/Class;

    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->d()Lcom/sec/chaton/settings/tellfriends/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    .line 94
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/au;->k:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/sec/chaton/d/az;->a:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/d/au;)Lcom/sec/chaton/settings/tellfriends/aa;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/chaton/d/au;->d:Lcom/sec/chaton/d/bb;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/d/au;->d:Lcom/sec/chaton/d/bb;

    invoke-static {}, Lcom/sec/chaton/d/az;->a()Lcom/sec/chaton/d/az;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/sec/chaton/d/bb;->a(ILcom/sec/chaton/d/az;)V

    .line 374
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/d/au;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/chaton/d/au;->a(I)V

    return-void
.end method

.method private a(Lcom/sec/chaton/a/a/f;)Z
    .locals 2

    .prologue
    .line 235
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/sec/chaton/d/au;->c()V

    .line 240
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/d/au;->c()V

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/a/a/m;)Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p1, Lcom/sec/chaton/a/a/m;->a:Z

    if-eqz v0, :cond_0

    .line 245
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->i()V

    .line 249
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/d/au;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/d/au;[J)[J
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/chaton/d/au;->l:[J

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/d/au;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->l()V

    return-void
.end method

.method private b(Lcom/sec/chaton/a/a/f;)Z
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 254
    const/16 v0, 0x515

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(I)V

    .line 259
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 256
    :cond_0
    const/16 v0, 0x516

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/d/au;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/d/au;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Lcom/sec/chaton/a/a/f;)Z
    .locals 4

    .prologue
    const/16 v3, 0x515

    .line 263
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_3

    .line 264
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 265
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetSnsFriendsList;

    .line 266
    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetSnsFriendsList;->sns:Ljava/util/ArrayList;

    .line 268
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/d/au;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 270
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 271
    iget-object v3, p0, Lcom/sec/chaton/d/au;->j:Ljava/util/ArrayList;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;->userid:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 273
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->m()V

    .line 286
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 277
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_1

    .line 281
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_1

    .line 284
    :cond_3
    const/16 v0, 0x516

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_1
.end method

.method private d()Lcom/sec/chaton/settings/tellfriends/aa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/chaton/d/au;->e:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/aj;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aj;-><init>(Landroid/app/Activity;)V

    .line 115
    :goto_0
    return-object v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->e:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/au;->e:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/au;->e:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/al;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/al;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 115
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/d/au;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/d/au;->k:Ljava/lang/String;

    return-object v0
.end method

.method private d(Lcom/sec/chaton/a/a/f;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 290
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 291
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/y;->a(Landroid/content/Context;)Lcom/sec/chaton/settings/tellfriends/y;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/au;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/y;->a(Ljava/lang/String;Z)V

    .line 292
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->k()V

    .line 296
    :goto_0
    return v2

    .line 294
    :cond_0
    const/16 v0, 0x516

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_0
.end method

.method private e()Lcom/sec/chaton/d/ba;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/aj;

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lcom/sec/chaton/d/ba;->b:Lcom/sec/chaton/d/ba;

    .line 129
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/an;

    if-eqz v0, :cond_1

    .line 122
    sget-object v0, Lcom/sec/chaton/d/ba;->c:Lcom/sec/chaton/d/ba;

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/ap;

    if-eqz v0, :cond_2

    .line 124
    sget-object v0, Lcom/sec/chaton/d/ba;->d:Lcom/sec/chaton/d/ba;

    goto :goto_0

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_3

    .line 126
    sget-object v0, Lcom/sec/chaton/d/ba;->e:Lcom/sec/chaton/d/ba;

    goto :goto_0

    .line 129
    :cond_3
    sget-object v0, Lcom/sec/chaton/d/ba;->a:Lcom/sec/chaton/d/ba;

    goto :goto_0
.end method

.method private e(Lcom/sec/chaton/a/a/f;)Z
    .locals 3

    .prologue
    .line 300
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_3

    .line 301
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;

    .line 303
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->uid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->chatonid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 304
    :cond_0
    const/16 v1, 0x517

    invoke-direct {p0, v1}, Lcom/sec/chaton/d/au;->a(I)V

    .line 305
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->uid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ChatONID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->chatonid:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 309
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->h()V

    goto :goto_0

    .line 312
    :cond_3
    const/16 v0, 0x516

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(I)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/d/au;)[J
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->n()[J

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/aj;

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->a:Ljava/lang/String;

    .line 143
    :goto_0
    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/an;

    if-eqz v0, :cond_1

    .line 136
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->b:Ljava/lang/String;

    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/ap;

    if-eqz v0, :cond_2

    .line 138
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->c:Ljava/lang/String;

    goto :goto_0

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_3

    .line 140
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->d:Ljava/lang/String;

    goto :goto_0

    .line 143
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/d/au;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->j()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aa;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    sget-object v0, Lcom/sec/chaton/d/az;->b:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    new-instance v1, Lcom/sec/chaton/d/av;

    invoke-direct {v1, p0}, Lcom/sec/chaton/d/av;-><init>(Lcom/sec/chaton/d/au;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    .line 175
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/d/au;)[J
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/d/au;->l:[J

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    .line 323
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/az;->g:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->b(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->c:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->c()V

    .line 333
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    if-nez v0, :cond_0

    .line 337
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/aa;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    if-nez v0, :cond_0

    .line 345
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    .line 348
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/az;->e:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/d/au;->l:[J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;[J)V

    .line 350
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/chaton/d/au;->b:Lcom/sec/chaton/d/ap;

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/d/au;->b:Lcom/sec/chaton/d/ap;

    .line 357
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/az;->b:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/d/au;->b:Lcom/sec/chaton/d/ap;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    .line 366
    :cond_0
    sget-object v0, Lcom/sec/chaton/d/az;->f:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/d/au;->a:Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->j:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/util/ArrayList;Z)V

    .line 368
    return-void
.end method

.method private n()[J
    .locals 5

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/chaton/d/au;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 378
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/d/au;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    aput-wide v3, v2, v1

    .line 378
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 381
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->g()V

    .line 148
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 385
    packed-switch p1, :pswitch_data_0

    .line 391
    :goto_0
    return-void

    .line 387
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/aa;->a(IILandroid/content/Intent;)V

    .line 388
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->l()V

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x7f99
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/chaton/d/bb;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/chaton/d/au;->d:Lcom/sec/chaton/d/bb;

    .line 100
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lcom/sec/chaton/d/ay;->a:[I

    invoke-static {}, Lcom/sec/chaton/d/az;->a()Lcom/sec/chaton/d/az;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/az;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    return-void

    .line 180
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/d/au;->a()V

    goto :goto_0

    .line 183
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->g()V

    goto :goto_0

    .line 186
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->l()V

    goto :goto_0

    .line 189
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/d/au;->c()V

    goto :goto_0

    .line 192
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->k()V

    goto :goto_0

    .line 195
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/d/au;->m()V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 201
    sget-object v0, Lcom/sec/chaton/d/az;->d:Lcom/sec/chaton/d/az;

    invoke-static {v0}, Lcom/sec/chaton/d/az;->a(Lcom/sec/chaton/d/az;)V

    .line 202
    sget-object v0, Lcom/sec/chaton/d/ay;->b:[I

    invoke-direct {p0}, Lcom/sec/chaton/d/au;->e()Lcom/sec/chaton/d/ba;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/ba;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 212
    :goto_0
    return-void

    .line 205
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->m:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/d/au;->g:Lcom/sec/chaton/settings/tellfriends/aa;

    iget-object v1, p0, Lcom/sec/chaton/d/au;->n:Lcom/sec/chaton/settings/tellfriends/ac;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Lcom/sec/chaton/settings/tellfriends/ac;)V

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 216
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 231
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 218
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->e(Lcom/sec/chaton/a/a/f;)Z

    move-result v0

    goto :goto_0

    .line 220
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/m;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/a/a/m;)Z

    move-result v0

    goto :goto_0

    .line 222
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/a/a/f;)Z

    move-result v0

    goto :goto_0

    .line 224
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->d(Lcom/sec/chaton/a/a/f;)Z

    move-result v0

    goto :goto_0

    .line 226
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->c(Lcom/sec/chaton/a/a/f;)Z

    move-result v0

    goto :goto_0

    .line 228
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {p0, v0}, Lcom/sec/chaton/d/au;->b(Lcom/sec/chaton/a/a/f;)Z

    move-result v0

    goto :goto_0

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        0xcc -> :sswitch_0
        0x137 -> :sswitch_5
        0x139 -> :sswitch_3
        0x13a -> :sswitch_4
        0x192 -> :sswitch_1
        0x194 -> :sswitch_2
    .end sparse-switch
.end method
