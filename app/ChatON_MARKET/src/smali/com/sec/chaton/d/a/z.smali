.class public Lcom/sec/chaton/d/a/z;
.super Lcom/sec/chaton/d/a/a;
.source "ChatONVApplicationInfoTask.java"


# static fields
.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/d/a/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/d/a/z;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected a(Lcom/sec/chaton/a/a/f;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_3

    .line 43
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->p()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;

    .line 45
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 46
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "install.enable: "

    aput-object v2, v1, v4

    iget-object v2, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->enable:Ljava/lang/Boolean;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "\nvideoCallMax: "

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->config:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;->videoCallMax:Ljava/lang/Integer;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "\nvoiceCallMax: "

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->config:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;->voiceCallMax:Ljava/lang/Integer;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/d/a/z;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chatonv_available"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->enable:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 53
    iget-object v1, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->enable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 54
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "install.urlList[0]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ninstall.urlList[1]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/d/a/z;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chatonv_apps_down_url"

    iget-object v1, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chatonv_google_down_url"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_2
    :goto_0
    return-void

    .line 64
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 65
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, "ChatON V application info task has network error."

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/z;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected synthetic a(Lcom/sec/common/d/a/e;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/sec/chaton/a/a/f;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/z;->b(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method protected b(Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/z;->a(Lcom/sec/chaton/a/a/f;)V

    .line 38
    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method
