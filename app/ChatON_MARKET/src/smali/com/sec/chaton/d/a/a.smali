.class public abstract Lcom/sec/chaton/d/a/a;
.super Lcom/sec/common/d/a/a/a;
.source "AbstractHttpTask2.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/d/a/a/a",
        "<",
        "Lcom/sec/chaton/j/h;",
        "Lcom/sec/chaton/a/a/f;",
        ">;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/chaton/d/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lcom/sec/chaton/j/h;

.field private b:I

.field private c:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/chaton/d/a/b;->c:Lcom/sec/chaton/d/a/b;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p2}, Lcom/sec/common/d/a/a/a;-><init>(Lcom/sec/common/d/a/a/b;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/d/a/a;->a:Lcom/sec/chaton/j/h;

    .line 57
    iput-object p2, p0, Lcom/sec/chaton/d/a/a;->a:Lcom/sec/chaton/j/h;

    .line 58
    invoke-virtual {p3}, Lcom/sec/chaton/d/a/b;->a()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/d/a/a;->b:I

    .line 60
    invoke-virtual {p2, p1}, Lcom/sec/chaton/j/h;->a(Landroid/os/Handler;)V

    .line 61
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/chaton/d/a/a;->b:I

    return v0
.end method

.method public a(Lcom/sec/chaton/d/a/a;)I
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 130
    iget v0, p0, Lcom/sec/chaton/d/a/a;->b:I

    invoke-virtual {p1}, Lcom/sec/chaton/d/a/a;->a()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 132
    cmp-long v2, v0, v3

    if-gez v2, :cond_0

    .line 133
    const/4 v0, -0x1

    .line 137
    :goto_0
    return v0

    .line 134
    :cond_0
    cmp-long v0, v0, v3

    if-nez v0, :cond_1

    .line 135
    const/4 v0, 0x0

    goto :goto_0

    .line 137
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/j/h;)Lcom/sec/chaton/a/a/f;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/sec/chaton/a/a/f;

    invoke-direct {v0, p1}, Lcom/sec/chaton/a/a/f;-><init>(Lcom/sec/common/d/a/c;)V

    return-object v0
.end method

.method public bridge synthetic a(Lcom/sec/common/d/a/c;)Lcom/sec/common/d/a/e;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lcom/sec/chaton/j/h;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/a;->a(Lcom/sec/chaton/j/h;)Lcom/sec/chaton/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lcom/sec/chaton/a/a/f;)V
.end method

.method protected synthetic a(Lcom/sec/common/d/a/e;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/sec/chaton/a/a/f;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/a;->b(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method public a(Ljava/util/concurrent/Future;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/d/a/a;->c:Ljava/util/concurrent/Future;

    .line 79
    return-void
.end method

.method public final b()Lcom/sec/chaton/a/a/f;
    .locals 1

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/d/a/a;->c:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->k()Lcom/sec/common/d/a/e;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;

    goto :goto_0
.end method

.method protected b(Lcom/sec/chaton/a/a/f;)V
    .locals 6

    .prologue
    const/16 v5, 0x2718

    const/16 v4, 0x2717

    .line 169
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-nez v0, :cond_2

    .line 170
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->j()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/InterruptedIOException;

    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->j()Lcom/sec/common/d/a/c;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/h;

    .line 173
    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->a()Lcom/sec/chaton/util/cg;

    move-result-object v2

    .line 174
    invoke-virtual {v2}, Lcom/sec/chaton/util/cg;->a()Z

    move-result v1

    .line 176
    invoke-virtual {v2}, Lcom/sec/chaton/util/cg;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 177
    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->a()Lcom/sec/chaton/util/cg;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/cd;->a(Lcom/sec/chaton/util/cg;)Lcom/sec/chaton/util/cf;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/util/cf;->a:Lcom/sec/chaton/util/cf;

    if-ne v2, v3, :cond_5

    .line 178
    sget-boolean v1, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v1, :cond_0

    .line 179
    const-string v1, "For task is stopped by timeout, change server type to secondary."

    iget-object v2, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->a()Lcom/sec/chaton/util/cg;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cf;->b:Lcom/sec/chaton/util/cf;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cd;->a(Lcom/sec/chaton/util/cg;Lcom/sec/chaton/util/cf;)V

    .line 184
    const/4 v0, 0x0

    .line 190
    :goto_0
    if-eqz v0, :cond_2

    .line 191
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_1

    .line 192
    const-string v0, "Re-execute GLD task."

    iget-object v1, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/sec/chaton/d/l;->a()V

    .line 205
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v0

    const/16 v1, 0x190

    if-ne v0, v1, :cond_9

    .line 206
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x2713

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x2714

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-ne v0, v5, :cond_9

    .line 212
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 223
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 224
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->a(I)V

    .line 258
    :cond_4
    :goto_1
    return-void

    .line 186
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->a()Lcom/sec/chaton/util/cg;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/util/cf;->a:Lcom/sec/chaton/util/cf;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/cd;->a(Lcom/sec/chaton/util/cg;Lcom/sec/chaton/util/cf;)V

    :cond_6
    move v0, v1

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    sget-boolean v1, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v1, :cond_4

    .line 215
    const-string v1, "QueueManager.resetAllQueue is interrpted."

    iget-object v2, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 226
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-ne v0, v5, :cond_8

    .line 227
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->a(I)V

    goto :goto_1

    .line 230
    :cond_8
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->b()V

    goto :goto_1

    .line 257
    :cond_9
    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/a;->a(Lcom/sec/chaton/a/a/f;)V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->p()V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/d/a/a;->c:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 100
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lcom/sec/chaton/d/a/a;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/d/a/a;->a(Lcom/sec/chaton/d/a/a;)I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/d/a/a;->c:Ljava/util/concurrent/Future;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/sec/chaton/a/a/f;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 115
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->n()Lcom/sec/common/d/a/e;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-object v0

    .line 116
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 117
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->k()Lcom/sec/common/d/a/e;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 122
    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/a/f;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/chaton/d/a/a;->a:Lcom/sec/chaton/j/h;

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->a()Lcom/sec/chaton/util/cg;

    move-result-object v0

    .line 153
    invoke-static {v0}, Lcom/sec/chaton/util/cd;->b(Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {v0}, Lcom/sec/chaton/util/cg;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "Because server address is null, execute provisioning."

    iget-object v1, p0, Lcom/sec/chaton/d/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/sec/chaton/d/l;->b()Lcom/sec/chaton/d/a/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/be;->e()Lcom/sec/chaton/a/a/f;

    .line 164
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/chaton/d/a/a;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
