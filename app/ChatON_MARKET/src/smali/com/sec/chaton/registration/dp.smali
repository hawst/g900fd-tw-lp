.class Lcom/sec/chaton/registration/dp;
.super Lcom/sec/chaton/util/ar;
.source "FragmentRegistPushName.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegistPushName;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-direct {p0}, Lcom/sec/chaton/util/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 550
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 639
    :goto_0
    return-void

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 563
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Push registration failed. Network is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v0, :cond_1

    const-string v0, "unavailable"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityProvisioning"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0xbbc

    if-ne v0, v1, :cond_2

    .line 568
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0042

    new-instance v3, Lcom/sec/chaton/registration/dr;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dr;-><init>(Lcom/sec/chaton/registration/dp;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/registration/dq;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dq;-><init>(Lcom/sec/chaton/registration/dp;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 630
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->g(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 637
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 563
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 590
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b029a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0223

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/registration/dt;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dt;-><init>(Lcom/sec/chaton/registration/dp;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0232

    new-instance v3, Lcom/sec/chaton/registration/ds;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/ds;-><init>(Lcom/sec/chaton/registration/dp;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto :goto_2

    .line 632
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/dp;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->e(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    goto :goto_3
.end method
