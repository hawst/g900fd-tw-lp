.class Lcom/sec/chaton/registration/r;
.super Landroid/os/Handler;
.source "ActivitySyncSignInPopup.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const v6, 0x7f0b02a9

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 351
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d3

    if-ne v1, v2, :cond_7

    .line 353
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 355
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;

    .line 357
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "userInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto :goto_0

    .line 369
    :cond_4
    const-string v1, "samsung_account_email"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v1, "samsung_account_faimly_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->familyName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v1, "samsung_account_given_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->givenName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 383
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 386
    :cond_5
    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->i(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v5}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V

    goto/16 :goto_0

    .line 395
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->d:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 398
    :cond_7
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d5

    if-ne v1, v2, :cond_1d

    .line 400
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_9

    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc353

    if-ne v1, v2, :cond_e

    .line 403
    :cond_9
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_account_country_code"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 405
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_a

    .line 406
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Final account ISO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_a
    const-string v1, "did_samsung_account_mapping"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 409
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "account_mapping_fail"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 414
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1, v5}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;I)I

    .line 416
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->b()V

    .line 417
    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 418
    invoke-static {}, Lcom/sec/chaton/event/f;->a()V

    .line 419
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->j(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto/16 :goto_0

    .line 422
    :cond_b
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/coolots/sso/a/a;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 431
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 436
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 437
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 443
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 444
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 455
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc356

    if-ne v1, v2, :cond_17

    .line 457
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_f

    .line 458
    const-string v0, "SSO token validaion was finished"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->m(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I

    move-result v0

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_15

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->p(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I

    move-result v0

    if-lt v0, v7, :cond_12

    .line 464
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 466
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 468
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 472
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->p(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I

    move-result v0

    if-ge v0, v7, :cond_14

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->m(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I

    move-result v0

    const v1, 0x24ab8

    if-lt v0, v1, :cond_13

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V

    goto/16 :goto_0

    .line 477
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V

    goto/16 :goto_0

    .line 481
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v5}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;I)I

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 485
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 490
    :cond_16
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->d(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    goto/16 :goto_0

    .line 494
    :cond_17
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc352

    if-ne v1, v2, :cond_19

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 496
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 497
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 500
    :cond_18
    const-string v0, "aucode was expired"

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->d(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    goto/16 :goto_0

    .line 503
    :cond_19
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc354

    if-ne v1, v2, :cond_1c

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 506
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 509
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 513
    :cond_1b
    const-string v0, "device over max of limitation"

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03d6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/registration/s;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/s;-><init>(Lcom/sec/chaton/registration/r;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 525
    :cond_1c
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->e:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 528
    :cond_1d
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d2

    if-ne v1, v2, :cond_0

    .line 530
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_21

    .line 532
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accessToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1e

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 538
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 542
    :cond_1f
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 545
    :cond_20
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;

    .line 546
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;

    .line 548
    const-string v0, "samsung_account_user_id"

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v0, "samsung_account_token"

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->s(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->t(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->s(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    iget-object v3, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 557
    :cond_21
    iget-object v1, p0, Lcom/sec/chaton/registration/r;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->f:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
