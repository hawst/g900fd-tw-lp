.class Lcom/sec/chaton/registration/bs;
.super Landroid/os/Handler;
.source "FragmentNonSelfSMS.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const v10, 0x7f0b0042

    const v9, 0x7f0b002a

    const/4 v8, 0x0

    const v7, 0x7f0b016c

    .line 880
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1298
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 884
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x515

    if-ne v1, v2, :cond_3

    .line 886
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_a

    .line 888
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iput-object v1, v2, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    .line 889
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    if-eqz v1, :cond_9

    .line 890
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/GetSMSAuthToken;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/GetSMSAuthToken;->token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Ljava/lang/String;)Ljava/lang/String;

    .line 894
    const-string v1, "token"

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/GetSMSAuthToken;->token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "SMSSaveCountInterval"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 897
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 916
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 918
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1002
    :cond_3
    :goto_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x516

    if-ne v1, v2, :cond_5

    .line 1012
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_e

    .line 1014
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    const-string v2, "SMS"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 1016
    const-string v1, "over_sms_request"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1069
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1070
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1071
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1078
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d4

    if-ne v1, v2, :cond_16

    .line 1080
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1082
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1083
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1086
    :cond_6
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_12

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 1091
    const-string v1, "authnum"

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "temp_country_ISO"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1099
    const-string v2, "msisdn"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "msisdn_temp"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->m(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 1126
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "account_country_code"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v11, :cond_11

    invoke-static {v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 1129
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1130
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v11}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1132
    :cond_7
    invoke-static {}, Lcom/sec/chaton/event/f;->a()V

    .line 1133
    const-string v2, "country_ISO"

    invoke-static {v2, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    .line 1227
    :cond_8
    :goto_4
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xcb

    if-ne v1, v2, :cond_0

    .line 1228
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1a

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1a

    .line 1232
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_country_ISO"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1233
    const-string v1, "country_ISO"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    const-string v0, "authnum"

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    const-string v0, "msisdn"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn_temp"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Z)Z

    .line 1244
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    goto/16 :goto_0

    .line 898
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 901
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/registration/bt;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/bt;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v2, v10, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 908
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 910
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 912
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_1

    .line 923
    :cond_a
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x7532

    if-ne v1, v2, :cond_b

    .line 926
    const-string v1, "server sent error cause by GET_SMS_AUTHTOKEN_GENERATED_RIGHT_BEFORE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;)V

    .line 932
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    const-string v2, "SMS"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 933
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 935
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_2

    .line 941
    :cond_b
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x7531

    if-ne v1, v2, :cond_d

    .line 944
    const-string v1, "server sent error cause by GET_SMS_AUTHTOKEN_INVALID"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b021e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/registration/bu;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/bu;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v2, v10, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 962
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 963
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1, v8}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 964
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 969
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 970
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 972
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_2

    .line 976
    :cond_d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 978
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/bv;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/bv;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v1, v10, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 988
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 989
    const-string v1, "SELF SMS failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 992
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 994
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_2

    .line 1020
    :cond_e
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x9c43

    if-ne v1, v2, :cond_f

    .line 1029
    const-string v1, "server sent error cause by SEND_SMS_REQUEST_LIMIT"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1038
    :cond_f
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x9c45

    if-ne v1, v2, :cond_10

    .line 1046
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->j(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 1047
    const-string v1, "over_sms_request"

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_3

    .line 1050
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1053
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1055
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/bw;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/bw;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v1, v10, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1065
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1066
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    .line 1067
    const-string v1, "SELF SMS failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1136
    :cond_11
    const-string v2, "country_ISO"

    invoke-static {v2, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    goto/16 :goto_4

    .line 1150
    :cond_12
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee7

    if-eq v1, v2, :cond_13

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee0

    if-ne v1, v2, :cond_14

    .line 1154
    :cond_13
    const-string v1, "[UpdateMSISN] verification code was wrong"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1156
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;Z)V

    goto/16 :goto_4

    .line 1160
    :cond_14
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee8

    if-ne v1, v2, :cond_15

    .line 1163
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1164
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Landroid/app/Activity;Z)V

    goto/16 :goto_4

    .line 1168
    :cond_15
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1171
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1172
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/bx;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/bx;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v1, v10, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1183
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1184
    const-string v1, "UpdateMSISDN failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1189
    :cond_16
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_8

    .line 1190
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 1191
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1193
    :cond_17
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_19

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_19

    .line 1194
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_18

    .line 1195
    const-string v1, "GET_VERSION_NOTICE success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    :cond_18
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    goto/16 :goto_4

    .line 1199
    :cond_19
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1202
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1203
    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0024

    new-instance v4, Lcom/sec/chaton/registration/bz;

    invoke-direct {v4, p0}, Lcom/sec/chaton/registration/bz;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0039

    new-instance v4, Lcom/sec/chaton/registration/by;

    invoke-direct {v4, p0}, Lcom/sec/chaton/registration/by;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1223
    const-string v1, "GET_VERSION_NOTICE failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1247
    :cond_1a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee7

    if-eq v1, v2, :cond_1b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee0

    if-ne v1, v2, :cond_1d

    .line 1250
    :cond_1b
    const-string v0, "verification code was wrong"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1252
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1253
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1255
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;Z)V

    goto/16 :goto_0

    .line 1258
    :cond_1d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee8

    if-ne v1, v2, :cond_1f

    .line 1260
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1261
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1262
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1264
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Landroid/app/Activity;Z)V

    goto/16 :goto_0

    .line 1268
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1270
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 1271
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1274
    :cond_20
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 1275
    iget-object v1, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v2, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ca;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ca;-><init>(Lcom/sec/chaton/registration/bs;)V

    invoke-virtual {v0, v10, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1290
    iget-object v0, p0, Lcom/sec/chaton/registration/bs;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1291
    const-string v0, "SELF SMS failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
