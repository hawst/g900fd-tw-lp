.class Lcom/sec/chaton/registration/ad;
.super Landroid/content/BroadcastReceiver;
.source "AuthenticatorActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/AuthenticatorActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 1618
    iput-object p1, p0, Lcom/sec/chaton/registration/ad;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1622
    const-string v0, "validation_result"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1624
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1625
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "validation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/ad;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->i(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/registration/ad;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->c(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z

    .line 1631
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/ad;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->o(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1632
    return-void
.end method
