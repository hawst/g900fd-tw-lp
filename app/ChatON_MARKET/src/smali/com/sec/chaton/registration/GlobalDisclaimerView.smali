.class public Lcom/sec/chaton/registration/GlobalDisclaimerView;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "GlobalDisclaimerView.java"


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/registration/GlobalDisclaimerView;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/registration/GlobalDisclaimerView;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/chaton/registration/GlobalDisclaimerView;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->d(Z)V

    .line 37
    :cond_0
    new-instance v0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-direct {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;-><init>()V

    return-object v0
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/GlobalDisclaimerView;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const v0, 0x7f0c00f8

    .line 68
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0c00f7

    goto :goto_0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/registration/GlobalDisclaimerView;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const v0, 0x7f0c00f6

    .line 59
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0c00f5

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 22
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_letter"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/GlobalDisclaimerView;->a:Ljava/lang/String;

    .line 24
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 44
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 45
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/registration/GlobalDisclaimerView;->setResult(ILandroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/registration/GlobalDisclaimerView;->finish()V

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
