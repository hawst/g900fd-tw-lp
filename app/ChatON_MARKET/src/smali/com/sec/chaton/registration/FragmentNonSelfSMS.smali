.class public Lcom/sec/chaton/registration/FragmentNonSelfSMS;
.super Landroid/support/v4/app/Fragment;
.source "FragmentNonSelfSMS.java"


# instance fields
.field private A:Landroid/widget/TextView;

.field private final B:I

.field private final C:I

.field private D:Lcom/sec/chaton/d/h;

.field private E:Landroid/view/View;

.field private F:Lcom/sec/common/a/d;

.field private G:Lcom/sec/common/a/d;

.field private H:Z

.field private I:Landroid/widget/Button;

.field private J:Z

.field private K:Landroid/widget/Button;

.field private L:Z

.field private M:Landroid/text/TextWatcher;

.field protected a:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

.field protected b:Ljava/lang/String;

.field final c:I

.field public d:Landroid/os/Handler;

.field e:Landroid/os/Handler;

.field f:Landroid/view/View$OnClickListener;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/sec/chaton/d/ar;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/os/CountDownTimer;

.field private n:Landroid/app/ProgressDialog;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/TextView;

.field private q:Ljava/lang/String;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/sec/chaton/d/ap;

.field private t:Lcom/sec/chaton/d/at;

.field private u:Landroid/content/BroadcastReceiver;

.field private final v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 78
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b:Ljava/lang/String;

    .line 108
    const-string v0, ": "

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->v:Ljava/lang/String;

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->c:I

    .line 124
    const v0, 0xea60

    iput v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->B:I

    .line 126
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->C:I

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    .line 133
    iput-boolean v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->H:Z

    .line 138
    iput-boolean v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->L:Z

    .line 720
    new-instance v0, Lcom/sec/chaton/registration/bn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/bn;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->d:Landroid/os/Handler;

    .line 877
    new-instance v0, Lcom/sec/chaton/registration/bs;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/registration/bs;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e:Landroid/os/Handler;

    .line 1531
    new-instance v0, Lcom/sec/chaton/registration/cc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/cc;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->M:Landroid/text/TextWatcher;

    .line 1573
    new-instance v0, Lcom/sec/chaton/registration/cd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/cd;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    return-object p1
.end method

.method protected static a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1523
    const-wide/32 v0, 0xea60

    div-long v0, p0, v0

    long-to-int v0, v0

    .line 1524
    mul-int/lit8 v1, v0, 0x3c

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    sub-long v1, p0, v1

    long-to-int v1, v1

    div-int/lit16 v1, v1, 0x3e8

    .line 1526
    const-string v2, "%02d:%02d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 579
    if-nez p0, :cond_0

    .line 604
    :goto_0
    return-void

    .line 585
    :cond_0
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 586
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 588
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0153

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget v4, Lcom/sec/chaton/c/a;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 591
    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/cj;

    invoke-direct {v3}, Lcom/sec/chaton/registration/cj;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 602
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 603
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Z)V
    .locals 4

    .prologue
    .line 628
    if-nez p0, :cond_0

    .line 648
    :goto_0
    return-void

    .line 632
    :cond_0
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 633
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 634
    const v1, 0x7f0b0154

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/ck;

    invoke-direct {v3, p1, p0}, Lcom/sec/chaton/registration/ck;-><init>(ZLandroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 646
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 647
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Z)Z
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->H:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 555
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->I:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 556
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    .line 558
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 559
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 560
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b008e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    .line 566
    :cond_0
    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->J:Z

    if-eqz v1, :cond_3

    .line 567
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->s:Lcom/sec/chaton/d/ap;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :cond_1
    :goto_1
    return-void

    .line 567
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    goto :goto_0

    .line 570
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->t:Lcom/sec/chaton/d/at;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    if-nez v3, :cond_4

    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/chaton/d/at;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    goto :goto_2
.end method

.method public static b(Landroid/app/Activity;Z)V
    .locals 4

    .prologue
    .line 653
    if-nez p0, :cond_0

    .line 673
    :goto_0
    return-void

    .line 657
    :cond_0
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 658
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 659
    const v1, 0x7f0b0224

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/cl;

    invoke-direct {v3, p1, p0}, Lcom/sec/chaton/registration/cl;-><init>(ZLandroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 671
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 672
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Z)Z
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->L:Z

    return p1
.end method

.method private c()V
    .locals 6

    .prologue
    .line 676
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "sms_expire_time"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 682
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0220

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, ""

    if-ne v0, v5, :cond_2

    const/16 v0, 0x18

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/registration/cm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/cm;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    .line 692
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0

    .line 682
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->H:Z

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->j:Lcom/sec/chaton/d/ar;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/ar;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->L:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->D:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1303
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1304
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1305
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1311
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1313
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1378
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->j:Lcom/sec/chaton/d/ar;

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/ar;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    return-void

    .line 1378
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 1793
    new-instance v0, Lcom/sec/chaton/registration/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ce;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->u:Landroid/content/BroadcastReceiver;

    .line 1876
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->u:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1877
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g()V

    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1884
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1885
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1886
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1888
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    invoke-virtual {v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1889
    const-string v3, "com.sec.chaton"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_1

    .line 1892
    invoke-static {v1, v2, v5}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1922
    :cond_0
    :goto_0
    return-void

    .line 1903
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1906
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1912
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "ChatON"

    const-string v2, "com.sec.chaton"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1913
    const-string v1, "com.android.contacts"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 1914
    const-string v1, "com.android.contacts"

    invoke-static {v0, v1, v5}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1915
    const-string v1, "com.android.contacts"

    invoke-static {v0, v1, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->c()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->I:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->J:Z

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->d()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1504
    const-string v0, "[countdown] cancelTimer"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "SMSSaveCountInterval"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1508
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "ACSSaveCountInterval"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1512
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->A:Landroid/widget/TextView;

    const-string v1, "00:00"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1513
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->m:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 1514
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->m:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1516
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1517
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1518
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1519
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    .line 1429
    const-wide/32 v0, 0xea60

    .line 1430
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1433
    const-string v4, "SMS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "over_sms_request"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1501
    :cond_0
    :goto_0
    return-void

    .line 1437
    :cond_1
    const-string v4, "ACS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "over_acs_request"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1441
    :cond_2
    iget-object v4, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1442
    iget-object v4, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1443
    iget-object v4, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1445
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "ACSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1446
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "ACSSaveCountInterval"

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 1447
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 1466
    :goto_1
    cmp-long v4, v2, v0

    if-gtz v4, :cond_3

    cmp-long v4, v2, v8

    if-gez v4, :cond_4

    :cond_3
    move-wide v2, v0

    .line 1471
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[countdown] realInterTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flag : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1474
    new-instance v0, Lcom/sec/chaton/registration/cb;

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/registration/cb;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;JJ)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/cb;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->m:Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 1449
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "ACSSaveCountInterval"

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v2, v4, v2

    goto :goto_1

    .line 1451
    :cond_6
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "SMS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1452
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v2, v4, v2

    goto :goto_1

    .line 1453
    :cond_7
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "SMSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "SMS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1454
    const-string v4, "SMSSaveCountInterval"

    add-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    move-wide v2, v0

    .line 1455
    goto/16 :goto_1

    .line 1456
    :cond_8
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "ACSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "ACS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1457
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "ACSSaveCountInterval"

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v2, v4, v2

    goto/16 :goto_1

    .line 1458
    :cond_9
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "ACSSaveCountInterval"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "ACS"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1459
    const-string v4, "ACSSaveCountInterval"

    add-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    move-wide v2, v0

    .line 1460
    goto/16 :goto_1

    :cond_a
    move-wide v2, v0

    .line 1462
    goto/16 :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1683
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1696
    :cond_0
    :goto_0
    return-void

    .line 1686
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1688
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ACS Result] Saved vToken : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    const-string v0, "ACS"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 1691
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "acstoken"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    goto :goto_0

    .line 1686
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 465
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 466
    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 467
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h()V

    .line 477
    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->setHasOptionsMenu(Z)V

    .line 478
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const v8, 0x7f0b019a

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 150
    const v0, 0x7f0300ce

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ar;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->j:Lcom/sec/chaton/d/ar;

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->s:Lcom/sec/chaton/d/ap;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->t:Lcom/sec/chaton/d/at;

    .line 156
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->D:Lcom/sec/chaton/d/h;

    .line 171
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_code"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    .line 180
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "phonenumber"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l:Ljava/lang/String;

    .line 181
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn_temp"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q:Ljava/lang/String;

    .line 184
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "authnum"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCountry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPhoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "isSMS"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->y:Z

    .line 192
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 195
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "acstoken"

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    .line 198
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isSMS : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->y:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vToken : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 202
    const v0, 0x7f0703a1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    .line 203
    const v0, 0x7f0703a5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    .line 206
    const v0, 0x7f0703a8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i:Landroid/widget/TextView;

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b01ae

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    const v0, 0x7f07039f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->o:Landroid/widget/ImageView;

    .line 221
    const v0, 0x7f0703a4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->A:Landroid/widget/TextView;

    .line 225
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->J:Z

    .line 233
    const v0, 0x7f0703a2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->I:Landroid/widget/Button;

    .line 237
    const v0, 0x7f07039e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->p:Landroid/widget/TextView;

    .line 239
    const v0, 0x7f0703a6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    .line 240
    const v0, 0x7f0703a0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->r:Landroid/widget/TextView;

    .line 241
    const v0, 0x7f0703a7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 244
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "enteredtab"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 250
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "skipRegi"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 257
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->y:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 265
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->d()V

    .line 267
    :cond_3
    const-string v0, "skipRegi"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 274
    :cond_4
    invoke-virtual {p0, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n%s"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 276
    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index of end on the string is = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->r:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 305
    const v1, 0x7f030107

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->E:Landroid/view/View;

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->E:Landroid/view/View;

    const v1, 0x7f07013b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 309
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->E:Landroid/view/View;

    const v3, 0x7f07013c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 312
    const-string v3, "for_wifi_only_device"

    invoke-static {v3}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 313
    new-instance v0, Lcom/sec/chaton/registration/bm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/bm;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 331
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->K:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->I:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/cf;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/cf;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->z:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->M:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/registration/cg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/cg;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/registration/ch;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/ch;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 390
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 395
    :cond_7
    const v0, 0x7f0703a3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 397
    const v1, 0x7f0b03a3

    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 405
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "SMSSaveCountInterval"

    invoke-virtual {v1, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "from_regist"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    if-eqz p3, :cond_9

    .line 407
    :cond_8
    const-string v1, "SMS"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 411
    :cond_9
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "ACSSaveCountInterval"

    invoke-virtual {v1, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "from_regist"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_a

    if-eqz p3, :cond_b

    .line 413
    :cond_a
    const-string v1, "ACS"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 416
    :cond_b
    const-string v1, "normal_acs"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 419
    const-string v0, "ACS"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Ljava/lang/String;)V

    .line 422
    :cond_c
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0277

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/ci;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/ci;-><init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    .line 453
    return-object v2

    .line 325
    :cond_d
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1755
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1756
    const-string v0, "onDestory..."

    const-string v1, "ActivityNonSelfSMS"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->u:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1761
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->u:Landroid/content/BroadcastReceiver;

    .line 1766
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1767
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->G:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1770
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1771
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1774
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1775
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1777
    :cond_3
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 502
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 503
    const-string v0, "authnum"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const/4 v0, 0x0

    .line 508
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1730
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1731
    const-string v0, "onPause..."

    const-string v1, "ActivityNonSelfSMS"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1733
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1787
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1788
    const-string v0, "onResume..."

    const-string v1, "ActivityNonSelfSMS"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1789
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1743
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 1744
    const-string v0, "onStop..."

    const-string v1, "ActivityNonSelfSMS"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    return-void
.end method
