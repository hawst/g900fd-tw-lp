.class Lcom/sec/chaton/registration/db;
.super Landroid/os/Handler;
.source "FragmentRegist.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 1279
    iput-object p1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0b021e

    const/4 v6, 0x0

    const v5, 0x7f0b01bc

    const v4, 0x7f0b0042

    const/4 v3, 0x1

    .line 1283
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1565
    :cond_0
    :goto_0
    return-void

    .line 1290
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1328
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_9

    .line 1329
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 1330
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetSSMServerAddress4;

    .line 1332
    :try_start_0
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetSSMServerAddress4;->msisdn:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msisnd ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetSSMServerAddress4;->msisdn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetSSMServerAddress4;->msisdn:Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    .line 1341
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1343
    :cond_2
    const-string v0, "phonenumber"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegist;->b(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegist;->c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    :goto_1
    const-string v0, "msisdn_temp"

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "authType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->h(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->i(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1556
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->h(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 1345
    :cond_4
    :try_start_1
    const-string v0, "phonenumber"

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1404
    :catch_0
    move-exception v0

    .line 1405
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 1408
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe2

    if-eq v1, v2, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe1

    if-ne v1, v2, :cond_8

    .line 1409
    :cond_6
    const-string v0, "server sent error cause by WRONG_PARAMETER_VALUE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1412
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1413
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1417
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/dc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dc;-><init>(Lcom/sec/chaton/registration/db;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1425
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1426
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_2

    .line 1431
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->b(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1433
    :cond_9
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_3

    .line 1434
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_14

    .line 1435
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;

    .line 1437
    if-eqz v1, :cond_13

    .line 1438
    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->update_msisdn_result:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->update_msisdn_result:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_b

    .line 1439
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1440
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1444
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/dd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dd;-><init>(Lcom/sec/chaton/registration/db;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1454
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 1456
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1457
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_2

    .line 1462
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->l(Lcom/sec/chaton/registration/FragmentRegist;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1463
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_c

    .line 1464
    const-string v0, "Already checked update MSISDN"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->m(Lcom/sec/chaton/registration/FragmentRegist;)V

    .line 1467
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0, v6}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Z)Z

    goto/16 :goto_0

    .line 1471
    :cond_d
    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_msisdn:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_msisdn:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_f

    .line 1473
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1474
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1477
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/df;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/df;-><init>(Lcom/sec/chaton/registration/db;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/registration/de;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/de;-><init>(Lcom/sec/chaton/registration/db;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1501
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1502
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_2

    .line 1504
    :cond_f
    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    iget-object v0, v1, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_11

    .line 1505
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1506
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1507
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->i(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Z)V

    .line 1508
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0, v3}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Z)Z

    goto/16 :goto_2

    .line 1511
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->m(Lcom/sec/chaton/registration/FragmentRegist;)V

    goto/16 :goto_2

    .line 1514
    :cond_11
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1515
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1516
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->i(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Z)V

    .line 1517
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0, v3}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Z)Z

    goto/16 :goto_2

    .line 1520
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->m(Lcom/sec/chaton/registration/FragmentRegist;)V

    goto/16 :goto_2

    .line 1526
    :cond_13
    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->b(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V

    .line 1527
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1528
    const-string v0, "result is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1532
    :cond_14
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee2

    if-ne v1, v2, :cond_16

    .line 1534
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1535
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1536
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1538
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/dg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dg;-><init>(Lcom/sec/chaton/registration/db;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1545
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1546
    iget-object v0, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_2

    .line 1552
    :cond_16
    iget-object v1, p0, Lcom/sec/chaton/registration/db;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->b(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
