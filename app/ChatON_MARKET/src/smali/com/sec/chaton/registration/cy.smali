.class Lcom/sec/chaton/registration/cy;
.super Ljava/lang/Object;
.source "FragmentRegist.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 736
    iput-object p1, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 742
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->b(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 754
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->d(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 755
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->e(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->e(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 808
    :cond_2
    :goto_0
    return-void

    .line 753
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 780
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->e(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 782
    iget-object v0, p0, Lcom/sec/chaton/registration/cy;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->e(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
