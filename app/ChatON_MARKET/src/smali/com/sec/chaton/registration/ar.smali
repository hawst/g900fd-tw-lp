.class Lcom/sec/chaton/registration/ar;
.super Ljava/lang/Object;
.source "FragmentConnectAccounts.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentConnectAccounts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0702ef

    if-ne v0, v1, :cond_1

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Lcom/sec/chaton/d/au;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;

    .line 137
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 152
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Lcom/sec/chaton/d/au;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Lcom/sec/chaton/d/au;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b:Lcom/sec/chaton/d/bb;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/au;->a(Lcom/sec/chaton/d/bb;)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Lcom/sec/chaton/d/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/au;->a()V

    goto :goto_0

    .line 139
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    new-instance v1, Lcom/sec/chaton/d/au;

    iget-object v2, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/an;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/d/au;-><init>(Landroid/app/Activity;Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;

    goto :goto_1

    .line 142
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    new-instance v1, Lcom/sec/chaton/d/au;

    iget-object v2, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/d/au;-><init>(Landroid/app/Activity;Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;

    goto :goto_1

    .line 145
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    new-instance v1, Lcom/sec/chaton/d/au;

    iget-object v2, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/d/au;-><init>(Landroid/app/Activity;Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;

    goto :goto_1

    .line 148
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    new-instance v1, Lcom/sec/chaton/d/au;

    iget-object v2, p0, Lcom/sec/chaton/registration/ar;->a:Lcom/sec/chaton/registration/FragmentConnectAccounts;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/al;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/d/au;-><init>(Landroid/app/Activity;Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;

    goto :goto_1

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x7f0702eb
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
