.class final enum Lcom/sec/chaton/registration/gi;
.super Ljava/lang/Enum;
.source "NewDisclaimerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/registration/gi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/registration/gi;

.field public static final enum b:Lcom/sec/chaton/registration/gi;

.field public static final enum c:Lcom/sec/chaton/registration/gi;

.field public static final enum d:Lcom/sec/chaton/registration/gi;

.field public static final enum e:Lcom/sec/chaton/registration/gi;

.field private static final synthetic f:[Lcom/sec/chaton/registration/gi;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 71
    new-instance v0, Lcom/sec/chaton/registration/gi;

    const-string v1, "get_server_address"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/registration/gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/gi;->a:Lcom/sec/chaton/registration/gi;

    .line 72
    new-instance v0, Lcom/sec/chaton/registration/gi;

    const-string v1, "version_for_nation"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/registration/gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/gi;->b:Lcom/sec/chaton/registration/gi;

    .line 73
    new-instance v0, Lcom/sec/chaton/registration/gi;

    const-string v1, "version_for_upgrade"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/registration/gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/gi;->c:Lcom/sec/chaton/registration/gi;

    .line 74
    new-instance v0, Lcom/sec/chaton/registration/gi;

    const-string v1, "accept_disclaimer"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/registration/gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/gi;->d:Lcom/sec/chaton/registration/gi;

    .line 75
    new-instance v0, Lcom/sec/chaton/registration/gi;

    const-string v1, "chaton_v"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/registration/gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/gi;->e:Lcom/sec/chaton/registration/gi;

    .line 70
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/registration/gi;

    sget-object v1, Lcom/sec/chaton/registration/gi;->a:Lcom/sec/chaton/registration/gi;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/registration/gi;->b:Lcom/sec/chaton/registration/gi;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/registration/gi;->c:Lcom/sec/chaton/registration/gi;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/registration/gi;->d:Lcom/sec/chaton/registration/gi;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/registration/gi;->e:Lcom/sec/chaton/registration/gi;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/registration/gi;->f:[Lcom/sec/chaton/registration/gi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/registration/gi;
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/sec/chaton/registration/gi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/gi;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/registration/gi;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/chaton/registration/gi;->f:[Lcom/sec/chaton/registration/gi;

    invoke-virtual {v0}, [Lcom/sec/chaton/registration/gi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/registration/gi;

    return-object v0
.end method
