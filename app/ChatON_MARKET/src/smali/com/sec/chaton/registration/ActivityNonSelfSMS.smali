.class public Lcom/sec/chaton/registration/ActivityNonSelfSMS;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityNonSelfSMS.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 32
    if-eqz v2, :cond_2

    .line 33
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 34
    sget-object v3, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 37
    :goto_0
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 38
    :cond_0
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->setTitle(I)V

    .line 40
    :cond_1
    new-instance v0, Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;-><init>()V

    return-object v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-static {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->a(Landroid/app/Activity;)V

    .line 65
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-static {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->a(Landroid/app/Activity;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 45
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 46
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->setResult(I)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityNonSelfSMS;->finish()V

    .line 48
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
