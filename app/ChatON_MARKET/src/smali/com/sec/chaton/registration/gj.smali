.class public Lcom/sec/chaton/registration/gj;
.super Ljava/lang/Object;
.source "PasswordLockWorker.java"


# static fields
.field private static e:Lcom/sec/chaton/registration/gj;


# instance fields
.field a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:J

.field private f:I

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/registration/gj;->e:Lcom/sec/chaton/registration/gj;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-class v0, Lcom/sec/chaton/registration/gj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/gj;->b:Ljava/lang/String;

    .line 13
    iput v2, p0, Lcom/sec/chaton/registration/gj;->c:I

    .line 14
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/chaton/registration/gj;->d:J

    .line 17
    iput v2, p0, Lcom/sec/chaton/registration/gj;->f:I

    .line 42
    new-instance v0, Lcom/sec/chaton/registration/gk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/gk;-><init>(Lcom/sec/chaton/registration/gj;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/gj;->g:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/gj;I)I
    .locals 0

    .prologue
    .line 10
    iput p1, p0, Lcom/sec/chaton/registration/gj;->f:I

    return p1
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/registration/gj;
    .locals 2

    .prologue
    .line 22
    const-class v1, Lcom/sec/chaton/registration/gj;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/registration/gj;->e:Lcom/sec/chaton/registration/gj;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/chaton/registration/gj;

    invoke-direct {v0}, Lcom/sec/chaton/registration/gj;-><init>()V

    sput-object v0, Lcom/sec/chaton/registration/gj;->e:Lcom/sec/chaton/registration/gj;

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/chaton/registration/gj;->e:Lcom/sec/chaton/registration/gj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/gj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/chaton/registration/gj;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCountWorker : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/registration/gj;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/registration/gj;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " requested Class : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/gj;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :cond_0
    if-nez p1, :cond_1

    .line 40
    :goto_0
    monitor-exit p0

    return-void

    .line 35
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/sec/chaton/registration/gj;->a:Landroid/content/Context;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/registration/gj;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 38
    iget-object v0, p0, Lcom/sec/chaton/registration/gj;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/sec/chaton/registration/gj;->g:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
