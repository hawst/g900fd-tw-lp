.class public Lcom/sec/chaton/registration/SelectLanguageFragment;
.super Landroid/support/v4/app/ListFragment;
.source "SelectLanguageFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Lcom/sec/chaton/widget/ClearableEditText;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/CharSequence;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/chaton/registration/gl;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/view/View;

.field private i:Landroid/support/v4/app/FragmentActivity;

.field private j:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->b:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->e:Lcom/sec/chaton/registration/gl;

    .line 39
    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    .line 43
    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    .line 144
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/SelectLanguageFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/registration/SelectLanguageFragment;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    .line 247
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 248
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 249
    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 250
    iget-object v3, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/chaton/util/as;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 251
    iget-object v3, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v3, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mFilteredCountry = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->e:Lcom/sec/chaton/registration/gl;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/gl;->notifyDataSetChanged()V

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 262
    const-string v0, "YES"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {p0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 270
    :goto_1
    return-void

    .line 266
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 119
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 120
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 122
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 128
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    .line 129
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 142
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 59
    const v0, 0x7f030009

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->j:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->d:Landroid/content/Context;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->j:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    const v2, 0x10000006

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setImeOptions(I)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v2, 0x4000

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setInputType(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v2, 0x1e

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setMaxLength(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->a:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->j:Landroid/view/View;

    const v2, 0x7f07002b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->h:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_hide_language"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    if-eqz v2, :cond_2

    .line 85
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 86
    :goto_0
    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 87
    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 88
    iget-object v4, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 95
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    :cond_3
    new-instance v0, Lcom/sec/chaton/registration/gl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/gl;-><init>(Lcom/sec/chaton/registration/SelectLanguageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->e:Lcom/sec/chaton/registration/gl;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->e:Lcom/sec/chaton/registration/gl;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->j:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDetach()V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    .line 136
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 225
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->b:Ljava/lang/String;

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "selectedLanguage ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "LANGUAGE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 233
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/gl;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/gl;->notifyDataSetChanged()V

    .line 234
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 280
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/registration/SelectLanguageFragment;->i:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0b0031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 284
    :cond_0
    return-void
.end method
