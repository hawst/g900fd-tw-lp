.class public Lcom/sec/chaton/registration/FragmentRegistPushName;
.super Landroid/support/v4/app/Fragment;
.source "FragmentRegistPushName.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# instance fields
.field a:Landroid/widget/EditText;

.field b:Landroid/app/Activity;

.field c:Lcom/sec/chaton/util/ar;

.field public d:Landroid/os/Handler;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private i:Lcom/sec/chaton/d/ap;

.field private j:Lcom/sec/common/a/d;

.field private k:Lcom/sec/chaton/d/at;

.field private l:Lcom/sec/chaton/chat/fh;

.field private m:Lcom/sec/chaton/d/h;

.field private final n:I

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Landroid/view/MenuItem;

.field private t:Lcom/coolots/sso/a/a;

.field private u:Ljava/lang/String;

.field private v:Lcom/sec/common/a/d;

.field private w:Landroid/view/View;

.field private x:Landroid/app/ProgressDialog;

.field private y:Landroid/text/TextWatcher;

.field private z:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 92
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    .line 106
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->n:I

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->r:I

    .line 117
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    .line 119
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    .line 121
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    .line 356
    new-instance v0, Lcom/sec/chaton/registration/do;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/do;-><init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->y:Landroid/text/TextWatcher;

    .line 546
    new-instance v0, Lcom/sec/chaton/registration/dp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/dp;-><init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->c:Lcom/sec/chaton/util/ar;

    .line 657
    new-instance v0, Lcom/sec/chaton/registration/du;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/du;-><init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->d:Landroid/os/Handler;

    .line 1366
    new-instance v0, Lcom/sec/chaton/registration/ek;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ek;-><init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->z:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegistPushName;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->r:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->s:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    const v1, 0x7f070369

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 518
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 484
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    const-string v1, "skip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 489
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 492
    :cond_2
    iput-object v3, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    .line 493
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    const v1, 0x7f07036a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 496
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    const v2, 0x7f070369

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 498
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 500
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0, v4}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 502
    :cond_4
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->u:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 6

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->y:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/registration/dn;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/dn;-><init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 354
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1331
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    const-string v0, "Input MCC is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    :goto_0
    return-void

    .line 1336
    :cond_0
    const/4 v1, 0x0

    .line 1340
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d001b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v2

    .line 1341
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d001a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v3

    .line 1343
    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 1344
    aget-object v4, v3, v0

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1345
    aget-object v0, v2, v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1350
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Input MCC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Output : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    const-string v1, "temp_account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1343
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->p:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 643
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    const-string v1, "skip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->i:Lcom/sec/chaton/d/ap;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;)V

    .line 655
    :goto_0
    return-void

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 646
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->i:Lcom/sec/chaton/d/ap;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    goto :goto_1

    .line 650
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->k:Lcom/sec/chaton/d/at;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->q:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1263
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    .line 1266
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1267
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1270
    :cond_1
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1298
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1299
    const-string v1, "com.sec.chaton"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1301
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1302
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1305
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1309
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->c()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->h:Lcom/sec/chaton/d/a;

    return-object v0
.end method

.method private f()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1314
    .line 1315
    const/4 v2, 0x0

    .line 1317
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "com.osp.app.signin"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1322
    :goto_0
    if-eqz v1, :cond_0

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v2, 0x32c9

    if-lt v1, v2, :cond_0

    .line 1323
    const/4 v0, 0x1

    .line 1326
    :cond_0
    return v0

    .line 1318
    :catch_0
    move-exception v1

    .line 1319
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->m:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/chat/fh;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->l:Lcom/sec/chaton/chat/fh;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->i:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->e()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/registration/FragmentRegistPushName;)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic s(Lcom/sec/chaton/registration/FragmentRegistPushName;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->r:I

    return v0
.end method

.method static synthetic t(Lcom/sec/chaton/registration/FragmentRegistPushName;)I
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->r:I

    return v0
.end method

.method static synthetic u(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->k:Lcom/sec/chaton/d/at;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->s:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 536
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 537
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0290

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->q:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->h:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->c:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 540
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->c:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x7530

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ar;->a(I)V

    .line 543
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 191
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 192
    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 193
    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 196
    const-string v1, "action_sso_receive_code"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 208
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 277
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 278
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 279
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->s:Landroid/view/MenuItem;

    .line 280
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 135
    const v0, 0x7f0300e5

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 137
    const v0, 0x7f070404

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    .line 140
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "hidden_skip_mode"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    const-string v0, "skip"

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    .line 150
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "msisdn"

    invoke-virtual {v0, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->g:Ljava/lang/String;

    .line 151
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "token"

    invoke-virtual {v0, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 154
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "acstoken"

    invoke-virtual {v0, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->e:Ljava/lang/String;

    .line 158
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "authnum"

    invoke-virtual {v0, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->f:Ljava/lang/String;

    .line 164
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->h:Lcom/sec/chaton/d/a;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->i:Lcom/sec/chaton/d/ap;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->k:Lcom/sec/chaton/d/at;

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->d:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/chat/fh;->a(Landroid/os/Handler;J)Lcom/sec/chaton/chat/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->l:Lcom/sec/chaton/chat/fh;

    .line 177
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->d:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->m:Lcom/sec/chaton/d/h;

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0280

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 181
    aget-object v2, v0, v4

    iput-object v2, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->p:Ljava/lang/String;

    .line 182
    const/4 v2, 0x1

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->q:Ljava/lang/String;

    .line 184
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b()V

    .line 186
    return-object v1

    .line 142
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "prov_phone"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    const-string v0, "phone"

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 146
    :cond_2
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->o:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 214
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 216
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->d()V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 224
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->t:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->v:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 231
    :cond_2
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 235
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->w:Landroid/view/View;

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 246
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 293
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a6

    if-ne v0, v1, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a()V

    .line 295
    const/4 v0, 0x1

    .line 298
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 285
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->s:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 289
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1419
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1420
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    :cond_0
    if-ne p1, v3, :cond_1

    .line 1424
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->m:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 1430
    :goto_0
    return-void

    .line 1426
    :cond_1
    new-instance v0, Lcom/sec/chaton/registration/an;

    invoke-direct {v0}, Lcom/sec/chaton/registration/an;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/an;->a()V

    .line 1427
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Ljava/lang/String;)V

    .line 1428
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegistPushName;->m:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 0

    .prologue
    .line 1435
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 420
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 421
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 425
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 426
    const-string v0, "PushName state is already done"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 429
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 437
    :cond_1
    :goto_0
    return-void

    .line 431
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 432
    const-string v0, "getActivity is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
