.class final enum Lcom/sec/chaton/registration/ab;
.super Ljava/lang/Enum;
.source "ActivitySyncSignInPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/registration/ab;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/registration/ab;

.field public static final enum b:Lcom/sec/chaton/registration/ab;

.field public static final enum c:Lcom/sec/chaton/registration/ab;

.field public static final enum d:Lcom/sec/chaton/registration/ab;

.field public static final enum e:Lcom/sec/chaton/registration/ab;

.field public static final enum f:Lcom/sec/chaton/registration/ab;

.field public static final enum g:Lcom/sec/chaton/registration/ab;

.field public static final enum h:Lcom/sec/chaton/registration/ab;

.field private static final synthetic i:[Lcom/sec/chaton/registration/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 114
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "get_buddies"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->a:Lcom/sec/chaton/registration/ab;

    .line 115
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "get_chatlist"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->b:Lcom/sec/chaton/registration/ab;

    .line 116
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "version_for_nation"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->c:Lcom/sec/chaton/registration/ab;

    .line 117
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "SA_user_info"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->d:Lcom/sec/chaton/registration/ab;

    .line 118
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "mapping"

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->e:Lcom/sec/chaton/registration/ab;

    .line 119
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "SA_access_token"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->f:Lcom/sec/chaton/registration/ab;

    .line 120
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "extra_info"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->g:Lcom/sec/chaton/registration/ab;

    .line 121
    new-instance v0, Lcom/sec/chaton/registration/ab;

    const-string v1, "upload_all_group"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/registration/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/ab;->h:Lcom/sec/chaton/registration/ab;

    .line 113
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/chaton/registration/ab;

    sget-object v1, Lcom/sec/chaton/registration/ab;->a:Lcom/sec/chaton/registration/ab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/registration/ab;->b:Lcom/sec/chaton/registration/ab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/registration/ab;->c:Lcom/sec/chaton/registration/ab;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/registration/ab;->d:Lcom/sec/chaton/registration/ab;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/registration/ab;->e:Lcom/sec/chaton/registration/ab;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/registration/ab;->f:Lcom/sec/chaton/registration/ab;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/registration/ab;->g:Lcom/sec/chaton/registration/ab;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/registration/ab;->h:Lcom/sec/chaton/registration/ab;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/registration/ab;->i:[Lcom/sec/chaton/registration/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/registration/ab;
    .locals 1

    .prologue
    .line 113
    const-class v0, Lcom/sec/chaton/registration/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/ab;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/registration/ab;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/sec/chaton/registration/ab;->i:[Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, [Lcom/sec/chaton/registration/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/registration/ab;

    return-object v0
.end method
