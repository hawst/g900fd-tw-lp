.class Lcom/sec/chaton/registration/v;
.super Landroid/content/BroadcastReceiver;
.source "ActivitySyncSignInPopup.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 925
    iput-object p1, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 930
    const-string v0, "version"

    const/4 v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 932
    iget-object v1, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    const-string v2, "authcode"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->d(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;

    .line 933
    const-string v1, "auth_token"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 935
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 936
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " authcode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " authToken : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 1000
    :goto_0
    return-void

    .line 942
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->t(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 945
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto :goto_0

    .line 954
    :pswitch_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 955
    const-string v0, "email_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 956
    const-string v2, "user_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 957
    const-string v3, "mcc"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 958
    const-string v4, "api_server_url"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 959
    const-string v5, "birthday"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 961
    const-string v6, "samsung_account_token"

    invoke-static {v6, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    const-string v1, "samsung_account_email"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    const-string v0, "samsung_account_user_id"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    const-string v0, "samsung_account_api_server"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    const-string v0, "samsung_account_birthday"

    invoke-static {v0, v5}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->i(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v4}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 971
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->c()V

    .line 972
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v7}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V

    goto/16 :goto_0

    .line 977
    :cond_3
    const-string v0, "check_list"

    invoke-virtual {p2, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 979
    if-lez v0, :cond_6

    .line 980
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 981
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 983
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 984
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 985
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 987
    :cond_5
    invoke-static {p1, p2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 989
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 990
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 993
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/v;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 940
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
