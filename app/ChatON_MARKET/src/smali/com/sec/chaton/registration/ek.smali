.class Lcom/sec/chaton/registration/ek;
.super Landroid/content/BroadcastReceiver;
.source "FragmentRegistPushName.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegistPushName;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 1366
    iput-object p1, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1372
    const-string v0, "auth_token"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1375
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1377
    const-string v1, "email_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1378
    const-string v2, "user_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1379
    const-string v3, "mcc"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1380
    const-string v4, "api_server_url"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1381
    const-string v5, "birthday"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1383
    const-string v6, "samsung_account_token"

    invoke-static {v6, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    const-string v6, "samsung_account_email"

    invoke-static {v6, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    const-string v1, "samsung_account_user_id"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    const-string v1, "samsung_account_api_server"

    invoke-static {v1, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    const-string v1, "samsung_account_birthday"

    invoke-static {v1, v5}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    iget-object v1, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1, v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->c(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 1390
    iget-object v1, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->u(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/at;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;)V

    .line 1410
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    return-void

    .line 1396
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1399
    const-string v1, "check_list"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1401
    if-lez v1, :cond_1

    .line 1402
    invoke-static {p1, p2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 1404
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/registration/ek;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto :goto_0
.end method
