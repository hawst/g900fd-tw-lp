.class Lcom/sec/chaton/registration/du;
.super Landroid/os/Handler;
.source "FragmentRegistPushName.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegistPushName;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const v8, 0x7f0b0039

    const v5, 0x7f0b002a

    const v7, 0x7f0b0024

    const/4 v4, 0x1

    const v6, 0x7f0b016c

    .line 661
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1256
    :cond_0
    :goto_0
    return-void

    .line 665
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 666
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 934
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_a

    .line 940
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    const-string v1, "append"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 669
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 671
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 672
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0

    .line 674
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 676
    const-string v1, "fail to set extra info"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 679
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 680
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ec;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ec;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v7, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/dv;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dv;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v8, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 704
    :sswitch_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->i(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 708
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 709
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 710
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 714
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 715
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 717
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->j(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/chat/fh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/fh;->a()V

    goto/16 :goto_0

    .line 721
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 723
    const-string v1, "fail to get all buddies"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 727
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 728
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ee;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ee;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v7, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ed;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ed;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v8, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 750
    :sswitch_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 754
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 756
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->d:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_7

    .line 760
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 764
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 765
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 766
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 767
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/eg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/eg;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v7, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ef;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ef;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v8, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 790
    :sswitch_4
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 792
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_8

    .line 795
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 796
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 797
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 798
    invoke-static {}, Lcom/sec/chaton/j/af;->c()Z

    .line 800
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 803
    :cond_8
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-ne v1, v4, :cond_9

    .line 804
    const-string v1, "Fail to sync ChatListInfo ... "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatListInfo Fail code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v4, :cond_9

    .line 808
    const-string v1, "01000006"

    const-string v2, "1008"

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    .line 812
    :cond_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 815
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 816
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/ei;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ei;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v7, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/registration/eh;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/eh;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v0, v8, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 944
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee7

    if-ne v1, v2, :cond_b

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 947
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;Z)V

    .line 949
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 950
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 954
    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee8

    if-ne v1, v2, :cond_c

    .line 956
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 957
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Landroid/app/Activity;Z)V

    .line 959
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 960
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 964
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 965
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 968
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 970
    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/dw;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dw;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v7, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/ej;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/ej;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v8, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 997
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Registration Failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1013
    :sswitch_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_e

    :cond_d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc357

    if-ne v1, v2, :cond_16

    .line 1017
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1020
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;

    .line 1022
    iget-object v2, v1, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->uid:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v1, v1, Lcom/sec/chaton/io/entry/SkipSMSVerifyServer;->chatonid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1023
    :cond_f
    sget-boolean v1, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v1, :cond_10

    .line 1024
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uid or chatonid is null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1028
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0299

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0042

    new-instance v3, Lcom/sec/chaton/registration/dx;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dx;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1066
    :cond_11
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_15

    .line 1068
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;I)I

    .line 1069
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->o(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1071
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "temp_account_country_code"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)Ljava/lang/String;

    .line 1073
    const-string v0, "did_samsung_account_mapping"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1075
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_12

    .line 1076
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Final contact sync ISO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->p(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    :cond_12
    const-string v0, "account_country_code"

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->p(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1085
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->d(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->q(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/coolots/sso/a/a;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1087
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->q(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 1089
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 1092
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 1093
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 1096
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;)V

    .line 1097
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->h(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 1099
    :cond_15
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xcc

    if-ne v0, v1, :cond_0

    .line 1100
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1101
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1102
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1103
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 1113
    :cond_16
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc356

    if-ne v1, v2, :cond_1a

    .line 1115
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_17

    .line 1116
    const-string v0, "SSO token validaion was finished"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1121
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->r(Lcom/sec/chaton/registration/FragmentRegistPushName;)Z

    move-result v1

    if-ne v1, v4, :cond_18

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->s(Lcom/sec/chaton/registration/FragmentRegistPushName;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_18

    .line 1122
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->t(Lcom/sec/chaton/registration/FragmentRegistPushName;)I

    .line 1123
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 1124
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "user_id"

    aput-object v3, v1, v2

    const-string v2, "email_id"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "mcc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "api_server_url"

    aput-object v3, v1, v2

    .line 1126
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1127
    const-string v3, "client_id"

    const-string v4, "fs24s8z0hh"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1128
    const-string v3, "client_secret"

    const-string v4, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    const-string v3, "mypackage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1130
    const-string v0, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1131
    const-string v0, "MODE"

    const-string v3, "BACKGROUND"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1132
    const-string v0, "additional"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1133
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1134
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1135
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retryCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->s(Lcom/sec/chaton/registration/FragmentRegistPushName;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1140
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->s(Lcom/sec/chaton/registration/FragmentRegistPushName;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_19

    .line 1141
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;I)I

    .line 1144
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1145
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1146
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/dy;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dy;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1165
    :cond_1a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc354

    if-ne v1, v2, :cond_1b

    .line 1166
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1167
    const-string v0, "device over max of limitation"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03d6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/dz;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dz;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1185
    :cond_1b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee7

    if-ne v1, v2, :cond_1c

    .line 1186
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1187
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;Z)V

    .line 1189
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1190
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 1194
    :cond_1c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x2ee8

    if-ne v1, v2, :cond_1d

    .line 1195
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1196
    iget-object v0, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Landroid/app/Activity;Z)V

    .line 1198
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1199
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 1202
    :cond_1d
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->b(Lcom/sec/chaton/registration/FragmentRegistPushName;)V

    .line 1203
    iget-object v1, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1207
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1208
    iget-object v2, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v3, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/eb;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/eb;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v7, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/ea;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/ea;-><init>(Lcom/sec/chaton/registration/du;)V

    invoke-virtual {v1, v8, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Registration Failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 666
    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0xcc -> :sswitch_5
        0x12d -> :sswitch_2
        0x12e -> :sswitch_3
        0x131 -> :sswitch_1
        0x7d1 -> :sswitch_5
        0x8fd -> :sswitch_4
    .end sparse-switch
.end method
