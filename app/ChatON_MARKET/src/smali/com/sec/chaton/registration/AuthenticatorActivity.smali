.class public Lcom/sec/chaton/registration/AuthenticatorActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "AuthenticatorActivity.java"


# instance fields
.field private A:Landroid/content/BroadcastReceiver;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field d:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/os/Handler;

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/d/bj;

.field private k:Lcom/sec/chaton/d/l;

.field private l:Lcom/sec/chaton/d/bj;

.field private m:Landroid/content/Context;

.field private n:Lcom/sec/common/a/d;

.field private o:Landroid/app/ProgressDialog;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Lcom/sec/common/a/d;

.field private v:Z

.field private w:Lcom/sec/chaton/registration/fu;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/Runnable;

.field private z:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 105
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->f:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    .line 139
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->p:Z

    .line 140
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->q:Z

    .line 142
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->r:Z

    .line 143
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->s:Z

    .line 153
    iput v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->t:I

    .line 160
    iput-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->v:Z

    .line 163
    const-class v0, Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    .line 478
    new-instance v0, Lcom/sec/chaton/registration/ah;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ah;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    .line 765
    new-instance v0, Lcom/sec/chaton/registration/ai;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ai;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->d:Landroid/os/AsyncTask;

    .line 1390
    new-instance v0, Lcom/sec/chaton/registration/aj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/aj;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->e:Landroid/os/Handler;

    .line 1618
    new-instance v0, Lcom/sec/chaton/registration/ad;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ad;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->z:Landroid/content/BroadcastReceiver;

    .line 1684
    new-instance v0, Lcom/sec/chaton/registration/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ae;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->A:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1702
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 1703
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1704
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    .line 1705
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 254
    if-nez p1, :cond_3

    .line 256
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 258
    const-string v0, "This is Wifi Only device"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->q:Z

    .line 261
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 276
    :goto_0
    return-void

    .line 263
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->j()V

    goto :goto_0

    .line 268
    :cond_3
    const-string v0, "isDisclaimer"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 270
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto :goto_0

    .line 273
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/AuthenticatorActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/am;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/registration/am;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 1498
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->h()V

    .line 1500
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/registration/am;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1545
    :cond_1
    :goto_0
    return-void

    .line 1508
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 1513
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/registration/al;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/registration/al;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/ak;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ak;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->f:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1710
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_3

    .line 1711
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/am;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1712
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 1713
    const-string v0, "isCurrentUserOwner is false"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    :cond_0
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0410

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/registration/af;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/af;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1742
    :goto_0
    return-void

    .line 1726
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1727
    const-string v0, "This is owner"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1729
    :cond_2
    new-instance v0, Lcom/sec/chaton/registration/es;

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->w:Lcom/sec/chaton/registration/fu;

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/chaton/registration/es;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/sec/chaton/registration/fu;)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/es;->a()V

    goto :goto_0

    .line 1734
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1735
    const-string v0, "android OS is lower than jelly bean MR1"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    :cond_4
    new-instance v0, Lcom/sec/chaton/registration/es;

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->w:Lcom/sec/chaton/registration/fu;

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/chaton/registration/es;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/sec/chaton/registration/fu;)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/es;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->r:Z

    return p1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 792
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityProvisioning;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 793
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->a:Ljava/lang/String;

    .line 794
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    .line 795
    const-string v1, "REG_NAME"

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 796
    const-string v1, "REG_PNO"

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 803
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 804
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->q:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->q:Z

    return p1
.end method

.method private d()V
    .locals 4

    .prologue
    .line 807
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityRegistPushName;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 808
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    .line 809
    const-string v1, "REG_PNO"

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 823
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 824
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 827
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 828
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    .line 829
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->c:Ljava/lang/String;

    .line 832
    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 834
    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 835
    const-string v1, "accountType"

    const-string v2, "com.sec.chaton"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 836
    const-string v1, "authtoken"

    iget-object v2, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 842
    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/Intent;)V

    .line 846
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d()V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 857
    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v0

    .line 859
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "country_letter"

    const-string v3, "GB"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 861
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 862
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[setDisclaimerView] SCC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MCC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 867
    const-string v0, "234"

    .line 870
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 871
    const-string v1, "GB"

    .line 877
    :cond_2
    const-string v2, "450"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "KR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 878
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/NewDisclaimerView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 879
    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 890
    :goto_0
    return-void

    .line 882
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/GlobalDisclaimerView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 883
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->g()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/registration/AuthenticatorActivity;)Lcom/sec/chaton/d/l;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->k:Lcom/sec/chaton/d/l;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1549
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    .line 1550
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/registration/AuthenticatorActivity;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->l:Lcom/sec/chaton/d/bj;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 1554
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1555
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1557
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1561
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1562
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->j:Lcom/sec/chaton/d/bj;

    const-string v1, "FIRST"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/bj;->a(Ljava/lang/String;)V

    .line 1576
    :goto_0
    return-void

    .line 1564
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->s:Z

    if-nez v0, :cond_1

    .line 1565
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->h()V

    .line 1566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->s:Z

    .line 1567
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/NewSPPUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1568
    const-string v1, "isCritical"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1569
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1571
    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1573
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->j:Lcom/sec/chaton/d/bj;

    const-string v1, "FIRST"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/bj;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private j()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x258

    .line 1586
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Landroid/content/Context;)I

    move-result v0

    .line 1587
    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1589
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 1590
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[checkSamsungAccountStatus] verSSO : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " email : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    :cond_0
    const v2, 0x22300

    if-lt v0, v2, :cond_3

    .line 1596
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1599
    const v1, 0x24ab8

    if-lt v0, v1, :cond_1

    .line 1600
    invoke-static {}, Lcom/sec/chaton/util/cb;->f()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1615
    :goto_0
    return-void

    .line 1602
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1603
    const-string v1, "action_sso_check_validation"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1604
    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->p:Z

    .line 1606
    invoke-static {p0}, Lcom/sec/chaton/util/cb;->e(Landroid/content/Context;)V

    goto :goto_0

    .line 1610
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1613
    :cond_3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->h()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->r:Z

    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->i()V

    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->o:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/registration/AuthenticatorActivity;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->j:Lcom/sec/chaton/d/bj;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 281
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_account_login"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    const-string v0, "Previous version is under multi device"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 284
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_selfsms_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 286
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "skipRegi"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "authnum"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 289
    :cond_1
    return-void
.end method

.method a(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 392
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 393
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 395
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "densityDpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Config : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 400
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 403
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v4, :cond_3

    .line 404
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-ne v3, v1, :cond_2

    .line 405
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setRequestedOrientation(I)V

    .line 417
    :cond_1
    :goto_0
    return-void

    .line 406
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 407
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 410
    :cond_3
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-nez v1, :cond_4

    .line 411
    invoke-virtual {p0, v3}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 412
    :cond_4
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v4, v0, :cond_1

    .line 413
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 358
    const v0, 0x7f0300f0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->setContentView(I)V

    .line 364
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/res/Configuration;)V

    .line 385
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v5, 0x5

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 927
    packed-switch p1, :pswitch_data_0

    .line 1387
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 931
    :pswitch_1
    if-ne p2, v5, :cond_1

    .line 935
    const-string v0, "prov_phone"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 936
    const-string v0, "provisioning_select_bridge"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const-string v0, "provisioning_account_login"

    const-string v1, "PHONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    const-string v0, "[PHONE SMS]"

    const-string v1, "ChatON"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 941
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 942
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 943
    const-string v0, "provisioning_selfsms_status"

    const-string v1, "START"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 946
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto :goto_0

    .line 951
    :pswitch_2
    if-ne p2, v4, :cond_0

    .line 952
    const-string v0, "MSISDN"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 954
    const-string v0, "msisdn"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    :cond_2
    invoke-static {}, Lcom/sec/chaton/c/a;->d()V

    .line 957
    const-string v0, "CONTACT_SYNC was finished "

    const-string v1, "runProvisioning"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->c()V

    goto :goto_0

    .line 964
    :pswitch_3
    if-ne p2, v4, :cond_3

    .line 968
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 969
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 970
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 971
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 974
    :cond_3
    if-ne p2, v1, :cond_5

    .line 976
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_letter"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 977
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 978
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 980
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 981
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/SelectCountry;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 982
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 988
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 994
    :pswitch_4
    if-ne p2, v4, :cond_6

    .line 995
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_sns_login_state"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v0, "connect sns accounts finished"

    const-string v1, "runProvisioning"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1001
    const-string v1, "accountManagerResponse"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1002
    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1008
    :cond_6
    if-ne p2, v1, :cond_7

    .line 1010
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1011
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1017
    :cond_7
    if-ne p2, v0, :cond_8

    .line 1018
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_sns_login_state"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d()V

    goto/16 :goto_0

    .line 1023
    :cond_8
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_sns_login_state"

    const-string v2, "CANCEL"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1032
    :pswitch_5
    if-ne p2, v4, :cond_b

    .line 1043
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_account_login"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SKIP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1046
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1047
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/SelectCountry;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1048
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1050
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1054
    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1055
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1061
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1069
    :pswitch_6
    if-ne p2, v4, :cond_c

    .line 1070
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    const-string v0, "first_time_launch"

    const-string v1, "FIRST_TIME"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1073
    :cond_c
    if-ne p2, v1, :cond_d

    .line 1074
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1075
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1078
    :cond_d
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1083
    :pswitch_7
    if-ne p2, v4, :cond_f

    .line 1086
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    invoke-static {}, Lcom/sec/chaton/c/a;->d()V

    .line 1089
    const-string v0, "push name was finished and skip sync "

    const-string v1, "runProvisioning"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "first_time_after_regi"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1091
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->e()V

    .line 1092
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_selfsms_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1093
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->e()V

    goto/16 :goto_0

    .line 1097
    :cond_e
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1103
    const-string v1, "accountManagerResponse"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1104
    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1142
    :cond_f
    const/4 v0, 0x6

    if-ne p2, v0, :cond_10

    .line 1143
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1144
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "authnum"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1145
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_selfsms_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1146
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "acstoken"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1147
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1148
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1149
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1158
    :cond_10
    if-nez p2, :cond_11

    .line 1160
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1163
    :cond_11
    if-ne p2, v1, :cond_0

    .line 1165
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_sns_login_state"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1166
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityConnectAccounts;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1167
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1172
    :pswitch_8
    if-ne p2, v4, :cond_12

    .line 1173
    const-string v0, "MSISDN"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->b:Ljava/lang/String;

    .line 1174
    const-string v0, "AUTH_NUM"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->i:Ljava/lang/String;

    .line 1175
    const-string v0, "TOKEN"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->h:Ljava/lang/String;

    .line 1176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAuthNum"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sToken"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Auther_SMS_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatON"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    const-string v0, "prov_phone"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1179
    const-string v0, "provisioning_selfsms_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d()V

    goto/16 :goto_0

    .line 1187
    :cond_12
    if-nez p2, :cond_13

    .line 1188
    const-string v0, "provisioning_selfsms_status"

    const-string v1, "CANCEL"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1228
    :cond_13
    if-ne p2, v1, :cond_14

    .line 1231
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "prov_phone"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1232
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_selfsms_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1233
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_select_bridge"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1235
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1241
    :cond_14
    if-ne p2, v0, :cond_0

    .line 1242
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_15

    .line 1243
    const-string v0, "[Skip Registration]"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    :cond_15
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d()V

    goto/16 :goto_0

    .line 1252
    :pswitch_9
    if-ne p2, v4, :cond_16

    .line 1253
    const-string v0, "[SamsungAccount] LogIn"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    const-string v0, "provisioning_account_login"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d()V

    goto/16 :goto_0

    .line 1259
    :cond_16
    if-nez p2, :cond_17

    .line 1260
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1261
    :cond_17
    if-ne p2, v0, :cond_19

    .line 1263
    const-string v0, "skip_sms"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1265
    const-string v0, "provisioning_account_login"

    const-string v1, "SKIP"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const-string v0, "[Skip SMS]"

    const-string v1, "ChatON"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 1272
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/SelectCountry;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1274
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1278
    :cond_18
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1282
    :cond_19
    if-ne p2, v5, :cond_1b

    .line 1286
    const-string v0, "prov_phone"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1288
    const-string v0, "provisioning_account_login"

    const-string v1, "PHONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    const-string v0, "[PHONE SMS]"

    const-string v1, "ChatON"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1a

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1292
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/SelectCountry;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1294
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1297
    :cond_1a
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->f()V

    goto/16 :goto_0

    .line 1301
    :cond_1b
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 1302
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->e()V

    .line 1304
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "first_time_after_regi"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1306
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1308
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1310
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1311
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1313
    :cond_1c
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1314
    const-string v1, "accountManagerResponse"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1315
    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1329
    :pswitch_a
    if-ne p2, v4, :cond_1d

    .line 1330
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1331
    const-string v1, "accountManagerResponse"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1333
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "first_time_after_regi"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1334
    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1337
    :cond_1d
    if-nez p2, :cond_0

    .line 1338
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1344
    :pswitch_b
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "skipRegi"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1345
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "skipRegi"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1350
    :cond_1e
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->c:Ljava/lang/String;

    .line 1351
    if-eq p2, v4, :cond_1f

    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 1352
    :cond_1f
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->e()V

    goto/16 :goto_0

    .line 1354
    :cond_20
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    goto/16 :goto_0

    .line 1373
    :pswitch_c
    if-ne p2, v4, :cond_23

    .line 1374
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_21

    .line 1375
    const-string v0, "[ActivityResult] Validation : true"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    :cond_21
    iput-boolean v3, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->q:Z

    .line 1381
    :cond_22
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->y:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1378
    :cond_23
    if-eqz p2, :cond_22

    .line 1379
    invoke-static {p3}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Intent;)V

    goto :goto_1

    .line 927
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_2
        :pswitch_4
        :pswitch_a
        :pswitch_1
        :pswitch_c
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    if-nez v0, :cond_0

    .line 613
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onBackPressed()V

    .line 615
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onBackPressed, Memory address :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSavedInstnace :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->f:Z

    .line 617
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    .line 618
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 780
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 781
    const-string v0, "onConfigurationChanged"

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 168
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 169
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate Mode :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iput-object p0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->j:Lcom/sec/chaton/d/bj;

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->k:Lcom/sec/chaton/d/l;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->l:Lcom/sec/chaton/d/bj;

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a()V

    .line 198
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b()V

    .line 201
    new-instance v0, Lcom/sec/chaton/registration/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ac;-><init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->w:Lcom/sec/chaton/registration/fu;

    .line 214
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    new-instance v0, Lcom/sec/chaton/registration/es;

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->w:Lcom/sec/chaton/registration/fu;

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/chaton/registration/es;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/sec/chaton/registration/fu;)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/es;->a()V

    .line 248
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 894
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 895
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->n:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->n:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->n:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 898
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->h()V

    .line 899
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->p:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 900
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 903
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->u:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->u:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->u:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 907
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->v:Z

    if-nez v0, :cond_3

    .line 908
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 910
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->m:Landroid/content/Context;

    .line 911
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1638
    invoke-super {p0, p1, p2}, Landroid/accounts/AccountAuthenticatorActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 623
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onResume()V

    .line 624
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    if-eqz v0, :cond_0

    .line 625
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    .line 627
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, Memory address :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSavedInstnace :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 917
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 918
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    .line 919
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onSaveInstanceState, Memory address :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSavedInstnace :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AuthenticatorActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    return-void
.end method
