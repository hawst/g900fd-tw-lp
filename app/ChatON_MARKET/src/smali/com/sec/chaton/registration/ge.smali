.class Lcom/sec/chaton/registration/ge;
.super Landroid/os/Handler;
.source "NewDisclaimerFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/NewDisclaimerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 282
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 284
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_0

    .line 294
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe2

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe1

    if-ne v1, v2, :cond_4

    .line 297
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->h(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    .line 299
    const-string v0, "server sent error cause by WRONG_PARAMETER_VALUE"

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/registration/gf;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/gf;-><init>(Lcom/sec/chaton/registration/ge;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 311
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 313
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    sget-object v2, Lcom/sec/chaton/registration/gi;->a:Lcom/sec/chaton/registration/gi;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]GLD failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->h(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    goto/16 :goto_0

    .line 324
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 326
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 327
    if-eqz v1, :cond_8

    .line 330
    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "3.3.51"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 332
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v2}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->c()V

    goto/16 :goto_0

    .line 351
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->h(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "NO"

    invoke-static {v1, v0, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 356
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    const-string v1, "FIRST"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/bj;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    sget-object v2, Lcom/sec/chaton/registration/gi;->c:Lcom/sec/chaton/registration/gi;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 363
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    sget-object v2, Lcom/sec/chaton/registration/gi;->c:Lcom/sec/chaton/registration/gi;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 372
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->j(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->j(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 373
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->j(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 376
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_c

    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xa411

    if-ne v1, v2, :cond_e

    .line 377
    :cond_c
    const-string v0, "first_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    iget-object v0, v0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->b:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 389
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "accept_disclaimer_iso"

    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_d
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 396
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 380
    :catch_0
    move-exception v0

    .line 381
    const-string v0, "the task is already running or finished"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 406
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 407
    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    sget-object v2, Lcom/sec/chaton/registration/gi;->d:Lcom/sec/chaton/registration/gi;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 413
    :sswitch_3
    const-string v1, "2"

    .line 414
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 416
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_f

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "YES"

    invoke-static {v0, v1, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 419
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 420
    const-string v0, "Samsung apps is ready to upgrade chaton"

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 423
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "NO"

    invoke-static {v0, v1, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 424
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 425
    const-string v0, "Samsung apps is NOT ready to upgrade chaton"

    iget-object v1, p0, Lcom/sec/chaton/registration/ge;->a:Lcom/sec/chaton/registration/NewDisclaimerFragment;

    invoke-static {v1}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 282
    nop

    :sswitch_data_0
    .sparse-switch
        0x68 -> :sswitch_0
        0x450 -> :sswitch_1
        0x451 -> :sswitch_2
        0x452 -> :sswitch_3
    .end sparse-switch
.end method
