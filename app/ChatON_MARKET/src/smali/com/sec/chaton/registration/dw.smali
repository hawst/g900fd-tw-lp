.class Lcom/sec/chaton/registration/dw;
.super Ljava/lang/Object;
.source "FragmentRegistPushName.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/du;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/du;)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 978
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->n(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->k(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->l(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v4, v4, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v4}, Lcom/sec/chaton/registration/FragmentRegistPushName;->m(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 984
    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v1, v1, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v2, v2, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->c(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v3, v3, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->d(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :cond_0
    return-void

    .line 979
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/dw;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->l(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
