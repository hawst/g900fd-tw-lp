.class Lcom/sec/chaton/registration/fm;
.super Ljava/lang/Object;
.source "FristLauncherDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/es;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/es;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/16 v1, 0xd

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->h(Lcom/sec/chaton/registration/es;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->b(Lcom/sec/chaton/registration/es;)I

    move-result v0

    if-le v0, v1, :cond_0

    .line 585
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v1}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 586
    const-string v1, "mapping_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 587
    const-string v1, "general_tab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 588
    iget-object v1, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v1}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/es;->a(Lcom/sec/chaton/registration/es;I)I

    .line 591
    :cond_0
    invoke-static {v3}, Lcom/sec/chaton/util/y;->a(I)V

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Log On"

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 593
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 611
    :cond_1
    :goto_0
    return-void

    .line 597
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->h(Lcom/sec/chaton/registration/es;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->b(Lcom/sec/chaton/registration/es;)I

    move-result v0

    if-le v0, v1, :cond_3

    .line 599
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v1}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 600
    const-string v1, "mapping_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 601
    const-string v1, "general_tab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 602
    iget-object v1, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v1}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/es;->a(Lcom/sec/chaton/registration/es;I)I

    .line 605
    :cond_3
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Log On With Save"

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lcom/sec/chaton/registration/fm;->a:Lcom/sec/chaton/registration/es;

    invoke-static {v0}, Lcom/sec/chaton/registration/es;->d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    goto :goto_0
.end method
