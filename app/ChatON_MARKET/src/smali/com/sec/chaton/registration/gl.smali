.class public Lcom/sec/chaton/registration/gl;
.super Landroid/widget/BaseAdapter;
.source "SelectLanguageFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/SelectLanguageFragment;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/registration/SelectLanguageFragment;)V
    .locals 1

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 150
    invoke-static {p1}, Lcom/sec/chaton/registration/SelectLanguageFragment;->a(Lcom/sec/chaton/registration/SelectLanguageFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/gl;->b:Landroid/view/LayoutInflater;

    .line 151
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->b(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->b(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 165
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030121

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 177
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/gl;->c:Landroid/widget/TextView;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->c:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 179
    const v0, 0x7f07014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/gl;->d:Landroid/widget/TextView;

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->d:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->c(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->c(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 186
    iget-object v2, p0, Lcom/sec/chaton/registration/gl;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->c(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFilteredCountry ="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v2}, Lcom/sec/chaton/registration/SelectLanguageFragment;->c(Lcom/sec/chaton/registration/SelectLanguageFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "position"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    :goto_0
    return-object v1

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v0}, Lcom/sec/chaton/registration/SelectLanguageFragment;->d(Lcom/sec/chaton/registration/SelectLanguageFragment;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/registration/gl;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/registration/gl;->a:Lcom/sec/chaton/registration/SelectLanguageFragment;

    invoke-static {v2}, Lcom/sec/chaton/registration/SelectLanguageFragment;->d(Lcom/sec/chaton/registration/SelectLanguageFragment;)[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
