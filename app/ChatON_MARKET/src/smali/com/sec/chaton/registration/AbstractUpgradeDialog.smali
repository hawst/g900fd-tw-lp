.class public abstract Lcom/sec/chaton/registration/AbstractUpgradeDialog;
.super Lcom/sec/chaton/base/BaseActivity;
.source "AbstractUpgradeDialog.java"


# instance fields
.field public a:Landroid/widget/TextView;

.field private final b:Ljava/lang/String;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 31
    const-class v0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->g:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/AbstractUpgradeDialog;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->l:Z

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 133
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "showPasswordLockActivity"

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 138
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->startActivity(Landroid/content/Intent;)V

    .line 143
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/registration/AbstractUpgradeDialog;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 218
    :try_start_0
    const-string v0, "com.sec.android.app.samsungapps"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 219
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220
    iget-object v2, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->o:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    const-string v0, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 222
    iget-object v2, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->m:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    :goto_0
    :try_start_1
    const-string v0, "com.android.vending"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 230
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    const-string v0, "com.android.vending"

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 233
    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 239
    :cond_1
    :goto_1
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 225
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 234
    :catch_1
    move-exception v0

    .line 235
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 236
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 149
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "initiateOneButtonAppl"

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->e:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/registration/a;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 244
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "handleCriticalCase"

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->k:Z

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->g:Landroid/widget/TextView;

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->i:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/e;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/registration/e;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->g:Landroid/widget/TextView;

    const v1, 0x7f0b02fb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->i:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/f;

    invoke-direct {v1, p0, p2}, Lcom/sec/chaton/registration/f;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 170
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "initiateTwoButtonAppl"

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->l:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    if-eqz v0, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->c()V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 178
    const v0, 0x7f07054e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->c:Landroid/widget/LinearLayout;

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/registration/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/registration/b;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    const v0, 0x7f070551

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->d:Landroid/widget/LinearLayout;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/registration/c;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/registration/c;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/d;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/registration/d;-><init>(Lcom/sec/chaton/registration/AbstractUpgradeDialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f070556

    const/16 v5, 0xb

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0, v3}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->requestWindowFeature(I)Z

    .line 61
    const v0, 0x7f030150

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 65
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_0

    .line 66
    invoke-virtual {p0, v2}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->setFinishOnTouchOutside(Z)V

    .line 69
    :cond_0
    const v0, 0x7f07054d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->g:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f07054c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->a:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f07023b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->e:Landroid/widget/LinearLayout;

    .line 74
    const v0, 0x7f070554

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->f:Landroid/widget/LinearLayout;

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f070555

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->i:Landroid/widget/Button;

    .line 78
    const v0, 0x7f07054f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->m:Landroid/widget/ImageView;

    .line 79
    const v0, 0x7f070552

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->n:Landroid/widget/ImageView;

    .line 81
    const v0, 0x7f070550

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->o:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f070553

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->p:Landroid/widget/TextView;

    .line 84
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_1

    .line 85
    const v0, 0x7f070557

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 87
    invoke-virtual {p0, v6}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 94
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isCritical"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->k:Z

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isReadyApps"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 97
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    .line 106
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/cb;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->l:Z

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->a()V

    .line 111
    return-void

    .line 89
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    const v0, 0x7f070557

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isReadyApps"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "YES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    iput-boolean v3, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    goto :goto_1

    .line 100
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isReadyApps"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    iput-boolean v2, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    goto :goto_1

    .line 103
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->j:Z

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 116
    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/base/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 126
    invoke-direct {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b()V

    .line 127
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    return-void
.end method
