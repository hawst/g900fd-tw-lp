.class Lcom/sec/chaton/registration/ct;
.super Landroid/os/Handler;
.source "FragmentRegist.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 1971
    iput-object p1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const v5, 0x7f0b00be

    const v4, 0x7f0b003b

    const/4 v6, 0x0

    .line 1975
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1977
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x12f

    if-ne v1, v2, :cond_0

    .line 1978
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2068
    :cond_0
    :goto_0
    return-void

    .line 1982
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->g(Lcom/sec/chaton/registration/FragmentRegist;)V

    .line 1984
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1985
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1988
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1989
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 1992
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1993
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1995
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_4

    .line 1996
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v3}, Lcom/sec/chaton/registration/FragmentRegist;->s(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1997
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegist;->n(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1998
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 2003
    :cond_4
    const-string v1, ""

    .line 2004
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_5

    .line 2007
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2030
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegist;->n(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 2031
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2032
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 2009
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_6

    .line 2010
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2012
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e85

    if-ne v0, v1, :cond_7

    .line 2014
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2026
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2037
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 2038
    iget-object v1, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegist;->n(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b016c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/registration/cu;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/cu;-><init>(Lcom/sec/chaton/registration/ct;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 2061
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2062
    iget-object v0, p0, Lcom/sec/chaton/registration/ct;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0
.end method
