.class Lcom/sec/chaton/registration/ce;
.super Landroid/content/BroadcastReceiver;
.source "FragmentNonSelfSMS.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 1793
    iput-object p1, p0, Lcom/sec/chaton/registration/ce;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1796
    const-string v2, ""

    .line 1798
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1800
    if-eqz v0, :cond_0

    .line 1801
    const-string v1, "pdus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 1802
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 1873
    :cond_0
    :goto_0
    return-void

    .line 1809
    :cond_1
    array-length v4, v0

    const/4 v1, 0x0

    move-object v3, v2

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v1, v0, v2

    .line 1810
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v1}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v1

    .line 1811
    if-nez v1, :cond_3

    .line 1821
    :cond_2
    if-eqz v3, :cond_0

    const-string v0, ": "

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837
    const-string v0, ": "

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1839
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1842
    const-string v1, ": "

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    .line 1847
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v2, v0, 0x4

    if-ge v1, v2, :cond_4

    .line 1848
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 1849
    const-string v0, "receviced code is less than excepted code"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1816
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v3

    .line 1817
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received verification Code :"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1855
    :cond_4
    add-int/lit8 v1, v0, 0x4

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1857
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "final received verification Code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860
    if-eqz v0, :cond_0

    .line 1861
    iget-object v1, p0, Lcom/sec/chaton/registration/ce;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1862
    iget-object v1, p0, Lcom/sec/chaton/registration/ce;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 1864
    const-string v1, "authnum"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
