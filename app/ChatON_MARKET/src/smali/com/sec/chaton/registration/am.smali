.class final enum Lcom/sec/chaton/registration/am;
.super Ljava/lang/Enum;
.source "AuthenticatorActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/registration/am;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/registration/am;

.field public static final enum b:Lcom/sec/chaton/registration/am;

.field public static final enum c:Lcom/sec/chaton/registration/am;

.field public static final enum d:Lcom/sec/chaton/registration/am;

.field public static final enum e:Lcom/sec/chaton/registration/am;

.field private static final synthetic f:[Lcom/sec/chaton/registration/am;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 146
    new-instance v0, Lcom/sec/chaton/registration/am;

    const-string v1, "get_server_address"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/registration/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/am;->a:Lcom/sec/chaton/registration/am;

    .line 147
    new-instance v0, Lcom/sec/chaton/registration/am;

    const-string v1, "version_for_nation"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/registration/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/am;->b:Lcom/sec/chaton/registration/am;

    .line 148
    new-instance v0, Lcom/sec/chaton/registration/am;

    const-string v1, "version_for_upgrade"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/registration/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/am;->c:Lcom/sec/chaton/registration/am;

    .line 149
    new-instance v0, Lcom/sec/chaton/registration/am;

    const-string v1, "accept_disclaimer"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/registration/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/am;->d:Lcom/sec/chaton/registration/am;

    .line 150
    new-instance v0, Lcom/sec/chaton/registration/am;

    const-string v1, "chaton_v"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/registration/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/registration/am;->e:Lcom/sec/chaton/registration/am;

    .line 145
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/registration/am;

    sget-object v1, Lcom/sec/chaton/registration/am;->a:Lcom/sec/chaton/registration/am;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/registration/am;->b:Lcom/sec/chaton/registration/am;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/registration/am;->c:Lcom/sec/chaton/registration/am;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/registration/am;->d:Lcom/sec/chaton/registration/am;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/registration/am;->e:Lcom/sec/chaton/registration/am;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/registration/am;->f:[Lcom/sec/chaton/registration/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/registration/am;
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/sec/chaton/registration/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/am;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/registration/am;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/chaton/registration/am;->f:[Lcom/sec/chaton/registration/am;

    invoke-virtual {v0}, [Lcom/sec/chaton/registration/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/registration/am;

    return-object v0
.end method
